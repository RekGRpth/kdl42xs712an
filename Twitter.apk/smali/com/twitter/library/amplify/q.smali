.class Lcom/twitter/library/amplify/q;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyPlayer;

.field final synthetic b:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/q;->b:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    iput-object p2, p0, Lcom/twitter/library/amplify/q;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/amplify/q;->b:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    iget-object v1, p0, Lcom/twitter/library/amplify/q;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->a(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;Lcom/twitter/library/amplify/AmplifyPlayer;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/q;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/q;->b:Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;

    invoke-static {v0}, Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;->a(Lcom/twitter/library/amplify/PlayerDelegateAmplifyView;)Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/q;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(ZZ)V

    :cond_0
    return-void
.end method
