.class Lcom/twitter/library/amplify/j;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/amplify/i;


# instance fields
.field a:Z

.field final b:Lcom/twitter/library/amplify/i;

.field final synthetic c:Lcom/twitter/library/amplify/AmplifyPlayer;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/AmplifyPlayer;Lcom/twitter/library/amplify/i;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->a()V

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/amplify/i;->a(II)V

    return-void
.end method

.method public a(IIZZ)V
    .locals 2

    invoke-static {}, Lcom/twitter/library/amplify/AmplifyPlayer;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/AmplifyPlayer;)I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(Lcom/twitter/library/amplify/AmplifyPlayer;)Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/AmplifyPlayer;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/AmplifyPlayer;I)I

    const/4 p4, 0x1

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(Lcom/twitter/library/amplify/AmplifyPlayer;)Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->b(Lcom/twitter/library/amplify/AmplifyPlayer;)Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->c(Lcom/twitter/library/amplify/AmplifyPlayer;)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(F)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/twitter/library/amplify/i;->a(IIZZ)V

    return-void
.end method

.method public a(ILjava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "error"

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->y()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->onCompletion(Landroid/media/MediaPlayer;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/amplify/i;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;->b:Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;

    if-eq p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Lcom/twitter/library/amplify/AmplifyPlayer;Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0, p1}, Lcom/twitter/library/amplify/i;->a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Lcom/twitter/library/amplify/AmplifyPlayer;)Lcom/twitter/library/amplify/model/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/d;->a()V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->b()V

    return-void
.end method

.method public b(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/amplify/i;->b(II)V

    return-void
.end method

.method public c()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->d(Lcom/twitter/library/amplify/AmplifyPlayer;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->c()V

    return-void
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->d()V

    return-void
.end method

.method public e()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/j;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/amplify/j;->a:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->e()V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/j;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/j;->a:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->f()V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/j;->b:Lcom/twitter/library/amplify/i;

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->g()V

    return-void
.end method
