.class Lcom/twitter/library/amplify/f;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyPlayer;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/f;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    const-string/jumbo v0, "android.intent.action.SCREEN_OFF"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/f;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/f;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->p()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "android.intent.action.SCREEN_ON"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/f;->a:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->n()Lcom/twitter/library/amplify/i;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/library/amplify/i;->b()V

    goto :goto_0
.end method
