.class Lcom/twitter/library/amplify/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnCompletionListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/c;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCompletion(Landroid/media/MediaPlayer;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/c;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->g:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/c;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->g:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/c;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b:Landroid/media/MediaPlayer$OnCompletionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/c;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->b:Landroid/media/MediaPlayer$OnCompletionListener;

    iget-object v1, p0, Lcom/twitter/library/amplify/c;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-static {v1}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/media/MediaPlayer$OnCompletionListener;->onCompletion(Landroid/media/MediaPlayer;)V

    :cond_0
    return-void
.end method
