.class public Lcom/twitter/library/amplify/AmplifyVideoTextureView;
.super Landroid/view/TextureView;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# instance fields
.field protected a:I

.field protected b:I

.field private final c:Lcom/twitter/library/amplify/AmplifyPlayer;

.field private d:Landroid/view/TextureView$SurfaceTextureListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/view/TextureView;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/library/amplify/m;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/m;-><init>(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->d:Landroid/view/TextureView$SurfaceTextureListener;

    iput-object p2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-direct {p0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/amplify/AmplifyVideoTextureView;)Lcom/twitter/library/amplify/AmplifyPlayer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    return-object v0
.end method

.method private b()V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    iput v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->d:Landroid/view/TextureView$SurfaceTextureListener;

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->setSurfaceTextureListener(Landroid/view/TextureView$SurfaceTextureListener;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->setFocusable(Z)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->requestFocus()Z

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->c:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->w()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->getSurfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->setSurfaceTexture(Landroid/graphics/SurfaceTexture;)V

    :cond_0
    return-void
.end method

.method public a(II)V
    .locals 1

    iput p1, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    iput p2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->requestLayout()V

    :cond_0
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Landroid/view/TextureView;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->requestFocus()Z

    return-void
.end method

.method public onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    const-class v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/view/TextureView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    const-class v0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onMeasure(II)V
    .locals 4

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    invoke-static {v0, p1}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->getDefaultSize(II)I

    move-result v1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    invoke-static {v0, p2}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->getDefaultSize(II)I

    move-result v0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    if-lez v2, :cond_0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    mul-int/2addr v3, v1

    if-le v2, v3, :cond_1

    iget v0, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    mul-int/2addr v0, v1

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    div-int/2addr v0, v2

    :cond_0
    :goto_0
    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->setMeasuredDimension(II)V

    return-void

    :cond_1
    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    mul-int/2addr v2, v0

    iget v3, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    mul-int/2addr v3, v1

    if-ge v2, v3, :cond_0

    iget v1, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->a:I

    mul-int/2addr v1, v0

    iget v2, p0, Lcom/twitter/library/amplify/AmplifyVideoTextureView;->b:I

    div-int/2addr v1, v2

    goto :goto_0
.end method
