.class public Lcom/twitter/library/amplify/model/AmplifyVideoStitched;
.super Lcom/twitter/library/amplify/model/AmplifyVideo;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final g:J

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/Map;

.field private final j:J

.field private final k:Ljava/lang/String;

.field private final l:Ljava/util/Map;

.field private final m:D


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/amplify/model/e;

    invoke-direct {v0}, Lcom/twitter/library/amplify/model/e;-><init>()V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Landroid/os/Parcel;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    invoke-static {p1}, Lky;->b(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    invoke-static {p1}, Lky;->b(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;JDLjava/util/Map;Ljava/util/Map;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Ljava/lang/String;)V

    iput-wide p3, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    iput-object p2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    iput-object p10, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    iput-wide p6, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    iput-object p5, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    iput-object p11, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    iput-wide p8, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;D)Ljava/util/List;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    cmpg-double v0, p2, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public b()D
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    return-wide v0
.end method

.method public b(D)Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    goto :goto_0
.end method

.method public c(D)Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    const-string/jumbo v0, "ad"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "video"

    goto :goto_0
.end method

.method public d(D)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    cmpg-double v0, p1, v0

    if-gez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    goto :goto_0
.end method

.method public e(D)Z
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    cmpl-double v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-super {p0, p1}, Lcom/twitter/library/amplify/model/AmplifyVideo;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    check-cast p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;

    iget-wide v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    iget-wide v4, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    invoke-static {v2, v3, v4, v5}, Ljava/lang/Double;->compare(DD)I

    move-result v2

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    iget-wide v4, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-wide v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    iget-wide v4, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    if-nez v2, :cond_8

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    if-nez v2, :cond_b

    :cond_d
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    if-nez v2, :cond_e

    :cond_10
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    if-eqz v2, :cond_11

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_11
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 7

    const/16 v6, 0x20

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/amplify/model/AmplifyVideo;->hashCode()I

    move-result v0

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    iget-wide v4, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    iget-wide v4, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    iget-wide v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    invoke-static {v1, v2}, Ljava/lang/Double;->doubleToLongBits(D)J

    move-result-wide v1

    mul-int/lit8 v0, v0, 0x1f

    ushr-long v3, v1, v6

    xor-long/2addr v1, v3

    long-to-int v1, v1

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/library/amplify/model/AmplifyVideo;->writeToParcel(Landroid/os/Parcel;I)V

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->g:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->h:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->i:Ljava/util/Map;

    invoke-static {v0, p1}, Lky;->b(Ljava/util/Map;Landroid/os/Parcel;)V

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->j:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->l:Ljava/util/Map;

    invoke-static {v0, p1}, Lky;->b(Ljava/util/Map;Landroid/os/Parcel;)V

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideoStitched;->m:D

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeDouble(D)V

    return-void
.end method
