.class final Lcom/twitter/library/amplify/model/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable$Creator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Parcel;)Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 4

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/amplify/model/AmplifyVideo;

    sget-object v1, Lcom/twitter/library/amplify/model/AmplifyVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->readTypedArray([Ljava/lang/Object;Landroid/os/Parcelable$Creator;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {p1}, Lky;->a(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/amplify/model/AmplifyInstance;

    invoke-direct {v3, v0, v1, v2}, Lcom/twitter/library/amplify/model/AmplifyInstance;-><init>([Lcom/twitter/library/amplify/model/AmplifyVideo;ILjava/util/Map;)V

    return-object v3
.end method

.method public a(I)[Lcom/twitter/library/amplify/model/AmplifyInstance;
    .locals 1

    new-array v0, p1, [Lcom/twitter/library/amplify/model/AmplifyInstance;

    return-object v0
.end method

.method public synthetic createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/model/a;->a(Landroid/os/Parcel;)Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    return-object v0
.end method

.method public synthetic newArray(I)[Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/amplify/model/a;->a(I)[Lcom/twitter/library/amplify/model/AmplifyInstance;

    move-result-object v0

    return-object v0
.end method
