.class public Lcom/twitter/library/amplify/model/AmplifyVideo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field protected final a:Ljava/lang/String;

.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/String;

.field protected final d:J

.field protected final e:Z

.field protected final f:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/amplify/model/c;

    invoke-direct {v0}, Lcom/twitter/library/amplify/model/c;-><init>()V

    sput-object v0, Lcom/twitter/library/amplify/model/AmplifyVideo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    invoke-static {p1}, Lky;->b(Landroid/os/Parcel;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected constructor <init>(Ljava/lang/String;)V
    .locals 8

    const/4 v1, 0x0

    const-wide/16 v3, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    move-object v2, v1

    move-object v5, p1

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLjava/util/Map;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLjava/util/Map;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p5, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    iput-object p1, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    iput-wide p3, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    iput-boolean p6, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    iput-object p7, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a(D)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    return-object v0
.end method

.method public a(Ljava/lang/String;D)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    goto :goto_0
.end method

.method public a()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()D
    .locals 2

    const-wide/high16 v0, -0x4010000000000000L    # -1.0

    return-wide v0
.end method

.method public b(D)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    return-object v0
.end method

.method public c(D)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    return-object v0
.end method

.method public d(D)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e(D)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/amplify/model/AmplifyVideo;

    iget-boolean v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    iget-boolean v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    iget-wide v4, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    invoke-interface {v2, v3}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    :cond_6
    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    if-nez v2, :cond_6

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    if-nez v2, :cond_9

    :cond_b
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    if-nez v2, :cond_c

    :cond_e
    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p1, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    iget-wide v4, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->e:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    iget-object v0, p0, Lcom/twitter/library/amplify/model/AmplifyVideo;->f:Ljava/util/Map;

    invoke-static {v0, p1}, Lky;->b(Ljava/util/Map;Landroid/os/Parcel;)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
