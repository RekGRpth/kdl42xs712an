.class public Lcom/twitter/library/amplify/model/parser/b;
.super Lcom/twitter/library/amplify/model/parser/a;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/amplify/model/parser/a;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/amplify/model/AmplifyVideo;
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    :goto_0
    if-eqz v0, :cond_0

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_0

    sget-object v2, Lcom/twitter/library/amplify/model/parser/c;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/amplify/model/parser/b;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/amplify/model/AmplifyVideo;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/amplify/model/AmplifyVideo;

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/amplify/model/AmplifyVideo;
    .locals 12

    const/4 v9, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    const-wide/16 v3, 0x0

    move v6, v9

    move-object v5, v7

    move-object v2, v7

    move-object v1, v7

    move-object v8, v7

    :goto_0
    if-eqz v0, :cond_5

    sget-object v10, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v10, :cond_5

    sget-object v10, Lcom/twitter/library/amplify/model/parser/c;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v10, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    move-object v0, v8

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v8

    move-object v11, v0

    move-object v0, v8

    move-object v8, v11

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v8

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v10, "uuid"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_1

    move-object v1, v0

    :cond_1
    const-string/jumbo v10, "video_type"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_2

    move-object v2, v0

    :cond_2
    const-string/jumbo v10, "source"

    invoke-virtual {v10, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_0

    move-object v5, v0

    move-object v0, v8

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "controllable"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    if-lez v0, :cond_4

    move v0, v9

    :goto_2
    move v6, v0

    :cond_3
    const-string/jumbo v0, "owner_id"

    invoke-virtual {v0, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v3

    move-object v0, v8

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    if-nez v5, :cond_6

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "amplify playlist item has no stream"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_6
    new-instance v0, Lcom/twitter/library/amplify/model/AmplifyVideo;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/amplify/model/AmplifyVideo;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;ZLjava/util/Map;)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Lcom/fasterxml/jackson/core/JsonParser;)V
    .locals 9

    const/4 v2, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v1, v6

    move-object v4, v0

    move-object v3, v2

    move v0, v6

    :goto_0
    if-eqz v4, :cond_9

    sget-object v7, Lcom/twitter/library/amplify/model/parser/c;->a:[I

    invoke-virtual {v4}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v4

    aget v4, v7, v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v4

    goto :goto_0

    :pswitch_0
    const-string/jumbo v4, "android"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    if-eqz v0, :cond_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v7, "android-v"

    invoke-virtual {v4, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v2}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v7, "android-v"

    const-string/jumbo v8, ""

    invoke-virtual {v4, v7, v8}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v4

    const/16 v7, 0xa

    :try_start_0
    invoke-static {v4, v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;I)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_2
    if-lt v1, v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/parser/b;->b()I

    move-result v4

    if-lt v4, v1, :cond_2

    move v4, v5

    :goto_3
    if-eqz v4, :cond_3

    iput-boolean v6, p0, Lcom/twitter/library/amplify/model/parser/b;->d:Z

    iput-boolean v6, p0, Lcom/twitter/library/amplify/model/parser/b;->c:Z

    move v0, v1

    goto :goto_1

    :catch_0
    move-exception v4

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_2
    move v4, v6

    goto :goto_3

    :cond_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_4
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v4, "videos"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {p1}, Lcom/twitter/library/amplify/model/parser/b;->b(Lcom/fasterxml/jackson/core/JsonParser;)[Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v3

    goto :goto_1

    :cond_5
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v4, "play_on_phone"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v4

    if-nez v4, :cond_6

    move v4, v5

    :goto_4
    iput-boolean v4, p0, Lcom/twitter/library/amplify/model/parser/b;->d:Z

    goto :goto_1

    :cond_6
    move v4, v6

    goto :goto_4

    :cond_7
    const-string/jumbo v4, "play_on_tablet"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v4

    if-nez v4, :cond_8

    move v4, v5

    :goto_5
    iput-boolean v4, p0, Lcom/twitter/library/amplify/model/parser/b;->c:Z

    goto/16 :goto_1

    :cond_8
    move v4, v6

    goto :goto_5

    :pswitch_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    :cond_9
    iput-object v3, p0, Lcom/twitter/library/amplify/model/parser/b;->a:[Lcom/twitter/library/amplify/model/AmplifyVideo;

    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Ljava/io/InputStream;)V
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p1}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/library/amplify/model/parser/b;->a(Lcom/fasterxml/jackson/core/JsonParser;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    return-void

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
.end method
