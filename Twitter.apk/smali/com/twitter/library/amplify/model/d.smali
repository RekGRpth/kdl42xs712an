.class public Lcom/twitter/library/amplify/model/d;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field a:Z

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field f:Z

.field g:Z

.field h:Z

.field private final i:Lcom/twitter/library/amplify/AmplifyPlayer;


# direct methods
.method public constructor <init>(Lcom/twitter/library/amplify/AmplifyPlayer;)V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->g:Z

    iput-object p1, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    return-void
.end method

.method public static a(II)I
    .locals 4

    if-lez p1, :cond_0

    if-gtz p0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    const-wide/high16 v0, 0x4059000000000000L    # 100.0

    int-to-double v2, p0

    mul-double/2addr v0, v2

    int-to-double v2, p1

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/amplify/model/AmplifyVideo;II)Lcom/twitter/library/amplify/r;
    .locals 6

    const/4 v5, 0x0

    if-eqz p0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/AmplifyVideo;->b()D

    move-result-wide v0

    double-to-int v4, v0

    const/4 v0, -0x1

    if-eq v4, v0, :cond_2

    if-gt p1, v4, :cond_1

    move v2, p1

    :goto_0
    if-eqz v4, :cond_0

    invoke-static {v2, v4}, Lcom/twitter/library/amplify/model/d;->a(II)I

    move-result v5

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/r;

    move v1, p1

    move v3, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/amplify/r;-><init>(IIIII)V

    return-object v0

    :cond_1
    sub-int v2, p1, v4

    sub-int v4, p2, v4

    goto :goto_0

    :cond_2
    move v4, p2

    move v2, p1

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 10

    const/16 v9, 0x19

    const/4 v8, 0x1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->f:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v0

    iget v1, v0, Lcom/twitter/library/amplify/r;->e:I

    iget-object v2, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v2}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v3

    iget-boolean v2, p0, Lcom/twitter/library/amplify/model/d;->h:Z

    if-eqz v2, :cond_1

    const/16 v2, 0x64

    if-lt v1, v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/library/amplify/model/d;->b:Z

    if-nez v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->b()V

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_3

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-boolean v2, p0, Lcom/twitter/library/amplify/model/d;->e:Z

    if-nez v2, :cond_3

    iget v0, v0, Lcom/twitter/library/amplify/r;->a:I

    int-to-double v4, v0

    invoke-virtual {v3}, Lcom/twitter/library/amplify/model/AmplifyVideo;->b()D

    move-result-wide v6

    cmpl-double v0, v4, v6

    if-ltz v0, :cond_3

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->g:Z

    if-eqz v0, :cond_3

    iput-boolean v8, p0, Lcom/twitter/library/amplify/model/d;->e:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->g:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_100"

    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/amplify/model/AmplifyVideo;D)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->e()V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->b()V

    goto :goto_0

    :cond_3
    const/16 v0, 0x4b

    if-lt v1, v0, :cond_4

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->d:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_75"

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/twitter/library/amplify/model/d;->d:Z

    goto :goto_1

    :cond_4
    const/16 v0, 0x32

    if-lt v1, v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->c:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_50"

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/twitter/library/amplify/model/d;->c:Z

    goto :goto_1

    :cond_5
    if-lt v1, v9, :cond_6

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->b:Z

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_25"

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/twitter/library/amplify/model/d;->b:Z

    goto :goto_1

    :cond_6
    if-ge v1, v9, :cond_2

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_0"

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    iput-boolean v8, p0, Lcom/twitter/library/amplify/model/d;->a:Z

    goto :goto_1

    :cond_7
    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->c()V

    goto/16 :goto_0
.end method

.method public a(Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->e()V

    iput-boolean v1, p0, Lcom/twitter/library/amplify/model/d;->f:Z

    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;->a:Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;

    if-ne p1, v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->h:Z

    sget-object v0, Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;->a:Lcom/twitter/library/amplify/AmplifyPlayer$PlayerStartType;

    if-eq p1, v0, :cond_0

    iput-boolean v1, p0, Lcom/twitter/library/amplify/model/d;->g:Z

    :cond_0
    iput-boolean v1, p0, Lcom/twitter/library/amplify/model/d;->a:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_0"

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->a()V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/model/d;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    const-wide/16 v1, 0x1f4

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/amplify/model/d;->sendMessageDelayed(Landroid/os/Message;J)Z

    return-void
.end method

.method public c()V
    .locals 1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/model/d;->removeMessages(I)V

    return-void
.end method

.method public d()V
    .locals 3

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/library/amplify/model/d;->f:Z

    iput-boolean v2, p0, Lcom/twitter/library/amplify/model/d;->h:Z

    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->c()V

    iget-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->e:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->e:Z

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    const-string/jumbo v1, "playback_100"

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Ljava/lang/String;)V

    :cond_0
    iput-boolean v2, p0, Lcom/twitter/library/amplify/model/d;->g:Z

    return-void
.end method

.method e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->a:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->b:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->c:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->d:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->e:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/model/d;->h:Z

    return-void
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/model/d;->i:Lcom/twitter/library/amplify/AmplifyPlayer;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/library/amplify/model/d;->a()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
