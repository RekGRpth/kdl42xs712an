.class Lcom/twitter/library/amplify/e;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;


# direct methods
.method private constructor <init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/e;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;Lcom/twitter/library/amplify/a;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/e;-><init>(Lcom/twitter/library/amplify/AmplifyMediaPlayer;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/media/MediaPlayer;IILjava/lang/String;)Z
    .locals 5

    const/4 v4, 0x1

    if-nez p4, :cond_0

    const-string/jumbo v0, "Error: %d, %d"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object p4

    :cond_0
    invoke-static {}, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "AmplifyMediaPlayer"

    invoke-static {v0, p4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/e;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->c:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/e;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    sget-object v1, Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iput-object v1, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->d:Lcom/twitter/library/amplify/AmplifyMediaPlayer$PlayerState;

    iget-object v0, p0, Lcom/twitter/library/amplify/e;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/amplify/e;->a:Lcom/twitter/library/amplify/AmplifyMediaPlayer;

    iget-object v0, v0, Lcom/twitter/library/amplify/AmplifyMediaPlayer;->a:Lcom/twitter/library/amplify/i;

    invoke-interface {v0, p2, p4}, Lcom/twitter/library/amplify/i;->a(ILjava/lang/String;)V

    :cond_2
    return v4
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/twitter/library/amplify/e;->a(Landroid/media/MediaPlayer;IILjava/lang/String;)Z

    move-result v0

    return v0
.end method
