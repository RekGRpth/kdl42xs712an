.class Lcom/twitter/library/amplify/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# instance fields
.field final synthetic a:Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/l;->a:Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/l;->a:Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->a(Landroid/view/SurfaceHolder;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/l;->a:Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/l;->a:Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->g(Z)V

    iget-object v0, p0, Lcom/twitter/library/amplify/l;->a:Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;

    invoke-static {v0}, Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;->a(Lcom/twitter/library/amplify/AmplifyVideoSurfaceView;)Lcom/twitter/library/amplify/AmplifyPlayer;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    return-void
.end method
