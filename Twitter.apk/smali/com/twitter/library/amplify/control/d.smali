.class Lcom/twitter/library/amplify/control/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;


# direct methods
.method constructor <init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;Landroid/content/Context;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iput-object p2, p0, Lcom/twitter/library/amplify/control/d;->a:Landroid/content/Context;

    iput p3, p0, Lcom/twitter/library/amplify/control/d;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-static {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Landroid/view/View;

    move-result-object v0

    sget v1, Lig;->msg:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    :try_start_0
    iget-object v1, p0, Lcom/twitter/library/amplify/control/d;->a:Landroid/content/Context;

    iget v2, p0, Lcom/twitter/library/amplify/control/d;->b:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-static {v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/d;->c:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-static {v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->bringChildToFront(Landroid/view/View;)V

    return-void

    :catch_0
    move-exception v1

    iget-object v1, p0, Lcom/twitter/library/amplify/control/d;->a:Landroid/content/Context;

    sget v2, Lil;->amplify_playlist_download_failed:I

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method
