.class public Lcom/twitter/library/amplify/control/AmplifyVideoControlView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# instance fields
.field private A:Z

.field private B:Z

.field a:Landroid/widget/TextView;

.field b:Landroid/widget/TextView;

.field c:Z

.field d:Z

.field final e:Lcom/twitter/library/amplify/AmplifyPlayer;

.field f:Landroid/widget/SeekBar;

.field final g:Landroid/os/Handler;

.field private final h:Lcom/twitter/library/card/element/j;

.field private i:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private final j:Landroid/view/View$OnClickListener;

.field private final k:Landroid/view/View$OnClickListener;

.field private final l:Z

.field private final m:Lcom/twitter/library/amplify/control/f;

.field private n:Landroid/view/View;

.field private o:Landroid/view/View;

.field private p:Landroid/view/View;

.field private q:Landroid/view/View;

.field private r:Lcom/twitter/library/amplify/control/h;

.field private s:Ljava/lang/StringBuilder;

.field private t:Ljava/util/Formatter;

.field private u:Landroid/widget/ImageButton;

.field private v:Landroid/widget/ImageButton;

.field private w:Landroid/widget/TextView;

.field private x:Ljava/lang/String;

.field private y:Z

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V
    .locals 8

    const/16 v7, 0x8

    const/4 v1, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v4, 0x1

    invoke-direct {p0, p1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Lcom/twitter/library/amplify/control/e;

    invoke-direct {v0, p0, v5}, Lcom/twitter/library/amplify/control/e;-><init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;Lcom/twitter/library/amplify/control/a;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/library/card/element/j;

    invoke-direct {v0}, Lcom/twitter/library/card/element/j;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h:Lcom/twitter/library/card/element/j;

    new-instance v0, Lcom/twitter/library/amplify/control/a;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/control/a;-><init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    new-instance v0, Lcom/twitter/library/amplify/control/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/control/b;-><init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->j:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/twitter/library/amplify/control/c;

    invoke-direct {v0, p0}, Lcom/twitter/library/amplify/control/c;-><init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k:Landroid/view/View$OnClickListener;

    iput-boolean v4, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->A:Z

    iput-boolean v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->B:Z

    invoke-virtual {p0, v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->setWillNotDraw(Z)V

    invoke-virtual {p0, v4}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->setFocusable(Z)V

    invoke-virtual {p0, v4}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->setFocusableInTouchMode(Z)V

    iput-boolean p3, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l:Z

    iput-object p2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s:Ljava/lang/StringBuilder;

    new-instance v0, Ljava/util/Formatter;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s:Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/Formatter;-><init>(Ljava/lang/Appendable;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->t:Ljava/util/Formatter;

    sget v0, Lil;->amplify_preroll_countdown:I

    invoke-virtual {p1, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->x:Ljava/lang/String;

    const-string/jumbo v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    sget v1, Lii;->amplify_media_controller:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->amplify_media_controller_controls:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->q:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1, p1}, Lcom/twitter/library/amplify/AmplifyPlayer;->f(Landroid/content/Context;)Lcom/twitter/library/amplify/control/h;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    new-instance v3, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    check-cast v2, Landroid/view/View;

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    invoke-interface {v1, p1, p2, p3}, Lcom/twitter/library/amplify/control/h;->a(Landroid/content/Context;Lcom/twitter/library/amplify/AmplifyPlayer;Z)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->time_current:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->time:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->countdown:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->w:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->mediacontroller_progress:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/SeekBar;

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    iget-object v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setMax(I)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->fullscreen:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->v:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    sget v2, Lig;->pause:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v1}, Landroid/widget/ImageButton;->requestFocus()Z

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    iget-object v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->j:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    sget v1, Lii;->amplify_playlist_complete_overlay:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p:Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    sget v1, Lii;->amplify_error_msg:I

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->v:Landroid/widget/ImageButton;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->addView(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->v:Landroid/widget/ImageButton;

    sget v1, Lif;->smallscreen:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_0
    new-instance v0, Lcom/twitter/library/amplify/control/f;

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->q:Landroid/view/View;

    sget v2, Lhz;->slide_up:I

    sget v3, Lhz;->slide_down:I

    invoke-direct {v0, v1, v4, v2, v3}, Lcom/twitter/library/amplify/control/f;-><init>(Landroid/view/View;ZII)V

    iput-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m:Lcom/twitter/library/amplify/control/f;

    return-void
.end method

.method private a(I)Ljava/lang/String;
    .locals 10

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    int-to-long v0, p1

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    long-to-int v0, v0

    rem-int/lit8 v1, v0, 0x3c

    div-int/lit8 v2, v0, 0x3c

    rem-int/lit8 v2, v2, 0x3c

    int-to-long v3, v0

    const-wide/32 v5, 0xea60

    div-long/2addr v3, v5

    const-wide/16 v5, 0x3c

    mul-long/2addr v3, v5

    long-to-int v0, v3

    iget-object v3, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s:Ljava/lang/StringBuilder;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->setLength(I)V

    if-lez v0, :cond_0

    iget-object v3, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->t:Ljava/util/Formatter;

    const-string/jumbo v4, "%d:%02d:%02d"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v7

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v9

    invoke-virtual {v3, v4, v5}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-lez v2, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->t:Ljava/util/Formatter;

    const-string/jumbo v3, "%02d:%02d"

    new-array v4, v9, [Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v4, v7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v8

    invoke-virtual {v0, v3, v4}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->t:Ljava/util/Formatter;

    const-string/jumbo v2, "0:%02d"

    new-array v3, v8, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v7

    invoke-virtual {v0, v2, v3}, Ljava/util/Formatter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/util/Formatter;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Formatter;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;I)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    const/16 v0, 0x8

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    return-object v0
.end method

.method private b(Landroid/content/Context;I)Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lcom/twitter/library/amplify/control/d;

    invoke-direct {v0, p0, p1, p2}, Lcom/twitter/library/amplify/control/d;-><init>(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;Landroid/content/Context;I)V

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r()Z

    move-result v0

    return v0
.end method

.method private getFullscreenButtonVisibility()I
    .locals 3

    const/4 v0, 0x4

    iget-boolean v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->A:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->y:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u()V

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->v:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getFullscreenButtonVisibility()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->t()V

    goto :goto_0
.end method

.method private p()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/amplify/model/AmplifyVideo;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v1

    iget v1, v1, Lcom/twitter/library/amplify/r;->a:I

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/model/AmplifyVideo;->e(D)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->y:Z

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o()V

    :cond_0
    return-void
.end method

.method private q()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    iput-boolean v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->B:Z

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    return-void
.end method

.method private r()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->q:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private t()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->w:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private u()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->w:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    return-void
.end method

.method private v()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h:Lcom/twitter/library/card/element/j;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/twitter/library/card/element/j;->a(Landroid/view/ViewGroup;Landroid/content/Context;)Z

    return-void
.end method

.method public a(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/amplify/control/h;->a(II)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->requestLayout()V

    return-void
.end method

.method public a(Landroid/content/Context;I)V
    .locals 3

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b(Landroid/content/Context;I)Ljava/lang/Runnable;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean p1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->y:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->z:Z

    iput-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->B:Z

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->v()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h:Lcom/twitter/library/card/element/j;

    invoke-virtual {v0, p0}, Lcom/twitter/library/card/element/j;->a(Landroid/view/ViewGroup;)Z

    return-void
.end method

.method public b(Z)V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->z:Z

    iput-boolean p1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->y:Z

    return-void
.end method

.method public c()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m()V

    return-void
.end method

.method public d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->z:Z

    return-void
.end method

.method public e()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b()V

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->u()Lcom/twitter/library/amplify/model/AmplifyVideo;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v1

    iget v1, v1, Lcom/twitter/library/amplify/r;->a:I

    int-to-double v1, v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/amplify/model/AmplifyVideo;->e(D)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->y:Z

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    goto :goto_0
.end method

.method public f()Z
    .locals 2

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->z:Z

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->i()V

    :goto_0
    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h()V

    goto :goto_0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m:Lcom/twitter/library/amplify/control/f;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/f;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    invoke-interface {v0}, Lcom/twitter/library/amplify/control/h;->e()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public i()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    invoke-virtual {v0}, Landroid/widget/ImageButton;->requestFocus()Z

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->l()I

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->s()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m:Lcom/twitter/library/amplify/control/f;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/control/f;->a()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    invoke-interface {v0}, Lcom/twitter/library/amplify/control/h;->f()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k()V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->g:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method j()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->q()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->B:Z

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->q()V

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->m()V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->k()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->p()V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->e(Z)V

    goto :goto_1
.end method

.method k()V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->B:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    sget v1, Lif;->ic_replay:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lil;->replay:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/AmplifyPlayer;->r()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    sget v1, Lif;->ic_media_pause:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lil;->pause:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    sget v1, Lif;->ic_media_play:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->u:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lil;->play:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method l()I
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->q()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->c:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->e:Lcom/twitter/library/amplify/AmplifyPlayer;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/AmplifyPlayer;->s()Lcom/twitter/library/amplify/r;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    if-eqz v2, :cond_2

    iget v2, v1, Lcom/twitter/library/amplify/r;->d:I

    if-lez v2, :cond_2

    iget v2, v1, Lcom/twitter/library/amplify/r;->b:I

    mul-int/lit16 v2, v2, 0x3e8

    iget v3, v1, Lcom/twitter/library/amplify/r;->d:I

    div-int/2addr v2, v3

    int-to-long v2, v2

    iget-object v4, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->f:Landroid/widget/SeekBar;

    long-to-int v2, v2

    invoke-virtual {v4, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    :cond_2
    iget-object v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a:Landroid/widget/TextView;

    iget v3, v1, Lcom/twitter/library/amplify/r;->d:I

    invoke-direct {p0, v3}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->b:Landroid/widget/TextView;

    iget v3, v1, Lcom/twitter/library/amplify/r;->b:I

    invoke-direct {p0, v3}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v2, v1, Lcom/twitter/library/amplify/r;->d:I

    iget v3, v1, Lcom/twitter/library/amplify/r;->b:I

    sub-int/2addr v2, v3

    invoke-direct {p0, v2}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(I)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->w:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->x:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v2, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget v0, v1, Lcom/twitter/library/amplify/r;->b:I

    goto :goto_0
.end method

.method m()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p:Landroid/view/View;

    invoke-direct {p0, v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Landroid/view/View;)V

    return-void
.end method

.method public n()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    invoke-interface {v0}, Lcom/twitter/library/amplify/control/h;->c()V

    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/RelativeLayout;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->B:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->y:Z

    invoke-virtual {p0, v0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->a(Z)V

    :cond_0
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 6

    const/4 v2, 0x0

    invoke-super/range {p0 .. p5}, Landroid/widget/RelativeLayout;->onLayout(ZIIII)V

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getRight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getLeft()I

    move-result v1

    sub-int v4, v0, v1

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getBottom()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->getTop()I

    move-result v1

    sub-int v5, v0, v1

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->p:Landroid/view/View;

    invoke-virtual {v0, v2, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->n:Landroid/view/View;

    invoke-virtual {v0, v2, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->r:Lcom/twitter/library/amplify/control/h;

    invoke-interface {v0, v2, v2, v4, v5}, Lcom/twitter/library/amplify/control/h;->layout(IIII)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->o:Landroid/view/View;

    invoke-virtual {v0, v2, v2, v4, v5}, Landroid/view/View;->layout(IIII)V

    iget-object v0, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->h:Lcom/twitter/library/card/element/j;

    move v1, p1

    move v3, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/card/element/j;->a(ZIIII)V

    return-void
.end method

.method public setIsFullScreenToggleAllowed(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->A:Z

    return-void
.end method
