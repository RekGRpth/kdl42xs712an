.class public abstract Lcom/twitter/library/telephony/TelephonyUtil;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static volatile a:Ljava/lang/String;

.field private static volatile b:I

.field private static volatile c:Z

.field private static volatile d:Z

.field private static volatile e:Z

.field private static volatile f:Lcom/twitter/library/telephony/d;

.field private static g:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/twitter/library/telephony/TelephonyUtil;->g:J

    return-void
.end method

.method public static a(I)I
    .locals 2

    const/high16 v1, 0x180000

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return p0

    :cond_0
    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_0
    const v0, 0x59999

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_1
    const v0, 0x8cccc

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x100000

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_3
    const v0, 0x133333

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_4
    invoke-static {p0, v1}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_5
    const/high16 v0, 0x200000

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    :pswitch_6
    const/high16 v0, 0x280000

    invoke-static {p0, v0}, Ljava/lang/Math;->min(II)I

    move-result p0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_6
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method static a(II)Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;
    .locals 3

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    const/4 v1, 0x1

    if-ne p0, v1, :cond_1

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->h:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-nez p0, :cond_0

    invoke-static {p1}, Lcom/twitter/library/telephony/TelephonyUtil;->b(I)F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-eqz v2, :cond_0

    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->b:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c()F

    move-result v2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_3

    const/4 v0, 0x2

    if-eq p1, v0, :cond_2

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->b:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0

    :cond_3
    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->d:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c()F

    move-result v2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_4

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->d:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0

    :cond_4
    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->e:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c()F

    move-result v2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_5

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->e:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0

    :cond_5
    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->f:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c()F

    move-result v2

    cmpg-float v2, v1, v2

    if-gtz v2, :cond_6

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->f:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0

    :cond_6
    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->g:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    invoke-virtual {v2}, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->c()F

    move-result v2

    cmpg-float v1, v1, v2

    if-gtz v1, :cond_0

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;->g:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/telephony/TelephonyUtil;->e:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(I)F
    .locals 4

    const v3, 0x449c4000    # 1250.0f

    const/high16 v2, 0x42fa0000    # 125.0f

    const/high16 v1, 0x42480000    # 50.0f

    const/high16 v0, 0x40c80000    # 6.25f

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const/high16 v0, 0x3fe00000    # 1.75f

    goto :goto_0

    :pswitch_2
    const/high16 v0, 0x40480000    # 3.125f

    goto :goto_0

    :pswitch_3
    const/high16 v0, 0x41480000    # 12.5f

    goto :goto_0

    :pswitch_4
    move v0, v1

    goto :goto_0

    :pswitch_5
    move v0, v1

    goto :goto_0

    :pswitch_6
    const/high16 v0, 0x42960000    # 75.0f

    goto :goto_0

    :pswitch_7
    const/high16 v0, 0x42af0000    # 87.5f

    goto :goto_0

    :pswitch_8
    const/high16 v0, 0x437a0000    # 250.0f

    goto :goto_0

    :pswitch_9
    move v0, v2

    goto :goto_0

    :pswitch_a
    move v0, v2

    goto :goto_0

    :pswitch_b
    const v0, 0x441c4000    # 625.0f

    goto :goto_0

    :pswitch_c
    move v0, v3

    goto :goto_0

    :pswitch_d
    move v0, v3

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_4
        :pswitch_1
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_8
        :pswitch_9
        :pswitch_7
        :pswitch_2
        :pswitch_b
        :pswitch_d
        :pswitch_a
        :pswitch_c
    .end packed-switch
.end method

.method public static b()I
    .locals 1

    sget v0, Lcom/twitter/library/telephony/TelephonyUtil;->b:I

    return v0
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "wifi"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/telephony/TelephonyUtil;->c:Z

    return v0
.end method

.method public static c(Landroid/content/Context;)Z
    .locals 1

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isConnectedOrConnecting()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d(Landroid/content/Context;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string/jumbo v0, "connectivity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    new-instance v0, Lcom/twitter/library/telephony/d;

    invoke-direct {v0}, Lcom/twitter/library/telephony/d;-><init>()V

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sput-wide v2, Lcom/twitter/library/telephony/TelephonyUtil;->g:J

    sput-boolean v4, Lcom/twitter/library/telephony/TelephonyUtil;->d:Z

    :goto_0
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    if-ne v0, v5, :cond_2

    const-string/jumbo v0, "wifi"

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    sput-boolean v5, Lcom/twitter/library/telephony/TelephonyUtil;->c:Z

    sput v4, Lcom/twitter/library/telephony/TelephonyUtil;->b:I

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/telephony/d;->c:Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    sget-object v1, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/telephony/d;->b:Ljava/lang/String;

    :goto_1
    return-void

    :cond_1
    sput-boolean v5, Lcom/twitter/library/telephony/TelephonyUtil;->d:Z

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v3

    invoke-static {v2, v3}, Lcom/twitter/library/telephony/TelephonyUtil;->a(II)Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    move-result-object v2

    iput-object v2, v0, Lcom/twitter/library/telephony/d;->a:Lcom/twitter/library/telephony/TelephonyUtil$DownloadQuality;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    sput-boolean v4, Lcom/twitter/library/telephony/TelephonyUtil;->c:Z

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v2

    sput v2, Lcom/twitter/library/telephony/TelephonyUtil;->b:I

    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperatorName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/twitter/library/telephony/d;->c:Ljava/lang/String;

    sget-object v2, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v0

    :goto_2
    iput-object v0, v2, Lcom/twitter/library/telephony/d;->b:Ljava/lang/String;

    sget v0, Lcom/twitter/library/telephony/TelephonyUtil;->b:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, "cellular"

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v0, ""

    goto :goto_2

    :pswitch_1
    const-string/jumbo v0, "2g"

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "unknown"

    sput-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->a:Ljava/lang/String;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static d()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/telephony/TelephonyUtil;->d:Z

    return v0
.end method

.method public static e()Lcom/twitter/library/telephony/d;
    .locals 1

    sget-object v0, Lcom/twitter/library/telephony/TelephonyUtil;->f:Lcom/twitter/library/telephony/d;

    return-object v0
.end method

.method public static f()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/telephony/TelephonyUtil;->e:Z

    return v0
.end method

.method public static g()Z
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    sget-wide v2, Lcom/twitter/library/telephony/TelephonyUtil;->g:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x3a98

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
