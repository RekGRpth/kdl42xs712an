.class Lcom/twitter/library/telephony/c;
.super Landroid/telephony/PhoneStateListener;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/telephony/a;


# direct methods
.method private constructor <init>(Lcom/twitter/library/telephony/a;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/telephony/c;->a:Lcom/twitter/library/telephony/a;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/telephony/a;Lcom/twitter/library/telephony/b;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/telephony/c;-><init>(Lcom/twitter/library/telephony/a;)V

    return-void
.end method


# virtual methods
.method public onSignalStrengthsChanged(Landroid/telephony/SignalStrength;)V
    .locals 2

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->isGsm()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/telephony/c;->a:Lcom/twitter/library/telephony/a;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getGsmSignalStrength()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/library/telephony/a;->a(Lcom/twitter/library/telephony/a;I)I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/telephony/c;->a:Lcom/twitter/library/telephony/a;

    invoke-virtual {p1}, Landroid/telephony/SignalStrength;->getCdmaDbm()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/library/telephony/a;->a(Lcom/twitter/library/telephony/a;I)I

    goto :goto_0
.end method
