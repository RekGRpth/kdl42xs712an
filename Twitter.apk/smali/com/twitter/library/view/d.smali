.class public Lcom/twitter/library/view/d;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;II)Ljava/lang/CharSequence;
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v6, v5

    move v7, v5

    invoke-static/range {v0 .. v7}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;IIIZZ)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;II)Ljava/lang/CharSequence;
    .locals 8

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    move v6, v4

    move v7, v4

    invoke-static/range {v0 .. v7}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;IIIZZ)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;IIIZZ)Ljava/lang/CharSequence;
    .locals 13

    if-eqz p1, :cond_3

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    move-object v2, p0

    move-object v4, p2

    move/from16 v5, p4

    move/from16 v6, p3

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v1 .. v9}, Lcom/twitter/library/view/d;->a(Landroid/text/Spannable;Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/view/c;IIIZZ)V

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    move-object v2, p0

    move-object v4, p2

    move/from16 v5, p4

    move/from16 v6, p3

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v1 .. v9}, Lcom/twitter/library/view/d;->a(Landroid/text/Spannable;Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/view/c;IIIZZ)V

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;

    move-object v2, p0

    move-object v4, p2

    move/from16 v5, p4

    move/from16 v6, p3

    move/from16 v7, p5

    move/from16 v8, p6

    move/from16 v9, p7

    invoke-static/range {v1 .. v9}, Lcom/twitter/library/view/d;->a(Landroid/text/Spannable;Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/view/c;IIIZZ)V

    iget-object v2, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x0

    new-instance v10, Landroid/util/SparseArray;

    invoke-direct {v10}, Landroid/util/SparseArray;-><init>()V

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    :cond_0
    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/library/api/UrlEntity;

    iget v3, v7, Lcom/twitter/library/api/UrlEntity;->start:I

    invoke-virtual {v10, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_0

    sget-object v4, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v10, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sub-int v12, v3, v2

    iget v3, v7, Lcom/twitter/library/api/UrlEntity;->end:I

    sub-int/2addr v3, v2

    if-ltz v12, :cond_0

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v4

    if-gt v3, v4, :cond_0

    iget-object v4, v7, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_5

    invoke-virtual {v1, v12, v3, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/2addr v4, v12

    sub-int v4, v3, v4

    sub-int/2addr v3, v4

    add-int v8, v2, v4

    move v9, v3

    :goto_1
    if-eqz p2, :cond_2

    new-instance v2, Lcom/twitter/library/view/e;

    if-nez p7, :cond_1

    const/4 v4, 0x1

    :goto_2
    move/from16 v3, p5

    move/from16 v5, p6

    move-object v6, p2

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/view/e;-><init>(IZZLcom/twitter/library/view/c;Lcom/twitter/library/api/UrlEntity;)V

    :goto_3
    const/16 v3, 0x21

    invoke-virtual {v1, v2, v12, v9, v3}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    move v2, v8

    goto :goto_0

    :cond_1
    const/4 v4, 0x0

    goto :goto_2

    :cond_2
    new-instance v2, Landroid/text/style/ForegroundColorSpan;

    move/from16 v0, p4

    invoke-direct {v2, v0}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    goto :goto_3

    :cond_3
    move-object v1, p0

    :cond_4
    return-object v1

    :cond_5
    move v9, v3

    move v8, v2

    goto :goto_1
.end method

.method private static a(Landroid/text/Spannable;Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/view/c;IIIZZ)V
    .locals 9

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Entity;

    iget v7, v0, Lcom/twitter/library/api/Entity;->start:I

    iget v8, v0, Lcom/twitter/library/api/Entity;->end:I

    invoke-virtual {p1, v7, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    if-nez p8, :cond_0

    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p5}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    add-int/lit8 v1, v7, 0x1

    const/16 v2, 0x21

    invoke-interface {p0, v0, v7, v1, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    :cond_0
    if-eqz p3, :cond_2

    new-instance v0, Lcom/twitter/library/view/f;

    if-nez p8, :cond_1

    const/4 v2, 0x1

    :goto_1
    move v1, p6

    move/from16 v3, p7

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/view/f;-><init>(IZZLcom/twitter/library/view/c;Ljava/lang/String;)V

    :goto_2
    add-int/lit8 v1, v7, 0x1

    const/16 v2, 0x21

    invoke-interface {p0, v0, v1, v8, v2}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/text/style/ForegroundColorSpan;

    invoke-direct {v0, p4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    goto :goto_2

    :cond_3
    return-void
.end method
