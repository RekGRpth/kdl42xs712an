.class final Lcom/twitter/library/view/m;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/twitter/library/view/l;


# direct methods
.method constructor <init>(Lcom/twitter/library/view/l;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/view/m;->a:Lcom/twitter/library/view/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    check-cast p1, Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/library/view/m;->a:Lcom/twitter/library/view/l;

    invoke-virtual {p1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/view/l;->a(Lcom/twitter/library/view/l;Landroid/text/Layout;)Landroid/text/Layout;

    iget-object v0, p0, Lcom/twitter/library/view/m;->a:Lcom/twitter/library/view/l;

    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingLeft()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollX()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/twitter/library/view/l;->a(Lcom/twitter/library/view/l;F)F

    iget-object v0, p0, Lcom/twitter/library/view/m;->a:Lcom/twitter/library/view/l;

    invoke-virtual {p1}, Landroid/widget/TextView;->getTotalPaddingTop()I

    move-result v1

    invoke-virtual {p1}, Landroid/widget/TextView;->getScrollY()I

    move-result v2

    add-int/2addr v1, v2

    int-to-float v1, v1

    invoke-static {v0, v1}, Lcom/twitter/library/view/l;->b(Lcom/twitter/library/view/l;F)F

    iget-object v0, p0, Lcom/twitter/library/view/m;->a:Lcom/twitter/library/view/l;

    invoke-virtual {v0, p2}, Lcom/twitter/library/view/l;->a(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method
