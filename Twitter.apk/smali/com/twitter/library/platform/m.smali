.class public Lcom/twitter/library/platform/m;
.super Lcom/twitter/library/platform/h;
.source "Twttr"


# static fields
.field private static c:Lcom/twitter/library/platform/m;


# instance fields
.field private d:Ljava/util/ArrayList;

.field private e:I

.field private f:Landroid/os/Handler;

.field private g:Landroid/location/Location;

.field private h:Lcom/twitter/library/platform/LocationProducer;

.field private i:Lcom/twitter/library/platform/h;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/platform/h;-><init>(Landroid/content/Context;Lcom/twitter/library/platform/i;)V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/platform/m;->e:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    new-instance v0, Lcom/twitter/library/platform/n;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/n;-><init>(Lcom/twitter/library/platform/m;)V

    iput-object v0, p0, Lcom/twitter/library/platform/m;->f:Landroid/os/Handler;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/platform/m;
    .locals 2

    const-class v1, Lcom/twitter/library/platform/m;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/platform/m;->c:Lcom/twitter/library/platform/m;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/platform/m;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/m;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/platform/m;->c:Lcom/twitter/library/platform/m;

    :cond_0
    sget-object v0, Lcom/twitter/library/platform/m;->c:Lcom/twitter/library/platform/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private h()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->j()V

    iget-object v0, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->i()Lcom/twitter/library/platform/h;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/m;->i:Lcom/twitter/library/platform/h;

    iget-object v0, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/h;)V

    return-void
.end method

.method private i()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    iget-object v1, p0, Lcom/twitter/library/platform/m;->i:Lcom/twitter/library/platform/h;

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/h;)V

    iput-object v2, p0, Lcom/twitter/library/platform/m;->i:Lcom/twitter/library/platform/h;

    iput-object v2, p0, Lcom/twitter/library/platform/m;->g:Landroid/location/Location;

    iget-object v0, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->j()V

    return-void
.end method


# virtual methods
.method public declared-synchronized a()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-direct {p0}, Lcom/twitter/library/platform/m;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/location/Location;)V
    .locals 2

    monitor-enter p0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/platform/m;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/location/Location;Landroid/location/Location;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Landroid/os/Message;)V
    .locals 4

    iget v0, p1, Landroid/os/Message;->what:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/library/platform/m;->e:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    rem-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/platform/m;->e:I

    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    iget v1, p0, Lcom/twitter/library/platform/m;->e:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    iget-object v1, p0, Lcom/twitter/library/platform/m;->h:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/location/Location;)V

    iput-object v0, p0, Lcom/twitter/library/platform/m;->g:Landroid/location/Location;

    iget-object v0, p0, Lcom/twitter/library/platform/m;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method public b()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/m;->g:Landroid/location/Location;

    return-object v0
.end method

.method public c()V
    .locals 4

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/platform/m;->e:I

    iget-object v0, p0, Lcom/twitter/library/platform/m;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    return-void
.end method

.method public declared-synchronized c(Landroid/location/Location;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/platform/m;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/m;->f:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method

.method public declared-synchronized e()Z
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized g()V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/m;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/platform/m;->h()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
