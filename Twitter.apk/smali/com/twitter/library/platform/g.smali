.class public final Lcom/twitter/library/platform/g;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/api/TwitterUser;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/twitter/library/network/OAuthToken;

.field private e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/api/TwitterUser;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/platform/g;->b:Lcom/twitter/library/api/TwitterUser;

    iput-object p3, p0, Lcom/twitter/library/platform/g;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/platform/g;->d:Lcom/twitter/library/network/OAuthToken;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/platform/g;->e:Z

    return-void
.end method

.method private b(Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)V
    .locals 20

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/platform/g;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v4, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    invoke-static {v1, v4, v5}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v1

    const/4 v2, 0x4

    const/4 v3, 0x0

    const/4 v6, 0x1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v4, v5, v2, v3}, Lcom/twitter/library/provider/az;->a(JII)J

    move-result-wide v7

    const/16 v11, 0x64

    :goto_0
    sget-boolean v2, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "TwitterDataSync"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Sync home timeline newer than sinceId: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v7, v8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v4

    const-wide/high16 v12, 0x7ff8000000000000L    # NaN

    const-wide/high16 v14, 0x7ff8000000000000L    # NaN

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v12

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v14

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/platform/g;->b:Lcom/twitter/library/api/TwitterUser;

    new-instance v6, Lcom/twitter/library/network/n;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/platform/g;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v6, v3}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    const-wide/16 v9, 0x0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/library/platform/g;->e:Z

    move/from16 v16, v0

    const/16 v17, 0x0

    const/16 v18, 0x0

    sget-object v19, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object v3, v1

    invoke-static/range {v2 .. v19}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/aa;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/network/a;JJIDDZZLjava/lang/String;Lcom/twitter/library/service/TimelineHelper$FilterType;)Lcom/twitter/library/service/r;

    move-result-object v5

    iget-object v2, v5, Lcom/twitter/library/service/r;->a:Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/platform/g;->b:Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/platform/g;->c:Ljava/lang/String;

    const-wide/16 v9, 0x0

    const/4 v12, 0x0

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/twitter/library/platform/g;->e:Z

    const/4 v14, 0x1

    sget-object v15, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object v3, v1

    invoke-static/range {v2 .. v15}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;Lcom/twitter/library/provider/az;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/service/r;Ljava/lang/String;JJIZZZLcom/twitter/library/service/TimelineHelper$FilterType;)V

    iget-object v2, v5, Lcom/twitter/library/service/r;->a:Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    iget v2, v2, Lcom/twitter/internal/network/k;->a:I

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_8

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/platform/g;->c:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;Ljava/lang/String;)V

    iget v2, v5, Lcom/twitter/library/service/r;->e:I

    iget v3, v5, Lcom/twitter/library/service/r;->d:I

    sget-boolean v4, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v4, :cond_2

    const-string/jumbo v4, "TwitterDataSync"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "====> Sync home timeline, got "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    if-lez v2, :cond_5

    new-instance v10, Lcom/twitter/library/platform/e;

    invoke-direct {v10}, Lcom/twitter/library/platform/e;-><init>()V

    iput v2, v10, Lcom/twitter/library/platform/e;->c:I

    move-object/from16 v0, p2

    iget-boolean v2, v0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    if-nez v2, :cond_4

    iput v3, v10, Lcom/twitter/library/platform/e;->b:I

    const/4 v2, 0x1

    if-ne v3, v2, :cond_3

    iget-object v2, v5, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/x;

    iget-object v3, v2, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v3, :cond_7

    iget-object v3, v2, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v10, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;

    move-result-object v3

    iget-object v2, v2, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v3, v2}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    :cond_3
    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/platform/g;->c:Ljava/lang/String;

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v10}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, v10, Lcom/twitter/library/platform/e;->g:J

    iget-object v8, v10, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    const/4 v9, 0x1

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)I

    move-result v1

    iput v1, v10, Lcom/twitter/library/platform/e;->n:I

    :cond_4
    move-object/from16 v0, p2

    iput-object v10, v0, Lcom/twitter/library/platform/DataSyncResult;->f:Lcom/twitter/library/platform/e;

    :cond_5
    :goto_2
    return-void

    :cond_6
    const-wide/16 v7, 0x0

    const/4 v11, 0x0

    goto/16 :goto_0

    :cond_7
    iget-object v3, v2, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz v3, :cond_3

    iget-object v2, v2, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    invoke-virtual {v2}, Lcom/twitter/library/api/Conversation;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;

    move-result-object v3

    iget-object v2, v2, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v3, v2}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    goto :goto_1

    :cond_8
    const/16 v1, 0x191

    if-ne v2, v1, :cond_9

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_2

    :cond_9
    if-nez v2, :cond_5

    move-object/from16 v0, p1

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_2
.end method


# virtual methods
.method public a()J
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/platform/g;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/platform/g;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v1, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/provider/az;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    return-wide v0
.end method

.method public a(Z)Lcom/twitter/library/platform/g;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/platform/g;->e:Z

    return-object p0
.end method

.method public a(Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)Z
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/platform/g;->b(Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)V

    invoke-virtual {p1}, Landroid/content/SyncResult;->hasError()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
