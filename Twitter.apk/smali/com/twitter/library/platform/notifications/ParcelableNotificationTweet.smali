.class public Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/provider/ParcelableTweet;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:J

.field private final c:J

.field private final d:J

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/platform/notifications/h;

    invoke-direct {v0}, Lcom/twitter/library/platform/notifications/h;-><init>()V

    sput-object v0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->a:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->b:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->c:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->d:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->e:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->f:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->g:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->h:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;JJJJLjava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->a:Ljava/lang/String;

    iput-wide p2, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->b:J

    iput-wide p4, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->c:J

    iput-wide p6, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->d:J

    iput-wide p8, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->e:J

    iput-object p10, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->f:Ljava/lang/String;

    iput-object p11, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->g:Ljava/lang/String;

    if-nez p12, :cond_0

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object p12

    :cond_0
    iput-object p12, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->h:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->h:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->reverse(Ljava/util/List;)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->a:Ljava/lang/String;

    return-object v0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->b:J

    return-wide v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->b:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->c:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->c:J

    iget-wide v2, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->d:J

    return-wide v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->e:J

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->f:Ljava/lang/String;

    return-object v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->g:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->i()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->h()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public l()Lcom/twitter/library/api/TweetClassicCard;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public m()Lcom/twitter/library/api/PromotedContent;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public n()Lcom/twitter/library/api/TwitterStatusCard;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public o()Ljava/util/ArrayList;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public p()Ljava/util/List;
    .locals 1

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public r()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public s()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public t()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public u()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public v()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public w()Ljava/util/ArrayList;
    .locals 1

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->a:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->b:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->c:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->d:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->e:J

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->h:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    return-void
.end method

.method public synthetic x()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/platform/notifications/ParcelableNotificationTweet;->w()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method
