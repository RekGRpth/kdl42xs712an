.class public final Lcom/twitter/library/platform/b;
.super Landroid/content/AbstractThreadedSyncAdapter;
.source "Twttr"


# static fields
.field static final a:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "TwitterDataSync"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/platform/b;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/content/AbstractThreadedSyncAdapter;-><init>(Landroid/content/Context;Z)V

    return-void
.end method

.method private static a(Landroid/content/Context;JLcom/twitter/library/network/OAuthToken;)J
    .locals 7

    const-wide/16 v2, 0x0

    const/4 v6, 0x1

    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v1, v4

    const-string/jumbo v4, "activity"

    aput-object v4, v1, v6

    const/4 v4, 0x2

    const-string/jumbo v5, "about_me"

    aput-object v5, v1, v4

    const/4 v4, 0x3

    const-string/jumbo v5, "unread"

    aput-object v5, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "cursor"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v1, 0x32

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v4, Lcom/twitter/library/network/d;

    invoke-direct {v4, p0, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, p1, p2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v4, Lcom/twitter/library/network/n;

    invoke-direct {v4, p3}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v4, v0, v4

    if-gtz v4, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    move-wide v0, v2

    goto :goto_0

    :cond_1
    move-wide v0, v2

    goto :goto_0
.end method

.method private a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V
    .locals 18

    sget-boolean v2, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "=====> Sync activity"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move-object/from16 v0, p5

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p2

    invoke-static {v0, v4, v5}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v2

    invoke-static/range {p2 .. p2}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v6

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/az;->d(I)J

    move-result-wide v7

    const/4 v3, 0x1

    move/from16 v0, p1

    if-ne v3, v0, :cond_6

    const-string/jumbo v3, "by_friends"

    :goto_0
    iget-object v9, v6, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, "1.1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string/jumbo v12, "activity"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    aput-object v3, v10, v11

    invoke-static {v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v9, ".json"

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    packed-switch p1, :pswitch_data_0

    :goto_1
    invoke-static/range {p2 .. p2}, Lcom/twitter/library/service/TimelineHelper;->b(Landroid/content/Context;)Z

    move-result v9

    if-eqz v9, :cond_1

    const-string/jumbo v9, "model_version"

    const/4 v10, 0x6

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_1
    const-string/jumbo v9, "include_entities"

    const/4 v10, 0x1

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v9, "include_user_entities"

    const/4 v10, 0x1

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v9, "include_media_features"

    const/4 v10, 0x1

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v9, "include_cards"

    const/4 v10, 0x1

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v6, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    const-wide/16 v9, 0x0

    cmp-long v6, v7, v9

    if-lez v6, :cond_2

    const-string/jumbo v6, "since_id"

    invoke-static {v3, v6, v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_2
    const-string/jumbo v6, "count"

    const/16 v7, 0x14

    invoke-static {v3, v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const/16 v6, 0x1b

    new-instance v7, Lcom/twitter/library/util/f;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, p2

    invoke-direct {v7, v0, v4, v5, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v6, v7}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v7, Lcom/twitter/library/network/d;

    move-object/from16 v0, p2

    invoke-direct {v7, v0, v3}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v7, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    new-instance v7, Lcom/twitter/library/network/n;

    move-object/from16 v0, p4

    invoke-direct {v7, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v3, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    const/4 v7, 0x1

    invoke-virtual {v3, v7}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v3

    iget v3, v3, Lcom/twitter/internal/network/k;->a:I

    const/16 v7, 0xc8

    if-ne v3, v7, :cond_10

    move-object/from16 v0, p3

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/twitter/library/platform/TwitterDataSyncService;->g(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    move/from16 v6, p1

    invoke-virtual/range {v2 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/ArrayList;JIZZZ)Ljava/util/ArrayList;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v12

    sget-boolean v3, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v3, :cond_3

    const-string/jumbo v3, "TwitterDataSync"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v7, "=====> Sync activity, got "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-static/range {p1 .. p1}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v3

    if-eqz v3, :cond_f

    if-eqz p8, :cond_f

    move-object/from16 v0, p2

    move-object/from16 v1, p4

    invoke-static {v0, v4, v5, v1}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;JLcom/twitter/library/network/OAuthToken;)J

    move-result-wide v3

    move-object/from16 v0, p3

    iget-object v5, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v0, v5, v3, v4}, Lcom/twitter/library/client/i;->b(Landroid/content/Context;Ljava/lang/String;J)Z

    move-object/from16 v0, p3

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-static {v0, v3}, Lcom/twitter/library/client/i;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v13

    const/4 v3, 0x1

    move/from16 v0, p1

    invoke-virtual {v2, v0, v13, v14, v3}, Lcom/twitter/library/provider/az;->a(IJZ)I

    new-instance v15, Lcom/twitter/library/platform/e;

    invoke-direct {v15}, Lcom/twitter/library/platform/e;-><init>()V

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x0

    invoke-virtual {v9}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    :cond_4
    :goto_2
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/ae;

    iget-wide v0, v3, Lcom/twitter/library/api/ae;->b:J

    move-wide/from16 v16, v0

    cmp-long v10, v16, v13

    if-lez v10, :cond_4

    move-object/from16 v0, p7

    iget-boolean v10, v0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    if-nez v10, :cond_7

    if-nez v4, :cond_7

    const/4 v10, 0x1

    :goto_3
    iget v0, v3, Lcom/twitter/library/api/ae;->a:I

    move/from16 v16, v0

    packed-switch v16, :pswitch_data_1

    :cond_5
    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    :goto_4
    move v9, v8

    move v8, v7

    move v7, v6

    move v6, v5

    move v5, v4

    move v4, v3

    goto :goto_2

    :cond_6
    const-string/jumbo v3, "about_me"

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v9, "filters"

    const-string/jumbo v10, "filtered"

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_1
    const-string/jumbo v9, "filters"

    const-string/jumbo v10, "following"

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_2
    const-string/jumbo v9, "filters"

    const-string/jumbo v10, "verified"

    invoke-static {v3, v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :cond_7
    const/4 v10, 0x0

    goto :goto_3

    :pswitch_3
    add-int/lit8 v9, v9, 0x1

    if-eqz v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v3, v3, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v15, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v4, 0x1

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto :goto_4

    :pswitch_4
    add-int/lit8 v9, v9, 0x1

    if-eqz v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v3, v3, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v15, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/library/api/TwitterStatus;->t:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v4, 0x1

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto :goto_4

    :pswitch_5
    add-int/lit8 v8, v8, 0x1

    if-eqz v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v4, v3, Lcom/twitter/library/api/ae;->k:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v15, v4}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v4, 0x1

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_4

    :pswitch_6
    add-int/lit8 v7, v7, 0x1

    if-eqz v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v3, v3, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v15, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v4, 0x1

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_4

    :pswitch_7
    add-int/lit8 v6, v6, 0x1

    if-eqz v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v10, v3, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-nez v10, :cond_5

    iget-object v4, v3, Lcom/twitter/library/api/ae;->o:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v4, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v15, v4}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/platform/e;

    move-result-object v4

    iget-object v3, v3, Lcom/twitter/library/api/ae;->g:Ljava/util/ArrayList;

    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v4, v3}, Lcom/twitter/library/platform/e;->a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/platform/e;

    const/4 v4, 0x1

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_4

    :pswitch_8
    add-int/lit8 v5, v5, 0x1

    move v3, v4

    move v4, v5

    move v5, v6

    move v6, v7

    move v7, v8

    move v8, v9

    goto/16 :goto_4

    :cond_8
    sget-boolean v3, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v3, :cond_9

    const-string/jumbo v3, "TwitterDataSync"

    const-string/jumbo v4, "=====> Sync about me activity, %d %d %d %d %d"

    const/4 v10, 0x5

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    const/4 v11, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    const/4 v11, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    const/4 v11, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    const/4 v11, 0x4

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v10, v11

    invoke-static {v4, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_9
    const/4 v3, 0x0

    if-lez v9, :cond_a

    const/4 v3, 0x1

    :cond_a
    if-lez v8, :cond_b

    or-int/lit8 v3, v3, 0x2

    :cond_b
    if-lez v7, :cond_c

    or-int/lit8 v3, v3, 0x4

    :cond_c
    if-lez v6, :cond_d

    or-int/lit8 v3, v3, 0x8

    :cond_d
    if-lez v5, :cond_12

    or-int/lit8 v3, v3, 0x10

    move v11, v3

    :goto_5
    invoke-static/range {p2 .. p2}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v3

    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, v4, v11}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    iput v12, v15, Lcom/twitter/library/platform/e;->c:I

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/az;->f(I)I

    move-result v4

    iput v4, v15, Lcom/twitter/library/platform/e;->b:I

    move-object/from16 v0, p3

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string/jumbo v5, "unread_interactions"

    iget v6, v15, Lcom/twitter/library/platform/e;->b:I

    invoke-virtual {v3, v4, v5, v6}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    if-lez v12, :cond_f

    move-object/from16 v0, p7

    iget-boolean v3, v0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    if-nez v3, :cond_e

    move-object/from16 v0, p3

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-virtual {v15}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v6

    iget-wide v7, v15, Lcom/twitter/library/platform/e;->g:J

    iget-object v9, v15, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    const/4 v10, 0x1

    invoke-virtual/range {v2 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)I

    move-result v2

    iput v2, v15, Lcom/twitter/library/platform/e;->n:I

    :cond_e
    move-object/from16 v0, p7

    iput v11, v0, Lcom/twitter/library/platform/DataSyncResult;->e:I

    move-object/from16 v0, p7

    iput-object v15, v0, Lcom/twitter/library/platform/DataSyncResult;->i:Lcom/twitter/library/platform/e;

    :cond_f
    :goto_6
    return-void

    :cond_10
    if-nez v3, :cond_11

    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_6

    :cond_11
    const/16 v2, 0x191

    if-ne v3, v2, :cond_f

    move-object/from16 v0, p6

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_6

    :cond_12
    move v11, v3

    goto :goto_5

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_7
        :pswitch_6
        :pswitch_8
    .end packed-switch
.end method

.method static a(Landroid/content/Context;)V
    .locals 14

    const v3, 0x7fffffff

    const/4 v1, 0x0

    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/twitter/library/platform/TwitterDataSyncService;

    invoke-direct {v0, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "on_poll_alarm_ev"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v6

    const/high16 v0, 0x20000000

    invoke-static {p0, v1, v6, v0}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v8

    const-string/jumbo v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-static {p0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v5

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v9

    const-string/jumbo v4, "alarm_interval"

    const/4 v7, -0x1

    invoke-interface {v9, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v10

    sget-object v4, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v11

    array-length v12, v11

    move v2, v1

    move v7, v3

    :goto_0
    if-ge v2, v12, :cond_0

    aget-object v4, v11, v2

    iget-object v13, v4, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p0, v4}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v4

    invoke-virtual {v5, v13, v4}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Z)I

    move-result v4

    invoke-static {v7, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    add-int/lit8 v2, v2, 0x1

    move v7, v4

    goto :goto_0

    :cond_0
    if-ge v7, v3, :cond_4

    if-ne v7, v10, :cond_1

    if-nez v8, :cond_3

    :cond_1
    int-to-long v2, v7

    const-wide/32 v4, 0xea60

    mul-long/2addr v4, v2

    if-eqz v8, :cond_2

    invoke-virtual {v0, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, v4

    invoke-static {p0, v1, v6, v1}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setInexactRepeating(IJJLandroid/app/PendingIntent;)V

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "alarm_interval"

    invoke-interface {v0, v1, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_3
    :goto_1
    return-void

    :cond_4
    if-eqz v8, :cond_3

    invoke-virtual {v0, v8}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    invoke-interface {v9}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "alarm_interval"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_1
.end method

.method private a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;ILandroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)V
    .locals 11

    sget-boolean v1, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "TwitterDataSync"

    const-string/jumbo v2, "=====> Sync direct messages"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-wide v5, p4, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {p1, v5, v6}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-static {p1}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v7

    const/4 v2, 0x1

    move/from16 v0, p5

    if-ne v0, v2, :cond_6

    invoke-virtual {v1}, Lcom/twitter/library/provider/az;->f()J

    move-result-wide v2

    move-wide v3, v2

    :goto_0
    const/4 v2, 0x1

    move/from16 v0, p5

    if-ne v0, v2, :cond_7

    iget-object v2, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "direct_messages"

    aput-object v9, v7, v8

    invoke-static {v2, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    :goto_1
    const-string/jumbo v7, ".json"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-wide/16 v7, 0x0

    cmp-long v7, v3, v7

    if-lez v7, :cond_1

    const-string/jumbo v7, "since_id"

    invoke-static {v2, v7, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_1
    const-string/jumbo v3, "count"

    const/16 v4, 0xc8

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v3, "include_entities"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v3, "include_media_features"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v3, "include_user_entities"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v3, 0x20

    new-instance v4, Lcom/twitter/library/util/f;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, p1, v5, v6, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v3, v4}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/network/d;

    invoke-direct {v4, p1, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v5, v6}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    new-instance v4, Lcom/twitter/library/network/n;

    invoke-direct {v4, p3}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    iget v2, v2, Lcom/twitter/internal/network/k;->a:I

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_8

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    move/from16 v0, p5

    invoke-virtual {v1, v2, v5, v6, v0}, Lcom/twitter/library/provider/az;->a(Ljava/util/List;JI)I

    move-result v3

    sget-boolean v4, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v4, :cond_2

    const-string/jumbo v4, "TwitterDataSync"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "====> Sync direct messages, got "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    const/4 v4, 0x1

    move/from16 v0, p5

    if-ne v0, v4, :cond_5

    invoke-virtual {v1}, Lcom/twitter/library/provider/az;->g()I

    move-result v4

    invoke-static {p1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v5

    iget-object v6, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string/jumbo v7, "message"

    invoke-virtual {v5, v6, v7, v4}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    if-lez v3, :cond_5

    new-instance v10, Lcom/twitter/library/platform/e;

    invoke-direct {v10}, Lcom/twitter/library/platform/e;-><init>()V

    iput v3, v10, Lcom/twitter/library/platform/e;->c:I

    move-object/from16 v0, p7

    iget-boolean v3, v0, Lcom/twitter/library/platform/DataSyncResult;->c:Z

    if-nez v3, :cond_4

    iput v4, v10, Lcom/twitter/library/platform/e;->b:I

    const/4 v3, 0x1

    if-ne v4, v3, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/ak;

    iget-object v3, v2, Lcom/twitter/library/api/ak;->b:Ljava/lang/String;

    iput-object v3, v10, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    iget-object v3, v2, Lcom/twitter/library/api/ak;->d:Lcom/twitter/library/api/TwitterUser;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/twitter/library/api/ak;->d:Lcom/twitter/library/api/TwitterUser;

    iget-wide v3, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-wide v3, v10, Lcom/twitter/library/platform/e;->g:J

    iget-object v2, v2, Lcom/twitter/library/api/ak;->d:Lcom/twitter/library/api/TwitterUser;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v2, v10, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    :cond_3
    iget-object v2, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    const/4 v3, 0x7

    const/4 v4, 0x0

    invoke-virtual {v10}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v5

    iget-wide v6, v10, Lcom/twitter/library/platform/e;->g:J

    iget-object v8, v10, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    const/4 v9, 0x1

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;JLjava/lang/String;Z)I

    move-result v1

    iput v1, v10, Lcom/twitter/library/platform/e;->n:I

    :cond_4
    move-object/from16 v0, p7

    iput-object v10, v0, Lcom/twitter/library/platform/DataSyncResult;->g:Lcom/twitter/library/platform/e;

    :cond_5
    :goto_2
    return-void

    :cond_6
    move/from16 v0, p5

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/az;->c(I)J

    move-result-wide v2

    move-wide v3, v2

    goto/16 :goto_0

    :cond_7
    iget-object v2, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "direct_messages"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v9, "sent"

    aput-object v9, v7, v8

    invoke-static {v2, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/16 :goto_1

    :cond_8
    if-nez v2, :cond_9

    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_2

    :cond_9
    const/16 v1, 0x191

    if-ne v2, v1, :cond_5

    move-object/from16 v0, p6

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_2
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;)V
    .locals 6

    iget-wide v2, p3, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {p1}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    const/4 v5, 0x1

    move-object v0, p1

    move-object v4, p2

    invoke-static/range {v0 .. v5}, Liy;->a(Landroid/content/Context;Lcom/twitter/library/network/aa;JLcom/twitter/library/network/OAuthToken;Z)V

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;)V
    .locals 13

    move-object/from16 v0, p3

    iget-wide v3, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {p1}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "discover"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "universal"

    aput-object v7, v5, v6

    invoke-static {v1, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string/jumbo v1, ".json"

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {p1, v3, v4}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v1

    const-string/jumbo v5, "include_media_features"

    const/4 v6, 0x1

    invoke-static {v2, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v5, "include_user_entities"

    const/4 v6, 0x1

    invoke-static {v2, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/4 v5, 0x5

    const/16 v6, 0x13

    invoke-virtual {v1, v5, v6, v3, v4}, Lcom/twitter/library/provider/az;->a(IIJ)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    const-string/jumbo v6, "prev_cursor"

    invoke-static {v2, v6, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/16 v5, 0x1c

    new-instance v6, Lcom/twitter/library/util/f;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, p1, v3, v4, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v5, v6}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v5

    new-instance v6, Lcom/twitter/library/network/d;

    invoke-direct {v6, p1, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    new-instance v6, Lcom/twitter/library/network/n;

    invoke-direct {v6, p2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    iget v2, v2, Lcom/twitter/internal/network/k;->a:I

    const/16 v6, 0xc8

    if-ne v2, v6, :cond_3

    invoke-virtual {v5}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/search/d;

    if-eqz v2, :cond_1

    iget-object v5, v2, Lcom/twitter/library/api/search/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x0

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/search/d;JZZZJJZ)I

    goto :goto_0

    :cond_3
    if-nez v2, :cond_4

    move-object/from16 v0, p4

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numParseExceptions:J

    goto :goto_0

    :cond_4
    const/16 v1, 0x191

    if-ne v2, v1, :cond_1

    move-object/from16 v0, p4

    iget-object v1, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/network/a;JII)V
    .locals 8

    const/4 v7, 0x1

    const/4 v1, 0x0

    invoke-static {p0, p2, p3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v2

    invoke-virtual {v2, p4, p5}, Lcom/twitter/library/provider/az;->a(II)[J

    move-result-object v3

    if-nez v3, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const-string/jumbo v5, "1.1"

    aput-object v5, v4, v1

    const-string/jumbo v5, "users"

    aput-object v5, v4, v7

    const/4 v5, 0x2

    const-string/jumbo v6, "lookup"

    aput-object v6, v4, v5

    invoke-static {v0, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ".json"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, "user_id"

    array-length v5, v3

    invoke-static {v4, v0, v3, v1, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    const-string/jumbo v0, "include_user_entities"

    invoke-static {v4, v0, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v0, Lcom/twitter/library/util/f;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, p0, p2, p3, v5}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    const/16 v5, 0x16

    invoke-static {v5, v0}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v5, Lcom/twitter/library/network/d;

    invoke-direct {v5, p0, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v5, p2, p3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, p1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    array-length v5, v3

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_0

    aget-wide v6, v3, v0

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v2, p2, p3, v6, v7}, Lcom/twitter/library/provider/az;->b(JJ)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method


# virtual methods
.method public a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V
    .locals 21

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/platform/b;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    const-string/jumbo v4, "account_user_info"

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v4}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_2

    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "ACCOUNT_USER_INFO_KEY content not found."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_0
    const-string/jumbo v5, "com.twitter.android.oauth.token"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v5, v6}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v5, "com.twitter.android.oauth.token.secret"

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v2, v0, v5, v6}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    if-eqz v7, :cond_3

    if-nez v2, :cond_4

    :cond_3
    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "Token not found."

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    :catch_0
    move-exception v2

    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numAuthExceptions:J

    goto :goto_0

    :cond_4
    :try_start_1
    invoke-static {v4}, Lcom/twitter/library/api/ap;->b(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v6

    new-instance v5, Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v5, v7, v2}, Lcom/twitter/library/network/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v19, Lcom/twitter/library/client/f;

    iget-object v2, v6, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    move-object/from16 v0, v19

    invoke-direct {v0, v3, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v3}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v20

    new-instance v9, Lcom/twitter/library/platform/DataSyncResult;

    iget-object v2, v6, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-wide v7, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    move/from16 v0, v20

    invoke-direct {v9, v2, v7, v8, v0}, Lcom/twitter/library/platform/DataSyncResult;-><init>(Ljava/lang/String;JZ)V

    const-string/jumbo v2, "home"

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lcom/twitter/library/platform/g;

    move-object/from16 v0, p1

    iget-object v4, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v2, v3, v6, v4, v5}, Lcom/twitter/library/platform/g;-><init>(Landroid/content/Context;Lcom/twitter/library/api/TwitterUser;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;)V

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Lcom/twitter/library/platform/g;->a(Z)Lcom/twitter/library/platform/g;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0, v9}, Lcom/twitter/library/platform/g;->a(Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)Z

    :cond_5
    const-string/jumbo v2, "messages"

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_6

    const/4 v7, 0x1

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;ILandroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)V

    const/4 v7, 0x0

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;ILandroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;)V

    :cond_6
    const-string/jumbo v2, "activity"

    const/4 v4, 0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "notifications_follow_only"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v2

    const/4 v11, 0x1

    const/16 v18, 0x1

    move-object/from16 v10, p0

    move-object v12, v3

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, p3

    move-object/from16 v17, v9

    invoke-direct/range {v10 .. v18}, Lcom/twitter/library/platform/b;->a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V

    const/4 v11, 0x0

    const/16 v18, 0x1

    move-object/from16 v10, p0

    move-object v12, v3

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, p3

    move-object/from16 v17, v9

    invoke-direct/range {v10 .. v18}, Lcom/twitter/library/platform/b;->a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V

    if-eqz v6, :cond_e

    iget-boolean v4, v6, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v4, :cond_e

    const/4 v11, 0x2

    const/16 v18, 0x0

    move-object/from16 v10, p0

    move-object v12, v3

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, p3

    move-object/from16 v17, v9

    invoke-direct/range {v10 .. v18}, Lcom/twitter/library/platform/b;->a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V

    const/4 v11, 0x3

    const/16 v18, 0x0

    move-object/from16 v10, p0

    move-object v12, v3

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, p3

    move-object/from16 v17, v9

    invoke-direct/range {v10 .. v18}, Lcom/twitter/library/platform/b;->a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V

    const/4 v11, 0x4

    const/16 v18, 0x0

    move-object/from16 v10, p0

    move-object v12, v3

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, p3

    move-object/from16 v17, v9

    invoke-direct/range {v10 .. v18}, Lcom/twitter/library/platform/b;->a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V

    :cond_7
    :goto_1
    invoke-virtual/range {p3 .. p3}, Landroid/content/SyncResult;->hasError()Z

    move-result v2

    if-nez v2, :cond_8

    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v6}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;)V

    :cond_8
    const-string/jumbo v2, "discover"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v0, v3, v5, v6, v1}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;)V

    :cond_9
    const-string/jumbo v2, "fs_config"

    const/4 v4, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->a()Lcom/twitter/library/featureswitch/b;

    move-result-object v2

    iget-wide v7, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v2, v7, v8}, Lcom/twitter/library/featureswitch/b;->d(J)V

    :cond_a
    invoke-static {v3}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v2

    invoke-virtual {v9}, Lcom/twitter/library/platform/DataSyncResult;->a()Z

    move-result v4

    if-eqz v4, :cond_b

    new-instance v4, Landroid/content/Intent;

    sget-object v7, Lcom/twitter/library/platform/TwitterDataSyncService;->a:Ljava/lang/String;

    invoke-direct {v4, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v7, v6, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v7}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;)I

    move-result v2

    iput v2, v9, Lcom/twitter/library/platform/DataSyncResult;->d:I

    const-string/jumbo v7, "show_notif"

    if-nez v20, :cond_f

    const-string/jumbo v2, "show_notif"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_f

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {v4, v7, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v7, "data"

    invoke-virtual {v2, v7, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    sget-object v2, Lcom/twitter/library/provider/w;->a:Ljava/lang/String;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->sendOrderedBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_b
    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v4, "antispam_query_frequency"

    const/4 v7, 0x0

    invoke-interface {v2, v4, v7}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v4, "antispam_last_poll_timestamp"

    const-wide/16 v7, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v7, v8}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v7

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v13

    int-to-long v9, v2

    const-wide/16 v11, 0x3e8

    mul-long/2addr v9, v11

    add-long/2addr v7, v9

    cmp-long v2, v13, v7

    if-lez v2, :cond_c

    const-string/jumbo v2, "antispam_connect_tweet_count"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v11

    const-string/jumbo v2, "antispam_connect_user_count"

    const/4 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v12

    new-instance v8, Lcom/twitter/library/network/n;

    invoke-direct {v8, v5}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iget-wide v9, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object v7, v3

    invoke-static/range {v7 .. v12}, Lcom/twitter/library/platform/b;->a(Landroid/content/Context;Lcom/twitter/library/network/a;JII)V

    invoke-virtual/range {v19 .. v19}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v2

    const-string/jumbo v4, "antispam_last_poll_timestamp"

    invoke-virtual {v2, v4, v13, v14}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    :cond_c
    const-string/jumbo v2, "auto_clean"

    const-wide/16 v4, 0x0

    move-object/from16 v0, v19

    invoke-virtual {v0, v2, v4, v5}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v4

    const-wide/32 v7, 0x36ee80

    add-long/2addr v4, v7

    cmp-long v2, v4, v13

    if-gez v2, :cond_d

    iget-wide v4, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4, v5}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v2

    iget-wide v3, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/provider/az;->i(J)V

    invoke-virtual/range {v19 .. v19}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v2

    const-string/jumbo v3, "auto_clean"

    invoke-virtual {v2, v3, v13, v14}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    :cond_d
    invoke-virtual/range {v19 .. v19}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v2

    const-string/jumbo v3, "last_sync"

    invoke-virtual {v2, v3, v13, v14}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/f;->d()V
    :try_end_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    :catch_1
    move-exception v2

    sget-boolean v2, Lcom/twitter/library/platform/b;->a:Z

    if-eqz v2, :cond_0

    const-string/jumbo v2, "TwitterDataSync"

    const-string/jumbo v3, "Sync canceled."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_e
    if-eqz v2, :cond_7

    const/4 v11, 0x3

    const/16 v18, 0x1

    move-object/from16 v10, p0

    move-object v12, v3

    move-object/from16 v13, p1

    move-object v14, v5

    move-object v15, v6

    move-object/from16 v16, p3

    move-object/from16 v17, v9

    :try_start_2
    invoke-direct/range {v10 .. v18}, Lcom/twitter/library/platform/b;->a(ILandroid/content/Context;Landroid/accounts/Account;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Landroid/content/SyncResult;Lcom/twitter/library/platform/DataSyncResult;Z)V
    :try_end_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_1

    :catch_2
    move-exception v2

    move-object/from16 v0, p3

    iget-object v2, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v5, 0x1

    add-long/2addr v3, v5

    iput-wide v3, v2, Landroid/content/SyncStats;->numIoExceptions:J

    goto/16 :goto_0

    :cond_f
    const/4 v2, 0x0

    goto/16 :goto_2
.end method

.method a(Landroid/accounts/Account;)Z
    .locals 9

    const-wide/32 v7, 0xea60

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/content/ContentResolver;->getSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {p1, v1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v1

    if-lez v1, :cond_0

    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/library/platform/b;->getContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-string/jumbo v4, "last_sync"

    const-wide/16 v5, 0x0

    invoke-virtual {v1, v4, v5, v6}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v4

    sub-long v1, v2, v4

    invoke-virtual {p0}, Lcom/twitter/library/platform/b;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v3

    iget-object v4, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/platform/b;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5, p1}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Z)I

    move-result v3

    int-to-long v3, v3

    mul-long/2addr v3, v7

    sub-long/2addr v3, v7

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 0

    invoke-virtual {p0, p1, p2, p5}, Lcom/twitter/library/platform/b;->a(Landroid/accounts/Account;Landroid/os/Bundle;Landroid/content/SyncResult;)V

    return-void
.end method
