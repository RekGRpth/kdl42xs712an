.class public Lcom/twitter/library/platform/LocationProducer;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/platform/i;


# static fields
.field private static a:Lcom/twitter/library/platform/LocationProducer;


# instance fields
.field private b:Landroid/content/Context;

.field private c:Ljava/util/HashSet;

.field private d:Landroid/os/Handler;

.field private volatile e:Landroid/location/Location;

.field private f:Landroid/location/LocationManager;

.field private g:Lcom/twitter/library/platform/h;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:J

.field private o:J

.field private p:J

.field private q:J

.field private r:J


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 5

    const-wide/16 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->b:Landroid/content/Context;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    new-instance v0, Lcom/twitter/library/platform/l;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/platform/l;-><init>(Lcom/twitter/library/platform/LocationProducer;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const-string/jumbo v0, "location"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    iput-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->f:Landroid/location/LocationManager;

    iput-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->h:Z

    iput-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->i:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->j:Z

    iput-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    iput-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->l:Z

    iput-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->m:Z

    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->n:J

    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    iput-wide v3, p0, Lcom/twitter/library/platform/LocationProducer;->p:J

    iput-wide v3, p0, Lcom/twitter/library/platform/LocationProducer;->q:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->r:J

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->j:Z

    invoke-direct {p0, v0}, Lcom/twitter/library/platform/LocationProducer;->b(Z)V

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;
    .locals 2

    const-class v1, Lcom/twitter/library/platform/LocationProducer;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/platform/LocationProducer;->a:Lcom/twitter/library/platform/LocationProducer;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/platform/LocationProducer;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/LocationProducer;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/platform/LocationProducer;->a:Lcom/twitter/library/platform/LocationProducer;

    :cond_0
    sget-object v0, Lcom/twitter/library/platform/LocationProducer;->a:Lcom/twitter/library/platform/LocationProducer;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/platform/LocationProducer;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/platform/LocationProducer;->b(Z)V

    return-void
.end method

.method private a(ZII)V
    .locals 7

    const/4 v6, -0x1

    const-wide/16 v4, 0x3e8

    if-eq p2, v6, :cond_0

    int-to-long v0, p2

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->n:J

    int-to-long v0, p2

    mul-long/2addr v0, v4

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const-wide/16 v0, 0x2710

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->n:J

    :cond_0
    if-eq p3, v6, :cond_1

    int-to-long v0, p3

    mul-long/2addr v0, v4

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    int-to-long v0, p3

    mul-long/2addr v0, v4

    const-wide/16 v2, 0x7530

    cmp-long v0, v0, v2

    if-gez v0, :cond_1

    const-wide/32 v0, 0x493e0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->i:Z

    if-eq v0, p1, :cond_2

    iput-boolean p1, p0, Lcom/twitter/library/platform/LocationProducer;->i:Z

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->m()V

    :cond_2
    return-void
.end method

.method private b(Landroid/location/Location;)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->e:Landroid/location/Location;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/location/Location;Landroid/location/Location;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/twitter/library/platform/LocationProducer;->e:Landroid/location/Location;

    :cond_0
    return-void
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->j:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/library/platform/LocationProducer;->j:Z

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/platform/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/library/platform/j;

    invoke-direct {v0, p0}, Lcom/twitter/library/platform/j;-><init>(Lcom/twitter/library/platform/LocationProducer;)V

    iget-object v1, p0, Lcom/twitter/library/platform/LocationProducer;->b:Landroid/content/Context;

    invoke-static {v1, p0, v0}, Lcom/twitter/library/platform/f;->a(Landroid/content/Context;Lcom/twitter/library/platform/i;Lcom/google/android/gms/common/d;)Lcom/twitter/library/platform/f;

    move-result-object v0

    :goto_1
    invoke-virtual {p0, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/h;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->b:Landroid/content/Context;

    invoke-static {v0, p0}, Lcom/twitter/library/platform/o;->a(Landroid/content/Context;Lcom/twitter/library/platform/i;)Lcom/twitter/library/platform/o;

    move-result-object v0

    goto :goto_1
.end method

.method private declared-synchronized k()V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->h()V

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->l()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private l()J
    .locals 8

    const-wide/16 v0, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/platform/LocationProducer;->q:J

    sub-long v4, v2, v4

    iget-wide v6, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    cmp-long v6, v4, v6

    if-ltz v6, :cond_0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->r:J

    iget-wide v6, p0, Lcom/twitter/library/platform/LocationProducer;->p:J

    cmp-long v0, v0, v6

    if-ltz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->r:J

    iget-wide v6, p0, Lcom/twitter/library/platform/LocationProducer;->q:J

    cmp-long v0, v0, v6

    if-gtz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    sub-long/2addr v0, v4

    goto :goto_0

    :cond_1
    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->r:J

    sub-long v0, v2, v0

    iget-wide v2, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    iget-wide v4, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    rem-long/2addr v0, v4

    sub-long v0, v2, v0

    goto :goto_0
.end method

.method private m()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->h:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->i:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    if-eq v0, v1, :cond_0

    iput-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->n()V

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->j()V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->g()V

    goto :goto_1
.end method

.method private n()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->k()V

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;)I
    .locals 6

    const-wide/16 v4, 0x3e8

    const/4 v0, 0x1

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/library/platform/k;->a:[I

    invoke-virtual {p1}, Lcom/twitter/library/platform/LocationProducer$CONFIG_TYPE;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :pswitch_0
    iget-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->i:Z

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :pswitch_1
    iget-boolean v2, p0, Lcom/twitter/library/platform/LocationProducer;->j:Z

    if-eqz v2, :cond_1

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    move v1, v0

    goto :goto_1

    :pswitch_2
    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->n:J

    div-long/2addr v0, v4

    long-to-int v0, v0

    goto :goto_0

    :pswitch_3
    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->o:J

    div-long/2addr v0, v4

    long-to-int v0, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    invoke-virtual {v0}, Lcom/twitter/library/platform/h;->b()Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/library/platform/LocationProducer;->b(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->e:Landroid/location/Location;

    return-object v0
.end method

.method protected declared-synchronized a(JLjava/lang/Object;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->l:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->p:J

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    invoke-virtual {v0}, Lcom/twitter/library/platform/h;->c()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->l:Z

    :cond_0
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_1

    if-nez p3, :cond_2

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1, p2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    :cond_2
    :try_start_1
    new-instance v0, Landroid/os/Message;

    invoke-direct {v0}, Landroid/os/Message;-><init>()V

    const/4 v1, 0x1

    iput v1, v0, Landroid/os/Message;->what:I

    iput-object p3, v0, Landroid/os/Message;->obj:Ljava/lang/Object;

    iget-object v1, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    invoke-virtual {v1, v0, p1, p2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Landroid/location/Location;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/twitter/library/platform/LocationProducer;->b(Landroid/location/Location;)V

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->e:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->r:J

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    new-array v1, v1, [Lcom/twitter/library/platform/i;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/platform/i;

    array-length v2, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    if-eqz v3, :cond_0

    iget-object v4, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    invoke-virtual {v4, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v3, p1}, Lcom/twitter/library/platform/i;->a(Landroid/location/Location;)V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    const/high16 v0, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/location/Location;F)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_2
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected a(Landroid/os/Message;)V
    .locals 3

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->n:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/platform/LocationProducer;->a(JLjava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/twitter/library/platform/i;

    invoke-virtual {p0, v0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->n()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Lcom/twitter/library/platform/h;)V
    .locals 1

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->n()V

    :cond_2
    iput-object p1, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->m:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->g()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/platform/i;)V
    .locals 2

    const-wide/16 v0, 0x2710

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;J)V

    return-void
.end method

.method public declared-synchronized a(Lcom/twitter/library/platform/i;J)V
    .locals 3

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->h:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {p1, v0}, Lcom/twitter/library/platform/i;->a(Landroid/location/Location;)V

    :cond_2
    const-wide/16 v1, 0x7530

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/location/Location;J)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    invoke-virtual {p0, p2, p3, p1}, Lcom/twitter/library/platform/LocationProducer;->a(JLjava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->h:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/platform/LocationProducer;->h:Z

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->m()V

    :cond_0
    return-void
.end method

.method public a(ZIII)V
    .locals 1

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/library/platform/LocationProducer;->b(Z)V

    :cond_0
    invoke-direct {p0, p1, p3, p4}, Lcom/twitter/library/platform/LocationProducer;->a(ZII)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;F)Z
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    cmpg-float v0, v0, p2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;J)Z
    .locals 5

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    sub-long/2addr v1, v3

    const-wide/16 v3, 0x0

    cmp-long v3, v1, v3

    if-ltz v3, :cond_0

    cmp-long v1, v1, p2

    if-gtz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected a(Landroid/location/Location;Landroid/location/Location;)Z
    .locals 8

    const/4 v1, 0x0

    const/4 v0, 0x1

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    move v0, v1

    :cond_1
    :goto_0
    return v0

    :cond_2
    if-eqz p2, :cond_1

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    invoke-virtual {p2}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    sub-long v4, v2, v4

    const-wide/16 v2, 0x7530

    cmp-long v2, v4, v2

    if-lez v2, :cond_3

    move v3, v0

    :goto_1
    const-wide/16 v6, -0x7530

    cmp-long v2, v4, v6

    if-gez v2, :cond_4

    move v2, v0

    :goto_2
    if-nez v3, :cond_1

    if-eqz v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_3
    move v3, v1

    goto :goto_1

    :cond_4
    move v2, v1

    goto :goto_2

    :cond_5
    const-wide/16 v2, 0x0

    cmp-long v2, v4, v2

    if-ltz v2, :cond_6

    const/high16 v2, 0x41a00000    # 20.0f

    invoke-virtual {p0, p1, v2}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/location/Location;F)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_6
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    cmpg-float v2, v2, v3

    if-lez v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/h;->b(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public declared-synchronized b(Lcom/twitter/library/platform/i;)V
    .locals 4

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->c:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/platform/LocationProducer;->h()V

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->l()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->f:Landroid/location/LocationManager;

    invoke-virtual {v0}, Landroid/location/LocationManager;->getAllProviders()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    return v0
.end method

.method public declared-synchronized e()Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->f:Landroid/location/LocationManager;

    const-string/jumbo v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->f:Landroid/location/LocationManager;

    const-string/jumbo v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public f()V
    .locals 4

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->m:Z

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    const/4 v1, 0x2

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    return-void
.end method

.method public g()V
    .locals 5

    const/4 v1, 0x2

    const/4 v4, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->m:Z

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->k:Z

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->l:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/platform/LocationProducer;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    cmp-long v2, v0, v2

    if-gez v2, :cond_2

    iget-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->n:J

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/library/platform/LocationProducer;->a(JLjava/lang/Object;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v2, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    invoke-virtual {v2, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/library/platform/LocationProducer;->d:Landroid/os/Handler;

    invoke-virtual {v2, v4, v0, v1}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0
.end method

.method protected declared-synchronized h()V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->l:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    invoke-virtual {v0}, Lcom/twitter/library/platform/h;->d()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->q:J

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/platform/LocationProducer;->l:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method i()Lcom/twitter/library/platform/h;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->g:Lcom/twitter/library/platform/h;

    return-object v0
.end method

.method declared-synchronized j()V
    .locals 2

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/library/platform/LocationProducer;->e:Landroid/location/Location;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/platform/LocationProducer;->r:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
