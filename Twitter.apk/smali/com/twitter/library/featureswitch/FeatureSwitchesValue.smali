.class public Lcom/twitter/library/featureswitch/FeatureSwitchesValue;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = -0x7b4fe60ef5038be4L


# instance fields
.field private mImpression:Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

.field protected mKey:Ljava/lang/String;

.field protected mValue:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    return-object v0
.end method

.method public a(Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mImpression:Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    return-void
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mImpression:Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mImpression:Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    const/4 v0, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-ne v1, v2, :cond_0

    check-cast p1, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;

    iget-object v1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    if-nez v1, :cond_4

    iget-object v1, p1, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    if-nez v1, :cond_0

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    if-nez v1, :cond_5

    iget-object v1, p1, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    if-nez v1, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0

    :cond_4
    iget-object v1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    iget-object v2, p1, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    iget-object v2, p1, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mValue:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mImpression:Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/featureswitch/FeatureSwitchesValue;->mImpression:Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;

    invoke-virtual {v1}, Lcom/twitter/library/featureswitch/FeatureSwitchesValue$FeatureSwitchesImpression;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method
