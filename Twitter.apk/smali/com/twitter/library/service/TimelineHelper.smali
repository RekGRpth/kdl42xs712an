.class public Lcom/twitter/library/service/TimelineHelper;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/aa;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/network/a;JJIDDZZLjava/lang/String;Lcom/twitter/library/service/TimelineHelper$FilterType;)Lcom/twitter/library/service/r;
    .locals 20

    sget-object v1, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object/from16 v0, p17

    if-ne v0, v1, :cond_1

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v4, 0x1

    :goto_0
    move-object/from16 v0, p3

    iget-wide v5, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v1, p0

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    move-object/from16 v7, p3

    move-wide/from16 v8, p5

    move-wide/from16 v10, p7

    move/from16 v12, p9

    move-wide/from16 v13, p10

    move-wide/from16 v15, p12

    move/from16 v17, p15

    move-object/from16 v18, p16

    move-object/from16 v19, p17

    invoke-static/range {v1 .. v19}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;Lcom/twitter/library/network/aa;Lcom/twitter/library/provider/az;ZJLcom/twitter/library/api/TwitterUser;JJIDDZLjava/lang/String;Lcom/twitter/library/service/TimelineHelper$FilterType;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v1, Lcom/twitter/library/util/f;

    move-object/from16 v0, p3

    iget-wide v5, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-direct {v1, v0, v5, v6, v3}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    if-nez v4, :cond_0

    sget-object v3, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object/from16 v0, p17

    if-eq v0, v3, :cond_2

    :cond_0
    const/16 v3, 0x2c

    move-object/from16 v0, p3

    invoke-static {v3, v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/api/TwitterUser;Lcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    :goto_1
    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p3

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/service/r;

    invoke-direct {v3, v2, v1}, Lcom/twitter/library/service/r;-><init>(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/api/ao;)V

    return-object v3

    :cond_1
    const/4 v4, 0x0

    goto :goto_0

    :cond_2
    const/4 v3, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p3

    invoke-static {v0, v3, v4, v1}, Lcom/twitter/library/api/ao;->a(Lcom/twitter/library/api/TwitterUser;ZZLcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/library/network/aa;Lcom/twitter/library/provider/az;ZJLcom/twitter/library/api/TwitterUser;JJIDDZLjava/lang/String;Lcom/twitter/library/service/TimelineHelper$FilterType;)Ljava/lang/StringBuilder;
    .locals 10

    sget-object v2, Lcom/twitter/library/service/q;->a:[I

    invoke-virtual/range {p18 .. p18}, Lcom/twitter/library/service/TimelineHelper$FilterType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    if-eqz p3, :cond_b

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "inject_ptr_enabled"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_a

    iget-object v2, p1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "discover"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "home"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    :goto_0
    const-string/jumbo v3, "user_id"

    invoke-static {v2, v3, p4, p5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :goto_1
    const-string/jumbo v3, "pc"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move-object v9, v2

    :goto_2
    if-lez p11, :cond_0

    const-string/jumbo v2, "count"

    move/from16 v0, p11

    invoke-static {v9, v2, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_0
    const-wide/16 v2, 0x0

    cmp-long v2, p9, v2

    if-lez v2, :cond_1

    const-string/jumbo v2, "max_id"

    move-wide/from16 v0, p9

    invoke-static {v9, v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-lez v2, :cond_2

    const-string/jumbo v2, "since_id"

    move-wide/from16 v0, p7

    invoke-static {v9, v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_2
    invoke-static/range {p12 .. p13}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_3

    invoke-static/range {p14 .. p15}, Ljava/lang/Double;->isNaN(D)Z

    move-result v2

    if-nez v2, :cond_3

    const-string/jumbo v2, "lat"

    move-wide/from16 v0, p12

    invoke-static {v9, v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    const-string/jumbo v2, "long"

    move-wide/from16 v0, p14

    invoke-static {v9, v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_3
    move-object/from16 v0, p6

    iget-wide v2, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    const-string/jumbo v4, "android_four_tweet_modules_1377"

    invoke-static {p0, v2, v3, v4}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "four_tweet_modules"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz p3, :cond_4

    if-eqz v2, :cond_4

    const-string/jumbo v2, "four_tweet_module"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_4
    const-string/jumbo v2, "earned"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_entities"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_user_entities"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_media_features"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_cards"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_my_retweet"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "timezone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v3, 0x8

    const/4 v4, 0x0

    invoke-virtual/range {p6 .. p6}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v5

    const-wide/16 v7, 0x0

    move-object v2, p2

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/library/provider/az;->a(IIJJ)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const-string/jumbo v3, "ptr_cursor"

    invoke-static {v9, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    if-eqz p16, :cond_7

    invoke-static {}, Lkm;->b()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "include_ptr"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-static {p0}, Lcom/twitter/library/service/TimelineHelper;->c(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    const-string/jumbo v3, "ptr_demo"

    invoke-static {v9, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v2, "exp"

    const-string/jumbo v3, "android_ptr_design_1870"

    invoke-static {}, Lkm;->a()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lkk;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_7
    if-eqz p17, :cond_8

    const/4 v2, 0x1

    move-object/from16 v0, p17

    invoke-static {v9, v0, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_8
    const-string/jumbo v2, "android_custom_timelines_follow_2030"

    invoke-static {v2}, Lkk;->c(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    const-string/jumbo v2, "include_curated_tweets"

    const/4 v3, 0x1

    invoke-static {v9, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_9
    invoke-virtual {p1, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    return-object v9

    :pswitch_0
    iget-object v2, p1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "timeline"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "home"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-static {v2, v3, p4, p5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v3, "filters"

    const-string/jumbo v4, "favorite_users"

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "pc"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move-object v9, v2

    goto/16 :goto_2

    :pswitch_1
    iget-object v2, p1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "timeline"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "home"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-static {v2, v3, p4, p5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v3, "filters"

    const-string/jumbo v4, "media"

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "pc"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move-object v9, v2

    goto/16 :goto_2

    :cond_a
    iget-object v2, p1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "timeline"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "home"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/16 :goto_0

    :cond_b
    iget-object v2, p1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "statuses"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "home_timeline"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/provider/az;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/service/r;Ljava/lang/String;JJIZZZLcom/twitter/library/service/TimelineHelper$FilterType;)V
    .locals 28

    sget-object v3, Lcom/twitter/library/service/q;->a:[I

    invoke-virtual/range {p13 .. p13}, Lcom/twitter/library/service/TimelineHelper$FilterType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    const/4 v11, 0x0

    const/4 v6, 0x0

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;)Z

    move-result v13

    :goto_0
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->a:Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v3

    if-eqz v3, :cond_12

    if-nez v13, :cond_0

    sget-object v3, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object/from16 v0, p13

    if-eq v0, v3, :cond_5

    :cond_0
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->b:Lcom/twitter/library/api/ao;

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/av;

    iget-object v0, v3, Lcom/twitter/library/api/av;->b:Ljava/lang/String;

    move-object/from16 v24, v0

    iget-object v3, v3, Lcom/twitter/library/api/av;->a:Ljava/util/ArrayList;

    move-object/from16 v0, p3

    iput-object v3, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    :cond_1
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v20

    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    const/4 v12, 0x0

    const-wide v14, 0x7fffffffffffffffL

    const-wide/16 v9, 0x0

    if-lez v20, :cond_14

    if-nez p10, :cond_14

    const-wide/16 v7, 0x0

    cmp-long v3, p7, v7

    if-lez v3, :cond_8

    move-object/from16 v0, p1

    move-wide/from16 v1, p7

    invoke-virtual {v0, v4, v5, v1, v2}, Lcom/twitter/library/provider/az;->d(JJ)I

    move-result v3

    if-lez v3, :cond_8

    const/4 v3, 0x1

    move-object/from16 v0, p3

    iput-boolean v3, v0, Lcom/twitter/library/service/r;->f:Z

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    add-int/lit8 v7, v20, -0x1

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/x;

    iget-object v7, v3, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v7, :cond_6

    iget-object v3, v3, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    move-object/from16 v16, v3

    :goto_1
    if-eqz v16, :cond_15

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v3

    iget-wide v7, v3, Lcom/twitter/library/api/TwitterStatus;->a:J

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v8}, Lcom/twitter/library/provider/az;->a(JIJ)Z

    move-result v3

    if-nez v3, :cond_15

    const/4 v3, 0x1

    move-object/from16 v0, v16

    iget-wide v7, v0, Lcom/twitter/library/api/TwitterStatus;->a:J

    move-object/from16 v0, v16

    iget-wide v9, v0, Lcom/twitter/library/api/TwitterStatus;->o:J

    :goto_2
    move-wide/from16 v16, v7

    move-wide/from16 v18, v9

    move v15, v3

    :goto_3
    const-wide/16 v7, 0x0

    cmp-long v3, p7, v7

    if-lez v3, :cond_2

    if-gtz v20, :cond_3

    :cond_2
    const-wide/16 v7, 0x0

    cmp-long v3, p5, v7

    if-nez v3, :cond_e

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_e

    :cond_3
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    add-int/lit8 v7, v20, -0x1

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/x;

    iget-object v7, v3, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v7, :cond_c

    iget-object v3, v3, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iget-wide v7, v3, Lcom/twitter/library/api/TwitterStatus;->a:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    :goto_4
    move-object/from16 v21, v3

    :goto_5
    move-object/from16 v0, p0

    invoke-static {v0, v4, v5}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;J)Z

    move-result v25

    move-object/from16 v0, p3

    iget-object v8, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    const-string/jumbo v12, "unspecified"

    const-wide/16 v9, 0x0

    cmp-long v3, p7, v9

    if-lez v3, :cond_f

    if-nez v15, :cond_f

    const/4 v14, 0x1

    :goto_6
    if-nez p11, :cond_10

    if-nez v21, :cond_10

    const/16 v20, 0x1

    :goto_7
    const/16 v22, 0x1

    const/16 v26, 0x0

    const/16 v27, 0x0

    move-object/from16 v7, p1

    move-wide v9, v4

    move/from16 v23, p12

    invoke-virtual/range {v7 .. v27}, Lcom/twitter/library/provider/az;->a(Ljava/util/ArrayList;JILjava/lang/String;ZZZJJZLjava/lang/String;ZZLjava/lang/String;ZLjava/lang/String;Ljava/lang/String;)I

    move-result v3

    move-object/from16 v0, p3

    iput v3, v0, Lcom/twitter/library/service/r;->e:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6}, Lcom/twitter/library/provider/az;->j(JI)I

    move-result v3

    move-object/from16 v0, p3

    iput v3, v0, Lcom/twitter/library/service/r;->d:I

    sget-object v3, Lcom/twitter/library/service/TimelineHelper$FilterType;->a:Lcom/twitter/library/service/TimelineHelper$FilterType;

    move-object/from16 v0, p13

    if-ne v0, v3, :cond_4

    invoke-static/range {p0 .. p0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v4

    const-string/jumbo v5, "tweet"

    move-object/from16 v0, p3

    iget v3, v0, Lcom/twitter/library/service/r;->d:I

    if-lez v3, :cond_11

    const/4 v3, 0x1

    :goto_8
    move-object/from16 v0, p4

    invoke-virtual {v4, v0, v5, v3}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    :cond_4
    :goto_9
    return-void

    :pswitch_0
    const/4 v11, 0x3

    const/16 v6, 0x16

    const/4 v13, 0x0

    goto/16 :goto_0

    :pswitch_1
    const/4 v11, 0x4

    const/16 v6, 0x1a

    const/4 v13, 0x0

    goto/16 :goto_0

    :cond_5
    const/16 v24, 0x0

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->b:Lcom/twitter/library/api/ao;

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/ArrayList;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p3

    iput-object v4, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterStatus;

    move-object/from16 v0, p3

    iget-object v5, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    invoke-static {v3}, Lcom/twitter/library/api/x;->a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/api/x;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_a

    :cond_6
    iget-object v7, v3, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz v7, :cond_7

    iget-object v3, v3, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    invoke-virtual {v3}, Lcom/twitter/library/api/Conversation;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v3

    move-object/from16 v16, v3

    goto/16 :goto_1

    :cond_7
    const/4 v3, 0x0

    move-object/from16 v16, v3

    goto/16 :goto_1

    :cond_8
    const-wide/16 v7, 0x0

    cmp-long v3, p5, v7

    if-lez v3, :cond_14

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v11}, Lcom/twitter/library/provider/az;->b(JI)J

    move-result-wide v16

    move/from16 v0, p9

    int-to-float v3, v0

    const v7, 0x3f333333    # 0.7f

    mul-float/2addr v3, v7

    float-to-int v3, v3

    const-wide/16 v7, 0x0

    cmp-long v7, v16, v7

    if-lez v7, :cond_14

    move/from16 v0, v20

    if-le v0, v3, :cond_14

    const/4 v12, 0x1

    add-int/lit8 v3, v20, -0x1

    move-wide v7, v9

    move-wide v9, v14

    move v14, v3

    :goto_b
    if-ltz v14, :cond_13

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v14}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/x;

    iget-object v15, v3, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v15, :cond_9

    iget-object v3, v3, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    :goto_c
    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterStatus;->c()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v15

    iget-wide v0, v15, Lcom/twitter/library/api/TwitterStatus;->a:J

    move-wide/from16 v18, v0

    cmp-long v15, v16, v18

    if-nez v15, :cond_a

    const/4 v12, 0x0

    move-wide/from16 v16, v7

    move-wide/from16 v18, v9

    move v15, v12

    goto/16 :goto_3

    :cond_9
    iget-object v15, v3, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz v15, :cond_b

    iget-object v3, v3, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    invoke-virtual {v3}, Lcom/twitter/library/api/Conversation;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v3

    goto :goto_c

    :cond_a
    iget-wide v0, v3, Lcom/twitter/library/api/TwitterStatus;->o:J

    move-wide/from16 v18, v0

    cmp-long v15, v9, v18

    if-lez v15, :cond_b

    iget-wide v9, v3, Lcom/twitter/library/api/TwitterStatus;->o:J

    iget-wide v7, v3, Lcom/twitter/library/api/TwitterStatus;->a:J

    :cond_b
    add-int/lit8 v3, v14, -0x1

    move v14, v3

    goto :goto_b

    :cond_c
    iget-object v7, v3, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz v7, :cond_d

    iget-object v3, v3, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    invoke-virtual {v3}, Lcom/twitter/library/api/Conversation;->a()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v3

    iget-wide v7, v3, Lcom/twitter/library/api/TwitterStatus;->a:J

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_4

    :cond_d
    const/4 v3, 0x0

    goto/16 :goto_4

    :cond_e
    const/16 v21, 0x0

    goto/16 :goto_5

    :cond_f
    const/4 v14, 0x0

    goto/16 :goto_6

    :cond_10
    const/16 v20, 0x0

    goto/16 :goto_7

    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_8

    :cond_12
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/twitter/library/service/r;->a:Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v3

    iget v3, v3, Lcom/twitter/internal/network/k;->a:I

    sparse-switch v3, :sswitch_data_0

    goto/16 :goto_9

    :sswitch_0
    if-nez v11, :cond_4

    const/16 v4, 0x8

    const/4 v5, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v6

    const-wide/16 v8, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Lcom/twitter/library/provider/az;->a(IIJJ)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v4

    const/4 v6, 0x0

    const/16 v7, 0x8

    const-wide/16 v8, 0x0

    move-object/from16 v3, p1

    invoke-virtual/range {v3 .. v9}, Lcom/twitter/library/provider/az;->a(JIIJ)V

    goto/16 :goto_9

    :cond_13
    move-wide/from16 v16, v7

    move-wide/from16 v18, v9

    move v15, v12

    goto/16 :goto_3

    :cond_14
    move-wide/from16 v16, v9

    move-wide/from16 v18, v14

    move v15, v12

    goto/16 :goto_3

    :cond_15
    move-wide v7, v9

    move v3, v12

    move-wide v9, v14

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x19d -> :sswitch_0
        0x19e -> :sswitch_0
    .end sparse-switch
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "conversations_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static a(Landroid/content/Context;J)Z
    .locals 2

    const-string/jumbo v0, "android_dont_dedup_convos_1334"

    invoke-static {p0, p1, p2, v0}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    const-string/jumbo v0, "android_dont_dedup_convos_1334"

    invoke-static {p0, p1, p2, v0}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "dont_dedup"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "new_connect_types_enabled"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public static c(Landroid/content/Context;)Ljava/lang/String;
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string/jumbo v2, "inject_ptr_enabled"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v2, "inject_ptr_order"

    const-string/jumbo v3, ""

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "inject_ptr_index"

    const-string/jumbo v4, "0"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, ""

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string/jumbo v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    const-string/jumbo v4, ","

    invoke-virtual {v2, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-ltz v3, :cond_2

    array-length v4, v2

    if-lt v3, v4, :cond_3

    :cond_2
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "inject_ptr_index"

    const-string/jumbo v3, "0"

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v3, 0x1

    array-length v4, v2

    rem-int/2addr v0, v4

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v4, "inject_ptr_index"

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    aget-object v0, v2, v3

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
