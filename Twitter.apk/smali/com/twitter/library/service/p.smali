.class public final Lcom/twitter/library/service/p;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Z

.field public final c:J

.field public final d:Lcom/twitter/library/network/OAuthToken;

.field public final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/library/client/Session;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/service/p;->b:Z

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)Z
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
