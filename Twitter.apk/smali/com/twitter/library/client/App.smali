.class public abstract Lcom/twitter/library/client/App;
.super Lcom/twitter/internal/android/TwApplication;
.source "Twttr"


# static fields
.field private static a:Z

.field private static b:Z

.field private static c:Z

.field private static d:Z

.field private static e:Z

.field private static f:Z

.field private static g:J

.field private static h:Ljava/lang/String;

.field private static i:Ljava/lang/String;

.field private static j:I

.field private static k:Z

.field private static l:Z

.field private static m:Z

.field private static n:Lcom/twitter/library/client/AppFlavor;

.field private static o:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "com.twitter.android"

    sput-object v0, Lcom/twitter/library/client/App;->h:Ljava/lang/String;

    const-string/jumbo v0, ""

    sput-object v0, Lcom/twitter/library/client/App;->i:Ljava/lang/String;

    return-void
.end method

.method protected constructor <init>(ZZZZZZJLjava/lang/String;Ljava/lang/String;IZZZLjava/lang/String;)V
    .locals 4

    invoke-direct {p0}, Lcom/twitter/internal/android/TwApplication;-><init>()V

    if-eqz p12, :cond_0

    const-string/jumbo v1, "App"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "debug="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nalpha="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nbeta="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\ndogfood="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nperftest="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\npreload="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nreleaseTimeMs="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p7, p8}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nauthority="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nbuildName="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nbuildNumber="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nmapsSupported="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p13

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nrestrictLogin="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p14

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "\nflavor="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p15

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v1

    sput-wide v1, Lcom/twitter/library/client/App;->o:J

    sput-boolean p1, Lcom/twitter/library/client/App;->a:Z

    sput-boolean p2, Lcom/twitter/library/client/App;->b:Z

    sput-boolean p3, Lcom/twitter/library/client/App;->c:Z

    sput-boolean p4, Lcom/twitter/library/client/App;->d:Z

    sput-boolean p5, Lcom/twitter/library/client/App;->e:Z

    sput-boolean p6, Lcom/twitter/library/client/App;->f:Z

    sput-wide p7, Lcom/twitter/library/client/App;->g:J

    sput-object p9, Lcom/twitter/library/client/App;->h:Ljava/lang/String;

    sput-object p10, Lcom/twitter/library/client/App;->i:Ljava/lang/String;

    sput p11, Lcom/twitter/library/client/App;->j:I

    sput-boolean p12, Lcom/twitter/library/client/App;->k:Z

    sput-boolean p13, Lcom/twitter/library/client/App;->l:Z

    sput-boolean p14, Lcom/twitter/library/client/App;->m:Z

    const-string/jumbo v1, "lite"

    move-object/from16 v0, p15

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v1, Lcom/twitter/library/client/AppFlavor;->b:Lcom/twitter/library/client/AppFlavor;

    sput-object v1, Lcom/twitter/library/client/App;->n:Lcom/twitter/library/client/AppFlavor;

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget-object v1, Lcom/twitter/library/client/AppFlavor;->a:Lcom/twitter/library/client/AppFlavor;

    sput-object v1, Lcom/twitter/library/client/App;->n:Lcom/twitter/library/client/AppFlavor;

    invoke-static {}, Lcom/twitter/library/client/App;->s()I

    move-result v1

    const/16 v2, 0x258

    if-ge v1, v2, :cond_3

    invoke-static {}, Lcom/twitter/library/util/v;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    const/16 v2, 0x212

    if-lt v1, v2, :cond_1

    :cond_3
    if-eqz p12, :cond_4

    const-string/jumbo v1, "App"

    const-string/jumbo v2, "Android for Tablets flavor configured."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    sget-object v1, Lcom/twitter/library/client/AppFlavor;->c:Lcom/twitter/library/client/AppFlavor;

    sput-object v1, Lcom/twitter/library/client/App;->n:Lcom/twitter/library/client/AppFlavor;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/library/client/App;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->a:Z

    return v0
.end method

.method public static b()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->d:Z

    return v0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->b:Z

    return v0
.end method

.method public static e()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->c:Z

    return v0
.end method

.method public static f()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->a:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/twitter/library/client/App;->d:Z

    if-nez v0, :cond_0

    sget-boolean v0, Lcom/twitter/library/client/App;->e:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->f:Z

    return v0
.end method

.method public static h()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->e:Z

    return v0
.end method

.method public static i()J
    .locals 2

    sget-wide v0, Lcom/twitter/library/client/App;->g:J

    return-wide v0
.end method

.method public static j()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/twitter/library/client/App;->h:Ljava/lang/String;

    return-object v0
.end method

.method public static k()Ljava/lang/String;
    .locals 1

    sget-object v0, Lcom/twitter/library/client/App;->i:Ljava/lang/String;

    return-object v0
.end method

.method public static l()I
    .locals 1

    sget v0, Lcom/twitter/library/client/App;->j:I

    return v0
.end method

.method public static m()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->k:Z

    return v0
.end method

.method public static n()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->l:Z

    return v0
.end method

.method public static o()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/App;->m:Z

    return v0
.end method

.method public static p()Lcom/twitter/library/client/AppFlavor;
    .locals 1

    sget-object v0, Lcom/twitter/library/client/App;->n:Lcom/twitter/library/client/AppFlavor;

    return-object v0
.end method

.method public static q()Z
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->p()Lcom/twitter/library/client/AppFlavor;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/AppFlavor;->b:Lcom/twitter/library/client/AppFlavor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static r()Z
    .locals 2

    sget-object v0, Lcom/twitter/library/client/App;->n:Lcom/twitter/library/client/AppFlavor;

    sget-object v1, Lcom/twitter/library/client/AppFlavor;->c:Lcom/twitter/library/client/AppFlavor;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static s()I
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xd

    if-lt v0, v1, :cond_0

    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    :goto_0
    return v0

    :cond_0
    invoke-static {}, Landroid/content/res/Resources;->getSystem()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v2, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    mul-int/lit16 v1, v1, 0xa0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    div-int v0, v1, v0

    goto :goto_0
.end method
