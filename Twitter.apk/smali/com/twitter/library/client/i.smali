.class public Lcom/twitter/library/client/i;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;)J
    .locals 8

    const-wide/16 v6, 0x0

    const-class v2, Lcom/twitter/library/client/i;

    monitor-enter v2

    :try_start_0
    invoke-static {p0, p1}, Lcom/twitter/library/client/i;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v3

    const-string/jumbo v0, "read_"

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v0, v4, v5}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v4, v0, v6

    if-nez v4, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {v3}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v3

    const-string/jumbo v4, "read_"

    invoke-virtual {v3, v4, v0, v1}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/f;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit v2

    return-wide v0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public static declared-synchronized a(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 3

    const-class v1, Lcom/twitter/library/client/i;

    monitor-enter v1

    :try_start_0
    invoke-static {p0, p1}, Lcom/twitter/library/client/i;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v2, "read_"

    invoke-virtual {v0, v2, p2, p3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/client/f;
    .locals 2

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "activity"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static declared-synchronized b(Landroid/content/Context;Ljava/lang/String;J)Z
    .locals 5

    const-class v1, Lcom/twitter/library/client/i;

    monitor-enter v1

    :try_start_0
    invoke-static {p0, p1}, Lcom/twitter/library/client/i;->b(Landroid/content/Context;Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v2, "read_"

    const-wide/16 v3, 0x0

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v2

    cmp-long v2, p2, v2

    if-lez v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v2, "read_"

    invoke-virtual {v0, v2, p2, p3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
