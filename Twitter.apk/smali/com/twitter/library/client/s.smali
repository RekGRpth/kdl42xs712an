.class public Lcom/twitter/library/client/s;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/s;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Ljt;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V
    .locals 2

    invoke-virtual {p2}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljt;->e()Z

    move-result v1

    iput-boolean v1, v0, Lcom/twitter/library/api/UserSettings;->p:Z

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/s;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/client/s;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/aa;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "rate_limit"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RateLimit;

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/RateLimit;)V

    iget-object v0, p1, Lcom/twitter/library/service/b;->b:Ljava/lang/String;

    const-class v3, Ljt;

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    move-object v0, p1

    check-cast v0, Ljt;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/library/client/s;->a(Ljt;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V

    :cond_1
    invoke-virtual {p0, p1, v1, v2}, Lcom/twitter/library/client/s;->a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/service/b;Lcom/twitter/internal/android/service/k;Lcom/twitter/library/client/Session;)V
    .locals 8

    const/4 v7, -0x1

    iget-object v1, p1, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    invoke-virtual {p2}, Lcom/twitter/internal/android/service/k;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    iget-object v0, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    const-string/jumbo v3, "scribe_item_count"

    invoke-virtual {v0, v3, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const-string/jumbo v0, "scribe_log"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v0, :cond_2

    :goto_1
    if-eqz v0, :cond_0

    if-le v3, v7, :cond_1

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->f(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    iget-object v3, p0, Lcom/twitter/library/client/s;->a:Landroid/content/Context;

    invoke-static {v3, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "scribe_event"

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_3

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    return-void
.end method
