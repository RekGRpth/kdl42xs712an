.class Lcom/twitter/library/client/aj;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/client/aa;


# direct methods
.method constructor <init>(Lcom/twitter/library/client/aa;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/aj;->a:Lcom/twitter/library/client/aa;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/aj;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 5

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/client/aj;->a:Lcom/twitter/library/client/aa;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v1, v1, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/client/Session;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    check-cast p1, Lcom/twitter/library/api/account/u;

    iget-object v2, p0, Lcom/twitter/library/client/aj;->a:Lcom/twitter/library/client/aa;

    invoke-static {v2}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/library/api/account/u;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/client/ak;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/aj;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->e()Lcom/twitter/library/network/LoginResponse;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->g()Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    invoke-static {v0, v1, v3, v4}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    if-eqz v2, :cond_0

    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/ak;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/twitter/library/client/aj;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v1, v3}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    const/4 v0, 0x2

    const/16 v3, 0x190

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->f()Lcom/twitter/library/api/t;

    move-result-object v4

    invoke-interface {v2, v1, v0, v3, v4}, Lcom/twitter/library/client/ak;->a(Lcom/twitter/library/client/Session;IILcom/twitter/library/api/t;)V

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/twitter/library/client/aj;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->h()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v1, v4}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v3, 0x19c

    if-ne v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->f()Lcom/twitter/library/api/t;

    move-result-object v0

    invoke-interface {v2, v1, v0}, Lcom/twitter/library/client/ak;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/t;)V

    goto :goto_0

    :cond_3
    const/4 v3, 0x1

    invoke-virtual {p1}, Lcom/twitter/library/api/account/u;->f()Lcom/twitter/library/api/t;

    move-result-object v4

    invoke-interface {v2, v1, v3, v0, v4}, Lcom/twitter/library/client/ak;->a(Lcom/twitter/library/client/Session;IILcom/twitter/library/api/t;)V

    goto :goto_0
.end method
