.class public Lcom/twitter/library/client/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field h:Z

.field i:I

.field j:Z

.field k:I

.field l:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v1, p0, Lcom/twitter/library/client/e;->h:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/client/e;->j:Z

    const/16 v0, 0x1e

    iput v0, p0, Lcom/twitter/library/client/e;->k:I

    iput-boolean v1, p0, Lcom/twitter/library/client/e;->l:Z

    return-void
.end method


# virtual methods
.method public final a(II)V
    .locals 3

    and-int v0, p1, p2

    invoke-virtual {p0}, Lcom/twitter/library/client/e;->b()I

    move-result v1

    xor-int/lit8 v2, p2, -0x1

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/e;->c(I)V

    return-void
.end method

.method public final b()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/client/e;->k:I

    return v0
.end method

.method protected final c()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/client/e;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Options are already configured!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public final c(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/client/e;->c()V

    iput p1, p0, Lcom/twitter/library/client/e;->k:I

    return-void
.end method

.method public final c(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/client/e;->c()V

    iput-boolean p1, p0, Lcom/twitter/library/client/e;->j:Z

    return-void
.end method

.method public final d(I)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/client/e;->c()V

    iput p1, p0, Lcom/twitter/library/client/e;->i:I

    return-void
.end method

.method public final d(Z)V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/client/e;->c()V

    iput-boolean p1, p0, Lcom/twitter/library/client/e;->l:Z

    return-void
.end method
