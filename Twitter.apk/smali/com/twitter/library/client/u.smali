.class public final Lcom/twitter/library/client/u;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final d:[I

.field private static final e:Z


# instance fields
.field private f:Lcom/twitter/library/api/TwitterUser;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x0

    new-array v1, v0, [I

    sput-object v1, Lcom/twitter/library/client/u;->d:[I

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "LegacyRequest"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    sput-boolean v0, Lcom/twitter/library/client/u;->e:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/client/u;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private A(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    const/4 v1, 0x3

    const/4 v6, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v3, v1, [Ljava/lang/Object;

    const-string/jumbo v4, "1.1"

    aput-object v4, v3, v6

    const-string/jumbo v4, "search"

    aput-object v4, v3, v0

    const/4 v4, 0x2

    const-string/jumbo v5, "typeahead"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "prefetch"

    invoke-static {v2, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v3, "cache_age"

    const-wide/16 v4, -0x1

    invoke-virtual {p4, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v5, "i_type"

    invoke-virtual {p4, v5, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    packed-switch v5, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    const-string/jumbo v1, "result_type"

    const-string/jumbo v5, "users"

    invoke-static {v2, v1, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "users_cache_age"

    invoke-static {v2, v1, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v1, "media_tagging_in_prefetch"

    invoke-static {v2, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move v1, v0

    :goto_0
    const-string/jumbo v0, "count"

    invoke-virtual {p4, v0, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    const-string/jumbo v3, "count"

    invoke-static {v2, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_0
    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p2, v0, v3, v4, v1}, Lcom/twitter/library/provider/az;->b(Ljava/util/ArrayList;JI)I

    :cond_1
    return-object v2

    :pswitch_2
    const-string/jumbo v0, "result_type"

    const-string/jumbo v5, "topics"

    invoke-static {v2, v0, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "topics_cache_age"

    invoke-static {v2, v0, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "result_type"

    const-string/jumbo v1, "oneclick"

    invoke-static {v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "topics_cache_age"

    invoke-static {v2, v0, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const/4 v0, 0x5

    move v1, v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/twitter/library/network/aa;Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JJLcom/twitter/library/api/TwitterUser;)Lcom/twitter/internal/network/HttpOperation;
    .locals 9

    const-wide/16 v2, 0x0

    cmp-long v2, p5, v2

    if-eqz v2, :cond_0

    const-wide/16 v2, 0x0

    cmp-long v2, p7, v2

    if-nez v2, :cond_1

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "friendships"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "show"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "source_id"

    invoke-static {v2, v3, p5, p6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v3, "target_id"

    move-wide/from16 v0, p7

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const/16 v3, 0x1e

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v4

    new-instance v3, Lcom/twitter/library/network/d;

    invoke-direct {v3, p1, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v5, p2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v5, v6}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/twitter/library/network/d;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    if-eqz p9, :cond_2

    move-object/from16 v0, p9

    iget-boolean v2, v0, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v2, :cond_3

    :cond_2
    iget-object v2, p0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "users"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "show"

    aput-object v7, v5, v6

    invoke-static {v2, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ".json"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "user_id"

    move-wide/from16 v0, p7

    invoke-static {v2, v5, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const/16 v5, 0x11

    invoke-static {v5}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v5

    new-instance v6, Lcom/twitter/library/network/d;

    invoke-direct {v6, p1, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v7, p2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v6, v7, v8}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, p4}, Lcom/twitter/library/network/d;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v5}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    move-object/from16 p9, v2

    :cond_3
    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    if-eqz p9, :cond_4

    move-object/from16 v0, p9

    iget v4, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v4}, Lcom/twitter/library/provider/ay;->j(I)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    or-int/lit16 v2, v2, 0x4000

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    :cond_4
    :goto_1
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-wide/from16 v0, p7

    invoke-virtual {p3, v0, v1, v2}, Lcom/twitter/library/provider/az;->c(JI)V

    :cond_5
    move-object v2, v3

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    and-int/lit16 v2, v2, -0x4001

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "device_follow"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string/jumbo v3, "lifeline_follow"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    const-string/jumbo v3, "user_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    const-string/jumbo v3, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v2, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v6, "challenges_passed"

    const/4 v7, 0x0

    invoke-virtual {v2, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string/jumbo v7, "impression_id"

    invoke-virtual {v2, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "earned"

    const/4 v9, 0x0

    invoke-virtual {v2, v8, v9}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v9, v9, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v14, "1.1"

    aput-object v14, v10, v11

    const/4 v11, 0x1

    const-string/jumbo v14, "friendships"

    aput-object v14, v10, v11

    const/4 v11, 0x2

    const-string/jumbo v14, "create"

    aput-object v14, v10, v11

    invoke-static {v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ".json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, "send_error_codes"

    const/4 v11, 0x1

    invoke-static {v9, v10, v11}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    new-instance v11, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v14, "user_id"

    invoke-static {v12, v13}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    invoke-direct {v11, v14, v15}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "follow"

    const-string/jumbo v14, "true"

    invoke-direct {v1, v11, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    if-eqz v5, :cond_1

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "lifeline"

    const-string/jumbo v14, "true"

    invoke-direct {v1, v11, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v7, :cond_2

    const-string/jumbo v1, "impression_id"

    invoke-static {v9, v1, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v8, :cond_2

    const-string/jumbo v1, "earned"

    const/4 v7, 0x1

    invoke-static {v9, v1, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_2
    if-eqz v6, :cond_3

    const-string/jumbo v1, "challenges_passed"

    const/4 v6, 0x1

    invoke-static {v9, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_3
    const-string/jumbo v1, "handles_challenges"

    const-string/jumbo v6, "1"

    invoke-static {v9, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v1, 0x11

    new-instance v6, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v3, v4, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v1, v6}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/client/u;->s()Lcom/twitter/library/service/p;

    move-result-object v14

    new-instance v6, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v7, v14, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v6, v7, v8}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v6

    sget-object v7, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v6, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v6

    move-object/from16 v0, p2

    invoke-virtual {v6, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v6

    invoke-virtual {v6, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v15

    invoke-virtual {v15}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v6

    if-eqz v6, :cond_7

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v1, "user"

    invoke-virtual {v2, v1, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-boolean v1, v11, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-eqz v1, :cond_6

    const/16 v1, 0x4000

    :cond_4
    :goto_0
    iget-wide v5, v11, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v5, v6, v1}, Lcom/twitter/library/provider/az;->d(JI)V

    iget-wide v1, v11, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->h(J)I

    move-result v1

    iput v1, v11, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iget-boolean v1, v11, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    if-nez v1, :cond_5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p1

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    const/4 v2, 0x2

    move-object/from16 v1, p1

    move-wide v5, v12

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(IJJ)V

    new-instance v1, Lcom/twitter/library/api/search/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v14, v11}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/u;->b(Lcom/twitter/internal/android/service/a;)V

    :cond_5
    :goto_1
    return-object v15

    :cond_6
    const/4 v1, 0x1

    if-eqz v5, :cond_4

    const/16 v1, 0x101

    goto :goto_0

    :cond_7
    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const-string/jumbo v3, "custom_errors"

    invoke-static {v1}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    move-object/from16 v0, p0

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/client/u;->a(Ljava/util/ArrayList;Landroid/os/Bundle;)V

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/service/p;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    const-string/jumbo v0, "email"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "i"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "users"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "suggest_screen_names"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "email"

    invoke-static {v2, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "full_name"

    invoke-static {v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v0, 0x1d

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    sget-object v3, Lcom/twitter/library/network/n;->s:Lcom/twitter/library/network/OAuthToken;

    sget-object v4, Lcom/twitter/library/network/n;->o:Ljava/lang/String;

    sget-object v5, Lcom/twitter/library/network/n;->p:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v2, "screen_name_suggestions"

    invoke-virtual {p2, v2, v0}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_2
    return-object v1
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v5

    const/4 v2, 0x1

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v2, "protected"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "protected"

    const-string/jumbo v4, "protected"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "discoverable_by_email"

    const-string/jumbo v4, "discoverable_by_email"

    invoke-virtual {p3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "discoverable_by_mobile_phone"

    const-string/jumbo v4, "discoverable_by_mobile_phone"

    invoke-virtual {p3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v2, "new_screen_name"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "screen_name"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    const-string/jumbo v2, "email"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_2

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "email"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const-string/jumbo v2, "old_password"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_3

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "current_password"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    const-string/jumbo v2, "new_password"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_4

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "new_password"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "password_confirmation"

    invoke-direct {v3, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    const/16 v2, 0xa

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UserSettings;

    const-string/jumbo v2, "settings"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :goto_0
    return-object v1

    :cond_5
    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v2, "custom_errors"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;Lcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;
    .locals 9

    const-wide/16 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, -0x1

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "users"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "recommendations"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "connections"

    const-string/jumbo v2, "connections"

    invoke-virtual {p3, v2, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {p3, v1, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    cmp-long v3, v1, v7

    if-lez v3, :cond_0

    const-string/jumbo v3, "user_id"

    invoke-static {v0, v3, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_0
    const-string/jumbo v1, "user_type"

    invoke-virtual {p3, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "cluster_follow_type"

    invoke-virtual {p3, v2, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-eq v2, v5, :cond_3

    packed-switch v2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid clusterFollowType: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "wtf-view-all-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string/jumbo v1, "limit"

    invoke-virtual {p3, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    if-lez v1, :cond_1

    const-string/jumbo v2, "limit"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_1
    const-string/jumbo v1, "users"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "users"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_2

    array-length v2, v1

    if-lez v2, :cond_2

    const-string/jumbo v2, "excluded"

    array-length v3, v1

    invoke-static {v0, v2, v1, v6, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    :cond_2
    const-string/jumbo v1, "pc"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v1, "include_user_entities"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p5, v1}, Lcom/twitter/library/util/f;->b(Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0

    :pswitch_1
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "wtf-view-all-cluster-follow-replenish"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "activity-target-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    packed-switch v1, :pswitch_data_1

    :pswitch_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid userType: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_4
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "st-component"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_5
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "profile-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_6
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "categories-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_7
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "contacts-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_8
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "followers-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_9
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "following-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_a
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "wtf-component"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_b
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "welcome-flow-recommendations"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_c
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "wtf-people-tab"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_d
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "pymk"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_e
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "tweet-detail-favorited-by-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_f
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "tweet-detail-retweeted-by-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_10
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "activity-source-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_11
    const-string/jumbo v1, "display_location"

    const-string/jumbo v2, "activity-target-cluster-follow"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_9
        :pswitch_8
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_7
        :pswitch_10
        :pswitch_a
        :pswitch_4
        :pswitch_e
        :pswitch_f
        :pswitch_11
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_b
        :pswitch_5
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Lcom/twitter/library/provider/az;JIJLandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 14

    const/4 v12, 0x0

    iget-object v2, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "users"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "reverse_lookup"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, "map"

    const/4 v4, 0x1

    invoke-static {v3, v2, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v2, 0x23

    new-instance v4, Lcom/twitter/library/util/f;

    iget-object v5, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-wide/from16 v0, p4

    invoke-direct {v4, v5, v0, v1, v6}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v4}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v5, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v4, v5, v3}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v5, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v4, v5, v6}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v5, v3, [J

    const/4 v3, 0x0

    move v4, v3

    :goto_0
    array-length v3, v5

    if-ge v4, v3, :cond_0

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/ArrayList;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v3}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v6

    aput-wide v6, v5, v4

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_0

    :cond_0
    const-string/jumbo v3, "users"

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/Collection;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v2, p3

    move-wide/from16 v4, p4

    move/from16 v6, p6

    move-wide/from16 v7, p7

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    add-int/2addr v2, v12

    :goto_1
    const-string/jumbo v3, "count"

    move-object/from16 v0, p9

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v13

    :cond_1
    move v2, v12

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Lcom/twitter/library/provider/az;J[Ljava/lang/String;[Ljava/lang/String;[JIJLjava/lang/String;ZLandroid/os/Bundle;Z)Lcom/twitter/internal/network/HttpOperation;
    .locals 23

    if-eqz p6, :cond_0

    move-object/from16 v0, p6

    array-length v15, v0

    :goto_0
    if-eqz p7, :cond_1

    move-object/from16 v0, p7

    array-length v14, v0

    :goto_1
    if-eqz p8, :cond_2

    move-object/from16 v0, p8

    array-length v2, v0

    :goto_2
    add-int v3, v15, v14

    add-int/2addr v3, v2

    const/16 v4, 0x64

    invoke-static {v3, v4}, Lcom/twitter/library/network/aa;->a(II)I

    move-result v19

    const/4 v4, 0x0

    new-instance v20, Ljava/util/ArrayList;

    invoke-direct/range {v20 .. v20}, Ljava/util/ArrayList;-><init>()V

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v3, v3, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "users"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "lookup"

    aput-object v7, v5, v6

    invoke-static {v3, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v5, ".json"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    new-instance v22, Ljava/lang/StringBuilder;

    invoke-direct/range {v22 .. v22}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v3, 0x0

    move/from16 v17, v3

    move/from16 v18, v4

    move v5, v2

    move v2, v14

    move v4, v15

    :goto_3
    move/from16 v0, v17

    move/from16 v1, v19

    if-ge v0, v1, :cond_b

    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->setLength(I)V

    move-object/from16 v0, v22

    move-object/from16 v1, v21

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v3, 0x64

    if-eqz p8, :cond_f

    if-lez v5, :cond_f

    const-string/jumbo v6, "user_id"

    move-object/from16 v0, p8

    array-length v7, v0

    sub-int/2addr v7, v5

    move-object/from16 v0, v22

    move-object/from16 v1, p8

    invoke-static {v0, v6, v1, v7, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    move-result v6

    sub-int/2addr v5, v6

    sub-int/2addr v3, v6

    move/from16 v16, v5

    :goto_4
    if-eqz p6, :cond_e

    if-lez v3, :cond_e

    if-lez v4, :cond_e

    const-string/jumbo v5, "email"

    move-object/from16 v0, p6

    array-length v6, v0

    sub-int/2addr v6, v4

    move-object/from16 v0, v22

    move-object/from16 v1, p6

    invoke-static {v0, v5, v1, v6, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/String;II)I

    move-result v5

    sub-int/2addr v4, v5

    sub-int/2addr v3, v5

    move v15, v4

    :goto_5
    if-eqz p7, :cond_d

    if-lez v3, :cond_d

    if-lez v2, :cond_d

    const-string/jumbo v4, "phone"

    move-object/from16 v0, p7

    array-length v5, v0

    sub-int/2addr v5, v2

    move-object/from16 v0, v22

    move-object/from16 v1, p7

    invoke-static {v0, v4, v1, v5, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/String;II)I

    move-result v3

    sub-int/2addr v2, v3

    move v14, v2

    :goto_6
    const-string/jumbo v2, "include_user_entities"

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-eqz p15, :cond_3

    const-string/jumbo v2, "map"

    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v2, 0x23

    new-instance v3, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-wide/from16 v0, p4

    invoke-direct {v3, v4, v0, v1, v5}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    move-object v12, v2

    check-cast v12, Landroid/util/Pair;

    iget-object v3, v12, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/util/Collection;

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x1

    move-object/from16 v2, p3

    move-wide/from16 v4, p4

    move/from16 v6, p9

    move-wide/from16 v7, p10

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    add-int v3, v18, v2

    iget-object v2, v12, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/util/Collection;

    move-object/from16 v0, v20

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    move-object v2, v13

    :goto_7
    add-int/lit8 v4, v17, 0x1

    move/from16 v17, v4

    move-object v13, v2

    move/from16 v18, v3

    move/from16 v5, v16

    move v2, v14

    move v4, v15

    goto/16 :goto_3

    :cond_0
    const/4 v15, 0x0

    goto/16 :goto_0

    :cond_1
    const/4 v14, 0x0

    goto/16 :goto_1

    :cond_2
    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_3
    const/16 v2, 0x16

    new-instance v3, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual/range {v22 .. v22}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    move-wide/from16 v0, p4

    invoke-direct {v3, v4, v0, v1, v5}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-object/from16 v0, v22

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v12

    invoke-virtual {v12}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    if-eqz p13, :cond_6

    if-eqz p8, :cond_6

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5, v4}, Ljava/util/HashMap;-><init>(I)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    iget-wide v6, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v6, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_8

    :cond_4
    move-object/from16 v0, p8

    array-length v6, v0

    const/4 v2, 0x0

    move v4, v2

    :goto_9
    if-ge v4, v6, :cond_7

    aget-wide v7, p8, v4

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    if-eqz v2, :cond_5

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_9

    :cond_6
    move-object v3, v2

    :cond_7
    if-nez v17, :cond_9

    const-string/jumbo v9, "-1"

    :goto_a
    const/4 v11, 0x1

    move-object/from16 v2, p3

    move-wide/from16 v4, p4

    move/from16 v6, p9

    move-wide/from16 v7, p10

    move-object/from16 v10, p12

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    add-int v2, v2, v18

    if-eqz p14, :cond_8

    const-string/jumbo v4, "users"

    move-object/from16 v0, p14

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_8
    move v3, v2

    move-object v2, v12

    goto/16 :goto_7

    :cond_9
    const/4 v9, 0x0

    goto :goto_a

    :cond_a
    move-object v13, v12

    :cond_b
    if-eqz p14, :cond_c

    const-string/jumbo v2, "count"

    move-object/from16 v0, p14

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz p15, :cond_c

    const-string/jumbo v2, "contact"

    move-object/from16 v0, p14

    move-object/from16 v1, v20

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_c
    return-object v13

    :cond_d
    move v14, v2

    goto/16 :goto_6

    :cond_e
    move v15, v4

    goto/16 :goto_5

    :cond_f
    move/from16 v16, v5

    goto/16 :goto_4
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Ljava/lang/String;JLcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "users"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "show"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-wide/16 v1, 0x0

    cmp-long v1, p4, v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "screen_name"

    invoke-static {v0, v1, p3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    const-string/jumbo v1, "include_media_features"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_user_entities"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "send_error_codes"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p7, v1}, Lcom/twitter/library/util/f;->b(Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v1, "user_id"

    invoke-static {v0, v1, p4, p5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JILandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v7, 0x1

    if-ne p6, v7, :cond_4

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v1, v4, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v3

    const-string/jumbo v2, "direct_messages"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v0, "dm_sync_enabled"

    invoke-virtual {p7, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p2}, Lcom/twitter/library/provider/az;->f()J

    move-result-wide v0

    :goto_0
    sget-boolean v3, Lcom/twitter/library/client/u;->e:Z

    if-eqz v3, :cond_0

    const-string/jumbo v3, "LegacyRequest"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Get messages newer than sinceId: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-wide/16 v3, 0x0

    cmp-long v3, v0, v3

    if-lez v3, :cond_1

    const-string/jumbo v3, "since_id"

    invoke-static {v2, v3, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_1
    const-string/jumbo v0, "count"

    const/16 v1, 0xc8

    invoke-static {v2, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v0, "include_entities"

    invoke-static {v2, v0, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v0, "include_media_features"

    invoke-static {v2, v0, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v0, "include_user_entities"

    invoke-static {v2, v0, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v0, "skip_status"

    invoke-static {v2, v0, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v0, 0x20

    new-instance v1, Lcom/twitter/library/util/f;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v3, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/library/client/u;->t()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p2, v0, p4, p5, p6}, Lcom/twitter/library/provider/az;->a(Ljava/util/List;JI)I

    if-ne p6, v7, :cond_2

    iget-object v0, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v0

    iget-object v2, p1, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    const-string/jumbo v3, "message"

    invoke-virtual {p2}, Lcom/twitter/library/provider/az;->g()I

    move-result v4

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    :cond_2
    return-object v1

    :cond_3
    invoke-virtual {p2, v7}, Lcom/twitter/library/provider/az;->c(I)J

    move-result-wide v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v3

    const-string/jumbo v2, "direct_messages"

    aput-object v2, v1, v7

    const-string/jumbo v2, "sent"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2, v3}, Lcom/twitter/library/provider/az;->c(I)J

    move-result-wide v0

    goto/16 :goto_0
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JLandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    invoke-virtual {p2, p4, p5}, Lcom/twitter/library/provider/az;->n(J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v2, p1, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v3, v3, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "direct_messages"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "read"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "send_error_codes"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "last_message_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-lez v0, :cond_1

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v1, "sender_id"

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v1, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p2, p4, p5}, Lcom/twitter/library/provider/az;->m(J)I

    invoke-virtual {p2}, Lcom/twitter/library/provider/az;->g()I

    move-result v0

    const-string/jumbo v1, "unread_dm"

    invoke-virtual {p6, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v1

    const-string/jumbo v5, "message"

    invoke-virtual {v1, v2, v5, v0}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    const-string/jumbo v0, "dm_sync_enabled"

    const/4 v1, 0x0

    invoke-virtual {p6, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v1, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JZ)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    const-wide/16 v0, 0x0

    cmp-long v0, p4, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "direct_messages"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "destroy"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "id"

    invoke-static {p4, p5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    if-eqz p6, :cond_0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p2, p4, p5}, Lcom/twitter/library/provider/az;->g(J)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    iget v1, v1, Lcom/twitter/internal/network/k;->a:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_0

    invoke-virtual {p2, p4, p5}, Lcom/twitter/library/provider/az;->g(J)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const/16 v10, 0x400

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v6

    const/4 v2, 0x1

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v0, "settings"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UserSettings;

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "geo_enabled"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->c:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "protected"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->j:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "discoverable_by_email"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->i:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "discoverable_by_mobile_phone"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->l:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "display_sensitive_media"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->k:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "sleep_time_enabled"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->e:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-boolean v3, v0, Lcom/twitter/library/api/UserSettings;->e:Z

    if-eqz v3, :cond_0

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "start_sleep_time"

    invoke-virtual {v0}, Lcom/twitter/library/api/UserSettings;->a()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "end_sleep_time"

    invoke-virtual {v0}, Lcom/twitter/library/api/UserSettings;->b()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "time_zone"

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const-string/jumbo v3, "trends"

    invoke-virtual {p4, v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-boolean v3, v0, Lcom/twitter/library/api/UserSettings;->p:Z

    if-eqz v3, :cond_6

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "personalized_trends"

    const-string/jumbo v5, "true"

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "screen_name"

    iget-object v5, v0, Lcom/twitter/library/api/UserSettings;->m:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, v0, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    if-eqz v3, :cond_2

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "allow_media_tagging"

    iget-object v5, v0, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "email_follow_enabled"

    iget-boolean v5, v0, Lcom/twitter/library/api/UserSettings;->o:Z

    invoke-static {v5}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v3, 0xa

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v3

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    new-instance v6, Lcom/twitter/library/network/d;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v7, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v6, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UserSettings;

    iget-wide v6, v1, Lcom/twitter/library/api/UserSettings;->a:J

    iget-wide v8, v0, Lcom/twitter/library/api/UserSettings;->a:J

    cmp-long v3, v6, v8

    if-nez v3, :cond_3

    iget-object v0, v0, Lcom/twitter/library/api/UserSettings;->b:Ljava/lang/String;

    iput-object v0, v1, Lcom/twitter/library/api/UserSettings;->b:Ljava/lang/String;

    :cond_3
    iget-object v0, v1, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, v1, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    const-string/jumbo v3, "none"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p2, v4, v5, v10}, Lcom/twitter/library/provider/az;->e(JI)V

    :cond_4
    :goto_1
    const-string/jumbo v0, "settings"

    invoke-virtual {p4, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_5
    return-object v2

    :cond_6
    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "trend_location_woeid"

    iget-wide v5, v0, Lcom/twitter/library/api/UserSettings;->a:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p2, v4, v5, v10}, Lcom/twitter/library/provider/az;->d(JI)V

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 18

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v1, "since_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const-string/jumbo v1, "max_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    const-string/jumbo v1, "count"

    const/16 v2, 0x64

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v12

    const-string/jumbo v1, "i_type"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    const/4 v1, 0x1

    if-ne v1, v5, :cond_6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v2, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "activity"

    aput-object v7, v2, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "by_friends"

    aput-object v7, v2, v6

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/service/TimelineHelper;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "model_version"

    const/4 v6, 0x6

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_0
    const-wide/16 v6, 0x0

    cmp-long v2, v10, v6

    if-nez v2, :cond_1

    const-wide/16 v6, 0x0

    cmp-long v2, v8, v6

    if-nez v2, :cond_1

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/az;->d(I)J

    move-result-wide v10

    :cond_1
    const-string/jumbo v2, "include_entities"

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_user_entities"

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_media_features"

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "include_cards"

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    const-string/jumbo v2, "send_error_codes"

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-lez v12, :cond_2

    const-string/jumbo v2, "count"

    invoke-static {v1, v2, v12}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_2
    const-wide/16 v6, 0x0

    cmp-long v2, v8, v6

    if-lez v2, :cond_3

    const-string/jumbo v2, "max_id"

    invoke-static {v1, v2, v8, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_3
    const-wide/16 v6, 0x0

    cmp-long v2, v10, v6

    if-lez v2, :cond_4

    const-string/jumbo v2, "since_id"

    invoke-static {v1, v2, v10, v11}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v2, "latest_results"

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_4
    invoke-static/range {p4 .. p4}, Ljs;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_5

    const/4 v6, 0x1

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_5
    const/16 v2, 0x1b

    new-instance v6, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v7, v3, v4, v13}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v6}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v2, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v7, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v13, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v13, v14}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v17

    const/4 v2, 0x0

    invoke-virtual/range {v17 .. v17}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_12

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v6, 0x0

    const-wide/16 v13, 0x0

    cmp-long v1, v10, v13

    if-lez v1, :cond_8

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v1, v12, :cond_7

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v6

    add-int/lit8 v6, v6, -0x1

    move v7, v6

    move v6, v1

    :goto_2
    if-ltz v7, :cond_8

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/ae;

    iget-wide v13, v1, Lcom/twitter/library/api/ae;->c:J

    cmp-long v1, v13, v10

    if-gtz v1, :cond_14

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    const/4 v1, 0x0

    :goto_3
    add-int/lit8 v6, v7, -0x1

    move v7, v6

    move v6, v1

    goto :goto_2

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v2, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "activity"

    aput-object v7, v2, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "about_me"

    aput-object v7, v2, v6

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    packed-switch v5, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v2, "filters"

    const-string/jumbo v6, "filtered"

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const-string/jumbo v2, "filters"

    const-string/jumbo v6, "following"

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    const-string/jumbo v2, "filters"

    const-string/jumbo v6, "verified"

    invoke-static {v1, v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    goto :goto_1

    :cond_8
    move/from16 v16, v6

    const/4 v1, 0x0

    const-wide/16 v6, 0x0

    cmp-long v6, v8, v6

    if-lez v6, :cond_13

    move-object/from16 v0, p2

    invoke-virtual {v0, v8, v9, v5}, Lcom/twitter/library/provider/az;->m(JI)I

    move-result v6

    if-lez v6, :cond_13

    const/4 v1, 0x1

    move v15, v1

    :goto_4
    const-wide/16 v6, 0x0

    cmp-long v1, v8, v6

    if-lez v1, :cond_e

    const/4 v6, 0x1

    :goto_5
    if-eqz v6, :cond_f

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v1, v12, :cond_f

    if-nez v15, :cond_f

    const/4 v7, 0x1

    :goto_6
    const/4 v8, 0x0

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/az;->a(Ljava/util/ArrayList;JIZZZ)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_9

    if-eqz v16, :cond_9

    add-int/lit8 v1, v3, -0x1

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/ae;

    if-eqz v1, :cond_9

    iget-wide v7, v1, Lcom/twitter/library/api/ae;->c:J

    const-wide/16 v12, 0x1

    sub-long v8, v7, v12

    iget-wide v1, v1, Lcom/twitter/library/api/ae;->b:J

    const-wide/16 v12, 0x1

    sub-long v12, v1, v12

    move-object/from16 v7, p2

    move v14, v5

    invoke-virtual/range {v7 .. v14}, Lcom/twitter/library/provider/az;->a(JJJI)V

    :cond_9
    const-wide/16 v1, 0x0

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v7

    if-eqz v7, :cond_a

    if-lez v3, :cond_a

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v1, v9}, Lcom/twitter/library/client/i;->a(Landroid/content/Context;Ljava/lang/String;)J

    move-result-wide v1

    const/4 v7, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v1, v2, v7}, Lcom/twitter/library/provider/az;->a(IJZ)I

    :cond_a
    move-wide v7, v1

    if-gtz v3, :cond_b

    if-nez v15, :cond_b

    if-eqz v16, :cond_c

    :cond_b
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/x;->a:Landroid/net/Uri;

    const/4 v10, 0x0

    invoke-virtual {v1, v2, v10}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_c
    invoke-static {v5}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    if-eqz v1, :cond_11

    if-nez v6, :cond_11

    if-lez v3, :cond_11

    const/4 v1, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :cond_d
    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/ae;

    iget-wide v10, v1, Lcom/twitter/library/api/ae;->b:J

    cmp-long v6, v10, v7

    if-lez v6, :cond_d

    iget v1, v1, Lcom/twitter/library/api/ae;->a:I

    packed-switch v1, :pswitch_data_1

    :pswitch_3
    move v1, v2

    :goto_8
    move v2, v1

    goto :goto_7

    :cond_e
    const/4 v6, 0x0

    goto/16 :goto_5

    :cond_f
    const/4 v7, 0x0

    goto/16 :goto_6

    :pswitch_4
    or-int/lit8 v1, v2, 0x1

    goto :goto_8

    :pswitch_5
    or-int/lit8 v1, v2, 0x2

    goto :goto_8

    :pswitch_6
    or-int/lit8 v1, v2, 0x4

    goto :goto_8

    :pswitch_7
    or-int/lit8 v1, v2, 0x8

    goto :goto_8

    :pswitch_8
    or-int/lit8 v1, v2, 0x10

    goto :goto_8

    :pswitch_9
    or-int/lit16 v1, v2, 0x200

    goto :goto_8

    :cond_10
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v1

    invoke-virtual {v1, v9, v2}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    const-string/jumbo v2, "unread_interactions"

    move-object/from16 v0, p2

    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/az;->f(I)I

    move-result v4

    invoke-virtual {v1, v9, v2, v4}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    :cond_11
    const-string/jumbo v1, "scribe_item_count"

    move-object/from16 v0, p5

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move v1, v3

    :goto_9
    const-string/jumbo v2, "count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v17

    :cond_12
    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const-string/jumbo v3, "custom_errors"

    invoke-static {v1}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    move v1, v2

    goto :goto_9

    :cond_13
    move v15, v1

    goto/16 :goto_4

    :cond_14
    move v1, v6

    goto/16 :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_8
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_9
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;[JZ)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string/jumbo v3, "friendships"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "lookup"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    const/16 v2, 0x64

    invoke-static {v0, v1, p5, v4, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    const/16 v1, 0x30

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "users"

    invoke-virtual {p4, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    invoke-virtual {p2, v1, p6}, Lcom/twitter/library/provider/az;->a(Ljava/util/HashMap;Z)V

    :cond_1
    return-object v2
.end method

.method private static a(Landroid/content/Context;)V
    .locals 15

    invoke-static {p0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v2

    array-length v0, v2

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "auto_clean"

    aput-object v1, v3, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "antispam_last_poll_timestamp"

    aput-object v1, v3, v0

    const/4 v0, 0x2

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v1, "notifications_follow_only"

    aput-object v1, v4, v0

    const/4 v0, 0x1

    const-string/jumbo v1, "connect_tab"

    aput-object v1, v4, v0

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v5

    const/4 v1, 0x0

    invoke-interface {v5}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v6

    array-length v7, v3

    const/4 v0, 0x0

    move v14, v0

    move v0, v1

    move v1, v14

    :goto_1
    if-ge v1, v7, :cond_4

    aget-object v8, v3, v1

    invoke-interface {v5, v8}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_3

    const-wide/16 v9, 0x0

    invoke-interface {v5, v8, v9, v10}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    array-length v11, v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v11, :cond_2

    aget-object v12, v2, v0

    new-instance v13, Lcom/twitter/library/client/f;

    iget-object v12, v12, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v13, p0, v12}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v12

    invoke-virtual {v12, v8, v9, v10}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v12

    invoke-virtual {v12}, Lcom/twitter/library/client/f;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-interface {v6, v8}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v0, 0x1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    array-length v3, v4

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v3, :cond_7

    aget-object v7, v4, v1

    invoke-interface {v5, v7}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    const/4 v0, 0x0

    invoke-interface {v5, v7, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    array-length v9, v2

    const/4 v0, 0x0

    :goto_4
    if-ge v0, v9, :cond_5

    aget-object v10, v2, v0

    new-instance v11, Lcom/twitter/library/client/f;

    iget-object v10, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v11, p0, v10}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v10

    invoke-virtual {v10, v7, v8}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v10

    invoke-virtual {v10}, Lcom/twitter/library/client/f;->d()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    invoke-interface {v6, v7}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const/4 v0, 0x1

    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_7
    if-eqz v0, :cond_0

    invoke-interface {v6}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method private declared-synchronized a(Landroid/os/Bundle;)V
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/library/client/App;->k()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "dl_v"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    new-instance v2, Ljava/io/File;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v3}, Lcom/twitter/library/util/Util;->d(Landroid/content/Context;)Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "tmp.apk"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    if-lez v1, :cond_2

    invoke-static {}, Lcom/twitter/library/client/App;->l()I

    move-result v3

    if-ne v1, v3, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    :cond_0
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "dl_v"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    invoke-direct {p0, p1, v1, v2}, Lcom/twitter/library/client/u;->a(Landroid/os/Bundle;ILjava/io/File;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    invoke-static {}, Lcom/twitter/library/client/App;->l()I

    move-result v0

    invoke-direct {p0, p1, v0, v2}, Lcom/twitter/library/client/u;->a(Landroid/os/Bundle;ILjava/io/File;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private a(Landroid/os/Bundle;ILjava/io/File;)V
    .locals 9

    const/4 v3, 0x1

    const/4 v2, 0x0

    const-string/jumbo v0, "https://mobile-tools.twitter.biz/taps/android/"

    const/16 v0, 0x24

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "https://mobile-tools.twitter.biz/taps/android/build_info?build_name="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-static {}, Lcom/twitter/library/client/App;->k()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/h;

    iget v1, v0, Lcom/twitter/library/api/h;->a:I

    if-le v1, p2, :cond_2

    move v1, v3

    :goto_0
    if-nez v1, :cond_0

    invoke-virtual {p3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, v0, Lcom/twitter/library/api/h;->b:Ljava/lang/String;

    invoke-static {p3, v4}, Lcom/twitter/library/util/Util;->a(Ljava/io/File;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    const-string/jumbo v4, "name"

    invoke-virtual {p1, v4, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    move v1, v2

    :goto_2
    if-gt v1, v3, :cond_1

    new-instance v4, Lcom/twitter/library/service/m;

    invoke-direct {v4, p3}, Lcom/twitter/library/service/m;-><init>(Ljava/io/File;)V

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "https://mobile-tools.twitter.biz/taps/android/download?build_name="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-static {}, Lcom/twitter/library/client/App;->k()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "&build_number="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v0, Lcom/twitter/library/api/h;->a:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/twitter/library/api/h;->b:Ljava/lang/String;

    invoke-static {p3, v4}, Lcom/twitter/library/util/Util;->a(Ljava/io/File;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/library/client/App;->k()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "dl_v"

    iget v0, v0, Lcom/twitter/library/api/h;->a:I

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string/jumbo v0, "name"

    invoke-virtual {p1, v0, p3}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p3}, Ljava/io/File;->delete()Z

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {}, Lcom/twitter/library/client/App;->k()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v4, "dl_v"

    invoke-interface {v1, v4}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v1, v3

    goto/16 :goto_1

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_2
.end method

.method private static a(Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 4

    invoke-static {}, Lcom/twitter/library/util/v;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "widget_provider"

    invoke-virtual {p0, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const/4 v2, 0x2

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/pm/PackageManager;->setComponentEnabledSetting(Landroid/content/ComponentName;II)V

    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/library/provider/az;JLcom/twitter/library/api/TwitterUser;)V
    .locals 10

    const/4 v7, 0x0

    iget v0, p4, Lcom/twitter/library/api/TwitterUser;->friendship:I

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v0

    iput v0, p4, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iget-wide v0, p4, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p1, p2, p3, v0, v1}, Lcom/twitter/library/provider/az;->b(JJ)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, p4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v4, 0x2

    const-wide/16 v5, -0x1

    const/4 v9, 0x1

    move-object v0, p1

    move-wide v2, p2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    return-void
.end method

.method private a(Lcom/twitter/library/provider/az;Landroid/os/Bundle;)V
    .locals 4

    const-wide/16 v2, 0x0

    const-string/jumbo v0, "search_id"

    invoke-virtual {p2, v0, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_0

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/provider/az;->f(J)I

    :cond_0
    return-void
.end method

.method private a(Ljava/lang/StringBuilder;Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "lv_puvlic_key"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "lv_puvlic_key"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "public_key"

    invoke-static {p1, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private a(Ljava/util/ArrayList;Landroid/os/Bundle;)V
    .locals 4

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v2, v0, Lcom/twitter/library/api/al;->a:I

    const/16 v3, 0xfa

    if-ne v2, v3, :cond_0

    const-string/jumbo v1, "age_before_timestamp"

    iget-wide v2, v0, Lcom/twitter/library/api/al;->c:J

    invoke-virtual {p2, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    return-void
.end method

.method private b(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "account"

    aput-object v2, v1, v5

    const/4 v2, 0x2

    const-string/jumbo v3, "settings"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "lang"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "lang"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v1, "locale"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "country"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0xa

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UserSettings;

    const-string/jumbo v1, "settings"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    return-object v2
.end method

.method private b(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 13

    iget-wide v8, p1, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const-string/jumbo v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    sget-object v1, Lcom/twitter/library/provider/af;->a:Landroid/net/Uri;

    invoke-static {v1, v8, v9}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    const-string/jumbo v2, "identifier"

    const-string/jumbo v3, "type=? AND page=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p2, v1, v2, v3, v4}, Lcom/twitter/library/provider/az;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "users"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_0
    const-string/jumbo v1, "limit"

    const/4 v2, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v5

    new-instance v6, Lcom/twitter/library/util/f;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v1, v8, v9}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;J)V

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;Lcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v12

    invoke-virtual {v12}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v5}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    move-object v1, p2

    move-wide v3, v8

    move-wide v5, v10

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/TwitterUser;JJI)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "user"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v12
.end method

.method private b(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 21

    new-instance v1, Ljava/lang/StringBuilder;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "discover"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "universal"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    const-string/jumbo v1, "max_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v9, 0x0

    const/4 v8, 0x0

    packed-switch v11, :pswitch_data_0

    const/4 v2, -0x1

    const/4 v1, 0x0

    move v12, v2

    move v3, v9

    move v2, v8

    :goto_0
    const-string/jumbo v8, "include_media_features"

    const/4 v9, 0x1

    invoke-static {v10, v8, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v8, "include_user_entities"

    const/4 v9, 0x1

    invoke-static {v10, v8, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v8, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    const/16 v8, 0x1c

    new-instance v9, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-direct {v9, v13, v4, v5, v14}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v8, v9}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v8

    new-instance v9, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v9, v13, v10}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v13, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v9, v13, v14}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v9

    move-object/from16 v0, p3

    invoke-virtual {v9, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v9

    invoke-virtual {v9}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v9

    if-eqz v9, :cond_8

    invoke-virtual {v8}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/twitter/library/api/search/d;

    const/4 v1, 0x2

    if-ne v11, v1, :cond_5

    const/4 v12, 0x1

    :goto_1
    if-eqz v12, :cond_6

    const-string/jumbo v1, ""

    iget-object v8, v9, Lcom/twitter/library/api/search/d;->c:Ljava/lang/String;

    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v9, Lcom/twitter/library/api/search/d;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_6

    :cond_0
    const/4 v13, 0x1

    :goto_2
    if-nez v2, :cond_1

    if-eqz v3, :cond_7

    :cond_1
    iget-object v1, v9, Lcom/twitter/library/api/search/d;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    const/4 v14, 0x1

    :goto_3
    const-string/jumbo v1, "gap_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    const/16 v19, 0x1

    move-object/from16 v8, p2

    move-wide v10, v4

    move-wide/from16 v17, v6

    invoke-virtual/range {v8 .. v19}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/search/d;JZZZJJZ)I

    move-result v1

    const-string/jumbo v2, "count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "scribe_item_count"

    move-object/from16 v0, p5

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_2
    :goto_4
    return-object v20

    :pswitch_0
    const/4 v2, 0x6

    const/16 v3, 0x13

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/library/provider/az;->a(IIJJ)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_a

    const-string/jumbo v1, "next_cursor"

    invoke-static {v10, v1, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-wide/16 v12, 0x0

    cmp-long v1, v6, v12

    if-lez v1, :cond_3

    const/4 v1, 0x1

    :goto_5
    move v12, v2

    move v2, v1

    move-object v1, v3

    move v3, v9

    goto/16 :goto_0

    :cond_3
    const/4 v1, 0x0

    goto :goto_5

    :pswitch_1
    const/4 v12, 0x5

    const/16 v1, 0x13

    move-object/from16 v0, p2

    invoke-virtual {v0, v12, v1, v4, v5}, Lcom/twitter/library/provider/az;->a(IIJ)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_9

    const-string/jumbo v1, "prev_cursor"

    invoke-static {v10, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x1

    if-ne v11, v1, :cond_4

    const/4 v1, 0x1

    :goto_6
    move v3, v1

    move-object v1, v2

    move v2, v8

    goto/16 :goto_0

    :cond_4
    const/4 v1, 0x0

    goto :goto_6

    :cond_5
    const/4 v12, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v13, 0x0

    goto :goto_2

    :cond_7
    const/4 v14, 0x0

    goto :goto_3

    :cond_8
    invoke-virtual/range {v20 .. v20}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    iget v2, v2, Lcom/twitter/internal/network/k;->a:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_4

    :sswitch_0
    const/4 v2, -0x1

    if-eq v12, v2, :cond_2

    if-eqz v1, :cond_2

    const/16 v11, 0x13

    move-object/from16 v8, p2

    move-wide v9, v4

    move-wide v13, v6

    invoke-virtual/range {v8 .. v14}, Lcom/twitter/library/provider/az;->a(JIIJ)V

    goto :goto_4

    :cond_9
    move-object v1, v2

    move v3, v9

    move v2, v8

    goto/16 :goto_0

    :cond_a
    move-object v1, v3

    move v12, v2

    move v2, v8

    move v3, v9

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x190 -> :sswitch_0
        0x19d -> :sswitch_0
        0x19e -> :sswitch_0
    .end sparse-switch
.end method

.method private static b(Landroid/os/Bundle;Landroid/content/Context;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/library/client/App;->k()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v1, "app_v"

    const/4 v3, -0x1

    invoke-interface {v2, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->l()I

    move-result v1

    :goto_0
    if-ge v3, v1, :cond_2

    invoke-static {p1}, Lcom/twitter/library/client/u;->a(Landroid/content/Context;)V

    const-string/jumbo v3, "owner_ids"

    invoke-virtual {p0, v3}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v3

    array-length v4, v3

    :goto_1
    if-ge v0, v4, :cond_1

    aget-wide v5, v3, v0

    invoke-static {p1, v5, v6}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v5

    invoke-virtual {v5, v7, v7}, Lcom/twitter/library/provider/az;->b(Ljava/lang/String;[Ljava/lang/String;)I

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/util/Util;->m(Landroid/content/Context;)I

    move-result v1

    goto :goto_0

    :cond_1
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "app_v"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string/jumbo v0, "app_updated"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    return-void
.end method

.method private c(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "update_email"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "email"

    const-string/jumbo v4, "email"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "password"

    const-string/jumbo v4, "pass"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v2, 0x41

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method private c(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 14

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const-string/jumbo v1, "user_type"

    const/4 v2, -0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v12

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    sparse-switch v12, :sswitch_data_0

    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v2, "Unsupported type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    sget-object v1, Lcom/twitter/library/provider/ax;->y:Landroid/net/Uri;

    invoke-static {v1, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    const-string/jumbo v4, "user_id"

    const/4 v5, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v4, v5, v6}, Lcom/twitter/library/provider/az;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)[J

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v4, "users"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v1}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_0
    const-string/jumbo v1, "limit"

    const/4 v4, 0x1

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const/4 v1, 0x7

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v5

    new-instance v6, Lcom/twitter/library/util/f;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v1, v2, v3}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;J)V

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    invoke-direct/range {v1 .. v6}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;Lcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v5}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v1, "user_tag"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    const-string/jumbo v1, "user_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    move-object/from16 v1, p2

    move-wide v3, v10

    move v5, v12

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/TwitterUser;JIJJ)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "user"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v13

    :sswitch_1
    sget-object v1, Lcom/twitter/library/provider/ax;->z:Landroid/net/Uri;

    invoke-static {v1, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    goto :goto_0

    :sswitch_2
    sget-object v1, Lcom/twitter/library/provider/ax;->x:Landroid/net/Uri;

    invoke-static {v1, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_0

    :sswitch_3
    sget-object v1, Lcom/twitter/library/provider/ax;->u:Landroid/net/Uri;

    invoke-static {v1, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x13 -> :sswitch_3
        0x14 -> :sswitch_1
        0x16 -> :sswitch_2
    .end sparse-switch
.end method

.method private d(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    const/16 v0, 0x12

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    sget v3, Lil;->mobile_config_url:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "carrier"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string/jumbo v3, "carrier"

    invoke-static {v1, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "carrier"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :cond_0
    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    const-string/jumbo v2, "settings"

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ClientConfiguration;

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v1
.end method

.method private d(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 19

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v1, "status_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v1, "rt_status_id"

    const-wide/16 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    const-string/jumbo v1, "impression_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v1, "earned"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    const/4 v6, 0x1

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(JJZ)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string/jumbo v13, "1.1"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string/jumbo v13, "favorites"

    aput-object v13, v11, v12

    const/4 v12, 0x2

    const-string/jumbo v13, "create"

    aput-object v13, v11, v12

    invoke-static {v1, v11}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, ".json"

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v13, "send_error_codes"

    const-string/jumbo v14, "true"

    invoke-direct {v12, v13, v14}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v12, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v13, "id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v12, v13, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v12}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v9, :cond_0

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "impression_id"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v10, :cond_0

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "earned"

    const-string/jumbo v9, "true"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "include_entities"

    const-string/jumbo v9, "true"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "include_media_features"

    const-string/jumbo v9, "true"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "include_cards"

    const-string/jumbo v9, "true"

    invoke-direct {v7, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v7, v11}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    const/16 v7, 0x25

    invoke-static {v7}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v7

    new-instance v8, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v8, v9, v10}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v8

    sget-object v9, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v8, v9}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v8

    move-object/from16 v0, p3

    invoke-virtual {v8, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v8

    invoke-virtual {v8, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterStatus;

    if-eqz v1, :cond_2

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/twitter/library/api/TwitterStatus;->s:Z

    iget v4, v1, Lcom/twitter/library/api/TwitterStatus;->z:I

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v1, Lcom/twitter/library/api/TwitterStatus;->z:I

    const-string/jumbo v4, "count"

    iget v5, v1, Lcom/twitter/library/api/TwitterStatus;->z:I

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x2

    const-wide/16 v9, -0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    move-object/from16 v4, p2

    move-wide v6, v2

    invoke-virtual/range {v4 .. v17}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZZZ)Ljava/util/Collection;

    :cond_1
    :goto_0
    return-object v18

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "Received null status"

    invoke-static {v1, v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v1

    const-string/jumbo v6, "custom_errors"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const/16 v6, 0x8b

    invoke-static {v1, v6}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v6, 0x0

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(JJZ)I

    goto :goto_0
.end method

.method private e(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const/4 v7, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x3

    const/16 v0, 0x3d

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v2, v6, [Ljava/lang/Object;

    const-string/jumbo v3, "1.1"

    aput-object v3, v2, v5

    const-string/jumbo v3, "device"

    aput-object v3, v2, v7

    const/4 v3, 0x2

    const-string/jumbo v4, "operator_signup_info"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".json"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    const-string/jumbo v3, "phone"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v3

    const/4 v4, 0x5

    if-ne v3, v4, :cond_0

    const-string/jumbo v3, "iso2_cc"

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimCountryIso()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "carrier_name"

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperatorName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    if-lt v4, v6, :cond_0

    const-string/jumbo v4, "mcc"

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v2, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v4, "mnc"

    invoke-virtual {v3, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-static {v2, v4, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_0
    :try_start_0
    const-string/jumbo v3, "phone_number"

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getLine1Number()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v0, v3, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v3, "device_operator_configuration"

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/DeviceOperatorConfiguration;

    invoke-virtual {p3, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    return-object v2

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private e(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 19

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v1, "status_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v1, "impression_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v1, "earned"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    const/4 v6, 0x0

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(JJZ)I

    move-result v6

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v9, 0x3

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const-string/jumbo v11, "1.1"

    aput-object v11, v9, v10

    const/4 v10, 0x1

    const-string/jumbo v11, "favorites"

    aput-object v11, v9, v10

    const/4 v10, 0x2

    const-string/jumbo v11, "destroy"

    aput-object v11, v9, v10

    invoke-static {v1, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v9, ".json"

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v12}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v7, :cond_0

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "impression_id"

    invoke-direct {v10, v11, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v8, :cond_0

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "earned"

    const-string/jumbo v10, "true"

    invoke-direct {v7, v8, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/16 v7, 0x25

    invoke-static {v7}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v7

    new-instance v8, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v8, v10, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v10, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v8, v10, v11}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v8, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v8}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    iget v1, v1, Lcom/twitter/internal/network/k;->a:I

    const/16 v8, 0xc8

    if-ne v1, v8, :cond_2

    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterStatus;

    if-eqz v1, :cond_1

    const/4 v4, 0x0

    iput-boolean v4, v1, Lcom/twitter/library/api/TwitterStatus;->s:Z

    iget v4, v1, Lcom/twitter/library/api/TwitterStatus;->z:I

    invoke-static {v4, v6}, Ljava/lang/Math;->max(II)I

    move-result v4

    iput v4, v1, Lcom/twitter/library/api/TwitterStatus;->z:I

    new-instance v5, Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-direct {v5, v4}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x2

    const-wide/16 v9, -0x1

    const/4 v11, 0x1

    const/4 v12, 0x0

    const/4 v13, 0x1

    const/4 v14, 0x0

    const/4 v15, 0x1

    const/16 v16, 0x1

    const/16 v17, 0x1

    move-object/from16 v4, p2

    move-wide v6, v2

    invoke-virtual/range {v4 .. v17}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZZZ)Ljava/util/Collection;

    :cond_1
    :goto_0
    return-object v18

    :cond_2
    const/16 v6, 0x194

    if-eq v1, v6, :cond_1

    const/4 v6, 0x1

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(JJZ)I

    goto :goto_0
.end method

.method private e()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ay()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_favorite_people_timeline_u4_1989"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const-string/jumbo v0, "message_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p3, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-string/jumbo v0, "report_as"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "direct_messages"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "report_spam"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "dm_id"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v6, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "report_as"

    invoke-direct {v1, v2, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private f(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 33

    const-string/jumbo v2, "page"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v7

    const-string/jumbo v2, "user_id"

    const-wide/16 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string/jumbo v2, "name"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string/jumbo v2, "user_type"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v2, "count"

    const/4 v3, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    const/4 v3, 0x1

    move-object/from16 v2, p2

    invoke-virtual/range {v2 .. v7}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v19

    sparse-switch v4, :sswitch_data_0

    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unknown user type: "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v11, "1.1"

    aput-object v11, v3, v8

    const/4 v8, 0x1

    const-string/jumbo v11, "followers"

    aput-object v11, v3, v8

    const/4 v8, 0x2

    const-string/jumbo v11, "list"

    aput-object v11, v3, v8

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v3, 0x2

    const/4 v2, 0x1

    move/from16 v25, v2

    move/from16 v26, v3

    move-object/from16 v27, v8

    :goto_0
    if-eqz v25, :cond_0

    const-string/jumbo v2, "pc"

    const/4 v3, 0x1

    move-object/from16 v0, v27

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    const-string/jumbo v2, "include_user_entities"

    const/4 v3, 0x1

    move-object/from16 v0, v27

    invoke-static {v0, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v2, "user_id"

    move-object/from16 v0, v27

    invoke-static {v0, v2, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    if-lez v9, :cond_3

    const/16 v2, 0x14

    invoke-static {v9, v2}, Lcom/twitter/library/network/aa;->a(II)I

    move-result v2

    move/from16 v21, v2

    :goto_1
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->length()I

    move-result v28

    const/16 v23, 0x0

    const/16 v22, 0x0

    new-instance v29, Ljava/util/ArrayList;

    invoke-direct/range {v29 .. v29}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p1

    iget-wide v0, v0, Lcom/twitter/library/service/p;->c:J

    move-wide/from16 v30, v0

    const/4 v2, 0x0

    move/from16 v24, v2

    move-object/from16 v2, v23

    :goto_2
    move/from16 v0, v24

    move/from16 v1, v21

    if-ge v0, v1, :cond_d

    if-eqz v19, :cond_1

    const-string/jumbo v2, "cursor"

    move-object/from16 v0, v27

    move-object/from16 v1, v19

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v2, 0x15

    new-instance v3, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v3, v8, v5, v6, v9}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-object/from16 v0, v27

    invoke-direct {v3, v8, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v30

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v23

    invoke-virtual/range {v23 .. v23}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/aj;

    if-nez v2, :cond_4

    invoke-virtual/range {v23 .. v23}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    const/4 v3, 0x0

    iput v3, v2, Lcom/twitter/internal/network/k;->a:I

    move/from16 v2, v22

    move-object/from16 v3, v23

    :goto_3
    const-string/jumbo v4, "count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "users"

    move-object/from16 v0, p4

    move-object/from16 v1, v29

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-object v3

    :sswitch_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v11, "1.1"

    aput-object v11, v3, v8

    const/4 v8, 0x1

    const-string/jumbo v11, "friends"

    aput-object v11, v3, v8

    const/4 v8, 0x2

    const-string/jumbo v11, "list"

    aput-object v11, v3, v8

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v3, 0x1

    const/4 v2, 0x1

    move/from16 v25, v2

    move/from16 v26, v3

    move-object/from16 v27, v8

    goto/16 :goto_0

    :sswitch_2
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/client/u;->e()Z

    move-result v2

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v11, "1.1"

    aput-object v11, v3, v8

    const/4 v8, 0x1

    const-string/jumbo v11, "favorite_users"

    aput-object v11, v3, v8

    const/4 v8, 0x2

    const-string/jumbo v11, "list"

    aput-object v11, v3, v8

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v3, 0x801

    const/4 v2, 0x0

    move/from16 v25, v2

    move/from16 v26, v3

    move-object/from16 v27, v8

    goto/16 :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v11, "1.1"

    aput-object v11, v3, v8

    const/4 v8, 0x1

    const-string/jumbo v11, "friends"

    aput-object v11, v3, v8

    const/4 v8, 0x2

    const-string/jumbo v11, "list"

    aput-object v11, v3, v8

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/16 v3, 0x11

    const/4 v2, 0x0

    const-string/jumbo v11, "type"

    const-string/jumbo v12, "sms"

    invoke-static {v8, v11, v12}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    move/from16 v25, v2

    move/from16 v26, v3

    move-object/from16 v27, v8

    goto/16 :goto_0

    :cond_3
    const/4 v2, 0x1

    move/from16 v21, v2

    goto/16 :goto_1

    :cond_4
    invoke-virtual {v2}, Lcom/twitter/library/api/aj;->b()Ljava/util/ArrayList;

    move-result-object v32

    cmp-long v3, v30, v5

    if-nez v3, :cond_7

    const/4 v3, 0x1

    move/from16 v18, v3

    :goto_4
    packed-switch v4, :pswitch_data_0

    const/4 v9, -0x1

    :goto_5
    if-eqz v25, :cond_8

    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_8

    const/4 v3, -0x1

    if-eq v9, v3, :cond_8

    const/4 v3, 0x1

    move/from16 v17, v3

    :goto_6
    invoke-virtual/range {v32 .. v32}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_5
    :goto_7
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterUser;

    if-eqz v18, :cond_6

    iget v8, v3, Lcom/twitter/library/api/TwitterUser;->friendship:I

    move/from16 v0, v26

    invoke-static {v8, v0}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v8

    iput v8, v3, Lcom/twitter/library/api/TwitterUser;->friendship:I

    :cond_6
    if-eqz v17, :cond_5

    iget-object v8, v3, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    if-eqz v8, :cond_5

    new-instance v8, Lcom/twitter/library/api/TwitterSocialProof;

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    invoke-direct/range {v8 .. v16}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    iget-object v11, v3, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    if-nez v11, :cond_9

    new-instance v11, Lcom/twitter/library/api/TwitterUserMetadata;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct {v11, v8, v12, v13, v14}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v11, v3, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    goto :goto_7

    :cond_7
    const/4 v3, 0x0

    move/from16 v18, v3

    goto :goto_4

    :pswitch_0
    const/4 v9, 0x2

    goto :goto_5

    :pswitch_1
    const/4 v9, 0x1

    goto :goto_5

    :cond_8
    const/4 v3, 0x0

    move/from16 v17, v3

    goto :goto_6

    :cond_9
    new-instance v11, Lcom/twitter/library/api/TwitterUserMetadata;

    const/4 v12, 0x0

    iget-object v13, v3, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    iget-object v13, v13, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    const/4 v14, 0x0

    invoke-direct {v11, v8, v12, v13, v14}, Lcom/twitter/library/api/TwitterUserMetadata;-><init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V

    iput-object v11, v3, Lcom/twitter/library/api/TwitterUser;->metadata:Lcom/twitter/library/api/TwitterUserMetadata;

    goto :goto_7

    :cond_a
    move-object/from16 v0, v29

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v2}, Lcom/twitter/library/api/aj;->a()Ljava/lang/String;

    move-result-object v19

    const-wide/16 v16, -0x1

    if-nez v24, :cond_b

    if-nez v7, :cond_b

    const-string/jumbo v18, "-1"

    :goto_8
    const/16 v20, 0x1

    move-object/from16 v11, p2

    move-object/from16 v12, v32

    move-wide v13, v5

    move v15, v4

    invoke-virtual/range {v11 .. v20}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    add-int v2, v2, v22

    const-string/jumbo v3, "0"

    move-object/from16 v0, v19

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    move-object/from16 v3, v23

    goto/16 :goto_3

    :cond_b
    const/16 v18, 0x0

    goto :goto_8

    :cond_c
    invoke-virtual/range {v27 .. v27}, Ljava/lang/StringBuilder;->length()I

    move-result v3

    move-object/from16 v0, v27

    move/from16 v1, v28

    invoke-virtual {v0, v1, v3}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    add-int/lit8 v3, v24, 0x1

    move/from16 v24, v3

    move/from16 v22, v2

    move-object/from16 v2, v23

    goto/16 :goto_2

    :cond_d
    move-object v3, v2

    move/from16 v2, v22

    goto/16 :goto_3

    :cond_e
    move/from16 v2, v22

    move-object/from16 v3, v23

    goto/16 :goto_3

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_0
        0x10 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private f()V
    .locals 9

    const-wide/16 v4, 0x0

    new-instance v0, Ljl;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/client/u;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/client/u;->f:Lcom/twitter/library/api/TwitterUser;

    const/4 v8, -0x2

    move-wide v6, v4

    invoke-direct/range {v0 .. v8}, Ljl;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;JJI)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljl;->c(Z)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/u;->b(Lcom/twitter/internal/android/service/a;)V

    return-void
.end method

.method private g(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v8, 0x2

    const-string/jumbo v0, "normalized_phone_number"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "text_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p3, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const-string/jumbo v6, "1.1"

    aput-object v6, v5, v9

    const-string/jumbo v6, "device"

    aput-object v6, v5, v10

    const-string/jumbo v6, "register_complete"

    aput-object v6, v5, v8

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "phone_number"

    invoke-direct {v6, v7, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "text_message"

    invoke-direct {v0, v6, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v6, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v6, v7}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "success"

    :goto_0
    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const-string/jumbo v3, "app:twitter_service:phone_number:complete_mobile_verification"

    aput-object v3, v2, v9

    aput-object v0, v2, v10

    invoke-virtual {v5, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v8}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-object v1

    :cond_0
    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    if-nez v0, :cond_1

    const-string/jumbo v0, "error"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "failure"

    goto :goto_0
.end method

.method private g(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 14

    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "users"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "interests"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "topics"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    const/4 v4, 0x5

    new-instance v5, Lcom/twitter/library/util/f;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v2, v3, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v4, v5}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v4

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v5, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v12

    invoke-virtual {v12}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Ljava/util/ArrayList;

    const-string/jumbo v1, "count"

    const/4 v2, 0x7

    move-object/from16 v0, p2

    invoke-virtual {v0, v10, v2}, Lcom/twitter/library/provider/az;->a(Ljava/util/ArrayList;I)I

    move-result v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const/4 v6, 0x0

    :goto_0
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v6, v1, :cond_1

    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iget-object v1, v11, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v2, v11, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    const/4 v5, 0x6

    iget-object v1, v11, Lcom/twitter/library/api/search/TwitterSearchQuery;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    int-to-long v7, v1

    const/4 v9, 0x0

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIIJLjava/lang/String;)V

    iget-object v1, v11, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    :cond_1
    const/4 v5, -0x1

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p2

    move-object v2, v13

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    if-lez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/y;->a:Landroid/net/Uri;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    :cond_2
    return-object v12
.end method

.method private h(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 9

    const-string/jumbo v0, "phone"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "text_message"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "owner_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p3, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "device"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "register"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "raw_phone_number"

    invoke-direct {v6, v7, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "text_message"

    invoke-direct {v6, v7, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "user_id"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v1, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x33

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v6, Lcom/twitter/library/network/d;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v7, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v7, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v6, v7, v8}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v4

    sget-object v6, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    if-eqz v0, :cond_0

    const-wide/16 v5, 0x0

    cmp-long v0, v2, v5

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->d()V

    :goto_0
    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v5, "device_registration_normalized_phone_number"

    invoke-interface {v1, v5, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    const-string/jumbo v1, "normalized_phone_number"

    invoke-virtual {p3, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "success"

    :goto_1
    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v6, "app:twitter_service:phone_number:begin_mobile_verification"

    aput-object v6, v2, v3

    const/4 v3, 0x1

    aput-object v0, v2, v3

    invoke-virtual {v5, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-object v4

    :cond_1
    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_4

    const/16 v1, 0x2c

    const/4 v5, 0x0

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v0, v0, Lcom/twitter/library/api/al;->a:I

    if-ne v1, v0, :cond_3

    const-string/jumbo v0, "unavailable"

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "failure"

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "error"

    goto :goto_1
.end method

.method private h(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "search"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "universal"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v1, "experiments"

    const-string/jumbo v2, "expertsearch:usersearch_sul:user_search_filtered"

    invoke-static {v7, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "q"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string/jumbo v1, "q"

    invoke-static {v7, v1, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "modules"

    const-string/jumbo v2, "User"

    invoke-static {v7, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "include_user_entities"

    const/4 v2, 0x1

    invoke-static {v7, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string/jumbo v1, "user_type"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v2, 0x1

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "next_cursor"

    invoke-static {v7, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p1

    iget-wide v1, v0, Lcom/twitter/library/service/p;->c:J

    const/16 v8, 0x18

    new-instance v10, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v1, v2, v12}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v8, v10}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v8

    new-instance v10, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v10, v11, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v10, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v8}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/search/f;

    iget-object v2, v1, Lcom/twitter/library/api/search/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-instance v7, Ljava/util/ArrayList;

    mul-int/lit8 v2, v10, 0x6

    invoke-direct {v7, v2}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    move v8, v2

    :goto_0
    if-ge v8, v10, :cond_2

    iget-object v2, v1, Lcom/twitter/library/api/search/f;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/search/g;

    const/4 v11, 0x1

    iget v12, v2, Lcom/twitter/library/api/search/g;->a:I

    if-ne v11, v12, :cond_1

    iget-object v2, v2, Lcom/twitter/library/api/search/g;->c:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_0

    :cond_2
    invoke-virtual {v9}, Ljava/lang/String;->hashCode()I

    move-result v2

    int-to-long v11, v2

    if-nez v6, :cond_4

    const-string/jumbo v13, "-1"

    :goto_1
    invoke-virtual {v7}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_5

    const-string/jumbo v14, "0"

    :goto_2
    const/4 v15, 0x1

    move-object/from16 v6, p2

    move-wide v8, v4

    move v10, v3

    invoke-virtual/range {v6 .. v15}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    const-string/jumbo v2, "count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_3
    return-object v16

    :cond_4
    const/4 v13, 0x0

    goto :goto_1

    :cond_5
    iget-object v14, v1, Lcom/twitter/library/api/search/f;->b:Ljava/lang/String;

    goto :goto_2
.end method

.method private i(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 10

    const/4 v5, 0x3

    const/4 v9, 0x0

    const/4 v8, 0x1

    const-string/jumbo v0, "q"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v1, "i_type"

    invoke-virtual {p3, v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    const-string/jumbo v2, "count"

    invoke-virtual {p3, v2, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v3, "src"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/Object;

    const-string/jumbo v6, "1.1"

    aput-object v6, v5, v9

    const-string/jumbo v6, "search"

    aput-object v6, v5, v8

    const/4 v6, 0x2

    const-string/jumbo v7, "typeahead"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "prefetch"

    invoke-static {v4, v5, v9}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v5, "q"

    invoke-static {v4, v5, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "src"

    invoke-static {v4, v0, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    packed-switch v1, :pswitch_data_0

    const-string/jumbo v0, "result_type"

    const-string/jumbo v1, "all"

    invoke-static {v4, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "filters"

    invoke-static {v4, v0, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :goto_1
    if-lez v2, :cond_2

    const-string/jumbo v0, "count"

    invoke-static {v4, v0, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_2
    const/16 v0, 0x27

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    const-string/jumbo v2, "typeahead"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_3
    move-object v0, v1

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v0, "result_type"

    const-string/jumbo v1, "users"

    invoke-static {v4, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "result_type"

    const-string/jumbo v1, "topics"

    invoke-static {v4, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "filters"

    invoke-static {v4, v0, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "result_type"

    const-string/jumbo v1, "hashtags"

    invoke-static {v4, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private i(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const-string/jumbo v0, "user_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v0, "owner_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v0, "impression_id"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "earned"

    const/4 v6, 0x0

    invoke-virtual {p4, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v6, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v6, v6, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "friendships"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v9, "destroy"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "user_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    const-string/jumbo v8, "impression_id"

    invoke-static {v6, v8, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    const-string/jumbo v0, "earned"

    const/4 v1, 0x1

    invoke-static {v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    const/16 v0, 0x11

    new-instance v1, Lcom/twitter/library/util/f;

    iget-object v8, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v1, v8, v2, v3, v9}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v8, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v8, v6}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v8, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v8, v9}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v6, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v7}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    const/4 v1, 0x1

    invoke-virtual {p2, v4, v5, v1}, Lcom/twitter/library/provider/az;->e(JI)V

    const/4 v1, 0x0

    iget-wide v4, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IJJ)V

    :cond_1
    return-object v6
.end method

.method private j(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "prompts"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "resend_confirmation_email"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const/16 v2, 0x2f

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v2, "reason_phrase"

    invoke-virtual {p3, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-object v1
.end method

.method private j(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 15

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v1, "user"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Lcom/twitter/library/api/TwitterUser;

    iget-wide v9, v7, Lcom/twitter/library/api/TwitterUser;->userId:J

    const-string/jumbo v1, "device_follow"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    const-string/jumbo v2, "lifeline_follow"

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    const-string/jumbo v2, "want_retweets"

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    iget-object v2, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v8, "1.1"

    aput-object v8, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v8, "friendships"

    aput-object v8, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v8, "update"

    aput-object v8, v5, v6

    invoke-static {v2, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, ".json"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x2

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "user_id"

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v6, "device_follow"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    if-eqz v1, :cond_8

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "device"

    const-string/jumbo v13, "true"

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    const-string/jumbo v6, "lifeline_follow"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-eqz v11, :cond_9

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "lifeline"

    const-string/jumbo v13, "true"

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    const-string/jumbo v6, "want_retweets"

    move-object/from16 v0, p4

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-eqz v12, :cond_a

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "retweets"

    const-string/jumbo v13, "true"

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    :goto_2
    const/16 v6, 0x1e

    invoke-static {v6}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v8, Lcom/twitter/library/network/d;

    iget-object v13, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v8, v13, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v13, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v8, v13, v14}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v8, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v8}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_7

    const-string/jumbo v2, "device_follow"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_5

    if-eqz v1, :cond_b

    new-instance v2, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v2, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0x10

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v1}, Lcom/twitter/library/provider/az;->d(JI)V

    const/16 v5, 0x10

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v8}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/i;->a:Landroid/net/Uri;

    invoke-static {v3, v2}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "notif_tweet"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    const/4 v1, 0x0

    if-eqz v2, :cond_4

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_3

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :cond_3
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_4
    if-nez v1, :cond_5

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    const/16 v2, 0x3e9

    iput v2, v1, Lcom/twitter/internal/network/k;->a:I

    :cond_5
    :goto_3
    const-string/jumbo v1, "lifeline_follow"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    if-eqz v11, :cond_c

    const/16 v1, 0x100

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v1}, Lcom/twitter/library/provider/az;->d(JI)V

    :cond_6
    :goto_4
    const-string/jumbo v1, "want_retweets"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    if-eqz v12, :cond_d

    const/16 v1, 0x200

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v1}, Lcom/twitter/library/provider/az;->d(JI)V

    :cond_7
    :goto_5
    return-object v13

    :cond_8
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "device"

    const-string/jumbo v13, "false"

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_9
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "lifeline"

    const-string/jumbo v13, "false"

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :cond_a
    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "retweets"

    const-string/jumbo v13, "false"

    invoke-direct {v6, v8, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_b
    const/16 v1, 0x10

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v1}, Lcom/twitter/library/provider/az;->e(JI)V

    const/16 v2, 0x10

    move-object/from16 v1, p2

    move-wide v5, v9

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(IJJ)V

    const/4 v1, 0x1

    iget-object v2, v7, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->b(ILjava/lang/String;)I

    goto :goto_3

    :cond_c
    const/16 v1, 0x100

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v1}, Lcom/twitter/library/provider/az;->e(JI)V

    goto :goto_4

    :cond_d
    const/16 v1, 0x200

    move-object/from16 v0, p2

    invoke-virtual {v0, v9, v10, v1}, Lcom/twitter/library/provider/az;->e(JI)V

    goto :goto_5
.end method

.method private k(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 19

    const-string/jumbo v1, "invite"

    move-object/from16 v0, p3

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v6

    array-length v7, v6

    const-string/jumbo v1, "status_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    const/16 v1, 0x64

    invoke-static {v7, v1}, Lcom/twitter/library/network/aa;->a(II)I

    move-result v10

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v11, "1.1"

    aput-object v11, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v11, "users"

    aput-object v11, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v11, "send_invites_by_email"

    aput-object v11, v4, v5

    invoke-static {v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ".json"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x0

    move v5, v1

    move-object v1, v2

    :goto_0
    if-ge v5, v10, :cond_4

    mul-int/lit8 v4, v5, 0x64

    add-int/lit8 v1, v4, 0x64

    invoke-static {v1, v7}, Ljava/lang/Math;->min(II)I

    move-result v14

    const/4 v1, 0x0

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->setLength(I)V

    move v2, v4

    :goto_1
    if-ge v2, v14, :cond_2

    aget-object v1, v6, v2

    check-cast v1, Lcom/twitter/library/api/TwitterContact;

    if-le v2, v4, :cond_0

    const-string/jumbo v15, ","

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    iget-object v15, v1, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    iget-object v0, v1, Lcom/twitter/library/api/TwitterContact;->b:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_1

    iget-object v1, v1, Lcom/twitter/library/api/TwitterContact;->b:Ljava/lang/String;

    invoke-virtual {v12, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_1
    const-string/jumbo v15, "\""

    invoke-virtual {v12, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v0, v1, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    move-object/from16 v16, v0

    const-string/jumbo v17, "\""

    const-string/jumbo v18, "\\\\\""

    invoke-virtual/range {v16 .. v18}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, "\""

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string/jumbo v16, " <"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    iget-object v1, v1, Lcom/twitter/library/api/TwitterContact;->b:Ljava/lang/String;

    invoke-virtual {v15, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v15, ">"

    invoke-virtual {v1, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    :cond_2
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "addresses"

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-wide/16 v1, 0x0

    cmp-long v1, v8, v1

    if-eqz v1, :cond_3

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "status_id"

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v2, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v13, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    new-instance v1, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v11}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v14, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v14, v15}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v13}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_6

    move-object v1, v2

    :goto_3
    invoke-virtual {v13}, Ljava/util/ArrayList;->clear()V

    add-int/lit8 v3, v5, 0x1

    move v5, v3

    move-object v3, v1

    move-object v1, v2

    goto/16 :goto_0

    :cond_4
    if-eqz v3, :cond_5

    :goto_4
    return-object v3

    :cond_5
    move-object v3, v1

    goto :goto_4

    :cond_6
    move-object v1, v3

    goto :goto_3
.end method

.method private k(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 15

    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    const-string/jumbo v1, "screen_name"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/16 v1, 0x11

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v7

    new-instance v8, Lcom/twitter/library/util/f;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v8, v1, v12, v13}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;J)V

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p3

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Ljava/lang/String;JLcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v14

    invoke-virtual {v14}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v11, v1

    check-cast v11, Lcom/twitter/library/api/TwitterUser;

    if-eqz v11, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iput-wide v1, v11, Lcom/twitter/library/api/TwitterUser;->lastUpdated:J

    new-instance v2, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v2, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-wide/16 v3, -0x1

    const/4 v5, -0x1

    const-wide/16 v6, -0x1

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x1

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v10}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    iget-wide v1, v11, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v1, v12, v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    iget-wide v8, v11, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-wide v6, v12

    move-object v10, v11

    invoke-static/range {v1 .. v10}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/network/aa;Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JJLcom/twitter/library/api/TwitterUser;)Lcom/twitter/internal/network/HttpOperation;

    :cond_0
    const-string/jumbo v1, "user"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v11}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    :goto_0
    return-object v14

    :cond_2
    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    const-string/jumbo v2, "custom_errors"

    invoke-static {v1}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_0
.end method

.method private l(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "activity"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "about_me"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "unread"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v2, "act_read_pos"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "cursor"

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v4, v5, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method private l(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v0, "status_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string/jumbo v0, "promoted_content"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_2

    iget-object v2, v0, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v1

    :goto_0
    iget-object v7, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v7, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v8, 0x4

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string/jumbo v10, "statuses"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "retweet"

    aput-object v10, v8, v9

    const/4 v9, 0x3

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, ".json"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    if-eqz v2, :cond_0

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "impression_id"

    invoke-direct {v7, v8, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_0

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "earned"

    const-string/jumbo v7, "true"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "send_error_codes"

    const-string/jumbo v7, "true"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "include_entities"

    const-string/jumbo v7, "true"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "include_media_features"

    const-string/jumbo v7, "true"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "include_cards"

    const-string/jumbo v7, "true"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v2, "earned_read"

    const-string/jumbo v7, "true"

    invoke-direct {v1, v2, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v1, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    const/4 v1, 0x2

    new-instance v2, Lcom/twitter/library/util/f;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v7, v3, v4, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v1, v2}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v7, v5}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v5, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterStatus;

    iget-object v5, v1, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    if-nez v5, :cond_1

    iput-object v0, v1, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    :cond_1
    invoke-virtual {p2, v1, v3, v4}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/TwitterStatus;J)V

    const-string/jumbo v0, "status_id"

    iget-wide v3, v1, Lcom/twitter/library/api/TwitterStatus;->a:J

    invoke-virtual {p4, v0, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :goto_1
    return-object v2

    :cond_2
    const/4 v2, 0x0

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "custom_errors"

    invoke-static {v0}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v0

    invoke-virtual {p4, v1, v0}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    goto :goto_1
.end method

.method private m(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "activity"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "about_me"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, "unread"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "cursor"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v1, 0x32

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    cmp-long v3, v0, v3

    if-gtz v3, :cond_1

    const-string/jumbo v3, "act_read_pos"

    invoke-virtual {p3, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    :goto_0
    return-object v2

    :cond_1
    const-string/jumbo v0, "act_read_pos"

    const-wide/16 v3, 0x0

    invoke-virtual {p3, v0, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    goto :goto_0
.end method

.method private m(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 10

    const/16 v9, 0xc8

    const/4 v8, 0x2

    const-string/jumbo v0, "status_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    iget-object v4, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "statuses"

    aput-object v7, v5, v6

    const-string/jumbo v6, "destroy"

    aput-object v6, v5, v8

    const/4 v6, 0x3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    new-instance v5, Lcom/twitter/library/util/f;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v2, v3, v7}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v8, v5}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v5

    new-instance v6, Lcom/twitter/library/network/d;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v7, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v6, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    sget-object v6, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v5}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {p2, v2, v3, v0}, Lcom/twitter/library/provider/az;->a(JLcom/twitter/library/api/TwitterStatus;)V

    :cond_0
    :goto_0
    return-object v4

    :cond_1
    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v5

    iget v6, v5, Lcom/twitter/internal/network/k;->a:I

    const/16 v7, 0x194

    if-ne v6, v7, :cond_0

    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/provider/az;->c(J)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v6

    if-eqz v6, :cond_2

    invoke-virtual {p2, v2, v3, v6}, Lcom/twitter/library/provider/az;->a(JLcom/twitter/library/api/TwitterStatus;)V

    iput v9, v5, Lcom/twitter/internal/network/k;->a:I

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/twitter/library/provider/az;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    iput v9, v5, Lcom/twitter/internal/network/k;->a:I

    goto :goto_0
.end method

.method private n(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "saved_searches"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "create"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "q"

    invoke-virtual {p4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v4}, Ljava/util/ArrayList;-><init>(I)V

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "query"

    invoke-direct {v3, v4, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    const/4 v1, 0x6

    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/provider/az;->b(Lcom/twitter/library/api/search/TwitterSearchQuery;I)J

    :cond_0
    return-object v2
.end method

.method private o(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    const-string/jumbo v0, "q"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x6

    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "1.1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "saved_searches"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v5, "destroy"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v3

    iget v4, v3, Lcom/twitter/internal/network/k;->a:I

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_0

    iget v3, v3, Lcom/twitter/internal/network/k;->a:I

    const/16 v4, 0x194

    if-ne v3, v4, :cond_1

    :cond_0
    invoke-virtual {p2, v0, v1}, Lcom/twitter/library/provider/az;->d(J)I

    :cond_1
    return-object v2
.end method

.method private p(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const-string/jumbo v0, "status_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-string/jumbo v0, "impression_id"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "earned"

    const/4 v4, 0x0

    invoke-virtual {p4, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    const-string/jumbo v0, "report_as"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, "block"

    const/4 v6, 0x0

    invoke-virtual {p4, v0, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const/4 v0, 0x0

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v7, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string/jumbo v10, "statuses"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "report_spam"

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "tweet_id"

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "block_user"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "report_as"

    invoke-direct {v8, v9, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v3, :cond_0

    const-string/jumbo v5, "impression_id"

    invoke-static {v7, v5, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v4, :cond_0

    const-string/jumbo v3, "earned"

    const/4 v4, 0x1

    invoke-static {v7, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    sget-object v4, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v3, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {p2, v1, v2}, Lcom/twitter/library/provider/az;->b(J)I

    :cond_1
    if-eqz v6, :cond_2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/library/client/u;->r(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    :cond_2
    return-object v0
.end method

.method private q(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 13

    iget-wide v2, p1, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v1, "user_id"

    const-wide/16 v4, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v1, "impression_id"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v6, "earned"

    const/4 v7, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    const-string/jumbo v7, "block"

    const/4 v8, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v7, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    iget-object v9, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v9, v9, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, "1.1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string/jumbo v12, "users"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string/jumbo v12, "report_spam"

    aput-object v12, v10, v11

    invoke-static {v9, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ".json"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "user_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v10, v11, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v1, :cond_0

    const-string/jumbo v4, "impression_id"

    invoke-static {v9, v4, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v6, :cond_0

    const-string/jumbo v1, "earned"

    const/4 v4, 0x1

    invoke-static {v9, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    const-string/jumbo v1, "perform_block"

    invoke-static {v9, v1, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v1, 0x11

    new-instance v4, Lcom/twitter/library/util/f;

    iget-object v5, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    iget-wide v10, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v10, v11, v6}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v1, v4}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v5, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v4, v5, v9}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v2, v3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v4

    sget-object v5, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v4

    move-object/from16 v0, p3

    invoke-virtual {v4, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v5, "screen_name"

    iget-object v6, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v7, :cond_1

    invoke-direct {p0, p2, v2, v3, v1}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/provider/az;JLcom/twitter/library/api/TwitterUser;)V

    :cond_1
    return-object v4
.end method

.method private r(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const-string/jumbo v0, "user_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-string/jumbo v2, "owner_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p4, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v4, "impression_id"

    invoke-virtual {p4, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "earned"

    const/4 v6, 0x0

    invoke-virtual {p4, v5, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v7, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v7, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string/jumbo v10, "blocks"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "create"

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "user_id"

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v9, v0}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v4, :cond_0

    const-string/jumbo v0, "impression_id"

    invoke-static {v7, v0, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v5, :cond_0

    const-string/jumbo v0, "earned"

    const/4 v1, 0x1

    invoke-static {v7, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    const/16 v0, 0x11

    new-instance v1, Lcom/twitter/library/util/f;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v4, v2, v3, v5}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v4, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v4, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v4, "screen_name"

    iget-object v5, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {p4, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p2, v2, v3, v0}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/provider/az;JLcom/twitter/library/api/TwitterUser;)V

    :cond_1
    return-object v1
.end method

.method private s(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const-string/jumbo v0, "user_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v0, "owner_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v0, "impression_id"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "earned"

    const/4 v6, 0x0

    invoke-virtual {p4, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iget-object v6, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v6, v6, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "blocks"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v9, "destroy"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "user_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v8, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    const-string/jumbo v8, "impression_id"

    invoke-static {v6, v8, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    if-eqz v1, :cond_0

    const-string/jumbo v0, "earned"

    const/4 v1, 0x1

    invoke-static {v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v0, v1, v6}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v8, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v8, v9}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v1, 0x2

    move-object v0, p2

    :try_start_0
    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IJJ)V

    const/4 v0, 0x4

    invoke-virtual {p2, v4, v5, v0}, Lcom/twitter/library/provider/az;->e(JI)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return-object v6

    :catch_0
    move-exception v0

    sget-boolean v1, Lcom/twitter/library/client/u;->e:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "LegacyRequest"

    const-string/jumbo v2, "destroyBlock"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private t(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 13

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v1, "name"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v1, "desc"

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v1, "list_mode"

    const/4 v6, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string/jumbo v1, "list_id"

    const-wide/16 v7, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    const-wide/16 v10, 0x0

    cmp-long v1, v7, v10

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v10, "lists"

    aput-object v10, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v10, "create"

    aput-object v10, v7, v8

    invoke-static {v1, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v7, ".json"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    :goto_0
    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "name"

    invoke-direct {v7, v8, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    packed-switch v6, :pswitch_data_0

    :goto_1
    if-eqz v5, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "description"

    invoke-direct {v2, v6, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    const/4 v2, 0x1

    const/4 v5, 0x1

    new-instance v6, Lcom/twitter/library/util/f;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v3, v4, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v5, v6}, Lcom/twitter/library/api/ao;->a(ZILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v6, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v5, v6, v7}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v5, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v9

    invoke-virtual {v9}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/twitter/library/api/am;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v2, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p2

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JILjava/lang/String;Z)V

    const-string/jumbo v1, "list_id"

    invoke-virtual {v8}, Lcom/twitter/library/api/am;->a()J

    move-result-wide v2

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_1
    return-object v9

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, "1.1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string/jumbo v12, "lists"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string/jumbo v12, "update"

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v10, ".json"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v10, "list_id"

    invoke-static {v1, v10, v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto/16 :goto_0

    :pswitch_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "mode"

    const-string/jumbo v7, "public"

    invoke-direct {v2, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    :pswitch_1
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "mode"

    const-string/jumbo v7, "private"

    invoke-direct {v2, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private u(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const-wide/16 v3, 0x0

    const-string/jumbo v0, "owner_id"

    invoke-virtual {p4, v0, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-string/jumbo v2, "list_id"

    invoke-virtual {p4, v2, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "lists"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "destroy"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "list_id"

    invoke-static {v4, v5, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v6, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v5, v6, v7}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v4

    sget-object v5, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/twitter/library/provider/az;->c(JJ)V

    :cond_0
    return-object v4
.end method

.method private v(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 17

    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    const-string/jumbo v1, "list_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    const-string/jumbo v1, "page"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v6

    const-string/jumbo v1, "user_type"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    const/4 v2, 0x1

    move-object/from16 v1, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v2

    packed-switch v3, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unknown user type: "

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "lists"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v9, "members"

    aput-object v9, v7, v8

    invoke-static {v1, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v7, ".json"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    :goto_0
    const-string/jumbo v7, "list_id"

    invoke-static {v1, v7, v11, v12}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v7, "include_user_entities"

    const/4 v8, 0x1

    invoke-static {v1, v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    if-eqz v2, :cond_0

    const-string/jumbo v7, "cursor"

    invoke-static {v1, v7, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const/16 v2, 0x15

    new-instance v7, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-wide v9, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v8, v9, v10, v13}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v7}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v7, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v7, v8, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    iget-wide v8, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v7, v8, v9}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/aj;

    if-nez v1, :cond_2

    invoke-virtual/range {v16 .. v16}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    const/4 v2, 0x0

    iput v2, v1, Lcom/twitter/internal/network/k;->a:I

    :cond_1
    :goto_1
    return-object v16

    :pswitch_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "lists"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v9, "subscribers"

    aput-object v9, v7, v8

    invoke-static {v1, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v7, ".json"

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    goto/16 :goto_0

    :cond_2
    invoke-virtual {v1}, Lcom/twitter/library/api/aj;->b()Ljava/util/ArrayList;

    move-result-object v7

    if-nez v6, :cond_3

    const-string/jumbo v13, "-1"

    :goto_2
    invoke-virtual {v1}, Lcom/twitter/library/api/aj;->a()Ljava/lang/String;

    move-result-object v14

    const/4 v15, 0x1

    move-object/from16 v6, p2

    move-wide v8, v4

    move v10, v3

    invoke-virtual/range {v6 .. v15}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v1

    const-string/jumbo v2, "count"

    move-object/from16 v0, p4

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_1

    :cond_3
    const/4 v13, 0x0

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private w(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 14

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v9

    const-string/jumbo v1, "user_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string/jumbo v1, "list_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    const-string/jumbo v1, "user_type"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    const/16 v1, 0x11

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v7

    const/4 v4, 0x0

    new-instance v8, Lcom/twitter/library/util/f;

    iget-object v1, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v8, v1, v9, v10}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;J)V

    move-object v1, p0

    move-object v2, p1

    move-object/from16 v3, p3

    invoke-direct/range {v1 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Ljava/lang/String;JLcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterUser;

    if-eqz v4, :cond_1

    packed-switch v13, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown user type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "1.1"

    aput-object v8, v3, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "lists"

    aput-object v8, v3, v7

    const/4 v7, 0x2

    const-string/jumbo v8, "members"

    aput-object v8, v3, v7

    const/4 v7, 0x3

    const-string/jumbo v8, "create"

    aput-object v8, v3, v7

    invoke-static {v1, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ".json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "list_id"

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v3, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "user_id"

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, v7, v5}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    const/4 v3, 0x1

    const/4 v5, 0x1

    new-instance v6, Lcom/twitter/library/util/f;

    iget-object v7, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v9, v10, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v3, v5, v6}, Lcom/twitter/library/api/ao;->a(ZILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v3

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v6, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v5, v6, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v6, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v5, v6, v7}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    sget-object v5, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v1, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/library/api/am;

    move-object/from16 v1, p2

    move-wide v2, v9

    move v5, v13

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/library/provider/az;->a(JLcom/twitter/library/api/TwitterUser;ILcom/twitter/library/api/am;)V

    :cond_0
    move-object v1, v7

    :goto_1
    return-object v1

    :pswitch_1
    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v3, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "lists"

    aput-object v6, v3, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "subscribers"

    aput-object v6, v3, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "create"

    aput-object v6, v3, v5

    invoke-static {v1, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ".json"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "list_id"

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v5, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private x(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 13

    const-string/jumbo v1, "owner_id"

    const-wide/16 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v1, "user_id"

    const-wide/16 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    const-string/jumbo v1, "list_id"

    const-wide/16 v7, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v7

    const-string/jumbo v1, "user_type"

    const/4 v2, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    packed-switch v2, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Unknown user type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, "1.1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string/jumbo v12, "lists"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string/jumbo v12, "members"

    aput-object v12, v10, v11

    const/4 v11, 0x3

    const-string/jumbo v12, "destroy"

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v10, ".json"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "list_id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v11, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v7, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "user_id"

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v8, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v7, v1

    :goto_0
    packed-switch v2, :pswitch_data_1

    const/4 v1, 0x0

    :goto_1
    const/4 v8, 0x1

    new-instance v10, Lcom/twitter/library/util/f;

    iget-object v11, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v3, v4, v12}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v8, v1, v10}, Lcom/twitter/library/api/ao;->a(ZILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v8, Lcom/twitter/library/network/d;

    iget-object v10, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v8, v10, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v10, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v8, v10, v11}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v7

    sget-object v8, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v7, v8}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v7

    move-object/from16 v0, p3

    invoke-virtual {v7, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7, v9}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/twitter/library/api/am;

    move-object v1, p2

    invoke-virtual/range {v1 .. v7}, Lcom/twitter/library/provider/az;->a(IJJLcom/twitter/library/api/am;)V

    :cond_0
    return-object v8

    :pswitch_1
    iget-object v1, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v10, 0x4

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-string/jumbo v12, "1.1"

    aput-object v12, v10, v11

    const/4 v11, 0x1

    const-string/jumbo v12, "lists"

    aput-object v12, v10, v11

    const/4 v11, 0x2

    const-string/jumbo v12, "subscribers"

    aput-object v12, v10, v11

    const/4 v11, 0x3

    const-string/jumbo v12, "destroy"

    aput-object v12, v10, v11

    invoke-static {v1, v10}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v10, ".json"

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "list_id"

    invoke-static {v7, v8}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v10, v11, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object v7, v1

    goto/16 :goto_0

    :pswitch_2
    const/4 v1, 0x1

    goto/16 :goto_1

    :pswitch_3
    const/4 v1, 0x2

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private y(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 11

    const/4 v5, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v7, 0x0

    const-string/jumbo v0, "user_id"

    const-wide/16 v1, 0x0

    invoke-virtual {p4, v0, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    const-string/jumbo v0, "list_id"

    const-wide/16 v3, 0x0

    invoke-virtual {p4, v0, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v0, "user_type"

    invoke-virtual {p4, v0, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v1, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unknown user type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/Object;

    const-string/jumbo v6, "1.1"

    aput-object v6, v5, v7

    const-string/jumbo v6, "lists"

    aput-object v6, v5, v8

    const-string/jumbo v6, "members"

    aput-object v6, v5, v9

    const-string/jumbo v6, "show"

    aput-object v6, v5, v10

    invoke-static {v0, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ".json"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    :goto_0
    const-string/jumbo v5, "user_id"

    invoke-static {v0, v5, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v1, "list_id"

    invoke-static {v0, v1, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v5, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v5, v6}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2, v3, v4, v8}, Lcom/twitter/library/provider/az;->a(JZ)V

    :cond_0
    :goto_1
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v5, v5, [Ljava/lang/Object;

    const-string/jumbo v6, "1.1"

    aput-object v6, v5, v7

    const-string/jumbo v6, "lists"

    aput-object v6, v5, v8

    const-string/jumbo v6, "subscribers"

    aput-object v6, v5, v9

    const-string/jumbo v6, "show"

    aput-object v6, v5, v10

    invoke-static {v0, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ".json"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    iget v1, v1, Lcom/twitter/internal/network/k;->a:I

    const/16 v2, 0x194

    if-ne v1, v2, :cond_0

    invoke-virtual {p2, v3, v4, v7}, Lcom/twitter/library/provider/az;->a(JZ)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private z(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "trends"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "available"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "lang"

    invoke-virtual {p4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "lang"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v1, "locale"

    invoke-virtual {p4, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string/jumbo v2, "country"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    const/16 v1, 0x28

    invoke-static {v1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, p1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p2, v0}, Lcom/twitter/library/provider/az;->d(Ljava/util/ArrayList;)I

    const-string/jumbo v1, "locations"

    invoke-virtual {p4, v1, v0}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_2
    return-object v2
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/TwitterUser;)Lcom/twitter/library/client/u;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/client/u;->f:Lcom/twitter/library/api/TwitterUser;

    return-object p0
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 28

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/client/u;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    iget-wide v0, v3, Lcom/twitter/library/service/p;->c:J

    move-wide/from16 v26, v0

    new-instance v5, Lcom/twitter/library/network/n;

    iget-object v2, v3, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v5, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iget-object v6, v3, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/client/u;->r()I

    move-result v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/library/client/u;->k:Landroid/os/Bundle;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-wide/from16 v0, v26

    invoke-static {v2, v0, v1}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-wide/from16 v0, v26

    invoke-static {v2, v0, v1}, Lcom/twitter/library/provider/c;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/c;

    move-result-object v2

    const/4 v11, 0x0

    sparse-switch v8, :sswitch_data_0

    :cond_0
    move-object v2, v11

    :cond_1
    :goto_0
    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    :cond_2
    return-void

    :sswitch_0
    const-string/jumbo v2, "act_read_pos"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    const-string/jumbo v5, "i_type"

    const/4 v7, 0x0

    invoke-virtual {v9, v5, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v5

    const-string/jumbo v7, "clear_all_notif"

    invoke-virtual {v9, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v2, 0x0

    invoke-virtual {v4, v5, v2}, Lcom/twitter/library/provider/az;->b(I[J)V

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v2

    invoke-virtual {v4, v5}, Lcom/twitter/library/provider/az;->f(I)I

    move-result v3

    const-string/jumbo v4, "unread_interactions"

    invoke-virtual {v2, v6, v4, v3}, Lcom/twitter/library/provider/f;->b(Ljava/lang/String;Ljava/lang/String;I)I

    const-string/jumbo v2, "count"

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v2, v11

    goto :goto_0

    :cond_3
    const/4 v7, 0x1

    invoke-virtual {v4, v5, v2, v3, v7}, Lcom/twitter/library/provider/az;->a(IJZ)I

    goto :goto_1

    :sswitch_1
    const-string/jumbo v2, "owner_id"

    const-wide/16 v5, 0x0

    invoke-virtual {v9, v2, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Lcom/twitter/library/provider/az;->i(J)V

    move-object v2, v11

    goto :goto_0

    :sswitch_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v9, v2}, Lcom/twitter/library/client/u;->b(Landroid/os/Bundle;Landroid/content/Context;)V

    invoke-static {v9, v2}, Lcom/twitter/library/client/u;->a(Landroid/os/Bundle;Landroid/content/Context;)V

    move-object v2, v11

    goto :goto_0

    :sswitch_3
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->d(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto :goto_0

    :sswitch_4
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->e(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto :goto_0

    :sswitch_5
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->f(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto :goto_0

    :sswitch_6
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/client/u;->f()V

    goto :goto_0

    :sswitch_7
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->i(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_8
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->j(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_9
    const-string/jumbo v2, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JILandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_a
    const-string/jumbo v2, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JILandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_b
    const-string/jumbo v2, "user_id"

    const-wide/16 v5, 0x0

    invoke-virtual {v9, v2, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v5, "unread_tweet"

    const/4 v6, 0x0

    invoke-virtual {v4, v2, v3, v6}, Lcom/twitter/library/provider/az;->j(JI)I

    move-result v6

    invoke-virtual {v9, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v5, "unread_mention"

    const/4 v6, 0x5

    invoke-virtual {v4, v2, v3, v6}, Lcom/twitter/library/provider/az;->j(JI)I

    move-result v2

    invoke-virtual {v9, v5, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "unread_dm"

    invoke-virtual {v4}, Lcom/twitter/library/provider/az;->g()I

    move-result v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_c
    const-string/jumbo v2, "message_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v8, 0x1

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JZ)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_d
    const-string/jumbo v2, "user_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    move-object/from16 v2, p0

    move-object v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JLandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_e
    const-wide/16 v6, 0x0

    move-object/from16 v2, p0

    move-object v8, v9

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JLandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_f
    const-string/jumbo v2, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    const-string/jumbo v2, "user_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    const/4 v15, 0x0

    move-object v8, v3

    move-object v9, v4

    move-object v10, v5

    invoke-static/range {v6 .. v15}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/network/aa;Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JJLcom/twitter/library/api/TwitterUser;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_10
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->k(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_11
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->l(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_12
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->m(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_13
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->q(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_14
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->r(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_15
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->f(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_16
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->s(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_17
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->p(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_18
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->t(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_19
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->u(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_1a
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->v(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_1b
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->w(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_1c
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->x(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_1d
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->y(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_1e
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/provider/az;Landroid/os/Bundle;)V

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_1f
    const/4 v2, 0x0

    invoke-virtual {v4, v2}, Lcom/twitter/library/provider/az;->a(I)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_20
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->j(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_21
    const-string/jumbo v3, "_data"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v8

    check-cast v8, Lcom/twitter/library/api/TweetEntities;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-wide/from16 v0, v26

    invoke-static {v3, v0, v1, v8}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;JLcom/twitter/library/api/TweetEntities;)Z

    const-string/jumbo v3, "impression_id"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_5

    new-instance v10, Lcom/twitter/library/api/PromotedContent;

    invoke-direct {v10}, Lcom/twitter/library/api/PromotedContent;-><init>()V

    iput-object v3, v10, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    const-string/jumbo v3, "earned"

    const/4 v4, 0x0

    invoke-virtual {v9, v3, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_4

    const-string/jumbo v3, "earned"

    iput-object v3, v10, Lcom/twitter/library/api/PromotedContent;->disclosureType:Ljava/lang/String;

    :cond_4
    :goto_2
    const-string/jumbo v3, "status_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v9, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const-string/jumbo v5, "android.intent.extra.TEXT"

    invoke-virtual {v9, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v6, "android.intent.extra.UID"

    const-wide/16 v12, 0x0

    invoke-virtual {v9, v6, v12, v13}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v9, 0x0

    invoke-virtual/range {v2 .. v10}, Lcom/twitter/library/provider/c;->a(JLjava/lang/String;JLcom/twitter/library/api/TweetEntities;ILcom/twitter/library/api/PromotedContent;)J

    move-object v2, v11

    goto/16 :goto_0

    :cond_5
    const/4 v10, 0x0

    goto :goto_2

    :sswitch_22
    const-string/jumbo v3, "status_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v9, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    const/4 v5, 0x1

    new-array v5, v5, [J

    const/4 v6, 0x0

    aput-wide v3, v5, v6

    invoke-virtual {v2, v5}, Lcom/twitter/library/provider/c;->a([J)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_23
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "saved_searches"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "list"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/4 v3, 0x4

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v7, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v5, 0x6

    invoke-virtual {v4, v2, v5}, Lcom/twitter/library/provider/az;->a(Ljava/util/ArrayList;I)I

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_24
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "users"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "suggestions"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "lang"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v6, "locale"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_6

    const-string/jumbo v7, "lang"

    invoke-static {v2, v7, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "country"

    invoke-static {v2, v3, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    const-string/jumbo v3, "show_members"

    const/4 v6, 0x1

    invoke-static {v2, v3, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v3, "max_members"

    const/4 v6, 0x6

    invoke-static {v2, v3, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const/4 v3, 0x5

    new-instance v6, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-wide/from16 v0, v26

    invoke-direct {v6, v7, v0, v1, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v3, v6}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v3

    new-instance v6, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v7, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v14

    invoke-virtual {v14}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_2c

    invoke-virtual {v3}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const-string/jumbo v3, "count"

    const/4 v5, 0x7

    invoke-virtual {v4, v2, v5}, Lcom/twitter/library/provider/az;->a(Ljava/util/ArrayList;I)I

    move-result v5

    invoke-virtual {v9, v3, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_7
    :goto_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_8

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iget-object v7, v3, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    if-eqz v7, :cond_7

    iget-object v3, v3, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    :cond_8
    const-string/jumbo v3, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    const/4 v8, -0x1

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    invoke-virtual/range {v4 .. v13}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    const/4 v9, 0x0

    :goto_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v9, v3, :cond_a

    invoke-virtual {v2, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iget-object v5, v3, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    if-eqz v5, :cond_9

    iget-object v5, v3, Lcom/twitter/library/api/search/TwitterSearchQuery;->i:Ljava/util/ArrayList;

    const/4 v8, 0x6

    iget-object v3, v3, Lcom/twitter/library/api/search/TwitterSearchQuery;->b:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v10, v3

    const/4 v12, 0x0

    invoke-virtual/range {v4 .. v12}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIIJLjava/lang/String;)V

    :cond_9
    add-int/lit8 v9, v9, 0x1

    goto :goto_4

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/y;->a:Landroid/net/Uri;

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;)V

    move-object v2, v14

    goto/16 :goto_0

    :sswitch_25
    const-string/jumbo v2, "q"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v6, 0x4

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "1.1"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "users"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string/jumbo v8, "suggestions"

    aput-object v8, v6, v7

    const/4 v7, 0x3

    aput-object v3, v6, v7

    invoke-static {v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ".json"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "include_user_entities"

    const/4 v7, 0x1

    invoke-static {v2, v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v6, "locale"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string/jumbo v7, "lang"

    invoke-virtual {v9, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_b

    const-string/jumbo v8, "lang"

    invoke-static {v2, v8, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v7, "country"

    invoke-static {v2, v7, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "localize"

    const/4 v7, 0x1

    invoke-static {v2, v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_b
    const/4 v6, 0x6

    new-instance v7, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    move-wide/from16 v0, v26

    invoke-direct {v7, v8, v0, v1, v10}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v6, v7}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v7, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v7, v8, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v7, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v5

    if-eqz v5, :cond_1

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    if-nez v11, :cond_c

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v3

    const/4 v4, 0x0

    iput v4, v3, Lcom/twitter/internal/network/k;->a:I

    goto/16 :goto_0

    :cond_c
    const-string/jumbo v5, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v5, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    const/4 v14, 0x6

    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v3

    int-to-long v15, v3

    const-string/jumbo v17, "-1"

    const-string/jumbo v18, "0"

    const/16 v19, 0x1

    move-object v10, v4

    invoke-virtual/range {v10 .. v19}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v3

    const-string/jumbo v4, "count"

    invoke-virtual {v9, v4, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto/16 :goto_0

    :sswitch_26
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->g(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_27
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->h(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_28
    const-string/jumbo v2, "page"

    const/4 v6, 0x0

    invoke-virtual {v9, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v6, "user_id"

    const-wide/16 v7, 0x0

    invoke-virtual {v9, v6, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    const-string/jumbo v6, "user_type"

    const/4 v7, -0x1

    invoke-virtual {v9, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v14

    const-string/jumbo v6, "persist_data"

    const/4 v7, 0x1

    invoke-virtual {v9, v6, v7}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    if-eqz v15, :cond_d

    const/16 v6, 0xa

    if-eq v14, v6, :cond_d

    const/16 v6, 0x9

    if-eq v14, v6, :cond_d

    const/16 v6, 0x13

    if-eq v14, v6, :cond_d

    const/16 v6, 0x14

    if-eq v14, v6, :cond_d

    const/16 v6, 0x15

    if-eq v14, v6, :cond_d

    const/16 v6, 0x16

    if-eq v14, v6, :cond_d

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Unsupported type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_d
    const/4 v6, 0x7

    invoke-static {v6}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v10

    new-instance v11, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    move-wide/from16 v0, v26

    invoke-direct {v11, v6, v0, v1}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;J)V

    move-object/from16 v6, p0

    move-object v7, v3

    move-object v8, v5

    invoke-direct/range {v6 .. v11}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;Lcom/twitter/library/api/ao;Lcom/twitter/library/util/f;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v5

    if-eqz v5, :cond_29

    invoke-virtual {v10}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    const-wide/16 v5, 0x0

    cmp-long v5, v12, v5

    if-lez v5, :cond_e

    :goto_5
    if-eqz v15, :cond_10

    const-wide/16 v15, -0x1

    if-nez v2, :cond_f

    const-string/jumbo v17, "-1"

    :goto_6
    const/16 v18, 0x0

    const/16 v19, 0x1

    move-object v10, v4

    invoke-virtual/range {v10 .. v19}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    move-result v2

    :goto_7
    const-string/jumbo v4, "count"

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v4, "users"

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Landroid/os/Parcelable;

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    move-object v2, v3

    goto/16 :goto_0

    :cond_e
    move-wide/from16 v12, v26

    goto :goto_5

    :cond_f
    const/16 v17, 0x0

    goto :goto_6

    :cond_10
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v2

    goto :goto_7

    :sswitch_29
    const-string/jumbo v2, "user_type"

    const/4 v6, -0x1

    invoke-virtual {v9, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v3, "Unsupported type"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :sswitch_2a
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->b(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_2b
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->c(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_2c
    const-string/jumbo v2, "screen_name"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "q"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "1.1"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "lists"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string/jumbo v8, "show"

    aput-object v8, v6, v7

    invoke-static {v4, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, ".json"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v6, "slug"

    invoke-static {v4, v6, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v3, "owner_screen_name"

    invoke-static {v4, v3, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x1

    const/4 v3, 0x0

    new-instance v6, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-wide/from16 v0, v26

    invoke-direct {v6, v7, v0, v1, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3, v6}, Lcom/twitter/library/api/ao;->a(ZILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v6, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/am;

    const-string/jumbo v4, "list_id"

    invoke-virtual {v2}, Lcom/twitter/library/api/am;->a()J

    move-result-wide v5

    invoke-virtual {v9, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v4, "owner_id"

    iget-object v5, v2, Lcom/twitter/library/api/am;->g:Lcom/twitter/library/api/TwitterUser;

    iget-wide v5, v5, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v9, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v4, "name"

    iget-object v2, v2, Lcom/twitter/library/api/am;->a:Ljava/lang/String;

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_2d
    const-string/jumbo v2, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string/jumbo v2, "email"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v16

    const-string/jumbo v2, "phone"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v17

    const-string/jumbo v2, "user_id"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v18

    const-string/jumbo v2, "i_type"

    const/4 v6, 0x7

    invoke-virtual {v9, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v19

    const-string/jumbo v2, "list_id"

    const-wide/16 v6, -0x1

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v20

    const-string/jumbo v2, "map_contacts"

    const/4 v6, 0x0

    invoke-virtual {v9, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v25

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v10, p0

    move-object v11, v3

    move-object v12, v5

    move-object v13, v4

    move-object/from16 v24, v9

    invoke-direct/range {v10 .. v25}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Lcom/twitter/library/provider/az;J[Ljava/lang/String;[Ljava/lang/String;[JIJLjava/lang/String;ZLandroid/os/Bundle;Z)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_2e
    const-string/jumbo v2, "owner_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v14

    const-string/jumbo v2, "i_type"

    const/4 v6, 0x7

    invoke-virtual {v9, v2, v6}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v16

    const-string/jumbo v2, "list_id"

    const-wide/16 v6, -0x1

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v17

    move-object/from16 v10, p0

    move-object v11, v3

    move-object v12, v5

    move-object v13, v4

    move-object/from16 v19, v9

    invoke-direct/range {v10 .. v19}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Lcom/twitter/library/provider/az;JIJLandroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_2f
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->k(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_30
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->h(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_31
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->g(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_32
    const-string/jumbo v2, "user_id"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v9

    array-length v2, v9

    const/16 v6, 0x64

    invoke-static {v2, v6}, Lcom/twitter/library/network/aa;->a(II)I

    move-result v10

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x0

    move v8, v2

    move v2, v6

    move v6, v7

    move-object v7, v11

    :goto_8
    if-ge v8, v10, :cond_12

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v7, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v11, 0x3

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    const-string/jumbo v13, "1.1"

    aput-object v13, v11, v12

    const/4 v12, 0x1

    const-string/jumbo v13, "friendships"

    aput-object v13, v11, v12

    const/4 v12, 0x2

    const-string/jumbo v13, "create_all"

    aput-object v13, v11, v12

    invoke-static {v7, v11}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v11, ".json"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v11, "user_id"

    mul-int/lit8 v12, v8, 0x64

    const/16 v13, 0x64

    invoke-static {v7, v11, v9, v12, v13}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    const/16 v11, 0x9

    invoke-static {v11}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v12

    new-instance v11, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v11, v13, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v11, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v7

    sget-object v11, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v7, v11}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7, v12}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v11

    invoke-virtual {v11}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v7

    if-eqz v7, :cond_11

    invoke-virtual {v12}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const/4 v7, 0x1

    invoke-virtual {v4, v2, v7}, Lcom/twitter/library/provider/az;->b(Ljava/util/Collection;I)V

    const/4 v2, 0x1

    new-instance v7, Lcom/twitter/library/api/search/a;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v7, v12, v3, v9}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;[J)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Lcom/twitter/library/client/u;->b(Lcom/twitter/internal/android/service/a;)V

    :goto_9
    add-int/lit8 v7, v8, 0x1

    move v8, v7

    move-object v7, v11

    goto :goto_8

    :cond_11
    const/4 v6, 0x1

    goto :goto_9

    :cond_12
    if-eqz v6, :cond_13

    if-eqz v2, :cond_13

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    const/16 v3, 0x1a2

    iput v3, v2, Lcom/twitter/internal/network/k;->a:I

    :cond_13
    invoke-direct/range {p0 .. p0}, Lcom/twitter/library/client/u;->f()V

    move-object v2, v7

    goto/16 :goto_0

    :sswitch_33
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->d(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_34
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->e(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_35
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_36
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_37
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->c(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_38
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->b(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_39
    const-string/jumbo v2, "thread_id"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    array-length v6, v2

    if-lez v6, :cond_0

    invoke-virtual {v4, v2}, Lcom/twitter/library/provider/az;->c([Ljava/lang/String;)[Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v2}, Lcom/twitter/library/provider/az;->b([Ljava/lang/String;)I

    array-length v13, v12

    const/4 v2, 0x0

    move v10, v2

    :goto_a
    if-ge v10, v13, :cond_14

    aget-object v2, v12, v10

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JZ)Lcom/twitter/internal/network/HttpOperation;

    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_a

    :cond_14
    invoke-virtual {v4}, Lcom/twitter/library/provider/az;->g()I

    move-result v2

    const-string/jumbo v3, "unread_dm"

    invoke-virtual {v9, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_3a
    const-string/jumbo v2, "message_id"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v10

    if-eqz v10, :cond_0

    array-length v2, v10

    if-lez v2, :cond_0

    array-length v12, v10

    const/4 v2, 0x0

    move v9, v2

    :goto_b
    if-ge v9, v12, :cond_15

    aget-wide v6, v10, v9

    const/4 v8, 0x0

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;JZ)Lcom/twitter/internal/network/HttpOperation;

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_b

    :cond_15
    invoke-virtual {v4, v10}, Lcom/twitter/library/provider/az;->c([J)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_3b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->c:Ljava/lang/String;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v5, "decider"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v5, "android"

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0xc

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v4

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v5, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/Decider;

    const-string/jumbo v4, "settings"

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_3c
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->n(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_3d
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->o(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_3e
    move-object/from16 v2, p0

    move-object v6, v9

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_3f
    move-object/from16 v2, p0

    move-object v6, v9

    invoke-direct/range {v2 .. v7}, Lcom/twitter/library/client/u;->b(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_40
    const-string/jumbo v2, "max_id"

    const-wide/16 v10, 0x0

    invoke-virtual {v9, v2, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v17

    const-string/jumbo v2, "since_id"

    const-wide/16 v10, 0x0

    invoke-virtual {v9, v2, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v19

    const-string/jumbo v2, "status_id"

    const-wide/16 v10, 0x0

    invoke-virtual {v9, v2, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v15

    const-string/jumbo v2, "user_id"

    const-wide/16 v10, 0x0

    invoke-virtual {v9, v2, v10, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v12

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v6, v8

    const/4 v8, 0x1

    const-string/jumbo v10, "conversation"

    aput-object v10, v6, v8

    const/4 v8, 0x2

    const-string/jumbo v10, "show"

    aput-object v10, v6, v8

    invoke-static {v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ".json"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "id"

    move-wide v0, v15

    invoke-static {v2, v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v6, "include_entities"

    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v6, "count"

    const/16 v8, 0x14

    invoke-static {v2, v6, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v6, "include_media_features"

    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v6, "include_cards"

    const/4 v8, 0x1

    invoke-static {v2, v6, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v6, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    const-wide/16 v10, 0x0

    cmp-long v6, v17, v10

    if-eqz v6, :cond_16

    const-string/jumbo v6, "max_id"

    move-wide/from16 v0, v17

    invoke-static {v2, v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_16
    const-wide/16 v10, 0x0

    cmp-long v6, v19, v10

    if-eqz v6, :cond_17

    const-string/jumbo v6, "since_id"

    move-wide/from16 v0, v19

    invoke-static {v2, v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_17
    const/4 v6, 0x3

    new-instance v8, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    move-wide/from16 v0, v26

    invoke-direct {v8, v10, v0, v1, v11}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v6, v8}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v8, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v8, v10, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v8, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_1c

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/ArrayList;

    const-wide/16 v21, 0x0

    cmp-long v2, v17, v21

    if-nez v2, :cond_1a

    const-wide/16 v17, 0x0

    cmp-long v2, v19, v17

    if-nez v2, :cond_1a

    const/4 v2, 0x1

    move v8, v2

    :goto_c
    const/4 v6, 0x0

    if-eqz v8, :cond_2b

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_18
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterStatus;->a()J

    move-result-wide v17

    cmp-long v2, v15, v17

    if-nez v2, :cond_18

    const/4 v2, 0x1

    :goto_d
    if-nez v2, :cond_19

    if-nez v8, :cond_1b

    :cond_19
    const/16 v14, 0x15

    const/16 v17, 0x1

    const/16 v18, 0x0

    const/16 v19, 0x0

    const/16 v20, 0x0

    const/16 v21, 0x1

    const/16 v22, 0x1

    const/16 v23, 0x1

    move-object v10, v4

    invoke-virtual/range {v10 .. v23}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZZZ)Ljava/util/Collection;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Collection;->size()I

    move-result v2

    add-int/2addr v2, v3

    :goto_e
    const-string/jumbo v3, "new_tweet"

    invoke-virtual {v9, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v3, "scribe_item_count"

    invoke-virtual {v7, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    move-object v2, v5

    goto/16 :goto_0

    :cond_1a
    const/4 v2, 0x0

    move v8, v2

    goto :goto_c

    :cond_1b
    invoke-virtual {v5}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v2

    const/4 v4, 0x0

    iput v4, v2, Lcom/twitter/internal/network/k;->a:I

    :cond_1c
    move v2, v3

    goto :goto_e

    :sswitch_41
    const-string/jumbo v2, "email"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "lang"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "i"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "users"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "email_available"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "email"

    invoke-static {v4, v5, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1d

    const-string/jumbo v2, "lang"

    invoke-static {v4, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1d
    const/16 v2, 0xd

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v5, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/c;

    iget-boolean v4, v2, Lcom/twitter/library/api/c;->a:Z

    if-nez v4, :cond_1e

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v4

    const/16 v5, 0x190

    iput v5, v4, Lcom/twitter/internal/network/k;->a:I

    iget-object v2, v2, Lcom/twitter/library/api/c;->b:Ljava/lang/String;

    iput-object v2, v4, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    :cond_1e
    move-object v2, v3

    goto/16 :goto_0

    :sswitch_42
    const-string/jumbo v2, "screen_name"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "lang"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v4, v4, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "i"

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "users"

    aput-object v7, v5, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "username_available"

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ".json"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "custom"

    const/4 v6, 0x1

    invoke-static {v4, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    const-string/jumbo v5, "context"

    const-string/jumbo v6, "signup"

    invoke-static {v4, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v5, "username"

    invoke-static {v4, v5, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1f

    const-string/jumbo v2, "lang"

    invoke-static {v4, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1f
    const/16 v2, 0xd

    invoke-static {v2}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v5, v4}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_29

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/c;

    iget-boolean v4, v2, Lcom/twitter/library/api/c;->a:Z

    if-nez v4, :cond_20

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v4

    const/16 v5, 0x190

    iput v5, v4, Lcom/twitter/internal/network/k;->a:I

    iget-object v2, v2, Lcom/twitter/library/api/c;->b:Ljava/lang/String;

    iput-object v2, v4, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    :cond_20
    move-object v2, v3

    goto/16 :goto_0

    :sswitch_43
    const-string/jumbo v2, "status_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v2, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v11

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "statuses"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    invoke-static {v11, v12}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v3, v6

    const/4 v6, 0x3

    const-string/jumbo v7, "activity"

    aput-object v7, v3, v6

    const/4 v6, 0x4

    const-string/jumbo v7, "summary"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "include_user_entities"

    const/4 v6, 0x1

    invoke-static {v2, v3, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-static {v9}, Ljs;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_21

    const/4 v6, 0x1

    invoke-static {v2, v3, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_21
    const/16 v3, 0xe

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v7, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/ActivitySummary;

    :try_start_0
    iget-object v5, v2, Lcom/twitter/library/api/ActivitySummary;->a:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    iget-object v5, v2, Lcom/twitter/library/api/ActivitySummary;->b:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v14

    iget v15, v2, Lcom/twitter/library/api/ActivitySummary;->c:I

    move-object v10, v4

    invoke-virtual/range {v10 .. v15}, Lcom/twitter/library/provider/az;->a(JIII)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_f
    const-string/jumbo v4, "q"

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_44
    const-string/jumbo v2, "q"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/twitter/library/provider/az;->b([J)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_45
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->i(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_46
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->A(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_47
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-string/jumbo v8, "1.1"

    aput-object v8, v6, v7

    const/4 v7, 0x1

    const-string/jumbo v8, "friendships"

    aput-object v8, v6, v7

    const/4 v7, 0x2

    const-string/jumbo v8, "incoming"

    aput-object v8, v6, v7

    invoke-static {v2, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, ".json"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "page"

    const/4 v7, 0x0

    invoke-virtual {v9, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v15

    const-string/jumbo v6, "owner_id"

    const-wide/16 v7, 0x0

    invoke-virtual {v9, v6, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v13

    const/4 v11, 0x1

    const/16 v12, 0x12

    move-object v10, v4

    invoke-virtual/range {v10 .. v15}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_22

    const-string/jumbo v7, "cursor"

    invoke-static {v2, v7, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_22
    const/16 v6, 0xf

    invoke-static {v6}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v7, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v7, v8, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v7, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_2a

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/aj;

    invoke-virtual {v2}, Lcom/twitter/library/api/aj;->b()Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_24

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_24

    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_10
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_23

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Long;

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v11

    const/16 v6, 0x20

    invoke-virtual {v4, v11, v12, v6}, Lcom/twitter/library/provider/az;->c(JI)V

    goto :goto_10

    :cond_23
    const/16 v16, 0x0

    const/16 v17, 0x0

    invoke-static {v8}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v18

    const/16 v19, 0x12

    const-wide/16 v20, -0x1

    invoke-virtual {v2}, Lcom/twitter/library/api/aj;->a()Ljava/lang/String;

    move-result-object v22

    const/16 v23, 0x1

    const/16 v24, 0x0

    const/16 v25, 0x0

    move-object/from16 v10, p0

    move-object v11, v3

    move-object v12, v5

    move-object v13, v4

    move-wide/from16 v14, v26

    invoke-direct/range {v10 .. v25}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Lcom/twitter/library/provider/az;J[Ljava/lang/String;[Ljava/lang/String;[JIJLjava/lang/String;ZLandroid/os/Bundle;Z)Lcom/twitter/internal/network/HttpOperation;

    const-string/jumbo v2, "users"

    invoke-static {v8}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_24
    move-object v2, v7

    goto/16 :goto_0

    :sswitch_48
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "friendships"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "accept"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const/16 v7, 0x12

    move-object v6, v4

    move-wide/from16 v8, v26

    invoke-virtual/range {v6 .. v11}, Lcom/twitter/library/provider/az;->a(IJJ)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v7, "user_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v6, 0x11

    new-instance v7, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    move-wide/from16 v0, v26

    invoke-direct {v7, v8, v0, v1, v9}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v6, v7}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v7, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v7, v8, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v7, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v7, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    if-eqz v2, :cond_25

    new-instance v5, Ljava/util/ArrayList;

    const/4 v6, 0x1

    invoke-direct {v5, v6}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/4 v8, 0x1

    const-wide/16 v9, -0x1

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x1

    move-wide/from16 v6, v26

    invoke-virtual/range {v4 .. v13}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_25
    move-object v2, v3

    goto/16 :goto_0

    :sswitch_49
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "friendships"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "deny"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    const-wide/16 v6, 0x0

    invoke-virtual {v9, v3, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v10

    const/16 v7, 0x12

    move-object v6, v4

    move-wide/from16 v8, v26

    invoke-virtual/range {v6 .. v11}, Lcom/twitter/library/provider/az;->a(IJJ)V

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "user_id"

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x11

    new-instance v6, Lcom/twitter/library/util/f;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    move-wide/from16 v0, v26

    invoke-direct {v6, v7, v0, v1, v8}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v4, v6}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v4

    new-instance v6, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v6, v7, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v6, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v6, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_4a
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v9}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_4b
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v4, v5, v9}, Lcom/twitter/library/client/u;->z(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_4c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    const-string/jumbo v3, "message_id"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/library/platform/PushService;->e(Landroid/content/Context;Ljava/lang/String;)V

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_4d
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    const-string/jumbo v2, "account"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    const-string/jumbo v4, "settings"

    invoke-virtual {v9, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-static {v3, v2, v4}, Lcom/twitter/library/platform/PushService;->a(Landroid/content/Context;Landroid/accounts/Account;I)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_4e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    const-string/jumbo v2, "account"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/accounts/Account;

    invoke-static {v3, v2}, Lcom/twitter/library/platform/PushService;->e(Landroid/content/Context;Landroid/accounts/Account;)V

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_4f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "account"

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "login_verification_enrollment"

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const/16 v3, 0x2e

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9}, Lcom/twitter/library/client/u;->a(Ljava/lang/StringBuilder;Landroid/os/Bundle;)V

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v6, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_26

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/account/LvEligibilityResponse;

    const-string/jumbo v4, "login_verification_enrolled"

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v2, v3

    goto/16 :goto_0

    :cond_26
    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const-string/jumbo v4, "custom_errors"

    invoke-static {v2}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v2

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    const-string/jumbo v2, "login_verification_enrolled"

    const/4 v4, 0x0

    invoke-virtual {v9, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_50
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "account"

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "login_verification_enrollment"

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "lv_puvlic_key"

    invoke-virtual {v9, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "lv_offline_code"

    invoke-virtual {v9, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "lv_offline_code_count"

    const-wide/16 v7, 0x0

    invoke-virtual {v9, v6, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x2b

    invoke-static {v7}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v10, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v11, "public_key"

    invoke-direct {v10, v11, v3}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v10, "offline_code"

    invoke-direct {v3, v10, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "iteration_count"

    invoke-direct {v3, v4, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "udid"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v6}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "token"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-static {v6}, Lcom/twitter/library/platform/PushService;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "update_push_destination"

    const-string/jumbo v6, "true"

    invoke-direct {v3, v4, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v3

    if-eqz v3, :cond_27

    new-instance v3, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "environment"

    const/4 v6, 0x2

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_27
    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-nez v2, :cond_29

    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const-string/jumbo v4, "custom_errors"

    invoke-static {v2}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v2

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_51
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "account"

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "login_verification_enrollment"

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v9}, Lcom/twitter/library/client/u;->a(Ljava/lang/StringBuilder;Landroid/os/Bundle;)V

    const/16 v3, 0x2b

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v4

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v6, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->d:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-nez v2, :cond_29

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    const-string/jumbo v4, "custom_errors"

    invoke-static {v2}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v2

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_52
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    const-string/jumbo v6, "i"

    aput-object v6, v3, v4

    const/4 v4, 0x1

    const-string/jumbo v6, "account"

    aput-object v6, v3, v4

    const/4 v4, 0x2

    const-string/jumbo v6, "change_password"

    aput-object v6, v3, v4

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-string/jumbo v4, "old_password"

    invoke-virtual {v9, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v6, "new_password"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const/16 v7, 0x40

    invoke-static {v7}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v7

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v10, "current_password"

    invoke-direct {v8, v10, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "password"

    invoke-direct {v4, v8, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "password_confirmation"

    invoke-direct {v4, v8, v6}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v4, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v4, v6, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v3}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v7}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-static {v2}, Lcom/twitter/library/client/u;->c(Ljava/util/ArrayList;)[I

    move-result-object v2

    array-length v4, v2

    if-lez v4, :cond_29

    const-string/jumbo v4, "custom_errors"

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    move-object v2, v3

    goto/16 :goto_0

    :sswitch_53
    const-string/jumbo v2, "user_id"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v7

    const-string/jumbo v2, "notify_content_provider"

    const/4 v6, 0x1

    invoke-virtual {v9, v2, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v8

    move-object/from16 v2, p0

    move-object v6, v9

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/client/u;->a(Lcom/twitter/library/service/p;Lcom/twitter/library/provider/az;Lcom/twitter/library/network/a;Landroid/os/Bundle;[JZ)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_54
    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v9}, Lcom/twitter/library/client/u;->a(Landroid/os/Bundle;)V

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_55
    const-string/jumbo v2, "entity_id"

    const-wide/16 v5, 0x0

    invoke-virtual {v9, v2, v5, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v4, v2, v3}, Lcom/twitter/library/provider/az;->q(J)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_56
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->m(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_57
    move-object/from16 v0, p0

    invoke-direct {v0, v3, v5, v9}, Lcom/twitter/library/client/u;->l(Lcom/twitter/library/service/p;Lcom/twitter/library/network/a;Landroid/os/Bundle;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    goto/16 :goto_0

    :sswitch_58
    const-string/jumbo v2, "q"

    invoke-virtual {v9, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/twitter/library/provider/az;->c(Ljava/lang/String;)I

    move-object v2, v11

    goto/16 :goto_0

    :sswitch_59
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/client/u;->m:Lcom/twitter/library/network/aa;

    iget-object v2, v2, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v6, 0x0

    const-string/jumbo v7, "1.1"

    aput-object v7, v3, v6

    const/4 v6, 0x1

    const-string/jumbo v7, "translations"

    aput-object v7, v3, v6

    const/4 v6, 0x2

    const-string/jumbo v7, "show"

    aput-object v7, v3, v6

    invoke-static {v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ".json"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "id"

    const-string/jumbo v6, "status_id"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v2, v3, v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v3, "dest"

    const-string/jumbo v6, "lang"

    invoke-virtual {v9, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v3, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const/16 v3, 0x38

    invoke-static {v3}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v6

    new-instance v3, Lcom/twitter/library/network/d;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/client/u;->l:Landroid/content/Context;

    invoke-direct {v3, v7, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    move-wide/from16 v0, v26

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_29

    invoke-virtual {v6}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterStatus$Translation;

    if-eqz v2, :cond_28

    iget-wide v5, v2, Lcom/twitter/library/api/TwitterStatus$Translation;->a:J

    iget-object v7, v2, Lcom/twitter/library/api/TwitterStatus$Translation;->b:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7}, Lcom/twitter/library/provider/az;->a(JLjava/lang/String;)V

    const-string/jumbo v4, "translated"

    invoke-virtual {v9, v4, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_28
    move-object v2, v3

    goto/16 :goto_0

    :catch_0
    move-exception v4

    goto/16 :goto_f

    :cond_29
    move-object v2, v3

    goto/16 :goto_0

    :cond_2a
    move-object v2, v7

    goto/16 :goto_0

    :cond_2b
    move v2, v6

    goto/16 :goto_d

    :cond_2c
    move-object v2, v14

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_e
        0x4 -> :sswitch_1
        0x7 -> :sswitch_3
        0x8 -> :sswitch_4
        0xc -> :sswitch_5
        0xd -> :sswitch_55
        0xe -> :sswitch_6
        0xf -> :sswitch_7
        0x10 -> :sswitch_9
        0x11 -> :sswitch_a
        0x12 -> :sswitch_b
        0x14 -> :sswitch_c
        0x16 -> :sswitch_f
        0x17 -> :sswitch_10
        0x19 -> :sswitch_11
        0x1a -> :sswitch_12
        0x1b -> :sswitch_14
        0x1c -> :sswitch_13
        0x1d -> :sswitch_16
        0x1e -> :sswitch_18
        0x1f -> :sswitch_18
        0x20 -> :sswitch_19
        0x22 -> :sswitch_1a
        0x23 -> :sswitch_1b
        0x24 -> :sswitch_1c
        0x25 -> :sswitch_1d
        0x28 -> :sswitch_1e
        0x2f -> :sswitch_21
        0x30 -> :sswitch_22
        0x32 -> :sswitch_23
        0x33 -> :sswitch_24
        0x34 -> :sswitch_25
        0x35 -> :sswitch_26
        0x36 -> :sswitch_27
        0x37 -> :sswitch_2c
        0x38 -> :sswitch_2d
        0x3a -> :sswitch_32
        0x3b -> :sswitch_33
        0x3c -> :sswitch_36
        0x3d -> :sswitch_38
        0x3e -> :sswitch_3a
        0x3f -> :sswitch_3b
        0x40 -> :sswitch_3c
        0x41 -> :sswitch_3d
        0x42 -> :sswitch_3e
        0x43 -> :sswitch_3f
        0x44 -> :sswitch_40
        0x45 -> :sswitch_28
        0x46 -> :sswitch_41
        0x47 -> :sswitch_42
        0x48 -> :sswitch_43
        0x49 -> :sswitch_44
        0x4c -> :sswitch_45
        0x4d -> :sswitch_8
        0x4f -> :sswitch_47
        0x50 -> :sswitch_48
        0x51 -> :sswitch_49
        0x53 -> :sswitch_4a
        0x54 -> :sswitch_2f
        0x56 -> :sswitch_29
        0x57 -> :sswitch_46
        0x59 -> :sswitch_4b
        0x5d -> :sswitch_4c
        0x5e -> :sswitch_4d
        0x5f -> :sswitch_4e
        0x61 -> :sswitch_50
        0x62 -> :sswitch_51
        0x66 -> :sswitch_4f
        0x69 -> :sswitch_d
        0x6a -> :sswitch_0
        0x6b -> :sswitch_1f
        0x6c -> :sswitch_15
        0x6e -> :sswitch_39
        0x6f -> :sswitch_20
        0x70 -> :sswitch_17
        0x74 -> :sswitch_2
        0x75 -> :sswitch_53
        0x76 -> :sswitch_56
        0x77 -> :sswitch_57
        0x78 -> :sswitch_30
        0x79 -> :sswitch_31
        0x7a -> :sswitch_2e
        0x7e -> :sswitch_58
        0x7f -> :sswitch_59
        0x80 -> :sswitch_8
        0x81 -> :sswitch_34
        0x82 -> :sswitch_52
        0x83 -> :sswitch_37
        0x84 -> :sswitch_35
        0x3e7 -> :sswitch_54
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x9 -> :sswitch_2a
        0xa -> :sswitch_2b
        0x13 -> :sswitch_2b
        0x14 -> :sswitch_2b
        0x16 -> :sswitch_2b
    .end sparse-switch
.end method
