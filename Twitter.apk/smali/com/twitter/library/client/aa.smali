.class public Lcom/twitter/library/client/aa;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Z

.field private static b:Lcom/twitter/library/client/aa;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Ljava/util/HashMap;

.field private final e:Ljava/util/ArrayList;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/library/client/w;

.field private final h:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SessionManager"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/client/aa;->a:Z

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/library/client/aa;->b:Lcom/twitter/library/client/aa;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/client/aa;->h:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    return-void
.end method

.method private a(Landroid/accounts/Account;)Lcom/twitter/library/client/Session;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v4

    const-string/jumbo v0, "account_user_info"

    invoke-virtual {v4, p1, v0}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {p0}, Lcom/twitter/library/client/aa;->d()Lcom/twitter/library/client/Session;

    move-result-object v1

    if-eqz v5, :cond_0

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    :try_start_0
    invoke-virtual {v4, p1}, Landroid/accounts/AccountManager;->getPassword(Landroid/accounts/Account;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/Session;->a(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/Session;->a(Z)V

    const/4 v0, 0x0

    invoke-virtual {p0, v1, v2, v3, v0}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;

    :cond_0
    :goto_1
    return-object v1

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    invoke-static {v4, p1}, Lcom/twitter/library/util/a;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v3

    const-string/jumbo v0, "account_settings"

    invoke-virtual {v4, p1, v0}, Landroid/accounts/AccountManager;->getUserData(Landroid/accounts/Account;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v3, :cond_0

    invoke-static {v5}, Lcom/twitter/library/api/ap;->b(Ljava/lang/String;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    invoke-static {v0}, Lcom/twitter/library/api/UserSettings;->a(Ljava/lang/String;)Lcom/twitter/library/api/UserSettings;

    move-result-object v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/UserSettings;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Landroid/accounts/Account;J)Lcom/twitter/library/client/Session;
    .locals 7

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    move v2, v0

    :goto_0
    const-wide/16 v3, 0x0

    cmp-long v3, p3, v3

    if-lez v3, :cond_0

    move v1, v0

    :cond_0
    if-nez v2, :cond_1

    if-eqz v1, :cond_7

    :cond_1
    iget-object v3, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    if-eqz v2, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    :cond_3
    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v5, p3, v5

    if-nez v5, :cond_2

    :cond_4
    monitor-exit v3

    :goto_1
    return-object v0

    :cond_5
    move v2, v1

    goto :goto_0

    :cond_6
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez p2, :cond_7

    if-eqz v2, :cond_8

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object p2

    :cond_7
    :goto_2
    if-nez p2, :cond_9

    invoke-direct {p0}, Lcom/twitter/library/client/aa;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_8
    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0, p3, p4}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;J)Landroid/accounts/Account;

    move-result-object p2

    goto :goto_2

    :cond_9
    invoke-direct {p0, p2}, Lcom/twitter/library/client/aa;->a(Landroid/accounts/Account;)Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/library/client/aa;
    .locals 3

    const-class v1, Lcom/twitter/library/client/aa;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/library/client/aa;->b:Lcom/twitter/library/client/aa;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/client/aa;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/twitter/library/client/aa;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/library/client/aa;->b:Lcom/twitter/library/client/aa;

    :cond_0
    sget-object v0, Lcom/twitter/library/client/aa;->b:Lcom/twitter/library/client/aa;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    iget-object v3, p2, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    iget-object v2, p3, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/UserSettings;)V

    invoke-static {p3}, Lcom/twitter/library/api/ap;->a(Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v2, v0, v1, v3}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p1, v7}, Lcom/twitter/library/client/Session;->a(Z)V

    :goto_0
    iget-object v4, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-ne v0, v6, :cond_1

    move v1, v6

    :goto_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_2
    if-ltz v2, :cond_2

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/z;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/client/z;->b(Lcom/twitter/library/client/Session;Z)V

    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_2

    :cond_0
    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v0, v1, v6}, Lcom/twitter/library/util/a;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    goto :goto_0

    :cond_1
    move v1, v7

    goto :goto_1

    :cond_2
    return-object v3
.end method

.method static synthetic a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/network/LoginResponse;Lcom/twitter/library/api/TwitterUser;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 2

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/UserSettings;)V
    .locals 1

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    invoke-virtual {p1, p4}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {p1, p2}, Lcom/twitter/library/client/Session;->a(Ljava/lang/String;)V

    invoke-virtual {p1, p3}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/network/OAuthToken;)V

    if-eqz p5, :cond_0

    invoke-virtual {p1, p5}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/UserSettings;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/aa;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/library/client/aa;Ljava/lang/String;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/client/aa;->f(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/client/aa;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/aa;->h:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/library/client/aa;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/library/client/aa;->a:Z

    return v0
.end method

.method private d()Lcom/twitter/library/client/Session;
    .locals 7

    iget-object v1, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v3, v4, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_0

    monitor-exit v1

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/twitter/library/client/Session;

    invoke-direct {v0}, Lcom/twitter/library/client/Session;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v3, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method static synthetic d(Lcom/twitter/library/client/aa;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/client/aa;->e()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/library/client/aa;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    return-object v0
.end method

.method private e()V
    .locals 2

    new-instance v0, Lcom/twitter/library/util/z;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/twitter/library/util/z;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/twitter/library/util/z;->a()V

    return-void
.end method

.method private f(Ljava/lang/String;)Z
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    const-wide/16 v1, 0x0

    invoke-static {v0, v3, v1, v2}, Lcom/twitter/library/provider/cn;->a(Landroid/content/Context;Ljava/lang/String;J)V

    iput-object v3, p0, Lcom/twitter/library/client/aa;->f:Ljava/lang/String;

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v0

    array-length v0, v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    return v0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lcom/twitter/library/client/Session;
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0, p1, p2}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Landroid/accounts/Account;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/client/Session;
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/client/aa;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/client/aa;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->c:Lcom/twitter/library/client/Session$LoginStatus;

    if-eq v0, v1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->b()Lcom/twitter/library/client/Session$LoginStatus;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    if-ne v0, v1, :cond_1

    :cond_0
    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->d:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    iget-object v0, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v1, Lcom/twitter/library/api/account/o;

    iget-object v2, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-direct {v1, v2, p1}, Lcom/twitter/library/api/account/o;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    new-instance v2, Lcom/twitter/library/client/ai;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/ai;-><init>(Lcom/twitter/library/client/aa;I)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/account/o;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;Lcom/twitter/library/client/ac;)Ljava/lang/String;
    .locals 6

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    new-instance v0, Lcom/twitter/library/api/account/c;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/account/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/library/api/account/c;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p5}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/ab;

    invoke-direct {v2, p0}, Lcom/twitter/library/client/ab;-><init>(Lcom/twitter/library/client/aa;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/c;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ac;)Ljava/lang/String;
    .locals 6

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    new-instance v0, Lcom/twitter/library/api/account/c;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    move-object v2, p1

    move-wide v3, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/account/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/library/api/account/c;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p6}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p5}, Lcom/twitter/library/api/account/c;->a(Ljava/lang/String;)Lcom/twitter/library/api/account/c;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/client/ab;

    invoke-direct {v2, p0}, Lcom/twitter/library/client/ab;-><init>(Lcom/twitter/library/client/aa;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/c;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ag;)Ljava/lang/String;
    .locals 8

    new-instance v0, Lcom/twitter/library/api/account/q;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    move-object v2, p5

    move-wide v3, p2

    move-object v5, p4

    move-object v6, p6

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/account/q;-><init>(Landroid/content/Context;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iget-object v1, v0, Lcom/twitter/library/api/account/q;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p7}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/af;

    const/4 v3, 0x3

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/af;-><init>(Lcom/twitter/library/client/aa;I)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/q;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Lcom/twitter/library/client/ag;)Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    new-instance v0, Lcom/twitter/library/api/account/s;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/library/api/account/s;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/library/api/account/s;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p3}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/af;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/af;-><init>(Lcom/twitter/library/client/aa;I)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/s;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    new-instance v0, Lcom/twitter/library/api/account/i;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/twitter/library/api/account/i;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/library/api/account/i;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p4}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/ad;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/ad;-><init>(Lcom/twitter/library/client/aa;I)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/i;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/twitter/library/client/ak;)Ljava/lang/String;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/a;->b(Landroid/content/Context;)I

    move-result v0

    if-lez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/client/aa;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    new-instance v2, Lcom/twitter/library/api/account/u;

    iget-object v3, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-direct {v2, v3, v0, p2, p4}, Lcom/twitter/library/api/account/u;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Lcom/twitter/library/api/account/u;->c(Ljava/lang/String;)Lcom/twitter/library/api/account/u;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/twitter/library/api/account/u;->d(Ljava/lang/String;)Lcom/twitter/library/api/account/u;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/twitter/library/api/account/u;->e(Ljava/lang/String;)Lcom/twitter/library/api/account/u;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/twitter/library/api/account/u;->b(Ljava/lang/String;)Lcom/twitter/library/api/account/u;

    move-result-object v0

    invoke-virtual {v0, p8}, Lcom/twitter/library/api/account/u;->a(Z)Lcom/twitter/library/api/account/u;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/twitter/library/api/account/u;->b(Z)Lcom/twitter/library/api/account/u;

    move-result-object v0

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/account/u;->a(Ljava/lang/String;)Lcom/twitter/library/api/account/u;

    :cond_0
    iget-object v1, v0, Lcom/twitter/library/api/account/u;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p9}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/aj;

    invoke-direct {v2, p0}, Lcom/twitter/library/client/aj;-><init>(Lcom/twitter/library/client/aa;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/u;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    goto :goto_0
.end method

.method public a()Ljava/util/ArrayList;
    .locals 3

    iget-object v1, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/twitter/library/client/z;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V
    .locals 1

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public b()Lcom/twitter/library/client/Session;
    .locals 8

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v4

    array-length v0, v4

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/provider/cn;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x0

    if-eqz v5, :cond_4

    array-length v6, v4

    :goto_0
    if-ge v2, v6, :cond_4

    aget-object v0, v4, v2

    iget-object v7, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1

    :goto_1
    if-nez v0, :cond_0

    const/4 v0, 0x0

    aget-object v0, v4, v0

    :cond_0
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-wide/16 v4, 0x0

    invoke-direct {p0, v1, v0, v4, v5}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Landroid/accounts/Account;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/Session;)V

    monitor-exit v3

    :goto_2
    return-object v0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/twitter/library/client/aa;->d()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/Session;)V

    :cond_3
    monitor-exit v3

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/client/Session;
    .locals 3

    const/4 v0, 0x0

    const-wide/16 v1, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Landroid/accounts/Account;J)Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v1, Lcom/twitter/library/api/account/v;

    iget-object v2, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    new-instance v3, Lcom/twitter/library/network/n;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-direct {v1, v2, p1, v3}, Lcom/twitter/library/api/account/v;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/n;)V

    new-instance v2, Lcom/twitter/library/client/ai;

    const/4 v3, 0x2

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/ai;-><init>(Lcom/twitter/library/client/aa;I)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/account/v;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;
    .locals 4

    sget-object v0, Lcom/twitter/library/client/Session$LoginStatus;->b:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {p1, v0}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    new-instance v0, Lcom/twitter/library/api/account/j;

    iget-object v1, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p1, p2, p3}, Lcom/twitter/library/api/account/j;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, v0, Lcom/twitter/library/api/account/j;->a:Ljava/lang/String;

    invoke-virtual {p0, v1, p4}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    iget-object v1, p0, Lcom/twitter/library/client/aa;->g:Lcom/twitter/library/client/w;

    new-instance v2, Lcom/twitter/library/client/ad;

    const/4 v3, 0x1

    invoke-direct {v2, p0, v3}, Lcom/twitter/library/client/ad;-><init>(Lcom/twitter/library/client/aa;I)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/account/j;->a(Lcom/twitter/internal/android/service/m;)Lcom/twitter/internal/android/service/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0, p1, p2, p3}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ae;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/twitter/library/client/z;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/client/Session;
    .locals 2

    iget-object v1, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/Session;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public c(Lcom/twitter/library/client/Session;)Z
    .locals 4

    const/4 v0, 0x1

    if-nez p1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/twitter/library/client/aa;->d:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public d(Lcom/twitter/library/client/Session;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/client/aa;->f:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/client/aa;->c:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/provider/cn;->a(Landroid/content/Context;Ljava/lang/String;J)V

    iget-object v2, p0, Lcom/twitter/library/client/aa;->e:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/z;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/z;->a(Lcom/twitter/library/client/Session;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/client/aa;->d(Lcom/twitter/library/client/Session;)V

    :cond_0
    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/client/aa;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method
