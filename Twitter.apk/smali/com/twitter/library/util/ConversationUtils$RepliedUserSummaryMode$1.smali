.class final enum Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode$1;
.super Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;
.source "Twttr"


# direct methods
.method constructor <init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;-><init>(Ljava/lang/String;ILcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;Lcom/twitter/library/util/d;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/res/Resources;Ljava/util/ArrayList;J)Ljava/lang/String;
    .locals 5

    const/4 v2, 0x1

    const/4 v4, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-array v1, v2, [Ljava/lang/String;

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    invoke-virtual {v0}, Lcom/twitter/library/api/RepliedUser;->a()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v4

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    new-array v2, v2, [I

    sget v3, Lil;->convo_to_reply:I

    aput v3, v2, v4

    sget v3, Lij;->replied_users_and_count:I

    invoke-static {p1, v1, v0, v2, v3}, Lcom/twitter/library/util/ConversationUtils;->a(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
