.class public Lcom/twitter/library/util/ConversationUtils;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static synthetic a(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1, p2, p3, p4}, Lcom/twitter/library/util/ConversationUtils;->b(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b(Landroid/content/res/Resources;[Ljava/lang/String;I[II)Ljava/lang/String;
    .locals 3

    const/4 v2, 0x0

    if-gtz p2, :cond_0

    array-length v0, p1

    add-int/lit8 v0, v0, -0x1

    aget v0, p3, v0

    check-cast p1, [Ljava/lang/Object;

    invoke-virtual {p0, v0, p1}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    array-length v0, p1

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/Object;

    array-length v1, p1

    invoke-static {p1, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v1, p1

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, p4, p2, v0}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
