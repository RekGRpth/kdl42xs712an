.class public Lcom/twitter/library/util/g;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;
.implements Ljava/lang/Iterable;
.implements Ljava/util/Iterator;


# instance fields
.field private a:Lcom/twitter/library/util/i;

.field private b:Ljava/io/RandomAccessFile;

.field private c:J

.field private d:J

.field private e:J

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/io/RandomAccessFile;JJ)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/util/g;->b:Ljava/io/RandomAccessFile;

    iput-wide p2, p0, Lcom/twitter/library/util/g;->c:J

    iput-wide p4, p0, Lcom/twitter/library/util/g;->d:J

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/twitter/library/util/g;->e:J

    return-void
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    return-void
.end method


# virtual methods
.method public a()Ljava/io/InputStream;
    .locals 8

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/util/g;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    move-object v0, v7

    :goto_0
    return-object v0

    :cond_0
    iget-wide v2, p0, Lcom/twitter/library/util/g;->e:J

    iget-wide v0, p0, Lcom/twitter/library/util/g;->d:J

    add-long/2addr v0, v2

    iget-wide v4, p0, Lcom/twitter/library/util/g;->c:J

    cmp-long v0, v0, v4

    if-ltz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/library/util/g;->c:J

    sub-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/util/g;->d:J

    :cond_1
    iget-wide v0, p0, Lcom/twitter/library/util/g;->e:J

    iget-wide v4, p0, Lcom/twitter/library/util/g;->d:J

    add-long/2addr v0, v4

    iput-wide v0, p0, Lcom/twitter/library/util/g;->e:J

    :try_start_0
    invoke-direct {p0}, Lcom/twitter/library/util/g;->e()V

    new-instance v0, Lcom/twitter/library/util/i;

    iget-object v1, p0, Lcom/twitter/library/util/g;->b:Ljava/io/RandomAccessFile;

    iget-wide v4, p0, Lcom/twitter/library/util/g;->d:J

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/util/i;-><init>(Ljava/io/RandomAccessFile;JJLcom/twitter/library/util/h;)V

    iput-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    iget-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/io/InputStream;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/g;->f:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/util/g;->b()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    iget-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    goto :goto_0

    :catch_0
    move-exception v0

    iput-object v7, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    goto :goto_1
.end method

.method public b()Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    invoke-static {v0}, Lcom/twitter/library/util/i;->a(Lcom/twitter/library/util/i;)V

    iget-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    return-object v0
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/util/g;->d:J

    return-wide v0
.end method

.method public close()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/util/g;->e()V

    return-void
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/g;->a:Lcom/twitter/library/util/i;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/g;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasNext()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/util/g;->e:J

    iget-wide v2, p0, Lcom/twitter/library/util/g;->c:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 0

    return-object p0
.end method

.method public synthetic next()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/util/g;->a()Ljava/io/InputStream;

    move-result-object v0

    return-object v0
.end method

.method public remove()V
    .locals 0

    return-void
.end method
