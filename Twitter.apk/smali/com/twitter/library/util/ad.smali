.class public abstract Lcom/twitter/library/util/ad;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/HashSet;

.field private static final b:Ljava/util/HashSet;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/twitter/library/util/ad;->a:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/twitter/library/util/ad;->b:Ljava/util/HashSet;

    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "bg"

    aput-object v1, v0, v3

    const-string/jumbo v1, "ca"

    aput-object v1, v0, v4

    const-string/jumbo v1, "zh"

    aput-object v1, v0, v5

    const-string/jumbo v1, "cs"

    aput-object v1, v0, v6

    const-string/jumbo v1, "da"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "nl"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "en"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "et"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "fi"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "fr"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "el"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "ht"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "hi"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "hu"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "id"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "ja"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "ko"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "lv"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "lt"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "no"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "pt"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "ro"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "ru"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "sk"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "sl"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "es"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "sv"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "th"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "tr"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "uk"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "vi"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "ar"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "fa"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "he"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "en-gb"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "da"

    aput-object v2, v1, v3

    const-string/jumbo v2, "de"

    aput-object v2, v1, v4

    const-string/jumbo v2, "en"

    aput-object v2, v1, v5

    const-string/jumbo v2, "es"

    aput-object v2, v1, v6

    const-string/jumbo v2, "et"

    aput-object v2, v1, v7

    const/4 v2, 0x5

    const-string/jumbo v3, "fi"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "fr"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "ht"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "hu"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "id"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "is"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "tr"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "vi"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "en-gb"

    aput-object v3, v1, v2

    const/16 v2, 0xe

    const-string/jumbo v3, "it"

    aput-object v3, v1, v2

    const/16 v2, 0xf

    const-string/jumbo v3, "lt"

    aput-object v3, v1, v2

    const/16 v2, 0x10

    const-string/jumbo v3, "lv"

    aput-object v3, v1, v2

    const/16 v2, 0x11

    const-string/jumbo v3, "nl"

    aput-object v3, v1, v2

    const/16 v2, 0x12

    const-string/jumbo v3, "no"

    aput-object v3, v1, v2

    const/16 v2, 0x13

    const-string/jumbo v3, "pl"

    aput-object v3, v1, v2

    const/16 v2, 0x14

    const-string/jumbo v3, "pt"

    aput-object v3, v1, v2

    const/16 v2, 0x15

    const-string/jumbo v3, "sk"

    aput-object v3, v1, v2

    const/16 v2, 0x16

    const-string/jumbo v3, "sl"

    aput-object v3, v1, v2

    const/16 v2, 0x17

    const-string/jumbo v3, "sv"

    aput-object v3, v1, v2

    const/16 v2, 0x18

    const-string/jumbo v3, "tl"

    aput-object v3, v1, v2

    sget-object v2, Lcom/twitter/library/util/ad;->a:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    sget-object v0, Lcom/twitter/library/util/ad;->b:Ljava/util/HashSet;

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p0}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    new-instance v1, Ljava/util/Locale;

    invoke-direct {v1, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/Locale;->getDisplayLanguage(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/twitter/library/util/ad;->a:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    sget-object v0, Lcom/twitter/library/util/ad;->b:Ljava/util/HashSet;

    invoke-virtual {v0, p0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
