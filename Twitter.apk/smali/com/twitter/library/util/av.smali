.class public Lcom/twitter/library/util/av;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private a:Landroid/widget/ImageView;

.field private b:F

.field private c:F

.field private d:Landroid/view/animation/AlphaAnimation;

.field private e:Landroid/view/animation/AlphaAnimation;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;FFI)V
    .locals 5

    const/4 v4, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    iput p2, p0, Lcom/twitter/library/util/av;->b:F

    iput p3, p0, Lcom/twitter/library/util/av;->c:F

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p2, p3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    int-to-long v1, p4

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v0, p0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v0, p0, Lcom/twitter/library/util/av;->e:Landroid/view/animation/AlphaAnimation;

    new-instance v1, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v1, p3, p2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    int-to-long v2, p4

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    invoke-virtual {v1, p0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object v1, p0, Lcom/twitter/library/util/av;->d:Landroid/view/animation/AlphaAnimation;

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-ge v2, v3, :cond_0

    invoke-virtual {v1, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {v0, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/library/util/av;->e:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/twitter/library/util/av;->d:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/av;->e:Landroid/view/animation/AlphaAnimation;

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    iget v1, p0, Lcom/twitter/library/util/av;->c:F

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/util/av;->d:Landroid/view/animation/AlphaAnimation;

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/av;->a:Landroid/widget/ImageView;

    iget v1, p0, Lcom/twitter/library/util/av;->b:F

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
