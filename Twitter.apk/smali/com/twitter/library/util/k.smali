.class public Lcom/twitter/library/util/k;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method private static a(Ljava/util/ArrayList;II)I
    .locals 11

    const/4 v6, 0x0

    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetMediaFeature;

    iget v1, v0, Lcom/twitter/library/api/TweetMediaFeature;->w:I

    iget v2, v0, Lcom/twitter/library/api/TweetMediaFeature;->h:I

    mul-int/2addr v1, v2

    int-to-long v1, v1

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-object v3, v0

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetMediaFeature;

    iget v4, v0, Lcom/twitter/library/api/TweetMediaFeature;->w:I

    iget v5, v0, Lcom/twitter/library/api/TweetMediaFeature;->h:I

    mul-int/2addr v4, v5

    int-to-long v4, v4

    cmp-long v8, v4, v1

    if-lez v8, :cond_5

    move-object v2, v0

    move-wide v0, v4

    :goto_1
    move-object v3, v2

    move-wide v9, v0

    move-wide v1, v9

    goto :goto_0

    :cond_0
    iget v0, v3, Lcom/twitter/library/api/TweetMediaFeature;->y:I

    iget v1, v3, Lcom/twitter/library/api/TweetMediaFeature;->h:I

    int-to-float v1, v1

    const v2, 0x3ec28f5c    # 0.38f

    mul-float/2addr v1, v2

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    add-int/2addr v0, v1

    div-int/lit8 v1, p2, 0x3

    mul-int/lit8 v2, p2, 0x2

    div-int/lit8 v2, v2, 0x3

    if-le v0, v1, :cond_4

    sub-int v3, p1, v0

    if-le v3, v2, :cond_1

    sub-int/2addr v0, v1

    :goto_2
    if-ltz v0, :cond_3

    :goto_3
    return v0

    :cond_1
    sub-int v3, p1, v0

    if-le v3, v1, :cond_2

    sub-int/2addr v0, v2

    goto :goto_2

    :cond_2
    sub-int v0, p1, p2

    goto :goto_2

    :cond_3
    move v0, v6

    goto :goto_3

    :cond_4
    move v0, v6

    goto :goto_2

    :cond_5
    move-wide v9, v1

    move-wide v0, v9

    move-object v2, v3

    goto :goto_1
.end method

.method public static a(IIIILjava/util/HashMap;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;ZZ)Landroid/graphics/Bitmap;
    .locals 9

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v4

    invoke-virtual {p5}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    if-eqz p8, :cond_1

    mul-int v2, v4, p3

    mul-int v3, p2, v5

    if-le v2, v3, :cond_0

    int-to-float v2, p3

    int-to-float v3, v5

    div-float/2addr v2, v3

    :goto_0
    invoke-virtual {p6, v2, v2}, Landroid/graphics/Matrix;->setScale(FF)V

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    int-to-float v1, v1

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p6, v1, v0}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    return-object p5

    :cond_0
    int-to-float v2, p2

    int-to-float v3, v4

    div-float/2addr v2, v3

    goto :goto_0

    :cond_1
    mul-int v2, v4, p3

    mul-int v3, p2, v5

    if-le v2, v3, :cond_2

    int-to-float v1, p3

    int-to-float v2, v5

    div-float v3, v1, v2

    int-to-float v1, p2

    int-to-float v2, v4

    mul-float/2addr v2, v3

    sub-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v2, v1

    move v1, v0

    :goto_1
    if-eqz p4, :cond_3

    const-string/jumbo v0, "large"

    invoke-virtual {p4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz p7, :cond_3

    const/4 v6, 0x0

    cmpl-float v6, v1, v6

    if-eqz v6, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    if-lez v6, :cond_3

    int-to-float v4, v4

    int-to-float v6, p0

    div-float/2addr v4, v6

    int-to-float v4, v5

    int-to-float v5, p1

    div-float/2addr v4, v5

    int-to-float v5, p3

    mul-float v6, v3, v4

    div-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->round(F)I

    move-result v5

    invoke-static {v0, p1, v5}, Lcom/twitter/library/util/k;->a(Ljava/util/ArrayList;II)I

    move-result v0

    if-ltz v0, :cond_3

    const-wide/16 v5, -0x1

    int-to-double v0, v0

    float-to-double v7, v4

    mul-double/2addr v0, v7

    float-to-double v7, v3

    mul-double/2addr v0, v7

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    mul-long/2addr v0, v5

    long-to-float v0, v0

    move v1, v2

    move v2, v3

    goto :goto_0

    :cond_2
    int-to-float v0, p2

    int-to-float v2, v4

    div-float v3, v0, v2

    int-to-float v0, p3

    int-to-float v2, v5

    mul-float/2addr v2, v3

    sub-float/2addr v0, v2

    const/high16 v2, 0x3f000000    # 0.5f

    mul-float/2addr v0, v2

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_3
    move v0, v1

    move v1, v2

    move v2, v3

    goto :goto_0
.end method
