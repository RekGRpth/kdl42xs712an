.class public Lcom/twitter/library/util/c;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:J

.field private b:Z

.field private c:J


# direct methods
.method public constructor <init>(J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/util/c;->c:J

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/util/c;->a:J

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/util/c;->b:Z

    return-void
.end method

.method public a()Z
    .locals 4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/library/util/c;->a:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/twitter/library/util/c;->c:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/util/c;->b:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/util/c;->b:Z

    return-void
.end method
