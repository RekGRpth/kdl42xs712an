.class public Lcom/twitter/library/util/aa;
.super Lcom/twitter/library/util/ag;
.source "Twttr"


# static fields
.field private static final g:[I

.field private static h:Lld;

.field private static i:Z


# instance fields
.field private final j:Ljava/util/ArrayList;

.field private final k:I

.field private l:Lcom/twitter/library/network/n;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [I

    sput-object v0, Lcom/twitter/library/util/aa;->g:[I

    sput-boolean v1, Lcom/twitter/library/util/aa;->i:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;II)V
    .locals 1

    invoke-direct {p0, p1, p3}, Lcom/twitter/library/util/ag;-><init>(Landroid/content/Context;I)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/util/aa;->l:Lcom/twitter/library/network/n;

    iput p2, p0, Lcom/twitter/library/util/aa;->k:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/util/aa;->j:Ljava/util/ArrayList;

    return-void
.end method

.method private static a(Landroid/content/Context;)Lld;
    .locals 7

    sget-object v1, Lcom/twitter/library/util/aa;->g:[I

    monitor-enter v1

    :try_start_0
    sget-boolean v0, Lcom/twitter/library/util/aa;->i:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    if-eqz v2, :cond_1

    new-instance v3, Ljava/io/File;

    const-string/jumbo v4, "photos"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x2

    const/4 v4, 0x1

    const-wide/32 v5, 0x6400000

    invoke-static {v3, v2, v4, v5, v6}, Lld;->a(Ljava/io/File;IIJ)Lld;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    :goto_0
    :try_start_2
    sput-object v0, Lcom/twitter/library/util/aa;->h:Lld;

    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/library/util/aa;->i:Z

    :cond_0
    sget-object v0, Lcom/twitter/library/util/aa;->h:Lld;

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    return-object v0

    :cond_1
    :try_start_3
    new-instance v2, Ljava/io/File;

    invoke-virtual {p0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v3

    const-string/jumbo v4, "photos"

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v3, 0x2

    const/4 v4, 0x1

    const-wide/32 v5, 0xa00000

    invoke-static {v2, v3, v4, v5, v6}, Lld;->a(Ljava/io/File;IIJ)Lld;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private a(Lld;Ljava/lang/String;Ljava/io/InputStream;)Z
    .locals 3

    const/4 v0, 0x0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lld;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, p2}, Lld;->b(Ljava/lang/String;)Llf;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v2, v0}, Llf;->a(I)Ljava/io/OutputStream;

    move-result-object v1

    const/16 v0, 0x1000

    invoke-static {p3, v1, v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/InputStream;Ljava/io/OutputStream;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x1

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-virtual {v2}, Llf;->a()V

    invoke-virtual {p1}, Lld;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-virtual {v2}, Llf;->b()V

    invoke-virtual {p1}, Lld;->b()V

    throw v0
.end method


# virtual methods
.method public a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;
    .locals 6

    if-eqz p3, :cond_0

    iget-object v4, p3, Lcom/twitter/library/util/m;->a:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/aa;->a(JLjava/lang/Object;Ljava/lang/String;Z)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/util/m;)Lcom/twitter/library/network/a;
    .locals 1

    iget-boolean v0, p1, Lcom/twitter/library/util/m;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/aa;->l:Lcom/twitter/library/network/n;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;)Lcom/twitter/library/network/a;
    .locals 1

    check-cast p1, Lcom/twitter/library/util/m;

    invoke-virtual {p0, p1}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;)Lcom/twitter/library/network/a;

    move-result-object v0

    return-object v0
.end method

.method protected a(JLcom/twitter/library/util/m;Ljava/lang/String;)Lcom/twitter/library/util/ae;
    .locals 7

    const/4 v0, 0x0

    invoke-static {p4}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v3, 0x0

    sget-object v4, Lcom/twitter/library/util/aa;->g:[I

    monitor-enter v4

    :try_start_0
    iget-object v2, p0, Lcom/twitter/library/util/aa;->c:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/util/aa;->a(Landroid/content/Context;)Lld;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lld;->a()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_3

    :try_start_1
    invoke-virtual {v2, v1}, Lld;->a(Ljava/lang/String;)Lli;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v2

    if-nez v2, :cond_1

    :try_start_2
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v4

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v4
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_1
    const/4 v1, 0x0

    :try_start_3
    invoke-virtual {v2, v1}, Lli;->a(I)Ljava/io/InputStream;
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-object v1

    if-nez v1, :cond_2

    :try_start_4
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v4
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :cond_2
    :try_start_5
    invoke-virtual {p0, p3, p4, v1}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_2
    .catchall {:try_start_5 .. :try_end_5} :catchall_3

    move-result-object v0

    :try_start_6
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v4

    goto :goto_0

    :catch_0
    move-exception v1

    move-object v1, v0

    move-object v2, v0

    :goto_1
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    :cond_3
    monitor-exit v4

    goto :goto_0

    :catchall_1
    move-exception v1

    move-object v2, v0

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    :goto_2
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_2

    :catchall_3
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v1

    move-object v1, v0

    goto :goto_1

    :catch_2
    move-exception v3

    goto :goto_1
.end method

.method protected a(JLcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;
    .locals 8

    const/4 v1, 0x0

    invoke-static {p4}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    if-nez v4, :cond_0

    invoke-virtual {p0, p3, p4, p5}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v5, Lcom/twitter/library/util/aa;->g:[I

    monitor-enter v5

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/util/aa;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/util/aa;->a(Landroid/content/Context;)Lld;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    :try_start_1
    invoke-direct {p0, v6, v4, p5}, Lcom/twitter/library/util/aa;->a(Lld;Ljava/lang/String;Ljava/io/InputStream;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v6, v4}, Lld;->a(Ljava/lang/String;)Lli;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v3

    const/4 v0, 0x0

    :try_start_2
    invoke-virtual {v3, v0}, Lli;->a(I)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result-object v2

    :try_start_3
    invoke-virtual {p0, p3, p4, v2}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v7

    if-nez v7, :cond_1

    invoke-virtual {v6, v4}, Lld;->c(Ljava/lang/String;)Z

    invoke-virtual {v6}, Lld;->b()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    :cond_1
    :try_start_4
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v0

    :cond_2
    :try_start_5
    invoke-virtual {p0, p3, p4, p5}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result-object v0

    :try_start_6
    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v5

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    move-object v2, v1

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    monitor-exit v5

    move-object v0, v1

    goto :goto_0

    :catchall_1
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_2
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_2

    :catchall_3
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    move-object v2, v3

    goto :goto_1

    :catch_2
    move-exception v0

    move-object v0, v2

    move-object v2, v3

    goto :goto_1
.end method

.method protected a(Lcom/twitter/library/util/m;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;
    .locals 1

    new-instance v0, Lcom/twitter/library/util/ae;

    invoke-direct {v0, p2, p3}, Lcom/twitter/library/util/ae;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method protected a(Lcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/util/aa;->c:Landroid/content/Context;

    invoke-static {v0, p3}, Lkw;->a(Landroid/content/Context;Ljava/io/InputStream;)Lkw;

    move-result-object v0

    iget-boolean v1, p1, Lcom/twitter/library/util/m;->d:Z

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/twitter/library/util/m;->b:I

    iget v2, p1, Lcom/twitter/library/util/m;->c:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lkw;->a(I)Lkw;

    :goto_0
    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0

    :cond_0
    iget v1, p0, Lcom/twitter/library/util/aa;->k:I

    invoke-virtual {v0, v1}, Lkw;->b(I)Lkw;

    goto :goto_0
.end method

.method protected bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p3, Lcom/twitter/library/util/m;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;Ljava/lang/String;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 6

    move-object v3, p3

    check-cast v3, Lcom/twitter/library/util/m;

    move-object v0, p0

    move-wide v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p1, Lcom/twitter/library/util/m;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p1, Lcom/twitter/library/util/m;

    check-cast p3, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/m;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/network/n;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/util/aa;->l:Lcom/twitter/library/network/n;

    return-void
.end method

.method public a(Lcom/twitter/library/util/ac;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/aa;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/util/aa;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "PhotoCache listener already added"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/aa;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/aa;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ac;

    invoke-interface {v0, p0, p1}, Lcom/twitter/library/util/ac;->a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/util/ag;->b(Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(JLcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;
    .locals 6

    if-eqz p3, :cond_0

    iget-object v4, p3, Lcom/twitter/library/util/m;->a:Ljava/lang/String;

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-super/range {v0 .. v5}, Lcom/twitter/library/util/ag;->a(JLjava/lang/Object;Ljava/lang/String;Z)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/util/ac;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/aa;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
