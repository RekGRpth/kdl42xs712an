.class Lcom/twitter/library/util/i;
.super Ljava/io/InputStream;
.source "Twttr"


# instance fields
.field private a:J

.field private b:J

.field private c:J

.field private d:Ljava/io/RandomAccessFile;


# direct methods
.method private constructor <init>(Ljava/io/RandomAccessFile;JJ)V
    .locals 2

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/util/i;->d:Ljava/io/RandomAccessFile;

    iput-wide p2, p0, Lcom/twitter/library/util/i;->a:J

    add-long v0, p2, p4

    iput-wide v0, p0, Lcom/twitter/library/util/i;->b:J

    invoke-direct {p0}, Lcom/twitter/library/util/i;->b()V

    return-void
.end method

.method synthetic constructor <init>(Ljava/io/RandomAccessFile;JJLcom/twitter/library/util/h;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/twitter/library/util/i;-><init>(Ljava/io/RandomAccessFile;JJ)V

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/i;->d:Ljava/io/RandomAccessFile;

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "File closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/util/i;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/util/i;->b()V

    return-void
.end method

.method private b()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/library/util/i;->a()V

    iget-wide v0, p0, Lcom/twitter/library/util/i;->a:J

    iput-wide v0, p0, Lcom/twitter/library/util/i;->c:J

    iget-object v0, p0, Lcom/twitter/library/util/i;->d:Ljava/io/RandomAccessFile;

    iget-wide v1, p0, Lcom/twitter/library/util/i;->c:J

    invoke-virtual {v0, v1, v2}, Ljava/io/RandomAccessFile;->seek(J)V

    return-void
.end method

.method private c()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/util/i;->c:J

    iget-wide v2, p0, Lcom/twitter/library/util/i;->b:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private d()J
    .locals 4

    invoke-direct {p0}, Lcom/twitter/library/util/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/util/i;->b:J

    iget-wide v2, p0, Lcom/twitter/library/util/i;->c:J

    sub-long/2addr v0, v2

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/util/i;->d:Ljava/io/RandomAccessFile;

    return-void
.end method

.method public read()I
    .locals 4

    invoke-direct {p0}, Lcom/twitter/library/util/i;->a()V

    invoke-direct {p0}, Lcom/twitter/library/util/i;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/util/i;->c:J

    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    iput-wide v0, p0, Lcom/twitter/library/util/i;->c:J

    iget-object v0, p0, Lcom/twitter/library/util/i;->d:Ljava/io/RandomAccessFile;

    invoke-virtual {v0}, Ljava/io/RandomAccessFile;->read()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    goto :goto_0
.end method

.method public read([BII)I
    .locals 5

    invoke-direct {p0}, Lcom/twitter/library/util/i;->a()V

    invoke-direct {p0}, Lcom/twitter/library/util/i;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, -0x1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/util/i;->d()J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {p3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/util/i;->d:Ljava/io/RandomAccessFile;

    invoke-virtual {v1, p1, p2, v0}, Ljava/io/RandomAccessFile;->read([BII)I

    move-result v0

    if-lez v0, :cond_0

    iget-wide v1, p0, Lcom/twitter/library/util/i;->c:J

    int-to-long v3, v0

    add-long/2addr v1, v3

    iput-wide v1, p0, Lcom/twitter/library/util/i;->c:J

    goto :goto_0
.end method
