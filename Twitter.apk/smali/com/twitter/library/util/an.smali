.class public Lcom/twitter/library/util/an;
.super Lcom/twitter/library/util/af;
.source "Twttr"


# instance fields
.field private d:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/io/File;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/util/af;-><init>(Ljava/lang/String;)V

    iput-object p2, p0, Lcom/twitter/library/util/an;->c:Ljava/lang/Object;

    return-void
.end method

.method private static a(J)I
    .locals 6

    const-wide/32 v4, -0x80000000

    and-long v0, p0, v4

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    and-long v0, p0, v4

    cmp-long v0, v0, v4

    if-nez v0, :cond_1

    :cond_0
    long-to-int v0, p0

    return v0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "File is too large to store in cache."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private d()I
    .locals 2

    iget v0, p0, Lcom/twitter/library/util/an;->d:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/an;->c:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/an;->c:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    invoke-static {v0, v1}, Lcom/twitter/library/util/an;->a(J)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/util/an;->d:I

    :cond_0
    iget v0, p0, Lcom/twitter/library/util/an;->d:I

    return v0
.end method


# virtual methods
.method public a()I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/util/an;->d()I

    move-result v0

    return v0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/util/an;->c:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method
