.class public Lcom/twitter/library/util/ao;
.super Lcom/twitter/library/util/ag;
.source "Twttr"


# static fields
.field private static final h:I


# instance fields
.field public final g:I

.field private final i:Ljava/util/ArrayList;

.field private final j:I

.field private final k:Ljava/lang/String;

.field private l:Lcom/twitter/library/util/e;

.field private m:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string/jumbo v0, "/profile_images/"

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    sput v0, Lcom/twitter/library/util/ao;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;IILjava/lang/String;)V
    .locals 3

    invoke-direct {p0, p1, p3}, Lcom/twitter/library/util/ag;-><init>(Landroid/content/Context;I)V

    iput p2, p0, Lcom/twitter/library/util/ao;->g:I

    iput-object p4, p0, Lcom/twitter/library/util/ao;->k:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/util/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/util/ao;->g:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    sget v1, Lie;->user_image_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/util/ao;->j:I

    :goto_0
    return-void

    :cond_0
    iget v1, p0, Lcom/twitter/library/util/ao;->g:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    sget v1, Lie;->mini_user_image_size:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/util/ao;->j:I

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid cache type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/twitter/library/util/ao;)Lcom/twitter/library/util/e;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/util/ao;->c()Lcom/twitter/library/util/e;

    move-result-object v0

    return-object v0
.end method

.method static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 8

    const/4 v2, -0x1

    invoke-static {p0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v4

    const/16 v0, 0x2e

    invoke-virtual {v4, v0}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v0

    if-eq v0, v2, :cond_1

    invoke-static {}, Lcom/twitter/library/util/Util$AvatarSize;->values()[Lcom/twitter/library/util/Util$AvatarSize;

    move-result-object v5

    array-length v6, v5

    const/4 v1, 0x0

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_3

    aget-object v7, v5, v3

    iget-object v1, v7, Lcom/twitter/library/util/Util$AvatarSize;->suffix:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int v1, v0, v1

    iget-object v7, v7, Lcom/twitter/library/util/Util$AvatarSize;->suffix:Ljava/lang/String;

    invoke-virtual {v4, v7, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_0

    :goto_1
    if-ne v1, v2, :cond_2

    :goto_2
    sget v1, Lcom/twitter/library/util/ao;->h:I

    invoke-virtual {v4, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    const/16 v1, 0x2f

    const/16 v2, 0x5f

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v0

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/library/util/ao;)I
    .locals 1

    iget v0, p0, Lcom/twitter/library/util/ao;->j:I

    return v0
.end method

.method private declared-synchronized c()Lcom/twitter/library/util/e;
    .locals 5

    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/library/util/ao;->m:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/ao;->k:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/util/ao;->l:Lcom/twitter/library/util/e;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-nez v0, :cond_2

    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/util/ao;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/util/ao;->k:Ljava/lang/String;

    const/4 v2, 0x2

    const/high16 v3, 0xa00000

    const/high16 v4, 0x200000

    invoke-static {v0, v1, v2, v3, v4}, Lcom/twitter/library/util/e;->a(Landroid/content/Context;Ljava/lang/String;III)Lcom/twitter/library/util/e;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/util/ao;->l:Lcom/twitter/library/util/e;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    iget-object v0, p0, Lcom/twitter/library/util/ao;->l:Lcom/twitter/library/util/e;

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/util/ao;->m:Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/twitter/library/util/ao;->l:Lcom/twitter/library/util/e;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method


# virtual methods
.method public a(JLjava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6

    iget v0, p0, Lcom/twitter/library/util/ao;->j:I

    invoke-static {p4, v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x1

    move-object v0, p0

    move-wide v1, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/ao;->a(JLjava/lang/Object;Ljava/lang/String;Z)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(JLjava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/util/ao;->c:Landroid/content/Context;

    invoke-static {v0, p5}, Lkw;->a(Landroid/content/Context;Ljava/io/InputStream;)Lkw;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/util/ao;->j:I

    invoke-virtual {v0, v1}, Lkw;->a(I)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/library/util/ao;->c()Lcom/twitter/library/util/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p3}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/util/e;->a(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    :cond_0
    invoke-virtual {p0, p3, p4, v0}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;
    .locals 1

    new-instance v0, Lcom/twitter/library/util/ae;

    invoke-direct {v0, p2, p3}, Lcom/twitter/library/util/ae;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-object v0
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;
    .locals 1

    if-eqz p3, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/ao;->c:Landroid/content/Context;

    invoke-static {v0, p3}, Lkw;->a(Landroid/content/Context;Ljava/io/InputStream;)Lkw;

    move-result-object v0

    invoke-virtual {v0}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected synthetic a(JLjava/lang/Object;Ljava/lang/String;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p3, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/library/util/ao;->b(JLjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(JLjava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 6

    move-object v3, p3

    check-cast v3, Ljava/lang/String;

    move-object v0, p0

    move-wide v1, p1

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/util/ao;->a(JLjava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p1, Ljava/lang/String;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Lcom/twitter/library/util/af;
    .locals 1

    check-cast p1, Ljava/lang/String;

    check-cast p3, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/library/util/ar;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/library/util/ao;->c()Lcom/twitter/library/util/e;

    move-result-object v0

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/library/util/ao;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/twitter/library/util/aq;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/library/util/aq;-><init>(Lcom/twitter/library/util/ao;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method protected a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/util/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/util/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ar;

    invoke-interface {v0, p0, p1}, Lcom/twitter/library/util/ar;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public a([Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/library/util/ao;->c()Lcom/twitter/library/util/e;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/twitter/library/util/ao;->b:Ljava/util/concurrent/ExecutorService;

    new-instance v1, Lcom/twitter/library/util/ap;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/util/ap;-><init>(Lcom/twitter/library/util/ao;[Ljava/lang/String;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/util/ag;->b(Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected b(JLjava/lang/String;Ljava/lang/String;)Lcom/twitter/library/util/ae;
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/library/util/ao;->c()Lcom/twitter/library/util/e;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p3}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/util/e;->a(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0, p3, p4, v1}, Lcom/twitter/library/util/ao;->a(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;)Lcom/twitter/library/util/ae;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method public b(Lcom/twitter/library/util/ar;)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "listener cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/util/ao;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    return-void
.end method
