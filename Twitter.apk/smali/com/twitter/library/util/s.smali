.class public Lcom/twitter/library/util/s;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/twitter/library/provider/ParcelableTweet;)I
    .locals 14

    const/4 v1, 0x1

    const-wide/16 v3, 0x0

    const/4 v6, 0x0

    invoke-interface {p0}, Lcom/twitter/library/provider/ParcelableTweet;->o()Ljava/util/ArrayList;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-interface {v8}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move v0, v6

    :cond_1
    :goto_0
    return v0

    :cond_2
    invoke-interface {v8}, Ljava/util/List;->size()I

    move-result v0

    const/4 v2, 0x4

    invoke-static {v0, v2}, Ljava/lang/Math;->min(II)I

    move-result v0

    if-le v0, v1, :cond_3

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->x()Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move-wide v1, v3

    move v5, v6

    move v7, v0

    :cond_4
    :goto_1
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, v6, v6}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;II)Z

    move-result v10

    if-eqz v10, :cond_4

    iget-wide v10, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    cmp-long v10, v10, v3

    if-nez v10, :cond_5

    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v7, v7, -0x1

    if-nez v7, :cond_a

    move v0, v5

    move v5, v7

    :goto_2
    if-gtz v0, :cond_1

    cmp-long v3, v1, v3

    if-nez v3, :cond_6

    move v0, v6

    goto :goto_0

    :cond_5
    cmp-long v10, v1, v3

    if-nez v10, :cond_a

    iget-wide v1, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    move-wide v12, v1

    move-wide v0, v12

    move v2, v5

    move v5, v7

    :goto_3
    move v7, v5

    move v5, v2

    move-wide v12, v0

    move-wide v1, v12

    goto :goto_1

    :cond_6
    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v3, v0

    move v4, v5

    :cond_7
    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, v6, v6}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;II)Z

    move-result v5

    if-eqz v5, :cond_7

    iget-wide v8, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    cmp-long v0, v8, v1

    if-nez v0, :cond_8

    add-int/lit8 v0, v3, 0x1

    add-int/lit8 v3, v4, -0x1

    if-eqz v3, :cond_1

    :goto_5
    move v4, v3

    move v3, v0

    goto :goto_4

    :cond_8
    move v0, v3

    move v3, v4

    goto :goto_5

    :cond_9
    move v0, v3

    goto :goto_0

    :cond_a
    move-wide v12, v1

    move-wide v0, v12

    move v2, v5

    move v5, v7

    goto :goto_3

    :cond_b
    move v0, v5

    move v5, v7

    goto :goto_2
.end method

.method public static a(Lcom/twitter/library/api/MediaEntity;)Lcom/twitter/library/util/m;
    .locals 4

    new-instance v0, Lcom/twitter/library/util/m;

    iget-object v1, p0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/library/api/MediaEntity;->width:I

    iget v3, p0, Lcom/twitter/library/api/MediaEntity;->height:I

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    return-object v0
.end method

.method public static a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/s;->a(Ljava/util/Collection;II)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/util/Collection;)Ljava/util/List;
    .locals 5

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget v3, v0, Lcom/twitter/library/api/MediaEntity;->type:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_4
    move-object v0, v1

    goto :goto_0
.end method

.method public static a(Ljava/util/Collection;II)Ljava/util/List;
    .locals 12

    const/4 v0, 0x1

    const-wide/16 v3, 0x0

    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/Collection;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {p0}, Ljava/util/Collection;->size()I

    move-result v1

    const/4 v2, 0x4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    if-le v1, v0, :cond_a

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->x()Z

    move-result v2

    if-nez v2, :cond_a

    :goto_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move-wide v1, v3

    move v5, v0

    :cond_2
    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;II)Z

    move-result v8

    if-eqz v8, :cond_2

    iget-wide v8, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    cmp-long v8, v8, v3

    if-nez v8, :cond_4

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, -0x1

    if-nez v5, :cond_9

    :cond_3
    invoke-interface {v6}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    move-object v0, v6

    goto :goto_0

    :cond_4
    cmp-long v8, v1, v3

    if-nez v8, :cond_9

    iget-wide v1, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    move-wide v10, v1

    move-wide v0, v10

    move v2, v5

    :goto_3
    move v5, v2

    move-wide v10, v0

    move-wide v1, v10

    goto :goto_2

    :cond_5
    cmp-long v0, v1, v3

    if-nez v0, :cond_6

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;II)Z

    move-result v4

    if-eqz v4, :cond_7

    iget-wide v7, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    cmp-long v4, v7, v1

    if-nez v4, :cond_7

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v5, v5, -0x1

    if-nez v5, :cond_7

    :cond_8
    move-object v0, v6

    goto :goto_0

    :cond_9
    move-wide v10, v1

    move-wide v0, v10

    move v2, v5

    goto :goto_3

    :cond_a
    move v0, v1

    goto :goto_1
.end method

.method private static a(Lcom/twitter/library/api/MediaEntity;II)Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->type:I

    if-ne v1, v0, :cond_0

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->width:I

    if-lt v1, p1, :cond_0

    iget v1, p0, Lcom/twitter/library/api/MediaEntity;->height:I

    if-lt v1, p2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Lcom/twitter/library/provider/Tweet;II)Lcom/twitter/library/api/MediaEntity;
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;II)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-wide v3, v0, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-nez v3, :cond_3

    move-object v1, v0

    goto :goto_0

    :cond_3
    if-nez v1, :cond_4

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_4
    move-object v0, v1

    goto :goto_2
.end method

.method public static b(Ljava/util/Collection;)Ljava/util/List;
    .locals 5

    invoke-static {p0}, Lcom/twitter/library/util/s;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget-wide v3, v0, Lcom/twitter/library/api/MediaEntity;->id:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public static c(Lcom/twitter/library/provider/Tweet;II)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, p1, p2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;II)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
