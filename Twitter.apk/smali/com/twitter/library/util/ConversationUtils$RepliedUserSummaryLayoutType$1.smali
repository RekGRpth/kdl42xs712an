.class final enum Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType$1;
.super Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;
.source "Twttr"


# direct methods
.method constructor <init>(Ljava/lang/String;I)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryLayoutType;-><init>(Ljava/lang/String;ILcom/twitter/library/util/d;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/CharSequence;Landroid/text/TextPaint;III)Landroid/text/StaticLayout;
    .locals 13

    new-instance v1, Landroid/text/StaticLayout;

    const/4 v3, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v4

    sget-object v7, Landroid/text/Layout$Alignment;->ALIGN_NORMAL:Landroid/text/Layout$Alignment;

    const/high16 v8, 0x3f800000    # 1.0f

    move/from16 v0, p4

    int-to-float v9, v0

    const/4 v10, 0x0

    sget-object v11, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object v2, p1

    move-object v5, p2

    move/from16 v6, p3

    move/from16 v12, p5

    invoke-direct/range {v1 .. v12}, Landroid/text/StaticLayout;-><init>(Ljava/lang/CharSequence;IILandroid/text/TextPaint;ILandroid/text/Layout$Alignment;FFZLandroid/text/TextUtils$TruncateAt;I)V

    return-object v1
.end method
