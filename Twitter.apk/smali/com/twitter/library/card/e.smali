.class Lcom/twitter/library/card/e;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/library/provider/Tweet;

.field final synthetic c:J

.field final synthetic d:Lcom/twitter/library/util/aa;

.field final synthetic e:Lcom/twitter/library/util/as;

.field final synthetic f:Lcom/twitter/library/card/Card;


# direct methods
.method constructor <init>(Lcom/twitter/library/card/Card;Landroid/content/Context;Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    iput-object p2, p0, Lcom/twitter/library/card/e;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/library/card/e;->b:Lcom/twitter/library/provider/Tweet;

    iput-wide p4, p0, Lcom/twitter/library/card/e;->c:J

    iput-object p6, p0, Lcom/twitter/library/card/e;->d:Lcom/twitter/library/util/aa;

    iput-object p7, p0, Lcom/twitter/library/card/e;->e:Lcom/twitter/library/util/as;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 6

    iget-object v1, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    iget-object v2, p0, Lcom/twitter/library/card/e;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/card/e;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)V

    iget-object v0, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    iget-object v2, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0, v2}, Lcom/twitter/library/card/Card;->d(Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    iget-wide v2, p0, Lcom/twitter/library/card/e;->c:J

    iget-object v4, p0, Lcom/twitter/library/card/e;->d:Lcom/twitter/library/util/aa;

    iget-object v5, p0, Lcom/twitter/library/card/e;->e:Lcom/twitter/library/util/as;

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/twitter/library/card/Card;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    iget-object v0, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/twitter/library/card/Card;->mToRefresh:Z

    monitor-exit v1

    const/4 v0, 0x0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->r()V

    iget-object v0, p0, Lcom/twitter/library/card/e;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->J()V

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/e;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/e;->a(Ljava/lang/Void;)V

    return-void
.end method
