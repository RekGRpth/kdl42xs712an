.class public Lcom/twitter/library/card/element/FormSelectView;
.super Lcom/twitter/library/card/element/FormFieldElementView;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field private c:Lcom/twitter/library/card/element/FormSelect;

.field private d:Landroid/widget/Spinner;

.field private e:Lcom/twitter/library/card/element/e;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormSelect;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/FormFieldElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)V

    iput-object p2, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/library/card/element/FormSelectView;->f:I

    return-void
.end method

.method private a(Landroid/content/Context;)Landroid/widget/Spinner;
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    new-instance v0, Landroid/widget/Spinner;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;I)V

    return-object v0
.end method

.method private a(Lcom/twitter/library/card/property/FormSelectOption;Lcom/twitter/library/card/Card;)Ljava/lang/String;
    .locals 1

    iget v0, p1, Lcom/twitter/library/card/property/FormSelectOption;->tokenizedTextId:I

    invoke-virtual {p0, v0, p2}, Lcom/twitter/library/card/element/FormSelectView;->a(ILcom/twitter/library/card/Card;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/content/Context;)Landroid/widget/Spinner;
    .locals 1

    new-instance v0, Landroid/widget/Spinner;

    invoke-direct {v0, p1}, Landroid/widget/Spinner;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private setUpAdapter(Lcom/twitter/library/card/Card;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    iget-object v0, v0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    array-length v0, v0

    if-lez v0, :cond_3

    new-instance v0, Lcom/twitter/library/card/element/e;

    invoke-virtual {p1}, Lcom/twitter/library/card/Card;->C()Landroid/content/Context;

    move-result-object v1

    const v2, 0x1090008    # android.R.layout.simple_spinner_item

    iget-object v3, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/library/card/element/e;-><init>(Lcom/twitter/library/card/element/FormSelectView;Landroid/content/Context;ILcom/twitter/library/card/element/FormSelect;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    const v1, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/element/e;->setDropDownViewResource(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    iget-object v1, v0, Lcom/twitter/library/card/element/FormSelect;->options:[Lcom/twitter/library/card/property/FormSelectOption;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    invoke-direct {p0, v3, p1}, Lcom/twitter/library/card/element/FormSelectView;->a(Lcom/twitter/library/card/property/FormSelectOption;Lcom/twitter/library/card/Card;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    invoke-virtual {v5, v4}, Lcom/twitter/library/card/element/e;->add(Ljava/lang/Object;)V

    iget-boolean v3, v3, Lcom/twitter/library/card/property/FormSelectOption;->selected:Z

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    invoke-virtual {v3, v4}, Lcom/twitter/library/card/element/e;->getPosition(Ljava/lang/Object;)I

    move-result v3

    iput v3, p0, Lcom/twitter/library/card/element/FormSelectView;->f:I

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/e;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    iget-object v1, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    iget-object v1, v1, Lcom/twitter/library/card/element/FormSelect;->fontName:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    iget-boolean v2, v2, Lcom/twitter/library/card/element/FormSelect;->fontBold:Z

    iget-object v3, p0, Lcom/twitter/library/card/element/FormSelectView;->c:Lcom/twitter/library/card/element/FormSelect;

    iget-boolean v3, v3, Lcom/twitter/library/card/element/FormSelect;->fontItalic:Z

    invoke-virtual {p0, v1, v2, v3}, Lcom/twitter/library/card/element/FormSelectView;->a(Ljava/lang/String;ZZ)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/element/e;->a:Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/twitter/library/card/element/FormSelectView;->e:Lcom/twitter/library/card/element/e;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget v0, p0, Lcom/twitter/library/card/element/FormSelectView;->f:I

    if-ltz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    iget v1, p0, Lcom/twitter/library/card/element/FormSelectView;->f:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/widget/Spinner;->setSelection(IZ)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    invoke-virtual {v0, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    :cond_3
    return-void
.end method


# virtual methods
.method protected a(Landroid/content/Context;Lcom/twitter/library/card/element/FormFieldElement;)Landroid/view/View;
    .locals 2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/library/card/element/FormSelectView;->b(Landroid/content/Context;)Landroid/widget/Spinner;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    return-object v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/card/element/FormSelectView;->a(Landroid/content/Context;)Landroid/widget/Spinner;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/card/element/FormSelectView;->setUpAdapter(Lcom/twitter/library/card/Card;)V

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/FormFieldElementView;->a(Lcom/twitter/library/card/Card;)V

    return-void
.end method

.method public getSelectedIndex()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/FormSelectView;->f:I

    return v0
.end method

.method public getSpinner()Landroid/widget/Spinner;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    return-object v0
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/FormSelectView;->d:Landroid/widget/Spinner;

    invoke-virtual {v0}, Landroid/widget/Spinner;->getSelectedItemPosition()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/FormSelectView;->f:I

    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
