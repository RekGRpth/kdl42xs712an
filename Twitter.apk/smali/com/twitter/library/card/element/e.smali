.class Lcom/twitter/library/card/element/e;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# instance fields
.field public a:Landroid/graphics/Typeface;

.field final synthetic b:Lcom/twitter/library/card/element/FormSelectView;

.field private c:Lcom/twitter/library/card/element/FormSelect;


# direct methods
.method public constructor <init>(Lcom/twitter/library/card/element/FormSelectView;Landroid/content/Context;ILcom/twitter/library/card/element/FormSelect;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/element/e;->b:Lcom/twitter/library/card/element/FormSelectView;

    invoke-direct {p0, p2, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    iput-object p4, p0, Lcom/twitter/library/card/element/e;->c:Lcom/twitter/library/card/element/FormSelect;

    return-void
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/e;->a:Landroid/graphics/Typeface;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/e;->c:Lcom/twitter/library/card/element/FormSelect;

    iget-boolean v0, v0, Lcom/twitter/library/card/element/FormSelect;->fontUnderline:Z

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    or-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    :goto_0
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/card/element/e;->c:Lcom/twitter/library/card/element/FormSelect;

    iget v1, v1, Lcom/twitter/library/card/element/FormSelect;->fontSize:F

    invoke-virtual {p1, v0, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/library/card/element/e;->c:Lcom/twitter/library/card/element/FormSelect;

    iget v0, v0, Lcom/twitter/library/card/element/FormSelect;->color:I

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setSingleLine(Z)V

    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v0

    and-int/lit8 v0, v0, -0x9

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_0
.end method


# virtual methods
.method public getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getDropDownView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/twitter/library/card/element/e;->a(Landroid/widget/TextView;)V

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/widget/ArrayAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-direct {p0, v0}, Lcom/twitter/library/card/element/e;->a(Landroid/widget/TextView;)V

    return-object v0
.end method
