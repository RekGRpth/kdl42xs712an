.class public Lcom/twitter/library/card/element/Image;
.super Lcom/twitter/library/card/element/Element;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x1f3405cd0a56989dL


# instance fields
.field public alignmentMode:Lcom/twitter/library/card/property/Vector2;

.field public cornerRadius:F

.field public fillAvailableSpace:Z

.field public loadingIndicator:I

.field protected mDrawBitmap:Landroid/graphics/Bitmap;

.field private mDstImageRect:Landroid/graphics/RectF;

.field protected mImageKey:Lcom/twitter/library/util/m;

.field protected mLoadedBitmap:Landroid/graphics/Bitmap;

.field private mSrcImageRect:Landroid/graphics/Rect;

.field public preserveAspectRatio:Z

.field public spec:Lcom/twitter/library/card/property/ImageSpec;

.field public specFullSize:Lcom/twitter/library/card/property/ImageSpec;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mSrcImageRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mDstImageRect:Landroid/graphics/RectF;

    return-void
.end method

.method private a(Landroid/graphics/Canvas;I)V
    .locals 4

    sget-object v0, Lcom/twitter/library/card/element/Image;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    sget-object v1, Lcom/twitter/library/card/element/Image;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, p2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/twitter/library/card/element/Image;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    const/4 v2, 0x0

    cmpl-float v2, v1, v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Image;->mLayoutRect:Landroid/graphics/RectF;

    sget-object v2, Lcom/twitter/library/card/element/Image;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :goto_0
    sget-object v1, Lcom/twitter/library/card/element/Image;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->mLayoutRect:Landroid/graphics/RectF;

    sget-object v3, Lcom/twitter/library/card/element/Image;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v2, v1, v1, v3}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method private b()V
    .locals 10

    const/high16 v8, 0x3f000000    # 0.5f

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->a()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v0

    iget v2, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget-boolean v3, p0, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/twitter/library/card/element/Image;->mLoadedBitmap:Landroid/graphics/Bitmap;

    if-eqz v3, :cond_3

    cmpl-float v3, v2, v1

    if-lez v3, :cond_3

    cmpl-float v3, v0, v1

    if-lez v3, :cond_3

    iget-object v3, p0, Lcom/twitter/library/card/element/Image;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v3, p0, Lcom/twitter/library/card/element/Image;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget v5, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    div-float v3, v2, v0

    div-float v0, v4, v5

    cmpl-float v0, v3, v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    xor-int/2addr v0, v2

    if-eqz v0, :cond_2

    float-to-double v6, v4

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v2, v6

    div-float v0, v4, v3

    float-to-double v3, v0

    invoke-static {v3, v4}, Ljava/lang/Math;->floor(D)D

    move-result-wide v3

    double-to-float v3, v3

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_2
    iget-object v4, p0, Lcom/twitter/library/card/element/Image;->mDstImageRect:Landroid/graphics/RectF;

    add-float/2addr v2, v1

    add-float/2addr v3, v0

    invoke-virtual {v4, v1, v0, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_0
    sub-float v0, v5, v3

    mul-float/2addr v0, v8

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-float v0, v4

    goto :goto_2

    :pswitch_1
    float-to-double v6, v3

    invoke-static {v6, v7}, Ljava/lang/Math;->floor(D)D

    move-result-wide v6

    double-to-float v0, v6

    sub-float v0, v5, v0

    goto :goto_2

    :cond_2
    mul-float v0, v5, v3

    float-to-double v2, v0

    invoke-static {v2, v3}, Ljava/lang/Math;->floor(D)D

    move-result-wide v2

    double-to-float v2, v2

    float-to-double v5, v5

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-float v3, v5

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    packed-switch v0, :pswitch_data_1

    move v0, v1

    goto :goto_2

    :pswitch_2
    sub-float v0, v4, v2

    mul-float/2addr v0, v8

    float-to-double v4, v0

    invoke-static {v4, v5}, Ljava/lang/Math;->floor(D)D

    move-result-wide v4

    double-to-float v0, v4

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_2

    :pswitch_3
    float-to-double v5, v2

    invoke-static {v5, v6}, Ljava/lang/Math;->floor(D)D

    move-result-wide v5

    double-to-float v0, v5

    sub-float v0, v4, v0

    move v9, v1

    move v1, v0

    move v0, v9

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mDstImageRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/card/element/Image;->mLayoutRect:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public J()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->J()V

    return-void
.end method

.method protected a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->a()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v2

    iget v1, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->y:F

    if-nez p1, :cond_3

    cmpl-float v0, v2, v0

    if-lez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->v()I

    move-result v0

    const/4 v3, 0x2

    if-ne v0, v3, :cond_1

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    mul-float/2addr v0, v1

    div-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0

    :cond_3
    cmpl-float v0, v1, v0

    if-lez v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->v()I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_4

    iget v0, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    mul-float/2addr v0, v2

    div-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-float v0, v0

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    goto :goto_0
.end method

.method protected a()Lcom/twitter/library/card/property/Vector2F;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    iget-object v0, v0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    return-object v0
.end method

.method public declared-synchronized a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :goto_0
    monitor-exit p0

    return-void

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    iget-object v0, v0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getEncodedPath()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "/2/proxy.jpg"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget v1, Lcom/twitter/library/util/b;->a:F

    const/high16 v2, 0x40000000    # 2.0f

    cmpl-float v1, v1, v2

    if-ltz v1, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "m"

    const-string/jumbo v2, "2"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->a()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/util/m;

    iget v3, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v3, v3

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v1, v1

    invoke-direct {v2, v0, v3, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    iput-object v2, p0, Lcom/twitter/library/card/element/Image;->mImageKey:Lcom/twitter/library/util/m;

    new-instance v0, Lcom/twitter/library/card/element/g;

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->mImageKey:Lcom/twitter/library/util/m;

    move-object v1, p3

    move-wide v3, p1

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/card/element/g;-><init>(Lcom/twitter/library/util/aa;Lcom/twitter/library/util/m;JLcom/twitter/library/card/element/Image;)V

    invoke-virtual {v0}, Lcom/twitter/library/card/element/g;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_2
    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "m"

    const-string/jumbo v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    iget-object v0, v0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public declared-synchronized a(Lcom/twitter/library/util/m;Landroid/graphics/Bitmap;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mImageKey:Lcom/twitter/library/util/m;

    if-ne p1, v0, :cond_0

    iput-object p2, p0, Lcom/twitter/library/card/element/Image;->mLoadedBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->K()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mLayoutRect:Landroid/graphics/RectF;

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/twitter/library/card/element/Image;->mSrcImageRect:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->mDstImageRect:Landroid/graphics/RectF;

    sget-object v3, Lcom/twitter/library/card/element/Image;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mCardView:Lcom/twitter/library/card/CardView;

    iget-object v0, v0, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    iget v0, v0, Lcom/twitter/library/card/o;->c:I

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/card/element/Image;->a(Landroid/graphics/Canvas;I)V

    goto :goto_0
.end method

.method public c(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mCardView:Lcom/twitter/library/card/CardView;

    iget-object v0, v0, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    iget v0, v0, Lcom/twitter/library/card/o;->b:I

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/card/element/Image;->a(Landroid/graphics/Canvas;I)V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/Image;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/element/Image;

    iget v2, p1, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    iget v3, p0, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    iget v3, p1, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    iget-object v3, p1, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p1, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-nez v2, :cond_8

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    iget-object v3, p1, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/ImageSpec;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p1, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->hashCode()I

    move-result v0

    mul-int/lit8 v3, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/ImageSpec;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v3

    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    if-eqz v0, :cond_2

    move v0, v2

    :goto_1
    add-int/2addr v0, v3

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    if-eqz v3, :cond_3

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-eqz v2, :cond_0

    iget v1, p0, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-static {v1}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method protected p()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->p()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    return-void
.end method

.method public q()V
    .locals 6

    const/4 v5, 0x0

    invoke-direct {p0}, Lcom/twitter/library/card/element/Image;->b()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mLoadedBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->cornerRadius:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Image;->C()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->mLoadedBitmap:Landroid/graphics/Bitmap;

    iget-object v3, p0, Lcom/twitter/library/card/element/Image;->mDstImageRect:Landroid/graphics/RectF;

    invoke-virtual {v3}, Landroid/graphics/RectF;->width()F

    move-result v3

    float-to-int v3, v3

    iget-object v4, p0, Lcom/twitter/library/card/element/Image;->mDstImageRect:Landroid/graphics/RectF;

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    float-to-int v4, v4

    invoke-static {v1, v2, v3, v4, v0}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/graphics/Bitmap;IIF)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mSrcImageRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {v0, v5, v5, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->mLoadedBitmap:Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/ImageSpec;

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/ImageSpec;

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->specFullSize:Lcom/twitter/library/card/property/ImageSpec;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2;

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    return-void
.end method

.method protected v()I
    .locals 4

    const/4 v1, 0x2

    const/4 v3, 0x5

    const/4 v0, 0x3

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v2, v0, :cond_3

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v2, v0, :cond_0

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-eq v2, v3, :cond_1

    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->y:I

    if-eq v2, v3, :cond_1

    const/4 v0, 0x4

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v2, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    if-eq v0, v3, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/card/element/Image;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v2, v0, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    invoke-super {p0}, Lcom/twitter/library/card/element/Element;->v()I

    move-result v0

    goto :goto_0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Element;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->specFullSize:Lcom/twitter/library/card/property/ImageSpec;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Image;->preserveAspectRatio:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Image;->fillAvailableSpace:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Image;->alignmentMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/Image;->cornerRadius:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget v0, p0, Lcom/twitter/library/card/element/Image;->loadingIndicator:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    return-void
.end method

.method public declared-synchronized y()V
    .locals 1

    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mLoadedBitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mDrawBitmap:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/element/Image;->mImageKey:Lcom/twitter/library/util/m;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
