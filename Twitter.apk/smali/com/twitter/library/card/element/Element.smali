.class public abstract Lcom/twitter/library/card/element/Element;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/GestureDetector$OnGestureListener;
.implements Landroid/view/View$OnTouchListener;
.implements Ljava/io/Externalizable;


# static fields
.field protected static final a:Landroid/graphics/Paint;

.field protected static b:Lcom/twitter/library/card/element/a; = null

.field private static final serialVersionUID:J = -0x30de32162fc537f8L


# instance fields
.field public debugId:Ljava/lang/String;

.field public doubleTapActionId:I

.field public id:I

.field public longPressActionId:I

.field protected mCard:Lcom/twitter/library/card/Card;

.field protected mCardView:Lcom/twitter/library/card/CardView;

.field protected mComputedStyle:Lcom/twitter/library/card/property/Style;

.field private mContext:Landroid/content/Context;

.field private mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

.field protected mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

.field protected final mLayoutRect:Landroid/graphics/RectF;

.field protected mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

.field protected mMeasuredConstraint:Lcom/twitter/library/card/property/Vector2F;

.field protected mMeasuredSize:Lcom/twitter/library/card/property/Vector2F;

.field protected mParent:Lcom/twitter/library/card/element/Container;

.field protected mPressed:Z

.field protected mPressedX:F

.field protected mPressedY:F

.field protected mSerialId:I

.field private mStyleStack:Ljava/util/ArrayList;

.field protected mView:Landroid/view/View;

.field private mVisible:Z

.field private mVisibleChild:Z

.field public margin:Lcom/twitter/library/card/property/Spacing;

.field public maxSize:Lcom/twitter/library/card/property/Vector2F;

.field public maxSizeMode:Lcom/twitter/library/card/property/Vector2;

.field public opacity:F

.field public position:Lcom/twitter/library/card/property/Vector2F;

.field public positionMode:I

.field public pressDownActionId:I

.field public pressUpActionId:I

.field public shadow:Lcom/twitter/library/card/property/Shadow;

.field public size:Lcom/twitter/library/card/property/Vector2F;

.field public sizeMode:Lcom/twitter/library/card/property/Vector2;

.field public tapActionId:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/twitter/library/card/element/Element;->a:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0xa

    iput v0, p0, Lcom/twitter/library/card/element/Element;->doubleTapActionId:I

    invoke-static {}, Lcom/twitter/library/card/property/Vector2F;->b()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mMeasuredSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-static {}, Lcom/twitter/library/card/property/Vector2F;->b()Lcom/twitter/library/card/property/Vector2F;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mMeasuredConstraint:Lcom/twitter/library/card/property/Vector2F;

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0}, Lcom/twitter/library/card/property/Vector2F;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mLayoutRect:Landroid/graphics/RectF;

    new-instance v0, Lcom/twitter/library/card/property/Style;

    invoke-direct {v0}, Lcom/twitter/library/card/property/Style;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    iput-boolean v1, p0, Lcom/twitter/library/card/element/Element;->mVisibleChild:Z

    return-void
.end method

.method private a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/card/element/a;)V
    .locals 0

    sput-object p0, Lcom/twitter/library/card/element/Element;->b:Lcom/twitter/library/card/element/a;

    return-void
.end method


# virtual methods
.method public A()Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    return-object v0
.end method

.method public B()Lcom/twitter/library/card/property/Vector2F;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    return-object v0
.end method

.method protected C()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method public D()Lcom/twitter/library/card/element/ElementState;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-instance v3, Lcom/twitter/library/card/element/ElementState;

    invoke-direct {v3, v0}, Lcom/twitter/library/card/element/ElementState;-><init>(I)V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Style;

    iget-object v5, v3, Lcom/twitter/library/card/element/ElementState;->a:[I

    add-int/lit8 v2, v1, 0x1

    iget v0, v0, Lcom/twitter/library/card/property/Style;->id:I

    aput v0, v5, v1

    move v1, v2

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public E()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected F()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    if-nez v0, :cond_0

    new-instance v0, Landroid/support/v4/view/GestureDetectorCompat;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/GestureDetectorCompat;->setIsLongpressEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    :cond_0
    return-void
.end method

.method public G()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Element;->mVisibleChild:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, v1, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/twitter/library/card/element/Element;->mVisibleChild:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/library/card/element/Element;->positionMode:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public I()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Element;->mVisibleChild:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->positionMode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->w()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->G()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->postInvalidate()V

    :cond_0
    return-void

    :cond_1
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public K()V
    .locals 0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->J()V

    iget-object p0, p0, Lcom/twitter/library/card/element/Element;->mParent:Lcom/twitter/library/card/element/Container;

    if-nez p0, :cond_0

    return-void
.end method

.method protected a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public a(I)Lcom/twitter/library/card/element/Element;
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/Element;->id:I

    if-ne v0, p1, :cond_0

    :goto_0
    return-object p0

    :cond_0
    const/4 p0, 0x0

    goto :goto_0
.end method

.method protected a(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mCard:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mCard:Lcom/twitter/library/card/Card;

    invoke-virtual {v0, p0, p1, p2}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/element/Element;II)V

    :cond_0
    return-void
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 0

    return-void
.end method

.method protected a(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/element/Element;->mContext:Landroid/content/Context;

    return-void
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Element;->mPressed:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/element/Element;->c(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method a(Landroid/view/View;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    return-void
.end method

.method public a(Lcom/twitter/library/card/Card;Lcom/twitter/library/card/element/ElementState;)V
    .locals 5

    iget-object v2, p2, Lcom/twitter/library/card/element/ElementState;->a:[I

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget v0, v2, v1

    iget-object v4, p1, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Style;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/property/Style;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->r()V

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->K()V

    return-void
.end method

.method protected a(Lcom/twitter/library/card/CardView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/element/Element;->mCardView:Lcom/twitter/library/card/CardView;

    return-void
.end method

.method public a(Lcom/twitter/library/card/element/c;)V
    .locals 0

    invoke-virtual {p1, p0}, Lcom/twitter/library/card/element/c;->a(Lcom/twitter/library/card/element/Element;)V

    return-void
.end method

.method public a(Lcom/twitter/library/card/property/Style;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/element/Element;->b(Lcom/twitter/library/card/property/Style;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public a(Lcom/twitter/library/card/property/Vector2F;Lcom/twitter/library/card/property/Vector2F;)V
    .locals 4

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/twitter/library/card/element/Element;->mLayoutPosition:Lcom/twitter/library/card/property/Vector2F;

    iput-object p2, p0, Lcom/twitter/library/card/element/Element;->mLayoutSize:Lcom/twitter/library/card/property/Vector2F;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mLayoutRect:Landroid/graphics/RectF;

    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget v2, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public a([I)V
    .locals 0

    return-void
.end method

.method public b(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mMeasuredSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v0

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mMeasuredConstraint:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v1, p1}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v3

    invoke-virtual {p2, p1}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v1

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v4

    if-nez v4, :cond_0

    cmpl-float v3, v1, v3

    if-nez v3, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/property/Vector2;->a(I)I

    move-result v3

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    iget-object v4, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v4, v4, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    invoke-virtual {v4}, Ljava/lang/Float;->floatValue()F

    move-result v4

    iget-object v5, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v5, v5, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    invoke-virtual {v5}, Ljava/lang/Float;->floatValue()F

    move-result v5

    invoke-direct {v0, v4, v5}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/property/Vector2F;->a(I)F

    move-result v0

    packed-switch v3, :pswitch_data_0

    move v0, v2

    :goto_1
    :pswitch_0
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->mMeasuredSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, p1, v0}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->mMeasuredConstraint:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, p1, v1}, Lcom/twitter/library/card/property/Vector2F;->a(IF)V

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0, p1, p2}, Lcom/twitter/library/card/element/Element;->a(ILcom/twitter/library/card/property/Vector2F;)F

    move-result v0

    goto :goto_1

    :pswitch_3
    cmpl-float v2, v0, v2

    if-lez v2, :cond_1

    :goto_2
    mul-float/2addr v0, v1

    goto :goto_1

    :cond_1
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public b(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/element/ElementView;

    invoke-direct {v0, p1, p0}, Lcom/twitter/library/card/element/ElementView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Element;)V

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->mView:Landroid/view/View;

    return-void
.end method

.method public b(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v0, v0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    const/high16 v2, 0x3f800000    # 1.0f

    cmpl-float v2, v0, v2

    if-eqz v2, :cond_0

    const/high16 v2, 0x437f0000    # 255.0f

    mul-float/2addr v0, v2

    float-to-int v5, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->getHeight()I

    move-result v0

    int-to-float v4, v0

    const/4 v6, 0x4

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->saveLayerAlpha(FFFFII)I

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/card/property/Style;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public c(Landroid/graphics/Canvas;)V
    .locals 3

    sget-object v0, Lcom/twitter/library/card/element/Element;->a:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getColor()I

    move-result v0

    sget-object v1, Lcom/twitter/library/card/element/Element;->a:Landroid/graphics/Paint;

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->mCardView:Lcom/twitter/library/card/CardView;

    iget-object v2, v2, Lcom/twitter/library/card/CardView;->a:Lcom/twitter/library/card/o;

    iget v2, v2, Lcom/twitter/library/card/o;->b:I

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->mLayoutRect:Landroid/graphics/RectF;

    sget-object v2, Lcom/twitter/library/card/element/Element;->a:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    sget-object v1, Lcom/twitter/library/card/element/Element;->a:Landroid/graphics/Paint;

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    return-void
.end method

.method public c(Z)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/twitter/library/card/Card;)V
    .locals 0

    return-void
.end method

.method public d(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/twitter/library/card/element/Element;->mPressed:Z

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Element;->mPressed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mCardView:Lcom/twitter/library/card/CardView;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardView;->a()V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->K()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mCardView:Lcom/twitter/library/card/CardView;

    invoke-virtual {v0}, Lcom/twitter/library/card/CardView;->b()V

    goto :goto_0
.end method

.method public e(Lcom/twitter/library/card/Card;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/element/Element;->mCard:Lcom/twitter/library/card/Card;

    return-void
.end method

.method public e(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;->a()V

    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/element/Element;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/element/Element;

    iget v2, p0, Lcom/twitter/library/card/element/Element;->id:I

    iget v3, p1, Lcom/twitter/library/card/element/Element;->id:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    iget v3, p1, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/element/Element;->mVisible:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p1, Lcom/twitter/library/card/element/Element;->opacity:F

    iget v3, p0, Lcom/twitter/library/card/element/Element;->opacity:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/card/element/Element;->positionMode:I

    iget v3, p1, Lcom/twitter/library/card/element/Element;->positionMode:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    iget v3, p1, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    iget v3, p1, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    iget v3, p1, Lcom/twitter/library/card/element/Element;->tapActionId:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    if-eqz v2, :cond_c

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    :cond_b
    move v0, v1

    goto :goto_0

    :cond_c
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    if-nez v2, :cond_b

    :cond_d
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    if-eqz v2, :cond_f

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Spacing;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_10

    :cond_e
    move v0, v1

    goto :goto_0

    :cond_f
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    if-nez v2, :cond_e

    :cond_10
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_12

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_13

    :cond_11
    move v0, v1

    goto/16 :goto_0

    :cond_12
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    if-nez v2, :cond_11

    :cond_13
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_15

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_16

    :cond_14
    move v0, v1

    goto/16 :goto_0

    :cond_15
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    if-nez v2, :cond_14

    :cond_16
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_19

    :cond_17
    move v0, v1

    goto/16 :goto_0

    :cond_18
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    if-nez v2, :cond_17

    :cond_19
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-eqz v2, :cond_1b

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Shadow;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1c

    :cond_1a
    move v0, v1

    goto/16 :goto_0

    :cond_1b
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-nez v2, :cond_1a

    :cond_1c
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_1e

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1f

    :cond_1d
    move v0, v1

    goto/16 :goto_0

    :cond_1e
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    if-nez v2, :cond_1d

    :cond_1f
    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_20

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget-object v3, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_20
    iget-object v2, p1, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public f(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/card/element/Element;->mSerialId:I

    return-void
.end method

.method public f(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/card/element/Element;->mVisibleChild:Z

    invoke-direct {p0}, Lcom/twitter/library/card/element/Element;->a()V

    return-void
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->id:I

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Element;->positionMode:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2F;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2F;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Vector2F;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Spacing;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/element/Element;->opacity:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_7

    iget v0, p0, Lcom/twitter/library/card/element/Element;->opacity:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Shadow;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_7

    :cond_9
    move v0, v1

    goto :goto_8
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x6

    iget v1, p0, Lcom/twitter/library/card/element/Element;->doubleTapActionId:I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/card/element/Element;->a(II)V

    const/4 v0, 0x0

    return v0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x2

    iget v1, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/card/element/Element;->a(II)V

    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 2

    const/4 v0, 0x5

    iget v1, p0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/card/element/Element;->a(II)V

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v0, 0x4

    iget v1, p0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/card/element/Element;->a(II)V

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 5

    const/4 v4, 0x3

    const/4 v3, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mGestureDetector:Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {v0, p2}, Landroid/support/v4/view/GestureDetectorCompat;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    iput v1, p0, Lcom/twitter/library/card/element/Element;->mPressedX:F

    iput v2, p0, Lcom/twitter/library/card/element/Element;->mPressedY:F

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/element/Element;->d(Z)V

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    invoke-virtual {p0, v4, v0}, Lcom/twitter/library/card/element/Element;->a(II)V

    invoke-virtual {p0, v3}, Lcom/twitter/library/card/element/Element;->d(Z)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/library/card/element/Element;->mPressedX:F

    sub-float v0, v1, v0

    iget v1, p0, Lcom/twitter/library/card/element/Element;->mPressedY:F

    sub-float v1, v2, v1

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    sget v1, Lcom/twitter/library/util/b;->d:F

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    invoke-virtual {p0, v4, v0}, Lcom/twitter/library/card/element/Element;->a(II)V

    invoke-virtual {p0, v3}, Lcom/twitter/library/card/element/Element;->d(Z)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    invoke-virtual {p0, v4, v0}, Lcom/twitter/library/card/element/Element;->a(II)V

    invoke-virtual {p0, v3}, Lcom/twitter/library/card/element/Element;->d(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected p()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/Style;->a()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->positionX:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->positionY:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->sizeX:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->sizeY:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->x:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->maxSizeX:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->maxSizeY:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->margin:Lcom/twitter/library/card/property/Spacing;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget v1, p0, Lcom/twitter/library/card/element/Element;->opacity:F

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->opacity:Ljava/lang/Float;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-boolean v1, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->visible:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    iget-object v1, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    iput-object v1, v0, Lcom/twitter/library/card/property/Style;->shadow:Lcom/twitter/library/card/property/Shadow;

    return-void
.end method

.method public q()V
    .locals 0

    return-void
.end method

.method public r()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->p()V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mStyleStack:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Style;

    iget-object v2, p0, Lcom/twitter/library/card/element/Element;->mComputedStyle:Lcom/twitter/library/card/property/Style;

    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/Style;->a(Lcom/twitter/library/card/property/Style;)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->id:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->positionMode:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Spacing;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->opacity:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Shadow;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    return-void
.end method

.method protected s()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->E()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->F()V

    :cond_0
    return-void
.end method

.method protected t()V
    .locals 0

    return-void
.end method

.method public u()V
    .locals 0

    return-void
.end method

.method protected v()I
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->x:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2;->y:I

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public w()V
    .locals 2

    const/high16 v1, 0x7fc00000    # NaNf

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->mMeasuredSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v0, v1, v1}, Lcom/twitter/library/card/property/Vector2F;->set(FF)V

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/Element;->id:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Element;->positionMode:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->sizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSizeMode:Lcom/twitter/library/card/property/Vector2;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->position:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->maxSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->margin:Lcom/twitter/library/card/property/Spacing;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/Element;->opacity:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->shadow:Lcom/twitter/library/card/property/Shadow;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressDownActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Element;->pressUpActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Element;->tapActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/element/Element;->longPressActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/twitter/library/card/element/Element;->mVisible:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    return-void
.end method

.method public x()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/card/element/Element;->J()V

    return-void
.end method

.method public y()V
    .locals 0

    return-void
.end method

.method public z()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/element/Element;->mSerialId:I

    return v0
.end method
