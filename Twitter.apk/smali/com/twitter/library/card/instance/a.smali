.class public abstract Lcom/twitter/library/card/instance/a;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/card/instance/CardInstanceData;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-direct {v2}, Lcom/twitter/library/card/instance/CardInstanceData;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_7

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_7

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "binding_values"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->a(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "users"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/card/instance/a;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "forward_card"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0, p1}, Lcom/twitter/library/card/instance/a;->c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/card/instance/e;

    move-result-object v1

    iget-object v3, v1, Lcom/twitter/library/card/instance/e;->a:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    iget-object v3, v1, Lcom/twitter/library/card/instance/e;->b:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    iget-object v3, v1, Lcom/twitter/library/card/instance/e;->c:Ljava/util/HashMap;

    iput-object v3, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/twitter/library/card/instance/e;->d:Ljava/util/HashMap;

    iput-object v3, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    iget-object v3, v1, Lcom/twitter/library/card/instance/e;->e:Lcom/twitter/library/card/instance/d;

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/card/instance/e;->e:Lcom/twitter/library/card/instance/d;

    iget-object v3, v1, Lcom/twitter/library/card/instance/d;->a:Lcom/twitter/library/card/instance/f;

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/card/instance/d;->a:Lcom/twitter/library/card/instance/f;

    iget-object v3, v1, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    iget-object v3, v3, Lcom/twitter/library/card/instance/c;->a:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceName:Ljava/lang/String;

    iget-object v1, v1, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    iget-object v1, v1, Lcom/twitter/library/card/instance/c;->b:Ljava/lang/String;

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardAudienceBucket:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "card_platform"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/d;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/twitter/library/card/instance/d;->a:Lcom/twitter/library/card/instance/f;

    if-eqz v1, :cond_0

    iget-object v3, v1, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    iget-object v3, v3, Lcom/twitter/library/card/instance/c;->a:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/card/instance/CardInstanceData;->audienceName:Ljava/lang/String;

    iget-object v1, v1, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    iget-object v1, v1, Lcom/twitter/library/card/instance/c;->b:Ljava/lang/String;

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->audienceBucket:Ljava/lang/String;

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :pswitch_2
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v1, "url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p0}, Lcom/twitter/library/card/l;->T(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v1, "card_type_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->T(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    goto/16 :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_7
    invoke-static {v2}, Lcom/twitter/library/card/instance/a;->a(Lcom/twitter/library/card/instance/CardInstanceData;)Z

    move-result v0

    if-nez v0, :cond_8

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid cardInstanceData"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_8
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;
    .locals 4

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/BindingValue;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static a(Lcom/twitter/library/card/instance/CardInstanceData;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_3

    move v3, v1

    :goto_1
    if-nez v0, :cond_0

    if-eqz v3, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    return v2

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/BindingValue;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/instance/BindingValue;

    invoke-direct {v2}, Lcom/twitter/library/card/instance/BindingValue;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_4

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_4

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "image_value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/l;->C(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/property/ImageSpec;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "user_value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/UserValue;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "string_value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "scribe_key"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/BindingValue;->scribeKey:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "boolean_value"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->k()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    goto :goto_1

    :pswitch_4
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, v2, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    if-nez v0, :cond_5

    new-instance v0, Lcom/twitter/library/card/CardParserException;

    const-string/jumbo v1, "invalid binding value"

    invoke-direct {v0, v1}, Lcom/twitter/library/card/CardParserException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method protected static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/HashMap;
    .locals 4

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x1

    invoke-static {p0, v1, p1}, Lcom/twitter/library/api/ap;->a(Lcom/fasterxml/jackson/core/JsonParser;ZLcom/twitter/library/util/w;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v2, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected static c(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/UserValue;
    .locals 4

    new-instance v2, Lcom/twitter/library/card/instance/UserValue;

    invoke-direct {v2}, Lcom/twitter/library/card/instance/UserValue;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    const/4 v0, 0x0

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    const-string/jumbo v1, "id_str"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/UserValue;->id:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected static c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Lcom/twitter/library/card/instance/e;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/card/instance/e;

    invoke-direct {v2, v0}, Lcom/twitter/library/card/instance/e;-><init>(Lcom/twitter/library/card/instance/b;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_5

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_5

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "binding_values"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->a(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/e;->c:Ljava/util/HashMap;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "users"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/card/instance/a;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/util/w;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/e;->d:Ljava/util/HashMap;

    goto :goto_1

    :cond_2
    const-string/jumbo v1, "card_platform"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/d;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/e;->e:Lcom/twitter/library/card/instance/d;

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/e;->a:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string/jumbo v1, "card_type_url"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/l;->T(Lcom/fasterxml/jackson/core/JsonParser;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/e;->b:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_5
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static d(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/d;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/card/instance/d;

    invoke-direct {v2, v0}, Lcom/twitter/library/card/instance/d;-><init>(Lcom/twitter/library/card/instance/b;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "platform"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->e(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/f;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/d;->a:Lcom/twitter/library/card/instance/f;

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static e(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/f;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/card/instance/f;

    invoke-direct {v2, v0}, Lcom/twitter/library/card/instance/f;-><init>(Lcom/twitter/library/card/instance/b;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_1

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "audience"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/card/instance/a;->f(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/c;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/f;->a:Lcom/twitter/library/card/instance/c;

    goto :goto_1

    :cond_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method private static f(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/instance/c;
    .locals 4

    const/4 v0, 0x0

    new-instance v2, Lcom/twitter/library/card/instance/c;

    invoke-direct {v2, v0}, Lcom/twitter/library/card/instance/c;-><init>(Lcom/twitter/library/card/instance/b;)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    :goto_0
    if-eqz v1, :cond_2

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_2

    sget-object v3, Lcom/twitter/library/card/instance/b;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v1

    aget v1, v3, v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "name"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/c;->a:Ljava/lang/String;

    goto :goto_1

    :cond_1
    const-string/jumbo v1, "bucket"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/twitter/library/card/instance/c;->b:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    return-object v2

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
