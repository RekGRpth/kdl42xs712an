.class public Lcom/twitter/library/card/CardState;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Landroid/util/SparseArray;

.field public b:Ljava/util/HashMap;

.field public c:I

.field public d:I

.field public e:I

.field public f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/n;

    invoke-direct {v0}, Lcom/twitter/library/card/n;-><init>()V

    sput-object v0, Lcom/twitter/library/card/CardState;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/CardState;->a:Landroid/util/SparseArray;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 3

    const/4 v0, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    const-class v1, Lcom/twitter/library/card/element/ElementState;

    invoke-static {v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Landroid/os/Parcel;)Landroid/util/SparseArray;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/card/CardState;->a:Landroid/util/SparseArray;

    const-class v1, Ljava/lang/String;

    const-class v2, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v1, v2, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Landroid/os/Parcel;)Ljava/util/HashMap;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/twitter/library/card/CardState;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/twitter/library/card/CardState;->d:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    iput v1, p0, Lcom/twitter/library/card/CardState;->e:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/card/CardState;->f:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/CardState;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/card/CardState;->c:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/CardState;->a:Landroid/util/SparseArray;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Landroid/util/SparseArray;Landroid/os/Parcel;)V

    iget-object v0, p0, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Landroid/os/Parcel;)V

    iget v0, p0, Lcom/twitter/library/card/CardState;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/CardState;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/CardState;->e:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/card/CardState;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeByte(B)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
