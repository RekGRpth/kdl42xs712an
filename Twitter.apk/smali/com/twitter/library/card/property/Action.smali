.class public Lcom/twitter/library/card/property/Action;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x28673e5b3d04d3e5L


# instance fields
.field public actionType:I

.field public apiRequest:Lcom/twitter/library/card/property/ApiRequest;

.field public appId:Ljava/lang/String;

.field public appUrl:Ljava/lang/String;

.field public debugId:Ljava/lang/String;

.field public displayAppUrl:Ljava/lang/String;

.field public displayUrl:Ljava/lang/String;

.field public failureActionId:I

.field public formId:I

.field public id:I

.field public phoneNumber:Ljava/lang/String;

.field public phoneNumberUrl:Ljava/lang/String;

.field public purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

.field public scribeAction:Ljava/lang/String;

.field public scribeElement:Ljava/lang/String;

.field public stylePairs:[Lcom/twitter/library/card/property/StylePair;

.field public successActionId:I

.field public tokenizedTextId:I

.field public url:Ljava/lang/String;

.field public userId:J

.field public validationStatus:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/Action;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/Action;

    iget v2, p0, Lcom/twitter/library/card/property/Action;->actionType:I

    iget v3, p1, Lcom/twitter/library/card/property/Action;->actionType:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/property/Action;->failureActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/Action;->failureActionId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/card/property/Action;->id:I

    iget v3, p1, Lcom/twitter/library/card/property/Action;->id:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/card/property/Action;->successActionId:I

    iget v3, p1, Lcom/twitter/library/card/property/Action;->successActionId:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    iget v3, p1, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-wide v2, p0, Lcom/twitter/library/card/property/Action;->userId:J

    iget-wide v4, p1, Lcom/twitter/library/card/property/Action;->userId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/ApiRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    if-nez v2, :cond_9

    :cond_b
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/PurchaseRequest;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    if-nez v2, :cond_c

    :cond_e
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    goto :goto_0

    :cond_10
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    if-nez v2, :cond_f

    :cond_11
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    if-nez v2, :cond_12

    :cond_14
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    if-nez v2, :cond_15

    :cond_17
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    if-eqz v2, :cond_19

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1a

    :cond_18
    move v0, v1

    goto/16 :goto_0

    :cond_19
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    if-nez v2, :cond_18

    :cond_1a
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    if-eqz v2, :cond_1c

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1d

    :cond_1b
    move v0, v1

    goto/16 :goto_0

    :cond_1c
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    if-nez v2, :cond_1b

    :cond_1d
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    if-eqz v2, :cond_1f

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_20

    :cond_1e
    move v0, v1

    goto/16 :goto_0

    :cond_1f
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    if-nez v2, :cond_1e

    :cond_20
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    if-eqz v2, :cond_22

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_23

    :cond_21
    move v0, v1

    goto/16 :goto_0

    :cond_22
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    if-nez v2, :cond_21

    :cond_23
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    if-eqz v2, :cond_25

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_26

    :cond_24
    move v0, v1

    goto/16 :goto_0

    :cond_25
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    if-nez v2, :cond_24

    :cond_26
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_27

    move v0, v1

    goto/16 :goto_0

    :cond_27
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    if-eqz v2, :cond_29

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2a

    :cond_28
    move v0, v1

    goto/16 :goto_0

    :cond_29
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    if-nez v2, :cond_28

    :cond_2a
    iget v2, p0, Lcom/twitter/library/card/property/Action;->formId:I

    iget v3, p1, Lcom/twitter/library/card/property/Action;->formId:I

    if-eq v2, v3, :cond_2b

    move v0, v1

    goto/16 :goto_0

    :cond_2b
    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    if-eqz v2, :cond_2c

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_2c
    iget-object v2, p1, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 7

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/Action;->id:I

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Action;->actionType:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/twitter/library/card/property/Action;->userId:J

    iget-wide v4, p0, Lcom/twitter/library/card/property/Action;->userId:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_7
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/ApiRequest;->hashCode()I

    move-result v0

    :goto_8
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/PurchaseRequest;->hashCode()I

    move-result v0

    :goto_9
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Action;->successActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Action;->failureActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_a
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_b
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/property/Action;->formId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto/16 :goto_1

    :cond_3
    move v0, v1

    goto/16 :goto_2

    :cond_4
    move v0, v1

    goto/16 :goto_3

    :cond_5
    move v0, v1

    goto/16 :goto_4

    :cond_6
    move v0, v1

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_6

    :cond_8
    move v0, v1

    goto :goto_7

    :cond_9
    move v0, v1

    goto :goto_8

    :cond_a
    move v0, v1

    goto :goto_9

    :cond_b
    move v0, v1

    goto :goto_a

    :cond_c
    move v0, v1

    goto :goto_b
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Action;->id:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Action;->actionType:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    const-class v0, [Lcom/twitter/library/card/property/StylePair;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/StylePair;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/card/property/Action;->userId:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/ApiRequest;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/PurchaseRequest;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Action;->successActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Action;->failureActionId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Action;->formId:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/Action;->id:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/Action;->actionType:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-wide v0, p0, Lcom/twitter/library/card/property/Action;->userId:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->purchaseRequest:Lcom/twitter/library/card/property/PurchaseRequest;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/property/Action;->successActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/card/property/Action;->failureActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/card/property/Action;->formId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
