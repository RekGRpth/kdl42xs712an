.class public Lcom/twitter/library/card/property/LocalizedTokenizedText;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x508060cfbc7360adL


# instance fields
.field public tokenizedTexts:Lcom/twitter/library/card/property/a;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/card/Card;I)Ljava/lang/String;
    .locals 10

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, p2}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TokenizedText;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    return-object v0

    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, v0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    array-length v5, v4

    move v2, v1

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v0, v4, v2

    iget-boolean v6, v0, Lcom/twitter/library/card/property/TextTokenGroup;->visible:Z

    if-eqz v6, :cond_2

    iget-object v6, v0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    array-length v7, v6

    move v0, v1

    :goto_2
    if-ge v0, v7, :cond_2

    aget-object v8, v6, v0

    iget-object v9, v8, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    if-eqz v9, :cond_1

    iget-object v8, v8, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_3
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/LocalizedTokenizedText;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/LocalizedTokenizedText;

    iget-object v2, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    iget-object v3, p1, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p1, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/a;->hashCode()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    const-class v0, Lcom/twitter/library/card/property/TokenizedText;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Lcom/twitter/library/card/property/a;Ljava/io/ObjectOutput;)V

    return-void
.end method
