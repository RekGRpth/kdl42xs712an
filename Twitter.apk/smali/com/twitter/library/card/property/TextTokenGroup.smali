.class public Lcom/twitter/library/card/property/TextTokenGroup;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0xc5ca6d67767ec91L


# instance fields
.field public id:I

.field public textTokens:[Lcom/twitter/library/card/property/TextToken;

.field public visible:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->visible:Z

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/TextTokenGroup;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/TextTokenGroup;

    iget v2, p0, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    iget v3, p1, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    if-eq v2, v3, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    iget-object v3, p1, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    const-class v0, [Lcom/twitter/library/card/property/TextToken;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/TextToken;

    iput-object v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    return-void
.end method
