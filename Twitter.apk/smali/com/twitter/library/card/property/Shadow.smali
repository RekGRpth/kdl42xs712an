.class public Lcom/twitter/library/card/property/Shadow;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x5d10109d25cfb6a4L


# instance fields
.field public blurRadius:F

.field public color:I

.field public inset:Z

.field public offset:Lcom/twitter/library/card/property/Vector2F;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;FFF)V
    .locals 6

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/card/property/Shadow;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget v1, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v2, v2, p2, p3}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p0}, Lcom/twitter/library/card/property/Shadow;->b()Z

    move-result v2

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    iget-object v3, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v4, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget v5, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-virtual {v0, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    :goto_1
    invoke-virtual {p1, v1, p4, p4, v0}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v3, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->offset(FF)V

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/graphics/Canvas;FFF)V
    .locals 8

    const/4 v3, 0x1

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/card/property/Shadow;->a()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {p1, v4, v4, p2, p3}, Landroid/graphics/Canvas;->clipRect(FFFF)Z

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-virtual {v1, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/16 v0, 0x8

    new-array v2, v0, [F

    const/4 v0, 0x0

    aput p4, v2, v0

    aput p4, v2, v3

    const/4 v0, 0x2

    aput p4, v2, v0

    const/4 v0, 0x3

    aput p4, v2, v0

    const/4 v0, 0x4

    aput p4, v2, v0

    const/4 v0, 0x5

    aput p4, v2, v0

    const/4 v0, 0x6

    aput p4, v2, v0

    const/4 v0, 0x7

    aput p4, v2, v0

    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    :goto_1
    iget v3, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    sub-float v3, v0, v3

    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    cmpg-float v0, v0, v4

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    :goto_2
    iget v4, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    sub-float/2addr v0, v4

    iget v4, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    sub-float/2addr v0, v4

    invoke-static {v3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v5

    new-instance v6, Landroid/graphics/RectF;

    invoke-direct {v6, v4, v5, v4, v5}, Landroid/graphics/RectF;-><init>(FFFF)V

    invoke-virtual {p1, v3, v0}, Landroid/graphics/Canvas;->translate(FF)V

    new-instance v0, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v6, v3}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    mul-float v2, v4, v7

    add-float/2addr v2, p2

    mul-float v3, v5, v7

    add-float/2addr v3, p3

    invoke-virtual {v0, v2, v3}, Landroid/graphics/drawable/shapes/RoundRectShape;->resize(FF)V

    invoke-virtual {p0}, Lcom/twitter/library/card/property/Shadow;->b()Z

    move-result v2

    if-eqz v2, :cond_3

    iget v2, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    iget-object v3, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v4, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v4, v4, Lcom/twitter/library/card/property/Vector2F;->y:F

    iget v5, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    :goto_3
    invoke-virtual {v0, p1, v1}, Landroid/graphics/drawable/shapes/RoundRectShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    neg-float v0, v0

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v0, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    neg-float v0, v0

    goto :goto_2

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    iget-object v3, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    goto :goto_3
.end method

.method public b()Z
    .locals 2

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/Shadow;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/Shadow;

    iget v2, p1, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    iget v3, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    invoke-static {v2, v3}, Ljava/lang/Float;->compare(FF)I

    move-result v2

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget v2, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    iget v3, p1, Lcom/twitter/library/card/property/Shadow;->color:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/library/card/property/Shadow;->inset:Z

    iget-boolean v3, p1, Lcom/twitter/library/card/property/Shadow;->inset:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v0, p0, Lcom/twitter/library/card/property/Shadow;->inset:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    const/4 v3, 0x0

    cmpl-float v0, v0, v3

    if-eqz v0, :cond_2

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    invoke-static {v0}, Ljava/lang/Float;->floatToIntBits(F)I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v1}, Lcom/twitter/library/card/property/Vector2F;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/card/property/Shadow;->inset:Z

    invoke-interface {p1}, Ljava/io/ObjectInput;->readFloat()F

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->color:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/card/property/Shadow;->inset:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    iget v0, p0, Lcom/twitter/library/card/property/Shadow;->blurRadius:F

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeFloat(F)V

    iget-object v0, p0, Lcom/twitter/library/card/property/Shadow;->offset:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
