.class public Lcom/twitter/library/card/property/ImageSpec;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Externalizable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final serialVersionUID:J = -0x4bbbbc4f6a245108L


# instance fields
.field public size:Lcom/twitter/library/card/property/Vector2F;

.field public url:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/property/c;

    invoke-direct {v0}, Lcom/twitter/library/card/property/c;-><init>()V

    sput-object v0, Lcom/twitter/library/card/property/ImageSpec;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(FF)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/twitter/library/card/property/Vector2F;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/card/property/Vector2F;-><init>(FF)V

    iput-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/property/ImageSpec;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/card/property/ImageSpec;

    iget-object v2, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iget-object v3, p1, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/Vector2F;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    if-nez v2, :cond_3

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {v1}, Lcom/twitter/library/card/property/Vector2F;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Vector2F;

    iput-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    return-void
.end method
