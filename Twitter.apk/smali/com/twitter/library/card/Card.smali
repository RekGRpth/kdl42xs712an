.class public Lcom/twitter/library/card/Card;
.super Lcom/twitter/library/card/element/Container;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static c:Ljava/util/concurrent/atomic/AtomicInteger; = null

.field private static d:Z = false

.field private static e:Landroid/graphics/Paint; = null

.field private static final serialVersionUID:J = -0x65c4b3fbf2963e25L


# instance fields
.field public actions:Lcom/twitter/library/card/property/a;

.field public bindings:[Lcom/twitter/library/card/property/Binding;

.field public forms:Lcom/twitter/library/card/property/a;

.field public loadActionId:I

.field public localizedTokenizedTexts:Ljava/util/HashMap;

.field private mApiRequestParameterMap:Landroid/util/SparseArray;

.field private mBindId:I

.field private mBindingValues:Ljava/util/HashMap;

.field private mCardEventListener:Lcom/twitter/library/card/j;

.field private mDeferredActionElementSerialId:I

.field private mDeferredActionId:I

.field private mDeferredActionTouchType:I

.field private mElementMap:Landroid/util/SparseArray;

.field private mFirstBind:Z

.field private mFirstLoad:Z

.field private mIsForwardCard:Z

.field private mPurchaseRequestParameterMap:Landroid/util/SparseArray;

.field private mSerialElementMap:Landroid/util/SparseArray;

.field private mTextTokenGroupMap:Landroid/util/SparseArray;

.field private mTextTokenMap:Landroid/util/SparseArray;

.field mToRefresh:Z

.field private mTweet:Lcom/twitter/library/provider/Tweet;

.field public name:Ljava/lang/String;

.field public platformKey:Ljava/lang/String;

.field public styles:Lcom/twitter/library/card/property/a;

.field public uuid:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/twitter/library/card/Card;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/twitter/library/card/element/Container;-><init>()V

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mElementMap:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mSerialElementMap:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenGroupMap:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenMap:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mApiRequestParameterMap:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mPurchaseRequestParameterMap:Landroid/util/SparseArray;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mBindingValues:Ljava/util/HashMap;

    iput-boolean v1, p0, Lcom/twitter/library/card/Card;->mFirstBind:Z

    iput-boolean v1, p0, Lcom/twitter/library/card/Card;->mFirstLoad:Z

    return-void
.end method

.method private L()V
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionElementSerialId:I

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionTouchType:I

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionId:I

    return-void
.end method

.method private M()V
    .locals 7

    const/16 v6, 0xb

    iget-object v0, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget v4, v3, Lcom/twitter/library/card/property/Binding;->destType:I

    if-ge v4, v6, :cond_3

    invoke-virtual {p0, v3}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;)V

    :cond_2
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_3
    iget v4, v3, Lcom/twitter/library/card/property/Binding;->destType:I

    if-eq v4, v6, :cond_2

    iget v4, v3, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v5, 0xc

    if-ne v4, v5, :cond_4

    invoke-virtual {p0, v3}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/property/Binding;)V

    goto :goto_1

    :cond_4
    iget v4, v3, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v5, 0x15

    if-ge v4, v5, :cond_5

    invoke-virtual {p0, v3}, Lcom/twitter/library/card/Card;->c(Lcom/twitter/library/card/property/Binding;)V

    goto :goto_1

    :cond_5
    invoke-virtual {p0, v3}, Lcom/twitter/library/card/Card;->d(Lcom/twitter/library/card/property/Binding;)V

    goto :goto_1
.end method

.method private N()V
    .locals 7

    const/4 v6, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/a;->size()I

    move-result v3

    move v1, v2

    :goto_0
    if-ge v1, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-eqz v0, :cond_0

    const/4 v4, 0x6

    iget v5, v0, Lcom/twitter/library/card/property/Action;->actionType:I

    if-ne v4, v5, :cond_1

    iput v6, v0, Lcom/twitter/library/card/property/Action;->validationStatus:I

    new-instance v4, Lcom/twitter/library/card/f;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/twitter/library/card/f;-><init>(Lcom/twitter/library/card/a;)V

    new-array v5, v6, [Lcom/twitter/library/card/property/Action;

    aput-object v0, v5, v2

    invoke-virtual {v4, v5}, Lcom/twitter/library/card/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v4, 0x2

    iput v4, v0, Lcom/twitter/library/card/property/Action;->validationStatus:I

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/twitter/library/card/Card;)Lcom/twitter/library/card/j;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    return-object v0
.end method

.method private a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)Ljava/lang/Boolean;
    .locals 5

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    iget v0, p2, Lcom/twitter/library/card/property/Action;->actionType:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Open URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/twitter/library/card/j;->a(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Popup Menu"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->f()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->t()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->b(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/twitter/library/card/j;->a(Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_3
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Open Photo"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/element/Element;)V

    move-object v0, v1

    goto :goto_0

    :pswitch_4
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Open Profile "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v3, p2, Lcom/twitter/library/card/property/Action;->userId:J

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-wide v2, p2, Lcom/twitter/library/card/property/Action;->userId:J

    invoke-interface {v0, v2, v3}, Lcom/twitter/library/card/j;->c(J)V

    move-object v0, v1

    goto :goto_0

    :pswitch_5
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Open Store "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/twitter/library/card/j;->d(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_6
    iget v0, p2, Lcom/twitter/library/card/property/Action;->validationStatus:I

    if-eq v2, v0, :cond_4

    const/4 v0, 0x2

    iget v2, p2, Lcom/twitter/library/card/property/Action;->validationStatus:I

    if-ne v0, v2, :cond_1

    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_1
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    :goto_1
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "Open App URL "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v2, v3}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-object v3, p2, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    invoke-interface {v2, v0, v3}, Lcom/twitter/library/card/j;->c(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    move-object v0, v1

    goto/16 :goto_0

    :cond_3
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Open App URL "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " is still validating"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Apply Styles"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    invoke-direct {p0, v0}, Lcom/twitter/library/card/Card;->a([Lcom/twitter/library/card/property/StylePair;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_8
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Remove Styles"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->stylePairs:[Lcom/twitter/library/card/property/StylePair;

    invoke-direct {p0, v0}, Lcom/twitter/library/card/Card;->b([Lcom/twitter/library/card/property/StylePair;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_9
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v1, "Compose Status"

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p2, Lcom/twitter/library/card/property/Action;->tokenizedTextId:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->c(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    invoke-interface {v1, v0, v2}, Lcom/twitter/library/card/j;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_a
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Favorite Status"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0}, Lcom/twitter/library/card/j;->e()V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_b
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Retweet Status"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0}, Lcom/twitter/library/card/j;->f()V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_c
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Share Status"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0}, Lcom/twitter/library/card/j;->g()V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Dial Phone "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p2, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget-object v2, p2, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/twitter/library/card/j;->e(Ljava/lang/String;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "API Request"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0, p1, p2}, Lcom/twitter/library/card/j;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_f
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Open Tweet Detail"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0}, Lcom/twitter/library/card/j;->h()V

    move-object v0, v1

    goto/16 :goto_0

    :pswitch_10
    iget-object v0, p2, Lcom/twitter/library/card/property/Action;->debugId:Ljava/lang/String;

    const-string/jumbo v2, "Submit Form"

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)V

    move-object v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/twitter/library/card/CardDebugLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "Action: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/twitter/library/card/CardDebugLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public static a(Z)V
    .locals 0

    sput-boolean p0, Lcom/twitter/library/card/Card;->d:Z

    return-void
.end method

.method private a([Lcom/twitter/library/card/property/StylePair;)V
    .locals 6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "Applying styles to elements "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v3, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, p1, v1

    iget v4, v0, Lcom/twitter/library/card/property/StylePair;->elementId:I

    invoke-virtual {p0, v4}, Lcom/twitter/library/card/Card;->a(I)Lcom/twitter/library/card/element/Element;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    iget v0, v0, Lcom/twitter/library/card/property/StylePair;->styleId:I

    invoke-virtual {v5, v0}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Style;

    if-eqz v0, :cond_0

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/property/Style;)V

    invoke-virtual {v4}, Lcom/twitter/library/card/element/Element;->r()V

    invoke-virtual {v4}, Lcom/twitter/library/card/element/Element;->K()V

    iget-object v0, v4, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v4, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {p2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-static {p1, p3}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    return v0

    :cond_1
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Lcom/twitter/library/util/text/Plurals;->a(Ljava/lang/String;)Lcom/twitter/library/util/text/Plurals$Rule;

    move-result-object v2

    if-eq v2, p3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p2, Ljava/lang/Long;

    if-eqz v2, :cond_3

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    long-to-float v2, v2

    invoke-static {v2}, Lcom/twitter/library/util/text/Plurals;->a(F)Lcom/twitter/library/util/text/Plurals$Rule;

    move-result-object v2

    if-eq v2, p3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    instance-of v2, p2, Ljava/lang/Integer;

    if-eqz v2, :cond_4

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    int-to-float v2, v2

    invoke-static {v2}, Lcom/twitter/library/util/text/Plurals;->a(F)Lcom/twitter/library/util/text/Plurals$Rule;

    move-result-object v2

    if-eq v2, p3, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method static synthetic a(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/card/Card;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/library/card/Card;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mElementMap:Landroid/util/SparseArray;

    return-object v0
.end method

.method private b(Lcom/twitter/library/card/element/Element;)V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    const/4 v1, 0x1

    new-array v1, v1, [I

    aput v3, v1, v3

    new-instance v2, Lcom/twitter/library/card/b;

    invoke-direct {v2, p0, p1, v1, v0}, Lcom/twitter/library/card/b;-><init>(Lcom/twitter/library/card/Card;Lcom/twitter/library/card/element/Element;[ILjava/util/ArrayList;)V

    invoke-virtual {p0, v2}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/element/c;)V

    iget-object v2, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    aget v1, v1, v3

    invoke-interface {v2, v0, v1}, Lcom/twitter/library/card/j;->a(Ljava/util/ArrayList;I)V

    return-void
.end method

.method private b(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)V
    .locals 8

    const/4 v3, 0x0

    iget v0, p2, Lcom/twitter/library/card/property/Action;->formId:I

    iget-object v1, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    invoke-virtual {v1, v0}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Form;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    iget v2, v0, Lcom/twitter/library/card/property/Form;->apiRequestAction:I

    invoke-virtual {v1, v2}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/property/Action;

    iget-object v2, v1, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    if-eqz v2, :cond_0

    new-instance v5, Lcom/twitter/library/card/property/Action;

    invoke-direct {v5}, Lcom/twitter/library/card/property/Action;-><init>()V

    iget v2, v1, Lcom/twitter/library/card/property/Action;->id:I

    iput v2, v5, Lcom/twitter/library/card/property/Action;->id:I

    iget v2, v1, Lcom/twitter/library/card/property/Action;->actionType:I

    iput v2, v5, Lcom/twitter/library/card/property/Action;->actionType:I

    new-instance v2, Lcom/twitter/library/card/property/ApiRequest;

    invoke-direct {v2}, Lcom/twitter/library/card/property/ApiRequest;-><init>()V

    iput-object v2, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v2, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v4, v1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget v4, v4, Lcom/twitter/library/card/property/ApiRequest;->method:I

    iput v4, v2, Lcom/twitter/library/card/property/ApiRequest;->method:I

    iget-object v2, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v4, v1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget v4, v4, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    iput v4, v2, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    iget-object v2, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v4, v1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget v4, v4, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    iput v4, v2, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    iget-object v2, v1, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    iput-object v2, v5, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    iget-object v2, v1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v2, v2, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    if-eqz v2, :cond_5

    iget-object v2, v1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v2, v2, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    array-length v2, v2

    :goto_1
    iget-object v4, v0, Lcom/twitter/library/card/property/Form;->elementIds:[I

    if-eqz v4, :cond_4

    iget-object v4, v0, Lcom/twitter/library/card/property/Form;->elementIds:[I

    array-length v4, v4

    :goto_2
    iget-object v6, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    add-int v7, v2, v4

    new-array v7, v7, [Lcom/twitter/library/card/property/ApiRequestParameter;

    iput-object v7, v6, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    if-lez v2, :cond_2

    iget-object v1, v1, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v1, v1, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    iget-object v6, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v6, v6, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-static {v1, v3, v6, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_3
    if-ge v3, v4, :cond_3

    iget-object v1, v0, Lcom/twitter/library/card/property/Form;->elementIds:[I

    aget v6, v1, v3

    invoke-virtual {p0, v6}, Lcom/twitter/library/card/Card;->a(I)Lcom/twitter/library/card/element/Element;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/element/FormFieldElement;

    if-eqz v1, :cond_0

    new-instance v7, Lcom/twitter/library/card/property/ApiRequestParameter;

    invoke-direct {v7}, Lcom/twitter/library/card/property/ApiRequestParameter;-><init>()V

    iput v6, v7, Lcom/twitter/library/card/property/ApiRequestParameter;->id:I

    invoke-virtual {v1}, Lcom/twitter/library/card/element/FormFieldElement;->b()Ljava/lang/String;

    move-result-object v6

    iput-object v6, v7, Lcom/twitter/library/card/property/ApiRequestParameter;->key:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/twitter/library/card/element/FormFieldElement;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v7, Lcom/twitter/library/card/property/ApiRequestParameter;->value:Ljava/lang/String;

    iget-object v1, v5, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget-object v1, v1, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    add-int v6, v3, v2

    aput-object v7, v1, v6

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0, p1, v5}, Lcom/twitter/library/card/j;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)V

    goto/16 :goto_0

    :cond_4
    move v4, v3

    goto :goto_2

    :cond_5
    move v2, v3

    goto :goto_1
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->C()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lia;->card_long_press_options:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/card/c;

    invoke-direct {v2, p0, p1, p2}, Lcom/twitter/library/card/c;-><init>(Lcom/twitter/library/card/Card;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setItems([Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private b([Lcom/twitter/library/card/property/StylePair;)V
    .locals 6

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "Removing styles from elements "

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    array-length v3, p1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, p1, v1

    iget v4, v0, Lcom/twitter/library/card/property/StylePair;->elementId:I

    invoke-virtual {p0, v4}, Lcom/twitter/library/card/Card;->a(I)Lcom/twitter/library/card/element/Element;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    iget v0, v0, Lcom/twitter/library/card/property/StylePair;->styleId:I

    invoke-virtual {v5, v0}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Style;

    if-eqz v0, :cond_0

    invoke-virtual {v4, v0}, Lcom/twitter/library/card/element/Element;->b(Lcom/twitter/library/card/property/Style;)V

    invoke-virtual {v4}, Lcom/twitter/library/card/element/Element;->r()V

    invoke-virtual {v4}, Lcom/twitter/library/card/element/Element;->K()V

    iget-object v0, v4, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, v4, Lcom/twitter/library/card/element/Element;->debugId:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v4, ", "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/card/CardDebugLog;->a(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/String;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/library/card/Card;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/library/card/Card;)Landroid/util/SparseArray;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mSerialElementMap:Landroid/util/SparseArray;

    return-object v0
.end method

.method private static c(Ljava/lang/String;)Z
    .locals 4

    const/4 v2, 0x0

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    :try_start_1
    invoke-virtual {v0, v1}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    const/16 v3, 0x12d

    if-eq v1, v3, :cond_0

    const/16 v3, 0x12e

    if-ne v1, v3, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    :try_start_2
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    move v0, v1

    :goto_1
    return v0

    :catchall_0
    move-exception v1

    :try_start_3
    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    throw v1
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    move v0, v2

    goto :goto_1

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_1

    :cond_1
    move v1, v2

    goto :goto_0
.end method

.method private d(Landroid/graphics/Canvas;)V
    .locals 8

    const/16 v4, 0xa0

    const/4 v7, 0x1

    const/high16 v5, 0x3f800000    # 1.0f

    const/high16 v6, 0x40800000    # 4.0f

    const/16 v3, 0xff

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    const/16 v0, 0xb0

    const/4 v1, 0x0

    invoke-static {v0, v3, v1, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    invoke-static {v3, v3, v4, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    const/16 v2, 0xc0

    invoke-static {v2, v3, v4, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v2

    sget-object v3, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    if-nez v3, :cond_0

    new-instance v3, Landroid/graphics/Paint;

    invoke-direct {v3, v7}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v3, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    :cond_0
    sget-object v3, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {v3}, Landroid/graphics/Paint;->clearShadowLayer()V

    sget-object v3, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {v3, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mLayoutRect:Landroid/graphics/RectF;

    sget-object v3, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    sget-object v0, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    const/high16 v3, 0x41400000    # 12.0f

    sget v4, Lcom/twitter/library/util/b;->b:F

    mul-float/2addr v3, v4

    invoke-virtual {v0, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    sget-object v0, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v7}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    sget-object v0, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    const/high16 v3, 0x40400000    # 3.0f

    const/high16 v4, -0x1000000

    invoke-virtual {v0, v3, v5, v5, v4}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    sget-object v0, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v0, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontSpacing()F

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "name="

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v0, v3}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-float v1, v0, v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "status="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/card/Card;->mTweet:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v6, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-float/2addr v1, v0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "platKey="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v6, v1, v4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-float/2addr v0, v1

    sget-object v1, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {v1, v2}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->C()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/aa;->a()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "currCardPlat="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/card/Card;->e:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v6, v0, v2}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method

.method private f(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-nez p2, :cond_1

    move v0, v1

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    return v0

    :cond_1
    instance-of v2, p2, Ljava/lang/String;

    if-eqz v2, :cond_2

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    instance-of v2, p2, Ljava/lang/Double;

    if-eqz v2, :cond_3

    check-cast p2, Ljava/lang/Double;

    invoke-virtual {p2}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmpl-double v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_3
    instance-of v2, p2, Ljava/lang/Float;

    if-eqz v2, :cond_4

    check-cast p2, Ljava/lang/Float;

    invoke-virtual {p2}, Ljava/lang/Float;->floatValue()F

    move-result v2

    const/4 v3, 0x0

    cmpl-float v2, v2, v3

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_4
    instance-of v2, p2, Ljava/lang/Long;

    if-eqz v2, :cond_5

    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_5
    instance-of v2, p2, Ljava/lang/Integer;

    if-eqz v2, :cond_6

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_6
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/Card;->mBindId:I

    return v0
.end method

.method public declared-synchronized a(Landroid/content/Context;Landroid/view/ViewGroup;IJLcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/aa;Lcom/twitter/library/util/as;Lcom/twitter/library/card/CardState;Lcom/twitter/library/card/j;Z)Lcom/twitter/library/card/Card;
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0, p1}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;)V

    iput-object p6, p0, Lcom/twitter/library/card/Card;->mTweet:Lcom/twitter/library/provider/Tweet;

    iput-boolean p11, p0, Lcom/twitter/library/card/Card;->mIsForwardCard:Z

    iget-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstBind:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->l()V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->m()V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->n()V

    :cond_0
    invoke-virtual {p0, p1, p6}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)V

    iget-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstBind:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/twitter/library/card/Card;->a(Landroid/view/ViewGroup;I)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mView:Landroid/view/View;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mView:Landroid/view/View;

    check-cast v0, Lcom/twitter/library/card/CardView;

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/CardView;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardView:Lcom/twitter/library/card/CardView;

    invoke-virtual {v0, p0}, Lcom/twitter/library/card/CardView;->setCard(Lcom/twitter/library/card/Card;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/Card;->mView:Landroid/view/View;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    invoke-virtual {p0, p0}, Lcom/twitter/library/card/Card;->d(Lcom/twitter/library/card/Card;)V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->r()V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->x()V

    iget-object v0, p6, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {p0, p9, p1, p6, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/CardState;Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/instance/CardInstanceData;)V

    invoke-virtual {p0, p10}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/j;)V

    invoke-virtual {p0, p4, p5, p7, p8}, Lcom/twitter/library/card/Card;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstBind:Z

    sget-object v0, Lcom/twitter/library/card/Card;->c:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/Card;->mBindId:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, p0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(I)Lcom/twitter/library/card/element/Element;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mElementMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    return-object v0
.end method

.method protected a(Lcom/twitter/library/card/property/Binding;Lcom/twitter/library/provider/Tweet;)Ljava/lang/Object;
    .locals 2

    iget v0, p1, Lcom/twitter/library/card/property/Binding;->sourceType:I

    packed-switch v0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->q:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->j:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->x:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    iget-wide v0, p2, Lcom/twitter/library/provider/Tweet;->y:D

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    iget v0, p2, Lcom/twitter/library/provider/Tweet;->V:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    iget-boolean v0, p2, Lcom/twitter/library/provider/Tweet;->l:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method protected a(Lcom/twitter/library/card/property/Binding;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/lang/Object;
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p1, Lcom/twitter/library/card/property/Binding;->sourceKey:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v0}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-object v3

    :cond_1
    instance-of v1, v0, Lcom/twitter/library/card/instance/UserValue;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/library/card/instance/UserValue;

    iget-object v1, v0, Lcom/twitter/library/card/instance/UserValue;->id:Ljava/lang/String;

    invoke-virtual {p3, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_0

    iget v4, p1, Lcom/twitter/library/card/property/Binding;->sourceType:I

    packed-switch v4, :pswitch_data_0

    move-object v0, v3

    :goto_1
    move-object v3, v0

    goto :goto_0

    :pswitch_0
    iget-object v0, v0, Lcom/twitter/library/card/instance/UserValue;->id:Ljava/lang/String;

    goto :goto_1

    :pswitch_1
    iget-object v0, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->C()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lie;->card_profile_image_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lie;->card_profile_image_height:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    new-instance v0, Lcom/twitter/library/card/property/ImageSpec;

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/card/property/ImageSpec;-><init>(FF)V

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->C()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lie;->card_banner_image_width:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    sget v3, Lie;->card_banner_image_height:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    new-instance v0, Lcom/twitter/library/card/property/ImageSpec;

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/card/property/ImageSpec;-><init>(FF)V

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    sget v2, Lcom/twitter/library/util/b;->a:F

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;F)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    goto :goto_1

    :pswitch_4
    iget-object v0, v1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    goto :goto_1

    :pswitch_5
    iget-object v0, v1, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    goto :goto_1

    :pswitch_6
    iget-object v0, v1, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    goto :goto_1

    :pswitch_7
    iget-object v0, v1, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    goto :goto_1

    :pswitch_8
    iget-boolean v0, v1, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :pswitch_9
    iget-boolean v0, v1, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :pswitch_a
    iget v0, v1, Lcom/twitter/library/api/TwitterUser;->friendship:I

    if-ne v0, v2, :cond_2

    move v0, v2

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :pswitch_b
    iget v0, v1, Lcom/twitter/library/api/TwitterUser;->statusesCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :pswitch_c
    iget v0, v1, Lcom/twitter/library/api/TwitterUser;->friendsCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_1

    :pswitch_d
    iget v0, v1, Lcom/twitter/library/api/TwitterUser;->followersCount:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method protected a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mBindingValues:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    :cond_0
    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(II)V
    .locals 0

    invoke-virtual {p0, p0, p1, p2}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/element/Element;II)V

    return-void
.end method

.method public a(IIZ)V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardView:Lcom/twitter/library/card/CardView;

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/library/card/Card;->b(I)Lcom/twitter/library/card/element/Element;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, p2}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-eqz v0, :cond_0

    if-eqz p3, :cond_1

    iget-object v0, v0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget v0, v0, Lcom/twitter/library/card/property/ApiRequest;->successActionId:I

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/card/Card;->a(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    iget v0, v0, Lcom/twitter/library/card/property/ApiRequest;->failureActionId:I

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/card/Card;->a(II)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)V
    .locals 10

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-boolean v1, p0, Lcom/twitter/library/card/Card;->mIsForwardCard:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardBindingValues:Ljava/util/HashMap;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    move-object v2, v0

    move-object v3, v1

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    if-nez v0, :cond_1

    :goto_1
    return-void

    :cond_0
    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    move-object v2, v0

    move-object v3, v1

    goto :goto_0

    :cond_1
    iget-object v7, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    array-length v8, v7

    const/4 v0, 0x0

    move v6, v0

    :goto_2
    if-ge v6, v8, :cond_d

    aget-object v9, v7, v6

    const/4 v0, 0x0

    const/4 v4, 0x0

    invoke-virtual {v9}, Lcom/twitter/library/card/property/Binding;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    move-object v1, v4

    :goto_3
    const/4 v4, 0x0

    iget v5, v9, Lcom/twitter/library/card/property/Binding;->controllerType:I

    packed-switch v5, :pswitch_data_1

    :goto_4
    if-nez v0, :cond_7

    :goto_5
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_2

    :pswitch_0
    iget-object v0, v9, Lcom/twitter/library/card/property/Binding;->sourceKey:Ljava/lang/String;

    const/16 v1, 0x2c

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(I)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    iget-object v1, v9, Lcom/twitter/library/card/property/Binding;->sourceKey:Ljava/lang/String;

    const/4 v0, 0x0

    :goto_6
    invoke-virtual {p0, v1, v3}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/Object;

    move-result-object v5

    if-eqz v0, :cond_f

    invoke-virtual {p0, v0, v3}, Lcom/twitter/library/card/Card;->a(Ljava/lang/String;Ljava/util/HashMap;)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v5

    goto :goto_3

    :cond_2
    iget-object v1, v9, Lcom/twitter/library/card/property/Binding;->sourceKey:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v1, v5, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v5, v9, Lcom/twitter/library/card/property/Binding;->sourceKey:Ljava/lang/String;

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {v5, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_6

    :pswitch_1
    invoke-virtual {p0, v9, v3, v2}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/util/HashMap;Ljava/util/HashMap;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v4

    goto :goto_3

    :pswitch_2
    invoke-virtual {p0, v9, p2}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Lcom/twitter/library/provider/Tweet;)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v4

    goto :goto_3

    :pswitch_3
    if-eqz v0, :cond_3

    const/4 v0, 0x1

    :goto_7
    invoke-virtual {v9, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_4

    :cond_3
    const/4 v0, 0x0

    goto :goto_7

    :pswitch_4
    if-eqz v1, :cond_5

    instance-of v4, v1, Ljava/lang/String;

    if-eqz v4, :cond_5

    if-eqz v0, :cond_4

    instance-of v4, v0, Ljava/lang/String;

    if-eqz v4, :cond_4

    check-cast v0, Ljava/lang/String;

    move-object v4, v0

    :goto_8
    move-object v0, v1

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, p1, v4, v0}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_4

    :cond_4
    const/4 v0, 0x0

    move-object v4, v0

    goto :goto_8

    :cond_5
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_4

    :pswitch_5
    if-eqz v0, :cond_6

    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_6

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/library/card/Card;->b(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v9, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :cond_6
    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Lcom/twitter/library/card/property/Binding;->a(Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_6
    invoke-direct {p0, v9, v0}, Lcom/twitter/library/card/Card;->f(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_7
    if-eqz v0, :cond_e

    instance-of v1, v0, Ljava/lang/Long;

    if-eqz v1, :cond_e

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v1, v4, v5}, Lcom/twitter/library/util/Util;->b(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_8
    sget-object v1, Lcom/twitter/library/util/text/Plurals$Rule;->a:Lcom/twitter/library/util/text/Plurals$Rule;

    invoke-direct {p0, v9, v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_9
    sget-object v1, Lcom/twitter/library/util/text/Plurals$Rule;->b:Lcom/twitter/library/util/text/Plurals$Rule;

    invoke-direct {p0, v9, v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_a
    sget-object v1, Lcom/twitter/library/util/text/Plurals$Rule;->c:Lcom/twitter/library/util/text/Plurals$Rule;

    invoke-direct {p0, v9, v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_b
    sget-object v1, Lcom/twitter/library/util/text/Plurals$Rule;->d:Lcom/twitter/library/util/text/Plurals$Rule;

    invoke-direct {p0, v9, v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_c
    sget-object v1, Lcom/twitter/library/util/text/Plurals$Rule;->e:Lcom/twitter/library/util/text/Plurals$Rule;

    invoke-direct {p0, v9, v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :pswitch_d
    sget-object v1, Lcom/twitter/library/util/text/Plurals$Rule;->f:Lcom/twitter/library/util/text/Plurals$Rule;

    invoke-direct {p0, v9, v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Lcom/twitter/library/util/text/Plurals$Rule;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto/16 :goto_4

    :cond_7
    iget v1, v9, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v4, 0xb

    if-ge v1, v4, :cond_8

    invoke-virtual {p0, v9, v0, v2}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Ljava/util/HashMap;)V

    goto/16 :goto_5

    :cond_8
    iget v1, v9, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v4, 0xb

    if-ne v1, v4, :cond_9

    invoke-virtual {p0, v9, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_9
    iget v1, v9, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v4, 0xc

    if-ne v1, v4, :cond_a

    invoke-virtual {p0, v9, v0}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_a
    iget v1, v9, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v4, 0x15

    if-ge v1, v4, :cond_b

    invoke-virtual {p0, v9, v0}, Lcom/twitter/library/card/Card;->c(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_b
    iget v1, v9, Lcom/twitter/library/card/property/Binding;->destType:I

    const/16 v4, 0x17

    if-ge v1, v4, :cond_c

    invoke-virtual {p0, v9, v0}, Lcom/twitter/library/card/Card;->d(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_c
    invoke-virtual {p0, v9, v0}, Lcom/twitter/library/card/Card;->e(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V

    goto/16 :goto_5

    :cond_d
    invoke-direct {p0}, Lcom/twitter/library/card/Card;->N()V

    goto/16 :goto_1

    :cond_e
    move-object v0, v4

    goto/16 :goto_4

    :cond_f
    move-object v1, v4

    move-object v0, v5

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 8

    iget-boolean v0, p0, Lcom/twitter/library/card/Card;->mToRefresh:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/card/e;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/card/e;-><init>(Lcom/twitter/library/card/Card;Landroid/content/Context;Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/e;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method public a(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Container;->a(Landroid/graphics/Canvas;)V

    sget-boolean v0, Lcom/twitter/library/card/Card;->d:Z

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/library/card/Card;->d(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewGroup;I)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p2, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/card/Card;->mView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->c(Landroid/content/Context;)V

    return-void
.end method

.method public a(Lcom/twitter/library/card/CardState;Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/instance/CardInstanceData;)V
    .locals 5

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, p1, Lcom/twitter/library/card/CardState;->a:Landroid/util/SparseArray;

    iget-object v0, p1, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mBindingValues:Ljava/util/HashMap;

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mBindingValues:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0, p2, p3}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {p0, p0}, Lcom/twitter/library/card/Card;->d(Lcom/twitter/library/card/Card;)V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->r()V

    :cond_1
    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_3

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/ElementState;

    invoke-virtual {p0, v4}, Lcom/twitter/library/card/Card;->a(I)Lcom/twitter/library/card/element/Element;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-virtual {v4, p0, v0}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/Card;Lcom/twitter/library/card/element/ElementState;)V

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget v0, p1, Lcom/twitter/library/card/CardState;->c:I

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionElementSerialId:I

    iget v0, p1, Lcom/twitter/library/card/CardState;->d:I

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionTouchType:I

    iget v0, p1, Lcom/twitter/library/card/CardState;->e:I

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionId:I

    iget-boolean v0, p1, Lcom/twitter/library/card/CardState;->f:Z

    iput-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstLoad:Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/element/Element;II)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionElementSerialId:I

    if-nez v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/card/element/Element;->z()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionElementSerialId:I

    iput p2, p0, Lcom/twitter/library/card/Card;->mDeferredActionTouchType:I

    iput p3, p0, Lcom/twitter/library/card/Card;->mDeferredActionId:I

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/j;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    return-void
.end method

.method protected a(Lcom/twitter/library/card/property/Binding;)V
    .locals 2

    iget v0, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->a(I)Lcom/twitter/library/card/element/Element;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_1
    check-cast v0, Lcom/twitter/library/card/element/Image;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_2
    check-cast v0, Lcom/twitter/library/card/element/Image;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/element/Image;->specFullSize:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0

    :pswitch_3
    check-cast v0, Lcom/twitter/library/card/element/Player;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    check-cast v0, Lcom/twitter/library/card/element/Player;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    check-cast v0, Lcom/twitter/library/card/element/Player;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    check-cast v0, Lcom/twitter/library/card/element/Player;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/element/Player;->spec:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0

    :pswitch_7
    check-cast v0, Lcom/twitter/library/card/element/FollowButtonElement;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/element/FollowButtonElement;->a(Lcom/twitter/library/api/TwitterUser;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenGroupMap:Landroid/util/SparseArray;

    iget v2, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TextTokenGroup;

    iput-boolean v1, v0, Lcom/twitter/library/card/property/TextTokenGroup;->visible:Z
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;Ljava/util/HashMap;)V
    .locals 2

    iget v0, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->a(I)Lcom/twitter/library/card/element/Element;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/element/Element;->e(Z)V

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    check-cast v0, Lcom/twitter/library/card/element/Image;

    check-cast p2, Lcom/twitter/library/card/property/ImageSpec;

    iput-object p2, v0, Lcom/twitter/library/card/element/Image;->spec:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :pswitch_2
    check-cast v0, Lcom/twitter/library/card/element/Image;

    check-cast p2, Lcom/twitter/library/card/property/ImageSpec;

    iput-object p2, v0, Lcom/twitter/library/card/element/Image;->specFullSize:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0

    :pswitch_3
    check-cast v0, Lcom/twitter/library/card/element/Player;

    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    check-cast v0, Lcom/twitter/library/card/element/Player;

    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/element/Player;->streamContentType:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    check-cast v0, Lcom/twitter/library/card/element/Player;

    iget-object v0, v0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, v0, Lcom/twitter/library/card/property/Vector2F;->x:F

    goto :goto_0

    :pswitch_6
    check-cast v0, Lcom/twitter/library/card/element/Player;

    iget-object v0, v0, Lcom/twitter/library/card/element/Player;->streamSize:Lcom/twitter/library/card/property/Vector2F;

    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    iput v1, v0, Lcom/twitter/library/card/property/Vector2F;->y:F

    goto :goto_0

    :pswitch_7
    check-cast v0, Lcom/twitter/library/card/element/Player;

    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_8
    check-cast v0, Lcom/twitter/library/card/element/Player;

    check-cast p2, Lcom/twitter/library/card/property/ImageSpec;

    iput-object p2, v0, Lcom/twitter/library/card/element/Player;->spec:Lcom/twitter/library/card/property/ImageSpec;

    goto :goto_0

    :pswitch_9
    check-cast v0, Lcom/twitter/library/card/element/FollowButtonElement;

    check-cast p2, Ljava/lang/String;

    invoke-virtual {p3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/element/FollowButtonElement;->a(Lcom/twitter/library/api/TwitterUser;)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public declared-synchronized a(Lcom/twitter/library/provider/Tweet;)V
    .locals 1

    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/twitter/library/card/Card;->mTweet:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->x()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/card/instance/BindingValue;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mBindingValues:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/card/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public b()Lcom/twitter/library/card/CardState;
    .locals 2

    new-instance v0, Lcom/twitter/library/card/CardState;

    invoke-direct {v0}, Lcom/twitter/library/card/CardState;-><init>()V

    new-instance v1, Lcom/twitter/library/card/a;

    invoke-direct {v1, p0, v0}, Lcom/twitter/library/card/a;-><init>(Lcom/twitter/library/card/Card;Lcom/twitter/library/card/CardState;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/element/c;)V

    iget-object v1, p0, Lcom/twitter/library/card/Card;->mBindingValues:Ljava/util/HashMap;

    iput-object v1, v0, Lcom/twitter/library/card/CardState;->b:Ljava/util/HashMap;

    iget v1, p0, Lcom/twitter/library/card/Card;->mDeferredActionElementSerialId:I

    iput v1, v0, Lcom/twitter/library/card/CardState;->c:I

    iget v1, p0, Lcom/twitter/library/card/Card;->mDeferredActionTouchType:I

    iput v1, v0, Lcom/twitter/library/card/CardState;->d:I

    iget v1, p0, Lcom/twitter/library/card/Card;->mDeferredActionId:I

    iput v1, v0, Lcom/twitter/library/card/CardState;->e:I

    iget-boolean v1, p0, Lcom/twitter/library/card/Card;->mFirstLoad:Z

    iput-boolean v1, v0, Lcom/twitter/library/card/CardState;->f:Z

    return-object v0
.end method

.method public b(I)Lcom/twitter/library/card/element/Element;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mSerialElementMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/element/Element;

    return-object v0
.end method

.method public b(Lcom/twitter/library/card/element/Element;II)V
    .locals 3

    if-nez p3, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, p3}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    :goto_1
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    iget v2, v0, Lcom/twitter/library/card/property/Action;->id:I

    invoke-interface {v1, p1, p2, v2}, Lcom/twitter/library/card/j;->a(Lcom/twitter/library/card/element/Element;II)V

    :cond_2
    invoke-direct {p0, p1, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)Ljava/lang/Boolean;

    move-result-object v1

    if-nez v1, :cond_3

    iget v0, v0, Lcom/twitter/library/card/property/Action;->id:I

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/element/Element;II)V

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_4

    iget v0, v0, Lcom/twitter/library/card/property/Action;->successActionId:I

    :goto_2
    invoke-virtual {v2, v0}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    goto :goto_1

    :cond_4
    iget v0, v0, Lcom/twitter/library/card/property/Action;->failureActionId:I

    goto :goto_2
.end method

.method protected b(Lcom/twitter/library/card/property/Binding;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenMap:Landroid/util/SparseArray;

    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TextToken;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method protected b(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    instance-of v0, p2, Ljava/lang/String;

    if-nez v0, :cond_2

    instance-of v0, p2, Ljava/lang/Integer;

    if-nez v0, :cond_2

    instance-of v0, p2, Ljava/lang/Long;

    if-nez v0, :cond_2

    instance-of v0, p2, Ljava/lang/Float;

    if-nez v0, :cond_2

    instance-of v0, p2, Ljava/lang/Double;

    if-nez v0, :cond_2

    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    :cond_2
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenMap:Landroid/util/SparseArray;

    iget v2, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TextToken;

    iput-object v1, v0, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    goto :goto_0
.end method

.method public b(Z)V
    .locals 4

    iget v0, p0, Lcom/twitter/library/card/Card;->mDeferredActionElementSerialId:I

    iget v1, p0, Lcom/twitter/library/card/Card;->mDeferredActionTouchType:I

    iget v2, p0, Lcom/twitter/library/card/Card;->mDeferredActionId:I

    invoke-direct {p0}, Lcom/twitter/library/card/Card;->L()V

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->b(I)Lcom/twitter/library/card/element/Element;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, v2}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-eqz p1, :cond_1

    iget v0, v0, Lcom/twitter/library/card/property/Action;->successActionId:I

    :goto_0
    invoke-virtual {p0, v3, v1, v0}, Lcom/twitter/library/card/Card;->b(Lcom/twitter/library/card/element/Element;II)V

    :cond_0
    return-void

    :cond_1
    iget v0, v0, Lcom/twitter/library/card/property/Action;->failureActionId:I

    goto :goto_0
.end method

.method public c(I)Ljava/lang/String;
    .locals 10

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, v0, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TokenizedText;

    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_2

    aget-object v0, v4, v2

    iget-boolean v6, v0, Lcom/twitter/library/card/property/TextTokenGroup;->visible:Z

    if-eqz v6, :cond_1

    iget-object v6, v0, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    array-length v7, v6

    move v0, v1

    :goto_1
    if-ge v0, v7, :cond_1

    aget-object v8, v6, v0

    iget-object v9, v8, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    if-eqz v9, :cond_0

    iget-object v8, v8, Lcom/twitter/library/card/property/TextToken;->text:Ljava/lang/String;

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected c(Lcom/twitter/library/card/property/Binding;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    const-wide/16 v1, 0x0

    iput-wide v1, v0, Lcom/twitter/library/card/property/Action;->userId:J

    goto :goto_0

    :pswitch_3
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method protected c(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->url:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->displayUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_1

    check-cast p2, Ljava/lang/String;

    invoke-static {p2}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/twitter/library/card/property/Action;->userId:J

    goto :goto_0

    :cond_1
    check-cast p2, Ljava/lang/Long;

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iput-wide v1, v0, Lcom/twitter/library/card/property/Action;->userId:J

    goto :goto_0

    :pswitch_3
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->appId:Ljava/lang/String;

    goto :goto_0

    :pswitch_4
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->appUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_5
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->displayAppUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_6
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->phoneNumberUrl:Ljava/lang/String;

    goto :goto_0

    :pswitch_7
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/Action;->phoneNumber:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/card/Card;->mIsForwardCard:Z

    return v0
.end method

.method public d()Lcom/twitter/library/card/CardView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardView:Lcom/twitter/library/card/CardView;

    return-object v0
.end method

.method public d(I)Lcom/twitter/library/card/property/ApiRequestParameter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mApiRequestParameterMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/ApiRequestParameter;

    return-object v0
.end method

.method protected d(Lcom/twitter/library/card/property/Binding;)V
    .locals 2

    iget v0, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->d(I)Lcom/twitter/library/card/property/ApiRequestParameter;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/ApiRequestParameter;->key:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/card/property/ApiRequestParameter;->value:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected d(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V
    .locals 2

    iget v0, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->d(I)Lcom/twitter/library/card/property/ApiRequestParameter;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/ApiRequestParameter;->key:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/ApiRequestParameter;->value:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public e()Lcom/twitter/library/card/j;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    return-object v0
.end method

.method public e(I)Lcom/twitter/library/card/property/PurchaseRequestParameter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mPurchaseRequestParameterMap:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/PurchaseRequestParameter;

    return-object v0
.end method

.method protected e(Lcom/twitter/library/card/property/Binding;Ljava/lang/Object;)V
    .locals 2

    iget v0, p1, Lcom/twitter/library/card/property/Binding;->destId:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->e(I)Lcom/twitter/library/card/property/PurchaseRequestParameter;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget v1, p1, Lcom/twitter/library/card/property/Binding;->destType:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/PurchaseRequestParameter;->key:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    check-cast p2, Ljava/lang/String;

    iput-object p2, v0, Lcom/twitter/library/card/property/PurchaseRequestParameter;->value:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/card/Card;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Container;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/card/Card;

    iget v2, p0, Lcom/twitter/library/card/Card;->loadActionId:I

    iget v3, p1, Lcom/twitter/library/card/Card;->loadActionId:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    if-nez v2, :cond_5

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    :cond_9
    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p1, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    if-nez v2, :cond_9

    :cond_b
    iget-object v2, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p1, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    if-nez v2, :cond_c

    :cond_e
    iget-object v2, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    if-eqz v2, :cond_10

    iget-object v2, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_11

    :cond_f
    move v0, v1

    goto :goto_0

    :cond_10
    iget-object v2, p1, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    if-nez v2, :cond_f

    :cond_11
    iget-object v2, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_13

    iget-object v2, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_14

    :cond_12
    move v0, v1

    goto/16 :goto_0

    :cond_13
    iget-object v2, p1, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    if-nez v2, :cond_12

    :cond_14
    iget-object v2, p0, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    if-eqz v2, :cond_16

    iget-object v2, p0, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_17

    :cond_15
    move v0, v1

    goto/16 :goto_0

    :cond_16
    iget-object v2, p1, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    if-nez v2, :cond_15

    :cond_17
    iget-object v2, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_18

    iget-object v2, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    iget-object v3, p1, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/property/a;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_18
    iget-object v2, p1, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public f()Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mTweet:Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method public g()Lcom/twitter/library/card/property/LocalizedTokenizedText;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->C()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/LocalizedTokenizedText;

    if-nez v0, :cond_0

    const-string/jumbo v0, "nb"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "no"

    :goto_1
    iget-object v1, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/LocalizedTokenizedText;

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "ms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "msa"

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "en"

    goto :goto_1
.end method

.method public declared-synchronized h()V
    .locals 1

    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->y()V

    invoke-direct {p0}, Lcom/twitter/library/card/Card;->M()V

    invoke-direct {p0}, Lcom/twitter/library/card/Card;->L()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/card/Card;->mTweet:Lcom/twitter/library/provider/Tweet;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstLoad:Z

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/card/Card;->mBindId:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    invoke-super {p0}, Lcom/twitter/library/card/element/Container;->hashCode()I

    move-result v0

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_3
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/a;->hashCode()I

    move-result v0

    :goto_4
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/a;->hashCode()I

    move-result v0

    :goto_5
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->hashCode()I

    move-result v0

    :goto_6
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/card/Card;->loadActionId:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    invoke-virtual {v1}, Lcom/twitter/library/card/property/a;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_3

    :cond_5
    move v0, v1

    goto :goto_4

    :cond_6
    move v0, v1

    goto :goto_5

    :cond_7
    move v0, v1

    goto :goto_6
.end method

.method public i()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->s()V

    return-void
.end method

.method public j()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->w()V

    iget-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstLoad:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/card/Card;->mFirstLoad:Z

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/library/card/Card;->loadActionId:I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/card/Card;->a(II)V

    :cond_0
    return-void
.end method

.method public k()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mCardEventListener:Lcom/twitter/library/card/j;

    invoke-interface {v0}, Lcom/twitter/library/card/j;->j()V

    return-void
.end method

.method protected l()V
    .locals 1

    new-instance v0, Lcom/twitter/library/card/d;

    invoke-direct {v0, p0}, Lcom/twitter/library/card/d;-><init>(Lcom/twitter/library/card/Card;)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/element/c;)V

    return-void
.end method

.method protected m()V
    .locals 14

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/card/Card;->g()Lcom/twitter/library/card/property/LocalizedTokenizedText;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v0, v5, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, v5, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/a;->size()I

    move-result v6

    move v4, v2

    :goto_0
    if-ge v4, v6, :cond_0

    iget-object v0, v5, Lcom/twitter/library/card/property/LocalizedTokenizedText;->tokenizedTexts:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, v4}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/TokenizedText;

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    if-nez v1, :cond_3

    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_3
    iget-object v7, v0, Lcom/twitter/library/card/property/TokenizedText;->textTokenGroups:[Lcom/twitter/library/card/property/TextTokenGroup;

    array-length v8, v7

    move v3, v2

    :goto_1
    if-ge v3, v8, :cond_2

    aget-object v1, v7, v3

    iget v9, v1, Lcom/twitter/library/card/property/TextTokenGroup;->id:I

    if-eqz v9, :cond_5

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenGroupMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v9}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v10, p0, Lcom/twitter/library/card/Card;->mTextTokenGroupMap:Landroid/util/SparseArray;

    invoke-virtual {v10, v9, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_4
    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_5
    iget-object v9, v1, Lcom/twitter/library/card/property/TextTokenGroup;->textTokens:[Lcom/twitter/library/card/property/TextToken;

    array-length v10, v9

    move v1, v2

    :goto_2
    if-ge v1, v10, :cond_8

    aget-object v11, v9, v1

    iget v12, v11, Lcom/twitter/library/card/property/TextToken;->id:I

    if-eqz v12, :cond_7

    iget-object v0, p0, Lcom/twitter/library/card/Card;->mTextTokenMap:Landroid/util/SparseArray;

    invoke-virtual {v0, v12}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-nez v0, :cond_6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v13, p0, Lcom/twitter/library/card/Card;->mTextTokenMap:Landroid/util/SparseArray;

    invoke-virtual {v13, v12, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_6
    invoke-virtual {v0, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_8
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1
.end method

.method protected n()V
    .locals 9

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    if-nez v0, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0}, Lcom/twitter/library/card/property/a;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, v2}, Lcom/twitter/library/card/property/a;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    iget-object v0, v0, Lcom/twitter/library/card/property/Action;->apiRequest:Lcom/twitter/library/card/property/ApiRequest;

    if-eqz v0, :cond_2

    iget-object v4, v0, Lcom/twitter/library/card/property/ApiRequest;->parameters:[Lcom/twitter/library/card/property/ApiRequestParameter;

    if-eqz v4, :cond_2

    array-length v5, v4

    move v0, v1

    :goto_1
    if-ge v0, v5, :cond_2

    aget-object v6, v4, v0

    iget-object v7, p0, Lcom/twitter/library/card/Card;->mApiRequestParameterMap:Landroid/util/SparseArray;

    iget v8, v6, Lcom/twitter/library/card/property/ApiRequestParameter;->id:I

    invoke-virtual {v7, v8, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0
.end method

.method public o()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/card/Card;->mToRefresh:Z

    return-void
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Container;->readExternal(Ljava/io/ObjectInput;)V

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    const-class v0, [Lcom/twitter/library/card/property/Binding;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/card/property/Binding;

    iput-object v0, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    const-class v0, Lcom/twitter/library/card/property/Action;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    const-class v0, Lcom/twitter/library/card/property/Style;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    const-class v0, Ljava/lang/String;

    const-class v1, Lcom/twitter/library/card/property/LocalizedTokenizedText;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Class;Ljava/lang/Class;Ljava/io/ObjectInput;)Ljava/util/HashMap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/card/Card;->loadActionId:I

    const-class v0, Lcom/twitter/library/card/property/Form;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Ljava/lang/Class;Ljava/io/ObjectInput;)Lcom/twitter/library/card/property/a;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/library/card/element/Container;->writeExternal(Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->uuid:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->platformKey:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->bindings:[Lcom/twitter/library/card/property/Binding;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Lcom/twitter/library/card/property/a;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->styles:Lcom/twitter/library/card/property/a;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Lcom/twitter/library/card/property/a;Ljava/io/ObjectOutput;)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->localizedTokenizedTexts:Ljava/util/HashMap;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/HashMap;Ljava/io/ObjectOutput;)V

    iget v0, p0, Lcom/twitter/library/card/Card;->loadActionId:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/card/Card;->forms:Lcom/twitter/library/card/property/a;

    invoke-static {v0, p1}, Lcom/twitter/library/card/property/a;->a(Lcom/twitter/library/card/property/a;Ljava/io/ObjectOutput;)V

    return-void
.end method
