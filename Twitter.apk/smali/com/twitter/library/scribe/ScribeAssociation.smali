.class public Lcom/twitter/library/scribe/ScribeAssociation;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Ljava/lang/String;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/scribe/a;

    invoke-direct {v0}, Lcom/twitter/library/scribe/a;-><init>()V

    sput-object v0, Lcom/twitter/library/scribe/ScribeAssociation;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->a:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->f:Ljava/lang/String;

    return-void
.end method

.method public static a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    invoke-virtual {v0, p0}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p3, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p3, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p3, Lcom/twitter/library/scribe/ScribeAssociation;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->a:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->b:Ljava/lang/String;

    return-object p0
.end method

.method public a(Lcom/fasterxml/jackson/core/JsonGenerator;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->a:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->b:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "association_id"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->c:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    const-string/jumbo v0, "association_type"

    iget v1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    if-eqz v0, :cond_4

    const-string/jumbo v0, "association_namespace"

    invoke-virtual {p1, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->e(Ljava/lang/String;)V

    const-string/jumbo v0, "page"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "section"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->f:Ljava/lang/String;

    if-eqz v0, :cond_3

    const-string/jumbo v0, "component"

    iget-object v1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    :cond_4
    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V

    return-void
.end method

.method public a()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    const-string/jumbo v1, "discover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 0

    iput p1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->c:I

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    return-object p0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    return-object v0
.end method

.method public c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    return-object p0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->a:I

    return v0
.end method

.method public d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/scribe/ScribeAssociation;->f:Ljava/lang/String;

    return-object p0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/scribe/ScribeAssociation;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
