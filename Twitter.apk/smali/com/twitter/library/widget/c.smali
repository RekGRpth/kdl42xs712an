.class Lcom/twitter/library/widget/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/ComposerBar;

.field private b:Landroid/support/v4/widget/ScrollerCompat;

.field private c:I

.field private d:I

.field private e:Z


# direct methods
.method private constructor <init>(Lcom/twitter/library/widget/ComposerBar;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Landroid/support/v4/widget/ScrollerCompat;->create(Landroid/content/Context;)Landroid/support/v4/widget/ScrollerCompat;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/c;->b:Landroid/support/v4/widget/ScrollerCompat;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/library/widget/ComposerBar;Landroid/content/Context;Lcom/twitter/library/widget/b;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/c;-><init>(Lcom/twitter/library/widget/ComposerBar;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 7

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar;->b:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar;->b:I

    if-eq v0, v6, :cond_0

    if-lez p1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar;->b:I

    const/4 v2, 0x3

    if-ne v0, v2, :cond_1

    iput-boolean v6, p0, Lcom/twitter/library/widget/c;->e:Z

    iput p1, p0, Lcom/twitter/library/widget/c;->c:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBar;->getMeasuredHeight()I

    move-result v2

    iget-object v0, p0, Lcom/twitter/library/widget/c;->b:Landroid/support/v4/widget/ScrollerCompat;

    neg-int v4, v2

    move v2, v1

    move v3, v1

    move v5, p1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/ScrollerCompat;->startScroll(IIIII)V

    iput v1, p0, Lcom/twitter/library/widget/c;->d:I

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iput v6, v0, Lcom/twitter/library/widget/ComposerBar;->b:I

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ComposerBar;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/ComposerBar;->setVisible(Z)V

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBar;->invalidate()V

    goto :goto_0
.end method

.method public run()V
    .locals 8

    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/c;->b:Landroid/support/v4/widget/ScrollerCompat;

    iget-boolean v2, p0, Lcom/twitter/library/widget/c;->e:Z

    if-eqz v2, :cond_2

    iput-boolean v1, p0, Lcom/twitter/library/widget/c;->e:Z

    invoke-virtual {v0}, Landroid/support/v4/widget/ScrollerCompat;->getCurrY()I

    move-result v2

    iput v2, p0, Lcom/twitter/library/widget/c;->d:I

    iget-object v3, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v3, v3, Lcom/twitter/library/widget/ComposerBar;->b:I

    if-ne v3, v7, :cond_1

    iget-object v3, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iput v6, v3, Lcom/twitter/library/widget/ComposerBar;->b:I

    neg-int v4, v2

    iget v5, p0, Lcom/twitter/library/widget/c;->c:I

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/ScrollerCompat;->startScroll(IIIII)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ComposerBar;->post(Ljava/lang/Runnable;)Z

    :goto_1
    return-void

    :cond_1
    iget-object v3, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v3, v3, Lcom/twitter/library/widget/ComposerBar;->b:I

    if-ne v3, v6, :cond_0

    iget-object v3, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iput v7, v3, Lcom/twitter/library/widget/ComposerBar;->b:I

    iget v3, p0, Lcom/twitter/library/widget/c;->d:I

    iget-object v4, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v4}, Lcom/twitter/library/widget/ComposerBar;->getMeasuredHeight()I

    move-result v4

    sub-int/2addr v4, v2

    iget v5, p0, Lcom/twitter/library/widget/c;->c:I

    move v2, v3

    move v3, v1

    invoke-virtual/range {v0 .. v5}, Landroid/support/v4/widget/ScrollerCompat;->startScroll(IIIII)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Landroid/support/v4/widget/ScrollerCompat;->computeScrollOffset()Z

    move-result v2

    invoke-virtual {v0}, Landroid/support/v4/widget/ScrollerCompat;->getCurrY()I

    move-result v3

    iget v0, p0, Lcom/twitter/library/widget/c;->d:I

    sub-int v4, v3, v0

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBar;->getChildCount()I

    move-result v5

    if-eqz v2, :cond_6

    :goto_2
    if-ge v1, v5, :cond_4

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ComposerBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    iget-boolean v0, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    if-nez v0, :cond_3

    invoke-virtual {v2, v4}, Landroid/view/View;->offsetTopAndBottom(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v2}, Landroid/view/View;->getLeft()I

    move-result v6

    invoke-virtual {v2}, Landroid/view/View;->getTop()I

    move-result v7

    add-int/2addr v7, v4

    invoke-static {v0, v2, v6, v7}, Lcom/twitter/library/widget/ComposerBar;->a(Lcom/twitter/library/widget/ComposerBar;Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBar;->invalidate()V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    iput v3, p0, Lcom/twitter/library/widget/c;->d:I

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ComposerBar;->post(Ljava/lang/Runnable;)Z

    :cond_5
    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBar;->invalidate()V

    goto :goto_1

    :cond_6
    iput v1, p0, Lcom/twitter/library/widget/c;->d:I

    move v2, v1

    :goto_3
    if-ge v2, v5, :cond_5

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ComposerBar;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    if-eqz v3, :cond_7

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    iget-boolean v0, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->e:Z

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar;->b:I

    if-ne v0, v6, :cond_8

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, v3, v6}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;Z)V

    :cond_7
    :goto_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar;->b:I

    if-ne v0, v7, :cond_7

    iget-object v0, p0, Lcom/twitter/library/widget/c;->a:Lcom/twitter/library/widget/ComposerBar;

    invoke-virtual {v0, v3, v1}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;Z)V

    goto :goto_4
.end method
