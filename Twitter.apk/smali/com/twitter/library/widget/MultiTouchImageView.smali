.class public Lcom/twitter/library/widget/MultiTouchImageView;
.super Landroid/widget/ImageView;
.source "Twttr"


# instance fields
.field protected final a:Landroid/graphics/Matrix;

.field protected final b:Landroid/graphics/RectF;

.field protected final c:Landroid/graphics/RectF;

.field protected d:I

.field private final e:Landroid/graphics/Rect;

.field private final f:Landroid/graphics/PointF;

.field private g:Z

.field private h:Z

.field private i:I

.field private j:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->e:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->j:F

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/MultiTouchImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/MultiTouchImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->e:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->j:F

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/MultiTouchImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    return-void
.end method

.method protected static a(FFFFF)F
    .locals 1

    add-float v0, p1, p0

    cmpg-float v0, v0, p3

    if-gez v0, :cond_1

    sub-float p0, p3, p1

    :cond_0
    :goto_0
    return p0

    :cond_1
    add-float v0, p2, p0

    cmpl-float v0, v0, p4

    if-lez v0, :cond_0

    sub-float p0, p4, p2

    goto :goto_0
.end method

.method protected static a(Landroid/graphics/RectF;Landroid/graphics/RectF;Z)F
    .locals 3

    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->min(FF)F

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {p0}, Landroid/graphics/RectF;->height()F

    move-result v1

    div-float/2addr v0, v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p0}, Landroid/graphics/RectF;->width()F

    move-result v2

    div-float/2addr v1, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->max(FF)F

    move-result v0

    goto :goto_0
.end method

.method private static a(Landroid/view/MotionEvent;)F
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v0

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getX(I)F

    move-result v1

    sub-float/2addr v0, v1

    invoke-virtual {p0, v2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v1

    invoke-virtual {p0, v3}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    sub-float/2addr v1, v2

    mul-float/2addr v0, v0

    mul-float/2addr v1, v1

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    double-to-float v0, v0

    return v0
.end method

.method private a(Landroid/graphics/RectF;I)V
    .locals 7

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    iput p2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->d:I

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/high16 v5, 0x3f800000    # 1.0f

    move-object v0, p0

    move v4, v3

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFFI)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1, p1}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;Landroid/graphics/RectF;)Z

    return-void
.end method


# virtual methods
.method protected a()V
    .locals 7

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    new-instance v5, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    invoke-direct {v5, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerX()F

    move-result v3

    invoke-virtual {v5}, Landroid/graphics/RectF;->centerY()F

    move-result v4

    sub-float/2addr v3, v1

    sub-float/2addr v4, v2

    const/4 v6, 0x1

    invoke-static {v0, v5, v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Z)F

    move-result v5

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFFI)V

    invoke-virtual {p0, v3, v4, v5}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFF)V

    return-void
.end method

.method protected a(FFF)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v2, 0x3f800000    # 1.0f

    const/4 v3, 0x0

    cmpl-float v0, p3, v2

    if-nez v0, :cond_0

    cmpl-float v0, p1, v3

    if-nez v0, :cond_0

    cmpl-float v0, p2, v3

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    cmpl-float v0, p3, v2

    if-eqz v0, :cond_1

    sub-float v0, v2, p3

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/RectF;->width()F

    move-result v1

    mul-float/2addr v0, v1

    sub-float v1, v2, p3

    iget-object v2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    mul-float/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    div-float/2addr v0, v4

    div-float/2addr v1, v4

    invoke-virtual {v2, v0, v1}, Landroid/graphics/RectF;->inset(FF)V

    :cond_1
    cmpl-float v0, p1, v3

    if-nez v0, :cond_2

    cmpl-float v0, p2, v3

    if-eqz v0, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, p1, p2}, Landroid/graphics/RectF;->offset(FF)V

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->invalidate()V

    goto :goto_0
.end method

.method public a(FFFFFI)V
    .locals 4

    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    rem-int/lit16 v0, p6, 0x168

    if-nez v0, :cond_0

    cmpl-float v1, p5, v3

    if-nez v1, :cond_0

    cmpl-float v1, p3, v2

    if-nez v1, :cond_0

    cmpl-float v1, p4, v2

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    int-to-float v0, v0

    invoke-virtual {v1, v0, p1, p2}, Landroid/graphics/Matrix;->postRotate(FFF)Z

    :cond_1
    cmpl-float v0, p5, v3

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, p5, p5, p1, p2}, Landroid/graphics/Matrix;->postScale(FFFF)Z

    :cond_2
    cmpl-float v0, p3, v2

    if-nez v0, :cond_3

    cmpl-float v0, p4, v2

    if-eqz v0, :cond_4

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v0, p3, p4}, Landroid/graphics/Matrix;->postTranslate(FF)Z

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/MultiTouchImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    goto :goto_0
.end method

.method protected b()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->g:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->a()V

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->d()V

    :cond_1
    return-void
.end method

.method protected c()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 11

    const/high16 v10, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->h:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->contains(Landroid/graphics/RectF;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    invoke-static {v7, v8, v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(Landroid/graphics/RectF;Landroid/graphics/RectF;Z)F

    move-result v1

    invoke-static {v10, v1}, Ljava/lang/Math;->max(FF)F

    move-result v5

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    move-object v0, p0

    move v4, v3

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFFI)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/graphics/RectF;->set(Landroid/graphics/RectF;)V

    iget v0, v7, Landroid/graphics/RectF;->right:F

    iget v4, v7, Landroid/graphics/RectF;->left:F

    iget v5, v8, Landroid/graphics/RectF;->right:F

    iget v9, v8, Landroid/graphics/RectF;->left:F

    invoke-static {v3, v0, v4, v5, v9}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFF)F

    move-result v5

    iget v0, v7, Landroid/graphics/RectF;->bottom:F

    iget v4, v7, Landroid/graphics/RectF;->top:F

    iget v7, v8, Landroid/graphics/RectF;->bottom:F

    iget v8, v8, Landroid/graphics/RectF;->top:F

    invoke-static {v3, v0, v4, v7, v8}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFF)F

    move-result v4

    move-object v0, p0

    move v3, v5

    move v5, v10

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFFI)V

    goto :goto_0
.end method

.method public getActiveRect()Landroid/graphics/RectF;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    return-object v0
.end method

.method protected getImageRect()Landroid/graphics/RectF;
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v1

    int-to-float v1, v1

    invoke-direct {v0, v3, v3, v2, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getImageRotation()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->d:I

    return v0
.end method

.method public getImageSelection()Landroid/graphics/RectF;
    .locals 3

    new-instance v0, Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-direct {v0, v1}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    new-instance v1, Landroid/graphics/Matrix;

    invoke-direct {v1}, Landroid/graphics/Matrix;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->a:Landroid/graphics/Matrix;

    invoke-virtual {v2, v1}, Landroid/graphics/Matrix;->invert(Landroid/graphics/Matrix;)Z

    invoke-virtual {v1, v0}, Landroid/graphics/Matrix;->mapRect(Landroid/graphics/RectF;)Z

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 2

    invoke-super/range {p0 .. p5}, Landroid/widget/ImageView;->onLayout(ZIIII)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/MultiTouchImageView;->getDrawingRect(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->b:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->e:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/RectF;->set(Landroid/graphics/Rect;)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->b()V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->g:Z

    check-cast p1, Landroid/os/Bundle;

    const-string/jumbo v0, "parent_bundle"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/ImageView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    const-string/jumbo v0, "image_selection"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    const-string/jumbo v1, "rotation"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/MultiTouchImageView;->a(Landroid/graphics/RectF;I)V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "parent_bundle"

    invoke-super {p0}, Landroid/widget/ImageView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "image_selection"

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getImageSelection()Landroid/graphics/RectF;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "rotation"

    iget v2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->c()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    invoke-static {p1}, Lcom/twitter/library/widget/MultiTouchImageView;->a(Landroid/view/MotionEvent;)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->j:F

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    goto :goto_1

    :pswitch_3
    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_4

    new-instance v7, Landroid/graphics/PointF;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-direct {v7, v0, v1}, Landroid/graphics/PointF;-><init>(FF)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getImageRect()Landroid/graphics/RectF;

    move-result-object v0

    iget v1, v7, Landroid/graphics/PointF;->x:F

    iget-object v2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->x:F

    sub-float/2addr v1, v2

    iget v2, v0, Landroid/graphics/RectF;->right:F

    iget v3, v0, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->right:F

    iget-object v5, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->left:F

    invoke-static {v1, v2, v3, v4, v5}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFF)F

    move-result v3

    iget v1, v7, Landroid/graphics/PointF;->y:F

    iget-object v2, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    iget v2, v2, Landroid/graphics/PointF;->y:F

    sub-float/2addr v1, v2

    iget v2, v0, Landroid/graphics/RectF;->bottom:F

    iget v0, v0, Landroid/graphics/RectF;->top:F

    iget-object v4, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->bottom:F

    iget-object v5, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->top:F

    invoke-static {v1, v2, v0, v4, v5}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFF)F

    move-result v4

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFFI)V

    const/4 v0, 0x0

    cmpl-float v0, v3, v0

    if-nez v0, :cond_2

    iget v0, v7, Landroid/graphics/PointF;->x:F

    iget-object v1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    iget v1, v1, Landroid/graphics/PointF;->x:F

    sub-float/2addr v0, v1

    cmpl-float v0, v3, v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-nez v0, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-interface {v1, v0}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->f:Landroid/graphics/PointF;

    invoke-virtual {v0, v7}, Landroid/graphics/PointF;->set(Landroid/graphics/PointF;)V

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_2

    :cond_3
    const/4 v0, 0x0

    goto :goto_3

    :cond_4
    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    invoke-static {p1}, Lcom/twitter/library/widget/MultiTouchImageView;->a(Landroid/view/MotionEvent;)F

    move-result v7

    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->j:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->j:F

    div-float v5, v7, v0

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v1

    iget-object v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->c:Landroid/graphics/RectF;

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v0, p0

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/widget/MultiTouchImageView;->a(FFFFFI)V

    :cond_5
    iput v7, p0, Lcom/twitter/library/widget/MultiTouchImageView;->j:F

    goto/16 :goto_1

    :cond_6
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto/16 :goto_0

    :pswitch_4
    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/widget/MultiTouchImageView;->i:I

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->d()V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/MultiTouchImageView;->b()V

    return-void
.end method

.method public setPositionLocked(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/MultiTouchImageView;->h:Z

    return-void
.end method
