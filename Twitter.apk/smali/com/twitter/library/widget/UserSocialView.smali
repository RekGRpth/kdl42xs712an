.class public Lcom/twitter/library/widget/UserSocialView;
.super Lcom/twitter/library/widget/UserView;
.source "Twttr"


# instance fields
.field private p:Lcom/twitter/library/widget/SocialBylineView;

.field private q:F

.field private r:F


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lib;->userViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/UserSocialView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/UserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    sget-object v0, Lim;->UserSocialView:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {p1}, Lcom/twitter/library/util/Util;->e(Landroid/content/Context;)F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/UserSocialView;->q:F

    invoke-static {p1}, Lcom/twitter/library/util/Util;->f(Landroid/content/Context;)F

    move-result v1

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/UserSocialView;->r:F

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a(FF)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->g:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p1}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->i:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v1, p2}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->p:Lcom/twitter/library/widget/SocialBylineView;

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/SocialBylineView;->setLabelSize(F)V

    return-void
.end method


# virtual methods
.method public a(IILjava/lang/String;IZ)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->p:Lcom/twitter/library/widget/SocialBylineView;

    if-lez p2, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    invoke-virtual {v0, p5}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    :goto_1
    return-void

    :pswitch_0
    if-lez p4, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lil;->social_follow_and_more_follow:I

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lil;->social_follow_and_follow:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/library/widget/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v1

    sget v2, Lil;->social_both_follow:I

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(IIZ)V
    .locals 4

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/library/widget/UserSocialView;->p:Lcom/twitter/library/widget/SocialBylineView;

    iget-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p2}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lil;->social_following:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    if-lez p1, :cond_3

    if-eqz v0, :cond_3

    invoke-virtual {v2, p1}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    invoke-virtual {v2, v1}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    invoke-virtual {v2, p3}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    :goto_2
    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    invoke-static {p2}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    sget v3, Lil;->social_follows_you:I

    invoke-virtual {v0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/16 v0, 0x8

    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_2
.end method

.method protected onFinishInflate()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/widget/UserView;->onFinishInflate()V

    sget v0, Lig;->social_byline:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserSocialView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SocialBylineView;

    iput-object v0, p0, Lcom/twitter/library/widget/UserSocialView;->p:Lcom/twitter/library/widget/SocialBylineView;

    iget v0, p0, Lcom/twitter/library/widget/UserSocialView;->q:F

    iget v1, p0, Lcom/twitter/library/widget/UserSocialView;->r:F

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/UserSocialView;->a(FF)V

    return-void
.end method

.method public setContentSize(F)V
    .locals 1

    iput p1, p0, Lcom/twitter/library/widget/UserSocialView;->q:F

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserSocialView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;F)F

    move-result v0

    iput v0, p0, Lcom/twitter/library/widget/UserSocialView;->r:F

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/UserSocialView;->a(FF)V

    return-void
.end method
