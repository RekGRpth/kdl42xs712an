.class public Lcom/twitter/library/widget/TweetView$InlineAction;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final i:[I

.field private static final j:[I

.field private static final k:[I

.field private static final l:[I

.field private static final m:[I


# instance fields
.field public a:Landroid/graphics/drawable/Drawable;

.field public final b:Landroid/graphics/Rect;

.field public final c:Landroid/graphics/Rect;

.field public d:Landroid/animation/ValueAnimator;

.field public e:Lcom/twitter/library/widget/TweetView$InlineAction$State;

.field public f:Lcom/twitter/library/view/TweetActionType;

.field public g:Landroid/text/StaticLayout;

.field public final h:Landroid/graphics/Rect;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [I

    sget v1, Lib;->state_inline_action_fav_pressed:I

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->i:[I

    new-array v0, v3, [I

    sget v1, Lib;->state_inline_action_retweet_pressed:I

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->j:[I

    new-array v0, v3, [I

    sget v1, Lib;->state_inline_action_follow_pressed:I

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->k:[I

    new-array v0, v3, [I

    sget v1, Lib;->state_inline_action_reply_pressed:I

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->l:[I

    new-array v0, v3, [I

    sget v1, Lib;->state_inline_action_pressed:I

    aput v1, v0, v2

    sput-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->m:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->b:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->h:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic a()[I
    .locals 1

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->i:[I

    return-object v0
.end method

.method private b(Landroid/graphics/Canvas;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v0

    div-int/lit8 v2, v0, 0x2

    int-to-float v0, v1

    int-to-float v3, v2

    invoke-virtual {p1, v0, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->d:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-virtual {p1, v0, v0}, Landroid/graphics/Canvas;->scale(FF)V

    neg-int v0, v1

    int-to-float v0, v0

    neg-int v1, v2

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    return-void
.end method

.method static synthetic b()[I
    .locals 1

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->j:[I

    return-object v0
.end method

.method static synthetic c()[I
    .locals 1

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->k:[I

    return-object v0
.end method

.method static synthetic d()[I
    .locals 1

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->l:[I

    return-object v0
.end method

.method static synthetic e()[I
    .locals 1

    sget-object v0, Lcom/twitter/library/widget/TweetView$InlineAction;->m:[I

    return-object v0
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;)V
    .locals 2

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->d:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/TweetView$InlineAction;->b(Landroid/graphics/Canvas;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->h:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->h:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetView$InlineAction;->g:Landroid/text/StaticLayout;

    invoke-virtual {v0, p1}, Landroid/text/StaticLayout;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    return-void
.end method
