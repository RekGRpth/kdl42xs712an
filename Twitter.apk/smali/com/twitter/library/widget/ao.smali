.class Lcom/twitter/library/widget/ao;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/widget/TweetView;


# direct methods
.method public constructor <init>(Lcom/twitter/library/widget/TweetView;)V
    .locals 1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object p1, p0, Lcom/twitter/library/widget/ao;->a:Lcom/twitter/library/widget/TweetView;

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Unknown message!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/widget/ao;->a:Lcom/twitter/library/widget/TweetView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;Z)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/library/widget/ao;->a:Lcom/twitter/library/widget/TweetView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/widget/TweetView;Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
