.class public Lcom/twitter/library/widget/ComposerBar;
.super Landroid/view/ViewGroup;
.source "Twttr"


# instance fields
.field a:Lcom/twitter/library/widget/c;

.field b:I

.field private c:I

.field private d:I

.field private e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/ComposerBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/ComposerBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput v3, p0, Lcom/twitter/library/widget/ComposerBar;->b:I

    new-instance v0, Lcom/twitter/library/widget/b;

    invoke-direct {v0, p0}, Lcom/twitter/library/widget/b;-><init>(Lcom/twitter/library/widget/ComposerBar;)V

    iput-object v0, p0, Lcom/twitter/library/widget/ComposerBar;->e:Ljava/lang/Runnable;

    new-instance v0, Lcom/twitter/library/widget/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/widget/c;-><init>(Lcom/twitter/library/widget/ComposerBar;Landroid/content/Context;Lcom/twitter/library/widget/b;)V

    iput-object v0, p0, Lcom/twitter/library/widget/ComposerBar;->a:Lcom/twitter/library/widget/c;

    sget-object v0, Lim;->ComposerBar:[I

    invoke-virtual {p1, p2, v0, p3, v3}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x1

    const/16 v2, 0xfa

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/ComposerBar;->c:I

    const/16 v1, 0x3e8

    invoke-virtual {v0, v3, v1}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/ComposerBar;->d:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a(Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    add-int/2addr v1, p2

    invoke-virtual {p1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int/2addr v2, p3

    invoke-virtual {v0, p2, p3, v1, v2}, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->a(IIII)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/widget/ComposerBar;Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a()Lcom/twitter/library/widget/ComposerBar$LayoutParams;
    .locals 2

    const/4 v1, -0x1

    new-instance v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    invoke-direct {v0, v1, v1}, Lcom/twitter/library/widget/ComposerBar$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public a(Landroid/util/AttributeSet;)Lcom/twitter/library/widget/ComposerBar$LayoutParams;
    .locals 2

    new-instance v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/twitter/library/widget/ComposerBar$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method a(Landroid/view/View;Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    iput v1, p0, Lcom/twitter/library/widget/ComposerBar;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/library/widget/ComposerBar;->b:I

    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getHeight()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    goto :goto_0
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1

    instance-of v0, p1, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    return v0
.end method

.method protected synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->a()Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/util/AttributeSet;)Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    invoke-direct {v0, p1}, Lcom/twitter/library/widget/ComposerBar$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/ComposerBar;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    iget v4, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->a:I

    iget v5, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->b:I

    iget v6, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->c:I

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->d:I

    invoke-virtual {v3, v4, v5, v6, v0}, Landroid/view/View;->layout(IIII)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 10

    const/16 v9, 0x8

    const/4 v3, 0x0

    invoke-super {p0, p1, p2}, Landroid/view/ViewGroup;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getChildCount()I

    move-result v8

    move v7, v3

    move v6, v3

    :goto_0
    if-ge v7, v8, :cond_0

    invoke-virtual {p0, v7}, Lcom/twitter/library/widget/ComposerBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v9, :cond_5

    move-object v0, p0

    move v2, p1

    move v4, p2

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/ComposerBar;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    :goto_1
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v0

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getSuggestedMinimumHeight()I

    move-result v0

    invoke-static {v6, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/library/widget/ComposerBar;->setMeasuredDimension(II)V

    :goto_2
    if-ge v3, v8, :cond_4

    invoke-virtual {p0, v3}, Lcom/twitter/library/widget/ComposerBar;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eq v0, v9, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getMeasuredHeight()I

    move-result v4

    sub-int v2, v4, v2

    div-int/lit8 v2, v2, 0x2

    iget v4, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    if-nez v4, :cond_2

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v4

    sub-int/2addr v0, v4

    div-int/lit8 v0, v0, 0x2

    :goto_3
    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;II)Lcom/twitter/library/widget/ComposerBar$LayoutParams;

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_2
    iget v4, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->f:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_3

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getMeasuredWidth()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    sub-int/2addr v4, v5

    iget v0, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->rightMargin:I

    sub-int v0, v4, v0

    goto :goto_3

    :cond_3
    iget v0, v0, Lcom/twitter/library/widget/ComposerBar$LayoutParams;->leftMargin:I

    goto :goto_3

    :cond_4
    return-void

    :cond_5
    move v0, v6

    goto :goto_1
.end method

.method public final setPadding(IIII)V
    .locals 0

    return-void
.end method

.method setVisible(Z)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/widget/ComposerBar;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/ComposerBar;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0, v2, p1}, Lcom/twitter/library/widget/ComposerBar;->a(Landroid/view/View;Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
