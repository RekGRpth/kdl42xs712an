.class Lcom/twitter/library/widget/y;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/TweetView;

.field private b:Lcom/twitter/library/widget/TweetView$InlineAction;


# direct methods
.method public constructor <init>(Lcom/twitter/library/widget/TweetView;Lcom/twitter/library/widget/TweetView$InlineAction;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/y;->a:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/library/widget/y;->b:Lcom/twitter/library/widget/TweetView$InlineAction;

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/library/widget/y;->b:Lcom/twitter/library/widget/TweetView$InlineAction;

    iget-object v0, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->c:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/widget/y;->a:Lcom/twitter/library/widget/TweetView;

    iget v4, v0, Landroid/graphics/Rect;->left:I

    sub-int/2addr v4, v1

    iget v5, v0, Landroid/graphics/Rect;->top:I

    sub-int/2addr v5, v2

    iget v6, v0, Landroid/graphics/Rect;->right:I

    add-int/2addr v1, v6

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v0, v2

    invoke-virtual {v3, v4, v5, v1, v0}, Lcom/twitter/library/widget/TweetView;->invalidate(IIII)V

    iget-object v0, p0, Lcom/twitter/library/widget/y;->b:Lcom/twitter/library/widget/TweetView$InlineAction;

    iput-object p1, v0, Lcom/twitter/library/widget/TweetView$InlineAction;->d:Landroid/animation/ValueAnimator;

    return-void
.end method
