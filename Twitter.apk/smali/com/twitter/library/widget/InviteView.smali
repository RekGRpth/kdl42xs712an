.class public Lcom/twitter/library/widget/InviteView;
.super Landroid/widget/RelativeLayout;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/CheckBox;

.field private e:Z

.field private f:Lcom/twitter/library/widget/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-virtual {p0, p0}, Lcom/twitter/library/widget/InviteView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/InviteView;->e:Z

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/InviteView;->f:Lcom/twitter/library/widget/i;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/InviteView;->e:Z

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InviteView;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/library/widget/InviteView;->f:Lcom/twitter/library/widget/i;

    iget v1, p0, Lcom/twitter/library/widget/InviteView;->a:I

    invoke-interface {v0, p0, v1}, Lcom/twitter/library/widget/i;->onClick(Ljava/lang/Object;I)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    sget v0, Lig;->name_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InviteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/InviteView;->b:Landroid/widget/TextView;

    sget v0, Lig;->email_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InviteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/library/widget/InviteView;->c:Landroid/widget/TextView;

    sget v0, Lig;->check_item:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/InviteView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/library/widget/InviteView;->d:Landroid/widget/CheckBox;

    return-void
.end method

.method public setChecked(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/InviteView;->e:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/library/widget/InviteView;->e:Z

    iget-object v0, p0, Lcom/twitter/library/widget/InviteView;->d:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public setEmail(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/InviteView;->c:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setId(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/InviteView;->a:I

    return-void
.end method

.method public setName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/InviteView;->b:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public setOnViewClickListener(Lcom/twitter/library/widget/i;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/InviteView;->f:Lcom/twitter/library/widget/i;

    return-void
.end method
