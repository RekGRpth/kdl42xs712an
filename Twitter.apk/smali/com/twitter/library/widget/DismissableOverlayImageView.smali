.class public Lcom/twitter/library/widget/DismissableOverlayImageView;
.super Lcom/twitter/internal/android/widget/OverlayImageView;
.source "Twttr"


# instance fields
.field private a:Landroid/graphics/drawable/Drawable;

.field private b:Z

.field private c:Lcom/twitter/library/widget/h;

.field private d:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/OverlayImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/internal/android/widget/OverlayImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/DismissableOverlayImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/internal/android/widget/OverlayImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/DismissableOverlayImageView;->a(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method private a(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lim;->DismissableOverlayImageView:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/content/res/TypedArray;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/DismissableOverlayImageView;->setDismissOverlayDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_1
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    goto :goto_0
.end method

.method private setDismissDrawableBounds(Landroid/graphics/drawable/Drawable;)V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->d:Z

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->getMeasuredHeight()I

    move-result v1

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v2

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    sub-int v2, v0, v2

    invoke-static {v4, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {p1, v2, v4, v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->invalidate()V

    :cond_0
    return-void
.end method


# virtual methods
.method public invalidateDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->invalidate()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/internal/android/widget/OverlayImageView;->invalidateDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/internal/android/widget/OverlayImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/internal/android/widget/OverlayImageView;->onMeasure(II)V

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->setDismissDrawableBounds(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 11

    const v10, 0x10100a7    # android.R.attr.state_pressed

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    iget-object v3, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v3

    new-instance v4, Landroid/graphics/Rect;

    iget v5, v3, Landroid/graphics/Rect;->left:I

    add-int/lit8 v5, v5, -0x32

    iget v6, v3, Landroid/graphics/Rect;->top:I

    add-int/lit8 v6, v6, -0x32

    iget v7, v3, Landroid/graphics/Rect;->right:I

    add-int/lit8 v7, v7, 0x32

    iget v3, v3, Landroid/graphics/Rect;->bottom:I

    add-int/lit8 v3, v3, 0x32

    invoke-direct {v4, v5, v6, v7, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v4, v2, v1}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Lcom/twitter/internal/android/widget/OverlayImageView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    new-array v1, v9, [I

    aput v10, v1, v8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    iput-boolean v9, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->b:Z

    goto :goto_0

    :pswitch_1
    if-eqz v1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->b:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->c:Lcom/twitter/library/widget/h;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->c:Lcom/twitter/library/widget/h;

    invoke-interface {v0}, Lcom/twitter/library/widget/h;->a()V

    :cond_1
    iput-boolean v8, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->b:Z

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    new-array v1, v8, [I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0

    :pswitch_2
    iput-boolean v8, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->b:Z

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    new-array v1, v8, [I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0

    :pswitch_3
    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    new-array v1, v9, [I

    aput v10, v1, v8

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    iput-boolean v9, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->b:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    new-array v1, v8, [I

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setState([I)Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public setDismissOverlayDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_1

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->unscheduleDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p0}, Lcom/twitter/library/widget/DismissableOverlayImageView;->requestLayout()V

    :cond_1
    return-void
.end method

.method protected setFrame(IIII)Z
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/internal/android/widget/OverlayImageView;->setFrame(IIII)Z

    move-result v0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->d:Z

    iget-object v1, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->a:Landroid/graphics/drawable/Drawable;

    invoke-direct {p0, v1}, Lcom/twitter/library/widget/DismissableOverlayImageView;->setDismissDrawableBounds(Landroid/graphics/drawable/Drawable;)V

    return v0
.end method

.method public setOnDismissListener(Lcom/twitter/library/widget/h;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/DismissableOverlayImageView;->c:Lcom/twitter/library/widget/h;

    return-void
.end method
