.class public Lcom/twitter/library/widget/UserView;
.super Lcom/twitter/library/widget/BaseUserView;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field public m:Lcom/twitter/library/widget/ActionButton;

.field public n:Lcom/twitter/library/widget/ActionButton;

.field public o:Landroid/widget/CheckBox;

.field private p:Lcom/twitter/library/widget/a;

.field private q:Lcom/twitter/library/widget/a;

.field private r:Lcom/twitter/library/widget/a;

.field private s:Lcom/twitter/library/scribe/ScribeItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/library/widget/a;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->h:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    invoke-virtual {p0, p2}, Lcom/twitter/library/widget/UserView;->setActionButtonClickListener(Lcom/twitter/library/widget/a;)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/library/widget/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public b(ILcom/twitter/library/widget/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p2, p0, Lcom/twitter/library/widget/UserView;->r:Lcom/twitter/library/widget/a;

    return-void
.end method

.method public getScribeItem()Lcom/twitter/library/scribe/ScribeItem;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->s:Lcom/twitter/library/scribe/ScribeItem;

    return-object v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lig;->action_button:I

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->p:Lcom/twitter/library/widget/a;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->p:Lcom/twitter/library/widget/a;

    iget-wide v2, p0, Lcom/twitter/library/widget/UserView;->f:J

    invoke-interface {v1, p0, v2, v3, v0}, Lcom/twitter/library/widget/a;->onClick(Lcom/twitter/library/widget/BaseUserView;JI)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->toggle()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    sget v1, Lig;->user_checkbox:I

    if-ne v0, v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->q:Lcom/twitter/library/widget/a;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->q:Lcom/twitter/library/widget/a;

    iget-wide v2, p0, Lcom/twitter/library/widget/UserView;->f:J

    invoke-interface {v1, p0, v2, v3, v0}, Lcom/twitter/library/widget/a;->onClick(Lcom/twitter/library/widget/BaseUserView;JI)V

    goto :goto_0

    :cond_3
    sget v1, Lig;->favorite_action_button:I

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->r:Lcom/twitter/library/widget/a;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/widget/UserView;->r:Lcom/twitter/library/widget/a;

    iget-wide v2, p0, Lcom/twitter/library/widget/UserView;->f:J

    invoke-interface {v1, p0, v2, v3, v0}, Lcom/twitter/library/widget/a;->onClick(Lcom/twitter/library/widget/BaseUserView;JI)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->toggle()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/widget/BaseUserView;->onFinishInflate()V

    sget v0, Lig;->action_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    sget v0, Lig;->favorite_action_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/library/widget/UserView;->n:Lcom/twitter/library/widget/ActionButton;

    sget v0, Lig;->user_checkbox:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/UserView;->o:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method

.method public setActionButtonClickListener(Lcom/twitter/library/widget/a;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/UserView;->p:Lcom/twitter/library/widget/a;

    return-void
.end method

.method public setCheckBoxClickListener(Lcom/twitter/library/widget/a;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/UserView;->q:Lcom/twitter/library/widget/a;

    return-void
.end method

.method public setScribeItem(Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/widget/UserView;->s:Lcom/twitter/library/scribe/ScribeItem;

    return-void
.end method
