.class public Lcom/twitter/library/widget/UserApprovalView;
.super Lcom/twitter/library/widget/BaseUserView;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final m:[I


# instance fields
.field private n:Lcom/twitter/library/widget/a;

.field private o:Lcom/twitter/library/widget/aq;

.field private p:Lcom/twitter/library/widget/aq;

.field private q:Lcom/twitter/library/widget/ActionButton;

.field private r:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a0    # android.R.attr.state_checked

    aput v2, v0, v1

    sput-object v0, Lcom/twitter/library/widget/UserApprovalView;->m:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/widget/BaseUserView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/UserApprovalView;->r:Z

    return-void
.end method

.method private b(I)Lcom/twitter/library/widget/aq;
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->o:Lcom/twitter/library/widget/aq;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->p:Lcom/twitter/library/widget/aq;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(II)V
    .locals 5

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    :goto_0
    if-lez p2, :cond_0

    invoke-virtual {v0, p2}, Landroid/view/View;->setBackgroundResource(I)V

    iget v1, p0, Lcom/twitter/library/widget/UserApprovalView;->a_:I

    iget v2, p0, Lcom/twitter/library/widget/UserApprovalView;->b:I

    iget v3, p0, Lcom/twitter/library/widget/UserApprovalView;->c:I

    iget v4, p0, Lcom/twitter/library/widget/UserApprovalView;->d:I

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/UserApprovalView;->b(I)Lcom/twitter/library/widget/aq;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    goto :goto_0
.end method

.method public a(IILcom/twitter/library/widget/a;)V
    .locals 3

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/ActionButton;->a(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/UserApprovalView;->b(I)Lcom/twitter/library/widget/aq;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->b:Landroid/widget/FrameLayout;

    if-nez p2, :cond_1

    const/16 v0, 0x8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v1, p2}, Landroid/widget/ImageButton;->setImageResource(I)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object p3, p0, Lcom/twitter/library/widget/UserApprovalView;->n:Lcom/twitter/library/widget/a;

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/widget/UserApprovalView;->b(I)Lcom/twitter/library/widget/aq;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p2, v0, Lcom/twitter/library/widget/aq;->c:Z

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserApprovalView;->refreshDrawableState()V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/library/widget/BaseUserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ActionButton;->setUsername(Ljava/lang/String;)V

    return-void
.end method

.method public a(I)Z
    .locals 1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/library/widget/UserApprovalView;->b(I)Lcom/twitter/library/widget/aq;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/library/widget/aq;->c:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/UserApprovalView;->r:Z

    return-void
.end method

.method public c()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->o:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->p:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    return-void
.end method

.method public d()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->o:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->p:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    return-void
.end method

.method protected drawableStateChanged()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Lcom/twitter/library/widget/BaseUserView;->drawableStateChanged()V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->o:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserApprovalView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageButton;->setImageState([IZ)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->p:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserApprovalView;->getDrawableState()[I

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageButton;->setImageState([IZ)V

    return-void
.end method

.method public e()V
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->o:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->p:Lcom/twitter/library/widget/aq;

    iget-object v0, v0, Lcom/twitter/library/widget/aq;->a:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->n:Lcom/twitter/library/widget/a;

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lig;->action_button_deny:I

    if-eq v0, v1, :cond_0

    sget v1, Lig;->action_button_deny_frame:I

    if-ne v0, v1, :cond_3

    :cond_0
    const/4 v1, 0x2

    invoke-virtual {p0, v1}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/twitter/library/widget/UserApprovalView;->n:Lcom/twitter/library/widget/a;

    iget-wide v2, p0, Lcom/twitter/library/widget/UserApprovalView;->f:J

    invoke-interface {v1, p0, v2, v3, v0}, Lcom/twitter/library/widget/a;->onClick(Lcom/twitter/library/widget/BaseUserView;JI)V

    :cond_2
    return-void

    :cond_3
    sget v1, Lig;->action_button_accept:I

    if-eq v0, v1, :cond_4

    sget v1, Lig;->action_button_accept_frame:I

    if-ne v0, v1, :cond_6

    :cond_4
    invoke-virtual {p0, v2}, Lcom/twitter/library/widget/UserApprovalView;->setState(I)V

    iget-boolean v1, p0, Lcom/twitter/library/widget/UserApprovalView;->r:Z

    if-eqz v1, :cond_5

    invoke-virtual {p0}, Lcom/twitter/library/widget/UserApprovalView;->d()V

    goto :goto_0

    :cond_5
    invoke-virtual {p0}, Lcom/twitter/library/widget/UserApprovalView;->e()V

    goto :goto_0

    :cond_6
    sget v1, Lig;->action_button:I

    if-eq v0, v1, :cond_7

    sget v1, Lig;->action_button_frame:I

    if-ne v0, v1, :cond_1

    :cond_7
    invoke-virtual {p0, v3, v2}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p0, v2, v3}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    iget-object v1, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ActionButton;->toggle()V

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0}, Lcom/twitter/library/widget/BaseUserView;->onFinishInflate()V

    sget v0, Lig;->action_button_accept_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/twitter/library/widget/aq;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/widget/aq;-><init>(Landroid/widget/ImageButton;Landroid/widget/FrameLayout;)V

    iput-object v2, p0, Lcom/twitter/library/widget/UserApprovalView;->o:Lcom/twitter/library/widget/aq;

    sget v0, Lig;->action_button_deny_frame:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    invoke-virtual {v0, v3}, Landroid/widget/FrameLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    new-instance v2, Lcom/twitter/library/widget/aq;

    invoke-direct {v2, v1, v0}, Lcom/twitter/library/widget/aq;-><init>(Landroid/widget/ImageButton;Landroid/widget/FrameLayout;)V

    iput-object v2, p0, Lcom/twitter/library/widget/UserApprovalView;->p:Lcom/twitter/library/widget/aq;

    sget v0, Lig;->action_button:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/widget/UserApprovalView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    return-void
.end method

.method public setState(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    invoke-virtual {p0, v1, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p0, v2, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p0, v2, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, v1, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p0, v2, v2}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v1, v2}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    invoke-virtual {p0, v2, v1}, Lcom/twitter/library/widget/UserApprovalView;->a(IZ)V

    iget-object v0, p0, Lcom/twitter/library/widget/UserApprovalView;->q:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
