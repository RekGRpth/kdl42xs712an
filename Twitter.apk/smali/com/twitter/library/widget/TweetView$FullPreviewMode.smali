.class public final enum Lcom/twitter/library/widget/TweetView$FullPreviewMode;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

.field public static final enum b:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

.field public static final enum c:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

.field public static final enum d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

.field private static final synthetic e:[Lcom/twitter/library/widget/TweetView$FullPreviewMode;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    const-string/jumbo v1, "Off"

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    new-instance v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    const-string/jumbo v1, "Normal"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->b:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    new-instance v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    const-string/jumbo v1, "NoCrop"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->c:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    new-instance v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    const-string/jumbo v1, "FullWidth"

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/widget/TweetView$FullPreviewMode;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->b:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->c:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->d:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->e:[Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/widget/TweetView$FullPreviewMode;
    .locals 1

    const-class v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/widget/TweetView$FullPreviewMode;
    .locals 1

    sget-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->e:[Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v0}, [Lcom/twitter/library/widget/TweetView$FullPreviewMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    return-object v0
.end method
