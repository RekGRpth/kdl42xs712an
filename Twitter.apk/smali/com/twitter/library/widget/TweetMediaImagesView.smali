.class public Lcom/twitter/library/widget/TweetMediaImagesView;
.super Landroid/view/View;
.source "Twttr"


# instance fields
.field private final a:F

.field private b:I

.field private c:F

.field private d:Lcom/twitter/library/api/TweetClassicCard;

.field private e:Ljava/util/List;

.field private f:Lcom/twitter/library/api/MediaEntity;

.field private g:[Lcom/twitter/library/util/m;

.field private h:I

.field private i:Landroid/widget/ImageView;

.field private j:[Landroid/widget/ImageView;

.field private k:I

.field private l:I

.field private m:Z

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->o:Z

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    iput v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->a:F

    sget-object v0, Lim;->TweetMediaImagesView:[I

    invoke-virtual {p1, p2, v0, p3, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void
.end method

.method private a(Landroid/graphics/Bitmap;I)V
    .locals 10

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v9, v0, p2

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->p:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    if-ne v0, v8, :cond_1

    :goto_0
    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->o:Z

    if-nez v0, :cond_0

    if-eqz v8, :cond_3

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    iget v0, v0, Lcom/twitter/library/api/TweetClassicCard;->imageWidth:I

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    iget v1, v1, Lcom/twitter/library/api/TweetClassicCard;->imageHeight:I

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    iget-object v4, v2, Lcom/twitter/library/api/TweetClassicCard;->features:Ljava/util/HashMap;

    :goto_1
    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    aget-object v3, v2, p2

    new-instance v6, Landroid/graphics/Matrix;

    invoke-direct {v6}, Landroid/graphics/Matrix;-><init>()V

    iget v2, v3, Lcom/twitter/library/util/m;->b:I

    iget v3, v3, Lcom/twitter/library/util/m;->c:I

    iget-boolean v7, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->o:Z

    move-object v5, p1

    invoke-static/range {v0 .. v8}, Lcom/twitter/library/util/k;->a(IIIILjava/util/HashMap;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;ZZ)Landroid/graphics/Bitmap;

    move-result-object p1

    sget-object v0, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    invoke-virtual {v9, v6}, Landroid/widget/ImageView;->setImageMatrix(Landroid/graphics/Matrix;)V

    :goto_2
    invoke-virtual {v9, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    const/4 v1, 0x0

    aput-object v1, v0, p2

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->h:I

    return-void

    :cond_1
    const/4 v8, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget v2, v0, Lcom/twitter/library/api/MediaEntity;->width:I

    iget v1, v0, Lcom/twitter/library/api/MediaEntity;->height:I

    iget-object v4, v0, Lcom/twitter/library/api/MediaEntity;->features:Ljava/util/HashMap;

    move v0, v2

    goto :goto_1

    :cond_3
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v9, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_2
.end method

.method private b(II)V
    .locals 3

    const/high16 v2, 0x40000000    # 2.0f

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-static {p1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {p2, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/ImageView;->measure(II)V

    return-void
.end method

.method private c(II)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {p2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {p2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v1, v0, v2}, Landroid/widget/ImageView;->measure(II)V

    return-void
.end method

.method private d(II)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v2, 0x0

    aget-object v1, v1, v2

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-static {p2, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/widget/ImageView;->measure(II)V

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/ImageView;->measure(II)V

    return-void
.end method

.method private e()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    iput-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->h:I

    return-void
.end method

.method private e(II)V
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/ImageView;->measure(II)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    invoke-static {v0, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-static {v1, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {v2, v0, v1}, Landroid/widget/ImageView;->measure(II)V

    return-void
.end method

.method private f()V
    .locals 7

    const/4 v4, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    iget v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    if-eq v3, v0, :cond_2

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->n:Z

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->g()V

    iput v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    if-nez v0, :cond_3

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v4, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    if-nez v1, :cond_4

    new-array v1, v4, [Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getContext()Landroid/content/Context;

    move-result-object v4

    iget v5, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->l:I

    move v3, v2

    :goto_2
    if-ge v3, v0, :cond_0

    aget-object v2, v1, v3

    aget-object v6, v1, v3

    if-nez v6, :cond_5

    new-instance v2, Landroid/widget/ImageView;

    invoke-direct {v2, v4}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    aput-object v2, v1, v3

    :cond_5
    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v6, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v6}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2
.end method

.method private f(II)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v0, v0, v1

    invoke-virtual {v0, v1, v1, p1, p2}, Landroid/widget/ImageView;->layout(IIII)V

    return-void
.end method

.method private g()V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    iget v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    iget-object v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, v3, v0

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    :cond_1
    return-void
.end method

.method private g(II)V
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v1, v1, v3

    invoke-virtual {v1, v3, v3, v0, p2}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v2, 0x1

    aget-object v1, v1, v2

    iget v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    add-int/2addr v0, v2

    invoke-virtual {v1, v0, v3, p1, p2}, Landroid/widget/ImageView;->layout(IIII)V

    return-void
.end method

.method private h(II)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v1, v1, v4

    invoke-virtual {v1, v4, v4, v0, p2}, Landroid/widget/ImageView;->layout(IIII)V

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    iget v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2, v0, v4, p1, v1}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v0, v1, p1, p2}, Landroid/widget/ImageView;->layout(IIII)V

    return-void
.end method

.method private i(II)V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v0, p1, v0

    div-int/lit8 v0, v0, 0x2

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    sub-int v1, p2, v1

    div-int/lit8 v1, v1, 0x2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v2, v2, v4

    invoke-virtual {v2, v4, v4, v0, v1}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    iget v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    add-int/2addr v3, v1

    invoke-virtual {v2, v4, v3, v0, p2}, Landroid/widget/ImageView;->layout(IIII)V

    iget v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    invoke-virtual {v2, v0, v4, p1, v1}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    iget v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    add-int/2addr v1, v3

    invoke-virtual {v2, v0, v1, p1, p2}, Landroid/widget/ImageView;->layout(IIII)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->f:Lcom/twitter/library/api/MediaEntity;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    iput-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    return-void
.end method

.method public a(II)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_0

    iput-object v4, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->f:Lcom/twitter/library/api/MediaEntity;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    if-nez v0, :cond_1

    iput-object v4, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->f:Lcom/twitter/library/api/MediaEntity;

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLeft()I

    move-result v3

    if-le p1, v3, :cond_2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getRight()I

    move-result v3

    if-ge p1, v3, :cond_2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getTop()I

    move-result v3

    if-le p2, v3, :cond_2

    invoke-virtual {v2}, Landroid/widget/ImageView;->getBottom()I

    move-result v2

    if-ge p2, v2, :cond_2

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iput-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->f:Lcom/twitter/library/api/MediaEntity;

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iput-object v4, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->f:Lcom/twitter/library/api/MediaEntity;

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->o:Z

    return-void
.end method

.method public a(Lcom/twitter/library/widget/ap;)Z
    .locals 8

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    if-nez v0, :cond_1

    move v1, v2

    :cond_0
    :goto_0
    return v1

    :cond_1
    iget v4, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    if-nez v0, :cond_3

    new-array v1, v4, [Lcom/twitter/library/util/m;

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    new-instance v5, Lcom/twitter/library/util/m;

    iget-object v6, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    iget v7, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->a:F

    invoke-virtual {v6, v7}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v6

    iget-object v6, v6, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    invoke-direct {v5, v6, v3, v0}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    aput-object v5, v1, v2

    :cond_2
    iput-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    move-object v0, v1

    :cond_3
    move v1, v2

    :goto_1
    if-ge v2, v4, :cond_6

    aget-object v3, v0, v2

    if-eqz v3, :cond_4

    aget-object v3, v0, v2

    invoke-interface {p1, v3}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eqz v3, :cond_4

    invoke-direct {p0, v3, v2}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Landroid/graphics/Bitmap;I)V

    const/4 v1, 0x1

    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_5
    move v3, v2

    :goto_2
    if-ge v3, v4, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v0, v0, v3

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v5

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v6

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    new-instance v7, Lcom/twitter/library/util/m;

    iget-object v0, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-direct {v7, v0, v5, v6}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    aput-object v7, v1, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_6
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->invalidate()V

    goto :goto_0
.end method

.method public a(Ljava/util/Map;)Z
    .locals 6

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    if-eqz v3, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget-object v0, v3, v2

    if-eqz v0, :cond_3

    aget-object v0, v3, v2

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v5

    if-eqz v5, :cond_3

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    if-eqz v0, :cond_3

    invoke-direct {p0, v0, v2}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(Landroid/graphics/Bitmap;I)V

    const/4 v0, 0x1

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->invalidate()V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public b()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->e()V

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->g()V

    return-void
.end method

.method public b(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->p:Z

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    return v0
.end method

.method public d()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->h:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLastTouchedMedia()Lcom/twitter/library/api/MediaEntity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->f:Lcom/twitter/library/api/MediaEntity;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 9

    const/high16 v8, 0x40000000    # 2.0f

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    move v0, v1

    :goto_0
    if-ge v0, v2, :cond_0

    iget-object v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    invoke-virtual {v3}, Landroid/widget/ImageView;->getLeft()I

    move-result v4

    invoke-virtual {v3}, Landroid/widget/ImageView;->getTop()I

    move-result v5

    invoke-virtual {v3}, Landroid/widget/ImageView;->getRight()I

    move-result v6

    invoke-virtual {v3}, Landroid/widget/ImageView;->getBottom()I

    move-result v7

    invoke-virtual {p1, v4, v5, v6, v7}, Landroid/graphics/Canvas;->clipRect(IIII)Z

    int-to-float v4, v4

    int-to-float v5, v5

    invoke-virtual {p1, v4, v5}, Landroid/graphics/Canvas;->translate(FF)V

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getMeasuredHeight()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    invoke-static {v0, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-static {v2, v8}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v5

    invoke-virtual {v3, v4, v5}, Landroid/widget/ImageView;->measure(II)V

    iget-object v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    invoke-virtual {v3, v1, v1, v0, v2}, Landroid/widget/ImageView;->layout(IIII)V

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->draw(Landroid/graphics/Canvas;)V

    :cond_1
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 3

    if-nez p1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->n:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->n:Z

    sub-int v0, p4, p2

    sub-int v1, p5, p3

    iget v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    packed-switch v2, :pswitch_data_0

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->i(II)V

    :cond_1
    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->f(II)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->g(II)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->h(II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    if-ne v0, v1, :cond_0

    if-eq v3, v1, :cond_2

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/View;->onMeasure(II)V

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getMeasuredWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getMeasuredHeight()I

    move-result v1

    move v2, v0

    :goto_0
    if-nez v1, :cond_3

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->c:F

    const/4 v4, 0x0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_3

    int-to-float v0, v2

    iget v4, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->c:F

    mul-float/2addr v0, v4

    float-to-int v0, v0

    const/high16 v4, -0x80000000

    if-ne v3, v4, :cond_1

    invoke-static {v1, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    :cond_1
    invoke-virtual {p0, v2, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->setMeasuredDimension(II)V

    :goto_1
    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, v2, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->e(II)V

    :goto_2
    return-void

    :cond_2
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->setMeasuredDimension(II)V

    move v2, v0

    goto :goto_0

    :pswitch_0
    invoke-direct {p0, v2, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->b(II)V

    goto :goto_2

    :pswitch_1
    invoke-direct {p0, v2, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->c(II)V

    goto :goto_2

    :pswitch_2
    invoke-direct {p0, v2, v0}, Lcom/twitter/library/widget/TweetMediaImagesView;->d(II)V

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/widget/TweetMediaImagesView;->a(II)V

    :cond_0
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public setClassicCard(Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 2

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->a:F

    invoke-virtual {p1, v0}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->a:F

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->a:F

    invoke-virtual {p1, v1}, Lcom/twitter/library/api/TweetClassicCard;->a(F)Lcom/twitter/library/api/aa;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/api/aa;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->e()V

    iput-object p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->d:Lcom/twitter/library/api/TweetClassicCard;

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->f()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->b()V

    goto :goto_1
.end method

.method public setForeground(Landroid/graphics/drawable/Drawable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    sget-object v1, Landroid/widget/ImageView$ScaleType;->CENTER:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iput-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->i:Landroid/widget/ImageView;

    :cond_0
    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setHeightWidthRatio(F)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->c:F

    return-void
.end method

.method public setMediaDividerSize(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->b:I

    return-void
.end method

.method public setMediaEntities(Ljava/util/List;)V
    .locals 1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    iget-boolean v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->m:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    if-eqz v0, :cond_1

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->e()V

    iput-object p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->e:Ljava/util/List;

    invoke-direct {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->f()V

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/widget/TweetMediaImagesView;->b()V

    goto :goto_1
.end method

.method public setMediaPlaceholder(I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    iput p1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->l:I

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->g:[Lcom/twitter/library/util/m;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->k:I

    :goto_0
    if-ge v0, v1, :cond_2

    iget-object v2, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v2, v2, v0

    invoke-virtual {v2, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v2, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v3, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v2, v1

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/twitter/library/widget/TweetMediaImagesView;->j:[Landroid/widget/ImageView;

    aget-object v3, v3, v0

    invoke-virtual {v3, v5}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v4, Landroid/widget/ImageView$ScaleType;->CENTER_CROP:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    return-void
.end method
