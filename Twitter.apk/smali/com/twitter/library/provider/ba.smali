.class Lcom/twitter/library/provider/ba;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:J

.field public final b:I

.field public final c:J

.field public final d:J

.field public final e:J

.field public final f:I

.field public final g:I

.field public final h:Ljava/util/ArrayList;

.field public final i:I

.field public final j:I

.field public final k:Ljava/util/ArrayList;

.field public final l:Ljava/util/ArrayList;

.field public final m:Ljava/util/ArrayList;

.field public final n:I

.field public final o:I

.field public final p:Ljava/util/ArrayList;

.field public final q:Ljava/util/ArrayList;

.field public final r:Ljava/util/ArrayList;

.field public final s:I


# direct methods
.method public constructor <init>(IILjava/util/ArrayList;IILjava/util/ArrayList;)V
    .locals 24

    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const-wide/16 v6, 0x0

    const-wide/16 v8, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    move-object/from16 v0, p0

    move/from16 v13, p1

    move/from16 v14, p2

    move-object/from16 v16, p3

    move/from16 v18, p4

    move/from16 v19, p5

    move-object/from16 v20, p6

    invoke-direct/range {v0 .. v23}, Lcom/twitter/library/provider/ba;-><init>(JIJJJIILjava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V

    return-void
.end method

.method public constructor <init>(JIJJJIILjava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;IILjava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/provider/ba;->a:J

    iput p3, p0, Lcom/twitter/library/provider/ba;->b:I

    iput-wide p4, p0, Lcom/twitter/library/provider/ba;->c:J

    iput-wide p6, p0, Lcom/twitter/library/provider/ba;->d:J

    iput-wide p8, p0, Lcom/twitter/library/provider/ba;->e:J

    iput p10, p0, Lcom/twitter/library/provider/ba;->f:I

    iput p11, p0, Lcom/twitter/library/provider/ba;->g:I

    iput-object p12, p0, Lcom/twitter/library/provider/ba;->h:Ljava/util/ArrayList;

    iput p13, p0, Lcom/twitter/library/provider/ba;->i:I

    iput p14, p0, Lcom/twitter/library/provider/ba;->j:I

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/library/provider/ba;->k:Ljava/util/ArrayList;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/library/provider/ba;->l:Ljava/util/ArrayList;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/library/provider/ba;->m:Ljava/util/ArrayList;

    move/from16 v0, p18

    iput v0, p0, Lcom/twitter/library/provider/ba;->n:I

    move/from16 v0, p19

    iput v0, p0, Lcom/twitter/library/provider/ba;->o:I

    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/twitter/library/provider/ba;->p:Ljava/util/ArrayList;

    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/twitter/library/provider/ba;->q:Ljava/util/ArrayList;

    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/twitter/library/provider/ba;->r:Ljava/util/ArrayList;

    move/from16 v0, p23

    iput v0, p0, Lcom/twitter/library/provider/ba;->s:I

    return-void
.end method
