.class public Lcom/twitter/library/provider/Tweet;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/provider/ParcelableTweet;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field public static final a:[Ljava/lang/String;

.field public static final b:[Ljava/lang/String;

.field public static final c:[Ljava/lang/String;


# instance fields
.field public A:Lcom/twitter/library/api/TweetEntities;

.field public B:Z

.field public C:J

.field public D:Z

.field public E:Lcom/twitter/library/api/geo/TwitterPlace;

.field public F:J

.field public G:Ljava/lang/String;

.field public H:I

.field public I:I

.field public J:Lcom/twitter/library/api/PromotedContent;

.field public K:Lcom/twitter/library/api/RecommendedContent;

.field public L:J

.field public M:Z

.field public N:I

.field public O:Lcom/twitter/library/api/TwitterStatusCard;

.field public P:I

.field public Q:Ljava/lang/String;

.field public R:I

.field public S:I

.field public T:[Lcom/twitter/library/api/Entity;

.field public U:Lcom/twitter/library/api/ActivitySummary;

.field public V:I

.field public W:Z

.field public X:Ljava/lang/String;

.field public Y:Ljava/lang/String;

.field public Z:I

.field public aa:Ljava/lang/String;

.field public ab:I

.field public ac:I

.field public ad:J

.field public ae:I

.field public af:Ljava/lang/String;

.field public ag:I

.field public ah:I

.field public ai:Ljava/util/ArrayList;

.field public aj:Ljava/lang/Object;

.field private ak:Z

.field private al:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Ljava/lang/String;

.field public g:Ljava/lang/String;

.field public h:J

.field public i:Ljava/lang/String;

.field public j:J

.field public k:Ljava/lang/String;

.field public l:Z

.field public m:Z

.field public n:J

.field public o:J

.field public p:Ljava/lang/String;

.field public q:J

.field public r:Z

.field public s:Z

.field public t:Z

.field public u:J

.field public v:I

.field public w:Z

.field public x:D

.field public y:D

.field public z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x0

    new-instance v0, Lcom/twitter/library/provider/v;

    invoke-direct {v0}, Lcom/twitter/library/provider/v;-><init>()V

    sput-object v0, Lcom/twitter/library/provider/Tweet;->CREATOR:Landroid/os/Parcelable$Creator;

    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "_id"

    aput-object v1, v0, v3

    const/4 v1, 0x1

    const-string/jumbo v2, "g_status_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "content"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "username"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "author_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "updated_at"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "source"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "in_r_status_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "image_url"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "user_flags"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "favorited"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "latitude"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "longitude"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "is_last"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "timeline"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "entities"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "type"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "tweet_type"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "sender_id"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "s_username"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "ref_id"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "s_name"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "created"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "r_content"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "flags"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "pc"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "g_flags"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "is_read"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "cards"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "friendship"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "friendship_time"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "retweet_count"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "favorite_count"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "lang"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "reply_count"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "view_count"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "place_data"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    sget-object v2, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v0, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v1, v1

    const-string/jumbo v2, "rt_orig_ref_id"

    aput-object v2, v0, v1

    sget-object v0, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/twitter/library/provider/Tweet;->c:[Ljava/lang/String;

    sget-object v0, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->c:[Ljava/lang/String;

    sget-object v2, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v2, v2

    invoke-static {v0, v3, v1, v3, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    sget-object v0, Lcom/twitter/library/provider/Tweet;->c:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v1, v1

    const-string/jumbo v2, "soc_type"

    aput-object v2, v0, v1

    sget-object v0, Lcom/twitter/library/provider/Tweet;->c:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x1

    const-string/jumbo v2, "soc_name"

    aput-object v2, v0, v1

    sget-object v0, Lcom/twitter/library/provider/Tweet;->c:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x2

    const-string/jumbo v2, "highlights"

    aput-object v2, v0, v1

    sget-object v0, Lcom/twitter/library/provider/Tweet;->c:[Ljava/lang/String;

    sget-object v1, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    array-length v1, v1

    add-int/lit8 v1, v1, 0x3

    const-string/jumbo v2, "retweet_count"

    aput-object v2, v0, v1

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v1, p0, Lcom/twitter/library/provider/Tweet;->j:J

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->v:I

    iput-wide v1, p0, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 3

    const-wide/16 v1, -0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v1, p0, Lcom/twitter/library/provider/Tweet;->j:J

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->v:I

    iput-wide v1, p0, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    invoke-virtual {p0, p1}, Lcom/twitter/library/provider/Tweet;->a(Landroid/database/Cursor;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 6

    const-wide/16 v3, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->j:J

    iput v1, p0, Lcom/twitter/library/provider/Tweet;->v:I

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->i:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->j:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->l:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->m:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->q:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->v:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->w:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->x:D

    invoke-virtual {p1}, Landroid/os/Parcel;->readDouble()D

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->y:D

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->z:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->B:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_6

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->D:Z

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RecommendedContent;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->K:Lcom/twitter/library/api/RecommendedContent;

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_7

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->M:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->N:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatusCard;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->P:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->R:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->S:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/Entity;

    check-cast v0, [Lcom/twitter/library/api/Entity;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ActivitySummary;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->U:Lcom/twitter/library/api/ActivitySummary;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->V:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_8

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->W:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->X:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->Y:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->Z:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->aa:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ab:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readLong()J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->ad:J

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ae:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ag:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ah:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readSerializable()Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_9

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->s:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_a

    :goto_a
    iput-boolean v1, p0, Lcom/twitter/library/provider/Tweet;->t:Z

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_7

    :cond_8
    move v0, v2

    goto :goto_8

    :cond_9
    move v0, v2

    goto :goto_9

    :cond_a
    move v1, v2

    goto :goto_a
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 2

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    :goto_0
    if-ge p1, v0, :cond_0

    invoke-virtual {p0, p1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(C)Z

    move-result v1

    if-eqz v1, :cond_0

    add-int/lit8 p1, p1, 0x1

    goto :goto_0

    :cond_0
    return p1
.end method

.method private a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/library/provider/Tweet;->af()Ljava/util/LinkedHashSet;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Entity;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/Entity;->a(I)V

    goto :goto_0

    :cond_0
    return-void
.end method

.method private ae()V
    .locals 7

    const/16 v6, 0x200f

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    array-length v0, v0

    if-lez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/library/util/text/b;->k:Ljava/util/regex/Pattern;

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->al:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->charAt(I)C

    move-result v0

    if-ne v0, v6, :cond_2

    const/4 v0, 0x1

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/twitter/library/provider/Tweet;->a(Ljava/lang/String;I)I

    move-result v0

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v3, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v5, v3, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    move v3, v0

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MentionEntity;

    iget v0, v0, Lcom/twitter/library/api/MentionEntity;->start:I

    if-ne v0, v3, :cond_3

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MentionEntity;

    invoke-static {v0}, Lcom/twitter/library/api/RepliedUser;->a(Lcom/twitter/library/api/MentionEntity;)Lcom/twitter/library/api/RepliedUser;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget v0, v0, Lcom/twitter/library/api/MentionEntity;->end:I

    invoke-static {v3, v0}, Lcom/twitter/library/provider/Tweet;->a(Ljava/lang/String;I)I

    move-result v0

    move v3, v0

    goto :goto_2

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    if-eqz v1, :cond_4

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    add-int/lit8 v3, v3, -0x1

    :cond_4
    neg-int v0, v3

    invoke-direct {p0, v0}, Lcom/twitter/library/provider/Tweet;->a(I)V

    iput-object v4, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method private af()Ljava/util/LinkedHashSet;
    .locals 2

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_4
    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    :cond_6
    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method private ag()Z
    .locals 2

    invoke-direct {p0}, Lcom/twitter/library/provider/Tweet;->ah()I

    move-result v0

    const/16 v1, 0x14

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/util/ad;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private ah()I
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-direct {p0}, Lcom/twitter/library/provider/Tweet;->af()Ljava/util/LinkedHashSet;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Entity;

    iget v3, v0, Lcom/twitter/library/api/Entity;->end:I

    iget v0, v0, Lcom/twitter/library/api/Entity;->start:I

    sub-int v0, v3, v0

    sub-int v0, v1, v0

    move v1, v0

    goto :goto_0

    :cond_0
    return v1
.end method

.method public static b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->ad()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v1

    :goto_0
    if-lez v0, :cond_0

    add-int/lit8 v2, v0, -0x1

    aget-char v2, v1, v2

    const/16 v3, 0x20

    if-gt v2, v3, :cond_0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public A()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->al:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->al:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->z()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public B()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public C()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit8 v0, v0, 0x10

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public D()Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->j:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public E()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public F()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public G()Z
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->ak:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->L()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->s:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->F()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->N:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->N:I

    const/16 v1, 0xd

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public H()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit8 v0, v0, 0x40

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public I()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit16 v0, v0, 0x80

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public J()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->Z:I

    invoke-static {v0}, Lcom/twitter/library/provider/au;->c(I)Z

    move-result v0

    return v0
.end method

.method public K()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->Z:I

    invoke-static {v0}, Lcom/twitter/library/provider/au;->d(I)Z

    move-result v0

    return v0
.end method

.method public L()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->K:Lcom/twitter/library/api/RecommendedContent;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public M()Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public N()Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public O()Z
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_0

    iget v1, v0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-wide v0, v0, Lcom/twitter/library/api/CardUser;->userId:J

    const-wide/32 v2, 0x22f7e725

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public P()Z
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_1

    iget v1, v0, Lcom/twitter/library/api/TweetClassicCard;->type:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-wide v1, v1, Lcom/twitter/library/api/CardUser;->userId:J

    const-wide/32 v3, 0x1286cf7

    cmp-long v1, v1, v3

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-wide v0, v0, Lcom/twitter/library/api/CardUser;->userId:J

    const-wide/32 v2, 0x270651

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Q()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    const-string/jumbo v1, "amplify"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    const-string/jumbo v1, "video"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public R()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    const-string/jumbo v1, "animated_gif"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public S()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/MediaEntity;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public T()Lcom/twitter/library/api/MediaEntity;
    .locals 1

    const/4 v0, 0x0

    invoke-static {p0, v0, v0}, Lcom/twitter/library/util/s;->b(Lcom/twitter/library/provider/Tweet;II)Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    return-object v0
.end method

.method public U()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0
.end method

.method public V()Lcom/twitter/library/card/instance/CardInstanceData;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    goto :goto_0
.end method

.method public W()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    const-string/jumbo v1, "promotion"

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public X()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    const-string/jumbo v1, "click_to_call"

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Y()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "promo_website"

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public Z()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/api/TwitterStatusCard;->a(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v0}, Lcom/twitter/library/api/TwitterStatusCard;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    return-object v0
.end method

.method public a(ZZZZZ)Ljava/lang/String;
    .locals 8

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v6

    if-eqz p1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->z()Ljava/lang/String;

    move-result-object v3

    :goto_0
    if-eqz p2, :cond_7

    if-nez v5, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->O()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->R()Z

    move-result v0

    if-eqz v0, :cond_7

    :cond_0
    move v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->l()Z

    move-result v4

    if-eqz v4, :cond_8

    move v4, v1

    :goto_2
    if-nez v0, :cond_1

    if-nez v4, :cond_1

    if-nez p3, :cond_1

    if-nez p4, :cond_1

    if-eqz p5, :cond_c

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->B()Z

    move-result v0

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v4, v4, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    const/16 v4, 0x200e

    invoke-static {v4}, Ljava/lang/Character;->toString(C)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_d

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v2, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    :goto_3
    if-eqz v5, :cond_2

    iget-object v7, v5, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    if-eqz v7, :cond_2

    iget-object v5, v5, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    iget-object v7, v0, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v5, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_3

    :cond_2
    if-eqz v6, :cond_9

    iget-object v5, v6, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    if-eqz v5, :cond_9

    iget-object v5, v6, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    iget-object v6, v0, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_9

    :cond_3
    :goto_4
    if-eqz v1, :cond_c

    if-eqz p1, :cond_a

    iget-object v1, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    :goto_5
    invoke-virtual {v4, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    if-eqz p1, :cond_b

    iget v0, v0, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    :goto_6
    invoke-virtual {v4, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :goto_7
    if-nez p3, :cond_4

    if-eqz p4, :cond_5

    :cond_4
    invoke-direct {p0, v0}, Lcom/twitter/library/provider/Tweet;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_5
    return-object v0

    :cond_6
    iget-object v3, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    goto/16 :goto_0

    :cond_7
    move v0, v2

    goto/16 :goto_1

    :cond_8
    move v4, v2

    goto :goto_2

    :cond_9
    move v1, v2

    goto :goto_4

    :cond_a
    iget-object v1, v0, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    goto :goto_5

    :cond_b
    iget v0, v0, Lcom/twitter/library/api/UrlEntity;->start:I

    goto :goto_6

    :cond_c
    move-object v0, v3

    goto :goto_7

    :cond_d
    move-object v4, v3

    goto :goto_3
.end method

.method public a(Landroid/database/Cursor;)V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, -0x1

    const/4 v2, 0x0

    const/4 v1, 0x1

    const/16 v0, 0x13

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->q:J

    const/16 v0, 0x14

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->u:J

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-interface {p1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    const/16 v0, 0x17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->h:J

    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->i:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->j:J

    const/16 v0, 0x9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    const/16 v0, 0xb

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_d

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->l:Z

    const/16 v0, 0xa

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    and-int/lit8 v0, v3, 0x1

    if-eqz v0, :cond_e

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->m:Z

    and-int/lit8 v0, v3, 0x2

    if-eqz v0, :cond_f

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->D:Z

    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->u:J

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->o:J

    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_10

    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_10

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->w:Z

    if-eqz v0, :cond_0

    const/16 v0, 0xc

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->x:D

    const/16 v0, 0xd

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->y:D

    :cond_0
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    :cond_1
    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, v1, :cond_11

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-ne v3, v7, :cond_12

    move v0, v1

    :goto_5
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->B:Z

    const/4 v0, 0x7

    if-ne v3, v0, :cond_13

    move v0, v1

    :goto_6
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->W:Z

    if-ne v3, v6, :cond_14

    move v0, v1

    :goto_7
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->s:Z

    const/4 v0, 0x5

    if-ne v3, v0, :cond_15

    move v0, v1

    :goto_8
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->t:Z

    const/16 v0, 0x15

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->C:J

    const/16 v0, 0x25

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_2

    const/16 v0, 0x25

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v0, :cond_16

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    if-eqz v0, :cond_16

    move v0, v1

    :goto_9
    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->z:Z

    const-string/jumbo v0, "rt_orig_ref_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_17

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->F:J

    :goto_a
    const/16 v0, 0x16

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    const/16 v0, 0x18

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    const/16 v0, 0x19

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    const/16 v0, 0x1b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    const/16 v0, 0x1a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->L:J

    const/16 v0, 0x1c

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v1, :cond_18

    :goto_b
    iput-boolean v1, p0, Lcom/twitter/library/provider/Tweet;->M:Z

    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->N:I

    const/16 v0, 0x1d

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterStatusCard;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    const-string/jumbo v0, "soc_type"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->P:I

    :cond_3
    const-string/jumbo v0, "soc_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    :cond_4
    const-string/jumbo v0, "soc_fav_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->R:I

    :cond_5
    const-string/jumbo v0, "soc_rt_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->S:I

    :cond_6
    const-string/jumbo v0, "highlights"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/Entity;

    check-cast v0, [Lcom/twitter/library/api/Entity;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    :cond_7
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->U:Lcom/twitter/library/api/ActivitySummary;

    const/16 v0, 0x20

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->V:I

    const/16 v0, 0x21

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ae:I

    const/16 v0, 0x23

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ag:I

    const/16 v0, 0x24

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ah:I

    const-string/jumbo v0, "cl_title"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_8

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->X:Ljava/lang/String;

    :cond_8
    const-string/jumbo v0, "t_flags"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->Z:I

    const/high16 v0, 0x10000

    iget v1, p0, Lcom/twitter/library/provider/Tweet;->Z:I

    if-ne v0, v1, :cond_9

    const-string/jumbo v0, "t_data"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_9

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RecommendedContent;

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->K:Lcom/twitter/library/api/RecommendedContent;

    :cond_9
    const-string/jumbo v0, "soc_second_name"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_a

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->aa:Ljava/lang/String;

    :cond_a
    const-string/jumbo v0, "soc_others_count"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v5, :cond_b

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ab:I

    :cond_b
    const/16 v0, 0x1e

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/provider/Tweet;->ac:I

    const/16 v0, 0x1f

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/provider/Tweet;->ad:J

    const/16 v0, 0x22

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-direct {p0}, Lcom/twitter/library/provider/Tweet;->ae()V

    :cond_c
    return-void

    :cond_d
    move v0, v2

    goto/16 :goto_0

    :cond_e
    move v0, v2

    goto/16 :goto_1

    :cond_f
    move v0, v2

    goto/16 :goto_2

    :cond_10
    move v0, v2

    goto/16 :goto_3

    :cond_11
    move v0, v2

    goto/16 :goto_4

    :cond_12
    move v0, v2

    goto/16 :goto_5

    :cond_13
    move v0, v2

    goto/16 :goto_6

    :cond_14
    move v0, v2

    goto/16 :goto_7

    :cond_15
    move v0, v2

    goto/16 :goto_8

    :cond_16
    move v0, v2

    goto/16 :goto_9

    :cond_17
    const-wide/16 v3, -0x1

    iput-wide v3, p0, Lcom/twitter/library/provider/Tweet;->F:J

    goto/16 :goto_a

    :cond_18
    move v1, v2

    goto/16 :goto_b
.end method

.method public a(Ljava/lang/Object;)V
    .locals 2

    invoke-static {}, Lkm;->f()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->ak:Z

    iput-object p1, p0, Lcom/twitter/library/provider/Tweet;->aj:Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public a(J)Z
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->F:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v0, v0, p1

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z
    .locals 6

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aQ()Z

    move-result v1

    if-eqz v1, :cond_0

    if-eqz p2, :cond_0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    if-eqz v1, :cond_0

    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/library/provider/Tweet;->m:Z

    if-nez v2, :cond_0

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-static {v1}, Lcom/twitter/library/util/ad;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-static {v2}, Lcom/twitter/library/util/ad;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/provider/Tweet;->ag()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-nez p1, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    iget-wide v2, p0, Lcom/twitter/library/provider/Tweet;->u:J

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    goto :goto_0

    :cond_3
    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p1, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p1, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-boolean v2, p0, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-boolean v3, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-boolean v2, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    iget-boolean v3, p1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget v2, p0, Lcom/twitter/library/provider/Tweet;->R:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->R:I

    if-eq v2, v3, :cond_8

    move v0, v1

    goto :goto_0

    :cond_8
    iget v2, p0, Lcom/twitter/library/provider/Tweet;->S:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->S:I

    if-eq v2, v3, :cond_9

    move v0, v1

    goto :goto_0

    :cond_9
    iget v2, p0, Lcom/twitter/library/provider/Tweet;->V:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->V:I

    if-eq v2, v3, :cond_a

    move v0, v1

    goto :goto_0

    :cond_a
    iget v2, p0, Lcom/twitter/library/provider/Tweet;->ae:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->ae:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget v2, p0, Lcom/twitter/library/provider/Tweet;->ag:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->ag:I

    if-eq v2, v3, :cond_c

    move v0, v1

    goto :goto_0

    :cond_c
    iget v2, p0, Lcom/twitter/library/provider/Tweet;->ah:I

    iget v3, p1, Lcom/twitter/library/provider/Tweet;->ah:I

    if-eq v2, v3, :cond_d

    move v0, v1

    goto :goto_0

    :cond_d
    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v3

    if-eqz v2, :cond_0

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v3}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-eq v2, v3, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public aa()Ljava/lang/String;
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v2, "app_id"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method public ab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-boolean v0, v0, Lcom/twitter/library/api/PromotedContent;->isPAcInTimeline:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ac()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-boolean v0, v0, Lcom/twitter/library/api/PromotedContent;->isSuppressMediaForward:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ad()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "rec_tweet"

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->s:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "injected_tweet"

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->t:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "curated_tweet"

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "tweet"

    goto :goto_0
.end method

.method public b()J
    .locals 4

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->F:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->F:J

    :goto_0
    return-wide v0

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->C:J

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    return-void
.end method

.method public c()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->C:J

    return-wide v0
.end method

.method public d()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->o:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/provider/Tweet;

    iget-wide v2, p0, Lcom/twitter/library/provider/Tweet;->u:J

    iget-wide v4, p1, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public f()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->q:J

    return-wide v0
.end method

.method public g()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->n:J

    return-wide v0
.end method

.method public h()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/twitter/library/provider/Tweet;->u:J

    iget-wide v2, p0, Lcom/twitter/library/provider/Tweet;->u:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    return-object v0
.end method

.method public j()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    goto :goto_0
.end method

.method public k()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    goto :goto_0
.end method

.method public l()Lcom/twitter/library/api/TweetClassicCard;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0
.end method

.method public m()Lcom/twitter/library/api/PromotedContent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    return-object v0
.end method

.method public n()Lcom/twitter/library/api/TwitterStatusCard;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    return-object v0
.end method

.method public o()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic p()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->U()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public q()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public r()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public s()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    and-int/lit8 v0, v0, 0x20

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    and-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public v()Z
    .locals 1

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public w()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->h:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->i:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->j:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->m:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->q:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->v:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->w:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->x:D

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->y:D

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeDouble(D)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->z:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->B:Z

    if-eqz v0, :cond_5

    move v0, v1

    :goto_5
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->D:Z

    if-eqz v0, :cond_6

    move v0, v1

    :goto_6
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->E:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->H:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->I:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->K:Lcom/twitter/library/api/RecommendedContent;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->M:Z

    if-eqz v0, :cond_7

    move v0, v1

    :goto_7
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->N:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->P:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->R:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->S:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->T:[Lcom/twitter/library/api/Entity;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->U:Lcom/twitter/library/api/ActivitySummary;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->V:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->W:Z

    if-eqz v0, :cond_8

    move v0, v1

    :goto_8
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->X:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->Y:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->Z:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->aa:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->ab:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-wide v3, p0, Lcom/twitter/library/provider/Tweet;->ad:J

    invoke-virtual {p1, v3, v4}, Landroid/os/Parcel;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->ae:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->af:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->ag:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/provider/Tweet;->ah:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->ai:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeSerializable(Ljava/io/Serializable;)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->s:Z

    if-eqz v0, :cond_9

    move v0, v1

    :goto_9
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/twitter/library/provider/Tweet;->t:Z

    if-eqz v0, :cond_a

    :goto_a
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto/16 :goto_0

    :cond_1
    move v0, v2

    goto/16 :goto_1

    :cond_2
    move v0, v2

    goto/16 :goto_2

    :cond_3
    move v0, v2

    goto/16 :goto_3

    :cond_4
    move v0, v2

    goto/16 :goto_4

    :cond_5
    move v0, v2

    goto/16 :goto_5

    :cond_6
    move v0, v2

    goto/16 :goto_6

    :cond_7
    move v0, v2

    goto/16 :goto_7

    :cond_8
    move v0, v2

    goto :goto_8

    :cond_9
    move v0, v2

    goto :goto_9

    :cond_a
    move v1, v2

    goto :goto_a
.end method

.method public synthetic x()Ljava/util/List;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->w()Ljava/util/ArrayList;

    move-result-object v0

    return-object v0
.end method

.method public y()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    return-object v0
.end method

.method public z()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    goto :goto_0
.end method
