.class public final Lcom/twitter/library/provider/ad;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/provider/BaseColumns;


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lcom/twitter/library/provider/w;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "dm_request_inbox"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/library/provider/ad;->a:Landroid/net/Uri;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "request_id"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "user_id"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "name"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "username"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "bio"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "profile_image_url"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/provider/ad;->b:[Ljava/lang/String;

    return-void
.end method
