.class public interface abstract Lcom/twitter/library/provider/ParcelableTweet;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# virtual methods
.method public abstract a()Ljava/lang/String;
.end method

.method public abstract b()J
.end method

.method public abstract c()J
.end method

.method public abstract d()J
.end method

.method public abstract e()Z
.end method

.method public abstract f()J
.end method

.method public abstract g()J
.end method

.method public abstract h()Ljava/lang/String;
.end method

.method public abstract i()Ljava/lang/String;
.end method

.method public abstract j()Ljava/lang/String;
.end method

.method public abstract k()Ljava/lang/String;
.end method

.method public abstract l()Lcom/twitter/library/api/TweetClassicCard;
.end method

.method public abstract m()Lcom/twitter/library/api/PromotedContent;
.end method

.method public abstract n()Lcom/twitter/library/api/TwitterStatusCard;
.end method

.method public abstract o()Ljava/util/ArrayList;
.end method

.method public abstract p()Ljava/util/List;
.end method

.method public abstract q()Z
.end method

.method public abstract r()Z
.end method

.method public abstract s()Z
.end method

.method public abstract t()Z
.end method

.method public abstract u()Z
.end method

.method public abstract v()Z
.end method

.method public abstract x()Ljava/util/List;
.end method
