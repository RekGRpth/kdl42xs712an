.class Lcom/twitter/library/provider/n;
.super Landroid/database/ContentObserver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/library/provider/m;


# direct methods
.method public constructor <init>(Lcom/twitter/library/provider/m;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/m;

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/m;

    iput-boolean p1, v0, Lcom/twitter/library/provider/m;->h:Z

    iget-object v0, p0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/m;

    iget-boolean v0, v0, Lcom/twitter/library/provider/m;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/m;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/twitter/library/provider/m;->g:Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/provider/n;->a:Lcom/twitter/library/provider/m;

    invoke-static {v0}, Lcom/twitter/library/provider/m;->a(Lcom/twitter/library/provider/m;)Landroid/database/ContentObservable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/database/ContentObservable;->dispatchChange(Z)V

    goto :goto_0
.end method
