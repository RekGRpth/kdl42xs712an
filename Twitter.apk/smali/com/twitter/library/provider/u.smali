.class public Lcom/twitter/library/provider/u;
.super Lcom/twitter/library/provider/m;
.source "Twttr"


# instance fields
.field private final i:Ljava/util/BitSet;

.field private final j:Ljava/util/BitSet;

.field private final k:Ljava/util/BitSet;

.field private final l:Ljava/util/BitSet;

.field private final m:Landroid/util/SparseIntArray;

.field private final n:Landroid/util/SparseIntArray;


# direct methods
.method public constructor <init>(Landroid/database/Cursor;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/library/provider/m;-><init>(Landroid/database/Cursor;)V

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/u;->i:Ljava/util/BitSet;

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/u;->j:Ljava/util/BitSet;

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/u;->k:Ljava/util/BitSet;

    new-instance v0, Ljava/util/BitSet;

    invoke-direct {v0}, Ljava/util/BitSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/u;->l:Ljava/util/BitSet;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/u;->m:Landroid/util/SparseIntArray;

    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/provider/u;->n:Landroid/util/SparseIntArray;

    return-void
.end method

.method private static a(II)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-static {p1}, Lcom/twitter/library/provider/au;->l(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;I)Z
    .locals 1

    if-eqz p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;ILjava/lang/String;I)Z
    .locals 1

    invoke-static {p1}, Lcom/twitter/library/api/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Lcom/twitter/library/api/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    if-ne p1, p3, :cond_1

    invoke-virtual {p0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 25

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/u;->i:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/u;->j:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/u;->k:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/u;->l:Ljava/util/BitSet;

    invoke-virtual {v2}, Ljava/util/BitSet;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/u;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->clear()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/provider/u;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v2}, Landroid/util/SparseIntArray;->clear()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->e:Landroid/database/Cursor;

    move-object/from16 v16, v0

    if-nez v16, :cond_0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    :goto_0
    return-void

    :cond_0
    new-instance v17, Ljava/util/ArrayList;

    invoke-direct/range {v17 .. v17}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->i:Ljava/util/BitSet;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->j:Ljava/util/BitSet;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->k:Ljava/util/BitSet;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->l:Ljava/util/BitSet;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->m:Landroid/util/SparseIntArray;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/library/provider/u;->n:Landroid/util/SparseIntArray;

    move-object/from16 v23, v0

    const/4 v10, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x0

    const/4 v4, -0x1

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    move-object v11, v9

    move v13, v10

    move v9, v8

    :goto_1
    sget v8, Lcom/twitter/library/provider/cc;->c:I

    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    sget v8, Lcom/twitter/library/provider/cc;->d:I

    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    sget v8, Lcom/twitter/library/provider/cc;->e:I

    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    sget v8, Lcom/twitter/library/provider/cc;->g:I

    move-object/from16 v0, v16

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v15, 0x1

    invoke-static {}, Lkm;->d()Z

    move-result v24

    if-nez v24, :cond_1

    const/16 v24, 0x2

    move/from16 v0, v24

    if-ne v10, v0, :cond_1

    invoke-static {v8}, Lcom/twitter/library/provider/au;->f(I)Z

    move-result v24

    if-eqz v24, :cond_1

    const/4 v15, 0x0

    :cond_1
    invoke-static {v12, v14, v11, v13}, Lcom/twitter/library/provider/u;->a(Ljava/lang/String;ILjava/lang/String;I)Z

    move-result v24

    if-nez v24, :cond_3

    if-ltz v2, :cond_2

    if-eqz v13, :cond_2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    :cond_2
    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    :cond_3
    invoke-virtual {v12, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_d

    const/4 v13, 0x1

    :goto_2
    if-eqz v13, :cond_5

    if-ltz v2, :cond_4

    invoke-static {v11, v7}, Lcom/twitter/library/provider/u;->a(Ljava/lang/String;I)Z

    move-result v11

    if-eqz v11, :cond_4

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    :cond_4
    invoke-static {v12, v8}, Lcom/twitter/library/provider/u;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/BitSet;->set(I)V

    :cond_5
    invoke-static {v8}, Lcom/twitter/library/provider/au;->m(I)Z

    move-result v2

    if-nez v2, :cond_6

    if-ne v9, v10, :cond_7

    :cond_6
    if-eqz v13, :cond_9

    :cond_7
    invoke-static {v9, v7}, Lcom/twitter/library/provider/u;->a(II)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v4}, Landroid/util/SparseIntArray;->put(II)V

    :cond_8
    invoke-static {v10, v8}, Lcom/twitter/library/provider/u;->a(II)Z

    move-result v2

    if-eqz v2, :cond_9

    move-object/from16 v0, v22

    invoke-virtual {v0, v3, v5}, Landroid/util/SparseIntArray;->put(II)V

    move v6, v3

    :cond_9
    invoke-static {v10, v8}, Lcom/twitter/library/provider/u;->a(II)Z

    move-result v2

    if-eqz v2, :cond_f

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    if-eqz v2, :cond_e

    const/4 v2, 0x1

    :goto_3
    if-eqz v2, :cond_a

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v17

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_a
    add-int/lit8 v5, v5, 0x1

    add-int/lit8 v4, v5, -0x1

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v2, v3, -0x1

    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-nez v7, :cond_10

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    invoke-static {v12, v8}, Lcom/twitter/library/provider/u;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_b

    move-object/from16 v0, v21

    invoke-virtual {v0, v2}, Ljava/util/BitSet;->set(I)V

    :cond_b
    invoke-static {v10, v8}, Lcom/twitter/library/provider/u;->a(II)Z

    move-result v2

    if-eqz v2, :cond_c

    move-object/from16 v0, v23

    invoke-virtual {v0, v6, v4}, Landroid/util/SparseIntArray;->put(II)V

    :cond_c
    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    goto/16 :goto_0

    :cond_d
    const/4 v13, 0x0

    goto/16 :goto_2

    :cond_e
    const/4 v2, 0x0

    goto :goto_3

    :cond_f
    move v2, v15

    goto :goto_3

    :cond_10
    move v7, v8

    move v9, v10

    move-object v11, v12

    move v13, v14

    goto/16 :goto_1
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/provider/u;->e:Landroid/database/Cursor;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/library/provider/u;->getPosition()I

    move-result v1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "entity_group_start"

    iget-object v3, p0, Lcom/twitter/library/provider/u;->i:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "entity_group_end"

    iget-object v3, p0, Lcom/twitter/library/provider/u;->j:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "entity_start"

    iget-object v3, p0, Lcom/twitter/library/provider/u;->k:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "entity_end"

    iget-object v3, p0, Lcom/twitter/library/provider/u;->l:Ljava/util/BitSet;

    invoke-virtual {v3, v1}, Ljava/util/BitSet;->get(I)Z

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v2, p0, Lcom/twitter/library/provider/u;->m:Landroid/util/SparseIntArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/provider/u;->n:Landroid/util/SparseIntArray;

    invoke-virtual {v3, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    const-string/jumbo v3, "data_type_source_start"

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "data_type_source_end"

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/library/provider/m;->getInt(I)I

    move-result v0

    sget v1, Lcom/twitter/library/provider/cc;->k:I

    if-ne p1, v1, :cond_0

    const/16 v1, 0x18

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/u;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    const-string/jumbo v2, "entity_start"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, -0x1

    :cond_0
    return v0
.end method

.method public isFirst()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Lcom/twitter/library/provider/m;->isFirst()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/u;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public isLast()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    invoke-super {p0}, Lcom/twitter/library/provider/m;->isFirst()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/provider/u;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    iget-object v2, p0, Lcom/twitter/library/provider/u;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public moveToPosition(I)Z
    .locals 2

    iget-object v1, p0, Lcom/twitter/library/provider/u;->e:Landroid/database/Cursor;

    if-eqz v1, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/library/provider/m;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/provider/u;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
