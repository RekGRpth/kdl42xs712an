.class public Lcom/twitter/library/provider/ActivityDataList;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Serializable;


# static fields
.field private static final serialVersionUID:J = 0x7f292e7c35a857d6L


# instance fields
.field public final creatorUserId:J

.field public final fullName:Ljava/lang/String;

.field public final id:J

.field public final name:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/am;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/library/api/am;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/provider/ActivityDataList;->id:J

    iget-object v0, p1, Lcom/twitter/library/api/am;->a:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/library/api/am;->b:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/provider/ActivityDataList;->fullName:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/library/api/am;->g:Lcom/twitter/library/api/TwitterUser;

    if-nez v0, :cond_0

    const-wide/16 v0, 0x0

    :goto_0
    iput-wide v0, p0, Lcom/twitter/library/provider/ActivityDataList;->creatorUserId:J

    return-void

    :cond_0
    iget-wide v0, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    goto :goto_0
.end method

.method public static a([B)Ljava/util/ArrayList;
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method

.method public static a(Ljava/util/ArrayList;)[B
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/am;

    new-instance v3, Lcom/twitter/library/provider/ActivityDataList;

    invoke-direct {v3, v0}, Lcom/twitter/library/provider/ActivityDataList;-><init>(Lcom/twitter/library/api/am;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-static {v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method
