.class public Lcom/twitter/library/network/y;
.super Lcom/twitter/internal/network/o;
.source "Twttr"


# static fields
.field private static final c:Ljava/util/Set;

.field private static final d:Ljava/util/Set;


# instance fields
.field private final e:Ljava/security/KeyStore;

.field private f:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/twitter/library/network/y;->c:Ljava/util/Set;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    sput-object v0, Lcom/twitter/library/network/y;->d:Ljava/util/Set;

    sget-object v0, Lcom/twitter/library/network/y;->c:Ljava/util/Set;

    const-string/jumbo v1, "api.twitter.com"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/library/network/y;->d:Ljava/util/Set;

    const-string/jumbo v1, "/1.1/help/settings.json"

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Ljava/security/KeyStore;Lcom/twitter/internal/network/j;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p2}, Lcom/twitter/internal/network/o;-><init>(Lcom/twitter/internal/network/j;)V

    iput-object v1, p0, Lcom/twitter/library/network/y;->f:Ljavax/net/ssl/SSLSocketFactory;

    iput-object p1, p0, Lcom/twitter/library/network/y;->e:Ljava/security/KeyStore;

    iget-object v0, p0, Lcom/twitter/library/network/y;->b:Lcom/squareup/okhttp/j;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/j;->a(Ljava/net/CookieHandler;)Lcom/squareup/okhttp/j;

    iget-object v0, p0, Lcom/twitter/library/network/y;->b:Lcom/squareup/okhttp/j;

    invoke-virtual {p0}, Lcom/twitter/library/network/y;->c()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/j;->a(Ljavax/net/ssl/SSLSocketFactory;)Lcom/squareup/okhttp/j;

    return-void
.end method

.method private a(Ljava/net/URI;)Z
    .locals 2

    sget-object v0, Lcom/twitter/library/network/y;->c:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/library/network/y;->d:Ljava/util/Set;

    invoke-virtual {p1}, Ljava/net/URI;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/internal/network/o;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-direct {p0, p2}, Lcom/twitter/library/network/y;->a(Ljava/net/URI;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "X-Android-Transports"

    const-string/jumbo v2, "spdy/3.1,http/1.1"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v1, "X-Android-Transports"

    const-string/jumbo v2, "http/1.1"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    goto :goto_0
.end method

.method protected declared-synchronized c()Ljavax/net/ssl/SSLSocketFactory;
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/y;->f:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    new-instance v1, Lhu;

    iget-object v0, p0, Lcom/twitter/library/network/y;->e:Ljava/security/KeyStore;

    invoke-direct {v1, v0}, Lhu;-><init>(Ljava/security/KeyStore;)V

    new-instance v0, Lhr;

    sget-object v2, Lku;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/client/App;->i()J

    move-result-wide v3

    sget-object v5, Lku;->c:[Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lhr;-><init>(Lhu;[Ljava/lang/String;J[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/network/y;->f:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/network/y;->f:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
