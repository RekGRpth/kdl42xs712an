.class public Lcom/twitter/library/network/b;
.super Lcom/twitter/library/network/a;
.source "Twttr"


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/network/a;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/network/b;->a:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/network/b;->b:Ljava/lang/String;

    return-void
.end method

.method public static declared-synchronized a(Landroid/content/Context;Lcom/twitter/internal/android/service/AsyncService;Lcom/twitter/library/service/p;)Lcom/twitter/library/network/a;
    .locals 8

    const/4 v0, 0x0

    const-class v5, Lcom/twitter/library/network/b;

    monitor-enter v5

    :try_start_0
    iget-boolean v1, p2, Lcom/twitter/library/service/p;->b:Z

    if-eqz v1, :cond_1

    new-instance v0, Lcom/twitter/library/network/n;

    iget-object v1, p2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v0, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    monitor-exit v5

    return-object v0

    :cond_1
    const/4 v1, 0x0

    move-object v2, v0

    move v3, v1

    move-object v1, v0

    :goto_1
    if-nez v1, :cond_3

    add-int/lit8 v4, v3, 0x1

    const/4 v6, 0x2

    if-ge v3, v6, :cond_3

    :try_start_1
    invoke-static {p0}, Lcom/twitter/library/network/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-static {p0, p1}, Lcom/twitter/library/network/b;->a(Landroid/content/Context;Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    invoke-static {p0}, Lcom/twitter/library/network/b;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    invoke-static {p0, p1, v1}, Lcom/twitter/library/network/b;->a(Landroid/content/Context;Lcom/twitter/internal/android/service/AsyncService;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move v3, v4

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_1

    :cond_3
    if-eqz v1, :cond_0

    new-instance v0, Lcom/twitter/library/network/b;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/network/b;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    :cond_4
    move v3, v4

    move-object v7, v1

    move-object v1, v2

    move-object v2, v7

    goto :goto_1
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/String;
    .locals 2

    new-instance v1, Lcom/twitter/library/api/account/g;

    invoke-direct {v1, p0}, Lcom/twitter/library/api/account/g;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/account/g;->c(Z)Lcom/twitter/library/service/b;

    invoke-virtual {v1, p1}, Lcom/twitter/library/api/account/g;->c(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/api/account/g;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/network/b;->a(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/twitter/internal/android/service/AsyncService;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v1, Lcom/twitter/library/api/account/h;

    invoke-direct {v1, p0, p2}, Lcom/twitter/library/api/account/h;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/account/h;->c(Z)Lcom/twitter/library/service/b;

    invoke-virtual {v1, p1}, Lcom/twitter/library/api/account/h;->c(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v2

    iget v2, v2, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/api/account/h;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/network/b;->b(Landroid/content/Context;Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    const/16 v0, 0x191

    if-eq v2, v0, :cond_1

    const/16 v0, 0x193

    if-ne v2, v0, :cond_2

    :cond_1
    invoke-static {p0}, Lcom/twitter/library/network/b;->b(Landroid/content/Context;)V

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 0

    invoke-static {p0}, Lcom/twitter/library/network/b;->c(Landroid/content/Context;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "guest_auth"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "access_token"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static b(Landroid/content/Context;)V
    .locals 2

    const-string/jumbo v0, "guest_auth"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "access_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "guest_auth"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "guest_token"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static c(Landroid/content/Context;)V
    .locals 2

    const-string/jumbo v0, "guest_auth"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "guest_token"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method private static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, "guest_auth"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "access_token"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Landroid/content/Context;)Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, "guest_auth"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "guest_token"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public synthetic a()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/library/network/b;->b()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/network/b;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/b;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "Authorization"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Bearer "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/network/b;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    const-string/jumbo v0, "X-Guest-Token"

    iget-object v1, p0, Lcom/twitter/library/network/b;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    :cond_0
    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/network/b;->b:Ljava/lang/String;

    return-object v0
.end method
