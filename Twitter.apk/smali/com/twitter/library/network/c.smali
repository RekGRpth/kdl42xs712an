.class public Lcom/twitter/library/network/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/network/f;


# static fields
.field private static a:Lcom/twitter/library/metrics/b;

.field private static b:Lcom/twitter/library/metrics/b;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:J

.field private e:Z

.field private f:Lcom/twitter/library/scribe/ScribeLog;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/library/network/c;-><init>(Landroid/content/Context;J)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/c;->c:Landroid/content/Context;

    iput-wide p2, p0, Lcom/twitter/library/network/c;->d:J

    return-void
.end method

.method public static a(Lcom/twitter/library/metrics/b;Z)V
    .locals 0

    if-eqz p1, :cond_0

    sput-object p0, Lcom/twitter/library/network/c;->b:Lcom/twitter/library/metrics/b;

    :goto_0
    return-void

    :cond_0
    sput-object p0, Lcom/twitter/library/network/c;->a:Lcom/twitter/library/metrics/b;

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/network/HttpOperation;)V
    .locals 4

    invoke-static {}, Lcom/twitter/library/client/l;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/network/c;->e:Z

    iget-wide v0, p0, Lcom/twitter/library/network/c;->d:J

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/c;->c:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeService;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v1, p0, Lcom/twitter/library/network/c;->d:J

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "api::::request"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/c;->f:Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/library/network/c;->f:Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/library/network/c;->c:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/internal/network/HttpOperation;)V
    .locals 6

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/library/network/c;->f:Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/c;->f:Lcom/twitter/library/scribe/ScribeLog;

    iget-boolean v2, p0, Lcom/twitter/library/network/c;->e:Z

    invoke-virtual {v0, p1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/internal/network/HttpOperation;Z)Lcom/twitter/library/scribe/ScribeLog;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/network/c;->f:Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-wide v2, p0, Lcom/twitter/library/network/c;->d:J

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-eqz v2, :cond_2

    iget v2, v1, Lcom/twitter/internal/network/k;->a:I

    const/16 v3, 0xc8

    if-eq v2, v3, :cond_2

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v3, p0, Lcom/twitter/library/network/c;->d:J

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "api::::error"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->h()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget v1, v1, Lcom/twitter/internal/network/k;->a:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/twitter/library/network/c;->c:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/util/ArrayList;)V

    :cond_3
    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/internal/network/k;->h:J

    iget-boolean v2, p0, Lcom/twitter/library/network/c;->e:Z

    if-eqz v2, :cond_4

    sget-object v2, Lcom/twitter/library/network/c;->b:Lcom/twitter/library/metrics/b;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/metrics/b;->a(J)V

    :goto_0
    return-void

    :cond_4
    sget-object v2, Lcom/twitter/library/network/c;->a:Lcom/twitter/library/metrics/b;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/metrics/b;->a(J)V

    goto :goto_0
.end method
