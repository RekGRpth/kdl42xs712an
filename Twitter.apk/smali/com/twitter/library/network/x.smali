.class public Lcom/twitter/library/network/x;
.super Lcom/twitter/internal/network/l;
.source "Twttr"


# instance fields
.field private final b:Ljava/security/KeyStore;

.field private c:Ljavax/net/ssl/SSLSocketFactory;


# direct methods
.method public constructor <init>(Ljava/security/KeyStore;Lcom/twitter/internal/network/j;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Lcom/twitter/internal/network/l;-><init>(Lcom/twitter/internal/network/j;)V

    iput-object v0, p0, Lcom/twitter/library/network/x;->c:Ljavax/net/ssl/SSLSocketFactory;

    iput-object p1, p0, Lcom/twitter/library/network/x;->b:Ljava/security/KeyStore;

    invoke-static {v0}, Ljava/net/CookieHandler;->setDefault(Ljava/net/CookieHandler;)V

    return-void
.end method

.method private declared-synchronized c()Ljavax/net/ssl/SSLSocketFactory;
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/network/x;->c:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    :try_start_1
    new-instance v1, Lhu;

    iget-object v0, p0, Lcom/twitter/library/network/x;->b:Ljava/security/KeyStore;

    invoke-direct {v1, v0}, Lhu;-><init>(Ljava/security/KeyStore;)V

    new-instance v0, Lhr;

    sget-object v2, Lku;->b:[Ljava/lang/String;

    invoke-static {}, Lcom/twitter/library/client/App;->i()J

    move-result-wide v3

    sget-object v5, Lku;->c:[Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lhr;-><init>(Lhu;[Ljava/lang/String;J[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/network/x;->c:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_1
    .catch Ljava/security/KeyManagementException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/UnrecoverableKeyException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/KeyStoreException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/network/x;->c:Ljavax/net/ssl/SSLSocketFactory;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v0

    :try_start_3
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public a(Ljava/net/URL;)Ljava/net/HttpURLConnection;
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/internal/network/l;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v1

    const-string/jumbo v0, "https"

    invoke-virtual {p1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    invoke-direct {p0}, Lcom/twitter/library/network/x;->c()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    :cond_0
    return-object v1
.end method
