.class public Lcom/twitter/library/network/LoginResponse;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lcom/twitter/library/network/OAuthToken;

.field public final b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

.field public final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/network/f;

    invoke-direct {v0}, Lcom/twitter/library/network/f;-><init>()V

    sput-object v0, Lcom/twitter/library/network/LoginResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IJLjava/lang/String;ILjava/lang/String;I)V
    .locals 7

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/twitter/library/network/LoginResponse;->c:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    new-instance v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    move-wide v1, p2

    move-object v3, p4

    move v4, p5

    move-object v5, p6

    move v6, p7

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/network/LoginVerificationRequiredResponse;-><init>(JLjava/lang/String;ILjava/lang/String;I)V

    iput-object v0, p0, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/twitter/library/network/LoginResponse;->c:I

    new-instance v0, Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v0, p2, p3}, Lcom/twitter/library/network/OAuthToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/network/LoginResponse;->c:I

    const-class v0, Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/OAuthToken;

    iput-object v0, p0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    const-class v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iput-object v0, p0, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/network/LoginResponse;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/network/LoginResponse;->b:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    return-void
.end method
