.class public Lcom/twitter/library/network/m;
.super Ljava/io/InputStream;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private b:Ljava/io/InputStream;

.field private c:I


# direct methods
.method public constructor <init>(Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/network/m;->a:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/network/m;->c:I

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/network/m;->b:Ljava/io/InputStream;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/m;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/network/m;->b:Ljava/io/InputStream;

    :cond_0
    return-void
.end method

.method public read()I
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x1

    new-array v0, v2, [B

    invoke-virtual {p0, v0, v3, v2}, Lcom/twitter/library/network/m;->read([BII)I

    move-result v1

    if-ne v1, v2, :cond_0

    aget-byte v0, v0, v3

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public read([BII)I
    .locals 7

    const/4 v2, 0x0

    const/4 v3, 0x0

    iget v1, p0, Lcom/twitter/library/network/m;->c:I

    iget-object v0, p0, Lcom/twitter/library/network/m;->b:Ljava/io/InputStream;

    move v6, v1

    move v1, v3

    move v3, v6

    :goto_0
    if-ge v1, p3, :cond_2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/network/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/j;

    invoke-interface {v0}, Lcom/twitter/library/network/j;->a()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/network/m;->b:Ljava/io/InputStream;

    :cond_0
    :goto_1
    add-int v4, p2, v1

    sub-int v5, p3, v1

    invoke-virtual {v0, p1, v4, v5}, Ljava/io/InputStream;->read([BII)I

    move-result v4

    const/4 v5, -0x1

    if-eq v4, v5, :cond_1

    add-int/2addr v1, v4

    goto :goto_1

    :cond_1
    if-eq v1, p3, :cond_2

    iget-object v4, p0, Lcom/twitter/library/network/m;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    if-ne v3, v4, :cond_3

    :cond_2
    return v1

    :cond_3
    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/twitter/library/network/m;->c:I

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    iput-object v2, p0, Lcom/twitter/library/network/m;->b:Ljava/io/InputStream;

    move-object v0, v2

    goto :goto_0
.end method
