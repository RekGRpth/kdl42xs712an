.class public Lcom/twitter/library/api/x;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:I

.field public final c:I

.field public final d:J

.field public final e:Lcom/twitter/library/api/TwitterStatus;

.field public final f:Lcom/twitter/library/api/Conversation;

.field public final g:Ljava/util/List;

.field public final h:Lcom/twitter/library/api/TwitterTopic;

.field public final i:Lcom/twitter/library/api/TimelineScribeContent;

.field public final j:Ljava/lang/String;

.field public k:J

.field private l:Lcom/twitter/library/api/TweetPivotOptions;

.field private m:Ljava/util/LinkedHashSet;

.field private n:Ljava/util/HashSet;

.field private o:Ljava/util/Collection;

.field private final p:Ljava/util/List;


# direct methods
.method private constructor <init>(Lcom/twitter/library/api/z;)V
    .locals 12

    const/4 v3, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/twitter/library/api/z;->a(Lcom/twitter/library/api/z;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->a:Ljava/lang/String;

    invoke-static {p1}, Lcom/twitter/library/api/z;->b(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    invoke-static {p1}, Lcom/twitter/library/api/z;->c(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/Conversation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    invoke-static {p1}, Lcom/twitter/library/api/z;->d(Lcom/twitter/library/api/z;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-static {p1}, Lcom/twitter/library/api/z;->d(Lcom/twitter/library/api/z;)Ljava/util/List;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/api/x;->g:Ljava/util/List;

    invoke-static {p1}, Lcom/twitter/library/api/z;->e(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TweetPivotOptions;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/library/api/z;->f(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/twitter/library/api/z;->g(Lcom/twitter/library/api/z;)V

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/api/z;->e(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TweetPivotOptions;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->l:Lcom/twitter/library/api/TweetPivotOptions;

    invoke-static {p1}, Lcom/twitter/library/api/z;->f(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterTopic;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    invoke-static {p1}, Lcom/twitter/library/api/z;->h(Lcom/twitter/library/api/z;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/x;->b:I

    invoke-static {p1}, Lcom/twitter/library/api/z;->i(Lcom/twitter/library/api/z;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/x;->c:I

    invoke-static {p1}, Lcom/twitter/library/api/z;->j(Lcom/twitter/library/api/z;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/api/x;->d:J

    invoke-static {p1}, Lcom/twitter/library/api/z;->k(Lcom/twitter/library/api/z;)Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/twitter/library/api/z;->k(Lcom/twitter/library/api/z;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/twitter/library/api/x;->p:Ljava/util/List;

    invoke-static {p1}, Lcom/twitter/library/api/z;->l(Lcom/twitter/library/api/z;)J

    move-result-wide v4

    iput-wide v4, p0, Lcom/twitter/library/api/x;->k:J

    invoke-static {p1}, Lcom/twitter/library/api/z;->m(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TimelineScribeContent;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->i:Lcom/twitter/library/api/TimelineScribeContent;

    invoke-static {p1}, Lcom/twitter/library/api/z;->n(Lcom/twitter/library/api/z;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/x;->j:Ljava/lang/String;

    invoke-static {p1}, Lcom/twitter/library/api/z;->o(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    if-nez v0, :cond_4

    iget-object v11, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    new-instance v0, Lcom/twitter/library/api/au;

    invoke-static {p1}, Lcom/twitter/library/api/z;->o(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v4

    move-object v2, v1

    move v5, v3

    move-object v6, v1

    move-object v7, v1

    move-object v8, v1

    move-object v9, v1

    move-object v10, v1

    invoke-direct/range {v0 .. v10}, Lcom/twitter/library/api/au;-><init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/twitter/library/api/TwitterSocialProof;ZLcom/twitter/library/api/TwitterSearchHighlight;Lcom/twitter/library/api/af;Lcom/twitter/library/api/DiscoverStoryMetadata;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v11, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    :cond_1
    :goto_2
    return-void

    :cond_2
    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_0

    :cond_3
    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    invoke-static {p1}, Lcom/twitter/library/api/z;->o(Lcom/twitter/library/api/z;)Lcom/twitter/library/api/TwitterSocialProof;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/api/au;->f:Lcom/twitter/library/api/TwitterSocialProof;

    goto :goto_2
.end method

.method synthetic constructor <init>(Lcom/twitter/library/api/z;Lcom/twitter/library/api/y;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/library/api/x;-><init>(Lcom/twitter/library/api/z;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;IIJLcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/Conversation;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic;Ljava/util/List;JLcom/twitter/library/api/TimelineScribeContent;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/x;->a:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iput-object p7, p0, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz p8, :cond_0

    :goto_0
    iput-object p8, p0, Lcom/twitter/library/api/x;->g:Ljava/util/List;

    iput-object p9, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    iput p2, p0, Lcom/twitter/library/api/x;->b:I

    iput p3, p0, Lcom/twitter/library/api/x;->c:I

    iput-wide p4, p0, Lcom/twitter/library/api/x;->d:J

    if-eqz p10, :cond_1

    invoke-static {p10}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/twitter/library/api/x;->p:Ljava/util/List;

    iput-wide p11, p0, Lcom/twitter/library/api/x;->k:J

    iput-object p13, p0, Lcom/twitter/library/api/x;->i:Lcom/twitter/library/api/TimelineScribeContent;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/api/x;->j:Ljava/lang/String;

    return-void

    :cond_0
    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object p8

    goto :goto_0

    :cond_1
    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    goto :goto_1
.end method

.method public constructor <init>(Ljava/lang/String;IILcom/twitter/library/api/TwitterStatus;JLcom/twitter/library/api/TimelineScribeContent;)V
    .locals 14

    const-wide/16 v4, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move-object/from16 v6, p4

    move-wide/from16 v11, p5

    move-object/from16 v13, p7

    invoke-direct/range {v0 .. v13}, Lcom/twitter/library/api/x;-><init>(Ljava/lang/String;IIJLcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/Conversation;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic;Ljava/util/List;JLcom/twitter/library/api/TimelineScribeContent;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/library/api/Conversation;J)V
    .locals 14

    const/4 v2, 0x2

    const/4 v3, 0x0

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v13, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v7, p2

    move-wide/from16 v11, p3

    invoke-direct/range {v0 .. v13}, Lcom/twitter/library/api/x;-><init>(Ljava/lang/String;IIJLcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/Conversation;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic;Ljava/util/List;JLcom/twitter/library/api/TimelineScribeContent;)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/twitter/library/api/TwitterTopic;JLcom/twitter/library/api/TimelineScribeContent;)V
    .locals 14

    const/4 v2, 0x5

    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v9, p2

    move-wide/from16 v11, p3

    move-object/from16 v13, p5

    invoke-direct/range {v0 .. v13}, Lcom/twitter/library/api/x;-><init>(Ljava/lang/String;IIJLcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/Conversation;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic;Ljava/util/List;JLcom/twitter/library/api/TimelineScribeContent;)V

    return-void
.end method

.method public static a(Lcom/twitter/library/api/TwitterStatus;)Lcom/twitter/library/api/x;
    .locals 8

    new-instance v0, Lcom/twitter/library/api/x;

    invoke-virtual {p0}, Lcom/twitter/library/api/TwitterStatus;->a()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-wide v5, p0, Lcom/twitter/library/api/TwitterStatus;->r:J

    const/4 v7, 0x0

    move-object v4, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/x;-><init>(Ljava/lang/String;IILcom/twitter/library/api/TwitterStatus;JLcom/twitter/library/api/TimelineScribeContent;)V

    return-object v0
.end method

.method public static a(I)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x2

    if-eq p0, v1, :cond_0

    if-eq p0, v0, :cond_0

    invoke-static {}, Lkm;->d()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x3

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterStatus;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    int-to-long v0, v0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/api/x;->a()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/library/api/x;->c:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/library/api/x;->b:I

    if-ne v1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/x;->l:Lcom/twitter/library/api/TweetPivotOptions;

    if-eqz v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Lcom/twitter/library/api/TweetPivotOptions;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/x;->l:Lcom/twitter/library/api/TweetPivotOptions;

    return-object v0
.end method

.method public f()Lcom/twitter/library/api/PromotedContent;
    .locals 5

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    iget-object v1, v0, Lcom/twitter/library/api/Conversation;->b:[Lcom/twitter/library/api/TwitterStatus;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    iget-object v4, v3, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    if-eqz v4, :cond_1

    iget-object v0, v3, Lcom/twitter/library/api/TwitterStatus;->p:Lcom/twitter/library/api/PromotedContent;

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/twitter/library/api/RecommendedContent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatus;->q:Lcom/twitter/library/api/RecommendedContent;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterStatus;->g()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized i()Ljava/util/Collection;
    .locals 7

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->m:Ljava/util/LinkedHashSet;

    if-nez v0, :cond_1

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/x;->e:Lcom/twitter/library/api/TwitterStatus;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    iput-object v1, p0, Lcom/twitter/library/api/x;->m:Ljava/util/LinkedHashSet;

    iput-object v2, p0, Lcom/twitter/library/api/x;->n:Ljava/util/HashSet;

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/x;->m:Ljava/util/LinkedHashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/x;->f:Lcom/twitter/library/api/Conversation;

    iget-object v3, v0, Lcom/twitter/library/api/Conversation;->b:[Lcom/twitter/library/api/TwitterStatus;

    array-length v4, v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_0

    aget-object v5, v3, v0

    invoke-virtual {v1, v5}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v5}, Lcom/twitter/library/api/TwitterStatus;->a()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterTopic;->o:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterTopic;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    :try_start_2
    iget-object v0, p0, Lcom/twitter/library/api/x;->g:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_5
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic;

    iget-object v4, v0, Lcom/twitter/library/api/TwitterTopic;->o:Ljava/util/ArrayList;

    if-eqz v4, :cond_5

    iget-object v0, v0, Lcom/twitter/library/api/TwitterTopic;->o:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->addAll(Ljava/util/Collection;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized j()Ljava/util/Collection;
    .locals 1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->n:Ljava/util/HashSet;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/x;->i()Ljava/util/Collection;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->n:Ljava/util/HashSet;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized k()Ljava/util/Collection;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->o:Ljava/util/Collection;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    if-eqz v0, :cond_1

    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p0, Lcom/twitter/library/api/x;->h:Lcom/twitter/library/api/TwitterTopic;

    invoke-interface {v0, v1}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/api/x;->o:Ljava/util/Collection;

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/x;->o:Ljava/util/Collection;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/twitter/library/api/x;->g:Ljava/util/List;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/x;->g:Ljava/util/List;

    goto :goto_0

    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public l()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/x;->p:Ljava/util/List;

    return-object v0
.end method
