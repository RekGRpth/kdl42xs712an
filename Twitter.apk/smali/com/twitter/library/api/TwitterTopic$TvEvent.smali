.class public Lcom/twitter/library/api/TwitterTopic$TvEvent;
.super Lcom/twitter/library/api/TwitterTopic$Data;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0x5f92fb542ed0c33eL


# instance fields
.field public channel:Ljava/lang/String;

.field public episode:I

.field public episodeDescription:Ljava/lang/String;

.field public episodeTitle:Ljava/lang/String;

.field public isNew:Z

.field public season:I

.field public seriesDescription:Ljava/lang/String;

.field public seriesTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesTitle:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesDescription:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeDescription:Ljava/lang/String;

    iput p4, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->season:I

    iput p5, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episode:I

    iput-object p6, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeTitle:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->channel:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->isNew:Z

    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesTitle:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesDescription:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeDescription:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->season:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episode:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeTitle:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->channel:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->isNew:Z

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesTitle:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesDescription:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeDescription:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->season:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episode:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeTitle:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->channel:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterTopic$TvEvent;->isNew:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    return-void
.end method
