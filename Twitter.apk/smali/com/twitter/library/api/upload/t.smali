.class public Lcom/twitter/library/api/upload/t;
.super Lcom/twitter/library/api/upload/b;
.source "Twttr"


# instance fields
.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    const-string/jumbo v0, "tweet::media:upload"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/upload/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object p2, p0, Lcom/twitter/library/api/upload/t;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/api/upload/t;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/library/service/e;)V
    .locals 10

    const/4 v9, 0x2

    move-object v6, p1

    check-cast v6, Lcom/twitter/library/api/upload/w;

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v7

    invoke-virtual {v6}, Lcom/twitter/library/api/upload/w;->f()Z

    move-result v8

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/ad;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v1, v0, Lcom/twitter/library/service/p;->c:J

    if-eqz v7, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tweet:"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/api/upload/t;->b:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v8, :cond_3

    const-string/jumbo v0, "_has_media"

    :goto_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/library/api/upload/t;->a:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/ad;->o()J

    move-result-wide v4

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;JLjava/lang/String;J)V

    if-eqz v8, :cond_0

    invoke-virtual {v6}, Lcom/twitter/library/api/upload/w;->g()F

    move-result v0

    const/high16 v3, 0x44800000    # 1024.0f

    div-float/2addr v0, v3

    iget-object v3, p0, Lcom/twitter/library/api/upload/t;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/ad;->o()J

    move-result-wide v5

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a(F)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v5, v6, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(JLjava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x3

    new-array v1, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v2, "app:twitter_service:tweet"

    aput-object v2, v1, v0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/library/api/upload/t;->b:Ljava/lang/String;

    aput-object v2, v1, v0

    if-eqz v7, :cond_4

    const-string/jumbo v0, "success"

    :goto_1
    aput-object v0, v1, v9

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/internal/network/k;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b(I)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/library/api/upload/t;->a:Landroid/content/Context;

    invoke-static {v2, v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;)V

    :cond_1
    if-eqz v8, :cond_5

    const-string/jumbo v0, "has_media"

    :goto_2
    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/library/api/upload/t;->b:Ljava/lang/String;

    const-string/jumbo v2, "retry_tweet"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v1, v9}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/upload/t;->a:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_3
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "failure"

    goto :goto_1

    :cond_5
    const-string/jumbo v0, "no_media"

    goto :goto_2
.end method
