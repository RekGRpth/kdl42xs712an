.class public Lcom/twitter/library/api/upload/ac;
.super Lcom/twitter/library/api/upload/n;
.source "Twttr"


# instance fields
.field private final n:Lcom/twitter/library/api/TwitterUser;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/ac;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/n;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput-object p3, p0, Lcom/twitter/library/api/upload/ac;->n:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method


# virtual methods
.method protected c(Lcom/twitter/library/service/e;)V
    .locals 11

    const-wide/16 v2, -0x1

    const/4 v9, 0x1

    const/4 v7, 0x0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/ac;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v10, p0, Lcom/twitter/library/api/upload/ac;->n:Lcom/twitter/library/api/TwitterUser;

    iget-object v1, p0, Lcom/twitter/library/api/upload/ac;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v4, v5

    const-string/jumbo v5, "account"

    aput-object v5, v4, v9

    const/4 v5, 0x2

    const-string/jumbo v6, "remove_profile_banner"

    aput-object v6, v4, v5

    invoke-static {v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ".json"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v4, Lcom/twitter/library/api/upload/ae;

    iget-object v5, p0, Lcom/twitter/library/api/upload/ac;->l:Landroid/content/Context;

    invoke-direct {v4, v5, v0}, Lcom/twitter/library/api/upload/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v4, v1}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/ac;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/ac;->p()V

    iget-object v0, p0, Lcom/twitter/library/api/upload/ac;->f:Lcom/twitter/library/api/upload/ae;

    iget-object v1, p0, Lcom/twitter/library/api/upload/ac;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/ac;->q()V

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v7, v10, Lcom/twitter/library/api/TwitterUser;->profileHeaderImageUrl:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/ac;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    const/4 v4, -0x1

    move-wide v5, v2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/ac;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method
