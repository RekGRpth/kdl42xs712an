.class public Lcom/twitter/library/api/upload/w;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"


# instance fields
.field protected d:Lcom/twitter/library/api/TweetEntities;

.field private n:Lcom/twitter/library/api/ao;

.field private o:Ljava/lang/String;

.field private p:J

.field private q:Landroid/location/Location;

.field private r:Ljava/lang/String;

.field private s:Lcom/twitter/library/api/PromotedContent;

.field private t:Lcom/twitter/library/api/TwitterStatus;

.field private u:J

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Ljava/util/ArrayList;

.field private z:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method

.method private a(IJ)V
    .locals 9

    const-wide/16 v1, 0x0

    iget-wide v3, p0, Lcom/twitter/library/api/upload/w;->u:J

    iget-wide v5, p0, Lcom/twitter/library/api/upload/w;->u:J

    cmp-long v0, v5, v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    invoke-static {v0, p2, p3}, Lcom/twitter/library/provider/c;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/c;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/library/api/upload/w;->o:Ljava/lang/String;

    iget-wide v4, p0, Lcom/twitter/library/api/upload/w;->p:J

    iget-object v6, p0, Lcom/twitter/library/api/upload/w;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v8, p0, Lcom/twitter/library/api/upload/w;->s:Lcom/twitter/library/api/PromotedContent;

    move v7, p1

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/library/provider/c;->a(JLjava/lang/String;JLcom/twitter/library/api/TweetEntities;ILcom/twitter/library/api/PromotedContent;)J

    move-result-wide v3

    iget-boolean v5, p0, Lcom/twitter/library/api/upload/w;->v:Z

    cmp-long v0, v3, v1

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    or-int/2addr v0, v5

    iput-boolean v0, p0, Lcom/twitter/library/api/upload/w;->v:Z

    :cond_0
    move-wide v0, v3

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "status_id"

    invoke-virtual {v2, v3, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/twitter/library/service/e;J)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/w;->b(Lcom/twitter/library/service/e;J)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x3eb

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/library/api/upload/w;->a(IJ)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2, p3}, Lcom/twitter/library/api/upload/w;->a(IJ)V

    goto :goto_0
.end method

.method private b(Lcom/twitter/library/service/e;J)Z
    .locals 5

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/twitter/library/api/upload/w;->w:Z

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->b()Ljava/lang/Exception;

    move-result-object v2

    instance-of v3, v2, Lorg/apache/http/conn/ConnectTimeoutException;

    if-nez v3, :cond_1

    instance-of v2, v2, Ljava/net/SocketTimeoutException;

    if-eqz v2, :cond_2

    :cond_1
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "app:twitter_service:tweet::timeout"

    aput-object v4, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->g()Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/api/upload/w;->u:J

    return-object p0
.end method

.method public a(Landroid/location/Location;)Lcom/twitter/library/api/upload/w;
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/api/upload/w;->q:Landroid/location/Location;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->q:Landroid/location/Location;

    :cond_0
    return-object p0
.end method

.method public a(Lcom/twitter/library/api/PromotedContent;)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->s:Lcom/twitter/library/api/PromotedContent;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->d:Lcom/twitter/library/api/TweetEntities;

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->o:Ljava/lang/String;

    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->y:Ljava/util/ArrayList;

    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/upload/w;->w:Z

    return-object p0
.end method

.method public b(J)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-wide p1, p0, Lcom/twitter/library/api/upload/w;->p:J

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->r:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/api/upload/w;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/upload/w;->x:Z

    return-object p0
.end method

.method protected b(Ljava/util/ArrayList;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/w;->z:Ljava/util/ArrayList;

    return-void
.end method

.method protected final c(Lcom/twitter/library/service/e;)V
    .locals 3

    const/16 v0, 0x25

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/w;->n:Lcom/twitter/library/api/ao;

    new-instance v0, Lcom/twitter/library/api/upload/ae;

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/w;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/upload/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/w;->t()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ae;->a(Z)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->n:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/w;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/w;->d(Lcom/twitter/library/service/e;)V

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/w;->e(Lcom/twitter/library/service/e;)V

    return-void
.end method

.method protected d(Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/w;->p()V

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/w;->n()Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->f:Lcom/twitter/library/api/upload/ae;

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/w;->q()V

    return-void
.end method

.method protected e()Ljava/lang/StringBuilder;
    .locals 5

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "statuses"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "update"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "include_entities"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_media_features"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_cards"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "earned_read"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    return-object v0
.end method

.method protected e(Lcom/twitter/library/service/e;)V
    .locals 21

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/upload/w;->s()Lcom/twitter/library/service/p;

    move-result-object v16

    move-object/from16 v0, v16

    iget-wide v0, v0, Lcom/twitter/library/service/p;->c:J

    move-wide/from16 v17, v0

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v4

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/upload/w;->n:Lcom/twitter/library/api/ao;

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v4

    move-object v15, v4

    check-cast v15, Lcom/twitter/library/api/TwitterStatus;

    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/upload/w;->k:Landroid/os/Bundle;

    const-string/jumbo v5, "status_id"

    invoke-virtual {v15}, Lcom/twitter/library/api/TwitterStatus;->a()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    iget-wide v4, v15, Lcom/twitter/library/api/TwitterStatus;->e:J

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    move-wide/from16 v0, v17

    invoke-static {v6, v0, v1}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;J)Z

    move-result v6

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/library/api/upload/w;->w()Lcom/twitter/library/provider/az;

    move-result-object v19

    const-wide/16 v7, 0x0

    cmp-long v4, v4, v7

    if-lez v4, :cond_0

    if-eqz v6, :cond_0

    iget-object v4, v15, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    if-nez v4, :cond_0

    iget-wide v4, v15, Lcom/twitter/library/api/TwitterStatus;->d:J

    move-object/from16 v0, v19

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/az;->a(J)Lcom/twitter/library/api/TwitterUser;

    move-result-object v20

    if-eqz v20, :cond_0

    new-instance v4, Lcom/twitter/library/api/au;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-direct/range {v4 .. v14}, Lcom/twitter/library/api/au;-><init>(Ljava/lang/String;Ljava/util/ArrayList;ZLcom/twitter/library/api/TwitterSocialProof;ZLcom/twitter/library/api/TwitterSearchHighlight;Lcom/twitter/library/api/af;Lcom/twitter/library/api/DiscoverStoryMetadata;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v4, v15, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    iget-object v13, v15, Lcom/twitter/library/api/TwitterStatus;->v:Lcom/twitter/library/api/au;

    new-instance v4, Lcom/twitter/library/api/TwitterSocialProof;

    const/16 v5, 0x18

    move-object/from16 v0, v20

    iget-object v6, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-direct/range {v4 .. v12}, Lcom/twitter/library/api/TwitterSocialProof;-><init>(ILjava/lang/String;IIIILjava/lang/String;I)V

    iput-object v4, v13, Lcom/twitter/library/api/au;->f:Lcom/twitter/library/api/TwitterSocialProof;

    :cond_0
    move-object/from16 v0, v19

    move-wide/from16 v1, v17

    invoke-virtual {v0, v15, v1, v2}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/TwitterStatus;J)V

    iget-object v4, v15, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    if-eqz v4, :cond_2

    iget-object v4, v15, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    iget-object v4, v4, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v4, :cond_2

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    iget-object v4, v15, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    iget-object v4, v4, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/MentionEntity;

    iget-wide v7, v4, Lcom/twitter/library/api/MentionEntity;->userId:J

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v5}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v4

    new-instance v5, Lcom/twitter/library/api/search/a;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    move-object/from16 v0, v16

    invoke-direct {v5, v6, v0, v4}, Lcom/twitter/library/api/search/a;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;[J)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/twitter/library/api/upload/w;->b(Lcom/twitter/internal/android/service/a;)V

    :cond_2
    :goto_1
    move-object/from16 v0, p0

    iput-object v15, v0, Lcom/twitter/library/api/upload/w;->t:Lcom/twitter/library/api/TwitterStatus;

    :goto_2
    return-void

    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/upload/w;->f:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v4}, Lcom/twitter/library/api/upload/ae;->b()Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/library/api/upload/w;->l:Landroid/content/Context;

    const-string/jumbo v6, "Received null user."

    move-wide/from16 v0, v17

    invoke-static {v5, v0, v1, v4, v6}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/library/api/upload/w;->n:Lcom/twitter/library/api/ao;

    invoke-virtual {v4}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    const-string/jumbo v5, "custom_errors"

    invoke-static {v4}, Lcom/twitter/library/api/upload/w;->c(Ljava/util/ArrayList;)[I

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v5, v4}, Lcom/twitter/library/api/upload/w;->a(Ljava/lang/String;[I)Lcom/twitter/library/service/b;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-wide/from16 v2, v17

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/api/upload/w;->a(Lcom/twitter/library/service/e;J)V

    goto :goto_2
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public g()F
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public h()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/w;->v:Z

    return v0
.end method

.method public i()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->o:Ljava/lang/String;

    return-object v0
.end method

.method public j()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/upload/w;->u:J

    return-wide v0
.end method

.method public k()Lcom/twitter/library/api/TwitterStatus;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->t:Lcom/twitter/library/api/TwitterStatus;

    return-object v0
.end method

.method public l()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/w;->x:Z

    return v0
.end method

.method public m()Lcom/twitter/library/api/TweetEntities;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/w;->d:Lcom/twitter/library/api/TweetEntities;

    return-object v0
.end method

.method protected n()Ljava/lang/StringBuilder;
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/w;->e()Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "status"

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->o:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "send_error_codes"

    invoke-static {v0, v1, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-wide v1, p0, Lcom/twitter/library/api/upload/w;->p:J

    const-wide/16 v3, 0x0

    cmp-long v1, v1, v3

    if-lez v1, :cond_0

    const-string/jumbo v1, "in_reply_to_status_id"

    iget-wide v2, p0, Lcom/twitter/library/api/upload/w;->p:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->s:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->s:Lcom/twitter/library/api/PromotedContent;

    iget-object v1, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "impression_id"

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->s:Lcom/twitter/library/api/PromotedContent;

    iget-object v2, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->s:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "earned"

    invoke-static {v0, v1, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->y:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->y:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "replied_users"

    const-string/jumbo v2, ","

    iget-object v3, p0, Lcom/twitter/library/api/upload/w;->y:Ljava/util/ArrayList;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->q:Landroid/location/Location;

    if-eqz v1, :cond_2

    const-string/jumbo v1, "lat"

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->q:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    const-string/jumbo v1, "long"

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->q:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->r:Ljava/lang/String;

    if-eqz v1, :cond_3

    const-string/jumbo v1, "place_id"

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->r:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->z:Ljava/util/ArrayList;

    invoke-static {v1}, Lcom/twitter/library/api/upload/ae;->b(Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_4

    const-string/jumbo v2, "media_ids"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_4
    iget-object v1, p0, Lcom/twitter/library/api/upload/w;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v2, p0, Lcom/twitter/library/api/upload/w;->z:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/library/api/TweetEntities;Ljava/util/ArrayList;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    const-string/jumbo v2, "media_tags"

    invoke-static {v0, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    return-object v0
.end method
