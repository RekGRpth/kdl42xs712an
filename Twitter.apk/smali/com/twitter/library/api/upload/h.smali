.class public Lcom/twitter/library/api/upload/h;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field protected static final a:Z


# instance fields
.field protected b:Lcom/twitter/library/api/upload/ae;

.field protected c:Lcom/twitter/library/api/upload/MediaProcessor;

.field protected d:Z

.field protected e:Lcom/twitter/library/api/MediaEntity;

.field protected f:Ljava/lang/String;

.field protected g:I

.field protected h:Ljava/util/ArrayList;

.field protected i:J

.field private j:Ljava/util/concurrent/ScheduledExecutorService;

.field private k:Landroid/content/Context;

.field private l:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "MediaUploader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/library/api/upload/h;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/h;->k:Landroid/content/Context;

    iput-wide p2, p0, Lcom/twitter/library/api/upload/h;->l:J

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/upload/h;->k:Landroid/content/Context;

    iput-wide p2, p0, Lcom/twitter/library/api/upload/h;->l:J

    iput-object p4, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    return-void
.end method

.method private a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/api/upload/i;)V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/library/api/upload/h;->d:Z

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v0

    if-eqz p2, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/api/upload/i;->a()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    iget-object v1, v0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    instance-of v1, v1, Ljavax/net/ssl/SSLException;

    if-nez v1, :cond_1

    iget-object v0, v0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    instance-of v0, v0, Ljava/net/SocketTimeoutException;

    if-eqz v0, :cond_2

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/library/api/upload/h;->d:Z

    :cond_2
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/upload/MediaProcessor;)Lcom/twitter/library/api/upload/h;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/h;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/upload/ae;)Lcom/twitter/library/api/upload/h;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    return-object p0
.end method

.method protected final a(Lcom/twitter/library/service/e;)Lcom/twitter/library/api/upload/h;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->f:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    iget-boolean v0, v0, Lcom/twitter/library/api/MediaEntity;->processed:Z

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_2
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadScheduledExecutor()Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/h;->j:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/h;->b(Lcom/twitter/library/service/e;)V

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->j:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdownNow()Ljava/util/List;

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/library/api/upload/MediaProcessor;->a(Z)V

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/upload/h;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/h;->f:Ljava/lang/String;

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;)V
    .locals 5

    new-instance v0, Lcom/twitter/library/api/upload/i;

    invoke-direct {v0, p1}, Lcom/twitter/library/api/upload/i;-><init>(Lcom/twitter/internal/network/HttpOperation;)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/h;->j:Ljava/util/concurrent/ScheduledExecutorService;

    const-wide/16 v2, 0x78

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v0, v2, v3, v4}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/i;->b()Z

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/api/upload/i;)V

    return-void
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/upload/h;->i:J

    return-wide v0
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/h;->c(Lcom/twitter/library/service/e;)V

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/upload/h;->g:I

    return v0
.end method

.method protected c(Lcom/twitter/library/service/e;)V
    .locals 6

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/library/api/upload/h;->h:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/library/api/upload/h;->g:I

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    invoke-interface {v0}, Lcom/twitter/library/api/upload/MediaProcessor;->e()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    iget-object v2, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    iget-object v3, p0, Lcom/twitter/library/api/upload/h;->f:Ljava/lang/String;

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/library/api/MediaEntity;Ljava/lang/String;Landroid/net/Uri;)Lcom/twitter/library/api/upload/ae;

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    iget-object v0, v0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    if-nez v0, :cond_1

    const/16 v0, 0x3ef

    new-instance v1, Lcom/twitter/library/util/MediaException;

    const-string/jumbo v2, "Error creating entity from image"

    invoke-direct {v1, v2}, Lcom/twitter/library/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/e;->a(ILjava/lang/Exception;)V

    :goto_0
    return-void

    :cond_1
    sget-boolean v0, Lcom/twitter/library/api/upload/h;->a:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "MediaUploader"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Uploading media.  fileSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/h;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    invoke-interface {v2}, Lcom/twitter/library/api/upload/MediaProcessor;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " retry #: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/library/api/upload/h;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/util/q;

    invoke-direct {v1}, Lcom/twitter/library/util/q;-><init>()V

    iget-object v2, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    iget-object v2, v2, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    invoke-interface {v2}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v2

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "upload-"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/twitter/library/api/upload/h;->g:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v4, v2, v3}, Lcom/twitter/library/util/q;->a(Ljava/lang/String;J)V

    iget v4, p0, Lcom/twitter/library/api/upload/h;->g:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/twitter/library/api/upload/h;->g:I

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/h;->a(Lcom/twitter/internal/network/HttpOperation;)V

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v4

    iget v4, v4, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {v1, v4}, Lcom/twitter/library/util/q;->a(I)V

    iget-object v4, p0, Lcom/twitter/library/api/upload/h;->h:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v1

    if-eqz v1, :cond_3

    iput-wide v2, p0, Lcom/twitter/library/api/upload/h;->i:J

    :goto_1
    iget-object v1, p0, Lcom/twitter/library/api/upload/h;->b:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1, v0, p1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/h;->f()Z

    move-result v1

    if-nez v1, :cond_0

    goto :goto_1
.end method

.method public d()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected e()Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    iget-boolean v0, v0, Lcom/twitter/library/api/MediaEntity;->processed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->k:Landroid/content/Context;

    iget-wide v1, p0, Lcom/twitter/library/api/upload/h;->l:J

    iget-object v3, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/h;->d:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    invoke-interface {v0}, Lcom/twitter/library/api/upload/MediaProcessor;->d()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/twitter/library/api/MediaEntity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/h;->e:Lcom/twitter/library/api/MediaEntity;

    return-object v0
.end method
