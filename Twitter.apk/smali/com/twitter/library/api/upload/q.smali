.class public Lcom/twitter/library/api/upload/q;
.super Lcom/twitter/library/api/upload/h;
.source "Twttr"


# instance fields
.field private j:Lcom/twitter/library/api/ao;

.field private k:J

.field private l:J

.field private final m:J


# direct methods
.method public constructor <init>(Landroid/content/Context;JJLcom/twitter/library/api/MediaEntity;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p6}, Lcom/twitter/library/api/upload/h;-><init>(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)V

    iput-wide p4, p0, Lcom/twitter/library/api/upload/q;->m:J

    return-void
.end method

.method private a(I)Lcom/twitter/library/api/upload/ae;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/q;->b:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/ae;->a()Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/library/api/upload/q;->j:Lcom/twitter/library/api/ao;

    iget-object v1, p0, Lcom/twitter/library/api/upload/q;->j:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/api/upload/ae;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    return-object v0
.end method

.method private a(Landroid/net/Uri;Lcom/twitter/library/service/e;)Z
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p2}, Lcom/twitter/library/api/upload/q;->d(Lcom/twitter/library/service/e;)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/twitter/library/api/upload/q;->b(Landroid/net/Uri;Lcom/twitter/library/service/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, p2}, Lcom/twitter/library/api/upload/q;->e(Lcom/twitter/library/service/e;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Ljava/io/InputStream;IJLjava/lang/String;Lcom/twitter/library/service/e;)Z
    .locals 5

    const/16 v0, 0x52

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/q;->a(I)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    const-string/jumbo v1, "media"

    long-to-int v2, p3

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/library/api/upload/ae;->a(Ljava/lang/String;Ljava/io/InputStream;I)Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "X-SessionPhase"

    const-string/jumbo v3, "APPEND"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "X-TotalBytes"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "X-MediaId"

    iget-wide v3, p0, Lcom/twitter/library/api/upload/q;->k:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "Content-MD5"

    invoke-virtual {v1, v2, p5}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "X-SegmentIndex"

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/upload/q;->a(Lcom/twitter/internal/network/HttpOperation;)V

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    return v0
.end method

.method private b(Landroid/net/Uri;Lcom/twitter/library/service/e;)Z
    .locals 12

    const/4 v6, 0x0

    const/4 v9, 0x0

    :try_start_0
    new-instance v1, Ljava/io/RandomAccessFile;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "r"

    invoke-direct {v1, v0, v2}, Ljava/io/RandomAccessFile;-><init>(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v0, Lcom/twitter/library/util/g;

    iget-wide v2, p0, Lcom/twitter/library/api/upload/q;->l:J

    iget-wide v4, p0, Lcom/twitter/library/api/upload/q;->m:J

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/util/g;-><init>(Ljava/io/RandomAccessFile;JJ)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0}, Lcom/twitter/library/util/g;->iterator()Ljava/util/Iterator;

    move-result-object v11

    move v4, v9

    :goto_0
    invoke-interface {v11}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v11}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/io/InputStream;

    if-nez v2, :cond_0

    const/16 v2, 0x3f0

    new-instance v3, Lcom/twitter/library/util/MediaException;

    const-string/jumbo v4, "Error reading image file"

    invoke-direct {v3, v4}, Lcom/twitter/library/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2, v3}, Lcom/twitter/library/service/e;->a(ILjava/lang/Exception;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    move v0, v9

    :goto_1
    return v0

    :cond_0
    :try_start_3
    invoke-virtual {v0}, Lcom/twitter/library/util/g;->c()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/twitter/library/util/g;->d()Ljava/lang/String;

    move-result-object v7

    move-object v3, v2

    move v10, v9

    :goto_2
    move-object v2, p0

    move-object v8, p2

    invoke-direct/range {v2 .. v8}, Lcom/twitter/library/api/upload/q;->a(Ljava/io/InputStream;IJLjava/lang/String;Lcom/twitter/library/service/e;)Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result v2

    if-eqz v2, :cond_2

    :cond_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v10, 0x1

    const/4 v3, 0x2

    if-le v2, v3, :cond_3

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    move v0, v9

    goto :goto_1

    :cond_3
    :try_start_4
    invoke-virtual {v0}, Lcom/twitter/library/util/g;->b()Ljava/io/InputStream;
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    move-result-object v3

    if-eqz v3, :cond_1

    move v10, v2

    goto :goto_2

    :cond_4
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x1

    goto :goto_1

    :catch_0
    move-exception v0

    move-object v0, v6

    move-object v1, v6

    :goto_3
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    move v0, v9

    goto :goto_1

    :catchall_0
    move-exception v0

    move-object v1, v6

    :goto_4
    invoke-static {v6}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v2

    move-object v6, v0

    move-object v0, v2

    goto :goto_4

    :catch_1
    move-exception v0

    move-object v0, v6

    goto :goto_3

    :catch_2
    move-exception v2

    goto :goto_3
.end method

.method private d(Lcom/twitter/library/service/e;)Z
    .locals 7

    const-wide/16 v5, 0x0

    const/16 v0, 0x37

    invoke-direct {p0, v0}, Lcom/twitter/library/api/upload/q;->a(I)Lcom/twitter/library/api/upload/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "X-SessionPhase"

    const-string/jumbo v3, "INIT"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    const-string/jumbo v2, "X-TotalBytes"

    iget-wide v3, p0, Lcom/twitter/library/api/upload/q;->l:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v0, v1, p1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    iput-wide v5, p0, Lcom/twitter/library/api/upload/q;->k:J

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/q;->j:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/upload/q;->k:J

    :cond_0
    iget-wide v0, p0, Lcom/twitter/library/api/upload/q;->k:J

    cmp-long v0, v0, v5

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/twitter/library/service/e;)Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/upload/q;->b:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/ae;->c()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    const-string/jumbo v1, "X-SessionPhase"

    const-string/jumbo v2, "FINALIZE"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    const-string/jumbo v1, "X-MediaId"

    iget-wide v2, p0, Lcom/twitter/library/api/upload/q;->k:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    iget-object v1, p0, Lcom/twitter/library/api/upload/q;->b:Lcom/twitter/library/api/upload/ae;

    invoke-virtual {v1, v0, p1}, Lcom/twitter/library/api/upload/ae;->a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 5

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/q;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    invoke-interface {v0}, Lcom/twitter/library/api/upload/MediaProcessor;->e()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/q;->c:Lcom/twitter/library/api/upload/MediaProcessor;

    invoke-interface {v1}, Lcom/twitter/library/api/upload/MediaProcessor;->c()I

    move-result v1

    int-to-long v1, v1

    iput-wide v1, p0, Lcom/twitter/library/api/upload/q;->l:J

    iget-wide v1, p0, Lcom/twitter/library/api/upload/q;->l:J

    iget-wide v3, p0, Lcom/twitter/library/api/upload/q;->m:J

    cmp-long v1, v1, v3

    if-gtz v1, :cond_1

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/q;->c(Lcom/twitter/library/service/e;)V

    :goto_0
    return-void

    :cond_1
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/library/api/upload/q;->d:Z

    invoke-direct {p0, v0, p1}, Lcom/twitter/library/api/upload/q;->a(Landroid/net/Uri;Lcom/twitter/library/service/e;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-wide v0, p0, Lcom/twitter/library/api/upload/q;->l:J

    iput-wide v0, p0, Lcom/twitter/library/api/upload/q;->i:J

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/q;->f()Z

    move-result v0

    if-nez v0, :cond_0

    goto :goto_0
.end method
