.class public Lcom/twitter/library/api/upload/a;
.super Lcom/twitter/library/api/upload/b;
.source "Twttr"


# instance fields
.field private b:Z

.field private c:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const-string/jumbo v0, "::media:upload"

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/upload/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Z)Lcom/twitter/library/api/upload/a;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/upload/a;->b:Z

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/upload/ad;Lcom/twitter/library/service/e;)V
    .locals 7

    const/4 v6, 0x2

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v1

    if-eqz v1, :cond_1

    move-object v0, p1

    check-cast v0, Lcom/twitter/library/api/upload/f;

    invoke-virtual {v0}, Lcom/twitter/library/api/upload/f;->f()Z

    move-result v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/ad;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string/jumbo v5, "app:twitter_service:media"

    aput-object v5, v4, v0

    const/4 v5, 0x1

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/a;->b:Z

    if-eqz v0, :cond_2

    const-string/jumbo v0, "retry_media"

    :goto_0
    aput-object v0, v4, v5

    iget v0, v1, Lcom/twitter/internal/network/k;->a:I

    const/16 v5, 0xc8

    if-ne v0, v5, :cond_3

    const-string/jumbo v0, "success"

    :goto_1
    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/a;->c:Z

    if-eqz v0, :cond_0

    invoke-virtual {v3, v6}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    if-eqz v2, :cond_4

    const-string/jumbo v0, "has_media"

    :goto_2
    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/internal/network/k;)I

    move-result v0

    invoke-virtual {v3, v0}, Lcom/twitter/library/scribe/ScribeLog;->b(I)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/library/api/upload/a;->a:Landroid/content/Context;

    invoke-static {v0, v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/internal/network/k;)V

    iget-object v0, p0, Lcom/twitter/library/api/upload/a;->a:Landroid/content/Context;

    invoke-static {v0, v3}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void

    :cond_2
    const-string/jumbo v0, "upload_media"

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "failure"

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "no_media"

    goto :goto_2
.end method

.method public b(Z)Lcom/twitter/library/api/upload/a;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/upload/a;->c:Z

    return-object p0
.end method
