.class public Lcom/twitter/library/api/upload/v;
.super Lcom/twitter/library/api/upload/w;
.source "Twttr"


# instance fields
.field private final n:Lcom/twitter/library/api/upload/g;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/upload/g;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/w;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput-object p3, p0, Lcom/twitter/library/api/upload/v;->n:Lcom/twitter/library/api/upload/g;

    return-void
.end method


# virtual methods
.method protected d(Lcom/twitter/library/service/e;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/api/upload/v;->n:Lcom/twitter/library/api/upload/g;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/v;->v()Lcom/twitter/internal/android/service/AsyncService;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/library/api/upload/g;->a(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/v;->n:Lcom/twitter/library/api/upload/g;

    invoke-interface {v0}, Lcom/twitter/library/api/upload/g;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/v;->b(Ljava/util/ArrayList;)V

    invoke-super {p0, p1}, Lcom/twitter/library/api/upload/w;->d(Lcom/twitter/library/service/e;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v1

    if-nez v1, :cond_1

    const/16 v0, 0x3ed

    new-instance v1, Lcom/twitter/library/util/MediaException;

    const-string/jumbo v2, "Failed to upload image"

    invoke-direct {v1, v2}, Lcom/twitter/library/util/MediaException;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/service/e;->a(ILjava/lang/Exception;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public g()F
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/v;->n:Lcom/twitter/library/api/upload/g;

    invoke-interface {v0}, Lcom/twitter/library/api/upload/g;->a()F

    move-result v0

    return v0
.end method
