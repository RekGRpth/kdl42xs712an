.class public abstract Lcom/twitter/library/api/upload/n;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"


# instance fields
.field protected d:Lcom/twitter/library/client/v;

.field private n:Lcom/twitter/library/api/ao;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    return-void
.end method


# virtual methods
.method protected a(JLjava/lang/String;)Lcom/twitter/library/api/ao;
    .locals 3

    const/16 v0, 0x11

    new-instance v1, Lcom/twitter/library/util/f;

    iget-object v2, p0, Lcom/twitter/library/api/upload/n;->l:Landroid/content/Context;

    invoke-direct {v1, v2, p1, p2, p3}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/n;->n:Lcom/twitter/library/api/ao;

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->n:Lcom/twitter/library/api/ao;

    return-object v0
.end method

.method public a(Lcom/twitter/library/client/v;)Lcom/twitter/library/api/upload/n;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/n;->d:Lcom/twitter/library/client/v;

    return-object p0
.end method

.method protected d(Lcom/twitter/library/service/e;)Z
    .locals 11

    const-wide/16 v2, -0x1

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->n:Lcom/twitter/library/api/ao;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->n:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/library/api/TwitterUser;

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/n;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    invoke-static {v0, v1, v10, v7}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/UserSettings;)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/n;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    const/4 v4, -0x1

    const/4 v9, 0x1

    move-wide v5, v2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    return v0
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->d:Lcom/twitter/library/client/v;

    iget-boolean v0, v0, Lcom/twitter/library/client/v;->c:Z

    return v0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/n;->d:Lcom/twitter/library/client/v;

    iget-boolean v0, v0, Lcom/twitter/library/client/v;->h:Z

    return v0
.end method
