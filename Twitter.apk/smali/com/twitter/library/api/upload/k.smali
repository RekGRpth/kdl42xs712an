.class public Lcom/twitter/library/api/upload/k;
.super Lcom/twitter/library/api/upload/ad;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/g;


# instance fields
.field private d:Lcom/twitter/library/api/TweetEntities;

.field private n:Lcom/twitter/library/api/upload/o;

.field private o:Ljava/util/ArrayList;

.field private p:J

.field private q:I

.field private final r:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    const v0, 0x7fffffff

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/library/api/upload/k;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;I)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/ad;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput p3, p0, Lcom/twitter/library/api/upload/k;->r:I

    return-void
.end method


# virtual methods
.method public a()F
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/upload/k;->p:J

    long-to-float v0, v0

    return v0
.end method

.method protected a(Lcom/twitter/library/api/upload/f;)Lcom/twitter/library/api/upload/f;
    .locals 0

    return-object p1
.end method

.method public a(Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/k;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/k;->d:Lcom/twitter/library/api/TweetEntities;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/k;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/k;->n:Lcom/twitter/library/api/upload/o;

    return-object p0
.end method

.method public a(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/library/service/e;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/library/api/upload/k;->c(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    return-object v0
.end method

.method protected a(Lcom/twitter/library/api/MediaEntity;)Z
    .locals 2

    iget v0, p0, Lcom/twitter/library/api/upload/k;->q:I

    iget v1, p0, Lcom/twitter/library/api/upload/k;->r:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/api/upload/f;Lcom/twitter/library/service/e;)Z
    .locals 2

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v1, p0, Lcom/twitter/library/api/upload/k;->q:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/library/api/upload/k;->q:I

    :cond_0
    return v0
.end method

.method public b()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/k;->o:Ljava/util/ArrayList;

    return-object v0
.end method

.method protected c(Lcom/twitter/library/service/e;)V
    .locals 11

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/k;->e()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/service/e;->a(Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/api/upload/k;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/p;->c:J

    const-wide/16 v4, 0x0

    iput-wide v4, p0, Lcom/twitter/library/api/upload/k;->p:J

    new-instance v0, Ljava/util/ArrayList;

    iget-object v4, p0, Lcom/twitter/library/api/upload/k;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v4, v4, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    invoke-direct {v0, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/k;->o:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/library/api/upload/k;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {p0, v0}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/MediaEntity;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/twitter/library/api/upload/k;->l:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-static {v5, v2, v3, v0, v6}, Lcom/twitter/library/api/upload/j;->a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;Z)Lcom/twitter/library/api/upload/h;

    move-result-object v5

    new-instance v6, Lcom/twitter/library/api/upload/f;

    iget-object v7, p0, Lcom/twitter/library/api/upload/k;->l:Landroid/content/Context;

    invoke-direct {v6, v7, v1}, Lcom/twitter/library/api/upload/f;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    invoke-virtual {v6, v5}, Lcom/twitter/library/api/upload/f;->a(Lcom/twitter/library/api/upload/h;)Lcom/twitter/library/api/upload/f;

    move-result-object v5

    iget-object v7, p0, Lcom/twitter/library/api/upload/k;->n:Lcom/twitter/library/api/upload/o;

    invoke-virtual {v5, v7}, Lcom/twitter/library/api/upload/f;->b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/k;->r()I

    move-result v7

    invoke-virtual {v5, v7}, Lcom/twitter/library/api/upload/ad;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0, v6}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/upload/f;)Lcom/twitter/library/api/upload/f;

    move-result-object v5

    invoke-virtual {p0, v5}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/upload/ad;)Lcom/twitter/library/service/e;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    invoke-virtual {p1}, Lcom/twitter/library/service/e;->a()Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v6}, Lcom/twitter/library/api/upload/f;->g()J

    move-result-wide v7

    iput-wide v7, v0, Lcom/twitter/library/api/MediaEntity;->id:J

    iget-object v0, p0, Lcom/twitter/library/api/upload/k;->o:Ljava/util/ArrayList;

    invoke-static {v7, v8}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v7, p0, Lcom/twitter/library/api/upload/k;->p:J

    invoke-virtual {v6}, Lcom/twitter/library/api/upload/f;->e()J

    move-result-wide v9

    add-long/2addr v7, v9

    iput-wide v7, p0, Lcom/twitter/library/api/upload/k;->p:J

    :cond_2
    invoke-virtual {p0, v6, p1}, Lcom/twitter/library/api/upload/k;->a(Lcom/twitter/library/api/upload/f;Lcom/twitter/library/service/e;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_3
    return-void
.end method

.method public e()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/k;->d:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/k;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/k;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
