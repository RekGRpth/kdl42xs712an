.class public Lcom/twitter/library/api/upload/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/twitter/internal/network/HttpOperation;

.field private final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private c:Z


# direct methods
.method public constructor <init>(Lcom/twitter/internal/network/HttpOperation;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/twitter/library/api/upload/i;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    iput-boolean v1, p0, Lcom/twitter/library/api/upload/i;->c:Z

    iput-object p1, p0, Lcom/twitter/library/api/upload/i;->a:Lcom/twitter/internal/network/HttpOperation;

    return-void
.end method


# virtual methods
.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/i;->c:Z

    return v0
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/i;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    return v0
.end method

.method public run()V
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/library/api/upload/i;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_0

    iput-boolean v1, p0, Lcom/twitter/library/api/upload/i;->c:Z

    iget-object v0, p0, Lcom/twitter/library/api/upload/i;->a:Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->d()V

    :cond_0
    return-void
.end method
