.class public Lcom/twitter/library/api/upload/l;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/upload/MediaProcessor;


# instance fields
.field private a:Landroid/content/Context;

.field private b:[I

.field private c:[I

.field private d:I

.field private e:Landroid/net/Uri;

.field private f:J

.field private g:Landroid/net/Uri;

.field private h:I

.field private i:I


# direct methods
.method public constructor <init>(Landroid/content/Context;[I[IILandroid/net/Uri;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/api/upload/l;->b:[I

    iput-object p3, p0, Lcom/twitter/library/api/upload/l;->c:[I

    iput p4, p0, Lcom/twitter/library/api/upload/l;->d:I

    iput-object p5, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    iput-wide p6, p0, Lcom/twitter/library/api/upload/l;->f:J

    iput-object p5, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    return-void
.end method

.method private a(II)Landroid/net/Uri;
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/twitter/media/MediaUtils;->a(Landroid/content/Context;Landroid/net/Uri;)Lcom/twitter/media/ImageInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-wide v3, p0, Lcom/twitter/library/api/upload/l;->f:J

    invoke-static {v2, v5, v3, v4}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    invoke-static {v3, v4, v2, p1, p2}, Lcom/twitter/media/MediaUtils;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/io/File;II)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string/jumbo v0, "resize"

    const-string/jumbo v3, "success"

    invoke-direct {p0, v0, v3, p1}, Lcom/twitter/library/api/upload/l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iget v2, v1, Lcom/twitter/media/ImageInfo;->width:I

    if-gt v2, p1, :cond_0

    iget v1, v1, Lcom/twitter/media/ImageInfo;->height:I

    if-gt v1, p1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    invoke-static {v2, v0}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v2

    int-to-float v2, v2

    int-to-float v1, v1

    div-float v1, v2, v1

    const v2, 0x3f333333    # 0.7f

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const-string/jumbo v1, "resize"

    const-string/jumbo v2, "skip"

    invoke-direct {p0, v1, v2, p1}, Lcom/twitter/library/api/upload/l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "resize"

    const-string/jumbo v2, "failure"

    invoke-direct {p0, v1, v2, p1}, Lcom/twitter/library/api/upload/l;->a(Ljava/lang/String;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    invoke-static {v1, v2}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v1

    invoke-virtual {v1, p1, p1}, Lkw;->a(II)Lkw;

    invoke-virtual {v1}, Lkw;->a()Lkw;

    invoke-virtual {v1}, Lkw;->b()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    invoke-static {v0, v2, v5}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;Z)Lcom/twitter/library/util/l;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-wide v3, p0, Lcom/twitter/library/api/upload/l;->f:J

    invoke-static {v0, v1, v3, v4, p2}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/graphics/Bitmap;JI)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    invoke-static {v1, v2, v0}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Lcom/twitter/library/util/l;Landroid/net/Uri;)V

    goto/16 :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "::image_processor"

    aput-object v1, v0, v5

    aput-object p1, v0, v6

    aput-object p2, v0, v7

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v3, p0, Lcom/twitter/library/api/upload/l;->f:J

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v6, [Ljava/lang/String;

    aput-object v0, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/twitter/library/scribe/ScribeLog;->c(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "size="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private f()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/upload/l;->i:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/upload/l;->i:I

    iget v1, p0, Lcom/twitter/library/api/upload/l;->d:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public a()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    return-object v0
.end method

.method public a(Z)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    invoke-direct {p0}, Lcom/twitter/library/api/upload/l;->g()V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/library/api/upload/l;->g()V

    return-void
.end method

.method public b()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/upload/l;->i:I

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->e:Landroid/net/Uri;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/upload/l;->h:I

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->b:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Landroid/net/Uri;
    .locals 3

    :goto_0
    invoke-direct {p0}, Lcom/twitter/library/api/upload/l;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/l;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/library/api/upload/l;->g()V

    :try_start_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->b:[I

    iget v1, p0, Lcom/twitter/library/api/upload/l;->h:I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->c:[I

    iget v2, p0, Lcom/twitter/library/api/upload/l;->h:I

    aget v1, v1, v2

    invoke-direct {p0, v0, v1}, Lcom/twitter/library/api/upload/l;->a(II)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;Landroid/net/Uri;)I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/upload/l;->i:I

    iget v0, p0, Lcom/twitter/library/api/upload/l;->h:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/library/api/upload/l;->h:I

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/l;->g:Landroid/net/Uri;

    return-object v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
