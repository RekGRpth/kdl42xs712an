.class public Lcom/twitter/library/api/upload/ae;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/twitter/library/service/p;

.field public c:Ljava/lang/StringBuilder;

.field public d:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

.field public e:Lcom/twitter/internal/network/i;

.field public f:Z

.field public g:I

.field public h:Lorg/apache/http/HttpEntity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/upload/ae;->a:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/library/api/upload/ae;->b:Lcom/twitter/library/service/p;

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iput-object v0, p0, Lcom/twitter/library/api/upload/ae;->d:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    const v0, 0xea60

    iput v0, p0, Lcom/twitter/library/api/upload/ae;->g:I

    return-void
.end method

.method public static a(Lcom/twitter/library/api/TweetEntities;Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 10

    const/4 v2, 0x0

    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    :cond_0
    move-object v0, v2

    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v1, ""

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v0, "{"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_5

    iget-wide v8, v0, Lcom/twitter/library/api/MediaEntity;->id:J

    cmp-long v8, v8, v5

    if-nez v8, :cond_5

    invoke-virtual {v0}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_5

    invoke-static {v0}, Lcom/twitter/library/api/upload/ae;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\""

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    const-string/jumbo v1, "\":"

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v0, ","

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_3
    const-string/jumbo v0, "}"

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    move-object v0, v2

    goto :goto_0

    :cond_4
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, v1

    goto :goto_2
.end method

.method private static a(Ljava/util/List;)Ljava/lang/String;
    .locals 9

    const/4 v1, 0x0

    :try_start_0
    new-instance v3, Ljava/io/StringWriter;

    const/16 v0, 0x200

    invoke-direct {v3, v0}, Ljava/io/StringWriter;-><init>(I)V

    sget-object v0, Lcom/twitter/library/api/ap;->a:Lcom/fasterxml/jackson/core/JsonFactory;

    invoke-virtual {v0, v3}, Lcom/fasterxml/jackson/core/JsonFactory;->a(Ljava/io/Writer;)Lcom/fasterxml/jackson/core/JsonGenerator;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    :try_start_1
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->a()V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->c()V

    const-string/jumbo v5, "type"

    const-string/jumbo v6, "user"

    invoke-virtual {v2, v5, v6}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-wide v5, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    const-wide/16 v7, 0x0

    cmp-long v5, v5, v7

    if-eqz v5, :cond_0

    const-string/jumbo v5, "user_id"

    iget-wide v6, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v5, v0, Lcom/twitter/library/api/MediaTag;->screenName:Ljava/lang/String;

    if-eqz v5, :cond_1

    const-string/jumbo v5, "screen_name"

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->screenName:Ljava/lang/String;

    invoke-virtual {v2, v5, v0}, Lcom/fasterxml/jackson/core/JsonGenerator;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->d()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v2

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    move-object v0, v1

    :goto_2
    return-object v0

    :cond_2
    :try_start_2
    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->b()V

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonGenerator;->flush()V

    invoke-virtual {v3}, Ljava/io/StringWriter;->getBuffer()Ljava/lang/StringBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_3
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0

    :catchall_1
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public static b(Ljava/util/ArrayList;)Ljava/lang/String;
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const-string/jumbo v0, ","

    invoke-static {v0, p0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/upload/ae;
    .locals 3

    new-instance v1, Lcom/twitter/library/api/upload/ae;

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/ae;->b:Lcom/twitter/library/service/p;

    invoke-direct {v1, v0, v2}, Lcom/twitter/library/api/upload/ae;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;)V

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v2, v1, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->d:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iput-object v0, v1, Lcom/twitter/library/api/upload/ae;->d:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->e:Lcom/twitter/internal/network/i;

    iput-object v0, v1, Lcom/twitter/library/api/upload/ae;->e:Lcom/twitter/internal/network/i;

    iget-boolean v0, p0, Lcom/twitter/library/api/upload/ae;->f:Z

    iput-boolean v0, v1, Lcom/twitter/library/api/upload/ae;->f:Z

    iget v0, p0, Lcom/twitter/library/api/upload/ae;->g:I

    iput v0, v1, Lcom/twitter/library/api/upload/ae;->g:I

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    iput-object v0, v1, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/api/upload/ae;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/ae;->e:Lcom/twitter/internal/network/i;

    return-object p0
.end method

.method public a(Lcom/twitter/library/api/MediaEntity;Ljava/lang/String;Landroid/net/Uri;)Lcom/twitter/library/api/upload/ae;
    .locals 4

    const/4 v1, 0x0

    if-eqz p3, :cond_2

    :try_start_0
    new-instance v0, Lcom/twitter/library/network/h;

    iget-object v2, p0, Lcom/twitter/library/api/upload/ae;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/network/h;-><init>(Landroid/content/Context;Lcom/twitter/internal/network/p;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v2, 0x8

    :try_start_1
    invoke-static {v2}, Lcom/twitter/internal/util/j;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, p2, v2, p3}, Lcom/twitter/library/network/h;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/MediaEntity;->b()Ljava/util/List;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    if-lez v2, :cond_1

    invoke-static {v1}, Lcom/twitter/library/api/upload/ae;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "media_tags"

    invoke-virtual {v0, v2, v1}, Lcom/twitter/library/network/h;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/network/h;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    return-object p0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/io/InputStream;I)Lcom/twitter/library/api/upload/ae;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Lcom/twitter/library/network/h;

    iget-object v2, p0, Lcom/twitter/library/api/upload/ae;->a:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/network/h;-><init>(Landroid/content/Context;Lcom/twitter/internal/network/p;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v1, 0x8

    :try_start_1
    invoke-static {v1}, Lcom/twitter/internal/util/j;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/twitter/library/network/h;->a(Ljava/lang/String;Ljava/lang/String;Ljava/io/InputStream;I)V

    invoke-virtual {v0}, Lcom/twitter/library/network/h;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_0
    iput-object v0, p0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    return-object p0

    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public a(Ljava/lang/StringBuilder;)Lcom/twitter/library/api/upload/ae;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    return-object p0
.end method

.method public a(Ljava/util/ArrayList;)Lcom/twitter/library/api/upload/ae;
    .locals 3

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    :try_start_0
    invoke-static {p1}, Lcom/twitter/library/util/Util;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Lorg/apache/http/entity/StringEntity;

    const-string/jumbo v2, "UTF-8"

    invoke-direct {v1, v0, v2}, Lorg/apache/http/entity/StringEntity;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    check-cast v0, Lorg/apache/http/entity/StringEntity;

    const-string/jumbo v1, "application/x-www-form-urlencoded"

    invoke-virtual {v0, v1}, Lorg/apache/http/entity/StringEntity;->setContentType(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-object p0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public a(Z)Lcom/twitter/library/api/upload/ae;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/upload/ae;->f:Z

    return-object p0
.end method

.method public a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)Lcom/twitter/library/service/e;
    .locals 1

    if-eqz p1, :cond_0

    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    :goto_0
    return-object p2

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/e;->a(Z)V

    goto :goto_0
.end method

.method public b()Ljava/lang/StringBuilder;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    return-object v0
.end method

.method public c()Lcom/twitter/internal/network/HttpOperation;
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Lcom/twitter/library/api/upload/ae;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/library/api/upload/ae;->c:Ljava/lang/StringBuilder;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/library/api/upload/ae;->b:Lcom/twitter/library/service/p;

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/ae;->d:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/library/api/upload/ae;->f:Z

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/n;

    iget-object v2, p0, Lcom/twitter/library/api/upload/ae;->b:Lcom/twitter/library/service/p;

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/ae;->e:Lcom/twitter/internal/network/i;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/upload/ae;->h:Lorg/apache/http/HttpEntity;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lorg/apache/http/HttpEntity;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    iget v1, p0, Lcom/twitter/library/api/upload/ae;->g:I

    if-lez v1, :cond_1

    iget v1, p0, Lcom/twitter/library/api/upload/ae;->g:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/network/HttpOperation;->a(I)V

    :cond_1
    return-object v0
.end method
