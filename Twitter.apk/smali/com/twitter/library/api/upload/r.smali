.class public Lcom/twitter/library/api/upload/r;
.super Lcom/twitter/library/api/upload/c;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/upload/r;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/upload/c;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/api/upload/w;)Lcom/twitter/library/api/upload/w;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/api/upload/w;->a(Z)Lcom/twitter/library/api/upload/w;

    return-object p1
.end method

.method protected a(Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/e;)Z
    .locals 1

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    return v0
.end method

.method protected e()Landroid/database/Cursor;
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/library/api/upload/r;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v0, v0, Lcom/twitter/library/service/p;->c:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/library/api/upload/r;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/twitter/library/provider/ah;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    invoke-virtual {v2, v3, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/upload/r;->d:[Ljava/lang/String;

    const-string/jumbo v3, "flags&1= 0"

    const/4 v4, 0x0

    const-string/jumbo v5, "updated_at ASC"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
