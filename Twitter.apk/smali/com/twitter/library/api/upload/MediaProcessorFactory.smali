.class public Lcom/twitter/library/api/upload/MediaProcessorFactory;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:[[[I

.field private static final b:[[[I

.field private static final c:[[[I

.field private static final d:[[[I

.field private static final e:[[[I

.field private static final f:[[[I

.field private static final g:[[[I

.field private static final h:[[[I

.field private static final i:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const v7, 0xaf000

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x3

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_0

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_1

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_2

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_3

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_4

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_5

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_6

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_7

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_8

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_9

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_a

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_b

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->b:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_c

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_d

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_e

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_f

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_10

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_11

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->c:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_12

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_13

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_14

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_15

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_16

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_17

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->d:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_18

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_19

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_1a

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_1b

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_1c

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_1d

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->e:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_1e

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_1f

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_20

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_21

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_22

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_23

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->f:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_24

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_25

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_26

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_27

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_28

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_29

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->g:[[[I

    new-array v0, v3, [[[I

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_2a

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_2b

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_2c

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_2d

    aput-object v2, v1, v5

    aput-object v1, v0, v5

    new-array v1, v6, [[I

    new-array v2, v3, [I

    fill-array-data v2, :array_2e

    aput-object v2, v1, v4

    new-array v2, v3, [I

    fill-array-data v2, :array_2f

    aput-object v2, v1, v5

    aput-object v1, v0, v6

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->h:[[[I

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->i:Landroid/util/SparseArray;

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/library/api/upload/MediaProcessor$Action;->a:Lcom/twitter/library/api/upload/MediaProcessor$Action;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/MediaProcessor$Action;->ordinal()I

    move-result v1

    new-instance v2, Lcom/twitter/library/api/upload/e;

    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->a:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    invoke-static {v3}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I

    move-result-object v3

    sget-object v4, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->a:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    invoke-static {v4}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->b(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I

    move-result-object v4

    const/high16 v5, 0x300000

    invoke-direct {v2, v3, v4, v5}, Lcom/twitter/library/api/upload/e;-><init>([I[II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/library/api/upload/MediaProcessor$Action;->b:Lcom/twitter/library/api/upload/MediaProcessor$Action;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/MediaProcessor$Action;->ordinal()I

    move-result v1

    new-instance v2, Lcom/twitter/library/api/upload/e;

    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->c:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    invoke-static {v3}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I

    move-result-object v3

    sget-object v4, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->c:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    invoke-static {v4}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->b(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I

    move-result-object v4

    invoke-direct {v2, v3, v4, v7}, Lcom/twitter/library/api/upload/e;-><init>([I[II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->i:Landroid/util/SparseArray;

    sget-object v1, Lcom/twitter/library/api/upload/MediaProcessor$Action;->c:Lcom/twitter/library/api/upload/MediaProcessor$Action;

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/MediaProcessor$Action;->ordinal()I

    move-result v1

    new-instance v2, Lcom/twitter/library/api/upload/e;

    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->b:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    invoke-static {v3}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I

    move-result-object v3

    sget-object v4, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->b:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    invoke-static {v4}, Lcom/twitter/library/api/upload/MediaProcessorFactory;->b(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I

    move-result-object v4

    invoke-direct {v2, v3, v4, v7}, Lcom/twitter/library/api/upload/e;-><init>([I[II)V

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void

    nop

    :array_0
    .array-data 4
        0x640
        0x4e4
        0x320
    .end array-data

    :array_1
    .array-data 4
        0x640
        0x4e4
        0x320
    .end array-data

    :array_2
    .array-data 4
        0x640
        0x4e4
        0x320
    .end array-data

    :array_3
    .array-data 4
        0x640
        0x4e4
        0x320
    .end array-data

    :array_4
    .array-data 4
        0x320
        0x258
        0x258
    .end array-data

    :array_5
    .array-data 4
        0x320
        0x258
        0x258
    .end array-data

    :array_6
    .array-data 4
        0x55
        0x50
        0x46
    .end array-data

    :array_7
    .array-data 4
        0x55
        0x50
        0x46
    .end array-data

    :array_8
    .array-data 4
        0x55
        0x50
        0x46
    .end array-data

    :array_9
    .array-data 4
        0x55
        0x50
        0x46
    .end array-data

    :array_a
    .array-data 4
        0x55
        0x50
        0x46
    .end array-data

    :array_b
    .array-data 4
        0x55
        0x50
        0x46
    .end array-data

    :array_c
    .array-data 4
        0x400
        0x400
        0x400
    .end array-data

    :array_d
    .array-data 4
        0x400
        0x400
        0x400
    .end array-data

    :array_e
    .array-data 4
        0x5dc
        0x5dc
        0x5dc
    .end array-data

    :array_f
    .array-data 4
        0x5dc
        0x5dc
        0x5dc
    .end array-data

    :array_10
    .array-data 4
        0x190
        0xc8
        0x80
    .end array-data

    :array_11
    .array-data 4
        0x190
        0xc8
        0x80
    .end array-data

    :array_12
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_13
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_14
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_15
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_16
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_17
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_18
    .array-data 4
        0x800
        0x400
        0x400
    .end array-data

    :array_19
    .array-data 4
        0x400
        0x400
        0x400
    .end array-data

    :array_1a
    .array-data 4
        0x5dc
        0x5dc
        0x5dc
    .end array-data

    :array_1b
    .array-data 4
        0x5dc
        0x5dc
        0x5dc
    .end array-data

    :array_1c
    .array-data 4
        0x190
        0xc8
        0x80
    .end array-data

    :array_1d
    .array-data 4
        0x190
        0xc8
        0x80
    .end array-data

    :array_1e
    .array-data 4
        0x37
        0x50
        0x50
    .end array-data

    :array_1f
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_20
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_21
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_22
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_23
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_24
    .array-data 4
        0x400
        0x300
        0x258
    .end array-data

    :array_25
    .array-data 4
        0x400
        0x300
        0x258
    .end array-data

    :array_26
    .array-data 4
        0x5dc
        0x4e4
        0x320
    .end array-data

    :array_27
    .array-data 4
        0x5dc
        0x4e4
        0x320
    .end array-data

    :array_28
    .array-data 4
        0x190
        0xc8
        0x80
    .end array-data

    :array_29
    .array-data 4
        0x190
        0xc8
        0x80
    .end array-data

    :array_2a
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_2b
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_2c
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_2d
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_2e
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data

    :array_2f
    .array-data 4
        0x50
        0x50
        0x50
    .end array-data
.end method

.method public static final a(Landroid/content/Context;Landroid/net/Uri;ILcom/twitter/library/api/upload/MediaProcessor$Action;J)Lcom/twitter/library/api/upload/MediaProcessor;
    .locals 8

    const/4 v0, 0x0

    packed-switch p2, :pswitch_data_0

    new-instance v0, Lcom/twitter/library/api/upload/m;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/upload/m;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    :goto_0
    return-object v0

    :pswitch_0
    invoke-static {}, Lcom/twitter/media/MediaUtils;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "android_native_photo_resize_2051"

    invoke-static {v1}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "android_native_photo_resize_2051"

    invoke-static {v2}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v2, "default"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "high"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "degrade"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    move v2, v0

    :goto_1
    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->i:Landroid/util/SparseArray;

    invoke-virtual {p3}, Lcom/twitter/library/api/upload/MediaProcessor$Action;->ordinal()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Lcom/twitter/library/api/upload/e;

    if-eqz v2, :cond_2

    new-instance v0, Lcom/twitter/library/api/upload/l;

    iget-object v2, v1, Lcom/twitter/library/api/upload/e;->a:[I

    iget-object v3, v1, Lcom/twitter/library/api/upload/e;->b:[I

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/e;->a()I

    move-result v4

    move-object v1, p0

    move-object v5, p1

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/upload/l;-><init>(Landroid/content/Context;[I[IILandroid/net/Uri;J)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/twitter/library/api/upload/d;

    iget-object v2, v1, Lcom/twitter/library/api/upload/e;->a:[I

    iget-object v3, v1, Lcom/twitter/library/api/upload/e;->b:[I

    invoke-virtual {v1}, Lcom/twitter/library/api/upload/e;->a()I

    move-result v4

    move-object v1, p0

    move-object v5, p1

    move-wide v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/upload/d;-><init>(Landroid/content/Context;[I[IILandroid/net/Uri;J)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/twitter/library/api/upload/m;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/upload/m;-><init>(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->a:[[[I

    const-string/jumbo v1, "android_native_photo_resize_2051"

    invoke-static {v1}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "default"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->c:[[[I

    :cond_0
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/App;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    sget-object v4, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->a:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    if-ne p0, v4, :cond_4

    :goto_2
    aget-object v0, v0, v3

    aget-object v0, v0, v1

    return-object v0

    :cond_1
    const-string/jumbo v4, "high"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->e:[[[I

    goto :goto_0

    :cond_2
    const-string/jumbo v4, "degrade"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->g:[[[I

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->c:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    if-ne p0, v3, :cond_5

    const/4 v3, 0x2

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_2
.end method

.method public static b(Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;)[I
    .locals 5

    const/4 v2, 0x1

    const/4 v3, 0x0

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->b:[[[I

    const-string/jumbo v1, "android_native_photo_resize_2051"

    invoke-static {v1}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v4, "default"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->d:[[[I

    :cond_0
    :goto_0
    invoke-static {}, Lcom/twitter/library/client/App;->q()Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v2

    :goto_1
    sget-object v4, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->a:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    if-ne p0, v4, :cond_4

    :goto_2
    aget-object v0, v0, v3

    aget-object v0, v0, v1

    return-object v0

    :cond_1
    const-string/jumbo v4, "high"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_2

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->f:[[[I

    goto :goto_0

    :cond_2
    const-string/jumbo v4, "degrade"

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/twitter/library/api/upload/MediaProcessorFactory;->h:[[[I

    goto :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    sget-object v3, Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;->c:Lcom/twitter/library/api/upload/MediaProcessorFactory$ImageUploadType;

    if-ne p0, v3, :cond_5

    const/4 v3, 0x2

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_2
.end method
