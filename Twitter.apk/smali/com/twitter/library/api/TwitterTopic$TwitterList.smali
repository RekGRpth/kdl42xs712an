.class public Lcom/twitter/library/api/TwitterTopic$TwitterList;
.super Lcom/twitter/library/api/TwitterTopic$Data;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = 0x2a303d3fb71b5c72L


# instance fields
.field public followStatus:I

.field public members:I

.field public mode:I

.field public mostRecentTweetTimestampMillis:J

.field public subscribers:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    return-void
.end method

.method constructor <init>(IIIIJ)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/library/api/TwitterTopic$Data;-><init>()V

    iput p1, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->members:I

    iput p2, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->subscribers:I

    iput p3, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mode:I

    iput p4, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->followStatus:I

    iput-wide p5, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mostRecentTweetTimestampMillis:J

    return-void
.end method


# virtual methods
.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->members:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->subscribers:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mode:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->followStatus:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mostRecentTweetTimestampMillis:J

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->members:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->subscribers:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mode:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->followStatus:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->mostRecentTweetTimestampMillis:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    return-void
.end method
