.class public Lcom/twitter/library/api/TwitterTopic;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;


# instance fields
.field public a:J

.field public final b:Lcom/twitter/library/api/TwitterTopic$Metadata;

.field public final c:Ljava/lang/String;

.field public final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field public final h:Ljava/lang/String;

.field public final i:Ljava/lang/String;

.field public final j:J

.field public final k:J

.field public final l:J

.field public final m:Lcom/twitter/library/api/TwitterUser;

.field public final n:Lcom/twitter/library/api/PromotedContent;

.field public final o:Ljava/util/ArrayList;

.field private final p:Lcom/twitter/library/api/TwitterTopic$Data;


# direct methods
.method constructor <init>(Lcom/twitter/library/api/TwitterTopic$Metadata;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJJLcom/twitter/library/api/PromotedContent;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterTopic$Data;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterTopic;->b:Lcom/twitter/library/api/TwitterTopic$Metadata;

    iput-object p2, p0, Lcom/twitter/library/api/TwitterTopic;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/api/TwitterTopic;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/TwitterTopic;->e:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/TwitterTopic;->f:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/library/api/TwitterTopic;->i:Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/api/TwitterTopic;->g:Ljava/lang/String;

    iput-object p8, p0, Lcom/twitter/library/api/TwitterTopic;->h:Ljava/lang/String;

    iput-wide p9, p0, Lcom/twitter/library/api/TwitterTopic;->j:J

    iput-wide p11, p0, Lcom/twitter/library/api/TwitterTopic;->k:J

    iput-wide p13, p0, Lcom/twitter/library/api/TwitterTopic;->l:J

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->n:Lcom/twitter/library/api/PromotedContent;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->o:Ljava/util/ArrayList;

    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->m:Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->p:Lcom/twitter/library/api/TwitterTopic$Data;

    return-void
.end method

.method public static a(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "UNPLANNED"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "TRENDS"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "TV"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "LOCAL"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "SPORTS"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CLIENT_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/api/TwitterTopic;->e(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    if-eqz p0, :cond_0

    const-string/jumbo v0, "CLIENT_"

    invoke-virtual {p0, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)I
    .locals 1

    const-string/jumbo v0, "UNPLANNED"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v0, "TRENDS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "TV"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x2

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "LOCAL"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x3

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "SPORTS"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto :goto_0

    :cond_4
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public static c(I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/api/TwitterTopic;->e(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static d(I)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/api/TwitterTopic;->e(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static e(I)Ljava/lang/String;
    .locals 1

    packed-switch p0, :pswitch_data_0

    const-string/jumbo v0, "unknown"

    :goto_0
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "tv_module"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "unplanned_module"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "local_module"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "sports_module"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "trend"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/TwitterTopic;->a:J

    return-wide v0
.end method

.method public a(Ljava/lang/Class;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->p:Lcom/twitter/library/api/TwitterTopic$Data;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->p:Lcom/twitter/library/api/TwitterTopic$Data;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->isInstance(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->p:Lcom/twitter/library/api/TwitterTopic$Data;

    invoke-virtual {p1, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterTopic;->b:Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterTopic$Metadata;->id:Ljava/lang/String;

    return-object v0
.end method
