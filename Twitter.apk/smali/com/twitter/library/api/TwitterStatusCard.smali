.class public Lcom/twitter/library/api/TwitterStatusCard;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final serialVersionUID:J = 0x280a447a0f9cf418L


# instance fields
.field public cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

.field public classicCard:Lcom/twitter/library/api/TweetClassicCard;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-array v0, v5, [Ljava/lang/String;

    const-string/jumbo v1, "promo_app"

    aput-object v1, v0, v3

    const-string/jumbo v1, "promo_image_app"

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/library/api/TwitterStatusCard;->a:[Ljava/lang/String;

    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "com.poptip:oscars1"

    aput-object v1, v0, v3

    const-string/jumbo v1, "com.poptip:oscars2"

    aput-object v1, v0, v4

    const-string/jumbo v1, "com.poptip:oscars3"

    aput-object v1, v0, v5

    const-string/jumbo v1, "com.poptip:oscars4"

    aput-object v1, v0, v6

    const-string/jumbo v1, "com.poptip:oscars5"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "com.poptip:oscars6"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "com.poptip:oscars7"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "com.poptip:oscars8"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/TwitterStatusCard;->b:[Ljava/lang/String;

    const/4 v0, 0x7

    new-array v0, v0, [Ljava/lang/String;

    const-string/jumbo v1, "photo_image"

    aput-object v1, v0, v3

    const-string/jumbo v1, "player_image"

    aput-object v1, v0, v4

    const-string/jumbo v1, "summary_photo_image"

    aput-object v1, v0, v5

    const-string/jumbo v1, "promo_image"

    aput-object v1, v0, v6

    const-string/jumbo v1, "thumbnail_image"

    aput-object v1, v0, v7

    const/4 v1, 0x5

    const-string/jumbo v2, "click_to_call_image"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "thumbnail"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/TwitterStatusCard;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0, v0}, Lcom/twitter/library/api/TwitterStatusCard;-><init>(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/card/instance/CardInstanceData;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/library/api/TwitterStatusCard;-><init>(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/card/instance/CardInstanceData;)V

    return-void
.end method

.method private constructor <init>(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/card/instance/CardInstanceData;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    iput-object p2, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/card/instance/CardInstanceData;)V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p1, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "player_url"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v0}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string/jumbo v1, "summary"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "summary_large_image"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_0
    invoke-static {p1, v5}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    :cond_1
    :goto_0
    iput-object p1, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    return-void

    :cond_2
    const-string/jumbo v1, "photo"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_3
    const-string/jumbo v1, "player"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-static {p1, v4}, Lcom/twitter/library/api/TwitterStatusCard;->c(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_4
    const-string/jumbo v1, "amplify"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-static {p1, v4}, Lcom/twitter/library/api/TwitterStatusCard;->b(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "animated_gif"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-static {p1, v4}, Lcom/twitter/library/api/TwitterStatusCard;->c(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_6
    const-string/jumbo v1, "promotion"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-static {p1, v3}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_7
    iget-object v1, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/api/TwitterStatusCard;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-static {p1, v3}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_8
    const-string/jumbo v1, "click_to_call"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-static {p1, v3}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto :goto_0

    :cond_9
    const-string/jumbo v1, "promo_website"

    iget-object v2, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_a

    invoke-static {p1, v3}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto/16 :goto_0

    :cond_a
    iget-object v1, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/api/TwitterStatusCard;->c(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-static {p1, v5}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto/16 :goto_0

    :cond_b
    iget-object v1, p1, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/api/TwitterStatusCard;->b(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_c

    invoke-static {p1, v3}, Lcom/twitter/library/api/TwitterStatusCard;->c(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto/16 :goto_0

    :cond_c
    invoke-static {v0}, Lcom/twitter/library/util/Util;->f(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p1, v4}, Lcom/twitter/library/api/TwitterStatusCard;->c(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    goto/16 :goto_0
.end method

.method private static a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;
    .locals 17

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v1, "title"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v1}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    sget-object v4, Lcom/twitter/library/api/TwitterStatusCard;->c:[Ljava/lang/String;

    array-length v5, v4

    const/4 v1, 0x0

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v1, v4, v2

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v1, :cond_1

    iget-object v1, v1, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v1, Lcom/twitter/library/card/property/ImageSpec;

    iget-object v6, v1, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    iget-object v2, v1, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v2, v2, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v9, v2

    iget-object v1, v1, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    iget v1, v1, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v10, v1

    :cond_0
    const-string/jumbo v1, "description"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v1}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    const-string/jumbo v1, "card_url"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v1}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    const-string/jumbo v1, "site"

    invoke-virtual {v7, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    iget-object v1, v1, Lcom/twitter/library/card/instance/BindingValue;->value:Ljava/lang/Object;

    check-cast v1, Lcom/twitter/library/card/instance/UserValue;

    iget-object v1, v1, Lcom/twitter/library/card/instance/UserValue;->id:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    move-object v15, v1

    :goto_1
    new-instance v1, Lcom/twitter/library/api/TweetClassicCard;

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/16 v16, 0x0

    move/from16 v2, p1

    invoke-direct/range {v1 .. v16}, Lcom/twitter/library/api/TweetClassicCard;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/Promotion;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/TwitterUser;)V

    return-object v1

    :cond_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_2
    const/4 v15, 0x0

    goto :goto_1
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    sget-object v2, Lcom/twitter/library/api/TwitterStatusCard;->a:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method private static b(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;
    .locals 2

    invoke-static {p0, p1}, Lcom/twitter/library/api/TwitterStatusCard;->c(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iget-object v1, v0, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    :cond_0
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Z
    .locals 1

    const-string/jumbo v0, "appplayer"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private static c(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;
    .locals 4

    iget-object v1, p0, Lcom/twitter/library/card/instance/CardInstanceData;->bindingValues:Ljava/util/HashMap;

    const-string/jumbo v0, "player_url"

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v0}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p0, p1}, Lcom/twitter/library/api/TwitterStatusCard;->a(Lcom/twitter/library/card/instance/CardInstanceData;I)Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v2

    const/4 v3, 0x1

    iput v3, v2, Lcom/twitter/library/api/TweetClassicCard;->playerType:I

    const-string/jumbo v3, "player_stream_url"

    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/card/instance/BindingValue;

    invoke-static {v1}, Lcom/twitter/library/card/instance/BindingValue;->a(Lcom/twitter/library/card/instance/BindingValue;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v2, Lcom/twitter/library/api/TweetClassicCard;->playerStreamUrl:Ljava/lang/String;

    iput-object v0, v2, Lcom/twitter/library/api/TweetClassicCard;->playerUrl:Ljava/lang/String;

    iget-object v0, v2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    iput-object v0, v2, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    :cond_0
    return-object v2
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    const-string/jumbo v1, "com.poptip:oscars"

    invoke-virtual {p0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v2, Lcom/twitter/library/api/TwitterStatusCard;->b:[Ljava/lang/String;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    invoke-virtual {v4, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    instance-of v2, p1, Lcom/twitter/library/api/TwitterStatusCard;

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    check-cast p1, Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {v2, v3}, Lcom/twitter/library/card/instance/CardInstanceData;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-nez v2, :cond_3

    :cond_5
    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {v1}, Lcom/twitter/library/card/instance/CardInstanceData;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/instance/CardInstanceData;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    :cond_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetClassicCard;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    :cond_1
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_0

    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_1

    invoke-interface {p1, v2}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    :goto_1
    return-void

    :cond_0
    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeInt(I)V

    goto :goto_0

    :cond_1
    invoke-interface {p1, v1}, Ljava/io/ObjectOutput;->writeInt(I)V

    goto :goto_1
.end method
