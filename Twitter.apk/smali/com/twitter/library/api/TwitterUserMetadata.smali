.class public Lcom/twitter/library/api/TwitterUserMetadata;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:Lcom/twitter/library/api/TwitterSocialProof;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/ay;

    invoke-direct {v0}, Lcom/twitter/library/api/ay;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TwitterUserMetadata;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterSocialProof;

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->d:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Lcom/twitter/library/api/TwitterSocialProof;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    iput-object p2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    iput-boolean p4, p0, Lcom/twitter/library/api/TwitterUserMetadata;->d:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TwitterUserMetadata;

    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->d:Z

    iget-boolean v3, p1, Lcom/twitter/library/api/TwitterUserMetadata;->d:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    invoke-virtual {v2, v3}, Lcom/twitter/library/api/TwitterSocialProof;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    if-nez v2, :cond_5

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    if-nez v2, :cond_8

    :cond_a
    iget-object v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    if-eqz v2, :cond_b

    iget-object v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p1, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterSocialProof;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v2, p0, Lcom/twitter/library/api/TwitterUserMetadata;->d:Z

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->a:Lcom/twitter/library/api/TwitterSocialProof;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->c:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/library/api/TwitterUserMetadata;->d:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
