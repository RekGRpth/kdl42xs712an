.class public abstract Lcom/twitter/library/api/conversations/e;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field protected final d:Ljava/lang/String;

.field protected e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

.field private f:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p4, p0, Lcom/twitter/library/api/conversations/e;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/e;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/e;->e()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x48

    new-instance v3, Lcom/twitter/library/util/f;

    iget-object v4, p0, Lcom/twitter/library/api/conversations/e;->l:Landroid/content/Context;

    iget-wide v5, v0, Lcom/twitter/library/service/p;->c:J

    invoke-direct {v3, v4, v5, v6, v1}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v2, v3}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/library/api/conversations/e;->f:Lcom/twitter/library/api/ao;

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/api/conversations/e;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/e;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/e;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/c;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/e;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/c;)V

    iget-object v0, v0, Lcom/twitter/library/api/conversations/c;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    iput-object v0, p0, Lcom/twitter/library/api/conversations/e;->e:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method protected abstract e()Ljava/lang/String;
.end method
