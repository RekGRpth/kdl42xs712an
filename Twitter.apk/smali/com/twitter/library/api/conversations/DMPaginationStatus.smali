.class public final enum Lcom/twitter/library/api/conversations/DMPaginationStatus;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/library/api/conversations/DMPaginationStatus;

.field public static final enum b:Lcom/twitter/library/api/conversations/DMPaginationStatus;

.field public static final enum c:Lcom/twitter/library/api/conversations/DMPaginationStatus;

.field public static final enum d:Lcom/twitter/library/api/conversations/DMPaginationStatus;

.field private static final synthetic e:[Lcom/twitter/library/api/conversations/DMPaginationStatus;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;

    const-string/jumbo v1, "AT_END"

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/conversations/DMPaginationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->a:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    new-instance v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;

    const-string/jumbo v1, "HAS_MORE"

    invoke-direct {v0, v1, v3}, Lcom/twitter/library/api/conversations/DMPaginationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->b:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    new-instance v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;

    const-string/jumbo v1, "TOO_DEEP"

    invoke-direct {v0, v1, v4}, Lcom/twitter/library/api/conversations/DMPaginationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->c:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    new-instance v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;

    const-string/jumbo v1, "NOT_FOUND"

    invoke-direct {v0, v1, v5}, Lcom/twitter/library/api/conversations/DMPaginationStatus;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->d:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    const/4 v0, 0x4

    new-array v0, v0, [Lcom/twitter/library/api/conversations/DMPaginationStatus;

    sget-object v1, Lcom/twitter/library/api/conversations/DMPaginationStatus;->a:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/library/api/conversations/DMPaginationStatus;->b:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/library/api/conversations/DMPaginationStatus;->c:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    aput-object v1, v0, v4

    sget-object v1, Lcom/twitter/library/api/conversations/DMPaginationStatus;->d:Lcom/twitter/library/api/conversations/DMPaginationStatus;

    aput-object v1, v0, v5

    sput-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->e:[Lcom/twitter/library/api/conversations/DMPaginationStatus;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/twitter/library/api/conversations/DMPaginationStatus;
    .locals 1

    :try_start_0
    invoke-static {p0}, Lcom/twitter/library/api/conversations/DMPaginationStatus;->valueOf(Ljava/lang/String;)Lcom/twitter/library/api/conversations/DMPaginationStatus;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/library/api/conversations/DMPaginationStatus;
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;

    return-object v0
.end method

.method public static values()[Lcom/twitter/library/api/conversations/DMPaginationStatus;
    .locals 1

    sget-object v0, Lcom/twitter/library/api/conversations/DMPaginationStatus;->e:[Lcom/twitter/library/api/conversations/DMPaginationStatus;

    invoke-virtual {v0}, [Lcom/twitter/library/api/conversations/DMPaginationStatus;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/conversations/DMPaginationStatus;

    return-object v0
.end method
