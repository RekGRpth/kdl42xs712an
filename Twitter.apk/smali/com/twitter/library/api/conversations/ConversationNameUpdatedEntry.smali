.class public Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;
.super Lcom/twitter/library/api/conversations/ConversationEntry;
.source "Twttr"


# static fields
.field private static final serialVersionUID:J = -0xcf4874bddacd5b9L


# instance fields
.field public final newConversationName:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V
    .locals 7

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-wide v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/conversations/ConversationEntry;-><init>(Ljava/lang/String;Ljava/lang/String;JJ)V

    iput-object p5, p0, Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;->newConversationName:Ljava/lang/String;

    return-void
.end method

.method public static b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;
    .locals 9

    const-wide/16 v3, -0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-wide v6, v3

    move-object v2, v5

    move-object v1, v5

    :goto_0
    if-eqz v0, :cond_5

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v8, :cond_5

    sget-object v8, Lcom/twitter/library/api/conversations/h;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v8, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v8, "id"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :cond_1
    const-string/jumbo v8, "conversation_id"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string/jumbo v8, "time"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->n()J

    move-result-wide v3

    goto :goto_1

    :cond_3
    const-string/jumbo v8, "conversation_name"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    :cond_4
    const-string/jumbo v8, "by_user_id"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->o()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_5
    new-instance v0, Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;-><init>(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;J)V

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/database/sqlite/SQLiteDatabase;J)V
    .locals 4

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;->conversationId:Ljava/lang/String;

    aput-object v2, v0, v1

    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    const-string/jumbo v2, "title"

    iget-object v3, p0, Lcom/twitter/library/api/conversations/ConversationNameUpdatedEntry;->newConversationName:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "conversations"

    const-string/jumbo v3, "conversation_id=?"

    invoke-virtual {p1, v2, v1, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/library/api/conversations/ConversationEntry;->a(Landroid/database/sqlite/SQLiteDatabase;J)V

    return-void
.end method

.method protected d()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method
