.class public Lcom/twitter/library/api/conversations/ar;
.super Lcom/twitter/library/api/conversations/as;
.source "Twttr"


# instance fields
.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;Z)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/conversations/ar;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/conversations/as;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lcom/twitter/library/api/conversations/ar;->d:Ljava/lang/String;

    invoke-virtual {p0, p4}, Lcom/twitter/library/api/conversations/ar;->a(Z)V

    return-void
.end method


# virtual methods
.method protected c(Lcom/twitter/library/service/e;)V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ar;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ar;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/az;->g(Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x5

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/DMLocalMessage;

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->id:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->senderId:J

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->conversationId:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ar;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {v1, v0, v5}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/DMLocalMessage;I)V

    iget-object v1, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "photo"

    iget-object v3, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    invoke-virtual {v3}, Lcom/twitter/library/api/conversations/k;->a()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_0
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/library/api/conversations/ar;->a(Lcom/twitter/library/api/conversations/DMLocalMessage;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/service/e;)V

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_2
    return-void

    :cond_3
    iget-object v1, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    check-cast v1, Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v1, v1, Lcom/twitter/library/api/conversations/DMPhoto;->mediaEntity:Lcom/twitter/library/api/MediaEntity;

    goto :goto_0
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
