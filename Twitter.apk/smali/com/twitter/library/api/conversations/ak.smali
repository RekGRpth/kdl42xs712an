.class public Lcom/twitter/library/api/conversations/ak;
.super Lcom/twitter/library/api/conversations/as;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/conversations/DMLocalMessage;

.field private final n:Lcom/twitter/library/api/MediaEntity;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/api/MediaEntity;)V
    .locals 11

    const-class v1, Lcom/twitter/library/api/conversations/ak;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, p1, v1, p2}, Lcom/twitter/library/api/conversations/as;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    new-instance v1, Lcom/twitter/library/api/conversations/DMLocalMessage;

    invoke-static {}, Lcom/twitter/library/api/conversations/DMLocalMessage;->f()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object/from16 v3, p5

    move-wide v6, p3

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v10}, Lcom/twitter/library/api/conversations/DMLocalMessage;-><init>(Ljava/lang/String;Ljava/lang/String;JJLjava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/api/conversations/k;)V

    iput-object v1, p0, Lcom/twitter/library/api/conversations/ak;->d:Lcom/twitter/library/api/conversations/DMLocalMessage;

    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    return-void
.end method


# virtual methods
.method protected c(Lcom/twitter/library/service/e;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    iget-boolean v0, v0, Lcom/twitter/library/api/MediaEntity;->processed:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ak;->l:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ak;->d:Lcom/twitter/library/api/conversations/DMLocalMessage;

    iget-wide v1, v1, Lcom/twitter/library/api/conversations/DMLocalMessage;->senderId:J

    iget-object v3, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;JLcom/twitter/library/api/MediaEntity;)Z

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    iget-object v1, v1, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ak;->d:Lcom/twitter/library/api/conversations/DMLocalMessage;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    invoke-static {v1}, Lcom/twitter/library/api/conversations/DMPhoto;->a(Lcom/twitter/library/api/MediaEntity;)Lcom/twitter/library/api/conversations/DMPhoto;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/api/conversations/DMLocalMessage;->attachment:Lcom/twitter/library/api/conversations/k;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/api/conversations/ak;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ak;->d:Lcom/twitter/library/api/conversations/DMLocalMessage;

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/conversations/ConversationEntry;)V

    iget-object v0, p0, Lcom/twitter/library/api/conversations/ak;->d:Lcom/twitter/library/api/conversations/DMLocalMessage;

    iget-object v1, p0, Lcom/twitter/library/api/conversations/ak;->n:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {p0, v0, v1, p1}, Lcom/twitter/library/api/conversations/ak;->a(Lcom/twitter/library/api/conversations/DMLocalMessage;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/service/e;)V

    return-void
.end method

.method public e()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
