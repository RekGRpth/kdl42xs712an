.class public Lcom/twitter/library/api/Conversation;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/library/api/Conversation$Metadata;

.field public final b:[Lcom/twitter/library/api/TwitterStatus;


# direct methods
.method public constructor <init>(Lcom/twitter/library/api/Conversation$Metadata;[Lcom/twitter/library/api/TwitterStatus;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/library/api/Conversation;->a:Lcom/twitter/library/api/Conversation$Metadata;

    iput-object p2, p0, Lcom/twitter/library/api/Conversation;->b:[Lcom/twitter/library/api/TwitterStatus;

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/TwitterStatus;
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/Conversation;->b:[Lcom/twitter/library/api/TwitterStatus;

    iget-object v1, p0, Lcom/twitter/library/api/Conversation;->b:[Lcom/twitter/library/api/TwitterStatus;

    array-length v1, v1

    add-int/lit8 v1, v1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/Conversation;->a:Lcom/twitter/library/api/Conversation$Metadata;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/Conversation;->a:Lcom/twitter/library/api/Conversation$Metadata;

    iget v0, v0, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
