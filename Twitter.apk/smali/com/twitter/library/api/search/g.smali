.class public Lcom/twitter/library/api/search/g;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:I

.field public final b:Lcom/twitter/library/api/TwitterStatus;

.field public final c:Lcom/twitter/library/api/TwitterUser;

.field public final d:Ljava/util/ArrayList;

.field public final e:Lcom/twitter/library/api/TwitterSearchSuggestion;

.field public final f:[Ljava/lang/String;

.field public final g:Ljava/util/ArrayList;

.field public final h:Lcom/twitter/library/api/af;

.field public final i:Lcom/twitter/library/api/TwitterSearchFilter;

.field public final j:Z

.field public final k:Ljava/util/ArrayList;

.field public final l:Z

.field public final m:Ljava/lang/String;


# direct methods
.method public constructor <init>(ILcom/twitter/library/api/TwitterStatus;Lcom/twitter/library/api/TwitterUser;Ljava/util/ArrayList;Lcom/twitter/library/api/TwitterSearchSuggestion;[Ljava/lang/String;Ljava/util/ArrayList;Lcom/twitter/library/api/af;Lcom/twitter/library/api/TwitterSearchFilter;ZLjava/util/ArrayList;ZLjava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/twitter/library/api/search/g;->a:I

    iput-object p2, p0, Lcom/twitter/library/api/search/g;->b:Lcom/twitter/library/api/TwitterStatus;

    iput-object p3, p0, Lcom/twitter/library/api/search/g;->c:Lcom/twitter/library/api/TwitterUser;

    iput-object p4, p0, Lcom/twitter/library/api/search/g;->d:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/twitter/library/api/search/g;->e:Lcom/twitter/library/api/TwitterSearchSuggestion;

    iput-object p6, p0, Lcom/twitter/library/api/search/g;->f:[Ljava/lang/String;

    iput-object p7, p0, Lcom/twitter/library/api/search/g;->g:Ljava/util/ArrayList;

    iput-object p8, p0, Lcom/twitter/library/api/search/g;->h:Lcom/twitter/library/api/af;

    iput-object p9, p0, Lcom/twitter/library/api/search/g;->i:Lcom/twitter/library/api/TwitterSearchFilter;

    iput-boolean p10, p0, Lcom/twitter/library/api/search/g;->j:Z

    iput-object p11, p0, Lcom/twitter/library/api/search/g;->k:Ljava/util/ArrayList;

    iput-boolean p12, p0, Lcom/twitter/library/api/search/g;->l:Z

    iput-object p13, p0, Lcom/twitter/library/api/search/g;->m:Ljava/lang/String;

    return-void
.end method
