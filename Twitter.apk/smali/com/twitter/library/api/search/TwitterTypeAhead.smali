.class public Lcom/twitter/library/api/search/TwitterTypeAhead;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:Ljava/util/ArrayList;

.field public final e:Lcom/twitter/library/api/TwitterUser;

.field public final f:Lcom/twitter/library/api/search/TwitterSearchQuery;

.field public final g:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/search/l;

    invoke-direct {v0}, Lcom/twitter/library/api/search/l;-><init>()V

    sput-object v0, Lcom/twitter/library/api/search/TwitterTypeAhead;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(IIILjava/util/ArrayList;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/api/search/TwitterSearchQuery;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->a:I

    iput p2, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->b:I

    iput p3, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->c:I

    iput-object p4, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->d:Ljava/util/ArrayList;

    iput-object p5, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/library/api/TwitterUser;

    iput-object p6, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/library/api/search/TwitterSearchQuery;

    iput-object p7, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->g:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->a:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->b:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->createStringArrayList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iput-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1, v1}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/search/TwitterSearchQuery;

    iput-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/library/api/search/TwitterSearchQuery;

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->g:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->d:Ljava/util/ArrayList;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeStringList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->f:Lcom/twitter/library/api/search/TwitterSearchQuery;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/library/api/search/TwitterTypeAhead;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    return-void
.end method
