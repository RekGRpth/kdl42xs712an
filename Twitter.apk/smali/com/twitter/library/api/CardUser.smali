.class public final Lcom/twitter/library/api/CardUser;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = 0x2b39017a019aca19L


# instance fields
.field public fullName:Ljava/lang/String;

.field public profileImageUrl:Ljava/lang/String;

.field public screenName:Ljava/lang/String;

.field public userId:J

.field public verified:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/CardUser;->userId:J

    iput-object p3, p0, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/twitter/library/api/CardUser;->verified:Z

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/api/TwitterUser;)V
    .locals 7

    iget-wide v1, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v3, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v4, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iget-object v5, p1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    iget-boolean v6, p1, Lcom/twitter/library/api/TwitterUser;->verified:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/library/api/CardUser;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    if-eq p0, p1, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    if-ne v0, v1, :cond_1

    iget-wide v0, p0, Lcom/twitter/library/api/CardUser;->userId:J

    check-cast p1, Lcom/twitter/library/api/CardUser;

    iget-wide v2, p1, Lcom/twitter/library/api/CardUser;->userId:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/twitter/library/api/CardUser;->userId:J

    iget-wide v2, p0, Lcom/twitter/library/api/CardUser;->userId:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 2

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/CardUser;->userId:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readBoolean()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/library/api/CardUser;->verified:Z

    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/CardUser;->userId:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget-object v0, p0, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/CardUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-boolean v0, p0, Lcom/twitter/library/api/CardUser;->verified:Z

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeBoolean(Z)V

    return-void
.end method
