.class public Lcom/twitter/library/api/Conversation$Metadata;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field private static final serialVersionUID:J = -0x11e616c5bca77659L


# instance fields
.field public participants:[Lcom/twitter/library/api/Conversation$Participant;

.field public participantsCount:I

.field public targetCount:I

.field public targetTweetId:J


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public constructor <init>(JI[Lcom/twitter/library/api/Conversation$Participant;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    iput p3, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    iput-object p4, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    iput p5, p0, Lcom/twitter/library/api/Conversation$Metadata;->participantsCount:I

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/Conversation$Metadata;

    iget v2, p0, Lcom/twitter/library/api/Conversation$Metadata;->participantsCount:I

    iget v3, p1, Lcom/twitter/library/api/Conversation$Metadata;->participantsCount:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    iget v3, p1, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget-wide v2, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    iget-wide v4, p1, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget-object v2, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    iget-object v3, p1, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    iget-wide v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    iget-wide v2, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    const/16 v4, 0x20

    ushr-long/2addr v2, v4

    xor-long/2addr v0, v2

    long-to-int v0, v0

    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    :goto_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/Conversation$Metadata;->participantsCount:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    add-int/2addr v0, v1

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 4

    invoke-interface {p1}, Ljava/io/ObjectInput;->readLong()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->participantsCount:I

    invoke-interface {p1}, Ljava/io/ObjectInput;->readInt()I

    move-result v2

    if-lez v2, :cond_0

    new-array v0, v2, [Lcom/twitter/library/api/Conversation$Participant;

    iput-object v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/Conversation$Participant;

    aput-object v0, v3, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    invoke-interface {p1, v0, v1}, Ljava/io/ObjectOutput;->writeLong(J)V

    iget v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->participantsCount:I

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/Conversation$Metadata;->participants:[Lcom/twitter/library/api/Conversation$Participant;

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/io/ObjectOutput;)V

    return-void
.end method
