.class public Lcom/twitter/library/api/account/v;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/network/n;

.field private e:Lcom/twitter/library/api/TwitterUser;

.field private f:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/network/n;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lcom/twitter/library/api/account/v;->d:Lcom/twitter/library/network/n;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/network/n;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/v;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput-object p3, p0, Lcom/twitter/library/api/account/v;->d:Lcom/twitter/library/network/n;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/library/api/account/v;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v2, "account"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    const-string/jumbo v3, "verify_credentials"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "include_user_entities"

    invoke-static {v0, v1, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    invoke-virtual {p0}, Lcom/twitter/library/api/account/v;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v1, v1, Lcom/twitter/library/service/p;->c:J

    const/16 v3, 0x11

    new-instance v4, Lcom/twitter/library/util/f;

    iget-object v5, p0, Lcom/twitter/library/api/account/v;->l:Landroid/content/Context;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v4, v5, v1, v2, v6}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v3, v4}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/library/api/account/v;->f:Lcom/twitter/library/api/ao;

    new-instance v3, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/api/account/v;->l:Landroid/content/Context;

    invoke-direct {v3, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v1, v2}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/v;->d:Lcom/twitter/library/network/n;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/v;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 10

    const-wide/16 v2, -0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/account/v;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iput-object v0, p0, Lcom/twitter/library/api/account/v;->e:Lcom/twitter/library/api/TwitterUser;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v4, p0, Lcom/twitter/library/api/account/v;->l:Landroid/content/Context;

    iget-wide v5, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v4, v5, v6}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    const/4 v4, -0x1

    const/4 v9, 0x1

    move-wide v5, v2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public e()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/v;->e:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method
