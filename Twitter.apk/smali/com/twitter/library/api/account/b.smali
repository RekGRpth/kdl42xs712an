.class public abstract Lcom/twitter/library/api/account/b;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field protected final d:Lcom/twitter/library/api/ao;

.field private e:Lcom/twitter/library/api/TwitterUser;

.field private f:[I

.field private g:Lcom/twitter/library/network/LoginResponse;


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    const/16 v0, 0x21

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/b;->d:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/library/api/account/b;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "oauth"

    aput-object v3, v1, v2

    const-string/jumbo v2, "access_token"

    aput-object v2, v1, v5

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "x_auth_mode"

    const-string/jumbo v4, "client_auth"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->z()Z

    move-result v2

    if-eqz v2, :cond_1

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "x_auth_login_verification"

    const-string/jumbo v4, "1"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->A()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "x_auth_login_challenge"

    const-string/jumbo v4, "1"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "send_error_codes"

    const-string/jumbo v4, "true"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/account/b;->a(Ljava/util/ArrayList;)V

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Lcom/twitter/library/api/account/b;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/library/api/account/b;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/n;

    invoke-direct {v2, v5}, Lcom/twitter/library/network/n;-><init>(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/b;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    const-string/jumbo v1, "Accept"

    const-string/jumbo v2, "application/json"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Lcom/twitter/internal/network/HttpOperation;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "x_auth_json_response"

    const-string/jumbo v4, "1"

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method protected final a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 5

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/account/b;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/LoginResponse;

    iput-object v0, p0, Lcom/twitter/library/api/account/b;->g:Lcom/twitter/library/network/LoginResponse;

    iget v1, v0, Lcom/twitter/library/network/LoginResponse;->c:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    new-instance v1, Lcom/twitter/library/api/account/v;

    iget-object v2, p0, Lcom/twitter/library/api/account/b;->l:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/library/api/account/b;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/network/LoginResponse;->a:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v4, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/api/account/v;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/network/n;)V

    invoke-virtual {p0, v1}, Lcom/twitter/library/api/account/b;->a(Lcom/twitter/internal/android/service/a;)Lcom/twitter/library/service/e;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/twitter/library/service/e;->a(Lcom/twitter/library/service/e;)V

    invoke-virtual {v1}, Lcom/twitter/library/api/account/v;->e()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/b;->e:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0, p2}, Lcom/twitter/library/api/account/b;->c(Lcom/twitter/library/service/e;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/library/api/account/b;->d:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/api/account/b;->c(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/b;->f:[I

    invoke-virtual {p0, p2}, Lcom/twitter/library/api/account/b;->d(Lcom/twitter/library/service/e;)V

    :cond_1
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    goto :goto_0
.end method

.method protected abstract a(Ljava/util/ArrayList;)V
.end method

.method protected c(Lcom/twitter/library/service/e;)V
    .locals 0

    return-void
.end method

.method protected d(Lcom/twitter/library/service/e;)V
    .locals 0

    return-void
.end method

.method public final e()Lcom/twitter/library/api/TwitterUser;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/b;->e:Lcom/twitter/library/api/TwitterUser;

    return-object v0
.end method

.method public final f()[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/b;->f:[I

    return-object v0
.end method

.method public final g()Lcom/twitter/library/network/LoginResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/b;->g:Lcom/twitter/library/network/LoginResponse;

    return-object v0
.end method
