.class public Lcom/twitter/library/api/account/a;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field public final d:Lcom/twitter/library/api/account/l;

.field private e:[I

.field private f:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/account/l;)V
    .locals 1

    const-class v0, Lcom/twitter/library/api/account/a;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lcom/twitter/library/api/account/a;->d:Lcom/twitter/library/api/account/l;

    const/16 v0, 0x2b

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/a;->f:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    iget-object v0, p0, Lcom/twitter/library/api/account/a;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "account"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "login_verification_request"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/a;->d:Lcom/twitter/library/api/account/l;

    iget-object v1, v1, Lcom/twitter/library/api/account/l;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/library/api/account/a;->d:Lcom/twitter/library/api/account/l;

    iget-object v2, v2, Lcom/twitter/library/api/account/l;->b:Ljava/lang/String;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    new-instance v4, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "lv_id"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v5, v1}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v4, "challenge_response"

    invoke-direct {v1, v4, v2}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0}, Lcom/twitter/library/api/account/a;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v4, p0, Lcom/twitter/library/api/account/a;->l:Landroid/content/Context;

    invoke-direct {v2, v4, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v4, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v4, v5}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    sget-object v2, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/account/a;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/account/a;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/api/account/a;->c(Ljava/util/ArrayList;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/account/a;->e:[I

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public e()[I
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/account/a;->e:[I

    return-object v0
.end method
