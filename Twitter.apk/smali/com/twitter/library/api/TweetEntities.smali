.class public Lcom/twitter/library/api/TweetEntities;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Externalizable;


# static fields
.field public static final a:Lcom/twitter/library/api/TweetEntities;

.field public static final b:Ljava/util/Comparator;

.field private static final c:[Ljava/lang/String;

.field private static final serialVersionUID:J = -0x4a1404847223bc12L


# instance fields
.field public cashtags:Ljava/util/ArrayList;

.field public contacts:Ljava/util/ArrayList;

.field public hashtags:Ljava/util/ArrayList;

.field public media:Ljava/util/ArrayList;

.field public mentions:Ljava/util/ArrayList;

.field public urls:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v0}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TweetEntities;->a:Lcom/twitter/library/api/TweetEntities;

    new-instance v0, Lcom/twitter/library/api/ab;

    invoke-direct {v0}, Lcom/twitter/library/api/ab;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TweetEntities;->b:Ljava/util/Comparator;

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "entities"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/library/api/TweetEntities;->c:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/TweetEntities;
    .locals 10

    const/4 v0, 0x3

    const/4 v2, 0x0

    new-instance v4, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v4}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    new-instance v5, Ljava/util/LinkedHashSet;

    invoke-direct {v5, v0}, Ljava/util/LinkedHashSet;-><init>(I)V

    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6, v0}, Ljava/util/LinkedHashSet;-><init>(I)V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    if-eqz v1, :cond_27

    move-object v0, v2

    :goto_0
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_27

    sget-object v3, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v7

    aget v3, v3, v7

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    const-string/jumbo v3, "urls"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    :cond_1
    :goto_2
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v3, :cond_1

    new-instance v3, Lcom/twitter/library/api/UrlEntity;

    invoke-direct {v3}, Lcom/twitter/library/api/UrlEntity;-><init>()V

    :cond_2
    :goto_3
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v7, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v7, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_1

    goto :goto_3

    :pswitch_4
    const-string/jumbo v7, "indices"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    invoke-static {p0, v3}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/Entity;)V

    goto :goto_3

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :pswitch_6
    const-string/jumbo v7, "url_https"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    goto :goto_3

    :cond_4
    const-string/jumbo v7, "expanded_url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    goto :goto_3

    :cond_5
    const-string/jumbo v7, "url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/UrlEntity;->insecureUrl:Ljava/lang/String;

    goto :goto_3

    :cond_6
    const-string/jumbo v7, "display_url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    goto :goto_3

    :cond_7
    iget-object v7, v3, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    if-nez v7, :cond_8

    iget-object v7, v3, Lcom/twitter/library/api/UrlEntity;->insecureUrl:Ljava/lang/String;

    iput-object v7, v3, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    :cond_8
    invoke-virtual {v5, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    :cond_9
    const-string/jumbo v3, "user_mentions"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_e

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v3, v1

    move-object v1, v0

    move-object v0, v2

    :goto_4
    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v8, :cond_d

    sget-object v8, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v8, v3

    packed-switch v3, :pswitch_data_2

    :cond_a
    :goto_5
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_4

    :pswitch_7
    new-instance v0, Lcom/twitter/library/api/MentionEntity;

    invoke-direct {v0}, Lcom/twitter/library/api/MentionEntity;-><init>()V

    goto :goto_5

    :pswitch_8
    if-eqz v0, :cond_a

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :pswitch_9
    if-eqz v0, :cond_b

    const-string/jumbo v3, "indices"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    invoke-static {p0, v0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/Entity;)V

    goto :goto_5

    :cond_b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_5

    :pswitch_a
    if-eqz v0, :cond_a

    const-string/jumbo v3, "screen_name"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/twitter/library/api/MentionEntity;->screenName:Ljava/lang/String;

    goto :goto_5

    :cond_c
    const-string/jumbo v3, "name"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/twitter/library/api/MentionEntity;->name:Ljava/lang/String;

    goto :goto_5

    :pswitch_b
    if-eqz v0, :cond_a

    const-string/jumbo v3, "id"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v8

    iput-wide v8, v0, Lcom/twitter/library/api/MentionEntity;->userId:J

    goto :goto_5

    :pswitch_c
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_5

    :cond_d
    iput-object v7, v4, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    move-object v0, v1

    goto/16 :goto_1

    :cond_e
    const-string/jumbo v3, "hashtags"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_12

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v3, v1

    move-object v1, v2

    :goto_6
    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v8, :cond_11

    sget-object v8, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v8, v3

    packed-switch v3, :pswitch_data_3

    :cond_f
    :goto_7
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_6

    :pswitch_d
    new-instance v1, Lcom/twitter/library/api/HashtagEntity;

    invoke-direct {v1}, Lcom/twitter/library/api/HashtagEntity;-><init>()V

    goto :goto_7

    :pswitch_e
    if-eqz v1, :cond_f

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_7

    :pswitch_f
    if-eqz v1, :cond_10

    const-string/jumbo v3, "indices"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-static {p0, v1}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/Entity;)V

    goto :goto_7

    :cond_10
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_7

    :pswitch_10
    if-eqz v1, :cond_f

    const-string/jumbo v3, "text"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_f

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/twitter/library/api/HashtagEntity;->text:Ljava/lang/String;

    goto :goto_7

    :cond_11
    iput-object v7, v4, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    goto/16 :goto_1

    :cond_12
    const-string/jumbo v3, "symbols"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_16

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    move-object v3, v1

    move-object v1, v2

    :goto_8
    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v8, :cond_15

    sget-object v8, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v3}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v3

    aget v3, v8, v3

    packed-switch v3, :pswitch_data_4

    :cond_13
    :goto_9
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_8

    :pswitch_11
    new-instance v1, Lcom/twitter/library/api/HashtagEntity;

    invoke-direct {v1}, Lcom/twitter/library/api/HashtagEntity;-><init>()V

    goto :goto_9

    :pswitch_12
    if-eqz v1, :cond_13

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_9

    :pswitch_13
    if-eqz v1, :cond_14

    const-string/jumbo v3, "indices"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_14

    invoke-static {p0, v1}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/Entity;)V

    goto :goto_9

    :cond_14
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_9

    :pswitch_14
    if-eqz v1, :cond_13

    const-string/jumbo v3, "text"

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_13

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v1, Lcom/twitter/library/api/HashtagEntity;->text:Ljava/lang/String;

    goto :goto_9

    :cond_15
    iput-object v7, v4, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;

    goto/16 :goto_1

    :cond_16
    const-string/jumbo v3, "media"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_26

    :cond_17
    :goto_a
    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v1, v3, :cond_17

    new-instance v3, Lcom/twitter/library/api/MediaEntity;

    invoke-direct {v3}, Lcom/twitter/library/api/MediaEntity;-><init>()V

    :cond_18
    :goto_b
    sget-object v7, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v1, v7, :cond_23

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    sget-object v7, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v1}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v8

    aget v7, v7, v8

    packed-switch v7, :pswitch_data_5

    :pswitch_15
    goto :goto_b

    :pswitch_16
    const-string/jumbo v7, "indices"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_19

    invoke-static {p0, v3}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/Entity;)V

    goto :goto_b

    :cond_19
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_b

    :pswitch_17
    const-string/jumbo v7, "sizes"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1a

    invoke-static {p0, v3}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V

    goto :goto_b

    :cond_1a
    const-string/jumbo v7, "features"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1b

    invoke-static {p0, v3}, Lcom/twitter/library/api/TweetEntities;->b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V

    goto :goto_b

    :cond_1b
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_b

    :pswitch_18
    const-string/jumbo v7, "id"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1c

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v7

    iput-wide v7, v3, Lcom/twitter/library/api/MediaEntity;->id:J

    goto :goto_b

    :cond_1c
    const-string/jumbo v7, "source_status_id"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_18

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->h()J

    move-result-wide v7

    iput-wide v7, v3, Lcom/twitter/library/api/MediaEntity;->sourceStatusId:J

    goto :goto_b

    :pswitch_19
    const-string/jumbo v7, "url_https"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1d

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    goto :goto_b

    :cond_1d
    const-string/jumbo v7, "url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1e

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->insecureUrl:Ljava/lang/String;

    goto/16 :goto_b

    :cond_1e
    const-string/jumbo v7, "expanded_url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_1f

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->expandedUrl:Ljava/lang/String;

    goto/16 :goto_b

    :cond_1f
    const-string/jumbo v7, "display_url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_20

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->displayUrl:Ljava/lang/String;

    goto/16 :goto_b

    :cond_20
    const-string/jumbo v7, "type"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_21

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "photo"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_18

    const/4 v7, 0x1

    iput v7, v3, Lcom/twitter/library/api/MediaEntity;->type:I

    goto/16 :goto_b

    :cond_21
    const-string/jumbo v7, "media_url_https"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_22

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    goto/16 :goto_b

    :cond_22
    const-string/jumbo v7, "media_url"

    invoke-virtual {v7, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_18

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v7

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->insecureMediaUrl:Ljava/lang/String;

    goto/16 :goto_b

    :cond_23
    iget-object v7, v3, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    if-nez v7, :cond_24

    iget-object v7, v3, Lcom/twitter/library/api/MediaEntity;->insecureUrl:Ljava/lang/String;

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->url:Ljava/lang/String;

    :cond_24
    iget-object v7, v3, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    if-nez v7, :cond_25

    iget-object v7, v3, Lcom/twitter/library/api/MediaEntity;->insecureMediaUrl:Ljava/lang/String;

    iput-object v7, v3, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    :cond_25
    invoke-virtual {v5, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v6, v3}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto/16 :goto_a

    :cond_26
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto/16 :goto_1

    :cond_27
    invoke-virtual {v5}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_28

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v5}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v4, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    sget-object v1, Lcom/twitter/library/api/TweetEntities;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_28
    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v0, :cond_29

    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    sget-object v1, Lcom/twitter/library/api/TweetEntities;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_29
    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2a

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, v4, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    iget-object v0, v4, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    sget-object v1, Lcom/twitter/library/api/TweetEntities;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    :cond_2a
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_7
        :pswitch_a
        :pswitch_8
        :pswitch_b
        :pswitch_c
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_f
        :pswitch_d
        :pswitch_10
        :pswitch_e
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_13
        :pswitch_11
        :pswitch_14
        :pswitch_12
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x1
        :pswitch_16
        :pswitch_17
        :pswitch_19
        :pswitch_15
        :pswitch_18
    .end packed-switch
.end method

.method public static a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)Ljava/lang/StringBuilder;
    .locals 8

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-nez v0, :cond_1

    :cond_0
    return-object p0

    :cond_1
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    const/4 v0, 0x0

    iget-object v1, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget v5, v0, Lcom/twitter/library/api/UrlEntity;->start:I

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    if-eqz v1, :cond_2

    iget v5, v1, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    iput v5, v0, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    iget v1, v1, Lcom/twitter/library/api/UrlEntity;->displayEnd:I

    iput v1, v0, Lcom/twitter/library/api/UrlEntity;->displayEnd:I

    goto :goto_0

    :cond_2
    invoke-virtual {v3, v5, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sub-int v1, v5, v2

    iget v5, v0, Lcom/twitter/library/api/UrlEntity;->end:I

    sub-int/2addr v5, v2

    if-ltz v1, :cond_3

    invoke-virtual {p0}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    if-gt v5, v6, :cond_3

    iget-object v6, v0, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_3

    invoke-virtual {p0, v1, v5, v6}, Ljava/lang/StringBuilder;->replace(IILjava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    add-int/2addr v6, v1

    sub-int/2addr v5, v6

    add-int/2addr v2, v5

    iput v1, v0, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    iput v6, v0, Lcom/twitter/library/api/UrlEntity;->displayEnd:I

    :cond_3
    move v0, v2

    move v2, v0

    goto :goto_0
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/Entity;)V
    .locals 3

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    iget v1, p1, Lcom/twitter/library/api/Entity;->start:I

    if-ne v1, v2, :cond_1

    iput v0, p1, Lcom/twitter/library/api/Entity;->start:I

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget v1, p1, Lcom/twitter/library/api/Entity;->end:I

    if-ne v1, v2, :cond_0

    iput v0, p1, Lcom/twitter/library/api/Entity;->end:I

    goto :goto_1

    :cond_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    return-void
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V
    .locals 5

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_8

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_8

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_7

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "large"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move v1, v2

    move-object v3, v0

    move v0, v2

    :goto_1
    if-eqz v3, :cond_4

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_4

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->i:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v4, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "w"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v1

    :cond_0
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v3

    goto :goto_1

    :cond_1
    const-string/jumbo v4, "h"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    goto :goto_2

    :cond_2
    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->b:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v3, v4, :cond_3

    sget-object v4, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v3, v4, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_4
    if-lez v1, :cond_5

    if-lez v0, :cond_5

    iput v1, p1, Lcom/twitter/library/api/MediaEntity;->width:I

    iput v0, p1, Lcom/twitter/library/api/MediaEntity;->height:I

    :cond_5
    :goto_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_7
    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-ne v0, v1, :cond_5

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_3

    :cond_8
    return-void
.end method

.method private static a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TweetMediaFeature;)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v2, v0

    move-object v0, v1

    :goto_0
    if-eqz v2, :cond_4

    sget-object v3, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v2, v3, :cond_4

    sget-object v3, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v2}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v2

    aget v2, v3, v2

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v2

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    const-string/jumbo v2, "x"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    iput v0, p1, Lcom/twitter/library/api/TweetMediaFeature;->x:I

    :cond_0
    :goto_2
    move-object v0, v1

    goto :goto_1

    :cond_1
    const-string/jumbo v2, "y"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    iput v0, p1, Lcom/twitter/library/api/TweetMediaFeature;->y:I

    goto :goto_2

    :cond_2
    const-string/jumbo v2, "h"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    iput v0, p1, Lcom/twitter/library/api/TweetMediaFeature;->h:I

    goto :goto_2

    :cond_3
    const-string/jumbo v2, "w"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->g()I

    move-result v0

    iput v0, p1, Lcom/twitter/library/api/TweetMediaFeature;->w:I

    goto :goto_2

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    move-object v0, v1

    goto :goto_1

    :cond_4
    return-void

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/MediaTag;
    .locals 10

    const-wide/16 v6, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    move-object v1, v4

    move-wide v2, v6

    move-object v5, v0

    move-object v0, v4

    :goto_0
    if-eqz v5, :cond_1

    sget-object v8, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v5, v8, :cond_1

    sget-object v8, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v5}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v5

    aget v5, v8, v5

    packed-switch v5, :pswitch_data_0

    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v5

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v8

    const/4 v5, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v9

    sparse-switch v9, :sswitch_data_0

    :cond_0
    :goto_2
    packed-switch v5, :pswitch_data_1

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->n()J

    move-result-wide v2

    goto :goto_1

    :sswitch_0
    const-string/jumbo v9, "user_id"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v5, 0x0

    goto :goto_2

    :sswitch_1
    const-string/jumbo v9, "name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v5, 0x1

    goto :goto_2

    :sswitch_2
    const-string/jumbo v9, "screen_name"

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/4 v5, 0x2

    goto :goto_2

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v1

    goto :goto_1

    :pswitch_3
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    cmp-long v5, v2, v6

    if-lez v5, :cond_2

    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    new-instance v4, Lcom/twitter/library/api/MediaTag;

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v2, v3, v1, v0}, Lcom/twitter/library/api/MediaTag;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    move-object v0, v4

    :goto_3
    return-object v0

    :cond_2
    move-object v0, v4

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        -0x8c511f1 -> :sswitch_0
        -0x2942982 -> :sswitch_2
        0x337a8b -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private static b(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->d()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "all"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "large"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    :cond_1
    invoke-static {p0, p1}, Lcom/twitter/library/api/TweetEntities;->c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V

    goto :goto_1

    :cond_2
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_3
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static c(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V
    .locals 2

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_2

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->c:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_2

    sget-object v1, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :goto_1
    :pswitch_0
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->f()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "faces"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {p0, p1}, Lcom/twitter/library/api/TweetEntities;->e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V

    goto :goto_1

    :cond_0
    const-string/jumbo v1, "tags"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {p0, p1}, Lcom/twitter/library/api/TweetEntities;->d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_1

    :cond_2
    return-void

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static d(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V
    .locals 3

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    :goto_1
    if-eqz v0, :cond_3

    sget-object v2, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v2, :cond_3

    sget-object v2, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v2, v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    :pswitch_0
    invoke-static {p0}, Lcom/twitter/library/api/TweetEntities;->b(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/MediaTag;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->b()Lcom/fasterxml/jackson/core/JsonParser;

    goto :goto_2

    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p1, v1}, Lcom/twitter/library/api/MediaEntity;->a(Ljava/util/List;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static e(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/MediaEntity;)V
    .locals 7

    const/4 v2, -0x1

    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->d:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    :goto_1
    if-eqz v0, :cond_3

    sget-object v1, Lcom/fasterxml/jackson/core/JsonToken;->e:Lcom/fasterxml/jackson/core/JsonToken;

    if-eq v0, v1, :cond_3

    sget-object v1, Lcom/twitter/library/api/ac;->a:[I

    invoke-virtual {v0}, Lcom/fasterxml/jackson/core/JsonToken;->ordinal()I

    move-result v0

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_2
    invoke-virtual {p0}, Lcom/fasterxml/jackson/core/JsonParser;->a()Lcom/fasterxml/jackson/core/JsonToken;

    move-result-object v0

    goto :goto_1

    :pswitch_0
    new-instance v0, Lcom/twitter/library/api/TweetMediaFeature;

    const-string/jumbo v1, "faces"

    move v3, v2

    move v4, v2

    move v5, v2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/library/api/TweetMediaFeature;-><init>(Ljava/lang/String;IIII)V

    invoke-static {p0, v0}, Lcom/twitter/library/api/TweetEntities;->a(Lcom/fasterxml/jackson/core/JsonParser;Lcom/twitter/library/api/TweetMediaFeature;)V

    iget v1, v0, Lcom/twitter/library/api/TweetMediaFeature;->x:I

    if-ltz v1, :cond_2

    iget v1, v0, Lcom/twitter/library/api/TweetMediaFeature;->y:I

    if-ltz v1, :cond_2

    iget v1, v0, Lcom/twitter/library/api/TweetMediaFeature;->h:I

    if-ltz v1, :cond_2

    iget v1, v0, Lcom/twitter/library/api/TweetMediaFeature;->w:I

    if-ltz v1, :cond_2

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    const-string/jumbo v0, "large"

    invoke-virtual {p1, v0, v6}, Lcom/twitter/library/api/MediaEntity;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public a([B)Lcom/twitter/library/api/TweetEntities;
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    :cond_0
    return-object p0
.end method

.method public a(I)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget v0, v0, Lcom/twitter/library/api/MediaEntity;->type:I

    and-int/2addr v0, p1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(J)Z
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MentionEntity;

    iget-wide v2, v0, Lcom/twitter/library/api/MentionEntity;->userId:J

    cmp-long v0, v2, p1

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()[B
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method public b(I)Lcom/twitter/library/api/MediaEntity;
    .locals 3

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget v2, v0, Lcom/twitter/library/api/MediaEntity;->type:I

    and-int/2addr v2, p1

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(J)Lcom/twitter/library/api/MediaEntity;
    .locals 4

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget-wide v2, v0, Lcom/twitter/library/api/MediaEntity;->id:J

    cmp-long v2, v2, p1

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b([B)Lcom/twitter/library/api/TweetEntities;
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    :cond_0
    return-object p0
.end method

.method public b()[B
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method public c([B)Lcom/twitter/library/api/TweetEntities;
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    :cond_0
    return-object p0
.end method

.method public c()[B
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method public d([B)Lcom/twitter/library/api/TweetEntities;
    .locals 1

    if-eqz p1, :cond_0

    invoke-static {p1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    :cond_0
    return-object p0
.end method

.method public d()[B
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method public e()[B
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Ljava/lang/Object;)[B

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TweetEntities;

    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    :cond_4
    move v0, v1

    goto :goto_0

    :cond_5
    iget-object v2, p1, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-nez v2, :cond_4

    :cond_6
    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    :cond_7
    move v0, v1

    goto :goto_0

    :cond_8
    iget-object v2, p1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-nez v2, :cond_7

    :cond_9
    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    iget-object v3, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_a
    iget-object v2, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    goto :goto_1
.end method

.method public f()Z
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget-boolean v0, v0, Lcom/twitter/library/api/MediaEntity;->processed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    :goto_0
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public readExternal(Ljava/io/ObjectInput;)V
    .locals 1

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    :try_start_0
    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/io/ObjectInput;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeExternal(Ljava/io/ObjectOutput;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/library/api/TweetEntities;->cashtags:Ljava/util/ArrayList;

    invoke-interface {p1, v0}, Ljava/io/ObjectOutput;->writeObject(Ljava/lang/Object;)V

    return-void
.end method
