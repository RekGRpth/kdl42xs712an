.class public Lcom/twitter/library/api/TwitterSocialProof;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public final b:I

.field public final c:I

.field public final d:I

.field public final e:Ljava/lang/String;

.field public final f:I

.field public final g:Ljava/lang/String;

.field public final h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/library/api/as;

    invoke-direct {v0}, Lcom/twitter/library/api/as;-><init>()V

    sput-object v0, Lcom/twitter/library/api/TwitterSocialProof;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;IIIILjava/lang/String;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/twitter/library/api/TwitterSocialProof;->a:I

    iput-object p2, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    iput p3, p0, Lcom/twitter/library/api/TwitterSocialProof;->b:I

    iput p4, p0, Lcom/twitter/library/api/TwitterSocialProof;->c:I

    iput p5, p0, Lcom/twitter/library/api/TwitterSocialProof;->d:I

    iput p6, p0, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    iput-object p7, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    iput p8, p0, Lcom/twitter/library/api/TwitterSocialProof;->h:I

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->a:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->b:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->c:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->d:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->h:I

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-ne p0, p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    if-eq v2, v3, :cond_3

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    check-cast p1, Lcom/twitter/library/api/TwitterSocialProof;

    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->b:I

    iget v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    goto :goto_0

    :cond_4
    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->d:I

    iget v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->d:I

    if-eq v2, v3, :cond_5

    move v0, v1

    goto :goto_0

    :cond_5
    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->c:I

    iget v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    goto :goto_0

    :cond_6
    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->a:I

    iget v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->a:I

    if-eq v2, v3, :cond_7

    move v0, v1

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    :cond_8
    move v0, v1

    goto :goto_0

    :cond_9
    iget-object v2, p1, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    if-nez v2, :cond_8

    :cond_a
    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    iget v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    if-eq v2, v3, :cond_b

    move v0, v1

    goto :goto_0

    :cond_b
    iget-object v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    if-eqz v2, :cond_d

    iget-object v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_e

    :cond_c
    move v0, v1

    goto :goto_0

    :cond_d
    iget-object v2, p1, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    :cond_e
    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->h:I

    iget v3, p1, Lcom/twitter/library/api/TwitterSocialProof;->h:I

    if-eq v2, v3, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 3

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->a:I

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->b:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->c:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->d:I

    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_0
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_0
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/TwitterSocialProof;->h:I

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->a:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->e:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->b:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->c:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->d:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->f:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->g:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/twitter/library/api/TwitterSocialProof;->h:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
