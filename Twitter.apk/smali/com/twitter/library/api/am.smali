.class public Lcom/twitter/library/api/am;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/api/k;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;

.field public final d:I

.field public final e:I

.field public final f:I

.field public final g:Lcom/twitter/library/api/TwitterUser;

.field public final h:I

.field private final i:J

.field private volatile j:I


# direct methods
.method public constructor <init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IIILcom/twitter/library/api/TwitterUser;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/library/api/am;->i:J

    iput-object p3, p0, Lcom/twitter/library/api/am;->a:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/library/api/am;->b:Ljava/lang/String;

    iput-object p5, p0, Lcom/twitter/library/api/am;->c:Ljava/lang/String;

    iput p6, p0, Lcom/twitter/library/api/am;->d:I

    iput p7, p0, Lcom/twitter/library/api/am;->e:I

    iput p8, p0, Lcom/twitter/library/api/am;->f:I

    iput-object p9, p0, Lcom/twitter/library/api/am;->g:Lcom/twitter/library/api/TwitterUser;

    iput p10, p0, Lcom/twitter/library/api/am;->h:I

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/am;->i:J

    return-wide v0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    iget-wide v0, p0, Lcom/twitter/library/api/am;->i:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 6

    const/4 v1, 0x0

    iget v0, p0, Lcom/twitter/library/api/am;->j:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/library/api/am;->j:I

    :goto_0
    return v0

    :cond_0
    iget-wide v2, p0, Lcom/twitter/library/api/am;->i:J

    iget-wide v4, p0, Lcom/twitter/library/api/am;->i:J

    const/16 v0, 0x20

    ushr-long/2addr v4, v0

    xor-long/2addr v2, v4

    long-to-int v0, v2

    add-int/lit16 v0, v0, 0x20f

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/am;->a:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/library/api/am;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_1
    add-int/2addr v0, v2

    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/twitter/library/api/am;->b:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/library/api/am;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    :goto_2
    add-int/2addr v0, v2

    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/twitter/library/api/am;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v1, p0, Lcom/twitter/library/api/am;->c:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :cond_1
    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/am;->h:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/am;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/am;->e:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/am;->d:I

    add-int/2addr v0, v1

    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/twitter/library/api/am;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/twitter/library/api/am;->j:I

    iget v0, p0, Lcom/twitter/library/api/am;->j:I

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method
