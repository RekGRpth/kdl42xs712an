.class public abstract Lcom/twitter/library/api/geo/a;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field protected d:Ljava/util/List;

.field private e:Ljava/lang/String;

.field private f:Ljava/lang/String;

.field private g:I

.field private n:Z

.field private o:Z


# direct methods
.method protected constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/library/api/geo/a;->d:Ljava/util/List;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/api/geo/a;
    .locals 0

    iput p1, p0, Lcom/twitter/library/api/geo/a;->g:I

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/geo/a;->e:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lcom/twitter/library/api/geo/a;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/geo/a;->n:Z

    return-object p0
.end method

.method protected a(Ljava/lang/StringBuilder;)Ljava/lang/StringBuilder;
    .locals 6

    const-wide/high16 v4, 0x7ff8000000000000L    # NaN

    iget-object v0, p0, Lcom/twitter/library/api/geo/a;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "lat"

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v1

    const-string/jumbo v3, "long"

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getDouble(Ljava/lang/String;D)D

    move-result-wide v3

    invoke-static {v1, v2}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3, v4}, Ljava/lang/Double;->isNaN(D)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "lat"

    invoke-static {v1, v2}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "long"

    invoke-static {v3, v4}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/library/api/geo/a;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "accuracy"

    invoke-static {p1, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/library/api/geo/a;->g()I

    move-result v0

    if-lez v0, :cond_2

    const-string/jumbo v1, "max_results"

    invoke-static {p1, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/library/api/geo/a;->f()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string/jumbo v1, "granularity"

    invoke-static {p1, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/library/api/geo/a;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "use_geoduck"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_4
    return-object p1
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;
    .locals 0

    iput-object p1, p0, Lcom/twitter/library/api/geo/a;->f:Ljava/lang/String;

    return-object p0
.end method

.method public b(Z)Lcom/twitter/library/api/geo/a;
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/library/api/geo/a;->o:Z

    return-object p0
.end method

.method public e()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/a;->f:Ljava/lang/String;

    return-object v0
.end method

.method public g()I
    .locals 1

    iget v0, p0, Lcom/twitter/library/api/geo/a;->g:I

    return v0
.end method

.method public h()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/twitter/library/api/geo/a;->d:Ljava/util/List;

    return-object v0
.end method

.method protected i()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/geo/a;->n:Z

    return v0
.end method

.method public j()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/library/api/geo/a;->o:Z

    return v0
.end method
