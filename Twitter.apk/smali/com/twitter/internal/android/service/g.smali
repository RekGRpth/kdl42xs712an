.class Lcom/twitter/internal/android/service/g;
.super Ljava/util/concurrent/ThreadPoolExecutor;
.source "Twttr"


# direct methods
.method public constructor <init>(I)V
    .locals 7

    const v2, 0x7fffffff

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/PriorityBlockingQueue;

    const/16 v0, 0xb

    new-instance v1, Lcom/twitter/internal/android/service/e;

    invoke-direct {v1}, Lcom/twitter/internal/android/service/e;-><init>()V

    invoke-direct {v6, v0, v1}, Ljava/util/concurrent/PriorityBlockingQueue;-><init>(ILjava/util/Comparator;)V

    move-object v0, p0

    move v1, p1

    invoke-direct/range {v0 .. v6}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;)V

    return-void
.end method


# virtual methods
.method protected newTaskFor(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/RunnableFuture;
    .locals 1

    new-instance v0, Lcom/twitter/internal/android/service/f;

    invoke-direct {v0, p1}, Lcom/twitter/internal/android/service/f;-><init>(Ljava/util/concurrent/Callable;)V

    return-object v0
.end method
