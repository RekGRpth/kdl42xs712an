.class Lcom/twitter/internal/android/service/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/service/AsyncService;

.field private final b:Ljava/util/concurrent/Callable;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/service/AsyncService;Ljava/util/concurrent/Callable;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/service/h;->a:Lcom/twitter/internal/android/service/AsyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/internal/android/service/h;->b:Ljava/util/concurrent/Callable;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/service/h;->a:Lcom/twitter/internal/android/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/internal/android/service/AsyncService;->a(Lcom/twitter/internal/android/service/AsyncService;)Lcom/twitter/internal/android/service/g;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/service/h;->b:Ljava/util/concurrent/Callable;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/service/g;->submit(Ljava/util/concurrent/Callable;)Ljava/util/concurrent/Future;

    return-void
.end method
