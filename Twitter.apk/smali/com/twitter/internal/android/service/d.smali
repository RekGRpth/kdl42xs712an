.class Lcom/twitter/internal/android/service/d;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field public final a:I

.field public final b:J

.field final synthetic c:Lcom/twitter/internal/android/service/AsyncService;

.field private final d:Lcom/twitter/internal/android/service/a;

.field private final e:Lcom/twitter/internal/android/service/k;


# direct methods
.method public constructor <init>(Lcom/twitter/internal/android/service/AsyncService;Lcom/twitter/internal/android/service/a;Lcom/twitter/internal/android/service/k;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/internal/android/service/d;->d:Lcom/twitter/internal/android/service/a;

    iput-object p3, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    invoke-virtual {p2}, Lcom/twitter/internal/android/service/a;->C_()I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/service/d;->a:I

    sget-object v0, Lcom/twitter/internal/android/service/AsyncService;->a:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/internal/android/service/d;->b:J

    return-void
.end method


# virtual methods
.method public a()Lcom/twitter/internal/android/service/k;
    .locals 8

    const/16 v2, 0xa

    invoke-static {}, Landroid/os/Process;->myTid()I

    move-result v0

    invoke-static {v0}, Landroid/os/Process;->getThreadPriority(I)I

    move-result v1

    if-eq v1, v2, :cond_0

    invoke-static {v0, v2}, Landroid/os/Process;->setThreadPriority(II)V

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/twitter/internal/android/service/d;->d:Lcom/twitter/internal/android/service/a;

    iget-object v1, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/service/a;->c(Lcom/twitter/internal/android/service/AsyncService;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/service/k;->a(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/internal/android/service/d;->d:Lcom/twitter/internal/android/service/a;

    iget-object v0, v0, Lcom/twitter/internal/android/service/a;->c:Lcom/twitter/internal/android/service/l;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/service/l;->a(Lcom/twitter/internal/android/service/k;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    invoke-static {v1}, Lcom/twitter/internal/android/service/AsyncService;->b(Lcom/twitter/internal/android/service/AsyncService;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/twitter/internal/android/service/h;

    iget-object v3, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    new-instance v4, Lcom/twitter/internal/android/service/d;

    iget-object v5, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    iget-object v6, p0, Lcom/twitter/internal/android/service/d;->d:Lcom/twitter/internal/android/service/a;

    iget-object v7, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/internal/android/service/d;-><init>(Lcom/twitter/internal/android/service/AsyncService;Lcom/twitter/internal/android/service/a;Lcom/twitter/internal/android/service/k;)V

    invoke-direct {v2, v3, v4}, Lcom/twitter/internal/android/service/h;-><init>(Lcom/twitter/internal/android/service/AsyncService;Ljava/util/concurrent/Callable;)V

    iget-object v3, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/service/l;->b(Lcom/twitter/internal/android/service/k;)J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    invoke-static {v1}, Lcom/twitter/internal/android/service/AsyncService;->b(Lcom/twitter/internal/android/service/AsyncService;)Landroid/os/Handler;

    move-result-object v1

    new-instance v2, Lcom/twitter/internal/android/service/c;

    invoke-direct {v2, v0}, Lcom/twitter/internal/android/service/c;-><init>(Ljava/lang/Exception;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/service/d;->d:Lcom/twitter/internal/android/service/a;

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/a;->D_()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/service/d;->c:Lcom/twitter/internal/android/service/AsyncService;

    invoke-static {v0}, Lcom/twitter/internal/android/service/AsyncService;->b(Lcom/twitter/internal/android/service/AsyncService;)Landroid/os/Handler;

    move-result-object v0

    new-instance v1, Lcom/twitter/internal/android/service/b;

    iget-object v2, p0, Lcom/twitter/internal/android/service/d;->d:Lcom/twitter/internal/android/service/a;

    iget-object v3, p0, Lcom/twitter/internal/android/service/d;->e:Lcom/twitter/internal/android/service/k;

    invoke-direct {v1, v2, v3}, Lcom/twitter/internal/android/service/b;-><init>(Lcom/twitter/internal/android/service/a;Lcom/twitter/internal/android/service/k;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public synthetic call()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/service/d;->a()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    return-object v0
.end method
