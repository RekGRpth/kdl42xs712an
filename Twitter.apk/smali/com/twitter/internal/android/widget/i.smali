.class Lcom/twitter/internal/android/widget/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/DockLayout;

.field private b:I

.field private c:J

.field private d:J

.field private e:I

.field private f:I

.field private g:I

.field private h:I


# direct methods
.method private constructor <init>(Lcom/twitter/internal/android/widget/DockLayout;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/internal/android/widget/DockLayout;Lcom/twitter/internal/android/widget/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/i;-><init>(Lcom/twitter/internal/android/widget/DockLayout;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    :cond_0
    return-void
.end method

.method public a(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    if-nez v0, :cond_2

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/twitter/internal/android/widget/i;->c:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/internal/android/widget/i;->d:J

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->d(Lcom/twitter/internal/android/widget/DockLayout;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->e(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->e:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->e(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->f:I

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->f(Lcom/twitter/internal/android/widget/DockLayout;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->g(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->g:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->g(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    neg-int v0, v0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->h:I

    :cond_1
    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/DockLayout;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public b()V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    goto :goto_0
.end method

.method public b(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    if-nez v0, :cond_2

    int-to-long v0, p1

    iput-wide v0, p0, Lcom/twitter/internal/android/widget/i;->c:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/internal/android/widget/i;->d:J

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->d(Lcom/twitter/internal/android/widget/DockLayout;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->e(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->e:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->e(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    rsub-int v0, v0, 0x2710

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->f:I

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->f(Lcom/twitter/internal/android/widget/DockLayout;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->g(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->g:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->g(Lcom/twitter/internal/android/widget/DockLayout;)I

    move-result v0

    rsub-int v0, v0, 0x2710

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->h:I

    :cond_1
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/DockLayout;->post(Ljava/lang/Runnable;)Z

    :cond_2
    return-void
.end method

.method public run()V
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v4, p0, Lcom/twitter/internal/android/widget/i;->d:J

    sub-long v4, v0, v4

    iget-wide v0, p0, Lcom/twitter/internal/android/widget/i;->c:J

    cmp-long v0, v4, v0

    if-lez v0, :cond_4

    move v1, v2

    :goto_0
    if-eqz v1, :cond_5

    const/high16 v0, 0x3f800000    # 1.0f

    :goto_1
    iget-object v4, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v4}, Lcom/twitter/internal/android/widget/DockLayout;->a(Lcom/twitter/internal/android/widget/DockLayout;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    iget v5, p0, Lcom/twitter/internal/android/widget/i;->e:I

    iget v6, p0, Lcom/twitter/internal/android/widget/i;->f:I

    int-to-float v6, v6

    mul-float/2addr v6, v0

    float-to-int v6, v6

    add-int/2addr v5, v6

    invoke-static {v4, v5}, Lcom/twitter/internal/android/widget/DockLayout;->a(Lcom/twitter/internal/android/widget/DockLayout;I)I

    :cond_0
    iget-object v4, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v4}, Lcom/twitter/internal/android/widget/DockLayout;->b(Lcom/twitter/internal/android/widget/DockLayout;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    iget v5, p0, Lcom/twitter/internal/android/widget/i;->g:I

    iget v6, p0, Lcom/twitter/internal/android/widget/i;->h:I

    int-to-float v6, v6

    mul-float/2addr v0, v6

    float-to-int v0, v0

    add-int/2addr v0, v5

    invoke-static {v4, v0}, Lcom/twitter/internal/android/widget/DockLayout;->b(Lcom/twitter/internal/android/widget/DockLayout;I)I

    :cond_1
    if-nez v1, :cond_2

    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_7

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_6

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setTopDocked(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setBottomDocked(Z)V

    :cond_3
    :goto_2
    iput v3, p0, Lcom/twitter/internal/android/widget/i;->b:I

    :goto_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/DockLayout;->c(Lcom/twitter/internal/android/widget/DockLayout;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DockLayout;->invalidate()V

    return-void

    :cond_4
    move v1, v3

    goto :goto_0

    :cond_5
    long-to-float v0, v4

    iget-wide v4, p0, Lcom/twitter/internal/android/widget/i;->c:J

    long-to-float v4, v4

    div-float/2addr v0, v4

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/twitter/internal/android/widget/i;->b:I

    if-ne v0, v2, :cond_3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/DockLayout;->setTopDocked(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/DockLayout;->setBottomDocked(Z)V

    goto :goto_2

    :cond_7
    iget-object v0, p0, Lcom/twitter/internal/android/widget/i;->a:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/DockLayout;->post(Ljava/lang/Runnable;)Z

    goto :goto_3
.end method
