.class public Lcom/twitter/internal/android/widget/RoundedGroupedRowView;
.super Landroid/view/ViewGroup;
.source "Twttr"


# static fields
.field private static final a:Landroid/graphics/Paint;

.field private static final b:Landroid/graphics/Paint;


# instance fields
.field private final c:F

.field private final d:I

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:F

.field private final k:I

.field private final l:I

.field private m:I

.field private n:I

.field private o:Landroid/graphics/drawable/shapes/RectShape;

.field private p:Landroid/graphics/drawable/shapes/RectShape;

.field private q:Landroid/graphics/RectF;

.field private r:Landroid/graphics/RectF;

.field private s:Landroid/graphics/RectF;

.field private t:Z

.field private u:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a:Landroid/graphics/Paint;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    sput-object v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->b:Landroid/graphics/Paint;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->groupedRowViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9

    const/16 v8, 0xf

    const v4, -0xbbbbbc

    const/4 v7, 0x0

    const/4 v6, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    sget-object v0, Lcom/twitter/internal/android/f;->RoundedGroupedRowView:[I

    invoke-virtual {p1, p2, v0, p3, v6}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    invoke-virtual {v0, v6, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    const/4 v1, 0x1

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->n:I

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->c:F

    const/16 v1, 0x10

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->h:I

    const/16 v1, 0x11

    const v2, -0x333334

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->i:I

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->l:I

    const/16 v1, 0xe

    invoke-virtual {v0, v1, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->j:F

    sget-object v1, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a:Landroid/graphics/Paint;

    const/4 v2, 0x5

    invoke-virtual {v0, v2, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    cmpl-float v3, v2, v7

    if-lez v3, :cond_2

    const/4 v3, 0x2

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    const/4 v4, 0x3

    invoke-virtual {v0, v4, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v4

    const/4 v5, 0x4

    invoke-virtual {v0, v5, v7}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v5

    invoke-virtual {v1, v2, v4, v5, v3}, Landroid/graphics/Paint;->setShadowLayer(FFFI)V

    iget v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->j:F

    cmpg-float v1, v1, v7

    if-gtz v1, :cond_1

    iput v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->k:I

    :goto_0
    const/4 v1, 0x7

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    const/16 v2, 0x8

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->d:I

    const/16 v2, 0x9

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->e:I

    const/16 v2, 0xa

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    iput v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->f:I

    const/16 v2, 0xb

    invoke-virtual {v0, v2, v1}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->g:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x106000d    # android.R.color.transparent

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->setBackgroundColor(I)V

    :cond_0
    const/4 v1, 0x6

    invoke-virtual {v0, v1, v6}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->t:Z

    invoke-super {p0, v6, v6, v6, v6}, Landroid/view/ViewGroup;->setPadding(IIII)V

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_1
    invoke-virtual {v0, v8, v3}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->k:I

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v8, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v1

    iput v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->k:I

    goto :goto_0
.end method

.method private static a(Landroid/graphics/RectF;FF)V
    .locals 4

    iget v0, p0, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, p1

    iget v1, p0, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, p2

    iget v2, p0, Landroid/graphics/RectF;->right:F

    add-float/2addr v2, p1

    iget v3, p0, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v3, p2

    invoke-virtual {p0, v0, v1, v2, v3}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method private a([FLandroid/graphics/RectF;)V
    .locals 3

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/drawable/shapes/RoundRectShape;

    invoke-direct {v0, p1, v1, v1}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    invoke-virtual {p2}, Landroid/graphics/RectF;->width()F

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/shapes/RoundRectShape;->resize(FF)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->p:Landroid/graphics/drawable/shapes/RectShape;

    :cond_0
    return-void
.end method


# virtual methods
.method public addView(Landroid/view/View;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getChildCount()I

    move-result v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->addView(Landroid/view/View;I)V

    return-void
.end method

.method public addView(Landroid/view/View;I)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "RoundedGroupedRowView can only hold a single child view."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/view/ViewGroup;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getChildCount()I

    move-result v0

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 8

    const/4 v5, 0x0

    const/4 v7, 0x1

    iget-object v6, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    iget v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->t:Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getRight()I

    move-result v3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getBottom()I

    move-result v4

    invoke-virtual {v2, v5, v5, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    if-nez v0, :cond_1

    if-eqz v1, :cond_2

    :cond_1
    if-nez v6, :cond_3

    :cond_2
    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    :goto_0
    return-void

    :cond_3
    sget-object v5, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a:Landroid/graphics/Paint;

    iget v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->l:I

    invoke-virtual {v5, v2}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->s:Landroid/graphics/RectF;

    invoke-virtual {p1, v2, v5}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->save(I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    iget v3, v3, Landroid/graphics/RectF;->left:F

    iget-object v4, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    iget v4, v4, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v3, v4}, Landroid/graphics/Canvas;->translate(FF)V

    iget v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->k:I

    invoke-virtual {v5, v3}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->o:Landroid/graphics/drawable/shapes/RectShape;

    invoke-virtual {v3, p1, v5}, Landroid/graphics/drawable/shapes/RectShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    iget-boolean v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->u:Z

    if-nez v2, :cond_5

    if-nez v1, :cond_5

    if-eq v0, v7, :cond_4

    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    :cond_4
    iget v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->i:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    iget v1, v6, Landroid/graphics/RectF;->left:F

    iget v2, v6, Landroid/graphics/RectF;->bottom:F

    iget v3, v6, Landroid/graphics/RectF;->right:F

    iget v0, v6, Landroid/graphics/RectF;->bottom:F

    iget v4, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->h:I

    int-to-float v4, v4

    add-float/2addr v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :cond_5
    iget-object v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->p:Landroid/graphics/drawable/shapes/RectShape;

    if-eqz v0, :cond_6

    sget-object v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->b:Landroid/graphics/Paint;

    const/16 v1, 0x14

    invoke-virtual {p1, v6, v0, v1}, Landroid/graphics/Canvas;->saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;I)I

    move-result v1

    invoke-virtual {p1, v7}, Landroid/graphics/Canvas;->save(I)I

    iget v2, v6, Landroid/graphics/RectF;->left:F

    iget v3, v6, Landroid/graphics/RectF;->top:F

    invoke-virtual {p1, v2, v3}, Landroid/graphics/Canvas;->translate(FF)V

    iget-object v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->p:Landroid/graphics/drawable/shapes/RectShape;

    invoke-virtual {v2, p1, v0}, Landroid/graphics/drawable/shapes/RectShape;->draw(Landroid/graphics/Canvas;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    sget-object v2, Lhk;->a:Landroid/graphics/PorterDuffXfermode;

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    const/16 v2, 0x10

    invoke-virtual {p1, v6, v0, v2}, Landroid/graphics/Canvas;->saveLayer(Landroid/graphics/RectF;Landroid/graphics/Paint;I)I

    move-result v2

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v2}, Landroid/graphics/Canvas;->restoreToCount(I)V

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->restoreToCount(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    goto :goto_0

    :cond_6
    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    move-result v0

    invoke-virtual {p1, v6}, Landroid/graphics/Canvas;->clipRect(Landroid/graphics/RectF;)Z

    invoke-super {p0, p1}, Landroid/view/ViewGroup;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->restoreToCount(I)V

    goto/16 :goto_0
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    new-instance v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {v0, p1}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 7

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v2, 0x8

    if-ne v0, v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iget v3, v2, Landroid/graphics/RectF;->left:F

    float-to-int v3, v3

    iget v4, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v3, v4

    iget v4, v2, Landroid/graphics/RectF;->top:F

    float-to-int v4, v4

    iget v5, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v4, v5

    iget v5, v2, Landroid/graphics/RectF;->right:F

    float-to-int v5, v5

    iget v6, v0, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    sub-int/2addr v5, v6

    iget v2, v2, Landroid/graphics/RectF;->bottom:F

    float-to-int v2, v2

    iget v0, v0, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    sub-int v0, v2, v0

    invoke-virtual {v1, v3, v4, v5, v0}, Landroid/view/View;->layout(IIII)V

    :cond_2
    iget v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->t:Z

    iget v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->c:F

    if-eqz v1, :cond_4

    iget-object v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_3

    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v2, v0, v1

    const/4 v1, 0x4

    aput v2, v0, v1

    const/4 v1, 0x5

    aput v2, v0, v1

    const/4 v1, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v2, v0, v1

    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-direct {v1, v0, v2, v5}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    :goto_1
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v1, v2, v4}, Landroid/graphics/drawable/shapes/RectShape;->resize(FF)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->o:Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {p0, v0, v3}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a([FLandroid/graphics/RectF;)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    goto :goto_1

    :cond_4
    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    iget-object v4, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_5

    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    aput v2, v0, v1

    const/4 v1, 0x1

    aput v2, v0, v1

    const/4 v1, 0x2

    aput v2, v0, v1

    const/4 v1, 0x3

    aput v2, v0, v1

    const/4 v1, 0x4

    const/4 v2, 0x0

    aput v2, v0, v1

    const/4 v1, 0x5

    const/4 v2, 0x0

    aput v2, v0, v1

    const/4 v1, 0x6

    const/4 v2, 0x0

    aput v2, v0, v1

    const/4 v1, 0x7

    const/4 v2, 0x0

    aput v2, v0, v1

    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/4 v2, 0x0

    const/4 v5, 0x0

    invoke-direct {v1, v0, v2, v5}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    :goto_2
    invoke-virtual {v4}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v4}, Landroid/graphics/RectF;->height()F

    move-result v4

    invoke-virtual {v1, v2, v4}, Landroid/graphics/drawable/shapes/RectShape;->resize(FF)V

    iget v2, v3, Landroid/graphics/RectF;->left:F

    iget v4, v3, Landroid/graphics/RectF;->top:F

    iget v5, v3, Landroid/graphics/RectF;->right:F

    iget v6, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->o:Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {p0, v0, v3}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a([FLandroid/graphics/RectF;)V

    goto/16 :goto_0

    :cond_5
    const/4 v0, 0x0

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    goto :goto_2

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v2

    invoke-virtual {v0}, Landroid/graphics/RectF;->height()F

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/graphics/drawable/shapes/RectShape;->resize(FF)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->o:Landroid/graphics/drawable/shapes/RectShape;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->p:Landroid/graphics/drawable/shapes/RectShape;

    goto/16 :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    const/4 v0, 0x0

    cmpl-float v0, v2, v0

    if-lez v0, :cond_6

    const/16 v0, 0x8

    new-array v0, v0, [F

    const/4 v1, 0x0

    const/4 v4, 0x0

    aput v4, v0, v1

    const/4 v1, 0x1

    const/4 v4, 0x0

    aput v4, v0, v1

    const/4 v1, 0x2

    const/4 v4, 0x0

    aput v4, v0, v1

    const/4 v1, 0x3

    const/4 v4, 0x0

    aput v4, v0, v1

    const/4 v1, 0x4

    aput v2, v0, v1

    const/4 v1, 0x5

    aput v2, v0, v1

    const/4 v1, 0x6

    aput v2, v0, v1

    const/4 v1, 0x7

    aput v2, v0, v1

    new-instance v1, Landroid/graphics/drawable/shapes/RoundRectShape;

    const/4 v2, 0x0

    const/4 v4, 0x0

    invoke-direct {v1, v0, v2, v4}, Landroid/graphics/drawable/shapes/RoundRectShape;-><init>([FLandroid/graphics/RectF;[F)V

    :goto_3
    iget-object v2, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    invoke-virtual {v2}, Landroid/graphics/RectF;->width()F

    move-result v4

    invoke-virtual {v2}, Landroid/graphics/RectF;->height()F

    move-result v2

    invoke-virtual {v1, v4, v2}, Landroid/graphics/drawable/shapes/RectShape;->resize(FF)V

    iget v2, v3, Landroid/graphics/RectF;->left:F

    iget v4, v3, Landroid/graphics/RectF;->top:F

    iget v5, v3, Landroid/graphics/RectF;->right:F

    iget v6, v3, Landroid/graphics/RectF;->bottom:F

    invoke-virtual {v3, v2, v4, v5, v6}, Landroid/graphics/RectF;->set(FFFF)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->o:Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {p0, v0, v3}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a([FLandroid/graphics/RectF;)V

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    new-instance v1, Landroid/graphics/drawable/shapes/RectShape;

    invoke-direct {v1}, Landroid/graphics/drawable/shapes/RectShape;-><init>()V

    goto :goto_3

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected onMeasure(II)V
    .locals 16

    const/4 v1, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v1

    const/16 v3, 0x8

    if-ne v1, v3, :cond_1

    :cond_0
    const/4 v1, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->setMeasuredDimension(II)V

    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->t:Z

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    move-object v7, v1

    check-cast v7, Landroid/view/ViewGroup$MarginLayoutParams;

    if-nez v12, :cond_2

    if-nez v13, :cond_2

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v3, p1

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    add-int/2addr v1, v3

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->rightMargin:I

    add-int/2addr v1, v3

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v2, v3

    iget v3, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int/2addr v2, v3

    new-instance v3, Landroid/graphics/RectF;

    const/4 v4, 0x0

    const/4 v5, 0x0

    int-to-float v6, v1

    int-to-float v7, v2

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->setMeasuredDimension(II)V

    goto :goto_0

    :cond_2
    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->e:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->g:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->d:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->f:I

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->u:Z

    if-eqz v1, :cond_4

    const/4 v1, 0x0

    move v8, v1

    :goto_1
    add-int v4, v14, v15

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v5

    invoke-static/range {p1 .. p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v1

    const/high16 v3, 0x40000000    # 2.0f

    if-ne v5, v3, :cond_5

    move v9, v1

    :goto_2
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getPaddingLeft()I

    move-result v3

    add-int/2addr v3, v4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getPaddingRight()I

    move-result v4

    add-int/2addr v3, v4

    iget v1, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    move/from16 v0, p1

    invoke-static {v0, v3, v1}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getChildMeasureSpec(III)I

    move-result v3

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object/from16 v1, p0

    move/from16 v5, p2

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->measureChildWithMargins(Landroid/view/View;IIII)V

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v1

    iget v2, v7, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    add-int/2addr v1, v2

    iget v2, v7, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    add-int v3, v1, v2

    sub-int v4, v9, v15

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->j:F

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->n:I

    if-eqz v13, :cond_8

    packed-switch v12, :pswitch_data_0

    :cond_3
    move v1, v11

    :goto_3
    add-int v2, v10, v1

    add-int/2addr v2, v3

    new-instance v3, Landroid/graphics/RectF;

    int-to-float v6, v14

    int-to-float v7, v10

    int-to-float v8, v4

    sub-int v1, v2, v1

    int-to-float v1, v1

    invoke-direct {v3, v6, v7, v8, v1}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1, v3}, Landroid/graphics/RectF;-><init>(Landroid/graphics/RectF;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    invoke-static {v1, v5, v5}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    move v1, v2

    :goto_4
    new-instance v2, Landroid/graphics/RectF;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    iget v5, v5, Landroid/graphics/RectF;->bottom:F

    int-to-float v4, v4

    int-to-float v6, v1

    invoke-direct {v2, v3, v5, v4, v6}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->s:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v1}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->setMeasuredDimension(II)V

    goto/16 :goto_0

    :cond_4
    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->h:I

    move v8, v1

    goto/16 :goto_1

    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->getSuggestedMinimumWidth()I

    move-result v3

    const/high16 v6, -0x80000000

    if-ne v5, v6, :cond_c

    invoke-static {v3, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v9, v1

    goto/16 :goto_2

    :pswitch_0
    const/4 v2, 0x2

    if-eq v1, v2, :cond_6

    const/4 v2, 0x3

    if-ne v1, v2, :cond_3

    :cond_6
    const/4 v1, 0x0

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v10

    move v1, v11

    goto :goto_3

    :pswitch_1
    const/4 v2, 0x2

    if-eq v1, v2, :cond_7

    const/4 v2, 0x3

    if-ne v1, v2, :cond_b

    :cond_7
    const/4 v1, 0x0

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    :goto_5
    div-int/lit8 v2, v11, 0x2

    move v10, v1

    move v1, v2

    goto :goto_3

    :pswitch_2
    div-int/lit8 v10, v10, 0x2

    div-int/lit8 v1, v11, 0x2

    goto :goto_3

    :pswitch_3
    div-int/lit8 v10, v10, 0x2

    move v1, v11

    goto/16 :goto_3

    :cond_8
    const/4 v2, 0x2

    if-eq v1, v2, :cond_9

    const/4 v2, 0x3

    if-ne v1, v2, :cond_a

    :cond_9
    const/4 v1, 0x0

    float-to-double v6, v5

    invoke-static {v6, v7}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v6

    double-to-int v2, v6

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v10

    :cond_a
    packed-switch v12, :pswitch_data_1

    move v1, v3

    goto :goto_4

    :pswitch_4
    add-int v1, v3, v10

    new-instance v2, Landroid/graphics/RectF;

    int-to-float v3, v14

    int-to-float v6, v10

    int-to-float v7, v4

    int-to-float v10, v1

    invoke-direct {v2, v3, v6, v7, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v3, Landroid/graphics/RectF;

    iget v6, v2, Landroid/graphics/RectF;->left:F

    iget v7, v2, Landroid/graphics/RectF;->top:F

    iget v10, v2, Landroid/graphics/RectF;->right:F

    iget v11, v2, Landroid/graphics/RectF;->bottom:F

    const/high16 v12, 0x41000000    # 8.0f

    add-float/2addr v11, v12

    invoke-direct {v3, v6, v7, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    invoke-static {v3, v5, v5}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    add-int/2addr v1, v8

    goto/16 :goto_4

    :pswitch_5
    new-instance v1, Landroid/graphics/RectF;

    int-to-float v2, v14

    const/4 v6, 0x0

    int-to-float v7, v4

    int-to-float v10, v3

    invoke-direct {v1, v2, v6, v7, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v2, Landroid/graphics/RectF;

    iget v6, v1, Landroid/graphics/RectF;->left:F

    iget v7, v1, Landroid/graphics/RectF;->top:F

    const/high16 v10, 0x41000000    # 8.0f

    sub-float/2addr v7, v10

    iget v10, v1, Landroid/graphics/RectF;->right:F

    iget v11, v1, Landroid/graphics/RectF;->bottom:F

    const/high16 v12, 0x41000000    # 8.0f

    add-float/2addr v11, v12

    invoke-direct {v2, v6, v7, v10, v11}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    invoke-static {v2, v5, v5}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    add-int v1, v3, v8

    goto/16 :goto_4

    :pswitch_6
    new-instance v1, Landroid/graphics/RectF;

    int-to-float v2, v14

    const/4 v6, 0x0

    int-to-float v7, v4

    int-to-float v8, v3

    invoke-direct {v1, v2, v6, v7, v8}, Landroid/graphics/RectF;-><init>(FFFF)V

    new-instance v2, Landroid/graphics/RectF;

    iget v6, v1, Landroid/graphics/RectF;->left:F

    iget v7, v1, Landroid/graphics/RectF;->top:F

    const/high16 v8, 0x41000000    # 8.0f

    sub-float/2addr v7, v8

    iget v8, v1, Landroid/graphics/RectF;->right:F

    iget v10, v1, Landroid/graphics/RectF;->bottom:F

    invoke-direct {v2, v6, v7, v8, v10}, Landroid/graphics/RectF;-><init>(FFFF)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->q:Landroid/graphics/RectF;

    invoke-static {v2, v5, v5}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->a(Landroid/graphics/RectF;FF)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->r:Landroid/graphics/RectF;

    add-int v1, v3, v11

    goto/16 :goto_4

    :cond_b
    move v1, v10

    goto/16 :goto_5

    :cond_c
    move v9, v3

    goto/16 :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setGroupStyle(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->n:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->n:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->requestLayout()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->invalidate()V

    goto :goto_0
.end method

.method public setPadding(IIII)V
    .locals 0

    return-void
.end method

.method public setSingle(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->t:Z

    if-eq p1, v0, :cond_1

    iput-boolean p1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->t:Z

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->requestLayout()V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->invalidate()V

    goto :goto_0
.end method

.method public setStyle(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    if-eq p1, v0, :cond_0

    iput p1, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->m:I

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->requestLayout()V

    :goto_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->u:Z

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/RoundedGroupedRowView;->invalidate()V

    goto :goto_0
.end method
