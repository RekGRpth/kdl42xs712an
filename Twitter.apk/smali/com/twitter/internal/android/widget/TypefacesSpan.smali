.class public Lcom/twitter/internal/android/widget/TypefacesSpan;
.super Landroid/text/style/StyleSpan;
.source "Twttr"


# instance fields
.field private final a:I

.field private final b:Lcom/twitter/internal/android/widget/ax;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 1

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/ax;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Landroid/text/style/StyleSpan;-><init>(I)V

    iput p2, p0, Lcom/twitter/internal/android/widget/TypefacesSpan;->a:I

    invoke-static {p1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/android/widget/TypefacesSpan;->b:Lcom/twitter/internal/android/widget/ax;

    return-void

    :cond_0
    move v0, p2

    goto :goto_0
.end method

.method private a(Landroid/graphics/Paint;)V
    .locals 3

    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/twitter/internal/android/widget/TypefacesSpan;->b:Lcom/twitter/internal/android/widget/ax;

    iget v2, p0, Lcom/twitter/internal/android/widget/TypefacesSpan;->a:I

    or-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/ax;->b(I)Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/graphics/Typeface;->getStyle()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/TypefacesSpan;->b:Lcom/twitter/internal/android/widget/ax;

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/ax;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/TypefacesSpan;->a(Landroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/text/style/StyleSpan;->updateDrawState(Landroid/text/TextPaint;)V

    goto :goto_0
.end method

.method public updateMeasureState(Landroid/text/TextPaint;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/TypefacesSpan;->b:Lcom/twitter/internal/android/widget/ax;

    iget-boolean v0, v0, Lcom/twitter/internal/android/widget/ax;->f:Z

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/internal/android/widget/TypefacesSpan;->a(Landroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Landroid/text/style/StyleSpan;->updateMeasureState(Landroid/text/TextPaint;)V

    goto :goto_0
.end method
