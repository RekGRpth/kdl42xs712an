.class final Lcom/twitter/internal/android/widget/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/twitter/internal/android/widget/HorizontalListView;


# direct methods
.method constructor <init>(Lcom/twitter/internal/android/widget/HorizontalListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Lcom/twitter/internal/android/widget/HorizontalListView;)I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Lcom/twitter/internal/android/widget/HorizontalListView;I)I

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->b(Lcom/twitter/internal/android/widget/HorizontalListView;)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v2}, Lcom/twitter/internal/android/widget/HorizontalListView;->c(Lcom/twitter/internal/android/widget/HorizontalListView;)I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->hasFocusable()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    iget-boolean v1, v1, Lcom/twitter/internal/android/widget/HorizontalListView;->f:Z

    if-nez v1, :cond_0

    invoke-virtual {v0, v3}, Landroid/view/View;->setPressed(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/HorizontalListView;->setPressed(Z)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->d(Lcom/twitter/internal/android/widget/HorizontalListView;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->refreshDrawableState()V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e(Lcom/twitter/internal/android/widget/HorizontalListView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-static {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->e(Lcom/twitter/internal/android/widget/HorizontalListView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getCurrent()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Landroid/graphics/drawable/TransitionDrawable;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/graphics/drawable/TransitionDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/TransitionDrawable;->resetTransition()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/s;->a:Lcom/twitter/internal/android/widget/HorizontalListView;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->a(Lcom/twitter/internal/android/widget/HorizontalListView;I)I

    :cond_1
    return-void
.end method
