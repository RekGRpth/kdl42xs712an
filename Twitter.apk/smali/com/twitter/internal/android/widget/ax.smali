.class public Lcom/twitter/internal/android/widget/ax;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static g:Lcom/twitter/internal/android/widget/ax;


# instance fields
.field public final a:Landroid/graphics/Typeface;

.field public final b:Landroid/graphics/Typeface;

.field public final c:Landroid/graphics/Typeface;

.field public final d:Landroid/graphics/Typeface;

.field public final e:Landroid/graphics/Typeface;

.field public final f:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xd
    .end annotation

    const/4 v3, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    const/16 v2, 0x140

    if-ge v1, v2, :cond_0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xd

    if-lt v1, v2, :cond_1

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->smallestScreenWidthDp:I

    const/16 v1, 0x258

    if-lt v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    invoke-virtual {p1}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v1

    const-string/jumbo v2, "fonts/light.ttf"

    invoke-static {v1, v2}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    const-string/jumbo v3, "fonts/lightItalic.ttf"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/internal/android/widget/ax;->b:Landroid/graphics/Typeface;

    const-string/jumbo v3, "fonts/bold.ttf"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    const-string/jumbo v3, "fonts/boldItalic.ttf"

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->createFromAsset(Landroid/content/res/AssetManager;Ljava/lang/String;)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ax;->d:Landroid/graphics/Typeface;

    iput-object v2, p0, Lcom/twitter/internal/android/widget/ax;->e:Landroid/graphics/Typeface;

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/ax;->f:Z

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ax;->b:Landroid/graphics/Typeface;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT_BOLD:Landroid/graphics/Typeface;

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-static {v1, v3}, Landroid/graphics/Typeface;->create(Landroid/graphics/Typeface;I)Landroid/graphics/Typeface;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ax;->d:Landroid/graphics/Typeface;

    sget-object v1, Landroid/graphics/Typeface;->SERIF:Landroid/graphics/Typeface;

    iput-object v1, p0, Lcom/twitter/internal/android/widget/ax;->e:Landroid/graphics/Typeface;

    goto :goto_1
.end method

.method public static declared-synchronized a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;
    .locals 2

    const-class v1, Lcom/twitter/internal/android/widget/ax;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/internal/android/widget/ax;->g:Lcom/twitter/internal/android/widget/ax;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/internal/android/widget/ax;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/ax;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/internal/android/widget/ax;->g:Lcom/twitter/internal/android/widget/ax;

    :cond_0
    sget-object v0, Lcom/twitter/internal/android/widget/ax;->g:Lcom/twitter/internal/android/widget/ax;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public a(I)Landroid/graphics/Typeface;
    .locals 1

    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_0

    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->d:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    :cond_0
    and-int/lit8 v0, p1, 0x1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    goto :goto_0

    :cond_1
    and-int/lit8 v0, p1, 0x2

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->b:Landroid/graphics/Typeface;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    goto :goto_0
.end method

.method public b(I)Landroid/graphics/Typeface;
    .locals 1

    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->d:Landroid/graphics/Typeface;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/ax;->b:Landroid/graphics/Typeface;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
