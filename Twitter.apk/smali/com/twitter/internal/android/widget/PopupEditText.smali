.class public Lcom/twitter/internal/android/widget/PopupEditText;
.super Landroid/widget/EditText;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/Filter$FilterListener;


# static fields
.field public static final a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field public static final b:Landroid/widget/Filterable;


# instance fields
.field c:Z

.field d:Z

.field private final e:Landroid/widget/PopupWindow;

.field private final f:Lcom/twitter/internal/android/widget/DropDownListView;

.field private final g:Lcom/twitter/internal/android/widget/ae;

.field private final h:I

.field private final i:Z

.field private final j:Z

.field private final k:Z

.field private final l:I

.field private final m:I

.field private n:Landroid/view/View$OnClickListener;

.field private o:Lcom/twitter/internal/android/widget/af;

.field private p:Landroid/widget/ListAdapter;

.field private q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

.field private r:Landroid/widget/Filterable;

.field private s:Landroid/database/DataSetObserver;

.field private t:J

.field private u:Lcom/twitter/internal/android/widget/e;

.field private v:Lcom/twitter/internal/android/widget/f;

.field private w:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/internal/android/widget/aa;

    invoke-direct {v0}, Lcom/twitter/internal/android/widget/aa;-><init>()V

    sput-object v0, Lcom/twitter/internal/android/widget/PopupEditText;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    new-instance v0, Lcom/twitter/internal/android/widget/ab;

    invoke-direct {v0}, Lcom/twitter/internal/android/widget/ab;-><init>()V

    sput-object v0, Lcom/twitter/internal/android/widget/PopupEditText;->b:Landroid/widget/Filterable;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/android/widget/PopupEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    sget v0, Lcom/twitter/internal/android/b;->popupEditTextStyle:I

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/internal/android/widget/PopupEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 5

    const/4 v4, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/EditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->c:Z

    iput-boolean v2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->d:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    sget-object v0, Lcom/twitter/internal/android/f;->PopupEditText:[I

    invoke-virtual {p1, p2, v0, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    invoke-virtual {v1, v2, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->l:I

    invoke-virtual {v1, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->m:I

    const/4 v0, 0x4

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->h:I

    const/4 v0, 0x5

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->i:Z

    const/4 v0, 0x3

    invoke-virtual {v1, v0, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->k:Z

    const/4 v0, 0x2

    invoke-virtual {v1, v0, v4}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->j:Z

    new-instance v2, Lcom/twitter/internal/android/widget/DropDownListView;

    const/4 v0, 0x0

    sget v3, Lcom/twitter/internal/android/b;->popupEditListStyle:I

    invoke-direct {v2, p1, v0, v3}, Lcom/twitter/internal/android/widget/DropDownListView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    invoke-virtual {v2, p0}, Lcom/twitter/internal/android/widget/DropDownListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->f:Lcom/twitter/internal/android/widget/DropDownListView;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v0, v3, :cond_0

    new-instance v0, Landroid/widget/PopupWindow;

    const v3, 0x10102ff    # android.R.attr.listPopupWindowStyle

    invoke-direct {v0, p1, p2, v3}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    :goto_0
    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Landroid/widget/PopupWindow;->setSoftInputMode(I)V

    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setOutsideTouchable(Z)V

    invoke-virtual {v0, v2}, Landroid/widget/PopupWindow;->setContentView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->e:Landroid/widget/PopupWindow;

    invoke-super {p0, p0}, Landroid/widget/EditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/twitter/internal/android/widget/ae;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2, p0}, Lcom/twitter/internal/android/widget/ae;-><init>(Landroid/os/Looper;Landroid/widget/Filter$FilterListener;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->g:Lcom/twitter/internal/android/widget/ae;

    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    return-void

    :cond_0
    new-instance v0, Landroid/widget/PopupWindow;

    invoke-direct {v0, p1, p2}, Landroid/widget/PopupWindow;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    goto :goto_0
.end method

.method private d()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getWindowVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->p:Landroid/widget/ListAdapter;

    invoke-interface {v0}, Landroid/widget/ListAdapter;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/PopupEditText;->d:Z

    goto :goto_0

    :cond_1
    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->k:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getWidth()I

    move-result v4

    :goto_1
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->j:Z

    if-eqz v1, :cond_2

    iget v2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->l:I

    iget v3, p0, Lcom/twitter/internal/android/widget/PopupEditText;->m:I

    move-object v1, p0

    invoke-virtual/range {v0 .. v5}, Landroid/widget/PopupWindow;->update(Landroid/view/View;IIII)V

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->f:Lcom/twitter/internal/android/widget/DropDownListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DropDownListView;->setSelectionAfterHeaderView()V

    iput-boolean v6, p0, Lcom/twitter/internal/android/widget/PopupEditText;->c:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getWidth()I

    move-result v4

    goto :goto_1

    :cond_4
    invoke-virtual {v0, v4}, Landroid/widget/PopupWindow;->setWidth(I)V

    const/4 v1, -0x2

    invoke-virtual {v0, v5, v1}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    invoke-virtual {v0, v6}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    iget-boolean v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->j:Z

    if-eqz v1, :cond_5

    iget v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->l:I

    iget v2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->m:I

    invoke-virtual {v0, p0, v1, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    :goto_3
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/af;->r()V

    goto :goto_2

    :cond_5
    invoke-virtual {v0, v5, v5}, Landroid/widget/PopupWindow;->setWindowLayoutMode(II)V

    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    const/4 v2, 0x2

    new-array v2, v2, [I

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/PopupEditText;->getLocationInWindow([I)V

    aget v2, v2, v6

    iget v1, v1, Landroid/graphics/Rect;->top:I

    sub-int v1, v2, v1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setHeight(I)V

    iget v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->l:I

    iget v2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->m:I

    invoke-virtual {v0, p0, v1, v2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    goto :goto_3
.end method

.method public a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;Landroid/widget/Filterable;J)V
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "tokenizer cannot be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->p:Landroid/widget/ListAdapter;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "setAdapter must be called first with a non-null adapter"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p2, p0, Lcom/twitter/internal/android/widget/PopupEditText;->r:Landroid/widget/Filterable;

    iput-object p1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    iput-wide p3, p0, Lcom/twitter/internal/android/widget/PopupEditText;->t:J

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->g:Lcom/twitter/internal/android/widget/ae;

    invoke-virtual {v0, p2}, Lcom/twitter/internal/android/widget/ae;->a(Landroid/widget/Filterable;)V

    return-void
.end method

.method public a(Z)V
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->r:Landroid/widget/Filterable;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionEnd()I

    move-result v2

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v0, v3, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v4

    sub-int v0, v2, v4

    iget v5, p0, Lcom/twitter/internal/android/widget/PopupEditText;->h:I

    if-ge v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    goto :goto_0

    :cond_2
    iget-object v5, p0, Lcom/twitter/internal/android/widget/PopupEditText;->g:Lcom/twitter/internal/android/widget/ae;

    invoke-virtual {v5, v1}, Landroid/os/Handler;->removeMessages(I)V

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v3, v4, v2}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v5, v1, v0, v1, v2}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->t:J

    invoke-virtual {v5, v0, v1, v2}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method protected a(I)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v0}, Landroid/widget/PopupWindow;->dismiss()V

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->c:Z

    iput-boolean v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->d:Z

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->g:Lcom/twitter/internal/android/widget/ae;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ae;->removeMessages(I)V

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->c:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/EditText;->onAttachedToWindow()V

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->i:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->e:Landroid/widget/PopupWindow;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->n:Landroid/view/View$OnClickListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->n:Landroid/view/View$OnClickListener;

    invoke-interface {v0, p1}, Landroid/view/View$OnClickListener;->onClick(Landroid/view/View;)V

    :cond_1
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 2

    invoke-super {p0, p1}, Landroid/widget/EditText;->onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Lcom/twitter/internal/android/widget/e;

    invoke-direct {v1, v0}, Lcom/twitter/internal/android/widget/e;-><init>(Landroid/view/inputmethod/InputConnection;)V

    iput-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->u:Lcom/twitter/internal/android/widget/e;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->u:Lcom/twitter/internal/android/widget/e;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->v:Lcom/twitter/internal/android/widget/f;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/f;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->u:Lcom/twitter/internal/android/widget/e;

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Landroid/widget/EditText;->onDetachedFromWindow()V

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    return-void
.end method

.method public onFilterComplete(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionEnd()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v1

    sub-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->h:I

    if-lt v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    goto :goto_0
.end method

.method protected onFocusChanged(ZILandroid/graphics/Rect;)V
    .locals 0

    invoke-super {p0, p1, p2, p3}, Landroid/widget/EditText;->onFocusChanged(ZILandroid/graphics/Rect;)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    :cond_0
    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->clearComposingText()V

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->r:Landroid/widget/Filterable;

    if-nez v1, :cond_1

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->length()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionEnd()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/internal/android/widget/PopupEditText;->q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    invoke-interface {v3, v1, v2}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v3

    iget-object v4, p0, Lcom/twitter/internal/android/widget/PopupEditText;->q:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    iget-object v5, p0, Lcom/twitter/internal/android/widget/PopupEditText;->r:Landroid/widget/Filterable;

    invoke-interface {v5}, Landroid/widget/Filterable;->getFilter()Landroid/widget/Filter;

    move-result-object v5

    invoke-virtual {v5, v0}, Landroid/widget/Filter;->convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v4, v0}, Landroid/widget/MultiAutoCompleteTextView$Tokenizer;->terminateToken(Ljava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v1, v3, v2, v0}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v1, v0, p3}, Lcom/twitter/internal/android/widget/af;->a(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/twitter/internal/android/widget/PopupEditText;->f:Lcom/twitter/internal/android/widget/DropDownListView;

    const/16 v0, 0x3e

    if-eq p1, v0, :cond_1

    invoke-virtual {v5}, Lcom/twitter/internal/android/widget/DropDownListView;->getSelectedItemPosition()I

    move-result v0

    if-gez v0, :cond_0

    const/16 v0, 0x42

    if-eq p1, v0, :cond_1

    const/16 v0, 0x17

    if-eq p1, v0, :cond_1

    :cond_0
    invoke-virtual {v5}, Lcom/twitter/internal/android/widget/DropDownListView;->getSelectedItemPosition()I

    move-result v6

    iget-object v7, p0, Lcom/twitter/internal/android/widget/PopupEditText;->e:Landroid/widget/PopupWindow;

    invoke-virtual {v7}, Landroid/widget/PopupWindow;->isAboveAnchor()Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    iget-object v8, p0, Lcom/twitter/internal/android/widget/PopupEditText;->p:Landroid/widget/ListAdapter;

    const v4, 0x7fffffff

    const/high16 v3, -0x80000000

    if-eqz v8, :cond_5

    invoke-interface {v8}, Landroid/widget/ListAdapter;->getCount()I

    move-result v3

    move v9, v3

    move v3, v2

    move v2, v9

    :goto_1
    invoke-virtual {v5, p1, p2}, Lcom/twitter/internal/android/widget/DropDownListView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v4

    if-eqz v4, :cond_3

    const/4 v0, 0x2

    invoke-virtual {v7, v0}, Landroid/widget/PopupWindow;->setInputMethodMode(I)V

    invoke-virtual {v5}, Lcom/twitter/internal/android/widget/DropDownListView;->requestFocusFromTouch()Z

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    sparse-switch p1, :sswitch_data_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    :goto_2
    :sswitch_0
    return v1

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    if-eqz v0, :cond_4

    const/16 v4, 0x14

    if-ne p1, v4, :cond_4

    if-ne v6, v2, :cond_1

    goto :goto_2

    :cond_4
    if-nez v0, :cond_1

    const/16 v0, 0x13

    if-ne p1, v0, :cond_1

    if-ne v6, v3, :cond_1

    goto :goto_2

    :cond_5
    move v2, v3

    move v3, v4

    goto :goto_1

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_0
        0x14 -> :sswitch_0
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method

.method public onKeyPreIme(ILandroid/view/KeyEvent;)Z
    .locals 2

    const/4 v0, 0x1

    const/4 v1, 0x4

    if-ne p1, v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p2}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2, p0}, Landroid/view/KeyEvent$DispatcherState;->startTracking(Landroid/view/KeyEvent;Ljava/lang/Object;)V

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getKeyDispatcherState()Landroid/view/KeyEvent$DispatcherState;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/view/KeyEvent$DispatcherState;->handleUpEvent(Landroid/view/KeyEvent;)V

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isTracking()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p2}, Landroid/view/KeyEvent;->isCanceled()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyPreIme(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->f:Lcom/twitter/internal/android/widget/DropDownListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DropDownListView;->getSelectedItemPosition()I

    move-result v1

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    if-ltz v1, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/DropDownListView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/widget/EditText;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected onSelectionChanged(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v0, p1, p2}, Lcom/twitter/internal/android/widget/af;->a(II)V

    :cond_0
    return-void
.end method

.method protected onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->onTextChanged(Ljava/lang/CharSequence;III)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a(Z)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 13

    const/4 v12, 0x2

    const/4 v7, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :cond_0
    :pswitch_0
    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    invoke-super {p0, p1}, Landroid/widget/EditText;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    return v1

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v4, v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getPaddingLeft()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getPaddingRight()I

    move-result v6

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getCompoundPaddingLeft()I

    move-result v7

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getCompoundPaddingRight()I

    move-result v8

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getWidth()I

    move-result v9

    move v0, v1

    :goto_1
    array-length v10, v3

    if-ge v0, v10, :cond_3

    aget-object v10, v3, v0

    if-eqz v10, :cond_5

    if-nez v0, :cond_4

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    add-int/2addr v10, v5

    add-int/2addr v10, v7

    if-gt v4, v10, :cond_5

    iput v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    :cond_3
    :goto_2
    move v0, v1

    goto :goto_0

    :cond_4
    if-ne v0, v12, :cond_5

    sub-int v11, v9, v6

    invoke-virtual {v10}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v10

    invoke-virtual {v10}, Landroid/graphics/Rect;->width()I

    move-result v10

    sub-int v10, v11, v10

    sub-int/2addr v10, v8

    if-lt v4, v10, :cond_5

    iput v12, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    goto :goto_2

    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :pswitch_2
    iget v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    if-eq v0, v7, :cond_0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    float-to-int v3, v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v4

    iget v5, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    aget-object v4, v4, v5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getHeight()I

    move-result v5

    if-ge v0, v5, :cond_6

    if-lez v0, :cond_6

    move v0, v2

    :goto_3
    iget v5, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    packed-switch v5, :pswitch_data_1

    :pswitch_3
    move v3, v1

    :goto_4
    if-eqz v0, :cond_9

    if-eqz v3, :cond_9

    iget v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a(I)Z

    move-result v0

    if-eqz v0, :cond_9

    move v0, v2

    :goto_5
    iput v7, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_3

    :pswitch_4
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getPaddingLeft()I

    move-result v5

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    add-int/2addr v4, v5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getCompoundPaddingLeft()I

    move-result v5

    add-int/2addr v4, v5

    if-gt v3, v4, :cond_7

    move v3, v2

    goto :goto_4

    :cond_7
    move v3, v1

    goto :goto_4

    :pswitch_5
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getPaddingRight()I

    move-result v6

    sub-int/2addr v5, v6

    invoke-virtual {v4}, Landroid/graphics/drawable/Drawable;->getBounds()Landroid/graphics/Rect;

    move-result-object v4

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v4

    sub-int v4, v5, v4

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->getCompoundPaddingRight()I

    move-result v5

    sub-int/2addr v4, v5

    if-lt v3, v4, :cond_8

    move v3, v2

    goto :goto_4

    :cond_8
    move v3, v1

    goto :goto_4

    :cond_9
    move v0, v1

    goto :goto_5

    :pswitch_6
    iput v7, p0, Lcom/twitter/internal/android/widget/PopupEditText;->w:I

    move v0, v1

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0

    invoke-super {p0, p1}, Landroid/widget/EditText;->onWindowFocusChanged(Z)V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/widget/ListAdapter;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->p:Landroid/widget/ListAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->p:Landroid/widget/ListAdapter;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->s:Landroid/database/DataSetObserver;

    invoke-interface {v0, v1}, Landroid/widget/ListAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->f:Lcom/twitter/internal/android/widget/DropDownListView;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/DropDownListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->s:Landroid/database/DataSetObserver;

    invoke-interface {p1, v0}, Landroid/widget/ListAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    iput-object p1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->p:Landroid/widget/ListAdapter;

    return-void

    :cond_0
    new-instance v0, Lcom/twitter/internal/android/widget/ad;

    invoke-direct {v0, p0}, Lcom/twitter/internal/android/widget/ad;-><init>(Lcom/twitter/internal/android/widget/PopupEditText;)V

    iput-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->s:Landroid/database/DataSetObserver;

    goto :goto_0
.end method

.method public setBatchListener(Lcom/twitter/internal/android/widget/f;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->v:Lcom/twitter/internal/android/widget/f;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->u:Lcom/twitter/internal/android/widget/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->u:Lcom/twitter/internal/android/widget/e;

    iget-object v1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->v:Lcom/twitter/internal/android/widget/f;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/e;->a(Lcom/twitter/internal/android/widget/f;)V

    :cond_0
    return-void
.end method

.method protected setFrame(IIII)Z
    .locals 2

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/EditText;->setFrame(IIII)Z

    move-result v0

    invoke-direct {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :cond_0
    return v0
.end method

.method public setOnClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->n:Landroid/view/View$OnClickListener;

    return-void
.end method

.method public setPopupEditTextListener(Lcom/twitter/internal/android/widget/af;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/internal/android/widget/PopupEditText;->o:Lcom/twitter/internal/android/widget/af;

    iget-object v0, p0, Lcom/twitter/internal/android/widget/PopupEditText;->g:Lcom/twitter/internal/android/widget/ae;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ae;->a(Lcom/twitter/internal/android/widget/af;)V

    return-void
.end method
