.class Lcom/twitter/internal/android/widget/at;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/util/Comparator;


# direct methods
.method constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lhn;Lhn;)I
    .locals 4

    const v1, 0x7fffffff

    const/4 v2, -0x1

    invoke-virtual {p1}, Lhn;->p()I

    move-result v0

    if-ne v0, v2, :cond_0

    move v0, v1

    :cond_0
    invoke-virtual {p2}, Lhn;->p()I

    move-result v3

    if-ne v3, v2, :cond_3

    :goto_0
    if-le v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    if-ge v0, v1, :cond_2

    move v0, v2

    goto :goto_1

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_0
.end method

.method public synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1

    check-cast p1, Lhn;

    check-cast p2, Lhn;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/internal/android/widget/at;->a(Lhn;Lhn;)I

    move-result v0

    return v0
.end method
