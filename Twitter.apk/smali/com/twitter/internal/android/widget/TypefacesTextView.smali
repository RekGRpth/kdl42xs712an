.class public Lcom/twitter/internal/android/widget/TypefacesTextView;
.super Landroid/widget/TextView;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/TextView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public setTypeface(Landroid/graphics/Typeface;I)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->isInEditMode()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v1

    iget-boolean v1, v1, Lcom/twitter/internal/android/widget/ax;->f:Z

    if-eqz v1, :cond_3

    const/4 v1, 0x3

    if-ne p2, v1, :cond_0

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->d:Landroid/graphics/Typeface;

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x1

    if-ne p2, v1, :cond_1

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->c:Landroid/graphics/Typeface;

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x2

    if-ne p2, v1, :cond_2

    invoke-static {v0}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->b:Landroid/graphics/Typeface;

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-super {p0, v0}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    goto :goto_0

    :cond_3
    invoke-super {p0, p1, p2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_0
.end method
