.class public final Lcom/twitter/internal/android/b;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final actionLayout:I = 0x7f010134

.field public static final allCaps:I = 0x7f010130

.field public static final autoUnlock:I = 0x7f010089

.field public static final badgeIndicatorStyle:I = 0x7f010003

.field public static final badgeMode:I = 0x7f01006c

.field public static final bgColorHint:I = 0x7f0100b3

.field public static final borderColor:I = 0x7f0100aa

.field public static final borderHeight:I = 0x7f0100ab

.field public static final bottomDockId:I = 0x7f010084

.field public static final bottomPeek:I = 0x7f010087

.field public static final cardStyle:I = 0x7f010005

.field public static final closeAnimDuration:I = 0x7f0100b0

.field public static final closeInterpolator:I = 0x7f0100ae

.field public static final cornerRadius:I = 0x7f0100fd

.field public static final croppableImageViewStyle:I = 0x7f01000b

.field public static final displayMode:I = 0x7f0100cf

.field public static final dividerColor:I = 0x7f010101

.field public static final dividerHeight:I = 0x7f010100

.field public static final dividerWidth:I = 0x7f0100b8

.field public static final dockLayoutStyle:I = 0x7f01000d

.field public static final draggable:I = 0x7f0100b6

.field public static final draggableEdgeSize:I = 0x7f0100b5

.field public static final drawerDirection:I = 0x7f0100b4

.field public static final dropDownListViewStyle:I = 0x7f010010

.field public static final edgePadding:I = 0x7f0100b9

.field public static final fillColor:I = 0x7f0100a9

.field public static final fillMode:I = 0x7f0100be

.field public static final fillWidthHeightRatio:I = 0x7f0100ba

.field public static final gapSize:I = 0x7f0100ac

.field public static final groupStyle:I = 0x7f010012

.field public static final groupedRowViewStyle:I = 0x7f010013

.field public static final gutterColor:I = 0x7f0100b2

.field public static final gutterSize:I = 0x7f0100b1

.field public static final hiddenDrawerStyle:I = 0x7f010014

.field public static final horizontalListViewStyle:I = 0x7f010015

.field public static final indicatorDrawable:I = 0x7f010065

.field public static final indicatorMarginBottom:I = 0x7f010066

.field public static final inset:I = 0x7f0100f7

.field public static final insetBottom:I = 0x7f0100fb

.field public static final insetBottomFillColor:I = 0x7f0100fc

.field public static final insetLeft:I = 0x7f0100f8

.field public static final insetRight:I = 0x7f0100fa

.field public static final insetTop:I = 0x7f0100f9

.field public static final leftFadeInDrawable:I = 0x7f0100bf

.field public static final listDivider:I = 0x7f0100b7

.field public static final navItemStyle:I = 0x7f01001c

.field public static final notchBg:I = 0x7f0100d1

.field public static final notchDrawable:I = 0x7f0100d0

.field public static final notchLeftOffset:I = 0x7f0100d2

.field public static final notchViewStyle:I = 0x7f01001e

.field public static final numberBackground:I = 0x7f010067

.field public static final numberColor:I = 0x7f010068

.field public static final numberMinHeight:I = 0x7f01006b

.field public static final numberMinWidth:I = 0x7f01006a

.field public static final numberTextSize:I = 0x7f010069

.field public static final openAnimDuration:I = 0x7f0100af

.field public static final openInterpolator:I = 0x7f0100ad

.field public static final order:I = 0x7f01001f

.field public static final overflowIcon:I = 0x7f010132

.field public static final overlayDrawable:I = 0x7f0100d3

.field public static final popupEditListStyle:I = 0x7f010026

.field public static final popupEditTextStyle:I = 0x7f010027

.field public static final popupMenuXOffset:I = 0x7f010029

.field public static final popupMenuYOffset:I = 0x7f01002a

.field public static final primaryItem:I = 0x7f010133

.field public static final priority:I = 0x7f01002b

.field public static final rightFadeInDrawable:I = 0x7f0100c0

.field public static final scrollDrawable:I = 0x7f0100bc

.field public static final scrollDrive:I = 0x7f010088

.field public static final scrollHeight:I = 0x7f0100bd

.field public static final scrollOffset:I = 0x7f0100bb

.field public static final segmentDivider:I = 0x7f010104

.field public static final segmentLabels:I = 0x7f010105

.field public static final segmentedControlStyle:I = 0x7f010031

.field public static final selectedTextStyle:I = 0x7f010032

.field public static final shadowColor:I = 0x7f010033

.field public static final shadowDx:I = 0x7f010034

.field public static final shadowDy:I = 0x7f010035

.field public static final shadowRadius:I = 0x7f010036

.field public static final shadowTextViewStyle:I = 0x7f010037

.field public static final showAsAction:I = 0x7f010135

.field public static final showAsDropdown:I = 0x7f0100e6

.field public static final showFullScreen:I = 0x7f0100e7

.field public static final showPopupOnInitialFocus:I = 0x7f0100e9

.field public static final single:I = 0x7f010038

.field public static final src:I = 0x7f010103

.field public static final stackFromRight:I = 0x7f01012d

.field public static final state_collapsed:I = 0x7f010042

.field public static final state_docked:I = 0x7f010044

.field public static final state_highlighted:I = 0x7f010045

.field public static final state_numbered:I = 0x7f01004c

.field public static final strokeColor:I = 0x7f0100ff

.field public static final strokeWidth:I = 0x7f0100fe

.field public static final subtitle:I = 0x7f010131

.field public static final subtitleTextSize:I = 0x7f01012e

.field public static final tabDrawable:I = 0x7f0101bc

.field public static final tabMaxHeight:I = 0x7f0101bd

.field public static final textColor:I = 0x7f01004f

.field public static final textSize:I = 0x7f010050

.field public static final textStyle:I = 0x7f010051

.field public static final threshold:I = 0x7f0100e8

.field public static final toolBarCustomViewId:I = 0x7f01012a

.field public static final toolBarDisplayOptions:I = 0x7f01012c

.field public static final toolBarHomeStyle:I = 0x7f010052

.field public static final toolBarIcon:I = 0x7f010127

.field public static final toolBarItemBackground:I = 0x7f010124

.field public static final toolBarItemPadding:I = 0x7f010125

.field public static final toolBarItemStyle:I = 0x7f010053

.field public static final toolBarOverflowContentDescription:I = 0x7f01012b

.field public static final toolBarOverflowDrawable:I = 0x7f010129

.field public static final toolBarOverflowSubtitleTextAppearance:I = 0x7f010054

.field public static final toolBarOverflowTextAppearance:I = 0x7f010055

.field public static final toolBarPopupWindowStyle:I = 0x7f010056

.field public static final toolBarSize:I = 0x7f010057

.field public static final toolBarStyle:I = 0x7f010058

.field public static final toolBarTitle:I = 0x7f010126

.field public static final toolBarUpIndicator:I = 0x7f010128

.field public static final topDockId:I = 0x7f010083

.field public static final topPeek:I = 0x7f010086

.field public static final turtle:I = 0x7f010085

.field public static final upIndicatorDescription:I = 0x7f01012f

.field public static final viewPagerScrollBarStyle:I = 0x7f010062
