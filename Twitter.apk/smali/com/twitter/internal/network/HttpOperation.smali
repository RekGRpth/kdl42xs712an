.class public abstract Lcom/twitter/internal/network/HttpOperation;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field protected static final a:Lcom/twitter/internal/network/i;

.field private static final e:Ljava/util/concurrent/atomic/AtomicLong;

.field private static final f:Lcom/twitter/internal/util/l;

.field private static final g:Lcom/twitter/internal/util/m;

.field private static h:Z


# instance fields
.field protected final b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

.field protected final c:Ljava/net/URI;

.field protected final d:Lcom/twitter/internal/network/i;

.field private i:Lorg/apache/http/HttpEntity;

.field private final j:Ljava/util/ArrayList;

.field private final k:Ljava/util/HashMap;

.field private l:Lcom/twitter/internal/network/k;

.field private volatile m:Z

.field private volatile n:Z

.field private o:Ljava/lang/Object;

.field private p:Ljava/lang/Object;

.field private q:I

.field private r:Ljava/util/zip/Inflater;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lcom/twitter/internal/network/e;

    invoke-direct {v0}, Lcom/twitter/internal/network/e;-><init>()V

    sput-object v0, Lcom/twitter/internal/network/HttpOperation;->a:Lcom/twitter/internal/network/i;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    sput-object v0, Lcom/twitter/internal/network/HttpOperation;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-static {}, Lcom/twitter/internal/util/i;->c()Lcom/twitter/internal/util/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/util/i;->a()Lcom/twitter/internal/util/l;

    move-result-object v0

    sput-object v0, Lcom/twitter/internal/network/HttpOperation;->f:Lcom/twitter/internal/util/l;

    invoke-static {}, Lcom/twitter/internal/util/i;->c()Lcom/twitter/internal/util/i;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/util/i;->b()Lcom/twitter/internal/util/m;

    move-result-object v0

    sput-object v0, Lcom/twitter/internal/network/HttpOperation;->g:Lcom/twitter/internal/util/m;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/twitter/internal/network/HttpOperation;->h:Z

    return-void
.end method

.method public constructor <init>(Lcom/twitter/internal/network/HttpOperation$RequestMethod;Ljava/net/URI;Lcom/twitter/internal/network/i;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->i:Lorg/apache/http/HttpEntity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    new-instance v0, Lcom/twitter/internal/network/k;

    invoke-direct {v0}, Lcom/twitter/internal/network/k;-><init>()V

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/internal/network/HttpOperation;->q:I

    iput-object p1, p0, Lcom/twitter/internal/network/HttpOperation;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    iput-object p2, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    if-nez p3, :cond_0

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->a:Lcom/twitter/internal/network/i;

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->d:Lcom/twitter/internal/network/i;

    :goto_0
    return-void

    :cond_0
    iput-object p3, p0, Lcom/twitter/internal/network/HttpOperation;->d:Lcom/twitter/internal/network/i;

    goto :goto_0
.end method

.method private a(Lhw;)J
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lhw;->a()J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->n()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Request not yet complete for this HttpOperation"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method private a(Lcom/twitter/internal/network/k;)V
    .locals 4

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->f:Lcom/twitter/internal/util/l;

    const-string/jumbo v1, "HttpOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] protocol: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/internal/network/k;->l:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/internal/network/k;->m:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", status: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/internal/util/l;->b(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private a(Lcom/twitter/internal/network/k;Ljava/lang/Object;)V
    .locals 7

    const/4 v3, -0x1

    invoke-virtual {p0, p2}, Lcom/twitter/internal/network/HttpOperation;->h(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, p2}, Lcom/twitter/internal/network/HttpOperation;->g(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p2}, Lcom/twitter/internal/network/HttpOperation;->f(Ljava/lang/Object;)I

    move-result v2

    const/4 v0, 0x0

    invoke-static {}, Lcom/twitter/internal/network/HttpOperation;->i()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v5, v4, v2}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/String;Ljava/lang/String;I)V

    :cond_0
    :try_start_0
    invoke-virtual {p0, p2}, Lcom/twitter/internal/network/HttpOperation;->e(Ljava/lang/Object;)Ljava/io/InputStream;

    move-result-object v1

    if-eqz v1, :cond_1

    new-instance v6, Lhw;

    invoke-direct {v6, v1}, Lhw;-><init>(Ljava/io/InputStream;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    new-instance v1, Ljava/io/BufferedInputStream;

    invoke-direct {v1, v6}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    if-eqz v4, :cond_2

    const-string/jumbo v0, "application/octet-stream"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "video/mp4"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "binary/octet-stream"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "application/json"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "text/html"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "text/plain"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "text/xml"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "application/xml"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "image/"

    invoke-virtual {v4, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Unsupported content type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    move-object v1, v0

    move-object v0, v6

    :goto_0
    invoke-virtual {p0, v1}, Lcom/twitter/internal/network/HttpOperation;->b(Ljava/lang/Exception;)V

    :cond_1
    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Lhw;)J

    move-result-wide v1

    iput-wide v1, p1, Lcom/twitter/internal/network/k;->e:J

    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lhw;->b()J

    move-result-wide v0

    :goto_2
    iput-wide v0, p1, Lcom/twitter/internal/network/k;->h:J

    return-void

    :cond_2
    if-eqz v5, :cond_6

    :try_start_2
    const-string/jumbo v0, "gzip"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    :goto_3
    new-instance v2, Lhv;

    invoke-direct {v2, v0}, Lhv;-><init>(Ljava/io/InputStream;)V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->d:Lcom/twitter/internal/network/i;

    iget v1, p1, Lcom/twitter/internal/network/k;->a:I

    invoke-interface/range {v0 .. v5}, Lcom/twitter/internal/network/i;->a(ILjava/io/InputStream;ILjava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->g:Lcom/twitter/internal/util/m;

    invoke-interface {v0}, Lcom/twitter/internal/util/m;->a()J

    move-result-wide v0

    invoke-static {v2}, Lcom/twitter/internal/network/HttpOperation;->a(Lhv;)Z

    move-result v3

    if-nez v3, :cond_3

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/InputStream;)I

    :cond_3
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    sget-object v2, Lcom/twitter/internal/network/HttpOperation;->g:Lcom/twitter/internal/util/m;

    invoke-interface {v2}, Lcom/twitter/internal/util/m;->a()J

    move-result-wide v2

    sub-long v0, v2, v0

    iput-wide v0, p1, Lcom/twitter/internal/network/k;->g:J

    move-object v0, v6

    goto :goto_1

    :cond_4
    const-string/jumbo v0, "deflate"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    new-instance v0, Ljava/util/zip/InflaterInputStream;

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->p()Ljava/util/zip/Inflater;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/util/zip/InflaterInputStream;-><init>(Ljava/io/InputStream;Ljava/util/zip/Inflater;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :cond_5
    const-wide/16 v0, 0x0

    goto :goto_2

    :catch_1
    move-exception v1

    goto :goto_0

    :cond_6
    move-object v0, v1

    move v3, v2

    goto :goto_3
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 4

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->f:Lcom/twitter/internal/util/l;

    const-string/jumbo v1, "HttpOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "] content-encoding: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", content-type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, ", content-length: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/twitter/internal/util/l;->b(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static a(Z)V
    .locals 0

    sput-boolean p0, Lcom/twitter/internal/network/HttpOperation;->h:Z

    return-void
.end method

.method private static a(Lhv;)Z
    .locals 1

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Lhv;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Z
    .locals 1

    const/16 v0, 0xc8

    if-lt p1, v0, :cond_0

    const/16 v0, 0x12c

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static i()Z
    .locals 1

    sget-boolean v0, Lcom/twitter/internal/network/HttpOperation;->h:Z

    return v0
.end method

.method private k(Ljava/lang/Object;)V
    .locals 5

    iget v0, p0, Lcom/twitter/internal/network/HttpOperation;->q:I

    if-ltz v0, :cond_0

    iget v0, p0, Lcom/twitter/internal/network/HttpOperation;->q:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;I)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->f()Lorg/apache/http/HttpEntity;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;Lorg/apache/http/HttpEntity;)V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {p0, p1, v2, v1}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method private l(Ljava/lang/Object;)V
    .locals 5

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/internal/network/HttpOperation;->m(Ljava/lang/Object;)Ljava/util/Date;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    sget-object v4, Lcom/twitter/internal/network/HttpOperation;->e:Ljava/util/concurrent/atomic/AtomicLong;

    sub-long/2addr v0, v2

    invoke-virtual {v4, v0, v1}, Ljava/util/concurrent/atomic/AtomicLong;->set(J)V

    goto :goto_0
.end method

.method private m(Ljava/lang/Object;)Ljava/util/Date;
    .locals 2

    const-string/jumbo v0, "Date"

    invoke-direct {p0, p1, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    :try_start_0
    sget-object v1, Lcom/twitter/internal/util/k;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v1, v0}, Ljava/text/SimpleDateFormat;->parse(Ljava/lang/String;)Ljava/util/Date;
    :try_end_0
    .catch Ljava/text/ParseException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private q()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/network/f;

    invoke-interface {v0, p0}, Lcom/twitter/internal/network/f;->b(Lcom/twitter/internal/network/HttpOperation;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method

.method private r()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/network/f;

    invoke-interface {v0, p0}, Lcom/twitter/internal/network/f;->a(Lcom/twitter/internal/network/HttpOperation;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/internal/network/f;)Lcom/twitter/internal/network/HttpOperation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/Exception;)Lcom/twitter/internal/network/HttpOperation;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->d()V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/internal/network/k;->a:I

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    iput-object p1, v0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/internal/network/HttpOperation;
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object p0

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public a(Lorg/apache/http/HttpEntity;)Lcom/twitter/internal/network/HttpOperation;
    .locals 0

    iput-object p1, p0, Lcom/twitter/internal/network/HttpOperation;->i:Lorg/apache/http/HttpEntity;

    return-object p0
.end method

.method protected abstract a(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/String;I)Ljava/lang/String;
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->k:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/internal/network/HttpOperation;->q:I

    return-void
.end method

.method protected abstract a(Ljava/lang/Object;I)V
.end method

.method protected abstract a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method protected abstract a(Ljava/lang/Object;Lorg/apache/http/HttpEntity;)V
.end method

.method protected abstract b(Ljava/lang/Object;)I
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method public b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/twitter/internal/network/HttpOperation;->a()V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->p:Ljava/lang/Object;

    invoke-direct {p0, v0, p1}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected b(Ljava/lang/Exception;)V
    .locals 4

    invoke-static {}, Lcom/twitter/internal/network/HttpOperation;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->f:Lcom/twitter/internal/util/l;

    const-string/jumbo v1, "HttpOperation"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2, p1}, Lcom/twitter/internal/util/l;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->d()V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    const/4 v1, 0x0

    iput v1, v0, Lcom/twitter/internal/network/k;->a:I

    iput-object p1, v0, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    return-void
.end method

.method protected abstract c()Ljava/lang/Object;
.end method

.method protected abstract c(Ljava/lang/Object;)Ljava/lang/Object;
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->d(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/internal/network/HttpOperation;->m:Z

    return-void
.end method

.method protected abstract d(Ljava/lang/Object;)V
.end method

.method public final e()Lcom/twitter/internal/network/HttpOperation;
    .locals 9

    const/4 v7, 0x1

    iget-boolean v0, p0, Lcom/twitter/internal/network/HttpOperation;->n:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    iput-boolean v7, p0, Lcom/twitter/internal/network/HttpOperation;->n:Z

    invoke-direct {p0}, Lcom/twitter/internal/network/HttpOperation;->r()V

    sget-boolean v0, Lcom/twitter/internal/network/HttpOperation;->h:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->m()V

    :cond_2
    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->g:Lcom/twitter/internal/util/m;

    invoke-interface {v0}, Lcom/twitter/internal/util/m;->a()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/twitter/internal/network/k;->m:Ljava/lang/String;

    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->c()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    invoke-direct {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->k(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->c(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->b(Ljava/lang/Object;)I

    move-result v1

    iput v1, v4, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->f(Ljava/lang/Object;)I

    move-result v1

    iput v1, v4, Lcom/twitter/internal/network/k;->j:I

    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->j(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/twitter/internal/network/k;->l:Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    sget-boolean v1, Lcom/twitter/internal/network/HttpOperation;->h:Z

    if-eqz v1, :cond_3

    invoke-direct {p0, v4}, Lcom/twitter/internal/network/HttpOperation;->a(Lcom/twitter/internal/network/k;)V

    :cond_3
    invoke-direct {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->l(Ljava/lang/Object;)V

    sget-object v1, Lcom/twitter/internal/network/HttpOperation;->g:Lcom/twitter/internal/util/m;

    invoke-interface {v1}, Lcom/twitter/internal/util/m;->a()J

    move-result-wide v5

    sub-long v1, v5, v2

    iput-wide v1, v4, Lcom/twitter/internal/network/k;->f:J

    if-eqz v0, :cond_4

    iget-object v1, v4, Lcom/twitter/internal/network/k;->c:Ljava/lang/Exception;

    if-nez v1, :cond_4

    invoke-direct {p0, v4, v0}, Lcom/twitter/internal/network/HttpOperation;->a(Lcom/twitter/internal/network/k;Ljava/lang/Object;)V

    :cond_4
    iget-wide v1, v4, Lcom/twitter/internal/network/k;->f:J

    iget-wide v5, v4, Lcom/twitter/internal/network/k;->e:J

    add-long/2addr v1, v5

    iget-wide v5, v4, Lcom/twitter/internal/network/k;->g:J

    add-long/2addr v1, v5

    iput-wide v1, v4, Lcom/twitter/internal/network/k;->d:J

    iput-boolean v7, p0, Lcom/twitter/internal/network/HttpOperation;->m:Z

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/internal/network/HttpOperation;->n:Z

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->p:Ljava/lang/Object;

    iget v0, v4, Lcom/twitter/internal/network/k;->a:I

    invoke-direct {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->b(I)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->d:Lcom/twitter/internal/network/i;

    invoke-interface {v0, v4}, Lcom/twitter/internal/network/i;->a(Lcom/twitter/internal/network/k;)V

    :cond_5
    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->o:Ljava/lang/Object;

    invoke-virtual {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->i(Ljava/lang/Object;)V

    :cond_6
    invoke-direct {p0}, Lcom/twitter/internal/network/HttpOperation;->q()V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    move-object v8, v0

    move-object v0, v1

    move-object v1, v8

    :goto_2
    invoke-virtual {p0, v1}, Lcom/twitter/internal/network/HttpOperation;->b(Ljava/lang/Exception;)V

    goto :goto_1

    :catch_1
    move-exception v1

    goto :goto_2
.end method

.method protected abstract e(Ljava/lang/Object;)Ljava/io/InputStream;
.end method

.method protected abstract f(Ljava/lang/Object;)I
.end method

.method public final f()Lorg/apache/http/HttpEntity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->i:Lorg/apache/http/HttpEntity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->i:Lorg/apache/http/HttpEntity;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Lcom/twitter/internal/network/HttpOperation$RequestMethod;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    return-object v0
.end method

.method protected abstract g(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method protected abstract h(Ljava/lang/Object;)Ljava/lang/String;
.end method

.method public h()Ljava/net/URI;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    return-object v0
.end method

.method protected i(Ljava/lang/Object;)V
    .locals 0

    return-void
.end method

.method protected j(Ljava/lang/Object;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "http/1.1"

    return-object v0
.end method

.method public j()Z
    .locals 2

    invoke-direct {p0}, Lcom/twitter/internal/network/HttpOperation;->a()V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    const/16 v1, 0xc8

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/internal/network/HttpOperation;->a()V

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    iget v0, v0, Lcom/twitter/internal/network/k;->a:I

    invoke-direct {p0, v0}, Lcom/twitter/internal/network/HttpOperation;->b(I)Z

    move-result v0

    return v0
.end method

.method public l()Lcom/twitter/internal/network/k;
    .locals 1

    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->l:Lcom/twitter/internal/network/k;

    return-object v0
.end method

.method public m()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/internal/network/HttpOperation;->f()Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    sget-object v2, Lcom/twitter/internal/network/HttpOperation;->f:Lcom/twitter/internal/util/l;

    const-string/jumbo v3, "HttpOperation"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "["

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "] "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/internal/network/HttpOperation;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ", has entity: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Lcom/twitter/internal/util/l;->b(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->f:Lcom/twitter/internal/util/l;

    const-string/jumbo v2, "HttpOperation"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "["

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/internal/network/HttpOperation;->c:Ljava/net/URI;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "] sending content-length: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-interface {v1}, Lorg/apache/http/HttpEntity;->getContentLength()J

    move-result-wide v4

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, " bytes"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Lcom/twitter/internal/util/l;->b(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public n()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/internal/network/HttpOperation;->m:Z

    return v0
.end method

.method public o()J
    .locals 2

    sget-object v0, Lcom/twitter/internal/network/HttpOperation;->e:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicLong;->get()J

    move-result-wide v0

    return-wide v0
.end method

.method public declared-synchronized p()Ljava/util/zip/Inflater;
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->r:Ljava/util/zip/Inflater;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/zip/Inflater;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/zip/Inflater;-><init>(Z)V

    iput-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->r:Ljava/util/zip/Inflater;

    :cond_0
    iget-object v0, p0, Lcom/twitter/internal/network/HttpOperation;->r:Ljava/util/zip/Inflater;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
