.class public abstract Lcom/twitter/internal/util/i;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Lcom/twitter/internal/util/i;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/twitter/internal/util/i;->a:Lcom/twitter/internal/util/i;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Lcom/twitter/internal/util/i;)V
    .locals 0

    sput-object p0, Lcom/twitter/internal/util/i;->a:Lcom/twitter/internal/util/i;

    return-void
.end method

.method public static c()Lcom/twitter/internal/util/i;
    .locals 2

    sget-object v0, Lcom/twitter/internal/util/i;->a:Lcom/twitter/internal/util/i;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    const-string/jumbo v1, "A default context has not been set"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    :cond_0
    sget-object v0, Lcom/twitter/internal/util/i;->a:Lcom/twitter/internal/util/i;

    return-object v0
.end method


# virtual methods
.method public abstract a()Lcom/twitter/internal/util/l;
.end method

.method public abstract b()Lcom/twitter/internal/util/m;
.end method
