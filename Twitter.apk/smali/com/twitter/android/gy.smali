.class Lcom/twitter/android/gy;
.super Landroid/support/v4/view/PagerAdapter;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/client/c;

.field private final b:Ljava/util/ArrayList;

.field private final c:Landroid/util/SparseArray;

.field private final d:I

.field private final e:Lcom/twitter/library/util/aa;

.field private final f:J

.field private g:Lcom/twitter/android/hi;

.field private h:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/gy;->a:Lcom/twitter/android/client/c;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/gy;->b:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/gy;->c:Landroid/util/SparseArray;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c008d    # com.twitter.android.R.dimen.media_thumbnail_size

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/gy;->d:I

    iget-object v0, p0, Lcom/twitter/android/gy;->a:Lcom/twitter/android/client/c;

    const-string/jumbo v1, "thumbs"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;Lcom/twitter/library/util/ab;)Lcom/twitter/library/util/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/gy;->e:Lcom/twitter/library/util/aa;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/gy;->f:J

    return-void
.end method

.method private a(Lcom/twitter/android/hi;Landroid/widget/ImageView;Lcom/twitter/library/util/ae;)V
    .locals 2

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/util/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    sget-object v1, Landroid/widget/ImageView$ScaleType;->MATRIX:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/MultiTouchImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    iget-object v0, p3, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v0, 0x0

    iput-boolean v0, p1, Lcom/twitter/android/hi;->b:Z

    new-instance v0, Ljava/lang/ref/WeakReference;

    iget-object v1, p3, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p1, Lcom/twitter/android/hi;->f:Ljava/lang/ref/WeakReference;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    const v0, 0x7f020285    # com.twitter.android.R.drawable.ic_tweet_placeholder_photo_dark_error

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/android/hi;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/gy;->g:Lcom/twitter/android/hi;

    return-object v0
.end method

.method public a(I)Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-gt v0, p1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gz;

    iget-object v0, v0, Lcom/twitter/android/gz;->a:Lcom/twitter/library/provider/Tweet;

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/hi;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    iget v1, p1, Lcom/twitter/android/hi;->c:I

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gz;

    iget-object v0, v0, Lcom/twitter/android/gz;->b:Lcom/twitter/library/util/m;

    iput-object v0, p1, Lcom/twitter/android/hi;->d:Lcom/twitter/library/util/m;

    iput-object v0, p1, Lcom/twitter/android/hi;->e:Lcom/twitter/library/util/m;

    iget-object v1, p0, Lcom/twitter/android/gy;->a:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v0, p1, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/gy;->a(Lcom/twitter/android/hi;Landroid/widget/ImageView;Lcom/twitter/library/util/ae;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/gy;->e:Lcom/twitter/library/util/aa;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/library/util/m;

    iget-object v0, v0, Lcom/twitter/library/util/m;->a:Ljava/lang/String;

    iget v2, p0, Lcom/twitter/android/gy;->d:I

    iget v3, p0, Lcom/twitter/android/gy;->d:I

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/twitter/android/gy;->e:Lcom/twitter/library/util/aa;

    invoke-virtual {v0, v1}, Lcom/twitter/library/util/aa;->b(Ljava/lang/Object;)Lcom/twitter/library/util/af;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v1, p1, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/MultiTouchImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V
    .locals 2

    if-eqz p2, :cond_0

    new-instance v0, Lcom/twitter/android/gz;

    invoke-static {p2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;)Lcom/twitter/library/util/m;

    move-result-object v1

    invoke-direct {v0, p1, v1}, Lcom/twitter/android/gz;-><init>(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/m;)V

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/gy;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 5

    iget-object v3, p0, Lcom/twitter/android/gy;->c:Landroid/util/SparseArray;

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v3, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/hi;

    iget-boolean v1, v0, Lcom/twitter/android/hi;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/hi;->e:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/hi;->e:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    iget-object v4, v0, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    invoke-direct {p0, v0, v4, v1}, Lcom/twitter/android/gy;->a(Lcom/twitter/android/hi;Landroid/widget/ImageView;Lcom/twitter/library/util/ae;)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    invoke-virtual {p0}, Lcom/twitter/android/gy;->notifyDataSetChanged()V

    return-void
.end method

.method public b(Ljava/util/List;)V
    .locals 6

    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/ImageSpec;

    iget-object v3, v0, Lcom/twitter/library/card/property/ImageSpec;->size:Lcom/twitter/library/card/property/Vector2F;

    new-instance v4, Lcom/twitter/library/util/m;

    iget-object v0, v0, Lcom/twitter/library/card/property/ImageSpec;->url:Ljava/lang/String;

    iget v5, v3, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v5, v5

    iget v3, v3, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v3, v3

    invoke-direct {v4, v0, v5, v3}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    new-instance v0, Lcom/twitter/android/gz;

    const/4 v3, 0x0

    invoke-direct {v0, v3, v4}, Lcom/twitter/android/gz;-><init>(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/m;)V

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/gy;->a(Ljava/util/List;)V

    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 3

    const/4 v2, 0x0

    check-cast p3, Lcom/twitter/android/hi;

    iget-object v0, p3, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/twitter/android/gy;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/android/gy;->c:Landroid/util/SparseArray;

    invoke-virtual {v1, p2}, Landroid/util/SparseArray;->remove(I)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    const/4 v0, 0x1

    iput-boolean v0, p3, Lcom/twitter/android/hi;->b:Z

    iput-object v2, p3, Lcom/twitter/android/hi;->f:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public finishUpdate(Landroid/view/ViewGroup;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/gy;->g:Lcom/twitter/android/hi;

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lcom/twitter/android/hi;->b:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/gy;->a(Lcom/twitter/android/hi;)V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gy;->h:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/gy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300dc    # com.twitter.android.R.layout.pager_image

    invoke-virtual {v0, v1, p1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/MultiTouchImageView;

    :goto_0
    new-instance v1, Lcom/twitter/android/hi;

    invoke-direct {v1}, Lcom/twitter/android/hi;-><init>()V

    iput p2, v1, Lcom/twitter/android/hi;->c:I

    iput-object v0, v1, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    invoke-virtual {p0, v1}, Lcom/twitter/android/gy;->a(Lcom/twitter/android/hi;)V

    iget-object v2, p0, Lcom/twitter/android/gy;->c:Landroid/util/SparseArray;

    invoke-virtual {v2, p2, v1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/gy;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/MultiTouchImageView;

    goto :goto_0
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    check-cast p2, Lcom/twitter/android/hi;

    iget-object v0, p2, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPrimaryItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    check-cast p3, Lcom/twitter/android/hi;

    iput-object p3, p0, Lcom/twitter/android/gy;->g:Lcom/twitter/android/hi;

    return-void
.end method
