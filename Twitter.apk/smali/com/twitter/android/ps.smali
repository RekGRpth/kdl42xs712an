.class Lcom/twitter/android/ps;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/bx;


# instance fields
.field final synthetic a:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/geo/TwitterPlace;Z)V
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    iget-boolean v1, v1, Lcom/twitter/android/PostActivity;->u:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    iget-object v1, v1, Lcom/twitter/android/PostActivity;->r:Lcom/twitter/android/PostStorage;

    invoke-virtual {v1}, Lcom/twitter/android/PostStorage;->c()Lcom/twitter/android/PostStorage$LocationItem;

    move-result-object v2

    if-eqz p1, :cond_3

    iget-object v1, p1, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    iget-object v0, p1, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    :goto_1
    if-eqz v2, :cond_2

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/PostStorage$LocationItem;->a(Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/PostActivity;->b(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/PostActivity;->a(IZ)V

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    iget-object v0, v0, Lcom/twitter/android/PostActivity;->i:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/ComposerLayout;->d()V

    goto :goto_0

    :cond_3
    move-object v1, v0

    goto :goto_1
.end method

.method public a(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    iget-boolean v0, v0, Lcom/twitter/android/PostActivity;->z:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/PostActivity;->b(Lcom/twitter/android/PostActivity;Z)V

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->e(Lcom/twitter/android/PostActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->f(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/PostActivity;->c(Lcom/twitter/android/PostActivity;Z)Z

    goto :goto_0
.end method

.method public b(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ps;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostActivity;->e(Z)V

    return-void
.end method
