.class Lcom/twitter/android/vy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field a:J

.field final synthetic b:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vy;->b:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/twitter/android/vy;->a:J

    const-wide/16 v4, 0x1388

    add-long/2addr v2, v4

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/vy;->b:Lcom/twitter/android/TweetActivity;

    new-instance v3, Lcom/twitter/library/api/conversations/an;

    iget-object v4, p0, Lcom/twitter/android/vy;->b:Lcom/twitter/android/TweetActivity;

    invoke-virtual {v4}, Lcom/twitter/android/TweetActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/vy;->b:Lcom/twitter/android/TweetActivity;

    iget-object v5, v5, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    iget-object v6, p0, Lcom/twitter/android/vy;->b:Lcom/twitter/android/TweetActivity;

    iget-object v6, v6, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v6, v6, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v4, v5, v6}, Lcom/twitter/library/api/conversations/an;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/4 v4, 0x6

    const/4 v5, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Lcom/twitter/library/service/b;II)Z

    iput-wide v0, p0, Lcom/twitter/android/vy;->a:J

    :cond_0
    return-void
.end method
