.class final Lcom/twitter/android/gj;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final a:Ljava/lang/ref/WeakReference;

.field final b:Ljava/lang/ref/WeakReference;

.field final c:Landroid/graphics/Bitmap;

.field final d:Lcom/twitter/android/gh;

.field final e:I

.field final f:I

.field final g:Lcom/twitter/media/filters/Filters;


# direct methods
.method constructor <init>(Lcom/twitter/android/FilterManager;Lcom/twitter/media/filters/Filters;ILandroid/graphics/Bitmap;Lcom/twitter/android/gh;Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/gj;->a:Ljava/lang/ref/WeakReference;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/gj;->b:Ljava/lang/ref/WeakReference;

    iput-object p4, p0, Lcom/twitter/android/gj;->c:Landroid/graphics/Bitmap;

    iput-object p5, p0, Lcom/twitter/android/gj;->d:Lcom/twitter/android/gh;

    iput-object p2, p0, Lcom/twitter/android/gj;->g:Lcom/twitter/media/filters/Filters;

    iget v0, p5, Lcom/twitter/android/gh;->b:I

    iput v0, p0, Lcom/twitter/android/gj;->e:I

    iput p3, p0, Lcom/twitter/android/gj;->f:I

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Landroid/graphics/Bitmap;
    .locals 5

    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/gj;->isCancelled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/gj;->g:Lcom/twitter/media/filters/Filters;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/gj;->g:Lcom/twitter/media/filters/Filters;

    iget v2, p0, Lcom/twitter/android/gj;->e:I

    iget v3, p0, Lcom/twitter/android/gj;->f:I

    iget-object v4, p0, Lcom/twitter/android/gj;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/media/filters/Filters;->a(IILandroid/graphics/Bitmap;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/gj;->c:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-static {v1}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/gj;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/gj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/FilterManager;

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/twitter/android/gj;->d:Lcom/twitter/android/gh;

    iget v2, v2, Lcom/twitter/android/gh;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, v1, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3, v2}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/gn;

    if-eqz v2, :cond_4

    iget-object v0, v2, Lcom/twitter/android/gn;->a:Landroid/view/View;

    move-object v2, v0

    :goto_0
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gf;

    iget-object v3, v0, Lcom/twitter/android/gf;->a:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/twitter/android/gf;->b:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    if-eqz p1, :cond_2

    invoke-virtual {v3, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    :goto_1
    if-eqz v1, :cond_1

    if-eqz v2, :cond_3

    const/4 v0, 0x1

    :goto_2
    iget-object v2, p0, Lcom/twitter/android/gj;->d:Lcom/twitter/android/gh;

    invoke-virtual {v1, v0, v2, p1}, Lcom/twitter/android/FilterManager;->a(ZLcom/twitter/android/gh;Landroid/graphics/Bitmap;)V

    :cond_1
    return-void

    :cond_2
    const v0, 0x7f020285    # com.twitter.android.R.drawable.ic_tweet_placeholder_photo_dark_error

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    :cond_4
    move-object v2, v0

    goto :goto_0
.end method

.method protected b(Landroid/graphics/Bitmap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/gj;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FilterManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/gj;->d:Lcom/twitter/android/gh;

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/android/FilterManager;->a(ZLcom/twitter/android/gh;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gj;->a([Ljava/lang/Void;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gj;->b(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gj;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method
