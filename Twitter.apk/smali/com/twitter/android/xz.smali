.class Lcom/twitter/android/xz;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xz;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIILjava/util/ArrayList;)V
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/xz;->a:Lcom/twitter/android/TweetSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetSettingsActivity;->c:Ljava/lang/String;

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xz;->a:Lcom/twitter/android/TweetSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetSettingsActivity;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xz;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-virtual {v0, p9}, Lcom/twitter/android/TweetSettingsActivity;->a(Ljava/util/ArrayList;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/xz;->a:Lcom/twitter/android/TweetSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/xz;->a:Lcom/twitter/android/TweetSettingsActivity;

    const v1, 0x7f0f0531    # com.twitter.android.R.string.users_fetch_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
