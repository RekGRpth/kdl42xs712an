.class Lcom/twitter/android/wf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wf;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/android/wf;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->k(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/wf;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, v2, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "tweet::reply_placeholder::click"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/wf;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/android/TweetActivity;Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/wf;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, p0, Lcom/twitter/android/wf;->a:Lcom/twitter/android/TweetActivity;

    iget-object v1, v1, Lcom/twitter/android/TweetActivity;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetActivity;->a(Lcom/twitter/library/provider/Tweet;)V

    goto :goto_0
.end method
