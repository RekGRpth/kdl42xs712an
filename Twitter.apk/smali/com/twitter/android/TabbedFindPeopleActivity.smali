.class public Lcom/twitter/android/TabbedFindPeopleActivity;
.super Lcom/twitter/android/TabbedFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/z;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TabbedFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;ILandroid/os/Bundle;Ljava/lang/Class;)V
    .locals 6

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TabbedFindPeopleActivity;->e:Lcom/twitter/android/TabsAdapter;

    iget-object v2, p0, Lcom/twitter/android/TabbedFindPeopleActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v2, p1}, Lcom/twitter/internal/android/widget/IconTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const v3, 0x7f0300dd    # com.twitter.android.R.layout.people_tab_indicator

    iget-object v4, p0, Lcom/twitter/android/TabbedFindPeopleActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v5, p2}, Lcom/twitter/android/widget/TabIndicator;->a(Landroid/view/LayoutInflater;ILandroid/widget/TabHost;II)Lcom/twitter/android/widget/TabIndicator;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    invoke-virtual {v1, v0, p4, p3}, Lcom/twitter/android/TabsAdapter;->a(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "personalized"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "refresh"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "is_hidden"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "popular"

    const v2, 0x7f0f019b    # com.twitter.android.R.string.find_people_tab_title_categories

    const-class v3, Lcom/twitter/android/CategoriesFragment;

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/twitter/android/TabbedFindPeopleActivity;->a(Ljava/lang/String;ILandroid/os/Bundle;Ljava/lang/Class;)V

    return-void
.end method

.method private b(I)Landroid/os/Bundle;
    .locals 5

    const/4 v2, 0x1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "find_friends"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "follow"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "fetch_always"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "hide_bio"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "originating_activity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "hide_contacts_import_cta"

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "hide_contacts_import_cta"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method private q()V
    .locals 6

    const/4 v5, 0x1

    const/16 v0, 0x15

    invoke-direct {p0, v0}, Lcom/twitter/android/TabbedFindPeopleActivity;->b(I)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "android_people_screen_cluster_follow_1596"

    new-array v2, v5, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, "cf_suggestions_category"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "cluster_follow"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "cluster_follow_type"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "cluster_follow_replenish_type"

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    const-string/jumbo v1, "suggested"

    const v2, 0x7f0f019d    # com.twitter.android.R.string.find_people_tab_title_suggested

    const-class v3, Lcom/twitter/android/UsersFragment;

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/twitter/android/TabbedFindPeopleActivity;->a(Ljava/lang/String;ILandroid/os/Bundle;Ljava/lang/Class;)V

    return-void
.end method

.method private r()V
    .locals 4

    const/16 v0, 0x16

    invoke-direct {p0, v0}, Lcom/twitter/android/TabbedFindPeopleActivity;->b(I)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "is_hidden"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "friends"

    const v2, 0x7f0f019c    # com.twitter.android.R.string.find_people_tab_title_contacts

    const-class v3, Lcom/twitter/android/UsersFragment;

    invoke-direct {p0, v1, v2, v0, v3}, Lcom/twitter/android/TabbedFindPeopleActivity;->a(Ljava/lang/String;ILandroid/os/Bundle;Ljava/lang/Class;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string/jumbo v0, "android_tabbed_ui_1358"

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "friends_tabbed_ui_bucket"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x3

    :goto_0
    iput v0, p0, Lcom/twitter/android/TabbedFindPeopleActivity;->a:I

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TabbedFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f0f0538    # com.twitter.android.R.string.users_pick_friend_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFindPeopleActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFindPeopleActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c00a3    # com.twitter.android.R.dimen.people_tabbar_margin

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFindPeopleActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/IconTabHost;->setOnTabClickedListener(Lcom/twitter/internal/android/widget/z;)V

    invoke-direct {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->q()V

    if-eqz v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->r()V

    :cond_0
    const-string/jumbo v0, "topic_find_people_1462"

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "personalized_topic_sul"

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/TabbedFindPeopleActivity;->a(Z)V

    return-void

    :cond_1
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public f()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->g()V

    return-void
.end method

.method protected g()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->aB()V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TabbedFragmentActivity;->onResume()V

    const-string/jumbo v0, "topic_find_people_1462"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_tabbed_ui_1358"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_people_screen_cluster_follow_1596"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/tz;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/android/tz;->n_()V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/TabbedFragmentActivity;->onTabChanged(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFindPeopleActivity;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/tz;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Lcom/twitter/android/tz;->e()V

    :cond_1
    return-void
.end method
