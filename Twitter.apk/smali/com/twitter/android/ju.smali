.class Lcom/twitter/android/ju;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, "connect_tab"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "vit_filters"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "notifications_follow_only"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const-string/jumbo v0, "connect_tab"

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->b(Lcom/twitter/android/MainActivity;)Z

    move-result v1

    if-eq v1, v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1, v0}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Z)Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->c(Lcom/twitter/android/MainActivity;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v1

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v3, v0}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Landroid/os/Bundle;)Lhb;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v3}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v1, v2, v0, v3}, Lcom/twitter/android/kf;->a(ILhb;Landroid/support/v4/app/FragmentManager;)V

    iget-object v0, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/kf;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/kr;->e()V

    iget-object v0, p0, Lcom/twitter/android/ju;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->e(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/km;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/km;->notifyDataSetChanged()V

    :cond_2
    return-void
.end method
