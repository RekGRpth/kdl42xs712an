.class public Lcom/twitter/android/ActivityDetailFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ob;


# instance fields
.field private A:Lcom/twitter/android/widget/cg;

.field private B:Lcom/twitter/android/aaa;

.field private C:Lcom/twitter/android/aaa;

.field private D:Lcom/twitter/android/ye;

.field private E:Lcom/twitter/android/ix;

.field private F:Z

.field private final a:Ljava/util/HashMap;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/ArrayList;

.field private e:J

.field private f:I

.field private g:I

.field private h:J

.field private i:J

.field private j:J

.field private k:Ljava/util/HashSet;

.field private l:Lcom/twitter/library/util/FriendshipCache;

.field private final m:Ljava/util/HashSet;

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:[I

.field private s:Z

.field private t:I

.field private u:I

.field private v:Z

.field private w:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

.field private x:Lcom/twitter/android/ClusterFollowAdapterData;

.field private y:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

.field private z:Lcom/twitter/android/ClusterFollowAdapterData;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, -0x1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->a:Ljava/util/HashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->d:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->m:Ljava/util/HashSet;

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->n:I

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->o:I

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->p:I

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->q:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ActivityDetailFragment;I)Lcom/twitter/android/ClusterFollowAdapterData;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/ActivityDetailFragment;->g(I)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/ActivityDetailFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ActivityDetailFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/lang/String;ZZ)Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-object p1

    :pswitch_1
    const-string/jumbo p1, "favorited_you"

    goto :goto_0

    :pswitch_2
    const-string/jumbo p1, "favorited_retweet"

    goto :goto_0

    :pswitch_3
    const-string/jumbo p1, "favorited_mention"

    goto :goto_0

    :pswitch_4
    const-string/jumbo p1, "retweeted_you"

    goto :goto_0

    :pswitch_5
    const-string/jumbo p1, "retweeted_retweet"

    goto :goto_0

    :pswitch_6
    const-string/jumbo p1, "retweeted_mention"

    goto :goto_0

    :pswitch_7
    if-eqz p2, :cond_0

    const-string/jumbo p1, "followed_you"

    goto :goto_0

    :pswitch_8
    if-eqz p2, :cond_0

    const-string/jumbo p1, "joined_twitter"

    goto :goto_0

    :pswitch_9
    if-eqz p3, :cond_0

    const-string/jumbo p1, "listed_you"

    goto :goto_0

    :pswitch_a
    const-string/jumbo p1, "media_tagged_you"

    goto :goto_0

    :pswitch_b
    const-string/jumbo p1, "favorited_media_tag"

    goto :goto_0

    :pswitch_c
    const-string/jumbo p1, "retweeted_media_tag"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_7
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_6
        :pswitch_3
        :pswitch_8
        :pswitch_0
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch
.end method

.method private a(Landroid/view/View;JLjava/lang/String;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-object v0, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "screen_name"

    invoke-virtual {v0}, Lcom/twitter/library/widget/BaseUserView;->getUserName()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    const/4 v2, 0x5

    iget-wide v3, p0, Lcom/twitter/android/ActivityDetailFragment;->Q:J

    iget-object v5, p0, Lcom/twitter/android/ActivityDetailFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ActivityDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ActivityDetailFragment;Landroid/view/View;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/ActivityDetailFragment;->a(Landroid/view/View;JLjava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/android/aaa;)V
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {p1}, Lcom/twitter/android/aaa;->getCount()I

    move-result v1

    invoke-direct {p0, p1}, Lcom/twitter/android/ActivityDetailFragment;->b(Lcom/twitter/android/aaa;)[Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch v1, :pswitch_data_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0f000f    # com.twitter.android.R.string.activity_by_others

    new-array v5, v5, [Ljava/lang/Object;

    aget-object v2, v2, v6

    aput-object v2, v5, v6

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f000e    # com.twitter.android.R.string.activity_by_one

    new-array v4, v7, [Ljava/lang/Object;

    aget-object v2, v2, v6

    aput-object v2, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f0010    # com.twitter.android.R.string.activity_by_two

    new-array v4, v5, [Ljava/lang/Object;

    aget-object v5, v2, v6

    aput-object v5, v4, v6

    aget-object v2, v2, v7

    aput-object v2, v4, v7

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/provider/Tweet;I)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v1, p1, v0, v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    add-int/lit8 v1, p2, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private a(Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v1}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, v0, v7, v6}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    if-eqz v0, :cond_1

    :goto_1
    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :goto_2
    return-void

    :pswitch_0
    const-string/jumbo v1, "magic_rec_favorited_by"

    const-string/jumbo v0, "magic_rec_favorited_by::user_module:user:follow"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "magic_rec_followed_by"

    const-string/jumbo v0, "magic_rec_followed_by::user_module:user:follow"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "magic_rec_retweeted_by"

    const-string/jumbo v0, "magic_rec_retweeted_by::user_module:user:follow"

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_1

    :pswitch_3
    goto :goto_2

    :pswitch_4
    const-string/jumbo v1, "favorited_by"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v1, "followed"

    goto :goto_0

    :pswitch_6
    const-string/jumbo v1, "retweeted_by"

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":::unfollow"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method private a(Ljava/lang/Boolean;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v1}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, v0, v7, v6}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    :goto_0
    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    invoke-virtual {v3, p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    new-array v4, v7, [Ljava/lang/String;

    if-eqz v0, :cond_2

    :goto_1
    aput-object v0, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_0

    new-array v3, v7, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v4, ":::follow_back"

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v3, v6

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    :goto_2
    return-void

    :pswitch_0
    const-string/jumbo v1, "magic_rec_favorited_by"

    const-string/jumbo v0, "magic_rec_favorited_by::user_module:user:follow"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "magic_rec_followed_by"

    const-string/jumbo v0, "magic_rec_followed_by::user_module:user:follow"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "magic_rec_retweeted_by"

    const-string/jumbo v0, "magic_rec_retweeted_by::user_module:user:follow"

    goto :goto_0

    :cond_1
    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_1

    :pswitch_3
    goto :goto_2

    :pswitch_4
    const-string/jumbo v1, "favorited_by"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v1, "followed"

    goto :goto_0

    :pswitch_6
    const-string/jumbo v1, "retweeted_by"

    goto :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v5, ":::follow"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_6
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/twitter/android/ActivityDetailFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method private b(J)V
    .locals 5

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->b:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    new-instance v3, Ljp;

    const/4 v4, -0x1

    invoke-direct {v3, v2, v0, v4, v1}, Ljp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/util/Collection;)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method private b(JJ)Z
    .locals 3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private b(Lcom/twitter/android/aaa;)[Ljava/lang/String;
    .locals 5

    const/4 v4, 0x3

    invoke-virtual {p1}, Lcom/twitter/android/aaa;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_1
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    aput-object v0, v1, v2

    move-object v0, v1

    goto :goto_0

    :cond_2
    const-string/jumbo v0, ""

    goto :goto_1
.end method

.method static synthetic c(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/aaa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    return-object v0
.end method

.method private c(J)V
    .locals 5

    const/4 v0, 0x0

    const/4 v4, 0x0

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v1}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, v0, v4, v4}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::stream::results"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_1
    return-void

    :pswitch_0
    const-string/jumbo v0, "magic_rec_followed_by::stream::results"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "magic_rec_favorited_by::stream::results"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "magic_rec_retweeted_by::stream::results"

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_1

    :pswitch_3
    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "favorited_by::stream::results"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "retweeted_by::stream::results"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
    .end packed-switch
.end method

.method private d(I)Lcom/twitter/android/aaa;
    .locals 20

    new-instance v8, Lcom/twitter/android/t;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v8, v0, v1}, Lcom/twitter/android/t;-><init>(Lcom/twitter/android/ActivityDetailFragment;I)V

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    if-eqz v2, :cond_0

    new-instance v2, Lcom/twitter/android/w;

    move-object/from16 v0, p0

    move/from16 v1, p1

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/w;-><init>(Lcom/twitter/android/ActivityDetailFragment;I)V

    sparse-switch p1, :sswitch_data_0

    const/4 v3, 0x0

    :goto_0
    new-instance v2, Lcom/twitter/android/ct;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v6

    const v7, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-direct/range {v2 .. v13}, Lcom/twitter/android/ct;-><init>(Lcom/twitter/android/ClusterFollowAdapterData;Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    :goto_1
    return-object v2

    :sswitch_0
    new-instance v3, Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ActivityDetailFragment;->w:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v3, v4, v5, v2, v6}, Lcom/twitter/android/ClusterFollowAdapterData;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/cu;Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ActivityDetailFragment;->x:Lcom/twitter/android/ClusterFollowAdapterData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ActivityDetailFragment;->x:Lcom/twitter/android/ClusterFollowAdapterData;

    goto :goto_0

    :sswitch_1
    new-instance v3, Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/ActivityDetailFragment;->y:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v3, v4, v5, v2, v6}, Lcom/twitter/android/ClusterFollowAdapterData;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/cu;Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/ActivityDetailFragment;->z:Lcom/twitter/android/ClusterFollowAdapterData;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/ActivityDetailFragment;->z:Lcom/twitter/android/ClusterFollowAdapterData;

    goto :goto_0

    :cond_0
    new-instance v4, Lcom/twitter/android/v;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    const/4 v6, 0x0

    const v7, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/twitter/android/ActivityDetailFragment;->v:Z

    move-object/from16 v0, p0

    iget v15, v0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/ActivityDetailFragment;->e:J

    move-wide/from16 v16, v0

    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/twitter/android/ActivityDetailFragment;->e:J

    const-wide/16 v18, 0x0

    cmp-long v2, v2, v18

    if-lez v2, :cond_1

    move-object/from16 v18, p0

    :goto_2
    invoke-direct/range {v4 .. v18}, Lcom/twitter/android/v;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZZIJLcom/twitter/android/ob;)V

    move-object v2, v4

    goto :goto_1

    :cond_1
    const/16 v18, 0x0

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic d(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/aaa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/client/au;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->M:Lcom/twitter/android/client/au;

    return-object v0
.end method

.method private e()Ljava/lang/String;
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v1}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v0, v2, v2}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/String;

    aput-object v0, v1, v2

    const/4 v0, 0x1

    const-string/jumbo v2, ":tweet:link:open_link"

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "favorited_by"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "retweeted_by"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method static synthetic f(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/client/au;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->M:Lcom/twitter/android/client/au;

    return-object v0
.end method

.method private g(I)Lcom/twitter/android/ClusterFollowAdapterData;
    .locals 1

    sparse-switch p1, :sswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->x:Lcom/twitter/android/ClusterFollowAdapterData;

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->z:Lcom/twitter/android/ClusterFollowAdapterData;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method static synthetic g(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/ye;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/ActivityDetailFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->m:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 3

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "connect"

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string/jumbo v0, "connect"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "favorited_by"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "followed_by"

    goto :goto_0

    :pswitch_3
    const-string/jumbo v0, "retweeted_by"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "list_member_added"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method

.method private r()[J
    .locals 5

    const/4 v4, 0x2

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-virtual {v1}, Lcom/twitter/android/aaa;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    if-eqz v1, :cond_2

    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/twitter/android/ActivityDetailFragment;->g(I)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/ClusterFollowAdapterData;->b()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    invoke-virtual {v1}, Lcom/twitter/android/aaa;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_3
    invoke-interface {v1, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_3

    :cond_4
    iget-boolean v1, p0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    if-eqz v1, :cond_5

    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcom/twitter/android/ActivityDetailFragment;->g(I)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/ClusterFollowAdapterData;->b()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    return-object v0
.end method

.method private s()Lcom/twitter/android/aaa;
    .locals 1

    const/16 v0, 0x8

    invoke-direct {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->d(I)Lcom/twitter/android/aaa;

    move-result-object v0

    return-object v0
.end method

.method private u()Lcom/twitter/android/aaa;
    .locals 1

    const/16 v0, 0xd

    invoke-direct {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->d(I)Lcom/twitter/android/aaa;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f030007    # com.twitter.android.R.layout.activity_detail_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/ActivityDetailFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-virtual {v2}, Lcom/twitter/android/aaa;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_0

    move v0, v1

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    invoke-virtual {v2}, Lcom/twitter/android/aaa;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    move v0, v1

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    invoke-virtual {v2}, Lcom/twitter/android/ye;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    invoke-virtual {v2}, Lcom/twitter/android/ix;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_3

    move v0, v1

    :cond_3
    if-eqz v0, :cond_4

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->b(I)V

    :cond_4
    iget-boolean v0, p0, Lcom/twitter/android/ActivityDetailFragment;->F:Z

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const/4 v1, 0x5

    if-eq v0, v1, :cond_6

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const/16 v1, 0xd

    if-eq v0, v1, :cond_6

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    :goto_1
    invoke-direct {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/android/aaa;)V

    :cond_5
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-virtual {v0, p2}, Lcom/twitter/android/aaa;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    invoke-virtual {v0, p2}, Lcom/twitter/android/aaa;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ye;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ix;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 4

    if-nez p2, :cond_1

    const-string/jumbo v0, "scribe_item"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    const-string/jumbo v1, "user_tag"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    if-eqz v0, :cond_0

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->d:Ljava/util/ArrayList;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p2, Lcom/twitter/library/provider/Tweet;->M:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-wide v2, p2, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/ActivityDetailFragment;->b(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->b:Ljava/util/ArrayList;

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v0, "position"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, p2, v0}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/library/provider/Tweet;I)V

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    goto :goto_0
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/ActivityDetailFragment;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/cg;->b(I)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->n:I

    if-eq v0, v1, :cond_2

    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->o:I

    if-ne v0, v1, :cond_3

    :cond_2
    const-string/jumbo v0, "user"

    invoke-direct {p0, p2, p4, p5, v0}, Lcom/twitter/android/ActivityDetailFragment;->a(Landroid/view/View;JLjava/lang/String;)V

    goto :goto_0

    :cond_3
    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->p:I

    if-ne v0, v1, :cond_6

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    new-instance v1, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v1, v0}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_4

    const/16 v0, 0x10

    iput v0, v1, Lcom/twitter/library/provider/Tweet;->P:I

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-direct {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->b(Lcom/twitter/android/aaa;)[Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_5

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iput-object v0, v1, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    :cond_5
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_6
    iget v1, p0, Lcom/twitter/android/ActivityDetailFragment;->q:I

    if-ne v0, v1, :cond_0

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 3

    const/4 v0, 0x1

    const/4 v1, 0x2

    iget v2, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v1, v2, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-virtual {v1}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    invoke-virtual {v1}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    invoke-virtual {v1}, Lcom/twitter/android/ix;->notifyDataSetChanged()V

    :cond_2
    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    if-nez p3, :cond_5

    :goto_0
    invoke-virtual {v1, p1, p2, v0}, Lcom/twitter/android/ye;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-virtual {v0, p2}, Lcom/twitter/android/aaa;->a(Ljava/util/HashMap;)V

    :cond_4
    return-void

    :cond_5
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    array-length v3, v2

    :goto_0
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    invoke-virtual {v1, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    array-length v3, v2

    :goto_1
    if-ge v0, v3, :cond_1

    aget v4, v2, v0

    invoke-virtual {v1, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    return-void
.end method

.method public a_()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0}, Lcom/twitter/android/widget/cg;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 15

    invoke-super/range {p0 .. p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v8

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0

    :pswitch_1
    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->s()Lcom/twitter/android/aaa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->n:I

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const/4 v2, 0x0

    aput v2, v0, v1

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/android/u;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/u;-><init>(Lcom/twitter/android/ActivityDetailFragment;Lcom/twitter/android/t;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->O:Lcom/twitter/library/client/j;

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->s()Lcom/twitter/android/aaa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->u()Lcom/twitter/android/aaa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->o:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->n:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->s()Lcom/twitter/android/aaa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    new-instance v0, Lcom/twitter/android/ye;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v8}, Lcom/twitter/android/client/c;->aa()Z

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x1

    iget-object v7, v8, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    new-instance v9, Lcom/twitter/android/yb;

    iget-object v10, p0, Lcom/twitter/android/ActivityDetailFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v11, p0, Lcom/twitter/android/ActivityDetailFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v12, "tweet"

    const-string/jumbo v13, "avatar"

    const-string/jumbo v14, "profile_click"

    invoke-static {v11, v12, v13, v14}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->e()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v9, p0, v10, v11, v12}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v10, 0x0

    const/4 v11, 0x0

    invoke-direct/range {v0 .. v11}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    invoke-virtual {v0, p0}, Lcom/twitter/android/ye;->b(Lcom/twitter/android/ob;)V

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->p:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->n:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    goto/16 :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->s()Lcom/twitter/android/aaa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    new-instance v0, Lcom/twitter/android/ix;

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v3, v8

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ix;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ZZ)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->n:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->q:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    goto/16 :goto_0

    :pswitch_4
    new-instance v0, Lcom/twitter/android/ix;

    const/4 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    move-object v3, v8

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ix;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ZZ)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->u()Lcom/twitter/android/aaa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    new-instance v0, Lcom/twitter/android/widget/cg;

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/twitter/android/widget/cg;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->q:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/ActivityDetailFragment;->o:I

    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->r:[I

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :array_0
    .array-data 4
        0x1
        0x0
    .end array-data

    :array_1
    .array-data 4
        0x2
        0x0
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x3
    .end array-data

    :array_3
    .array-data 4
        0x3
        0x1
    .end array-data
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    const-wide/16 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->a(JI)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->b(JI)V

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->A:Lcom/twitter/android/widget/cg;

    invoke-virtual {v0}, Lcom/twitter/android/widget/cg;->notifyDataSetChanged()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 9

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getScribeItem()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v0}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->k:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3, v1}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    invoke-direct {p0, v2}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/library/scribe/ScribeItem;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    if-eqz v1, :cond_3

    const/4 v3, 0x0

    invoke-virtual {v0, p2, p3, v3, v1}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/ActivityDetailFragment;->d(Ljava/lang/String;)V

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/zx;

    iget v1, v1, Lcom/twitter/android/zx;->e:I

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/Boolean;Lcom/twitter/library/scribe/ScribeItem;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/ActivityDetailFragment;->t:I

    invoke-static {v1, v2}, Lcom/twitter/android/util/c;->b(Landroid/content/Context;I)V

    iget-boolean v1, p0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    if-eqz v1, :cond_1

    invoke-direct {p0, p4}, Lcom/twitter/android/ActivityDetailFragment;->g(I)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v2

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/zx;

    iget-wide v6, v1, Lcom/twitter/android/zx;->c:J

    invoke-virtual {v2, v6, v7}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v3, 0x1

    :goto_2
    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->r()[J

    move-result-object v2

    iget v8, p0, Lcom/twitter/android/ActivityDetailFragment;->u:I

    move v1, p4

    move-wide v4, p2

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/c;->a(I[JIJJI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p4}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->k:Ljava/util/HashSet;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    const/4 v3, 0x3

    goto :goto_2
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const/4 v8, -0x1

    const-wide/16 v3, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "magic_rec_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityDetailFragment;->e:J

    const-string/jumbo v1, "event_type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    const-string/jumbo v1, "user_tag"

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityDetailFragment;->h:J

    const-string/jumbo v1, "status_tag"

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityDetailFragment;->i:J

    const-string/jumbo v1, "list_id"

    invoke-virtual {v0, v1, v3, v4}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ActivityDetailFragment;->j:J

    const-string/jumbo v1, "cluster_follow"

    invoke-virtual {v0, v1, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    const-string/jumbo v1, "cluster_follow_experiment"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->t:I

    const-string/jumbo v1, "cluster_follow_type"

    invoke-virtual {v0, v1, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/ActivityDetailFragment;->u:I

    iput-boolean v5, p0, Lcom/twitter/android/ActivityDetailFragment;->F:Z

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->k:Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    if-eqz p1, :cond_2

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "friendship_cache"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/FriendshipCache;

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    :goto_0
    const-string/jumbo v0, "hide_action_button"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ActivityDetailFragment;->v:Z

    const-string/jumbo v0, "state_source_cluster_recommendations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->w:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    const-string/jumbo v0, "state_target_cluster_recommendations"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->y:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    :cond_0
    :goto_1
    invoke-virtual {p0, v5, p0}, Lcom/twitter/android/ActivityDetailFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0, v7, p0}, Lcom/twitter/android/ActivityDetailFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-direct {p0}, Lcom/twitter/android/ActivityDetailFragment;->q()V

    return-void

    :cond_1
    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    goto :goto_0

    :cond_2
    const-string/jumbo v2, "hide_action_button"

    invoke-virtual {v0, v2, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ActivityDetailFragment;->v:Z

    new-instance v0, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v0}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->g:I

    invoke-static {v0}, Lcom/twitter/library/provider/x;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const/16 v2, 0xd

    if-eq v0, v2, :cond_3

    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_4

    :cond_3
    iput-boolean v6, p0, Lcom/twitter/android/ActivityDetailFragment;->F:Z

    :cond_4
    const/4 v0, 0x0

    invoke-direct {p0, v0, v5, v5}, Lcom/twitter/android/ActivityDetailFragment;->a(Ljava/lang/String;ZZ)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    aput-object v0, v4, v6

    const-string/jumbo v0, ":::impression"

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_1

    :cond_5
    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_2
    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    aput-object v0, v4, v6

    const-string/jumbo v0, ":::impression"

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    const-string/jumbo v0, "favorited"

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "followed"

    goto :goto_2

    :pswitch_3
    const-string/jumbo v0, "listed"

    iput-boolean v6, p0, Lcom/twitter/android/ActivityDetailFragment;->F:Z

    goto :goto_2

    :pswitch_4
    const-string/jumbo v0, "retweeted"

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_4
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9

    const/4 v5, 0x1

    const/4 v8, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    packed-switch p1, :pswitch_data_0

    :cond_0
    move-object v0, v4

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v4, Lcom/twitter/library/provider/ax;->n:Landroid/net/Uri;

    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/cm;->a:[Ljava/lang/String;

    const-string/jumbo v4, "tag=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/ActivityDetailFragment;->h:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const-string/jumbo v6, "_id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v4, Lcom/twitter/library/provider/ax;->o:Landroid/net/Uri;

    invoke-static {v4, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/cm;->a:[Ljava/lang/String;

    const-string/jumbo v4, "tag=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/ActivityDetailFragment;->h:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const-string/jumbo v6, "_id ASC"

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/ActivityDetailFragment;->f:I

    packed-switch v0, :pswitch_data_1

    :pswitch_3
    move-object v2, v4

    :goto_1
    if-eqz v2, :cond_0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    const-string/jumbo v4, "tag=?"

    new-array v5, v5, [Ljava/lang/String;

    iget-wide v6, p0, Lcom/twitter/android/ActivityDetailFragment;->i:J

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v8

    const-string/jumbo v6, "_id ASC"

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/twitter/library/provider/at;->o:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    :pswitch_5
    sget-object v0, Lcom/twitter/library/provider/at;->p:Landroid/net/Uri;

    invoke-static {v0, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    :pswitch_6
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v5, Lcom/twitter/library/provider/ai;->b:Landroid/net/Uri;

    iget-wide v6, p0, Lcom/twitter/android/ActivityDetailFragment;->j:J

    invoke-static {v5, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5, v2, v3}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/iz;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_6
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_4
    .end packed-switch
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ActivityDetailFragment;->b(ILcom/twitter/library/util/ar;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/ActivityDetailFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/ActivityDetailFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->B:Lcom/twitter/android/aaa;

    invoke-virtual {v0, v1}, Lcom/twitter/android/aaa;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->C:Lcom/twitter/android/aaa;

    invoke-virtual {v0, v1}, Lcom/twitter/android/aaa;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->D:Lcom/twitter/android/ye;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->E:Lcom/twitter/android/ix;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ix;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityDetailFragment;->a(Z)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "hide_action_button"

    iget-boolean v1, p0, Lcom/twitter/android/ActivityDetailFragment;->v:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0}, Lcom/twitter/library/util/FriendshipCache;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "friendship_cache"

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->l:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/ActivityDetailFragment;->s:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->x:Lcom/twitter/android/ClusterFollowAdapterData;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "state_source_cluster_recommendations"

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->x:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v1}, Lcom/twitter/android/ClusterFollowAdapterData;->c()Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->z:Lcom/twitter/android/ClusterFollowAdapterData;

    if-eqz v0, :cond_2

    const-string/jumbo v0, "state_target_cluster_recommendations"

    iget-object v1, p0, Lcom/twitter/android/ActivityDetailFragment;->z:Lcom/twitter/android/ClusterFollowAdapterData;

    invoke-virtual {v1}, Lcom/twitter/android/ClusterFollowAdapterData;->c()Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    return-void
.end method

.method public onStop()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->k:Ljava/util/HashSet;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityDetailFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a([J)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ActivityDetailFragment;->k:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/ActivityDetailFragment;->b(J)V

    invoke-direct {p0, v1, v2}, Lcom/twitter/android/ActivityDetailFragment;->c(J)V

    return-void
.end method
