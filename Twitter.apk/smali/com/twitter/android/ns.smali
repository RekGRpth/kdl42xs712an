.class public Lcom/twitter/android/ns;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/NearbyFragment;

.field private final b:I

.field private c:Z

.field private d:I

.field private e:I

.field private f:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/NearbyFragment;Landroid/content/Context;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ns;->b:I

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    const/4 v2, -0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->j(Lcom/twitter/android/NearbyFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->g(Lcom/twitter/android/NearbyFragment;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/ns;->e:I

    iput v0, p0, Lcom/twitter/android/ns;->f:I

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ns;->d:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/ns;->d:I

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->findPointerIndex(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    invoke-virtual {p2, v1}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ns;->d:I

    move v0, v1

    :cond_2
    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v0

    float-to-int v7, v0

    iget-boolean v0, p0, Lcom/twitter/android/ns;->c:Z

    if-nez v0, :cond_3

    iget v0, p0, Lcom/twitter/android/ns;->e:I

    sub-int/2addr v0, v7

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/twitter/android/ns;->b:I

    if-le v0, v2, :cond_3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/ns;->c:Z

    :cond_3
    iget-boolean v0, p0, Lcom/twitter/android/ns;->c:Z

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/android/ns;->f:I

    sub-int v0, v7, v0

    div-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_4

    iget-object v2, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->k(Lcom/twitter/android/NearbyFragment;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->l(Lcom/twitter/android/NearbyFragment;)Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->m(Lcom/twitter/android/NearbyFragment;)I

    move-result v2

    add-int/2addr v0, v2

    iget-object v2, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->n(Lcom/twitter/android/NearbyFragment;)I

    move-result v2

    if-ge v0, v2, :cond_5

    iget-object v0, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->n(Lcom/twitter/android/NearbyFragment;)I

    move-result v0

    move v3, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    iget-object v2, p0, Lcom/twitter/android/ns;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->m(Lcom/twitter/android/NearbyFragment;)I

    move-result v2

    int-to-float v2, v2

    int-to-float v4, v3

    const-wide/16 v5, 0x0

    move v3, v1

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/NearbyFragment;->a(Lcom/twitter/android/NearbyFragment;IFIFJ)V

    :cond_4
    iput v7, p0, Lcom/twitter/android/ns;->f:I

    goto/16 :goto_0

    :pswitch_3
    iput-boolean v1, p0, Lcom/twitter/android/ns;->c:Z

    iput v2, p0, Lcom/twitter/android/ns;->d:I

    goto/16 :goto_0

    :pswitch_4
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getY(I)F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p2, v0}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ns;->d:I

    iput v2, p0, Lcom/twitter/android/ns;->f:I

    goto/16 :goto_0

    :cond_5
    move v3, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method
