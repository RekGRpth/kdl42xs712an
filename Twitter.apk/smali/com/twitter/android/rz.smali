.class Lcom/twitter/android/rz;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/SearchActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/SearchActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/SearchActivity;Lcom/twitter/android/rv;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/rz;-><init>(Lcom/twitter/android/SearchActivity;)V

    return-void
.end method


# virtual methods
.method public j(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/SearchActivity;->a(Lcom/twitter/android/SearchActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    const v1, 0x7f0f0388    # com.twitter.android.R.string.save_search_confirmation

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SearchFragment;->d(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    const v1, 0x7f0f0389    # com.twitter.android.R.string.save_search_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public k(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/SearchActivity;->b(Lcom/twitter/android/SearchActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    const v1, 0x7f0f00f4    # com.twitter.android.R.string.delete_search_confirmation

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    iget-object v0, v0, Lcom/twitter/android/SearchActivity;->a:Lcom/twitter/android/SearchFragment;

    invoke-virtual {v0, v2}, Lcom/twitter/android/SearchFragment;->d(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/rz;->a:Lcom/twitter/android/SearchActivity;

    const v1, 0x7f0f00f5    # com.twitter.android.R.string.delete_search_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
