.class public Lcom/twitter/android/LoginVerificationFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/library/client/Session;

.field private b:[Landroid/text/style/StyleSpan;

.field private c:Lcom/twitter/android/jm;

.field private d:Landroid/app/ProgressDialog;

.field private e:Landroid/widget/ArrayAdapter;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method

.method private static a([I)I
    .locals 2

    const/4 v0, 0x0

    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    aget v0, p0, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/LoginVerificationFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/LoginVerificationFragment;->q()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginVerificationFragment;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/LoginVerificationFragment;->d(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/LoginVerificationFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/jm;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    if-ge v1, v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/account/LoginVerificationRequest;

    if-eqz p1, :cond_3

    if-eqz v0, :cond_3

    iget-object v2, v0, Lcom/twitter/library/api/account/LoginVerificationRequest;->b:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v0}, Landroid/widget/ArrayAdapter;->remove(Ljava/lang/Object;)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/jm;

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1
.end method

.method private b([I)V
    .locals 2

    invoke-static {p1}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    const v0, 0x7f0f0242    # com.twitter.android.R.string.login_verification_currently_unavailable

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->d(I)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f024e    # com.twitter.android.R.string.login_verification_please_reenroll_title

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f024d    # com.twitter.android.R.string.login_verification_please_reenroll

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x104000a    # android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->h(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0f0251    # com.twitter.android.R.string.login_verification_request_not_found

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->d(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0xeb
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/LoginVerificationFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private d(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/LoginVerificationFragment;)[Landroid/text/style/StyleSpan;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->b:[Landroid/text/style/StyleSpan;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/LoginVerificationFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 3

    new-instance v0, Lcom/twitter/library/api/account/d;

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/account/d;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v1, 0x1

    const/4 v2, 0x3

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/LoginVerificationFragment;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method


# virtual methods
.method a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030003    # com.twitter.android.R.layout.account_row_view

    invoke-virtual {v0, v1, p1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f090068    # com.twitter.android.R.id.checkmark

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/4 v2, 0x4

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    move-object v0, v1

    check-cast v0, Lcom/twitter/library/widget/UserView;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    iget-object v3, v2, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v3, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/twitter/library/widget/UserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/UserView;->setVerified(Z)V

    invoke-virtual {v0, v5}, Lcom/twitter/library/widget/UserView;->setProtected(Z)V

    return-object v1

    :cond_0
    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method protected a(IILcom/twitter/library/service/b;)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseListFragment;->a(IILcom/twitter/library/service/b;)V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login_verification::::get_newer"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 11

    const/16 v10, 0x58

    const/16 v4, 0xc8

    const/4 v9, 0x1

    const/4 v8, 0x0

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->e()Lcom/twitter/internal/network/k;

    move-result-object v0

    iget v1, v0, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v7

    packed-switch p2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->u()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(I)V

    if-ne v1, v4, :cond_1

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::get_requests::success"

    aput-object v3, v2, v8

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    move-object v6, v0

    :goto_1
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    check-cast p4, Lcom/twitter/library/api/account/d;

    invoke-virtual {p4}, Lcom/twitter/library/api/account/d;->e()Ljava/util/ArrayList;

    move-result-object v5

    if-ne v1, v4, :cond_2

    if-eqz v5, :cond_2

    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    sget-object v0, Lcom/twitter/library/api/account/LoginVerificationRequest;->a:Lcom/twitter/library/api/account/LoginVerificationRequest;

    invoke-virtual {v5, v8, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    new-instance v0, Lcom/twitter/android/jp;

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0300b1    # com.twitter.android.R.layout.login_verification_request_row_view

    const v4, 0x7f0901c8    # com.twitter.android.R.id.login_verification_request_content

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/jp;-><init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;IILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    :cond_0
    :goto_2
    invoke-virtual {v7, v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::get_requests::failure"

    aput-object v3, v2, v8

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    move-object v6, v0

    goto :goto_1

    :cond_2
    const-string/jumbo v0, "0"

    invoke-virtual {v6, v0}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/jm;

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->e:Landroid/widget/ArrayAdapter;

    if-eq v1, v4, :cond_0

    invoke-virtual {p4}, Lcom/twitter/library/api/account/d;->f()[I

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v0

    if-ne v0, v10, :cond_3

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::get_requests::rate_limit"

    aput-object v4, v3, v8

    invoke-virtual {v7, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_3
    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const v0, 0x7f0f0242    # com.twitter.android.R.string.login_verification_currently_unavailable

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->d(I)V

    goto :goto_2

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->e()V

    if-ne v1, v4, :cond_4

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::request:reject:success"

    aput-object v3, v2, v8

    invoke-virtual {v7, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f0f0252    # com.twitter.android.R.string.login_verification_request_rejected

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->d(I)V

    check-cast p4, Lcom/twitter/library/api/account/t;

    iget-object v0, p4, Lcom/twitter/library/api/account/t;->d:Lcom/twitter/library/api/account/l;

    iget-object v0, v0, Lcom/twitter/library/api/account/l;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_4
    check-cast p4, Lcom/twitter/library/api/account/t;

    invoke-virtual {p4}, Lcom/twitter/library/api/account/t;->e()[I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b([I)V

    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v0

    if-ne v0, v10, :cond_5

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, "login_verification::request:reject:rate_limit"

    aput-object v5, v4, v8

    invoke-virtual {v7, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_5
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:reject:failure"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->e()V

    if-ne v1, v4, :cond_6

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v2, v9, [Ljava/lang/String;

    const-string/jumbo v3, "login_verification::request:accept:success"

    aput-object v3, v2, v8

    invoke-virtual {v7, v0, v1, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f0f0250    # com.twitter.android.R.string.login_verification_request_accepted

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->d(I)V

    check-cast p4, Lcom/twitter/library/api/account/a;

    iget-object v0, p4, Lcom/twitter/library/api/account/a;->d:Lcom/twitter/library/api/account/l;

    iget-object v0, v0, Lcom/twitter/library/api/account/l;->a:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    check-cast p4, Lcom/twitter/library/api/account/a;

    invoke-virtual {p4}, Lcom/twitter/library/api/account/a;->e()[I

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/LoginVerificationFragment;->b([I)V

    invoke-static {v0}, Lcom/twitter/android/LoginVerificationFragment;->a([I)I

    move-result v0

    if-ne v0, v10, :cond_7

    iget-object v2, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v9, [Ljava/lang/String;

    const-string/jumbo v5, "login_verification::request:accept:rate_limit"

    aput-object v5, v4, v8

    invoke-virtual {v7, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_7
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v9, [Ljava/lang/String;

    const-string/jumbo v4, "login_verification::request:accept:failure"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method a(Ljava/lang/String;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, p1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method

.method e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    return-void
.end method

.method protected i()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/LoginVerificationFragment;->q()V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    const v3, 0x7f0f025a    # com.twitter.android.R.string.login_verifications_empty

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    new-instance v5, Ljava/util/ArrayList;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginVerificationFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    new-instance v0, Lcom/twitter/android/jm;

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0300b0    # com.twitter.android.R.layout.login_verification_request_empty_row_view

    const v4, 0x7f0901c8    # com.twitter.android.R.id.login_verification_request_content

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/jm;-><init>(Lcom/twitter/android/LoginVerificationFragment;Landroid/content/Context;IILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->c:Lcom/twitter/android/jm;

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/LoginVerificationFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "lv_account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    :goto_0
    new-instance v0, Lcom/twitter/android/jn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/jn;-><init>(Lcom/twitter/android/LoginVerificationFragment;Lcom/twitter/android/jk;)V

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->O:Lcom/twitter/library/client/j;

    const/4 v0, 0x3

    new-array v0, v0, [Landroid/text/style/StyleSpan;

    const/4 v1, 0x0

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v2, v0, v1

    new-instance v1, Landroid/text/style/StyleSpan;

    invoke-direct {v1, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v1, v0, v3

    const/4 v1, 0x2

    new-instance v2, Landroid/text/style/StyleSpan;

    invoke-direct {v2, v3}, Landroid/text/style/StyleSpan;-><init>(I)V

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->b:[Landroid/text/style/StyleSpan;

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    goto :goto_0
.end method

.method public onResume()V
    .locals 4

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/LoginVerificationFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "login_verification::::impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getCallingActivity()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, ".twitter."

    invoke-virtual {v1, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string/jumbo v1, "settings"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginVerificationFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    invoke-direct {p0}, Lcom/twitter/android/LoginVerificationFragment;->q()V

    return-void

    :cond_0
    const-string/jumbo v1, "push"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_0
.end method
