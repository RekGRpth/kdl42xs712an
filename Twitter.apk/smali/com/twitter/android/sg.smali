.class Lcom/twitter/android/sg;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ob;


# instance fields
.field final synthetic a:Lcom/twitter/android/SearchPhotosFragment;

.field private final b:Ljava/util/HashSet;


# direct methods
.method private constructor <init>(Lcom/twitter/android/SearchPhotosFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/sg;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/sg;->b:Ljava/util/HashSet;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/SearchPhotosFragment;Lcom/twitter/android/sf;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/sg;-><init>(Lcom/twitter/android/SearchPhotosFragment;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-object v1, p0, Lcom/twitter/android/sg;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchPhotosFragment;->b(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/HashSet;

    move-result-object v1

    iget-wide v2, v0, Lcom/twitter/android/ok;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, v0, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/sg;->b:Ljava/util/HashSet;

    iget-object v3, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/sg;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchPhotosFragment;->c(Lcom/twitter/android/SearchPhotosFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/ok;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/sg;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchPhotosFragment;->d(Lcom/twitter/android/SearchPhotosFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-static {v4, v0, v1, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sg;->a:Lcom/twitter/android/SearchPhotosFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchPhotosFragment;->e(Lcom/twitter/android/SearchPhotosFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    return-void
.end method
