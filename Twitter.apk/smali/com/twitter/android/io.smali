.class public Lcom/twitter/android/io;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private a:Ljava/util/List;

.field private b:Lcom/twitter/android/client/c;

.field private c:Ljava/util/HashSet;

.field private d:Ljava/util/Map;

.field private e:Lcom/twitter/android/in;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Ljava/util/HashSet;ZLcom/twitter/android/in;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;ILjava/util/List;)V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/io;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/twitter/android/io;->c:Ljava/util/HashSet;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/io;->b:Lcom/twitter/android/client/c;

    iput-object p5, p0, Lcom/twitter/android/io;->e:Lcom/twitter/android/in;

    iput-boolean p4, p0, Lcom/twitter/android/io;->f:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/io;->d:Ljava/util/Map;

    return-void
.end method


# virtual methods
.method public a()Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/io;->c:Ljava/util/HashSet;

    return-object v0
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/io;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/InReplyToDialogRowLayout;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/android/InReplyToDialogRowLayout;->setProfileImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 5

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-object v2, p0, Lcom/twitter/android/io;->d:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/io;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/InReplyToDialogRowLayout;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->getImageUrl()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/io;->d:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->getScreenName()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    if-eqz v1, :cond_1

    iget-object v3, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/android/InReplyToDialogRowLayout;->setImageUrl(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/twitter/android/io;->b:Lcom/twitter/android/client/c;

    iget-object v4, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/android/client/c;->h(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/InReplyToDialogRowLayout;->setProfileImage(Landroid/graphics/Bitmap;)V

    iget-boolean v1, v1, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/InReplyToDialogRowLayout;->setVerified(Z)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/io;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v3, v0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    if-nez p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/io;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v4, 0x7f03009b    # com.twitter.android.R.layout.in_reply_to_dialog_row

    invoke-virtual {v1, v4, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/InReplyToDialogRowLayout;

    iget-object v4, p0, Lcom/twitter/android/io;->a:Ljava/util/List;

    new-instance v5, Ljava/lang/ref/WeakReference;

    invoke-direct {v5, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object p2, v1

    :goto_0
    invoke-virtual {p2}, Lcom/twitter/android/InReplyToDialogRowLayout;->a()V

    iget-object v1, p0, Lcom/twitter/android/io;->d:Ljava/util/Map;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TwitterUser;

    iget-object v3, v0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/twitter/android/InReplyToDialogRowLayout;->setScreenName(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/twitter/library/api/RepliedUser;->name:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/twitter/android/InReplyToDialogRowLayout;->setAuthorName(Ljava/lang/String;)V

    iget-wide v3, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-virtual {p2, v3, v4}, Lcom/twitter/android/InReplyToDialogRowLayout;->setUserId(J)V

    if-eqz v1, :cond_1

    iget-object v3, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p2, v3}, Lcom/twitter/android/InReplyToDialogRowLayout;->setImageUrl(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/twitter/android/io;->b:Lcom/twitter/android/client/c;

    iget-object v4, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/android/client/c;->h(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {p2, v3}, Lcom/twitter/android/InReplyToDialogRowLayout;->setProfileImage(Landroid/graphics/Bitmap;)V

    iget-boolean v1, v1, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {p2, v1}, Lcom/twitter/android/InReplyToDialogRowLayout;->setVerified(Z)V

    :goto_1
    iget-boolean v1, p0, Lcom/twitter/android/io;->f:Z

    if-eqz v1, :cond_2

    invoke-virtual {p2, v2}, Lcom/twitter/android/InReplyToDialogRowLayout;->setCheckboxVisible(Z)V

    :goto_2
    invoke-virtual {p2, p0}, Lcom/twitter/android/InReplyToDialogRowLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-object p2

    :cond_0
    check-cast p2, Lcom/twitter/android/InReplyToDialogRowLayout;

    goto :goto_0

    :cond_1
    invoke-virtual {p2, v6}, Lcom/twitter/android/InReplyToDialogRowLayout;->setImageUrl(Ljava/lang/String;)V

    invoke-virtual {p2, v6}, Lcom/twitter/android/InReplyToDialogRowLayout;->setProfileImage(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/io;->c:Ljava/util/HashSet;

    iget-wide v3, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->setChecked(Z)V

    if-eqz p1, :cond_3

    const/4 v0, 0x1

    :goto_3
    invoke-virtual {p2, v0}, Lcom/twitter/android/InReplyToDialogRowLayout;->setEnabled(Z)V

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    check-cast p1, Lcom/twitter/android/InReplyToDialogRowLayout;

    iget-boolean v0, p0, Lcom/twitter/android/io;->f:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/io;->e:Lcom/twitter/android/in;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/io;->e:Lcom/twitter/android/in;

    invoke-virtual {p1}, Lcom/twitter/android/InReplyToDialogRowLayout;->getUserId()J

    move-result-wide v1

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/in;->a(J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/android/InReplyToDialogRowLayout;->b()V

    invoke-virtual {p1}, Lcom/twitter/android/InReplyToDialogRowLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/io;->c:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/twitter/android/InReplyToDialogRowLayout;->getUserId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/io;->c:Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/twitter/android/InReplyToDialogRowLayout;->getUserId()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method
