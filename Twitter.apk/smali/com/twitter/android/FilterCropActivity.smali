.class public Lcom/twitter/android/FilterCropActivity;
.super Lcom/twitter/android/CropActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/library/widget/e;


# instance fields
.field private d:Landroid/widget/ImageView;

.field private e:Landroid/widget/ImageView;

.field private f:Landroid/widget/ImageView;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/CropActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/FilterCropActivity;->g:I

    return-void
.end method

.method private a(I)V
    .locals 2

    iget v0, p0, Lcom/twitter/android/FilterCropActivity;->g:I

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput p1, p0, Lcom/twitter/android/FilterCropActivity;->g:I

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->d:Landroid/widget/ImageView;

    const v1, 0x7f02018a    # com.twitter.android.R.drawable.ic_filters_crop_original_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->e:Landroid/widget/ImageView;

    const v1, 0x7f02019a    # com.twitter.android.R.drawable.ic_filters_crop_wide_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->f:Landroid/widget/ImageView;

    const v1, 0x7f020194    # com.twitter.android.R.drawable.ic_filters_crop_square_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->d:Landroid/widget/ImageView;

    const v1, 0x7f02018b    # com.twitter.android.R.drawable.ic_filters_crop_original_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->e:Landroid/widget/ImageView;

    const v1, 0x7f02019b    # com.twitter.android.R.drawable.ic_filters_crop_wide_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->f:Landroid/widget/ImageView;

    const v1, 0x7f020195    # com.twitter.android.R.drawable.ic_filters_crop_square_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;

    move-result-object v0

    const v1, 0x7f030071    # com.twitter.android.R.layout.filter_crop_view

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/CropActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/CroppableImageView;->setCropListener(Lcom/twitter/library/widget/e;)V

    const v0, 0x7f090175    # com.twitter.android.R.id.rotate

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09016f    # com.twitter.android.R.id.orig_crop_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090171    # com.twitter.android.R.id.wide_crop_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090173    # com.twitter.android.R.id.square_crop_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090170    # com.twitter.android.R.id.orig_crop_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/FilterCropActivity;->d:Landroid/widget/ImageView;

    const v0, 0x7f090172    # com.twitter.android.R.id.wide_crop_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/FilterCropActivity;->e:Landroid/widget/ImageView;

    const v0, 0x7f090174    # com.twitter.android.R.id.square_crop_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/FilterCropActivity;->f:Landroid/widget/ImageView;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "crop_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterCropActivity;->a(I)V

    :cond_0
    const v0, 0x7f0f00e5    # com.twitter.android.R.string.crop_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterCropActivity;->setTitle(I)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/FilterCropActivity;->V()V

    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->a(Z)V

    if-eqz p1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterCropActivity;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    iget-object v1, p0, Lcom/twitter/android/FilterCropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v1}, Lcom/twitter/android/CropManager;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setCropAspectRatio(F)V

    :cond_0
    return-void
.end method

.method protected a(ZLandroid/content/Intent;I)V
    .locals 2

    const-string/jumbo v0, "crop_type"

    iget v1, p0, Lcom/twitter/android/FilterCropActivity;->g:I

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/CropActivity;->a(ZLandroid/content/Intent;I)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    const v0, 0x7f09030c    # com.twitter.android.R.id.menu_crop

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->b:Lcom/twitter/android/CropManager;

    iget-object v0, v0, Lcom/twitter/android/CropManager;->a:Lcom/twitter/android/dc;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lhn;->c(Z)Lhn;

    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/CropActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f11000c    # com.twitter.android.R.menu.filter_crop

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f09030c    # com.twitter.android.R.id.menu_crop

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/FilterCropActivity;->a()V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v1, v2, :cond_1

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/twitter/android/FilterCropActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/FilterCropActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterCropActivity;->a(I)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090175    # com.twitter.android.R.id.rotate

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    const/16 v1, -0x5a

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/CroppableImageView;->a(IZ)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f09016f    # com.twitter.android.R.id.orig_crop_button

    if-ne v0, v1, :cond_2

    invoke-direct {p0, v2}, Lcom/twitter/android/FilterCropActivity;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    iget-object v1, p0, Lcom/twitter/android/FilterCropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v1}, Lcom/twitter/android/CropManager;->d()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setCropAspectRatio(F)V

    goto :goto_0

    :cond_2
    const v1, 0x7f090171    # com.twitter.android.R.id.wide_crop_button

    if-ne v0, v1, :cond_3

    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterCropActivity;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    const/high16 v1, 0x3f100000    # 0.5625f

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setCropAspectRatio(F)V

    goto :goto_0

    :cond_3
    const v1, 0x7f090173    # com.twitter.android.R.id.square_crop_button

    if-ne v0, v1, :cond_0

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/twitter/android/FilterCropActivity;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/FilterCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setCropAspectRatio(F)V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "crop_type"

    iget v1, p0, Lcom/twitter/android/FilterCropActivity;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
