.class public Lcom/twitter/android/MessagesComposeFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field public a:Landroid/widget/AutoCompleteTextView;

.field protected b:J

.field private c:Lcom/twitter/android/ym;

.field private d:Lcom/twitter/android/mk;

.field private e:Z

.field private f:Landroid/view/View;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/android/mk;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->d:Lcom/twitter/android/mk;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/MessagesComposeFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/MessagesComposeFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    return-object v0
.end method

.method private e()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    return-void
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300c5    # com.twitter.android.R.layout.messages_compose_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/MessagesComposeFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 6

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->d:Lcom/twitter/android/mk;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "messages:compose:list:user:select"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/twitter/android/MessagesComposeFragment;->d:Lcom/twitter/android/mk;

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    const/4 v3, 0x3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v3, v4, v0}, Lcom/twitter/android/mk;->a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/mk;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/MessagesComposeFragment;->d:Lcom/twitter/android/mk;

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->c:Lcom/twitter/android/ym;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->c:Lcom/twitter/android/ym;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ym;->a(Ljava/util/HashMap;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ao;

    invoke-virtual {v0, p2}, Lcom/twitter/android/ao;->a(Ljava/util/HashMap;)V

    :cond_1
    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 12

    const/4 v4, 0x0

    const/4 v11, 0x1

    const/4 v10, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesComposeFragment;->h(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v3, 0x7f030134    # com.twitter.android.R.layout.section_header

    invoke-static {v0, v3, v4}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v2, v0, v4, v10}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-instance v5, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "dm"

    invoke-direct {v5, v6, v7, v8}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v6, "followers_timestamp"

    const-wide/16 v7, 0x0

    invoke-virtual {v5, v6, v7, v8}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    add-long/2addr v6, v8

    cmp-long v6, v6, v3

    if-gez v6, :cond_0

    invoke-virtual {v5}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v5

    const-string/jumbo v6, "followers_timestamp"

    invoke-virtual {v5, v6, v3, v4}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/f;->d()V

    const/16 v3, 0x190

    invoke-virtual {v1, v0, v10, v11, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;III)Ljava/lang/String;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v3, 0x7f0901e7    # com.twitter.android.R.id.to_username

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/AutoCompleteTextView;

    new-instance v3, Lcom/twitter/android/mh;

    invoke-direct {v3, p0}, Lcom/twitter/android/mh;-><init>(Lcom/twitter/android/MessagesComposeFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v3, Lcom/twitter/android/mi;

    invoke-direct {v3, p0, v1, v2}, Lcom/twitter/android/mi;-><init>(Lcom/twitter/android/MessagesComposeFragment;Lcom/twitter/android/client/c;Landroid/widget/ListView;)V

    invoke-virtual {v0, v3}, Landroid/widget/AutoCompleteTextView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    new-instance v1, Lcom/twitter/android/ym;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    sget-object v4, Lcom/twitter/android/provider/SuggestionsProvider;->e:Landroid/net/Uri;

    invoke-direct {v1, v3, v4}, Lcom/twitter/android/ym;-><init>(Landroid/app/Activity;Landroid/net/Uri;)V

    iput-object v1, p0, Lcom/twitter/android/MessagesComposeFragment;->c:Lcom/twitter/android/ym;

    iget-object v1, p0, Lcom/twitter/android/MessagesComposeFragment;->c:Lcom/twitter/android/ym;

    invoke-virtual {v0, v1}, Landroid/widget/AutoCompleteTextView;->setAdapter(Landroid/widget/ListAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->a:Landroid/widget/AutoCompleteTextView;

    invoke-virtual {v0, p0}, Landroid/widget/AutoCompleteTextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v0, Lcom/twitter/android/ve;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/ve;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    iput-boolean v11, p0, Lcom/twitter/android/MessagesComposeFragment;->e:Z

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f0901e8    # com.twitter.android.R.id.list_disabler

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_1
    invoke-virtual {p0, v10}, Lcom/twitter/android/MessagesComposeFragment;->a_(Z)V

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    if-ne p1, v0, :cond_0

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    new-instance v1, Lcom/twitter/android/mj;

    invoke-direct {v1, p0}, Lcom/twitter/android/mj;-><init>(Lcom/twitter/android/MessagesComposeFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v1, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->requestFocus()Z

    invoke-direct {p0}, Lcom/twitter/android/MessagesComposeFragment;->e()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MessagesComposeFragment;->e:Z

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v4, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    if-eqz v0, :cond_0

    const-string/jumbo v3, "owner_id"

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/MessagesComposeFragment;->b:J

    :goto_0
    invoke-virtual {p0, v4, p0}, Lcom/twitter/android/MessagesComposeFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v4, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages:compose:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void

    :cond_0
    iput-wide v1, p0, Lcom/twitter/android/MessagesComposeFragment;->b:J

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesComposeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider;->e:Landroid/net/Uri;

    sget-object v3, Lcom/twitter/android/provider/m;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    const/4 v3, 0x0

    if-eqz p2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/MessagesComposeFragment;->e:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/4 v1, 0x0

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x190

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    iget-object v1, p0, Lcom/twitter/android/MessagesComposeFragment;->f:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iput-boolean v3, p0, Lcom/twitter/android/MessagesComposeFragment;->e:Z

    :cond_0
    return-void
.end method
