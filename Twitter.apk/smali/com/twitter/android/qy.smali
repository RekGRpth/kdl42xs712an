.class Lcom/twitter/android/qy;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ProfileFragment;

.field private b:Landroid/os/Handler;

.field private c:Lcom/twitter/android/qp;


# direct methods
.method private constructor <init>(Lcom/twitter/android/ProfileFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ProfileFragment;Lcom/twitter/android/qe;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/qy;-><init>(Lcom/twitter/android/ProfileFragment;)V

    return-void
.end method

.method private a()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qy;->b:Landroid/os/Handler;

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/qy;->b:Landroid/os/Handler;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qy;->c:Lcom/twitter/android/qp;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/qp;

    iget-object v1, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {v0, v1}, Lcom/twitter/android/qp;-><init>(Lcom/twitter/android/ProfileFragment;)V

    iput-object v0, p0, Lcom/twitter/android/qy;->c:Lcom/twitter/android/qp;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/qy;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/qy;->c:Lcom/twitter/android/qp;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    const-wide/16 v2, 0x1388

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method private a(Lcom/twitter/library/api/upload/n;Lcom/twitter/library/service/e;)V
    .locals 7

    iget-object v0, p1, Lcom/twitter/library/api/upload/n;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "header_uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p1, Lcom/twitter/library/api/upload/n;->k:Landroid/os/Bundle;

    const-string/jumbo v2, "avatar_uri"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    iget-object v2, p1, Lcom/twitter/library/api/upload/n;->k:Landroid/os/Bundle;

    const-string/jumbo v3, "user"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    iget-object v3, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    iget-boolean v3, v3, Lcom/twitter/android/ProfileFragment;->D:Z

    if-eqz v3, :cond_2

    if-eqz v2, :cond_2

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    iget-wide v5, v5, Lcom/twitter/android/ProfileFragment;->I:J

    cmp-long v3, v3, v5

    if-nez v3, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/n;->e()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/n;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    invoke-virtual {v3, v2}, Lcom/twitter/android/ProfileFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v2, 0xc9

    if-eq v0, v2, :cond_2

    new-instance v0, Lcom/twitter/android/qu;

    iget-object v2, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    invoke-direct {v0, v2}, Lcom/twitter/android/qu;-><init>(Lcom/twitter/android/ProfileFragment;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/twitter/android/qu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    if-eqz v1, :cond_3

    invoke-virtual {p2}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/qy;->a()V

    :cond_3
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/qy;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/qy;->a:Lcom/twitter/android/ProfileFragment;

    invoke-static {v0}, Lcom/twitter/android/ProfileFragment;->N(Lcom/twitter/android/ProfileFragment;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/service/p;->a(Lcom/twitter/library/client/Session;)Z

    move-result v1

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    if-eqz v1, :cond_0

    instance-of v1, p1, Lcom/twitter/library/api/upload/n;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/twitter/library/api/upload/n;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/qy;->a(Lcom/twitter/library/api/upload/n;Lcom/twitter/library/service/e;)V

    :cond_0
    return-void
.end method
