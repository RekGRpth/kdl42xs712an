.class final Lcom/twitter/android/tm;
.super Lcom/twitter/android/yb;
.source "Twttr"


# static fields
.field public static a:Lcom/twitter/android/widget/SignedOutDialogFragment;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 9

    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    move-object v8, v3

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/yb;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(JJLjava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/widget/TweetView;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/tm;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SignedOutActivity;

    if-eqz v0, :cond_0

    sget-object v1, Lcom/twitter/android/SignedOutFragment;->b:Ljava/lang/String;

    invoke-static {v1, v0, p7, p5}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a(Ljava/lang/String;Lcom/twitter/android/SignedOutActivity;Lcom/twitter/library/widget/TweetView;Ljava/lang/String;)Lcom/twitter/android/widget/SignedOutDialogFragment;

    move-result-object v1

    sput-object v1, Lcom/twitter/android/tm;->a:Lcom/twitter/android/widget/SignedOutDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SignedOutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/widget/TweetView;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/tm;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/SignedOutActivity;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/view/TweetActionType;->name()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->h()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a(Ljava/lang/String;Lcom/twitter/android/SignedOutActivity;Ljava/lang/String;)Lcom/twitter/android/widget/SignedOutDialogFragment;

    move-result-object v1

    sput-object v1, Lcom/twitter/android/tm;->a:Lcom/twitter/android/widget/SignedOutDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SignedOutActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/SignedOutDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_0
    return-void
.end method
