.class Lcom/twitter/android/zv;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/UserSelectFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/UserSelectFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/zv;->a:Lcom/twitter/android/UserSelectFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/UserSelectFragment;Lcom/twitter/android/zn;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/zv;-><init>(Lcom/twitter/android/UserSelectFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IZLjava/lang/String;Lcom/twitter/library/api/search/TwitterTypeAheadGroup;)V
    .locals 2

    const/4 v1, 0x1

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-ne p5, v1, :cond_0

    if-nez p6, :cond_1

    iget-object v0, p8, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a:Ljava/util/ArrayList;

    invoke-static {p7, v0}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {p8}, Lcom/twitter/library/api/search/TwitterTypeAheadGroup;->a()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/zv;->a:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->a(Z)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/zv;->a:Lcom/twitter/android/UserSelectFragment;

    invoke-static {v0}, Lcom/twitter/android/UserSelectFragment;->b(Lcom/twitter/android/UserSelectFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/zv;->a:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0, v1}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->a(Z)V

    goto :goto_0
.end method
