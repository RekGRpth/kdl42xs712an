.class public Lcom/twitter/android/PhotoSelectHelper;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Ljava/util/List;


# instance fields
.field private final b:Ljava/util/List;

.field private c:Lcom/twitter/android/ou;

.field private d:Landroid/app/Activity;

.field private e:Lcom/twitter/android/AttachMediaListener;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/library/client/Session;

.field private h:Ljava/io/File;

.field private i:Z

.field private j:Landroid/net/Uri;

.field private k:Ljava/util/EnumSet;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/twitter/android/PhotoSelectHelper;->a:Ljava/util/List;

    sget-object v0, Lcom/twitter/android/PhotoSelectHelper;->a:Ljava/util/List;

    const-string/jumbo v1, "gmail-ls"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/PhotoSelectHelper;->a:Ljava/util/List;

    const-string/jumbo v1, "http"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/PhotoSelectHelper;->a:Ljava/util/List;

    const-string/jumbo v1, "https"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/PhotoSelectHelper;->a:Ljava/util/List;

    const-string/jumbo v1, "com.android.providers.media.documents"

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->b:Ljava/util/List;

    iput-object p1, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    iput-object p2, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    iput-object p3, p0, Lcom/twitter/android/PhotoSelectHelper;->f:Ljava/lang/String;

    iput-object p4, p0, Lcom/twitter/android/PhotoSelectHelper;->k:Ljava/util/EnumSet;

    invoke-direct {p0}, Lcom/twitter/android/PhotoSelectHelper;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PhotoSelectHelper;->i:Z

    return-void
.end method

.method public static a(Landroid/content/Intent;)Landroid/net/Uri;
    .locals 3

    const/4 v1, 0x0

    if-eqz p0, :cond_2

    const-string/jumbo v0, "uri"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/webkit/URLUtil;->isValidUrl(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v0}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    move-object v0, v1

    :cond_1
    :goto_0
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/PhotoSelectHelper;Landroid/net/Uri;)Landroid/net/Uri;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/PhotoSelectHelper;->j:Landroid/net/Uri;

    return-object p1
.end method

.method private a(Landroid/content/Intent;J)V
    .locals 5

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/PhotoSelectHelper;->f:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":image_attachment::done"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    if-eqz p1, :cond_4

    const-string/jumbo v1, "filter_effect"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "twitter:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "filter_effect"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    const-string/jumbo v1, "filter_enhanced"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "filter_enhanced"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    const-string/jumbo v1, "filter_filesize"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "filter_filesize"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getFloatExtra(Ljava/lang/String;F)F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c(F)Lcom/twitter/library/scribe/ScribeLog;

    :cond_2
    const-string/jumbo v1, "filter_orientation"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string/jumbo v1, "filter_orientation"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->j(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    const-string/jumbo v1, "filter_numseen"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string/jumbo v1, "filter_numseen"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PhotoSelectHelper;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/PhotoSelectHelper;->g()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/PhotoSelectHelper;Landroid/content/Intent;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/content/Intent;J)V

    return-void
.end method

.method static synthetic a(Landroid/net/Uri;)Z
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/PhotoSelectHelper;->c(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/PhotoSelectHelper;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/PhotoSelectHelper;->i:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    return-object v0
.end method

.method private b(Ljava/util/Collection;Z)V
    .locals 8

    new-instance v0, Lcom/twitter/android/ot;

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/ot;-><init>(Lcom/twitter/android/PhotoSelectHelper;Lcom/twitter/android/AttachMediaListener;Landroid/content/Context;Ljava/util/Collection;ZJ)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoSelectHelper;->c(Lcom/twitter/android/ou;)V

    return-void
.end method

.method static synthetic b(Landroid/net/Uri;)Z
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/PhotoSelectHelper;->d(Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/PhotoSelectHelper;)Ljava/util/EnumSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->k:Ljava/util/EnumSet;

    return-object v0
.end method

.method private static c(Landroid/net/Uri;)Z
    .locals 2

    invoke-virtual {p0}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    sget-object v1, Lcom/twitter/android/PhotoSelectHelper;->a:Ljava/util/List;

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/PhotoSelectHelper;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->f:Ljava/lang/String;

    return-object v0
.end method

.method private static d(Landroid/net/Uri;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v2, "content"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "android.resource"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    const-string/jumbo v2, "file"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private f()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "photo_service"

    const-string/jumbo v2, "2"

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "2"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "photo_service"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method


# virtual methods
.method public a(I)Landroid/app/Dialog;
    .locals 7

    const v6, 0x7f0f0308    # com.twitter.android.R.string.photo_service_update

    const v5, 0x7f0f02d5    # com.twitter.android.R.string.ok

    const v4, 0x7f0f0032    # com.twitter.android.R.string.app_name

    const/4 v3, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/twitter/android/oq;

    invoke-direct {v0, p0}, Lcom/twitter/android/oq;-><init>(Lcom/twitter/android/PhotoSelectHelper;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/twitter/android/or;

    invoke-direct {v0, p0}, Lcom/twitter/android/or;-><init>(Lcom/twitter/android/PhotoSelectHelper;)V

    new-instance v1, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v5, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;
    .locals 2

    new-instance v0, Lcom/twitter/android/PostStorage$MediaItem;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/android/PostStorage$MediaItem;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)V

    const/4 v1, 0x3

    iput v1, v0, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    return-object v0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    invoke-virtual {p0}, Lcom/twitter/android/PhotoSelectHelper;->b()V

    return-void
.end method

.method public a(IILandroid/content/Intent;J)V
    .locals 6

    const/4 v5, 0x1

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eq p2, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p3}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    invoke-interface {v1, v0}, Lcom/twitter/android/AttachMediaListener;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {p0, v0, p4, p5, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    goto :goto_0

    :pswitch_1
    if-eq p2, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->h:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    invoke-interface {v1, v0}, Lcom/twitter/android/AttachMediaListener;->a(Landroid/net/Uri;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    invoke-virtual {p0, v0, p4, p5, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V

    goto :goto_0

    :pswitch_2
    invoke-static {p3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->j:Landroid/net/Uri;

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v1

    if-eq p2, v3, :cond_3

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    iput v5, v1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v2, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v2, v1}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoSelectHelper;->e()V

    goto :goto_0

    :cond_3
    if-nez v0, :cond_4

    iput v5, v1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v2, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v2, v1}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoSelectHelper;->e()V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "filter_id"

    invoke-virtual {p3, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    const-string/jumbo v2, "enhanced"

    invoke-virtual {p3, v2, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput v0, v1, Lcom/twitter/android/PostStorage$MediaItem;->h:I

    iput-boolean v2, v1, Lcom/twitter/android/PostStorage$MediaItem;->i:Z

    invoke-direct {p0, p3, p4, p5}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/content/Intent;J)V

    invoke-static {v1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/PhotoSelectHelper;->a(Ljava/util/Collection;Z)V

    goto/16 :goto_0

    :pswitch_3
    if-eq p2, v3, :cond_5

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    goto/16 :goto_0

    :cond_5
    if-eqz p3, :cond_6

    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_7

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "tempImageFile"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    if-nez v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    sget-object v1, Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V

    goto/16 :goto_0

    :cond_8
    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    invoke-interface {v2, v1}, Lcom/twitter/android/AttachMediaListener;->a(Landroid/net/Uri;)Z

    move-result v2

    if-eqz v2, :cond_0

    new-instance v2, Lcom/twitter/android/ov;

    iget-object v3, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-virtual {v3}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3, v0}, Lcom/twitter/android/ov;-><init>(Landroid/content/Context;Ljava/io/File;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v0}, Lcom/twitter/android/ov;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    invoke-virtual {p0, v1, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    invoke-virtual {p0, v0, v4}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x101
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;JLcom/twitter/android/PhotoSelectHelper$AddMediaMode;)V
    .locals 7

    if-eqz p1, :cond_0

    new-instance v0, Lcom/twitter/android/os;

    iget-object v6, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    move-object v1, p0

    move-wide v2, p2

    move-object v4, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/os;-><init>(Lcom/twitter/android/PhotoSelectHelper;JLandroid/net/Uri;Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;Lcom/twitter/android/AttachMediaListener;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoSelectHelper;->c(Lcom/twitter/android/ou;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/net/Uri;Z)V
    .locals 1

    invoke-virtual {p0, p1, p1}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 1

    const-string/jumbo v0, "pic"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/io/File;

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->h:Ljava/io/File;

    const-string/jumbo v0, "photo_service"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/PhotoSelectHelper;->i:Z

    const-string/jumbo v0, "scribe_section"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->f:Ljava/lang/String;

    const-string/jumbo v0, "pending_media_uri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->j:Landroid/net/Uri;

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;Z)V
    .locals 2

    invoke-static {p1}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    if-nez p2, :cond_0

    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->c(Landroid/net/Uri;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Ljava/util/Collection;Z)V

    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/ou;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/PhotoSelectHelper;->c(Lcom/twitter/android/ou;)V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/PhotoSelectHelper;->g:Lcom/twitter/library/client/Session;

    return-void
.end method

.method a(Ljava/util/Collection;Z)V
    .locals 8

    new-instance v0, Lcom/twitter/android/ot;

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->e:Lcom/twitter/android/AttachMediaListener;

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    move-object v1, p0

    move-object v4, p1

    move v5, p2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/ot;-><init>(Lcom/twitter/android/PhotoSelectHelper;Lcom/twitter/android/AttachMediaListener;Landroid/content/Context;Ljava/util/Collection;ZJ)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/ou;)Z

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Ljava/util/Collection;Z)V

    return-void
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    invoke-interface {v0}, Lcom/twitter/android/ou;->b()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    return-void
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "pic"

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->h:Ljava/io/File;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "photo_service"

    iget-boolean v1, p0, Lcom/twitter/android/PhotoSelectHelper;->i:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "scribe_section"

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->f:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "pending_media_uri"

    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->j:Landroid/net/Uri;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-void
.end method

.method b(Lcom/twitter/android/ou;)Z
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    invoke-interface {p1}, Lcom/twitter/android/ou;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 6

    const v5, 0x7f0f0088    # com.twitter.android.R.string.camera_photo_error

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-static {v0}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-static {v0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/PhotoSelectHelper;->i:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->g:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->h:Ljava/io/File;

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->h:Ljava/io/File;

    if-eqz v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "android.media.action.IMAGE_CAPTURE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "output"

    iget-object v2, p0, Lcom/twitter/android/PhotoSelectHelper;->h:Ljava/io/File;

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const/16 v2, 0x102

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const v1, 0x7f0f0514    # com.twitter.android.R.string.unsupported_feature

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    invoke-static {v0, v5, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method c(Lcom/twitter/android/ou;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/ou;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->b:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public d()V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/PhotoSelectHelper;->i:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const/16 v1, 0x102

    invoke-virtual {v0, v1}, Landroid/app/Activity;->showDialog(I)V

    :goto_0
    return-void

    :cond_0
    :try_start_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aj()Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "image/*"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.action.GET_CONTENT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.category.OPENABLE"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const/16 v2, 0x101

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->d:Landroid/app/Activity;

    const v1, 0x7f0f0514    # com.twitter.android.R.string.unsupported_feature

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    goto :goto_0

    :cond_1
    :try_start_1
    const-string/jumbo v1, "android.intent.action.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Landroid/provider/MediaStore$Images$Media;->EXTERNAL_CONTENT_URI:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "vnd.android.cursor.dir/image"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;
    :try_end_1
    .catch Landroid/content/ActivityNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method e()V
    .locals 2

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->c:Lcom/twitter/android/ou;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PhotoSelectHelper;->b:Ljava/util/List;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ou;

    invoke-virtual {p0, v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/ou;)Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    return-void
.end method
