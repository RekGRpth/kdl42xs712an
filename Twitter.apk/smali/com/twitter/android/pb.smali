.class Lcom/twitter/android/pb;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/PostActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/PostActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/pb;->a:Lcom/twitter/android/PostActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/pb;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v0}, Lcom/twitter/android/PostActivity;->w(Lcom/twitter/android/PostActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, -0x1

    if-ne v1, p2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/pb;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->x(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "composition:lifeline_alerts:::tweet"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/pb;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0}, Lcom/twitter/android/PostActivity;->p()V

    iget-object v0, p0, Lcom/twitter/android/pb;->a:Lcom/twitter/android/PostActivity;

    invoke-virtual {v0}, Lcom/twitter/android/PostActivity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v1, -0x2

    if-ne v1, p2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/pb;->a:Lcom/twitter/android/PostActivity;

    invoke-static {v1}, Lcom/twitter/android/PostActivity;->y(Lcom/twitter/android/PostActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "composition:lifeline_alerts:::cancel"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method
