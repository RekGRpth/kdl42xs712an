.class final Lcom/twitter/android/gc;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;

.field private final b:Landroid/content/Context;

.field private final c:Z

.field private final d:Landroid/net/Uri;

.field private e:Lcom/twitter/android/gk;

.field private f:Lcom/twitter/android/gk;


# direct methods
.method constructor <init>(Lcom/twitter/android/FilterActivity;Landroid/net/Uri;Z)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/gc;->a:Ljava/lang/ref/WeakReference;

    iput-object p2, p0, Lcom/twitter/android/gc;->d:Landroid/net/Uri;

    invoke-virtual {p1}, Lcom/twitter/android/FilterActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/gc;->b:Landroid/content/Context;

    iput-boolean p3, p0, Lcom/twitter/android/gc;->c:Z

    return-void
.end method

.method private a()Landroid/graphics/Bitmap$Config;
    .locals 5

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/gc;->b:Landroid/content/Context;

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/gc;->d:Landroid/net/Uri;

    invoke-static {v2, v1}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v1

    const/16 v3, 0x40

    invoke-virtual {v1, v3}, Lkw;->b(I)Lkw;

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v1, v3}, Lkw;->a(Landroid/graphics/Bitmap$Config;)Lkw;

    invoke-virtual {v1}, Lkw;->b()Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    :try_start_1
    invoke-virtual {p0}, Lcom/twitter/android/gc;->isCancelled()Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/gc;->d:Landroid/net/Uri;

    invoke-static {v2, v3}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;Landroid/net/Uri;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    :goto_0
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    return-object v0

    :cond_2
    :try_start_2
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    throw v0

    :catchall_1
    move-exception v0

    goto :goto_1
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/twitter/android/gc;->b:Landroid/content/Context;

    :try_start_0
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    invoke-direct {p0}, Lcom/twitter/android/gc;->a()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    if-nez v3, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-boolean v4, p0, Lcom/twitter/android/gc;->c:Z

    if-nez v4, :cond_3

    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    int-to-float v4, v4

    div-float/2addr v4, v2

    float-to-int v4, v4

    iget v5, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    int-to-float v5, v5

    div-float/2addr v5, v2

    float-to-int v5, v5

    iget-object v6, p0, Lcom/twitter/android/gc;->d:Landroid/net/Uri;

    invoke-static {v0, v6}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v6

    invoke-virtual {v6, v4, v5}, Lkw;->b(II)Lkw;

    sget-object v4, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v6, v4}, Lkw;->a(Landroid/graphics/Bitmap$Config;)Lkw;

    invoke-virtual {v6}, Lkw;->c()Landroid/graphics/Rect;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/gc;->isCancelled()Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_2
    new-instance v5, Lcom/twitter/android/gk;

    invoke-direct {v5}, Lcom/twitter/android/gk;-><init>()V

    iput-object v5, p0, Lcom/twitter/android/gc;->f:Lcom/twitter/android/gk;

    iget-object v5, p0, Lcom/twitter/android/gc;->f:Lcom/twitter/android/gk;

    invoke-virtual {v4}, Landroid/graphics/Rect;->width()I

    move-result v6

    iput v6, v5, Lcom/twitter/android/gk;->a:I

    iget-object v5, p0, Lcom/twitter/android/gc;->f:Lcom/twitter/android/gk;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    iput v4, v5, Lcom/twitter/android/gk;->b:I

    iget-object v4, p0, Lcom/twitter/android/gc;->f:Lcom/twitter/android/gk;

    iput-object v3, v4, Lcom/twitter/android/gk;->c:Landroid/graphics/Bitmap$Config;

    :cond_3
    iget v4, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v4, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    div-int/lit8 v1, v1, 0x3

    int-to-float v1, v1

    div-float/2addr v1, v2

    float-to-int v1, v1

    iget-object v2, p0, Lcom/twitter/android/gc;->d:Landroid/net/Uri;

    invoke-static {v0, v2}, Lkw;->a(Landroid/content/Context;Landroid/net/Uri;)Lkw;

    move-result-object v0

    invoke-virtual {v0, v1}, Lkw;->c(I)Lkw;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-virtual {v0, v1}, Lkw;->a(Landroid/graphics/Bitmap$Config;)Lkw;

    invoke-virtual {v0}, Lkw;->c()Landroid/graphics/Rect;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/gc;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_5

    :cond_4
    const/4 v0, 0x2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    :cond_5
    invoke-virtual {v0}, Landroid/graphics/Rect;->width()I

    move-result v1

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    if-le v1, v0, :cond_6

    sub-int/2addr v1, v0

    div-int/lit8 v1, v1, 0x2

    const/4 v4, 0x0

    add-int v5, v1, v0

    invoke-virtual {v2, v1, v4, v5, v0}, Landroid/graphics/Rect;->set(IIII)V

    :goto_1
    new-instance v0, Lcom/twitter/android/gk;

    invoke-direct {v0}, Lcom/twitter/android/gk;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/gc;->e:Lcom/twitter/android/gk;

    iget-object v0, p0, Lcom/twitter/android/gc;->e:Lcom/twitter/android/gk;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v1

    iput v1, v0, Lcom/twitter/android/gk;->a:I

    iget-object v0, p0, Lcom/twitter/android/gc;->e:Lcom/twitter/android/gk;

    invoke-virtual {v2}, Landroid/graphics/Rect;->height()I

    move-result v1

    iput v1, v0, Lcom/twitter/android/gk;->b:I

    iget-object v0, p0, Lcom/twitter/android/gc;->e:Lcom/twitter/android/gk;

    iput-object v3, v0, Lcom/twitter/android/gk;->c:Landroid/graphics/Bitmap$Config;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    :cond_6
    sub-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    const/4 v4, 0x0

    add-int v5, v0, v1

    invoke-virtual {v2, v4, v0, v1, v5}, Landroid/graphics/Rect;->set(IIII)V
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto/16 :goto_0
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/gc;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/android/FilterActivity;

    if-eqz v5, :cond_1

    iget-object v0, v5, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/gc;->d:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/gc;->e:Lcom/twitter/android/gk;

    iget-object v3, p0, Lcom/twitter/android/gc;->f:Lcom/twitter/android/gk;

    invoke-static {v5}, Lcom/twitter/android/FilterActivity;->j(Lcom/twitter/android/FilterActivity;)Z

    move-result v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/FilterManager;->a(Landroid/net/Uri;Lcom/twitter/android/gk;Lcom/twitter/android/gk;ZLcom/twitter/android/gm;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object p1

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v5, v6, v6, v0}, Lcom/twitter/android/FilterActivity;->a(Lcom/twitter/android/gk;[Landroid/graphics/Bitmap;I)V

    :cond_1
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gc;->a([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/twitter/android/gc;->a(Ljava/lang/Integer;)V

    return-void
.end method
