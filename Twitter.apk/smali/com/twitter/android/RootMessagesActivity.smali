.class public Lcom/twitter/android/RootMessagesActivity;
.super Lcom/twitter/android/MessagesActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/MessagesActivity;-><init>()V

    return-void
.end method

.method public static b(Landroid/content/Context;)Ljava/lang/Class;
    .locals 1

    invoke-static {p0}, Lcom/twitter/library/api/conversations/ae;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/DMInboxActivity;

    :goto_0
    return-object v0

    :cond_0
    const-class v0, Lcom/twitter/android/RootMessagesActivity;

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/MessagesActivity;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(Z)V

    return-object v0
.end method
