.class Lcom/twitter/android/kf;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/ArrayList;

.field private c:Lcom/twitter/android/kg;

.field private d:Landroid/util/SparseArray;

.field private e:Landroid/app/Activity;

.field private f:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;Ljava/util/ArrayList;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/kf;->f:I

    iput-object p1, p0, Lcom/twitter/android/kf;->e:Landroid/app/Activity;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    iput-object p2, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    return-void
.end method

.method private a(IILhb;)V
    .locals 5

    const/4 v1, 0x0

    if-le p2, p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v2

    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3, v2}, Landroid/util/SparseArray;-><init>(I)V

    if-eqz v0, :cond_3

    :goto_1
    if-ge v1, v2, :cond_6

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    if-le v0, p1, :cond_2

    if-gt v0, p2, :cond_2

    add-int/lit8 v0, v0, -0x1

    iget-object v4, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_0
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    if-eq v0, p1, :cond_0

    iget-object v4, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_2

    :cond_3
    :goto_3
    if-ge v1, v2, :cond_6

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    if-ge v0, p1, :cond_5

    if-lt v0, p2, :cond_5

    add-int/lit8 v0, v0, 0x1

    iget-object v4, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_4
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_5
    if-eq v0, p1, :cond_4

    iget-object v4, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v4, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_4

    :cond_6
    if-eqz p3, :cond_7

    invoke-virtual {v3, p2, p3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_7
    iput-object v3, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    return-void
.end method


# virtual methods
.method public a(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/android/kg;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public a(Lcom/twitter/android/kn;)I
    .locals 3

    invoke-virtual {p0, p1}, Lcom/twitter/android/kf;->a(Lcom/twitter/android/kg;)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/kf;->f:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    iget-object v2, p1, Lcom/twitter/android/kn;->a:Lhb;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    iget-object v2, p1, Lcom/twitter/android/kn;->a:Lhb;

    invoke-virtual {v1, v0, v2}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    return v0
.end method

.method public a(Lhb;)I
    .locals 4

    new-instance v0, Lcom/twitter/android/kn;

    iget-object v1, p0, Lcom/twitter/android/kf;->e:Landroid/app/Activity;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/twitter/android/kn;-><init>(Landroid/content/Context;Lhb;ILandroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/kf;->a(Lcom/twitter/android/kn;)I

    move-result v0

    return v0
.end method

.method public a(Lhb;Landroid/content/Intent;)I
    .locals 3

    new-instance v0, Lcom/twitter/android/kn;

    iget-object v1, p0, Lcom/twitter/android/kf;->e:Landroid/app/Activity;

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, v2, p2}, Lcom/twitter/android/kn;-><init>(Landroid/content/Context;Lhb;ILandroid/content/Intent;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/kf;->a(Lcom/twitter/android/kn;)I

    move-result v0

    return v0
.end method

.method public a(Landroid/net/Uri;)Lhb;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, v0, Lhb;->c:Landroid/net/Uri;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v2, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/kf;->c:Lcom/twitter/android/kg;

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/kf;->f:I

    return-void
.end method

.method public a(II)V
    .locals 6

    const/4 v5, -0x1

    if-ne p2, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kg;

    iget-object v1, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, p2, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v2, p1, 0x1

    add-int/lit8 v3, p2, 0x1

    iget v1, p0, Lcom/twitter/android/kf;->f:I

    if-eq v1, v5, :cond_2

    iget v1, p0, Lcom/twitter/android/kf;->f:I

    if-ge v2, v1, :cond_2

    iget v1, p0, Lcom/twitter/android/kf;->f:I

    if-lt v3, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/twitter/android/kf;->f:I

    const/4 v0, 0x0

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/kf;->a(IILhb;)V

    goto :goto_0

    :cond_2
    iget v1, p0, Lcom/twitter/android/kf;->f:I

    if-eq v1, v5, :cond_4

    iget v1, p0, Lcom/twitter/android/kf;->f:I

    if-le v2, v1, :cond_4

    iget v1, p0, Lcom/twitter/android/kf;->f:I

    if-gt v3, v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    iget-object v4, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    check-cast v0, Lcom/twitter/android/kn;

    iget-object v0, v0, Lcom/twitter/android/kn;->a:Lhb;

    if-eq v1, v5, :cond_3

    iget-object v4, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :goto_1
    iget v1, p0, Lcom/twitter/android/kf;->f:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/android/kf;->f:I

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/kf;->a(IILhb;)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    iget v0, p0, Lcom/twitter/android/kf;->f:I

    if-eq v0, v5, :cond_5

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    if-le v2, v0, :cond_5

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    if-gt v3, v0, :cond_0

    :cond_5
    iget v0, p0, Lcom/twitter/android/kf;->f:I

    if-eq v0, v5, :cond_6

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    if-ge v2, v0, :cond_0

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    if-ge v3, v0, :cond_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lhb;

    iget-object v4, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v1

    iget-object v5, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v1, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/kf;->a(IILhb;)V

    goto/16 :goto_0
.end method

.method public a(ILhb;Landroid/support/v4/app/FragmentManager;)V
    .locals 5

    const/4 v2, 0x0

    move v1, v2

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget v3, v0, Lhb;->f:I

    if-ne v3, p1, :cond_2

    invoke-virtual {v0, p3}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-virtual {p3}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/support/v4/app/FragmentTransaction;->remove(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v4/app/FragmentTransaction;->setTransition(I)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commitAllowingStateLoss()I

    invoke-virtual {p3}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v1, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v0, p2}, Landroid/util/SparseArray;->setValueAt(ILjava/lang/Object;)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method public areAllItemsEnabled()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()Lcom/twitter/android/kg;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->c:Lcom/twitter/android/kg;

    return-object v0
.end method

.method public b(Lcom/twitter/android/kg;)V
    .locals 2

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot add two dividers!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/kf;->a(Lcom/twitter/android/kg;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/kf;->f:I

    return-void
.end method

.method public b(Lhb;)V
    .locals 6

    invoke-virtual {p0, p1}, Lcom/twitter/android/kf;->d(Lhb;)I

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    add-int/lit8 v2, v1, -0x1

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    iget-object v0, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    new-instance v2, Landroid/util/SparseArray;

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {v2, v0}, Landroid/util/SparseArray;-><init>(I)V

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_2

    iget-object v4, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    if-ge v4, v1, :cond_1

    iget-object v5, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    if-eq v4, v1, :cond_0

    add-int/lit8 v4, v4, -0x1

    iget-object v5, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    iput-object v2, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    return-void
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/kf;->f:I

    return v0
.end method

.method public c(Lhb;)I
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/kf;->c:Lcom/twitter/android/kg;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Home page can only be set once"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/twitter/android/kn;

    iget-object v1, p0, Lcom/twitter/android/kf;->e:Landroid/app/Activity;

    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/twitter/android/kn;-><init>(Landroid/content/Context;Lhb;ILandroid/content/Intent;)V

    iget-object v1, p0, Lcom/twitter/android/kf;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v4, p1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/kf;->c:Lcom/twitter/android/kg;

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, v4, p1}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    return v4
.end method

.method public d(Lhb;)I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfValue(Ljava/lang/Object;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/kf;->d:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    return v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kg;

    iget v0, v0, Lcom/twitter/android/kg;->b:I

    return v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kg;

    invoke-virtual {v0, v1, p2, p3}, Lcom/twitter/android/kg;->a(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public isEnabled(I)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kf;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/kg;

    invoke-virtual {v0}, Lcom/twitter/android/kg;->a()Z

    move-result v0

    return v0
.end method
