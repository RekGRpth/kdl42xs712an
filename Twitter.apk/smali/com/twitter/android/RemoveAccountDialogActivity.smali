.class public Lcom/twitter/android/RemoveAccountDialogActivity;
.super Lcom/twitter/android/client/BaseActivity;
.source "Twttr"


# instance fields
.field a:Z

.field b:Z

.field c:Lcom/twitter/android/client/c;

.field d:Lcom/twitter/library/client/aa;

.field private e:Ljava/lang/String;

.field private f:Lcom/twitter/library/client/j;

.field private g:Lcom/twitter/android/rq;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/RemoveAccountDialogActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    iput-boolean v7, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->b:Z

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Lcom/twitter/android/client/c;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v7, [Ljava/lang/String;

    const-string/jumbo v6, "settings::::logout"

    aput-object v6, v5, v8

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-static {p0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v3, v3

    if-ne v3, v7, :cond_0

    iget-object v3, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Lcom/twitter/android/client/c;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "settings::::logout_last"

    aput-object v2, v1, v8

    invoke-virtual {v4, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/twitter/library/api/account/k;->b(Landroid/content/Context;Ljava/lang/String;)Z

    :cond_1
    invoke-virtual {p0, v7}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-static {p0, v0}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->e:Ljava/lang/String;

    new-instance v0, Lcom/twitter/android/rq;

    invoke-direct {v0, p0}, Lcom/twitter/android/rq;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->g:Lcom/twitter/android/rq;

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->g:Lcom/twitter/android/rq;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    new-instance v0, Lcom/twitter/android/rp;

    invoke-direct {v0, p0}, Lcom/twitter/android/rp;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    iput-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->f:Lcom/twitter/library/client/j;

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->f:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    goto :goto_0
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7

    const/4 v6, 0x0

    const v5, 0x1040009    # android.R.string.no

    const/4 v2, 0x1

    const/4 v4, 0x0

    new-instance v1, Lcom/twitter/android/rl;

    invoke-direct {v1, p0}, Lcom/twitter/android/rl;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Lcom/twitter/android/rm;

    invoke-direct {v0, p0}, Lcom/twitter/android/rm;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f01d2    # com.twitter.android.R.string.home_logout

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x1040013    # android.R.string.yes

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    goto :goto_0

    :pswitch_1
    new-instance v0, Lcom/twitter/android/rn;

    invoke-direct {v0, p0}, Lcom/twitter/android/rn;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f0125    # com.twitter.android.R.string.dont_be_locked_out

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f01d3    # com.twitter.android.R.string.home_logout_despite_logout_verification_lockout

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f0f00cf    # com.twitter.android.R.string.cont

    invoke-virtual {v2, v3, v0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setEnabled(Z)V

    new-instance v2, Lcom/twitter/android/ro;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/ro;-><init>(Lcom/twitter/android/RemoveAccountDialogActivity;Landroid/widget/Button;)V

    const-wide/16 v3, 0x1388

    invoke-virtual {v1, v2, v3, v4}, Landroid/widget/Button;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :pswitch_2
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f01d1    # com.twitter.android.R.string.home_logging_out

    invoke-virtual {p0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_0

    :pswitch_3
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f01d8    # com.twitter.android.R.string.home_unenrolling_login_verification

    invoke-virtual {p0, v1}, Lcom/twitter/android/RemoveAccountDialogActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v4}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->d:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->g:Lcom/twitter/android/rq;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->c:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->f:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;Landroid/os/Bundle;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseActivity;->onPrepareDialog(ILandroid/app/Dialog;)V

    :goto_0
    return-void

    :pswitch_0
    check-cast p2, Landroid/app/AlertDialog;

    iget-object v0, p0, Lcom/twitter/android/RemoveAccountDialogActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0f01d5    # com.twitter.android.R.string.home_logout_question_login_verification

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_0
    const v0, 0x7f0f01d4    # com.twitter.android.R.string.home_logout_question

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/app/AlertDialog;->setMessage(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseActivity;->onResume()V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/RemoveAccountDialogActivity;->showDialog(I)V

    return-void
.end method
