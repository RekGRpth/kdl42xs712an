.class public abstract Lcom/twitter/android/BaseSignUpActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;
.implements Landroid/widget/TextView$OnEditorActionListener;


# instance fields
.field public a:Landroid/widget/EditText;

.field public b:Landroid/widget/EditText;

.field public c:Lcom/twitter/internal/android/widget/PopupEditText;

.field public d:Landroid/widget/EditText;

.field public e:Landroid/widget/CheckBox;

.field protected f:Lcom/twitter/android/util/d;

.field g:Landroid/widget/TextView;

.field h:Landroid/widget/TextView;

.field i:Landroid/widget/TextView;

.field j:Landroid/widget/TextView;

.field k:I

.field l:I

.field m:I

.field n:I

.field o:Lcom/twitter/android/zy;

.field p:Lcom/twitter/android/bj;

.field q:Z

.field private r:I

.field private s:I

.field private t:Landroid/graphics/drawable/Drawable;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/widget/EditText;Landroid/widget/TextView;I)I
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Z)V

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v1}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/widget/EditText;->length()I

    move-result v1

    if-lt v1, p3, :cond_1

    const/4 v0, 0x2

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v2, 0x1

    if-le p3, v2, :cond_0

    if-ge v1, p3, :cond_0

    const/4 v0, 0x3

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/BaseSignUpActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z
    .locals 1

    if-nez p1, :cond_0

    invoke-virtual {p2}, Landroid/widget/EditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p3}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/BaseSignUpActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/BaseSignUpActivity;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->d(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method abstract a()V
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    new-instance v0, Lcom/twitter/android/bj;

    invoke-direct {v0, p0}, Lcom/twitter/android/bj;-><init>(Lcom/twitter/android/BaseSignUpActivity;)V

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->p:Lcom/twitter/android/bj;

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0201ab    # com.twitter.android.R.drawable.ic_form_check

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->t:Landroid/graphics/drawable/Drawable;

    const v1, 0x106000c    # android.R.color.black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/BaseSignUpActivity;->s:I

    const v1, 0x7f0b0056    # com.twitter.android.R.color.form_error

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/BaseSignUpActivity;->r:I

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    const v0, 0x7f0900c7    # com.twitter.android.R.id.email

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    const v0, 0x7f090098    # com.twitter.android.R.id.username

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    const v0, 0x7f0900c8    # com.twitter.android.R.id.password

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    const v0, 0x7f090140    # com.twitter.android.R.id.phone_number_addr_book_checkbox

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->e:Landroid/widget/CheckBox;

    new-instance v0, Lcom/twitter/android/zy;

    invoke-direct {v0}, Lcom/twitter/android/zy;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->o:Lcom/twitter/android/zy;

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->o:Lcom/twitter/android/zy;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f09013c    # com.twitter.android.R.id.name_err

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->g:Landroid/widget/TextView;

    const v0, 0x7f09013e    # com.twitter.android.R.id.email_err

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    const v0, 0x7f09013d    # com.twitter.android.R.id.username_err

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    const v0, 0x7f09013f    # com.twitter.android.R.id.password_err

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->j:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    sget-object v1, Landroid/graphics/Typeface;->DEFAULT:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-static {p0}, Lcom/twitter/android/util/h;->a(Landroid/content/Context;)Lcom/twitter/android/util/d;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->f:Lcom/twitter/android/util/d;

    new-instance v0, Lcom/twitter/android/bk;

    invoke-direct {v0, p0}, Lcom/twitter/android/bk;-><init>(Lcom/twitter/android/BaseSignUpActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->a(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1

    if-eqz p3, :cond_0

    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->r:I

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    invoke-virtual {p2, p3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->s:I

    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method a(Landroid/widget/EditText;Z)V
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/widget/EditText;->getCompoundDrawables()[Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    if-eqz p2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->t:Landroid/graphics/drawable/Drawable;

    invoke-virtual {p1, v0, v1, v2, v1}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    :goto_1
    return-void

    :cond_0
    const/4 v2, 0x0

    aget-object v0, v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0, v1, v1, v1}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 6

    const/4 v2, 0x3

    const/4 v1, 0x2

    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/BaseSignUpActivity;->g:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/BaseSignUpActivity;->k:I

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v3

    const/16 v4, 0x20

    if-ne v3, v4, :cond_0

    iput-boolean v0, p0, Lcom/twitter/android/BaseSignUpActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->b()V

    :cond_0
    :goto_0
    if-ne v2, v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->p:Lcom/twitter/android/bj;

    invoke-virtual {v1, v0}, Lcom/twitter/android/bj;->a(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->a()V

    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/BaseSignUpActivity;->l:I

    move v2, v0

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v4}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v3, p0, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    invoke-direct {p0, v0, v3, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    move v5, v2

    move v2, v0

    move v0, v5

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    goto :goto_2

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    iget-object v3, p0, Lcom/twitter/android/BaseSignUpActivity;->j:Landroid/widget/TextView;

    invoke-direct {p0, v2, v3, v0}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;I)I

    move-result v2

    iput v2, p0, Lcom/twitter/android/BaseSignUpActivity;->n:I

    const/4 v0, 0x4

    goto :goto_0

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->p:Lcom/twitter/android/bj;

    invoke-virtual {v1, v0}, Lcom/twitter/android/bj;->removeMessages(I)V

    goto :goto_1
.end method

.method b()V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/BaseSignUpActivity;->q:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/client/c;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method protected d()V
    .locals 13

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    move v5, v2

    :goto_0
    if-eqz v5, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    move v4, v2

    :goto_1
    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_3

    move v1, v2

    :goto_2
    if-eqz v5, :cond_0

    if-eqz v1, :cond_4

    const-string/jumbo v0, "select"

    :goto_3
    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v6

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v7

    invoke-virtual {v7}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    new-array v9, v12, [Ljava/lang/String;

    const-string/jumbo v10, "signup:form:phone_number"

    aput-object v10, v9, v3

    const-string/jumbo v10, ""

    aput-object v10, v9, v2

    aput-object v0, v9, v11

    invoke-virtual {v6, v7, v8, v9}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aR()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p0, v0, v4, v1}, Lcom/twitter/android/util/ai;->a(Landroid/app/Activity;Landroid/content/res/Resources;ZZ)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->finish()V

    :goto_4
    return-void

    :cond_1
    move v5, v3

    goto :goto_0

    :cond_2
    move v4, v3

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_2

    :cond_4
    const-string/jumbo v0, "skip"

    goto :goto_3

    :cond_5
    if-eqz v5, :cond_7

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v5, "onboard_interest"

    const/4 v6, 0x0

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v5, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v5}, Lcom/twitter/android/FollowFlowController;-><init>()V

    sget-object v6, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v5, v6}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v5

    invoke-virtual {v5, v2}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/twitter/android/FollowFlowController;->b(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    invoke-virtual {v1, v4}, Lcom/twitter/android/FollowFlowController;->c(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/android/BaseSignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v4}, Lcom/twitter/android/util/d;->b()Z

    move-result v4

    invoke-virtual {v1, v4}, Lcom/twitter/android/FollowFlowController;->d(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/android/FollowFlowController;->a(I)Lcom/twitter/android/FollowFlowController;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/android/util/x;->b(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-virtual {v1, v0}, Lcom/twitter/android/FollowFlowController;->b(Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    new-array v4, v12, [Ljava/lang/String;

    const-string/jumbo v5, "bogus step for sign up form"

    aput-object v5, v4, v3

    const-string/jumbo v3, "follow_interest"

    aput-object v3, v4, v2

    const-string/jumbo v2, "follow_friends"

    aput-object v2, v4, v11

    invoke-virtual {v0, v4}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    :goto_5
    invoke-virtual {v1, p0}, Lcom/twitter/android/FollowFlowController;->b(Landroid/app/Activity;)V

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->finish()V

    goto :goto_4

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0, v2}, Lcom/twitter/android/util/d;->a(Z)[Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    goto :goto_5

    :cond_7
    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "android.intent.extra.INTENT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-eqz v0, :cond_8

    invoke-virtual {p0, v0}, Lcom/twitter/android/BaseSignUpActivity;->startActivity(Landroid/content/Intent;)V

    :cond_8
    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->finish()V

    goto/16 :goto_4
.end method

.method l_()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->hasFocus()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->o:Lcom/twitter/android/zy;

    invoke-virtual {v0}, Lcom/twitter/android/zy;->getCount()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090098    # com.twitter.android.R.id.username

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->l_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f090098    # com.twitter.android.R.id.username

    if-ne v0, v1, :cond_0

    const/4 v0, 0x5

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4

    const/4 v3, 0x3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090097    # com.twitter.android.R.id.name

    if-ne v0, v1, :cond_1

    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/BaseSignUpActivity;->q:Z

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0900c7    # com.twitter.android.R.id.email

    if-ne v0, v1, :cond_3

    if-nez p2, :cond_0

    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->l:I

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->h:Landroid/widget/TextView;

    const v2, 0x7f0f0442    # com.twitter.android.R.string.signup_error_email

    invoke-virtual {p0, v2}, Lcom/twitter/android/BaseSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->l:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->p:Lcom/twitter/android/bj;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->b(I)V

    goto :goto_0

    :cond_3
    const v1, 0x7f090098    # com.twitter.android.R.id.username

    if-ne v0, v1, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/BaseSignUpActivity;->l_()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    goto :goto_0

    :cond_4
    if-nez p2, :cond_0

    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->i:Landroid/widget/TextView;

    const v2, 0x7f0f0444    # com.twitter.android.R.string.signup_error_username

    invoke-virtual {p0, v2}, Lcom/twitter/android/BaseSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->m:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->p:Lcom/twitter/android/bj;

    invoke-virtual {v0, v3}, Lcom/twitter/android/bj;->b(I)V

    goto :goto_0

    :cond_6
    const v1, 0x7f0900c8    # com.twitter.android.R.id.password

    if-ne v0, v1, :cond_0

    if-nez p2, :cond_0

    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->n:I

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    iget-object v2, p0, Lcom/twitter/android/BaseSignUpActivity;->j:Landroid/widget/TextView;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(ILandroid/widget/EditText;Landroid/widget/TextView;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->d:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/BaseSignUpActivity;->j:Landroid/widget/TextView;

    const v2, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    invoke-virtual {p0, v2}, Lcom/twitter/android/BaseSignUpActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    iget v0, p0, Lcom/twitter/android/BaseSignUpActivity;->n:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/BaseSignUpActivity;->p:Lcom/twitter/android/bj;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/android/bj;->b(I)V

    goto/16 :goto_0
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
