.class public Lcom/twitter/android/AgeGateActivity;
.super Lcom/twitter/android/UserQueryActivity;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field protected a:Lcom/twitter/library/scribe/ScribeAssociation;

.field private e:Lcom/twitter/library/api/PromotedContent;

.field private f:J

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/widget/DatePicker;

.field private i:Lcom/twitter/internal/android/widget/TypefacesTextView;

.field private j:Lcom/twitter/library/util/m;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UserQueryActivity;-><init>()V

    return-void
.end method

.method private static a(Ljava/util/Calendar;)J
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xb

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xc

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xd

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    const/16 v0, 0xe

    invoke-virtual {p0, v0, v1}, Ljava/util/Calendar;->set(II)V

    invoke-virtual {p0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic a(Lcom/twitter/android/AgeGateActivity;)Lcom/twitter/library/util/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AgeGateActivity;->j:Lcom/twitter/library/util/m;

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-static {p0, v0, v1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object p1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AgeGateActivity;->g:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/AgeGateActivity;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/AgeGateActivity;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public static a(JJ)Z
    .locals 5

    const-string/jumbo v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, p0, p1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v0}, Lcom/twitter/android/AgeGateActivity;->a(Ljava/util/Calendar;)J

    move-result-wide v1

    invoke-virtual {v0, p2, p3}, Ljava/util/Calendar;->setTimeInMillis(J)V

    invoke-static {v0}, Lcom/twitter/android/AgeGateActivity;->a(Ljava/util/Calendar;)J

    move-result-wide v3

    cmp-long v0, v3, v1

    if-gtz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 5

    const-string/jumbo v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AgeGateActivity;->h:Landroid/widget/DatePicker;

    invoke-virtual {v1}, Landroid/widget/DatePicker;->getYear()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/AgeGateActivity;->h:Landroid/widget/DatePicker;

    invoke-virtual {v2}, Landroid/widget/DatePicker;->getMonth()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/AgeGateActivity;->h:Landroid/widget/DatePicker;

    invoke-virtual {v3}, Landroid/widget/DatePicker;->getDayOfMonth()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    iget-wide v1, p0, Lcom/twitter/android/AgeGateActivity;->f:J

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/android/AgeGateActivity;->a(JJ)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030010    # com.twitter.android.R.layout.age_gating

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected a()Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const-string/jumbo v1, "age_gate"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 1

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->j()V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 10

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->a()Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AgeGateActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/AgeGateActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v3}, Lcom/twitter/android/client/bn;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/android/client/bn;

    const v0, 0x7f09007a    # com.twitter.android.R.id.avatar_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/AgeGateActivity;->g:Landroid/widget/ImageView;

    const v0, 0x7f09007b    # com.twitter.android.R.id.header_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    iput-object v0, p0, Lcom/twitter/android/AgeGateActivity;->i:Lcom/twitter/internal/android/widget/TypefacesTextView;

    const v0, 0x7f09007e    # com.twitter.android.R.id.birthday_picker

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/DatePicker;

    iput-object v0, p0, Lcom/twitter/android/AgeGateActivity;->h:Landroid/widget/DatePicker;

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/AgeGateActivity;->a:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, ""

    const-string/jumbo v8, ""

    const-string/jumbo v9, "impression"

    invoke-static {v6, v7, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v0, v5

    invoke-virtual {v1, v3, v4, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f090080    # com.twitter.android.R.id.tos

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    new-instance v1, Lcom/twitter/android/ae;

    invoke-direct {v1, p0}, Lcom/twitter/android/ae;-><init>(Lcom/twitter/android/AgeGateActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->getPaintFlags()I

    move-result v1

    or-int/lit8 v1, v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setPaintFlags(I)V

    invoke-virtual {v2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/AgeGateActivity;->b:J

    const-string/jumbo v1, "user_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/AgeGateActivity;->c:Ljava/lang/String;

    const-string/jumbo v1, "age_gate_timestamp"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/AgeGateActivity;->f:J

    const-string/jumbo v1, "impression_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "is_earned"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/twitter/library/api/PromotedContent;

    invoke-direct {v2}, Lcom/twitter/library/api/PromotedContent;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/AgeGateActivity;->e:Lcom/twitter/library/api/PromotedContent;

    iget-object v2, p0, Lcom/twitter/android/AgeGateActivity;->e:Lcom/twitter/library/api/PromotedContent;

    iput-object v1, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AgeGateActivity;->e:Lcom/twitter/library/api/PromotedContent;

    const-string/jumbo v1, "earned"

    iput-object v1, v0, Lcom/twitter/library/api/PromotedContent;->disclosureType:Ljava/lang/String;

    :cond_1
    const v0, 0x7f0f0022    # com.twitter.android.R.string.age_gating_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->setTitle(I)V

    new-instance v0, Lcom/twitter/android/af;

    invoke-direct {v0, p0}, Lcom/twitter/android/af;-><init>(Lcom/twitter/android/AgeGateActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->a(Lcom/twitter/library/client/j;)V

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->g()V

    goto/16 :goto_0
.end method

.method protected a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    iget-object v0, p1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00af    # com.twitter.android.R.dimen.profile_avatar_size

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/util/m;

    invoke-direct {v3, v2, v1, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    iput-object v3, p0, Lcom/twitter/android/AgeGateActivity;->j:Lcom/twitter/library/util/m;

    iget-object v1, p0, Lcom/twitter/android/AgeGateActivity;->j:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/AgeGateActivity;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    const v0, 0x7f0f0021    # com.twitter.android.R.string.age_gating_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AgeGateActivity;->i:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/high16 v0, 0x7f110000    # com.twitter.android.R.menu.age_gating

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 7

    const/4 v5, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090300    # com.twitter.android.R.id.verify

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/AgeGateActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/AgeGateActivity;->b:J

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/AgeGateActivity;->e:Lcom/twitter/library/api/PromotedContent;

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;ZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->d(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->j()V

    :cond_0
    :goto_0
    return v5

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->H()V

    const v0, 0x7f0f001e    # com.twitter.android.R.string.age_gating_failed

    invoke-virtual {p0, v0}, Lcom/twitter/android/AgeGateActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->j()V

    goto :goto_0

    :cond_2
    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/AgeGateActivity;->j()V

    goto :goto_0
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    return-void
.end method
