.class public Lcom/twitter/android/ListTabActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;
.implements Lcom/twitter/android/widget/ce;


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field b:J

.field c:J

.field d:Ljava/lang/String;

.field e:J

.field f:I

.field g:Ljava/lang/String;

.field h:Ljava/lang/String;

.field i:Lcom/twitter/android/gs;

.field private j:Lcom/twitter/android/iv;

.field private k:Landroid/widget/RadioGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "ev_content"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/ListTabActivity;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ListTabActivity;->f:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/ListTabActivity;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/ListTabActivity;->a(Z)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    invoke-virtual {v0, p1}, Lcom/twitter/android/gs;->a(Ljava/lang/String;)Z

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const-string/jumbo v1, "list_tweets"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "list:tweets:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "tweets"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->b(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v1, "list_members"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "list:members:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "members"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->b(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    goto :goto_0
.end method

.method private a(Z)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    const v0, 0x7f0900e1    # com.twitter.android.R.id.dock

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/DockLayout;

    const v1, 0x7f0900e0    # com.twitter.android.R.id.loading

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    if-eqz p1, :cond_0

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/DockLayout;->setVisibility(I)V

    invoke-virtual {v1, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ListTabActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ListTabActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/ListTabActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ListTabActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/ListTabActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ListTabActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(Z)V

    const v1, 0x7f0300a2    # com.twitter.android.R.layout.list_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->e(J)Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "me:lists:list::delete"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->finish()V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 6

    const-wide/16 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/iw;

    invoke-direct {v1, p0}, Lcom/twitter/android/iw;-><init>(Lcom/twitter/android/ListTabActivity;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListTabActivity;->a(Lcom/twitter/library/client/j;)V

    new-instance v1, Lcom/twitter/android/iv;

    invoke-direct {v1, p0, p0}, Lcom/twitter/android/iv;-><init>(Lcom/twitter/android/ListTabActivity;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/android/ListTabActivity;->j:Lcom/twitter/android/iv;

    const-string/jumbo v1, "owner_id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ListTabActivity;->e:J

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ListTabActivity;->d:Ljava/lang/String;

    const-string/jumbo v1, "slug"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ListTabActivity;->h:Ljava/lang/String;

    if-eqz p1, :cond_0

    const-string/jumbo v1, "state_cur_tag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/ListTabActivity;->g:Ljava/lang/String;

    :cond_0
    const-string/jumbo v1, "creator_id"

    invoke-virtual {v0, v1, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ListTabActivity;->c:J

    const-string/jumbo v1, "list_id"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/ListTabActivity;->b:J

    iget-wide v1, p0, Lcom/twitter/android/ListTabActivity;->b:J

    cmp-long v1, v1, v4

    if-lez v1, :cond_1

    iget-wide v1, p0, Lcom/twitter/android/ListTabActivity;->c:J

    cmp-long v1, v1, v4

    if-gtz v1, :cond_3

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ListTabActivity;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/ListTabActivity;->h:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ListTabActivity;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ListTabActivity;->h:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->c(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListTabActivity;->d(Ljava/lang/String;)V

    :goto_0
    const-string/jumbo v1, "list_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListTabActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "list"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    return-void

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->c()V

    goto :goto_0
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-wide v3, p0, Lcom/twitter/android/ListTabActivity;->c:J

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    const v3, 0x7f09031a    # com.twitter.android.R.id.menu_edit_list

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    const v3, 0x7f09031b    # com.twitter.android.R.id.menu_delete_list

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    invoke-virtual {v3, v0}, Lhn;->b(Z)Lhn;

    const v3, 0x7f09031c    # com.twitter.android.R.id.menu_follow_list

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    if-nez v0, :cond_2

    iget v3, p0, Lcom/twitter/android/ListTabActivity;->f:I

    const/4 v5, 0x2

    if-ne v3, v5, :cond_2

    move v3, v1

    :goto_1
    invoke-virtual {v4, v3}, Lhn;->b(Z)Lhn;

    const v3, 0x7f09031d    # com.twitter.android.R.id.menu_unfollow_list

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ListTabActivity;->f:I

    if-ne v0, v1, :cond_0

    move v2, v1

    :cond_0
    invoke-virtual {v3, v2}, Lhn;->b(Z)Lhn;

    return v1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f110013    # com.twitter.android.R.menu.list_tab

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 6

    const/4 v1, 0x5

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v2

    const v3, 0x7f09031a    # com.twitter.android.R.id.menu_edit_list

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/ListCreateEditActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "list_id"

    iget-wide v4, p0, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v3, "name"

    const-string/jumbo v4, "list_name"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "description"

    const-string/jumbo v4, "list_description"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "full_name"

    const-string/jumbo v4, "list_fullname"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v3, "mode"

    const-string/jumbo v4, "list_mode"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v2, v0}, Lcom/twitter/android/ListTabActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return v0

    :cond_0
    const v3, 0x7f09031b    # com.twitter.android.R.id.menu_delete_list

    if-ne v2, v3, :cond_1

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0216    # com.twitter.android.R.string.lists_delete_list

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0217    # com.twitter.android.R.string.lists_delete_question

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0

    :cond_1
    const v0, 0x7f09031d    # com.twitter.android.R.id.menu_unfollow_list

    if-ne v2, v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/ListTabActivity;->b:J

    iget-wide v4, p0, Lcom/twitter/android/ListTabActivity;->c:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->b(IJJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListTabActivity;->d(Ljava/lang/String;)V

    :cond_2
    :goto_1
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0

    :cond_3
    const v0, 0x7f09031c    # com.twitter.android.R.id.menu_follow_list

    if-ne v2, v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v2, p0, Lcom/twitter/android/ListTabActivity;->b:J

    iget-wide v4, p0, Lcom/twitter/android/ListTabActivity;->c:J

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(IJJ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListTabActivity;->d(Ljava/lang/String;)V

    goto :goto_1
.end method

.method b()V
    .locals 10

    const/4 v2, 0x0

    sget-object v0, Lcom/twitter/library/provider/ai;->b:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    iget-wide v3, p0, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string/jumbo v1, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->j:Lcom/twitter/android/iv;

    const/4 v1, 0x2

    sget-object v4, Lcom/twitter/android/ListTabActivity;->a:[Ljava/lang/String;

    const-string/jumbo v5, "list_mapping_user_id=?"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-wide v8, p0, Lcom/twitter/android/ListTabActivity;->e:J

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/iv;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method c()V
    .locals 12

    const/4 v2, 0x0

    const v11, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    const/4 v10, 0x0

    const-wide/16 v8, 0x0

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    if-nez v0, :cond_0

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/twitter/android/gr;

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v5, "type"

    const/16 v6, 0x9

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v5, "owner_id"

    iget-wide v6, p0, Lcom/twitter/android/ListTabActivity;->c:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v5, "tag"

    iget-wide v6, p0, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v5, "shim_size"

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Lcom/twitter/android/gr;

    const-class v6, Lcom/twitter/android/TimelineFragment;

    const-string/jumbo v7, "list_tweets"

    invoke-direct {v5, v4, v6, v7}, Lcom/twitter/android/gr;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    aput-object v5, v0, v10

    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v5, "type"

    const-string/jumbo v6, "type"

    const/4 v7, 0x4

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v5, "owner_id"

    iget-wide v6, p0, Lcom/twitter/android/ListTabActivity;->c:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v5, "tag"

    iget-wide v6, p0, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v5, "follow"

    const-string/jumbo v6, "follow"

    invoke-virtual {v3, v6, v10}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v5, "shim_size"

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6, v11}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v5, Lcom/twitter/android/gr;

    const-class v6, Lcom/twitter/android/UsersFragment;

    const-string/jumbo v7, "list_members"

    invoke-direct {v5, v4, v6, v7}, Lcom/twitter/android/gr;-><init>(Landroid/os/Bundle;Ljava/lang/Class;Ljava/lang/String;)V

    aput-object v5, v0, v1

    new-instance v4, Lcom/twitter/android/gs;

    const v5, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-direct {v4, p0, v5, v0}, Lcom/twitter/android/gs;-><init>(Landroid/support/v4/app/FragmentActivity;I[Lcom/twitter/android/gr;)V

    iput-object v4, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    const v0, 0x7f0900e2    # com.twitter.android.R.id.list_scope_bar

    invoke-virtual {p0, v0}, Lcom/twitter/android/ListTabActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioGroup;

    iput-object v0, p0, Lcom/twitter/android/ListTabActivity;->k:Landroid/widget/RadioGroup;

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->k:Landroid/widget/RadioGroup;

    invoke-virtual {v0, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->g:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->g:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/ListTabActivity;->a(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->m_()V

    iget-wide v3, p0, Lcom/twitter/android/ListTabActivity;->b:J

    cmp-long v0, v3, v8

    if-lez v0, :cond_4

    iget-wide v3, p0, Lcom/twitter/android/ListTabActivity;->e:J

    cmp-long v0, v3, v8

    if-lez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->j:Lcom/twitter/android/iv;

    sget-object v3, Lcom/twitter/library/provider/ai;->b:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    iget-wide v4, p0, Lcom/twitter/android/ListTabActivity;->b:J

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "ownerId"

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    sget-object v4, Lcom/twitter/android/ListTabActivity;->a:[Ljava/lang/String;

    const-string/jumbo v5, "list_mapping_user_id=?"

    new-array v6, v1, [Ljava/lang/String;

    iget-wide v7, p0, Lcom/twitter/android/ListTabActivity;->e:J

    invoke-static {v7, v8}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/iv;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const-string/jumbo v0, "tab"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "tab"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ListTabActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "list_tweets"

    invoke-direct {p0, v0}, Lcom/twitter/android/ListTabActivity;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_4
    iget-wide v2, p0, Lcom/twitter/android/ListTabActivity;->b:J

    cmp-long v0, v2, v8

    if-lez v0, :cond_5

    iget-wide v2, p0, Lcom/twitter/android/ListTabActivity;->c:J

    cmp-long v0, v2, v8

    if-gtz v0, :cond_1

    :cond_5
    const v0, 0x7f0f021c    # com.twitter.android.R.string.lists_no_content

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "list_name"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "list_description"

    const-string/jumbo v3, "description"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "list_fullname"

    const-string/jumbo v3, "full_name"

    invoke-virtual {p3, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v2, "list_mode"

    const-string/jumbo v3, "mode"

    const/4 v4, 0x0

    invoke-virtual {p3, v3, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v1}, Lcom/twitter/android/ListTabActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const v1, 0x7f0901b1    # com.twitter.android.R.id.tweets

    if-ne p2, v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    invoke-virtual {v1, v5}, Lcom/twitter/android/gs;->a(I)Z

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "list:tweets:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "tweets"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->b(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0901b2    # com.twitter.android.R.id.people

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    invoke-virtual {v1, v3}, Lcom/twitter/android/gs;->a(I)Z

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "list:members:::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, "members"

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->b(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    goto :goto_0
.end method

.method protected onResume()V
    .locals 4

    const-wide/16 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/android/ListTabActivity;->e:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-wide v0, p0, Lcom/twitter/android/ListTabActivity;->b:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Z()Z

    move-result v0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->Z()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ListTabActivity;->f:I

    invoke-virtual {p0}, Lcom/twitter/android/ListTabActivity;->b()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    goto :goto_0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "state_cur_tag"

    iget-object v1, p0, Lcom/twitter/android/ListTabActivity;->i:Lcom/twitter/android/gs;

    invoke-virtual {v1}, Lcom/twitter/android/gs;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method
