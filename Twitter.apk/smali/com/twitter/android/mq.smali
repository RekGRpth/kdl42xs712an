.class Lcom/twitter/android/mq;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/MessagesFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v0, p1}, Lcom/twitter/android/MessagesFragment;->a(Lcom/twitter/android/MessagesFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-static {v1, v0}, Lcom/twitter/android/MessagesFragment;->a(Lcom/twitter/android/MessagesFragment;I)V

    const/16 v0, 0xc8

    if-eq p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0288    # com.twitter.android.R.string.messages_fetch_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesFragment;->d(Lcom/twitter/android/MessagesFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public g(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lcom/twitter/android/mq;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public h(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p2, p3}, Lcom/twitter/android/mq;->a(Ljava/lang/String;I)V

    return-void
.end method

.method public i(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/MessagesFragment;->b(Lcom/twitter/android/MessagesFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-static {v1, v0}, Lcom/twitter/android/MessagesFragment;->b(Lcom/twitter/android/MessagesFragment;I)V

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x194

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mq;->a:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0287    # com.twitter.android.R.string.messages_delete_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
