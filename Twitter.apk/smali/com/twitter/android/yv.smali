.class Lcom/twitter/android/yv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/zb;

.field final synthetic b:J

.field final synthetic c:Lcom/twitter/library/api/PromotedContent;

.field final synthetic d:Lcom/twitter/android/zd;

.field final synthetic e:Lcom/twitter/android/za;

.field final synthetic f:Landroid/content/res/Resources;

.field final synthetic g:Lcom/twitter/android/yu;


# direct methods
.method constructor <init>(Lcom/twitter/android/yu;Lcom/twitter/android/zb;JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zd;Lcom/twitter/android/za;Landroid/content/res/Resources;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/yv;->g:Lcom/twitter/android/yu;

    iput-object p2, p0, Lcom/twitter/android/yv;->a:Lcom/twitter/android/zb;

    iput-wide p3, p0, Lcom/twitter/android/yv;->b:J

    iput-object p5, p0, Lcom/twitter/android/yv;->c:Lcom/twitter/library/api/PromotedContent;

    iput-object p6, p0, Lcom/twitter/android/yv;->d:Lcom/twitter/android/zd;

    iput-object p7, p0, Lcom/twitter/android/yv;->e:Lcom/twitter/android/za;

    iput-object p8, p0, Lcom/twitter/android/yv;->f:Landroid/content/res/Resources;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/yv;->a:Lcom/twitter/android/zb;

    iget-wide v1, p0, Lcom/twitter/android/yv;->b:J

    iget-object v3, p0, Lcom/twitter/android/yv;->c:Lcom/twitter/library/api/PromotedContent;

    iget-object v4, p0, Lcom/twitter/android/yv;->g:Lcom/twitter/android/yu;

    invoke-static {v4}, Lcom/twitter/android/yu;->a(Lcom/twitter/android/yu;)I

    move-result v4

    iget-object v5, p0, Lcom/twitter/android/yv;->d:Lcom/twitter/android/zd;

    invoke-interface/range {v0 .. v5}, Lcom/twitter/android/zb;->b(JLcom/twitter/library/api/PromotedContent;ILcom/twitter/android/zd;)I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/yv;->e:Lcom/twitter/android/za;

    iget-object v2, p0, Lcom/twitter/android/yv;->f:Landroid/content/res/Resources;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/za;->a(ILandroid/content/res/Resources;)V

    iget-object v1, p0, Lcom/twitter/android/yv;->d:Lcom/twitter/android/zd;

    iput v0, v1, Lcom/twitter/android/zd;->b:I

    return-void
.end method
