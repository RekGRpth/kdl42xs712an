.class Lcom/twitter/android/ui;
.super Lcom/twitter/library/view/g;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/um;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/ur;

.field final synthetic d:Lcom/twitter/android/uh;


# direct methods
.method constructor <init>(Lcom/twitter/android/uh;Landroid/view/ViewParent;ILcom/twitter/android/um;ILcom/twitter/android/ur;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    iput-object p4, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iput p5, p0, Lcom/twitter/android/ui;->b:I

    iput-object p6, p0, Lcom/twitter/android/ui;->c:Lcom/twitter/android/ur;

    invoke-direct {p0, p2, p3}, Lcom/twitter/library/view/g;-><init>(Landroid/view/ViewParent;I)V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 12

    const/4 v11, 0x2

    const/4 v10, 0x0

    const/4 v7, 0x3

    const/4 v9, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v0, v0, Lcom/twitter/android/um;->b:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PipView;->getPipOnPosition()I

    move-result v8

    iget-object v0, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v0, v0, Lcom/twitter/android/um;->b:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    invoke-super {p0, p1}, Lcom/twitter/library/view/g;->onPageSelected(I)V

    iget-object v0, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v0, v0, Lcom/twitter/android/um;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    invoke-virtual {v0}, Lcom/twitter/android/uh;->getCount()I

    move-result v0

    if-le v0, v9, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v0, v0, Lcom/twitter/android/um;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/az;

    invoke-virtual {v0, p1}, Lcom/twitter/android/az;->a(I)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v1, v0, Lcom/twitter/android/um;->d:Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/ui;->b:I

    sparse-switch v0, :sswitch_data_0

    const-wide/16 v2, -0x1

    const/4 v4, -0x1

    move-object v5, v6

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ui;->c:Lcom/twitter/android/ur;

    iget-object v0, v0, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    iput p1, v1, Lcom/twitter/library/scribe/ScribeItem;->h:I

    const-string/jumbo v0, "multipage"

    iput-object v0, v1, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    iget-object v0, v0, Lcom/twitter/android/uh;->b:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    iget-object v3, v3, Lcom/twitter/android/uh;->c:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    invoke-static {v4}, Lcom/twitter/android/uh;->a(Lcom/twitter/android/uh;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v10

    aput-object v6, v3, v9

    iget-object v4, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v4, v4, Lcom/twitter/android/um;->d:Ljava/lang/String;

    aput-object v4, v3, v11

    iget-object v4, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v4, v4, Lcom/twitter/android/um;->e:Ljava/lang/String;

    aput-object v4, v3, v7

    const/4 v4, 0x4

    const-string/jumbo v5, "results"

    aput-object v5, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    if-ge v8, p1, :cond_1

    const-string/jumbo v0, "next"

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    iget-object v2, v2, Lcom/twitter/android/uh;->b:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    iget-object v4, v4, Lcom/twitter/android/uh;->c:Lcom/twitter/library/client/aa;

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/ui;->d:Lcom/twitter/android/uh;

    invoke-static {v5}, Lcom/twitter/android/uh;->a(Lcom/twitter/android/uh;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v10

    aput-object v6, v4, v9

    iget-object v5, p0, Lcom/twitter/android/ui;->a:Lcom/twitter/android/um;

    iget-object v5, v5, Lcom/twitter/android/um;->d:Ljava/lang/String;

    aput-object v5, v4, v11

    aput-object v0, v4, v7

    const/4 v0, 0x4

    const-string/jumbo v5, "click"

    aput-object v5, v4, v0

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void

    :sswitch_0
    const-wide/16 v2, -0x1

    const/16 v4, 0x10

    goto/16 :goto_0

    :sswitch_1
    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    move-object v5, v6

    move v4, v7

    goto/16 :goto_0

    :cond_1
    const-string/jumbo v0, "previous"

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_0
        0x8 -> :sswitch_1
    .end sparse-switch
.end method
