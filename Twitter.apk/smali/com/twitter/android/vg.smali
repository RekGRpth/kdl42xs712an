.class Lcom/twitter/android/vg;
.super Landroid/support/v4/widget/SimpleCursorAdapter;
.source "Twttr"


# instance fields
.field private final a:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Landroid/support/v4/widget/SimpleCursorAdapter;-><init>(Landroid/content/Context;ILandroid/database/Cursor;[Ljava/lang/String;[II)V

    iput-object p4, p0, Lcom/twitter/android/vg;->a:[Ljava/lang/String;

    return-void
.end method

.method private a([Ljava/lang/String;Landroid/database/Cursor;)V
    .locals 4

    if-eqz p2, :cond_2

    array-length v1, p1

    iget-object v0, p0, Lcom/twitter/android/vg;->mFrom:[I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vg;->mFrom:[I

    array-length v0, v0

    if-eq v0, v1, :cond_1

    :cond_0
    new-array v0, v1, [I

    iput-object v0, p0, Lcom/twitter/android/vg;->mFrom:[I

    :cond_1
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_3

    iget-object v2, p0, Lcom/twitter/android/vg;->mFrom:[I

    aget-object v3, p1, v0

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v3

    aput v3, v2, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/vg;->mFrom:[I

    :cond_3
    return-void
.end method


# virtual methods
.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/vg;->mFrom:[I

    if-nez v0, :cond_0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vg;->a:[Ljava/lang/String;

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/vg;->a([Ljava/lang/String;Landroid/database/Cursor;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/support/v4/widget/SimpleCursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
