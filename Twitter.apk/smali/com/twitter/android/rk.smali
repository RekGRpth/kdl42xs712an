.class Lcom/twitter/android/rk;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/RemoveAccountActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/RemoveAccountActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rk;->a:Lcom/twitter/android/RemoveAccountActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/rk;->a:Lcom/twitter/android/RemoveAccountActivity;

    iget-object v0, v0, Lcom/twitter/android/RemoveAccountActivity;->a:Landroid/accounts/AccountAuthenticatorResponse;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "booleanResult"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v1, p0, Lcom/twitter/android/rk;->a:Lcom/twitter/android/RemoveAccountActivity;

    iget-object v1, v1, Lcom/twitter/android/RemoveAccountActivity;->a:Landroid/accounts/AccountAuthenticatorResponse;

    invoke-virtual {v1, v0}, Landroid/accounts/AccountAuthenticatorResponse;->onResult(Landroid/os/Bundle;)V

    :cond_0
    return-void
.end method
