.class Lcom/twitter/android/nv;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/NotificationSettingsActivity;

.field private b:Z

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:I

.field private t:Z

.field private final u:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/twitter/android/NotificationSettingsActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/nv;->u:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/preference/Preference;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_0

    check-cast p1, Landroid/preference/ListPreference;

    invoke-static {p2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/twitter/library/util/Util;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    goto :goto_0
.end method

.method private b(Landroid/preference/Preference;I)V
    .locals 1

    const/4 v0, 0x1

    check-cast p1, Landroid/preference/CheckBoxPreference;

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Ljava/lang/Void;)V
    .locals 6

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/NotificationSettingsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/NotificationSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    const-string/jumbo v4, "vibrate"

    invoke-virtual {v0, v4}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    invoke-static {v3}, Lcom/twitter/library/util/Util;->p(Landroid/content/Context;)Z

    move-result v4

    if-nez v4, :cond_1

    iput-boolean v2, p0, Lcom/twitter/android/nv;->b:Z

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSelectable(Z)V

    const v4, 0x7f0f042d    # com.twitter.android.R.string.settings_vibrate_not_available_summary

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setSummary(I)V

    :cond_1
    iget-object v4, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v5, p0, Lcom/twitter/android/nv;->b:Z

    iput-boolean v5, v4, Lcom/twitter/android/NotificationSettingsActivity;->y:Z

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v4, p0, Lcom/twitter/android/nv;->b:Z

    invoke-virtual {v0, v4}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    const-string/jumbo v4, "ringtone"

    invoke-virtual {v0, v4}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/nv;->d:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/preference/Preference;->setDefaultValue(Ljava/lang/Object;)V

    invoke-static {v3}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v3, "ringtone"

    iget-object v4, p0, Lcom/twitter/android/nv;->d:Ljava/lang/String;

    invoke-interface {v0, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v3, p0, Lcom/twitter/android/nv;->d:Ljava/lang/String;

    iput-object v3, v0, Lcom/twitter/android/NotificationSettingsActivity;->A:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v3, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v3, v3, Lcom/twitter/android/NotificationSettingsActivity;->A:Ljava/lang/String;

    iput-object v3, v0, Lcom/twitter/android/NotificationSettingsActivity;->d:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    const-string/jumbo v3, "use_led"

    invoke-virtual {v0, v3}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v3, p0, Lcom/twitter/android/nv;->c:Z

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v3, p0, Lcom/twitter/android/nv;->c:Z

    iput-boolean v3, v0, Lcom/twitter/android/NotificationSettingsActivity;->z:Z

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->E:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->g:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->a(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->H:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->j:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->K:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->k:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_6

    iget-object v3, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, p0, Lcom/twitter/android/nv;->e:I

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_1
    iget v4, p0, Lcom/twitter/android/nv;->f:I

    invoke-virtual {v3, v0, v4, v2}, Lcom/twitter/android/NotificationSettingsActivity;->a(ZIZ)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v3, p0, Lcom/twitter/android/nv;->t:Z

    iput-boolean v3, v0, Lcom/twitter/android/NotificationSettingsActivity;->B:Z

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->L:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->n:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->I:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->l:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->J:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->m:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->M:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->o:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->N:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->p:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    iget v3, p0, Lcom/twitter/android/nv;->q:I

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->j:Z

    if-eqz v0, :cond_5

    iget-object v3, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v4, p0, Lcom/twitter/android/nv;->h:I

    iget v0, p0, Lcom/twitter/android/nv;->r:I

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v3, v4, v0}, Lcom/twitter/android/NotificationSettingsActivity;->a(IZ)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v3, p0, Lcom/twitter/android/nv;->i:I

    iget v4, p0, Lcom/twitter/android/nv;->s:I

    if-ne v4, v1, :cond_2

    move v2, v1

    :cond_2
    invoke-virtual {v0, v3, v2}, Lcom/twitter/android/NotificationSettingsActivity;->b(IZ)V

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->l:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->q:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->m:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->r:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->h:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->f:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->i:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->h:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->o:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->t:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->p:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->u:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->q:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->v:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->r:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->w:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->s:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->x:I

    :goto_4
    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->g:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->l:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->k:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->p:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->e:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->k:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->h:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->m:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->i:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->n:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->j:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->o:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, p0, Lcom/twitter/android/nv;->n:I

    iput v2, v0, Lcom/twitter/android/NotificationSettingsActivity;->s:I

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    iget v2, p0, Lcom/twitter/android/nv;->h:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/nv;->a(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    iget v2, p0, Lcom/twitter/android/nv;->i:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/nv;->a(Landroid/preference/Preference;I)V

    goto/16 :goto_3

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    iget v2, p0, Lcom/twitter/android/nv;->e:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/nv;->b(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    iget v2, p0, Lcom/twitter/android/nv;->h:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/nv;->a(Landroid/preference/Preference;I)V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    iget v2, p0, Lcom/twitter/android/nv;->i:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/nv;->a(Landroid/preference/Preference;I)V

    goto :goto_4
.end method

.method protected varargs a([Ljava/lang/Void;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->a(Lcom/twitter/android/NotificationSettingsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/nv;->t:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    return-void
.end method

.method protected varargs b([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 20

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nv;->u:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/twitter/android/nv;->t:Z

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/nv;->publishProgress([Ljava/lang/Object;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-virtual {v2}, Lcom/twitter/android/NotificationSettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nv;->u:Ljava/lang/String;

    move-object/from16 v18, v0

    sget-object v3, Lcom/twitter/library/provider/i;->a:Landroid/net/Uri;

    move-object/from16 v0, v18

    invoke-static {v3, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/16 v4, 0xb

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "vibrate"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "ringtone"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "light"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "notif_tweet"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "notif_mention"

    aput-object v6, v4, v5

    const/4 v5, 0x5

    const-string/jumbo v6, "notif_message"

    aput-object v6, v4, v5

    const/4 v5, 0x6

    const-string/jumbo v6, "notif_experimental"

    aput-object v6, v4, v5

    const/4 v5, 0x7

    const-string/jumbo v6, "notif_lifeline_alerts"

    aput-object v6, v4, v5

    const/16 v5, 0x8

    const-string/jumbo v6, "notif_recommendations"

    aput-object v6, v4, v5

    const/16 v5, 0x9

    const-string/jumbo v6, "notif_news"

    aput-object v6, v4, v5

    const/16 v5, 0xa

    const-string/jumbo v6, "notif_vit_notable_event"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v14

    const/4 v3, 0x1

    sget-object v4, Lcom/twitter/library/provider/i;->b:Ljava/lang/String;

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v7, v7, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v7, :cond_d

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    :goto_0
    if-eqz v14, :cond_11

    invoke-interface {v14}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v15

    if-eqz v15, :cond_10

    const/4 v15, 0x0

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_1

    const/4 v3, 0x0

    invoke-interface {v14, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v15, 0x1

    if-ne v3, v15, :cond_e

    const/4 v3, 0x1

    :cond_1
    :goto_1
    const/4 v15, 0x1

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_2

    const/4 v4, 0x1

    invoke-interface {v14, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    :cond_2
    const/4 v15, 0x2

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_3

    const/4 v5, 0x2

    invoke-interface {v14, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v15, 0x1

    if-ne v5, v15, :cond_f

    const/4 v5, 0x1

    :cond_3
    :goto_2
    const/4 v15, 0x3

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_4

    const/4 v6, 0x3

    invoke-interface {v14, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    :cond_4
    const/4 v15, 0x4

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_5

    const/4 v7, 0x4

    invoke-interface {v14, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    :cond_5
    const/4 v15, 0x5

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_6

    const/4 v8, 0x5

    invoke-interface {v14, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    :cond_6
    const/4 v15, 0x6

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_7

    const/4 v9, 0x6

    invoke-interface {v14, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    :cond_7
    const/4 v15, 0x7

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_8

    const/4 v10, 0x7

    invoke-interface {v14, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    :cond_8
    const/16 v15, 0x8

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_9

    const/16 v11, 0x8

    invoke-interface {v14, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    :cond_9
    const/16 v15, 0x9

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_a

    const/16 v12, 0x9

    invoke-interface {v14, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    :cond_a
    const/16 v15, 0xa

    invoke-interface {v14, v15}, Landroid/database/Cursor;->isNull(I)Z

    move-result v15

    if-nez v15, :cond_10

    const/16 v13, 0xa

    invoke-interface {v14, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    move/from16 v19, v13

    move v13, v3

    move/from16 v3, v19

    :goto_3
    invoke-interface {v14}, Landroid/database/Cursor;->close()V

    move v14, v7

    move v15, v5

    move-object/from16 v16, v4

    move/from16 v17, v13

    move v13, v3

    :goto_4
    sget-object v3, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v3, v6}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/nv;->e:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/nv;->e:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_b

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v3}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    sget-object v5, Lcom/twitter/library/provider/ax;->l:Landroid/net/Uri;

    invoke-static {v5, v3, v4}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "_id"

    aput-object v6, v4, v5

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_b

    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/nv;->f:I

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    :cond_b
    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/twitter/android/nv;->b:Z

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/nv;->d:Ljava/lang/String;

    move-object/from16 v0, p0

    iput-boolean v15, v0, Lcom/twitter/android/nv;->c:Z

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->g:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->h:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->i:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->j:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v8}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->k:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v10}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->n:I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v2, :cond_c

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->l:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v9}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->m:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->r:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v14}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->s:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v11}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->o:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v12}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->p:I

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v13}, Lcom/twitter/library/provider/NotificationSetting;->c(I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/nv;->q:I

    :cond_c
    const/4 v2, 0x0

    return-object v2

    :cond_d
    const/16 v7, 0x7d5

    const/4 v8, 0x1

    const/4 v9, 0x1

    const/4 v10, 0x1

    const/4 v11, 0x1

    const/4 v12, 0x1

    const/4 v13, 0x1

    goto/16 :goto_0

    :cond_e
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_f
    const/4 v5, 0x0

    goto/16 :goto_2

    :cond_10
    move/from16 v19, v13

    move v13, v3

    move/from16 v3, v19

    goto/16 :goto_3

    :cond_11
    move v14, v7

    move v15, v5

    move-object/from16 v16, v4

    move/from16 v17, v3

    goto/16 :goto_4
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/nv;->b([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/nv;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/twitter/android/nv;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/NotificationSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setEnabled(Z)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/nv;->a([Ljava/lang/Void;)V

    return-void
.end method
