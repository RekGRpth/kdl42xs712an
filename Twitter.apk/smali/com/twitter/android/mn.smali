.class Lcom/twitter/android/mn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mn;->a:Lcom/twitter/android/MessagesFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 8

    const/4 v7, 0x1

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/twitter/android/mn;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesFragment;->a(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "messages:inbox:user_list:user:select"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/mn;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v4}, Lcom/twitter/android/MessagesFragment;->b(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {v4, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/mn;->a:Lcom/twitter/android/MessagesFragment;

    invoke-virtual {v4}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    const-class v5, Lcom/twitter/android/MessagesDetailActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "user_name"

    const/4 v5, 0x2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v4, "user_fullname"

    const/4 v5, 0x3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v4, "user_id"

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v4, "user_profile_image"

    const/4 v5, 0x4

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "owner_id"

    invoke-virtual {v3, v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v0, "keyboard_open"

    invoke-virtual {v3, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/mn;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesFragment;->c(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/android/md;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/mn;->a:Lcom/twitter/android/MessagesFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesFragment;->c(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/android/md;

    move-result-object v0

    invoke-interface {v0, v3}, Lcom/twitter/android/md;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
