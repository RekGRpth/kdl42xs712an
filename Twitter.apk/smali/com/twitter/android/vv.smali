.class public Lcom/twitter/android/vv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;


# instance fields
.field private final a:Lcom/twitter/android/vs;

.field private final b:Landroid/widget/ListView;

.field private c:I

.field private d:I

.field private e:I

.field private f:Lcom/twitter/android/vw;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseListFragment;Lcom/twitter/android/vs;Landroid/widget/ListView;I)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "tweetActionsHelper cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/twitter/android/vv;->a:Lcom/twitter/android/vs;

    iput-object p3, p0, Lcom/twitter/android/vv;->b:Landroid/widget/ListView;

    iput p4, p0, Lcom/twitter/android/vv;->e:I

    iget-object v0, p0, Lcom/twitter/android/vv;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vv;->a:Lcom/twitter/android/vs;

    invoke-virtual {v0}, Lcom/twitter/android/vs;->b()V

    iget-object v0, p0, Lcom/twitter/android/vv;->b:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setPressed(Z)V

    return-void
.end method

.method public a(Lcom/twitter/android/vw;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vv;->f:Lcom/twitter/android/vw;

    return-void
.end method

.method public a(Lcom/twitter/android/yd;J)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vv;->a:Lcom/twitter/android/vs;

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/vs;->a(Lcom/twitter/android/yd;J)V

    iget-object v0, p0, Lcom/twitter/android/vv;->b:Landroid/widget/ListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setPressed(Z)V

    const/4 v0, 0x1

    return v0
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/vv;->f:Lcom/twitter/android/vw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vv;->f:Lcom/twitter/android/vw;

    invoke-interface {v0, p2}, Lcom/twitter/android/vw;->b(Landroid/view/View;)Lcom/twitter/android/yd;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0, p4, p5}, Lcom/twitter/android/vv;->a(Lcom/twitter/android/yd;J)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/yd;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/twitter/android/yd;

    invoke-virtual {p0, v0, p4, p5}, Lcom/twitter/android/vv;->a(Lcom/twitter/android/yd;J)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    const/4 v0, 0x0

    return v0

    :pswitch_1
    iput v2, p0, Lcom/twitter/android/vv;->c:I

    iput v1, p0, Lcom/twitter/android/vv;->d:I

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/vv;->c:I

    sub-int/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v2, p0, Lcom/twitter/android/vv;->e:I

    if-gt v0, v2, :cond_1

    iget v0, p0, Lcom/twitter/android/vv;->d:I

    sub-int/2addr v0, v1

    invoke-static {v0}, Ljava/lang/Math;->abs(I)I

    move-result v0

    iget v1, p0, Lcom/twitter/android/vv;->e:I

    if-le v0, v1, :cond_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/vv;->a()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
