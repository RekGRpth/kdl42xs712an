.class Lcom/twitter/android/zs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Landroid/view/ViewGroup$MarginLayoutParams;

.field final synthetic b:Lcom/twitter/android/UserSelectFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/UserSelectFragment;Landroid/view/ViewGroup$MarginLayoutParams;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/zs;->b:Lcom/twitter/android/UserSelectFragment;

    iput-object p2, p0, Lcom/twitter/android/zs;->a:Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/zs;->b:Lcom/twitter/android/UserSelectFragment;

    iget-object v0, v0, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->getLineCount()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/zs;->b:Lcom/twitter/android/UserSelectFragment;

    invoke-virtual {v0}, Lcom/twitter/android/UserSelectFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0084    # com.twitter.android.R.dimen.media_tag_compose_extra_line_spacing

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/zs;->b:Lcom/twitter/android/UserSelectFragment;

    iget-object v1, v1, Lcom/twitter/android/UserSelectFragment;->a:Lcom/twitter/android/UserSelectFragment$UserSelectEditText;

    int-to-float v2, v0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/UserSelectFragment$UserSelectEditText;->setLineSpacing(FF)V

    iget-object v1, p0, Lcom/twitter/android/zs;->a:Landroid/view/ViewGroup$MarginLayoutParams;

    neg-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$MarginLayoutParams;->bottomMargin:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
