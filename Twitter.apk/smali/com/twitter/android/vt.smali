.class Lcom/twitter/android/vt;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private final a:Landroid/view/animation/Animation;

.field private final b:Z

.field private c:Landroid/view/View;

.field private d:Landroid/view/ViewGroup;


# direct methods
.method constructor <init>(Landroid/view/animation/Animation;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0xfa

    invoke-virtual {p1, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    invoke-virtual {p1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iput-object p1, p0, Lcom/twitter/android/vt;->a:Landroid/view/animation/Animation;

    iput-boolean p2, p0, Lcom/twitter/android/vt;->b:Z

    return-void
.end method


# virtual methods
.method a(Landroid/view/View;Landroid/view/ViewGroup;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/vt;->c:Landroid/view/View;

    iput-object p2, p0, Lcom/twitter/android/vt;->d:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/vt;->a:Landroid/view/animation/Animation;

    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    return-void
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/vt;->c:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/vt;->b:Z

    if-eqz v1, :cond_3

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    iput-object v4, p0, Lcom/twitter/android/vt;->c:Landroid/view/View;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/vt;->d:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/vt;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vt;->d:Landroid/view/ViewGroup;

    invoke-virtual {v0, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    :cond_1
    iput-object v4, p0, Lcom/twitter/android/vt;->d:Landroid/view/ViewGroup;

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method
