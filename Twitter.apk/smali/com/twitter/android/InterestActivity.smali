.class public Lcom/twitter/android/InterestActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 2

    new-instance v0, Lcom/twitter/android/iu;

    new-instance v1, Lcom/twitter/android/TimelineFragment;

    invoke-direct {v1}, Lcom/twitter/android/TimelineFragment;-><init>()V

    invoke-direct {v0, v1}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/InterestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "timeline_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
