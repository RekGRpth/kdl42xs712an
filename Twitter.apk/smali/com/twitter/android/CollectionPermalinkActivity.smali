.class public Lcom/twitter/android/CollectionPermalinkActivity;
.super Lcom/twitter/android/TimelineActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/TimelineActivity;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(Z)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 3

    check-cast p2, Lcom/twitter/android/ug;

    new-instance v0, Lcom/twitter/android/CollectionPermalinkFragment;

    invoke-direct {v0}, Lcom/twitter/android/CollectionPermalinkFragment;-><init>()V

    invoke-virtual {v0, p1}, Lcom/twitter/android/CollectionPermalinkFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v1

    iget-boolean v2, p2, Lcom/twitter/android/ug;->a:Z

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    new-instance v1, Lcom/twitter/android/iu;

    invoke-direct {v1, v0}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v1
.end method
