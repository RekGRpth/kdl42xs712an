.class public Lcom/twitter/android/TimelineFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/ty;
.implements Lcom/twitter/android/zb;


# instance fields
.field private final a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private b:Landroid/net/Uri;

.field private c:I

.field protected d:I

.field protected e:Ljava/lang/String;

.field protected f:Z

.field private g:I

.field private h:J

.field private i:J

.field private j:Z

.field private k:Lcom/twitter/library/util/aa;

.field private l:Ljava/util/ArrayList;

.field private final m:Ljava/util/HashMap;

.field private final n:Ljava/util/HashSet;

.field private o:Lcom/twitter/library/util/FriendshipCache;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private final s:Ljava/util/Map;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/TimelineFragment;->d:I

    new-instance v0, Lcom/twitter/android/uv;

    invoke-direct {v0, p0}, Lcom/twitter/android/uv;-><init>(Lcom/twitter/android/TimelineFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->l:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->m:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->n:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->s:Ljava/util/Map;

    return-void
.end method

.method private G()Z
    .locals 3

    const/4 v0, 0x0

    const/16 v1, 0x10

    iget v2, p0, Lcom/twitter/android/TimelineFragment;->F:I

    if-ne v1, v2, :cond_1

    const-string/jumbo v1, "LOCAL"

    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "TV"

    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "SPORTS"

    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method private H()Z
    .locals 3

    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    iget-boolean v1, p0, Lcom/twitter/android/TimelineFragment;->j:Z

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0

    :sswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/TimelineFragment;->j:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_0
        0x1c -> :sswitch_1
        0x1e -> :sswitch_1
    .end sparse-switch
.end method

.method private I()I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private J()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->g()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private K()Z
    .locals 2

    const-string/jumbo v0, "android_summary_cards_media_forward_1531"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "cards_media_forward_enabled"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method private L()I
    .locals 6

    const/4 v1, 0x2

    const/4 v0, 0x1

    const/4 v2, 0x0

    const-string/jumbo v3, "android_media_cards_media_forward_1567"

    invoke-static {v3}, Lkk;->c(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "android_media_cards_media_forward_1567"

    invoke-static {v3}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v3, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v5

    sparse-switch v5, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v3, :pswitch_data_0

    move v0, v2

    :goto_1
    :pswitch_0
    return v0

    :sswitch_0
    const-string/jumbo v5, "photo_only"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v2

    goto :goto_0

    :sswitch_1
    const-string/jumbo v5, "video_only"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v0

    goto :goto_0

    :sswitch_2
    const-string/jumbo v5, "photo_video_gallery"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v3, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x3

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6070a710 -> :sswitch_1
        -0x1e435667 -> :sswitch_0
        0x3a18d841 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private M()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->J()Z

    move-result v1

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->K()Z

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->f(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/ye;->d(Z)V

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->L()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->d(I)V

    invoke-static {}, Lgv;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ye;->g(Z)V

    invoke-virtual {v0}, Lcom/twitter/android/ye;->j()V

    return-void
.end method

.method private N()V
    .locals 1

    invoke-static {}, Lgv;->b()V

    const-string/jumbo v0, "android_view_count_on_me_t_1756"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_rt_action_1837"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->j()V

    return-void
.end method

.method private O()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method private T()V
    .locals 6

    const/4 v4, 0x6

    const/4 v0, 0x0

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    packed-switch v1, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid status type."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    :goto_0
    new-instance v3, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v3}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->b(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    goto :goto_0

    :pswitch_3
    const-string/jumbo v1, "profile_tweets"

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :pswitch_4
    const-string/jumbo v1, "favorites"

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :pswitch_5
    const-string/jumbo v2, "connect"

    const-string/jumbo v1, "mentions"

    goto :goto_0

    :pswitch_6
    const-string/jumbo v2, "connect"

    const-string/jumbo v1, "mentions_filtered"

    goto :goto_0

    :pswitch_7
    const-string/jumbo v2, "connect"

    const-string/jumbo v1, "mentions_following"

    goto :goto_0

    :pswitch_8
    const-string/jumbo v2, "connect"

    const-string/jumbo v1, "mentions_verified"

    goto :goto_0

    :pswitch_9
    const-string/jumbo v2, "list"

    const-string/jumbo v1, "tweets"

    goto :goto_0

    :pswitch_a
    const-string/jumbo v1, "favorite_people"

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :pswitch_b
    const-string/jumbo v1, "media_only_home"

    move-object v2, v1

    move-object v1, v0

    goto :goto_0

    :pswitch_c
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    move-object v5, v1

    move-object v1, v0

    move-object v0, v5

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_b
        :pswitch_c
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method private a(II)J
    .locals 5

    const-wide/16 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    :pswitch_2
    return-wide v0

    :cond_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    const/16 v3, 0x11

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne v3, p1, :cond_3

    const/16 v3, 0xf

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_3

    const/16 v3, 0x1a

    invoke-interface {v2, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    :goto_1
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    goto :goto_0

    :cond_4
    sparse-switch p1, :sswitch_data_0

    :cond_5
    const/16 v0, 0x15

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :sswitch_0
    sget v3, Lcom/twitter/library/provider/cc;->g:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/twitter/library/provider/au;->e(I)Z

    move-result v4

    if-nez v4, :cond_6

    invoke-static {v3}, Lcom/twitter/library/provider/au;->d(I)Z

    move-result v4

    if-eqz v4, :cond_3

    :cond_6
    invoke-static {v3}, Lcom/twitter/library/provider/au;->n(I)Z

    move-result v4

    if-nez v4, :cond_3

    invoke-static {v3}, Lcom/twitter/library/provider/au;->s(I)Z

    move-result v3

    if-eqz v3, :cond_5

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_0
        0x16 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method static synthetic a(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TimelineFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TimelineFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TimelineFragment;Lcom/twitter/internal/android/widget/DockLayout;Z)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/internal/android/widget/DockLayout;Z)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/vn;Lcom/twitter/android/ul;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    invoke-static/range {p0 .. p5}, Lcom/twitter/android/TimelineFragment;->b(Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/vn;Lcom/twitter/android/ul;Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method

.method private a(Lcom/twitter/internal/android/widget/DockLayout;Z)V
    .locals 1

    if-nez p2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/DockLayout;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/DockLayout;->setAutoUnlockEnabled(Z)V

    invoke-virtual {p1, p2}, Lcom/twitter/internal/android/widget/DockLayout;->setLocked(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private b(II)J
    .locals 6

    const/16 v5, 0x15

    const-wide/16 v0, 0x0

    packed-switch p2, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_0
    const/16 v3, 0x11

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-ne p1, v3, :cond_2

    const/16 v3, 0xf

    invoke-interface {v2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v3, v4, :cond_2

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :cond_1
    :goto_0
    :pswitch_2
    return-wide v0

    :cond_2
    invoke-interface {v2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v3

    if-nez v3, :cond_0

    goto :goto_0

    :pswitch_3
    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_1

    iget v3, p0, Lcom/twitter/android/TimelineFragment;->c:I

    invoke-interface {v2, v3}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v3

    if-eqz v3, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/TimelineFragment;->c:I

    invoke-interface {v2, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method static synthetic b(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method private b(J)V
    .locals 10

    const/4 v4, 0x3

    const/4 v3, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v9

    const-string/jumbo v2, "stream::results"

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, p1, p2, v1}, Lcom/twitter/android/TimelineFragment;->a(JLjava/lang/String;)V

    new-array v1, v4, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v8

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v9

    const-string/jumbo v0, "stream:linger:results"

    aput-object v0, v1, v3

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-object v0, p0

    move-wide v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/TimelineFragment;->a(JLjava/lang/String;J)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->l:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Ljava/util/ArrayList;)V

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->m:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    if-nez v2, :cond_4

    const-string/jumbo v2, ""

    :goto_1
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "trends:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::trend:results"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "trends:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v5, "::event:results"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    :cond_2
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v9, [Ljava/lang/String;

    aput-object v0, v5, v8

    invoke-virtual {v2, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_3
    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    goto :goto_1

    :cond_5
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/scribe/ScribeItem;

    new-instance v6, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v6, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v7, v9, [Ljava/lang/String;

    aput-object v0, v7, v8

    invoke-virtual {v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->m:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v9, [Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string/jumbo v6, "::rec_tweet::impression"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v8

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->s:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    :cond_9
    return-void
.end method

.method private static b(Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/vn;Lcom/twitter/android/ul;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 1

    new-instance v0, Lcom/twitter/android/uy;

    invoke-direct {v0, p0, p4, p1, p5}, Lcom/twitter/android/uy;-><init>(Lcom/twitter/android/client/c;Lcom/twitter/android/ul;Lcom/twitter/library/client/aa;Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {p3, p2, v0}, Lcom/twitter/android/vn;->a(Landroid/support/v4/app/FragmentActivity;Landroid/content/DialogInterface$OnClickListener;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/TimelineFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->M()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/TimelineFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->O()V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/TimelineFragment;)Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->o:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method private g(I)I
    .locals 1

    packed-switch p1, :pswitch_data_0

    const/16 v0, 0x28

    :goto_0
    return v0

    :pswitch_0
    const/16 v0, 0x64

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic g(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/TimelineFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/TimelineFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->n:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/TimelineFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TimelineFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->m:Ljava/util/HashMap;

    return-object v0
.end method

.method private l(I)I
    .locals 3

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid fetch type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private m(I)J
    .locals 2

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/TimelineFragment;->a(II)J

    move-result-wide v0

    return-wide v0
.end method

.method private n(I)Z
    .locals 1

    const/4 v0, 0x2

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->v()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private o(I)J
    .locals 2

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-direct {p0, v0, p1}, Lcom/twitter/android/TimelineFragment;->b(II)J

    move-result-wide v0

    return-wide v0
.end method


# virtual methods
.method public B()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->B()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected C()Ljava/lang/String;
    .locals 3

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "::tweet:link:open_link"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    if-nez v0, :cond_0

    const-string/jumbo v0, ""

    :goto_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":tweet:link:open_link"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "profile::tweet:link:open_link"

    goto :goto_0

    :pswitch_4
    const-string/jumbo v0, "favorites::tweet:link:open_link"

    goto :goto_0

    :pswitch_5
    const-string/jumbo v0, "connect:mentions:tweet:link:open_link"

    goto :goto_0

    :pswitch_6
    const-string/jumbo v0, "connect:mentions_filtered:tweet:link:open_link"

    goto :goto_0

    :pswitch_7
    const-string/jumbo v0, "connect:mentions_following:tweet:link:open_link"

    goto :goto_0

    :pswitch_8
    const-string/jumbo v0, "connect:mentions_verified:tweet:link:open_link"

    goto :goto_0

    :pswitch_9
    const-string/jumbo v0, "favorite_people::tweet:link:open_link"

    goto :goto_0

    :pswitch_a
    const-string/jumbo v0, "media_only_home::tweet:link:open_link"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method public D()Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method protected F()Z
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1, v0}, Landroid/support/v4/widget/CursorAdapter;->getItemViewType(I)I

    move-result v1

    invoke-static {v1}, Lcom/twitter/android/uh;->a(I)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    return v0
.end method

.method public S()V
    .locals 0

    return-void
.end method

.method protected a(JJ)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;)V

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->b(J)V

    return-void
.end method

.method public a(JLcom/twitter/library/api/PromotedContent;ILcom/twitter/android/zd;)V
    .locals 6

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->ab()Z

    move-result v0

    if-eqz v0, :cond_0

    const-class v0, Lcom/twitter/android/RootProfileActivity;

    :goto_0
    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v0, "user_id"

    invoke-virtual {v1, v0, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    iget-object v2, p5, Lcom/twitter/android/zd;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "::user_module:user:profile_click"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p5, Lcom/twitter/android/zd;->c:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-class v0, Lcom/twitter/android/ProfileActivity;

    goto :goto_0
.end method

.method public a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/TweetListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v1, 0x191

    if-ne v0, v1, :cond_1

    const v0, 0x7f0f04f9    # com.twitter.android.R.string.tweets_unauthorized_error

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0f04e3    # com.twitter.android.R.string.tweets_fetch_error

    invoke-static {p1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    const/16 v1, 0x190

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->b(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->r()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    iput-boolean v5, p0, Lcom/twitter/android/TimelineFragment;->f:Z

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v4}, Lcom/twitter/android/TimelineFragment;->i(Z)V

    new-instance v0, Lcom/twitter/refresh/widget/a;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->h:J

    iget v3, p0, Lcom/twitter/android/TimelineFragment;->g:I

    invoke-direct {v0, v4, v1, v2, v3}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/TweetListFragment;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    if-nez v0, :cond_0

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->L()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->s:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v3, p2, v0, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->s:Ljava/util/Map;

    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public bridge synthetic a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 0

    check-cast p2, Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/TimelineFragment;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 18

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/TweetListFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v2

    move-object v9, v2

    check-cast v9, Landroid/database/Cursor;

    if-eqz v9, :cond_0

    const/16 v2, 0x12

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    invoke-virtual/range {p1 .. p1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int v2, p3, v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/TimelineFragment;->c:I

    const/4 v2, 0x7

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v2, Lcom/twitter/android/ye;

    const/4 v3, 0x1

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    invoke-virtual {v2, v3, v4}, Lcom/twitter/android/ye;->c(J)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v10

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v11

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v2, :sswitch_data_0

    const/4 v2, 0x0

    :goto_1
    new-instance v4, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v4, v9}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget v3, v4, Lcom/twitter/library/provider/Tweet;->Z:I

    invoke-static {v3}, Lcom/twitter/library/provider/au;->g(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {}, Lkm;->c()Z

    move-result v3

    if-eqz v3, :cond_0

    :cond_2
    new-instance v5, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->ab()Z

    move-result v3

    if-eqz v3, :cond_16

    const-class v3, Lcom/twitter/android/RootTweetActivity;

    :goto_2
    invoke-direct {v5, v6, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "tw"

    invoke-virtual {v5, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v5, "association"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v6, 0x1b

    if-ne v5, v6, :cond_3

    const-string/jumbo v5, "timeline_id"

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-virtual {v3, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    if-nez v2, :cond_0

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->ad()Ljava/lang/String;

    move-result-object v7

    const-string/jumbo v8, "tweet"

    const-string/jumbo v9, "click"

    invoke-static {v6, v7, v8, v9}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :sswitch_0
    sget v2, Lcom/twitter/library/provider/cc;->g:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget v3, Lcom/twitter/library/provider/cc;->e:I

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    packed-switch v3, :pswitch_data_0

    const/4 v2, 0x0

    goto/16 :goto_1

    :pswitch_0
    invoke-static {v2}, Lcom/twitter/library/provider/au;->f(I)Z

    move-result v3

    if-eqz v3, :cond_4

    sget v2, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v3, v2, Lcom/twitter/library/api/TimelineScribeContent;->id:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string/jumbo v3, "discover_module"

    const-string/jumbo v3, "discover_module"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "::discover_module:more:click"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v2, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/DiscoverActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    invoke-static {v2}, Lcom/twitter/library/provider/au;->j(I)Z

    move-result v3

    if-eqz v3, :cond_6

    sget v2, Lcom/twitter/library/provider/cc;->f:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    move-object v8, v2

    check-cast v8, Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget v2, v8, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    invoke-static {v2}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v3

    sget v2, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TimelineScribeContent;

    const-wide/16 v4, -0x1

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v5, 0x10

    if-ne v2, v5, :cond_5

    iget v2, v8, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    const/4 v5, 0x2

    if-ne v2, v5, :cond_5

    const-string/jumbo v2, "more"

    iget-object v5, v8, Lcom/twitter/library/api/TwitterTopic$Metadata;->id:Ljava/lang/String;

    iput-object v5, v4, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    :goto_3
    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v6, 0x5

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v9

    aput-object v9, v6, v7

    const/4 v7, 0x1

    const/4 v9, 0x0

    aput-object v9, v6, v7

    const/4 v7, 0x2

    aput-object v3, v6, v7

    const/4 v3, 0x3

    aput-object v2, v6, v3

    const/4 v2, 0x4

    const-string/jumbo v3, "click"

    aput-object v3, v6, v2

    invoke-virtual {v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v2, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/EventsActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "type"

    const/16 v4, 0x10

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "timeline_tag"

    iget v4, v8, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    invoke-static {v4}, Lcom/twitter/library/api/TwitterTopic;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "empty_title"

    const v4, 0x7f0f0153    # com.twitter.android.R.string.empty_timeline

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "empty_desc"

    const v4, 0x7f0f0154    # com.twitter.android.R.string.empty_timeline_desc

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    const-string/jumbo v2, "more"

    iget-object v5, v8, Lcom/twitter/library/api/TwitterTopic$Metadata;->id:Ljava/lang/String;

    iput-object v5, v4, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    goto :goto_3

    :cond_6
    invoke-static {v2}, Lcom/twitter/library/provider/au;->k(I)Z

    move-result v2

    if-eqz v2, :cond_8

    sget v2, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TimelineScribeContent;

    const-string/jumbo v3, "user_module"

    const-string/jumbo v3, "user_module"

    const-wide/16 v4, -0x1

    const/4 v6, 0x3

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const/4 v6, 0x0

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "user_module"

    aput-object v6, v4, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "more"

    aput-object v6, v4, v5

    const/4 v5, 0x4

    const-string/jumbo v6, "click"

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v3, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->ab()Z

    move-result v2

    if-eqz v2, :cond_7

    const-class v2, Lcom/twitter/android/RootTabbedFindPeopleActivity;

    :goto_4
    invoke-direct {v3, v4, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_7
    const-class v2, Lcom/twitter/android/TabbedFindPeopleActivity;

    goto :goto_4

    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ur;

    iget-object v2, v2, Lcom/twitter/android/ur;->a:Lcom/twitter/android/uj;

    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string/jumbo v4, "twitter"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "tweet"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v4, "status_id"

    iget-wide v5, v2, Lcom/twitter/android/uj;->b:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->ab()Z

    move-result v2

    if-eqz v2, :cond_a

    const-class v2, Lcom/twitter/android/RootTweetActivity;

    :goto_5
    invoke-direct {v4, v5, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "association"

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v4, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v4, 0x1b

    if-ne v3, v4, :cond_9

    const-string/jumbo v3, "timeline_id"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_9
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    const-class v2, Lcom/twitter/android/TweetActivity;

    goto :goto_5

    :pswitch_1
    sget v3, Lcom/twitter/library/provider/cc;->F:I

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    sget v3, Lcom/twitter/library/provider/cc;->E:I

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v3, Lcom/twitter/library/provider/cc;->C:I

    invoke-interface {v9, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    sget v4, Lcom/twitter/library/provider/cc;->D:I

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    sget v7, Lcom/twitter/library/provider/cc;->N:I

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    invoke-static {v4}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v13

    new-instance v14, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v14}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iput-object v13, v14, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    const/16 v7, 0x10

    iput v7, v14, Lcom/twitter/library/scribe/ScribeItem;->c:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v15, 0x10

    if-ne v7, v15, :cond_e

    invoke-static {v2}, Lcom/twitter/library/provider/au;->h(I)Z

    move-result v2

    if-eqz v2, :cond_e

    sget v2, Lcom/twitter/library/provider/cc;->L:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/PromotedContent;

    if-eqz v2, :cond_c

    const/4 v7, 0x1

    :goto_6
    if-eqz v7, :cond_b

    sget-object v15, Lcom/twitter/library/api/PromotedEvent;->h:Lcom/twitter/library/api/PromotedEvent;

    iget-wide v0, v2, Lcom/twitter/library/api/PromotedContent;->promotedTrendId:J

    move-wide/from16 v16, v0

    move-wide/from16 v0, v16

    invoke-virtual {v10, v15, v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;J)V

    iget-wide v15, v2, Lcom/twitter/library/api/PromotedContent;->promotedTrendId:J

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v14, Lcom/twitter/library/scribe/ScribeItem;->e:Ljava/lang/String;

    :cond_b
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v15, 0x3

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    const-string/jumbo v17, "trends"

    aput-object v17, v15, v16

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x2

    const-string/jumbo v17, "trend::search"

    aput-object v17, v15, v16

    invoke-virtual {v2, v15}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v6}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    sget v2, Lcom/twitter/library/provider/cc;->K:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    iput v2, v14, Lcom/twitter/library/scribe/ScribeItem;->y:I

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ur;

    iget v2, v2, Lcom/twitter/android/ur;->k:I

    iput v2, v14, Lcom/twitter/library/scribe/ScribeItem;->h:I

    const/4 v2, 0x2

    if-ne v4, v2, :cond_d

    iput-object v3, v14, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v2, 0x0

    :goto_7
    move v9, v7

    :goto_8
    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v7, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v15, 0x5

    new-array v15, v15, [Ljava/lang/String;

    const/16 v16, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v17

    aput-object v17, v15, v16

    const/16 v16, 0x1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    move-object/from16 v17, v0

    aput-object v17, v15, v16

    const/16 v16, 0x2

    aput-object v13, v15, v16

    const/4 v13, 0x3

    aput-object v2, v15, v13

    const/4 v2, 0x4

    const-string/jumbo v13, "click"

    aput-object v13, v15, v2

    invoke-static {v15}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-virtual {v7, v11}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v14}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual/range {p2 .. p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ur;

    if-eqz v2, :cond_12

    iget-object v7, v2, Lcom/twitter/android/ur;->e:Lcom/twitter/android/widget/EventView;

    if-eqz v7, :cond_10

    iget-object v2, v2, Lcom/twitter/android/ur;->e:Lcom/twitter/android/widget/EventView;

    invoke-virtual {v2}, Lcom/twitter/android/widget/EventView;->getTopicData()Lcom/twitter/android/widget/TopicView$TopicData;

    move-result-object v10

    :goto_9
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-string/jumbo v7, "trend"

    invoke-static/range {v2 .. v10}, Lcom/twitter/android/vf;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/TimelineFragment;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_c
    const/4 v7, 0x0

    goto/16 :goto_6

    :cond_d
    iput-object v5, v14, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const-string/jumbo v2, "more"

    goto :goto_7

    :cond_e
    const/4 v2, 0x0

    iput-object v3, v14, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v15, 0x10

    if-ne v7, v15, :cond_f

    const/4 v7, 0x2

    if-ne v4, v7, :cond_f

    sget v7, Lcom/twitter/library/provider/cc;->K:I

    invoke-interface {v9, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    iput v7, v14, Lcom/twitter/library/scribe/ScribeItem;->y:I

    const/4 v7, 0x0

    move v9, v2

    move-object v2, v7

    goto/16 :goto_8

    :cond_f
    const-string/jumbo v7, "event"

    move v9, v2

    move-object v2, v7

    goto/16 :goto_8

    :cond_10
    iget-object v7, v2, Lcom/twitter/android/ur;->d:Lcom/twitter/android/us;

    if-eqz v7, :cond_11

    iget-object v2, v2, Lcom/twitter/android/ur;->d:Lcom/twitter/android/us;

    iget-object v10, v2, Lcom/twitter/android/us;->f:Lcom/twitter/android/widget/TopicView$TopicData;

    goto :goto_9

    :cond_11
    const/4 v10, 0x0

    goto :goto_9

    :cond_12
    const/4 v10, 0x0

    goto :goto_9

    :pswitch_2
    invoke-static {v2}, Lcom/twitter/library/provider/au;->g(I)Z

    move-result v2

    if-eqz v2, :cond_13

    invoke-static {}, Lkm;->c()Z

    move-result v2

    if-eqz v2, :cond_13

    const/4 v2, 0x1

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    sget v2, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TimelineScribeContent;

    const-string/jumbo v3, "discover_module"

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v7}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "::discover_module:tweet:click"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_13
    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v3, 0x10

    if-ne v2, v3, :cond_15

    sget v2, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {v9, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TimelineScribeContent;

    iget v3, v2, Lcom/twitter/library/api/TimelineScribeContent;->eventType:I

    invoke-static {v3}, Lcom/twitter/library/api/TwitterTopic;->d(I)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v4}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iget v5, v2, Lcom/twitter/library/api/TimelineScribeContent;->tweetProofCursor:I

    iput v5, v4, Lcom/twitter/library/scribe/ScribeItem;->h:I

    iput-object v3, v4, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    const/4 v5, 0x1

    invoke-interface {v9, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v5

    iput-wide v5, v4, Lcom/twitter/library/scribe/ScribeItem;->a:J

    iget-object v5, v2, Lcom/twitter/library/api/TimelineScribeContent;->query:Ljava/lang/String;

    iput-object v5, v4, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    const/16 v5, 0x8

    iput v5, v4, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-interface {v9}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    iput v5, v4, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-wide v5, v2, Lcom/twitter/library/api/TimelineScribeContent;->tweetCount:J

    long-to-int v5, v5

    iput v5, v4, Lcom/twitter/library/scribe/ScribeItem;->y:I

    iget v2, v2, Lcom/twitter/library/api/TimelineScribeContent;->eventType:I

    const/4 v5, 0x2

    if-ne v2, v5, :cond_14

    move-object v2, v3

    :goto_a
    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v3, v11, v12}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    aput-object v2, v5, v6

    const/4 v2, 0x3

    const-string/jumbo v6, "tweet"

    aput-object v6, v5, v2

    const/4 v2, 0x4

    const-string/jumbo v6, "click"

    aput-object v6, v5, v2

    invoke-virtual {v3, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    const/4 v2, 0x1

    goto/16 :goto_1

    :cond_14
    const-string/jumbo v2, "tweet"

    goto :goto_a

    :cond_15
    const/4 v2, 0x0

    goto/16 :goto_1

    :cond_16
    const-class v3, Lcom/twitter/android/TweetActivity;

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_0
        0x16 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_0
        0x1c -> :sswitch_0
        0x1d -> :sswitch_0
        0x1e -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 2

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TimelineFragment;->e(J)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->a(Z)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/ye;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/refresh/widget/a;)V
    .locals 5

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v0, :sswitch_data_0

    :goto_0
    return-void

    :sswitch_0
    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "timeline"

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tweet_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iget-wide v3, p1, Lcom/twitter/refresh/widget/a;->b:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "off_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;I)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method protected a(Lcom/twitter/refresh/widget/a;Z)V
    .locals 4

    iget-wide v0, p1, Lcom/twitter/refresh/widget/a;->b:J

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TimelineFragment;->a(J)I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    if-nez p2, :cond_1

    :cond_0
    iget v2, p1, Lcom/twitter/refresh/widget/a;->c:I

    invoke-virtual {v1, v0, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_1
    return-void
.end method

.method protected a(Ljava/util/HashMap;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Ljava/util/HashMap;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/TimelineFragment;->b(Ljava/util/HashMap;)V

    return-void
.end method

.method protected a(Z)V
    .locals 6

    const-wide/16 v4, 0x0

    const/4 v3, 0x5

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a(Z)V

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Invalid status type: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_1
    sget-object v0, Lcom/twitter/library/provider/av;->b:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    if-eqz v0, :cond_5

    if-eqz p1, :cond_3

    invoke-virtual {p0, v3}, Lcom/twitter/android/TimelineFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->P()Z

    :cond_1
    :goto_1
    return-void

    :pswitch_2
    sget-object v0, Lcom/twitter/library/provider/av;->e:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/twitter/library/provider/av;->f:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/twitter/library/provider/as;->b:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/twitter/library/provider/at;->i:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/twitter/library/provider/at;->g:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_7
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "include_rts"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    sget-object v0, Lcom/twitter/library/provider/at;->k:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/twitter/library/provider/at;->f:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_8
    iget-wide v0, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/TimelineFragment;->i:J

    cmp-long v0, v0, v4

    if-lez v0, :cond_0

    sget-object v0, Lcom/twitter/library/provider/at;->e:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/twitter/library/provider/at;->o:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_a
    sget-object v0, Lcom/twitter/library/provider/at;->p:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_b
    sget-object v0, Lcom/twitter/library/provider/av;->d:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_c
    sget-object v0, Lcom/twitter/library/provider/av;->g:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_d
    sget-object v0, Lcom/twitter/library/provider/av;->h:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_e
    sget-object v0, Lcom/twitter/library/provider/av;->i:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :pswitch_f
    sget-object v0, Lcom/twitter/library/provider/av;->j:Landroid/net/Uri;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_4

    invoke-virtual {p0, v3}, Lcom/twitter/android/TimelineFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->p()V

    goto/16 :goto_1

    :cond_4
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TimelineFragment;->f:Z

    goto/16 :goto_1

    :cond_5
    invoke-virtual {p0, v3}, Lcom/twitter/android/TimelineFragment;->b(I)V

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_3
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method protected a_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->a_()V

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/ye;->a()V

    invoke-virtual {v0}, Lcom/twitter/android/ye;->d()V

    :cond_0
    return-void
.end method

.method protected a_(I)V
    .locals 4

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    if-nez v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->a_(I)V

    return-void

    :pswitch_1
    const-string/jumbo v0, "home:refresh"

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->at()Lcom/twitter/library/metrics/h;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;J)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->i()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "home:first_tweet_cache"

    invoke-static {v0, v1}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public b(JLcom/twitter/library/api/PromotedContent;ILcom/twitter/android/zd;)I
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TimelineFragment;->o:Lcom/twitter/library/util/FriendshipCache;

    iget v1, p5, Lcom/twitter/android/zd;->b:I

    invoke-virtual {v3, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v3, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-static {v1, v6}, Lcom/twitter/library/provider/ay;->b(II)I

    move-result v1

    invoke-virtual {v2, p1, p2, p3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v3, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "::user_module:user:unfollow"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v6, [Ljava/lang/String;

    aput-object v0, v4, v7

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v3, p5, Lcom/twitter/android/zd;->c:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return v1

    :cond_0
    invoke-static {v1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    goto :goto_0

    :cond_1
    invoke-static {v1, v6}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    invoke-virtual {v2, p1, p2, v7, p3}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v3, p1, p2}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, "::user_module:user:follow"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method protected b()V
    .locals 4

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->b()V

    iget-wide v0, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->am()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->a(Z)V

    :cond_1
    return-void
.end method

.method protected b(I)V
    .locals 4

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    if-nez v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    packed-switch p1, :pswitch_data_1

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->b(I)V

    :goto_1
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "home:refresh"

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->at()Lcom/twitter/library/metrics/h;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;J)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->j()V

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "home:refresh"

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->at()Lcom/twitter/library/metrics/h;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;J)Lcom/twitter/library/metrics/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->k()V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->n(J)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->U()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    const-string/jumbo v3, "home:first_tweet_cache"

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JLjava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "home:first_tweet_cache"

    invoke-static {v0, v1}, Lcom/twitter/android/util/AppMetrics;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->g()V

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x7
        :pswitch_3
    .end packed-switch
.end method

.method public b(Z)V
    .locals 0

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->an()V

    :cond_0
    return-void
.end method

.method protected b_(I)Landroid/app/Dialog;
    .locals 3

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->b_(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/ux;

    invoke-direct {v1, p0}, Lcom/twitter/android/ux;-><init>(Lcom/twitter/android/TimelineFragment;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v0, 0x7f0f038e    # com.twitter.android.R.string.scan_contacts_confirm_title

    invoke-virtual {v2, v0}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f038c    # com.twitter.android.R.string.scan_contacts_confirm_message

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x65
        :pswitch_0
    .end packed-switch
.end method

.method protected c()V
    .locals 9

    const/4 v1, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v5

    const-string/jumbo v0, "ref_event"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "ref_event"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v6, "ref_event"

    invoke-virtual {v5, v6}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :goto_0
    iget v5, p0, Lcom/twitter/android/TimelineFragment;->F:I

    packed-switch v5, :pswitch_data_0

    :goto_1
    :pswitch_0
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0

    :pswitch_1
    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v7

    iget-object v4, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    aput-object v4, v3, v8

    const/4 v4, 0x2

    aput-object v1, v3, v4

    const/4 v4, 0x3

    aput-object v1, v3, v4

    const/4 v1, 0x4

    const-string/jumbo v4, "impression"

    aput-object v4, v3, v1

    invoke-virtual {v5, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    new-array v3, v8, [Ljava/lang/String;

    aput-object v0, v3, v7

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_1

    :pswitch_2
    const-string/jumbo v1, "profile_tweets::::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_3
    const-string/jumbo v1, "connect:mentions:::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_4
    const-string/jumbo v1, "connect:mentions_filtered:::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_5
    const-string/jumbo v1, "connect:mentions_following:::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_6
    const-string/jumbo v1, "connect:mentions_verified:::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_7
    const-string/jumbo v1, "favorites::::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_8
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v0, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v8, [Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "::::impression"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v7

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    const/4 v3, -0x1

    invoke-static {v1, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_1

    :pswitch_9
    const-string/jumbo v1, "favorite_people::::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    :pswitch_a
    const-string/jumbo v1, "media_only_home::::impression"

    invoke-virtual {v2, v3, v4, v1, v0}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_a
        :pswitch_8
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected c(I)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TimelineFragment;->c_(I)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/TimelineFragment;->d(I)Lcom/twitter/library/service/b;

    move-result-object v1

    if-eqz v1, :cond_0

    iget v2, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-virtual {p0, v1, v2, p1}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method protected c_(I)Z
    .locals 1

    const/4 v0, 0x2

    if-ne v0, p1, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->c_(I)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d(I)Lcom/twitter/library/service/b;
    .locals 12

    const/4 v0, 0x0

    const/4 v9, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v10

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x7

    invoke-static {v2, v0, p1, v3}, Lcom/twitter/android/TimelineFragment;->a(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v11

    iget v2, p0, Lcom/twitter/android/TimelineFragment;->F:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-object v0

    :pswitch_1
    new-instance v0, Ljl;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->m(I)J

    move-result-wide v4

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->o(I)J

    move-result-wide v6

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->g(I)I

    move-result v8

    invoke-direct/range {v0 .. v8}, Ljl;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;JJI)V

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->n(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Ljl;->a(Z)Ljl;

    const-string/jumbo v1, "scribe_event"

    invoke-virtual {v0, v1, v11}, Ljl;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/service/l;

    invoke-direct {v2, v9}, Lcom/twitter/library/service/l;-><init>(I)V

    invoke-virtual {v1, v2}, Lcom/twitter/library/service/b;->a(Lcom/twitter/internal/android/service/l;)Lcom/twitter/internal/android/service/a;

    invoke-virtual {v10, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v10, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v2, 0x16

    if-ne v1, v2, :cond_0

    sget-object v1, Lcom/twitter/library/service/TimelineHelper$FilterType;->b:Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-virtual {v0, v1}, Ljl;->a(Lcom/twitter/library/service/TimelineHelper$FilterType;)Ljl;

    goto :goto_0

    :cond_0
    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v2, 0x1a

    if-ne v1, v2, :cond_1

    sget-object v1, Lcom/twitter/library/service/TimelineHelper$FilterType;->c:Lcom/twitter/library/service/TimelineHelper$FilterType;

    invoke-virtual {v0, v1}, Ljl;->a(Lcom/twitter/library/service/TimelineHelper$FilterType;)Ljl;

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v9}, Ljl;->b(Z)Ljl;

    goto :goto_0

    :pswitch_2
    new-instance v0, Ljj;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    iget-object v5, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v5}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v5

    div-int/lit8 v5, v5, 0x14

    add-int/lit8 v5, v5, 0x1

    invoke-direct/range {v0 .. v5}, Ljj;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V

    goto :goto_0

    :pswitch_3
    new-instance v0, Ljs;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-direct {v0, v1, v2, v3, v4}, Ljs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->m(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljs;->b(J)Ljs;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->o(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljs;->c(J)Ljs;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->g(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljs;->a(I)Ljs;

    move-result-object v1

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-virtual {v1, v0}, Ljs;->c(I)Lcom/twitter/library/service/b;

    iget-wide v2, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-nez v0, :cond_2

    invoke-static {}, Lgw;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v9

    :goto_1
    invoke-virtual {v1, v0}, Ljs;->a(Z)Ljs;

    const-string/jumbo v0, "scribe_event"

    invoke-virtual {v1, v0, v11}, Ljs;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/service/b;

    move-object v0, v1

    goto/16 :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_4
    new-instance v0, Lje;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->l(I)I

    move-result v4

    invoke-direct {v0, v1, v2, v3, v4}, Lje;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    goto/16 :goto_0

    :pswitch_5
    new-instance v0, Ljo;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v3, p0, Lcom/twitter/android/TimelineFragment;->Q:J

    iget-wide v5, p0, Lcom/twitter/android/TimelineFragment;->i:J

    invoke-direct/range {v0 .. v6}, Ljo;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJ)V

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->m(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljo;->a(J)Ljo;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->o(I)J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljo;->b(J)Ljo;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/TimelineFragment;->g(I)I

    move-result v1

    int-to-long v1, v1

    invoke-virtual {v0, v1, v2}, Ljo;->c(J)Ljo;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_6
    new-instance v0, Ljt;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Ljt;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    invoke-virtual {v10, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    invoke-virtual {v10, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    goto/16 :goto_0

    :pswitch_7
    new-instance v0, Ljn;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljn;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v10, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    goto/16 :goto_0

    :pswitch_8
    new-instance v0, Lji;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lji;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_0
        :pswitch_8
    .end packed-switch
.end method

.method protected d()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->d()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/TimelineFragment;->b(J)V

    return-void
.end method

.method public f(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    sget v1, Lcom/twitter/library/provider/cc;->b:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public f()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->f()V

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/twitter/android/uu;->a(Lcom/twitter/android/client/c;Landroid/database/Cursor;Lcom/twitter/library/client/aa;)V

    :cond_0
    return-void
.end method

.method protected i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->O()V

    return-void
.end method

.method protected j()I
    .locals 1

    const/16 v0, 0x15

    return v0
.end method

.method protected k()V
    .locals 6

    const/4 v5, 0x0

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v0, :sswitch_data_0

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->k()V

    :goto_0
    return-void

    :sswitch_0
    new-instance v1, Lcom/twitter/library/client/f;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "timeline"

    invoke-direct {v1, v2, v3, v4}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "tweet_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-wide/16 v3, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/twitter/android/TimelineFragment;->h:J

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "off_"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v5}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TimelineFragment;->g:I

    new-instance v0, Lcom/twitter/refresh/widget/a;

    iget-wide v1, p0, Lcom/twitter/android/TimelineFragment;->h:J

    iget v3, p0, Lcom/twitter/android/TimelineFragment;->g:I

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x5 -> :sswitch_0
        0x16 -> :sswitch_0
        0x17 -> :sswitch_0
        0x18 -> :sswitch_0
        0x19 -> :sswitch_0
        0x1a -> :sswitch_0
    .end sparse-switch
.end method

.method protected l()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->q:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->q:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v0, :sswitch_data_0

    const-string/jumbo v0, "unknown"

    goto :goto_0

    :sswitch_0
    const-string/jumbo v0, "home"

    goto :goto_0

    :sswitch_1
    const-string/jumbo v0, "trends"

    goto :goto_0

    :sswitch_2
    const-string/jumbo v0, "custom"

    goto :goto_0

    :sswitch_3
    const-string/jumbo v0, "exploreux"

    goto :goto_0

    :sswitch_4
    const-string/jumbo v0, "explore"

    goto :goto_0

    :sswitch_5
    const-string/jumbo v0, "nearby"

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_1
        0x1b -> :sswitch_2
        0x1c -> :sswitch_4
        0x1d -> :sswitch_5
        0x1e -> :sswitch_3
    .end sparse-switch
.end method

.method protected m()Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v1, 0x1a

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v1, 0x16

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-static {v0}, Lcom/twitter/library/provider/ar;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected n()V
    .locals 1

    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->c(I)Z

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 25

    invoke-super/range {p0 .. p1}, Lcom/twitter/android/TweetListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v10

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/android/TimelineFragment;->F:I

    if-nez v1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lkm;->a(Landroid/content/Context;J)V

    const-string/jumbo v1, "android_custom_timelines_follow_2030"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v1, :cond_b

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->y()Lcom/twitter/android/yb;

    move-result-object v11

    new-instance v1, Lcom/twitter/library/util/FriendshipCache;

    invoke-direct {v1}, Lcom/twitter/library/util/FriendshipCache;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/TimelineFragment;->o:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v10}, Lcom/twitter/android/client/c;->aa()Z

    move-result v1

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v3, :sswitch_data_0

    if-eqz v1, :cond_7

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/4 v3, 0x1

    if-eq v1, v3, :cond_1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v3, 0x9

    if-eq v1, v3, :cond_1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/4 v3, 0x2

    if-ne v1, v3, :cond_7

    :cond_1
    const/16 v16, 0x1

    :goto_0
    new-instance v12, Lcom/twitter/android/ye;

    const/4 v14, 0x2

    const/4 v15, 0x0

    const/16 v17, 0x0

    const/16 v18, 0x0

    iget-object v0, v10, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/TimelineFragment;->C:Lcom/twitter/android/vs;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/TimelineFragment;->o:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v23, v0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->z()Z

    move-result v24

    move-object v13, v2

    move-object/from16 v20, v10

    move-object/from16 v21, v11

    invoke-direct/range {v12 .. v24}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;Z)V

    move-object/from16 v0, p0

    iget-wide v1, v0, Lcom/twitter/android/TimelineFragment;->Q:J

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_8

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_2

    invoke-static {}, Lgw;->b()I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/twitter/android/ye;->e(I)V

    :cond_2
    :goto_2
    move-object/from16 v0, p0

    invoke-virtual {v0, v12}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/android/client/av;)Lcom/twitter/android/client/BaseListFragment;

    move-object/from16 v0, p0

    invoke-virtual {v12, v0}, Lcom/twitter/android/ye;->b(Lcom/twitter/android/ob;)V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->K()Z

    move-result v1

    invoke-virtual {v12, v1}, Lcom/twitter/android/ye;->d(Z)V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->L()I

    move-result v1

    invoke-virtual {v12, v1}, Lcom/twitter/android/ye;->d(I)V

    if-eqz p1, :cond_a

    const-string/jumbo v1, "spinning_gap_ids"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getLongArray(Ljava/lang/String;)[J

    move-result-object v2

    if-eqz v2, :cond_a

    array-length v3, v2

    const/4 v1, 0x0

    :goto_3
    if-ge v1, v3, :cond_9

    aget-wide v4, v2, v1

    invoke-virtual {v12, v4, v5}, Lcom/twitter/android/ye;->c(J)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :sswitch_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TimelineFragment;->k:Lcom/twitter/library/util/aa;

    if-nez v3, :cond_3

    invoke-virtual {v10, v2}, Lcom/twitter/android/client/c;->f(Landroid/content/Context;)Lcom/twitter/library/util/aa;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v3, v0}, Lcom/twitter/library/util/aa;->a(Lcom/twitter/library/util/ac;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/TimelineFragment;->k:Lcom/twitter/library/util/aa;

    :cond_3
    if-eqz v1, :cond_4

    move-object/from16 v0, p0

    iget v1, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v3, 0x10

    if-eq v1, v3, :cond_4

    const/4 v6, 0x1

    :goto_4
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->J()Z

    move-result v7

    new-instance v1, Lcom/twitter/android/uh;

    const/4 v3, 0x2

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/4 v5, 0x0

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->I()I

    move-result v8

    iget-object v9, v10, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    new-instance v12, Lcom/twitter/android/vf;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/twitter/android/vf;-><init>(Landroid/support/v4/app/Fragment;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/TimelineFragment;->C:Lcom/twitter/android/vs;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/TimelineFragment;->o:Lcom/twitter/library/util/FriendshipCache;

    move-object/from16 v16, v0

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->l()Ljava/lang/String;

    move-result-object v17

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->z()Z

    move-result v18

    move-object/from16 v13, p0

    move-object/from16 v15, p0

    invoke-direct/range {v1 .. v18}, Lcom/twitter/android/uh;-><init>(Landroid/app/Activity;IIZZZILcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vf;Lcom/twitter/android/zb;Lcom/twitter/android/vs;Landroid/view/View$OnClickListener;Lcom/twitter/library/util/FriendshipCache;Ljava/lang/String;Z)V

    invoke-virtual {v1, v6}, Lcom/twitter/android/uh;->e(Z)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_6

    if-eqz v2, :cond_5

    const v3, 0x7f0900e1    # com.twitter.android.R.id.dock

    invoke-virtual {v2, v3}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/internal/android/widget/DockLayout;

    :goto_5
    new-instance v3, Lcom/twitter/android/vd;

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v10, v2}, Lcom/twitter/android/vd;-><init>(Lcom/twitter/android/TimelineFragment;Lcom/twitter/android/client/c;Lcom/twitter/internal/android/widget/DockLayout;)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/uh;->a(Lcom/twitter/android/ob;)V

    new-instance v2, Lcom/twitter/android/uw;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v1, v10}, Lcom/twitter/android/uw;-><init>(Lcom/twitter/android/TimelineFragment;Lcom/twitter/android/uh;Lcom/twitter/android/client/c;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/uh;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    move-object v12, v1

    goto/16 :goto_2

    :cond_4
    const/4 v6, 0x0

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    goto :goto_5

    :cond_6
    const/4 v2, 0x0

    goto :goto_5

    :cond_7
    const/16 v16, 0x0

    goto/16 :goto_0

    :cond_8
    const/4 v1, 0x0

    goto/16 :goto_1

    :cond_9
    invoke-virtual {v12}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_a
    move-object/from16 v0, p0

    iput-object v12, v0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_b
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->s()Lcom/twitter/android/va;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/TimelineFragment;->O:Lcom/twitter/library/client/j;

    new-instance v1, Lcom/twitter/android/vb;

    move-object/from16 v0, p0

    invoke-direct {v1, v0}, Lcom/twitter/android/vb;-><init>(Lcom/twitter/android/TimelineFragment;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/library/client/z;)V

    if-eqz p1, :cond_c

    const-string/jumbo v1, "friends_count"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/twitter/android/TimelineFragment;->d:I

    :goto_6
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void

    :cond_c
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TimelineFragment;->O()V

    goto :goto_6

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x10 -> :sswitch_0
        0x16 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_0
        0x1c -> :sswitch_0
        0x1d -> :sswitch_0
        0x1e -> :sswitch_0
    .end sparse-switch
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    if-ne v0, v1, :cond_1

    const/16 v0, 0x65

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->k(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f0901a3    # com.twitter.android.R.id.dismiss

    if-ne v0, v1, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/ul;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TimelineFragment;->B:Lcom/twitter/android/vn;

    iget-object v5, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/TimelineFragment;->b(Lcom/twitter/android/client/c;Lcom/twitter/library/client/aa;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/android/vn;Lcom/twitter/android/ul;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const-string/jumbo v1, "tag"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/twitter/android/TimelineFragment;->i:J

    const-string/jumbo v1, "timeline_tag"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    const-string/jumbo v1, "scribe_section"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TimelineFragment;->r:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    if-nez v1, :cond_0

    const-string/jumbo v1, "unspecified"

    iput-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    :cond_0
    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v2, 0x1b

    if-ne v1, v2, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    const/4 v2, -0x1

    invoke-static {v1, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TimelineFragment;->G:Lcom/twitter/library/scribe/ScribeItem;

    :cond_1
    const-string/jumbo v1, "is_collapsed"

    invoke-virtual {v0, v1, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/TimelineFragment;->j:Z

    const-string/jumbo v1, "experiments"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TimelineFragment;->p:Ljava/lang/String;

    const-string/jumbo v1, "scribe_page"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->q:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->T()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/TimelineFragment;->a(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 9

    const/4 v4, 0x0

    const/4 v2, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const-string/jumbo v0, "unspecified"

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v1, 0x1c

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_1

    :cond_0
    const-string/jumbo v4, "t_tag=?"

    new-array v5, v7, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    aput-object v0, v5, v6

    :goto_0
    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    sparse-switch v0, :sswitch_data_0

    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    const-string/jumbo v6, "updated_at DESC, _id ASC"

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    new-instance v0, Lcom/twitter/android/uu;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TimelineFragment;->b:Landroid/net/Uri;

    invoke-static {v2, v7, v8}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/uu;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->H()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v4, "t_tag=? AND t_flags=?"

    new-array v5, v2, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    aput-object v0, v5, v6

    const/16 v0, 0x40

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->G()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v4, "t_tag=? AND t_flags!=?"

    new-array v5, v2, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    aput-object v0, v5, v6

    const/16 v0, 0x80

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v7

    goto :goto_0

    :cond_3
    const-string/jumbo v4, "t_tag=?"

    new-array v5, v7, [Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->e:Ljava/lang/String;

    aput-object v0, v5, v6

    goto :goto_0

    :cond_4
    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-static {v0}, Lcom/twitter/library/provider/ar;->a(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const-string/jumbo v4, "type=?"

    new-array v5, v7, [Ljava/lang/String;

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    goto :goto_0

    :cond_5
    iget-wide v0, p0, Lcom/twitter/android/TimelineFragment;->i:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_6

    const-string/jumbo v4, "tag=?"

    new-array v5, v7, [Ljava/lang/String;

    iget-wide v0, p0, Lcom/twitter/android/TimelineFragment;->i:J

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v5, v6

    goto/16 :goto_0

    :cond_6
    move-object v5, v4

    goto/16 :goto_0

    :sswitch_0
    sget-object v3, Lcom/twitter/library/provider/cc;->a:[Ljava/lang/String;

    const-string/jumbo v6, "t_updated_at DESC, _id ASC"

    goto/16 :goto_1

    :sswitch_1
    sget-object v3, Lcom/twitter/library/provider/cc;->a:[Ljava/lang/String;

    const-string/jumbo v6, "t_sort_index DESC, t_updated_at DESC, _id ASC"

    goto/16 :goto_1

    :sswitch_2
    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    const-string/jumbo v6, "updated_at DESC, _id ASC"

    goto/16 :goto_1

    :sswitch_3
    sget-object v3, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    const-string/jumbo v6, "updated_at DESC, _id ASC"

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x9 -> :sswitch_3
        0x10 -> :sswitch_0
        0x16 -> :sswitch_0
        0x1a -> :sswitch_0
        0x1b -> :sswitch_1
        0x1c -> :sswitch_0
        0x1d -> :sswitch_0
        0x1e -> :sswitch_0
    .end sparse-switch
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->a:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/TimelineFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onDestroyView()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->k:Lcom/twitter/library/util/aa;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p0}, Lcom/twitter/library/util/aa;->b(Lcom/twitter/library/util/ac;)V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroyView()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TimelineFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onResume()V

    invoke-direct {p0}, Lcom/twitter/android/TimelineFragment;->N()V

    iget v0, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineFragment;->a(Z)V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 6

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ye;

    invoke-virtual {v0}, Lcom/twitter/android/ye;->f()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [J

    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    aput-wide v4, v2, v1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    const-string/jumbo v0, "spinning_gap_ids"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putLongArray(Ljava/lang/String;[J)V

    :cond_1
    const-string/jumbo v0, "friends_count"

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->d:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/TimelineFragment;->I:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/android/uz;

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/uz;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;)V

    iput-object v0, p0, Lcom/twitter/android/TimelineFragment;->B:Lcom/twitter/android/vn;

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    return-void
.end method

.method protected q()Z
    .locals 3

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v2, 0x16

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/twitter/android/TimelineFragment;->F:I

    const/16 v2, 0x1a

    if-ne v1, v2, :cond_2

    :cond_0
    iget-boolean v1, p0, Lcom/twitter/android/TimelineFragment;->f:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v1

    const/16 v2, 0x14

    if-ge v1, v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected r()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method protected r_()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->r_()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->F()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected s()Lcom/twitter/android/va;
    .locals 1

    new-instance v0, Lcom/twitter/android/va;

    invoke-direct {v0, p0}, Lcom/twitter/android/va;-><init>(Lcom/twitter/android/TimelineFragment;)V

    return-object v0
.end method

.method protected u()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->W()Lcom/twitter/android/PromptView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u_()V
    .locals 0

    return-void
.end method

.method protected v()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->X()Z

    move-result v0

    return v0
.end method

.method protected y()Lcom/twitter/android/yb;
    .locals 4

    new-instance v0, Lcom/twitter/android/vc;

    iget-object v1, p0, Lcom/twitter/android/TimelineFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineFragment;->C()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TimelineFragment;->B:Lcom/twitter/android/vn;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/vc;-><init>(Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Lcom/twitter/android/vn;)V

    return-object v0
.end method

.method protected z()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
