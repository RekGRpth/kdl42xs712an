.class Lcom/twitter/android/widget/bq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Landroid/view/ViewTreeObserver;

.field final synthetic b:Landroid/widget/ListView;

.field final synthetic c:Lcom/twitter/android/widget/PoiFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/PoiFragment;Landroid/view/ViewTreeObserver;Landroid/widget/ListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    iput-object p2, p0, Lcom/twitter/android/widget/bq;->a:Landroid/view/ViewTreeObserver;

    iput-object p3, p0, Lcom/twitter/android/widget/bq;->b:Landroid/widget/ListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 14

    const/4 v11, 0x0

    const/16 v2, 0xc8

    const/4 v10, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/twitter/android/widget/bq;->a:Landroid/view/ViewTreeObserver;

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/bq;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/bq;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int v13, v0, v1

    move v9, v10

    move v3, v10

    move v5, v8

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/bq;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v9, v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/widget/bq;->b:Landroid/widget/ListView;

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    add-int v0, v13, v9

    if-ltz v0, :cond_0

    iget-object v6, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v6}, Lcom/twitter/android/widget/PoiFragment;->i(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bu;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/android/widget/bu;->getCount()I

    move-result v6

    if-lt v0, v6, :cond_2

    :cond_0
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    iget-object v6, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v6}, Lcom/twitter/android/widget/PoiFragment;->j(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/HashMap;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v6

    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    if-eq v7, v6, :cond_9

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v6, v0, v6

    if-eqz v5, :cond_3

    new-instance v3, Lcom/twitter/android/widget/br;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/br;-><init>(Lcom/twitter/android/widget/bq;)V

    move v12, v10

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    int-to-float v6, v6

    move v5, v4

    move v7, v4

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;ILjava/lang/Runnable;FFFFZ)V

    move v1, v8

    move v3, v12

    :goto_3
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    move v5, v3

    move v3, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    iget-object v6, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v6}, Lcom/twitter/android/widget/PoiFragment;->i(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bu;

    move-result-object v6

    iget-object v7, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v7, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;I)I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    goto :goto_1

    :cond_3
    move-object v3, v11

    move v12, v5

    goto :goto_2

    :cond_4
    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v0

    iget-object v3, p0, Lcom/twitter/android/widget/bq;->b:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getDividerHeight()I

    move-result v3

    add-int/2addr v0, v3

    if-lez v9, :cond_5

    :goto_4
    add-int/2addr v0, v6

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sub-int v6, v0, v6

    if-eqz v5, :cond_6

    new-instance v3, Lcom/twitter/android/widget/bs;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/bs;-><init>(Lcom/twitter/android/widget/bq;)V

    move v12, v10

    :goto_5
    iget-object v0, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    int-to-float v6, v6

    move v5, v4

    move v7, v4

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;ILjava/lang/Runnable;FFFFZ)V

    move v1, v8

    move v3, v12

    goto :goto_3

    :cond_5
    neg-int v0, v0

    goto :goto_4

    :cond_6
    move-object v3, v11

    move v12, v5

    goto :goto_5

    :cond_7
    if-nez v3, :cond_8

    iget-object v0, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->e(Lcom/twitter/android/widget/PoiFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setEnabled(Z)V

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/widget/bq;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->j(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    return v8

    :cond_9
    move v1, v3

    move v3, v5

    goto :goto_3
.end method
