.class public Lcom/twitter/android/widget/BadgedImageView;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private a:I

.field private b:Landroid/widget/ImageView;

.field private c:Landroid/widget/ImageView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    return-void
.end method

.method private c()V
    .locals 1

    const v0, 0x7f090189    # com.twitter.android.R.id.gallery_image_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BadgedImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->b:Landroid/widget/ImageView;

    const v0, 0x7f09018a    # com.twitter.android.R.id.gallery_image_badge

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BadgedImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->c:Landroid/widget/ImageView;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public getBadge()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onFinishInflate()V

    invoke-direct {p0}, Lcom/twitter/android/widget/BadgedImageView;->c()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/BadgedImageView;->setBadge(I)V

    return-void
.end method

.method public setBadge(I)V
    .locals 2

    iput p1, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    iget v0, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->c:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->c:Landroid/widget/ImageView;

    iget v1, p0, Lcom/twitter/android/widget/BadgedImageView;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->b:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/BadgedImageView;->b()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/BadgedImageView;->a()V

    goto :goto_0
.end method

.method public setImageResource(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/BadgedImageView;->b:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void
.end method
