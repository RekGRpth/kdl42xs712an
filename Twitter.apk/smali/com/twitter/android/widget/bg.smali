.class Lcom/twitter/android/widget/bg;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/twitter/android/client/c;

.field final synthetic c:Lcom/twitter/android/widget/PhoneVerificationDialog;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/PhoneVerificationDialog;Landroid/content/Context;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bg;->c:Lcom/twitter/android/widget/PhoneVerificationDialog;

    iput-object p2, p0, Lcom/twitter/android/widget/bg;->a:Landroid/content/Context;

    iput-object p3, p0, Lcom/twitter/android/widget/bg;->b:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method private a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/bg;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void
.end method


# virtual methods
.method public e(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bg;->a:Landroid/content/Context;

    const v1, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-direct {p0}, Lcom/twitter/android/widget/bg;->a()V

    :cond_0
    return-void
.end method

.method public f(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 4

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bg;->b:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;J)Ljava/lang/String;

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/widget/bg;->a()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/bg;->a:Landroid/content/Context;

    const v1, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
