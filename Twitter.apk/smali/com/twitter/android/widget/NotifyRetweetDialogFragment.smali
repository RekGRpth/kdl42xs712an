.class public Lcom/twitter/android/widget/NotifyRetweetDialogFragment;
.super Lcom/twitter/android/widget/RetweetDialogFragment;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/widget/RetweetDialogFragment;-><init>()V

    return-void
.end method

.method public static a(IJLcom/twitter/library/provider/ParcelableTweet;ZZLandroid/content/Intent;)Lcom/twitter/android/widget/NotifyRetweetDialogFragment;
    .locals 7

    new-instance v6, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;

    invoke-direct {v6}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;-><init>()V

    move v0, p0

    move-wide v1, p1

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->a(IJLcom/twitter/library/provider/ParcelableTweet;ZZLcom/twitter/android/widget/RetweetDialogFragment;)V

    invoke-virtual {v6}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "notif_service_intent"

    invoke-virtual {v0, v1, p6}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object v6
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "notif_service_intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/NotifyRetweetDialogFragment;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/widget/RetweetDialogFragment;->onClick(Landroid/content/DialogInterface;I)V

    goto :goto_0
.end method
