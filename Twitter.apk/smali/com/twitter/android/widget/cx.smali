.class public Lcom/twitter/android/widget/cx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/cy;


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field private final c:J

.field private final d:J

.field private final e:Landroid/graphics/Rect;


# direct methods
.method constructor <init>(JJLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/android/widget/cx;->c:J

    iput-wide p3, p0, Lcom/twitter/android/widget/cx;->d:J

    iput-object p5, p0, Lcom/twitter/android/widget/cx;->a:Ljava/lang/String;

    iput-object p6, p0, Lcom/twitter/android/widget/cx;->b:Ljava/lang/String;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v1, v1, v1, v1}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/twitter/android/widget/cx;->e:Landroid/graphics/Rect;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/cx;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/widget/cx;->c:J

    return-wide v0
.end method


# virtual methods
.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/cx;->e:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-void
.end method

.method public c()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/cx;->e:Landroid/graphics/Rect;

    return-object v0
.end method
