.class Lcom/twitter/android/widget/bw;
.super Landroid/widget/Filter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/PoiFragment;

.field private b:Lcom/twitter/android/util/z;


# direct methods
.method private constructor <init>(Lcom/twitter/android/widget/PoiFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    invoke-static {}, Lcom/twitter/android/util/z;->a()Lcom/twitter/android/util/z;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/bw;->b:Lcom/twitter/android/util/z;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/android/widget/bh;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/bw;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    return-void
.end method


# virtual methods
.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 7

    const/4 v6, 0x0

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Landroid/widget/Filter$FilterResults;

    invoke-direct {v3}, Landroid/widget/Filter$FilterResults;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->m(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->n(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/ArrayList;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->l(Lcom/twitter/android/widget/PoiFragment;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    :try_start_0
    iput-object v0, v3, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iput v0, v3, Landroid/widget/Filter$FilterResults;->count:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v3

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->m(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/ArrayList;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/bw;->b:Lcom/twitter/android/util/z;

    invoke-virtual {v0}, Lcom/twitter/android/util/z;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v5, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v5, v5, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v5}, Lcom/twitter/android/widget/PoiFragment;->f(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/library/api/geo/TwitterPlace;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v1, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_1

    :cond_2
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/bw;->b:Lcom/twitter/android/util/z;

    invoke-virtual {v0}, Lcom/twitter/android/util/z;->b()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v5, v0, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    iget-object v5, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v5}, Lcom/twitter/android/widget/PoiFragment;->f(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/twitter/library/api/geo/TwitterPlace;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_5

    invoke-virtual {v1, v6, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    goto :goto_2

    :cond_5
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    move-object v0, v1

    goto/16 :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/bw;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->i(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bu;

    move-result-object v1

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/bu;->a(Ljava/util/ArrayList;)V

    return-void
.end method
