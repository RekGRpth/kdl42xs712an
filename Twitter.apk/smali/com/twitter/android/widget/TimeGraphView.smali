.class public Lcom/twitter/android/widget/TimeGraphView;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final a:Ljava/text/SimpleDateFormat;

.field private static final b:Ljava/text/SimpleDateFormat;


# instance fields
.field private A:Lcom/twitter/android/widget/cy;

.field private B:Lcom/twitter/android/widget/cy;

.field private C:Lcom/twitter/android/widget/cz;

.field private final c:Landroid/graphics/Paint;

.field private final d:Landroid/graphics/Paint;

.field private final e:Landroid/graphics/Paint;

.field private final f:Landroid/graphics/Paint;

.field private final g:I

.field private final h:I

.field private final i:I

.field private final j:I

.field private final k:I

.field private final l:I

.field private final m:I

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I

.field private final r:F

.field private final s:Landroid/graphics/LinearGradient;

.field private final t:Landroid/graphics/LinearGradient;

.field private final u:Ljava/util/ArrayList;

.field private final v:Ljava/util/ArrayList;

.field private w:I

.field private x:[Lcom/twitter/android/widget/cw;

.field private y:[Lcom/twitter/android/widget/cv;

.field private z:[Lcom/twitter/android/widget/cx;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "MMM"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/twitter/android/widget/TimeGraphView;->a:Ljava/text/SimpleDateFormat;

    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string/jumbo v1, "yyyy"

    invoke-direct {v0, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/twitter/android/widget/TimeGraphView;->b:Ljava/text/SimpleDateFormat;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/widget/TimeGraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/TimeGraphView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 18

    invoke-direct/range {p0 .. p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->u:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->v:Ljava/util/ArrayList;

    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/TimeGraphView;->setClickable(Z)V

    sget-object v2, Lcom/twitter/android/rg;->TimeGraphView:[I

    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v2

    const/4 v3, 0x2

    const/4 v4, 0x5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->g:I

    const/4 v3, 0x3

    const/16 v4, 0x14

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    const/4 v3, 0x6

    const/16 v4, 0x14

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    div-int/lit8 v3, v3, 0x2

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->h:I

    const/16 v3, 0x8

    const/16 v4, 0x30

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->i:I

    const/16 v3, 0x9

    const v4, -0x333334

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->j:I

    const/16 v3, 0xa

    const/4 v4, 0x6

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->m:I

    const/16 v3, 0xb

    const/16 v4, -0x100

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->n:I

    const/16 v3, 0xc

    const/16 v4, -0x100

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->o:I

    const/16 v3, 0xf

    const/16 v4, 0x4e

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->p:I

    const/16 v3, 0x10

    const/16 v4, 0x26

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    move-object/from16 v0, p0

    iput v3, v0, Lcom/twitter/android/widget/TimeGraphView;->q:I

    const/4 v3, 0x1

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v10

    const/4 v3, 0x0

    const/4 v4, 0x3

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v11

    const/4 v3, 0x5

    const/16 v4, 0xc

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v3

    int-to-float v12, v3

    const/4 v3, 0x4

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v13

    const/16 v3, 0x11

    const/high16 v4, -0x1000000

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v14

    const/16 v3, 0x12

    const v4, -0x777778

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v15

    const/16 v3, 0x13

    const/4 v4, -0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v16

    invoke-virtual {v2}, Landroid/content/res/TypedArray;->recycle()V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TimeGraphView;->g:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->h:I

    add-int v17, v2, v3

    new-instance v2, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/widget/TimeGraphView;->p:I

    add-int v6, v6, v17

    int-to-float v6, v6

    const/4 v7, 0x3

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v14, v7, v8

    const/4 v8, 0x1

    aput v15, v7, v8

    const/4 v8, 0x2

    aput v16, v7, v8

    const/4 v8, 0x0

    sget-object v9, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->s:Landroid/graphics/LinearGradient;

    new-instance v2, Landroid/graphics/LinearGradient;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/widget/TimeGraphView;->q:I

    add-int v6, v6, v17

    int-to-float v6, v6

    const/4 v7, 0x3

    new-array v7, v7, [I

    const/4 v8, 0x0

    aput v14, v7, v8

    const/4 v8, 0x1

    aput v15, v7, v8

    const/4 v8, 0x2

    aput v16, v7, v8

    const/4 v8, 0x0

    sget-object v9, Landroid/graphics/Shader$TileMode;->REPEAT:Landroid/graphics/Shader$TileMode;

    invoke-direct/range {v2 .. v9}, Landroid/graphics/LinearGradient;-><init>(FFFF[I[FLandroid/graphics/Shader$TileMode;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->t:Landroid/graphics/LinearGradient;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TimeGraphView;->i:I

    mul-int/lit8 v2, v2, 0xc

    int-to-float v2, v2

    const v3, 0x43b68000    # 365.0f

    div-float/2addr v2, v3

    move-object/from16 v0, p0

    iput v2, v0, Lcom/twitter/android/widget/TimeGraphView;->r:F

    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->c:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->d:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    invoke-virtual {v2, v10}, Landroid/graphics/Paint;->setColor(I)V

    int-to-float v3, v11

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v3, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    sget-object v3, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    new-instance v3, Landroid/graphics/CornerPathEffect;

    div-int/lit8 v4, v11, 0x2

    int-to-float v4, v4

    invoke-direct {v3, v4}, Landroid/graphics/CornerPathEffect;-><init>(F)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setPathEffect(Landroid/graphics/PathEffect;)Landroid/graphics/PathEffect;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->e:Landroid/graphics/Paint;

    new-instance v2, Landroid/graphics/Paint;

    const/16 v3, 0x81

    invoke-direct {v2, v3}, Landroid/graphics/Paint;-><init>(I)V

    sget-object v3, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    invoke-virtual {v2, v13}, Landroid/graphics/Paint;->setColor(I)V

    invoke-virtual {v2, v12}, Landroid/graphics/Paint;->setTextSize(F)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->f:Landroid/graphics/Paint;

    const-wide v2, 0x10a1e926f70L

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/widget/TimeGraphView;->setUpSegments(J)V

    return-void
.end method

.method private setUpSegments(J)V
    .locals 16

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    move-wide/from16 v0, p1

    invoke-virtual {v10, v0, v1}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v4, 0x1

    invoke-virtual {v10, v4}, Ljava/util/Calendar;->get(I)I

    move-result v4

    const/4 v5, 0x2

    invoke-virtual {v10, v5}, Ljava/util/Calendar;->get(I)I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v10, v4, v5, v6}, Ljava/util/Calendar;->set(III)V

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide v6, 0x9ca41900L

    div-long/2addr v2, v6

    long-to-int v2, v2

    add-int/lit8 v11, v2, 0x1

    add-int/lit8 v12, v11, -0x1

    new-array v13, v11, [Lcom/twitter/android/widget/cx;

    invoke-virtual {v10, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    const/4 v2, 0x0

    move v9, v2

    :goto_0
    if-ge v9, v12, :cond_0

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v3

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8, v3, v4}, Ljava/util/Date;-><init>(J)V

    const/4 v2, 0x2

    const/4 v5, 0x1

    invoke-virtual {v10, v2, v5}, Ljava/util/Calendar;->add(II)V

    const/4 v2, 0x5

    const/4 v5, -0x1

    invoke-virtual {v10, v2, v5}, Ljava/util/Calendar;->add(II)V

    new-instance v2, Lcom/twitter/android/widget/cx;

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v5

    const-wide/16 v14, 0x3e8

    div-long/2addr v5, v14

    sget-object v7, Lcom/twitter/android/widget/TimeGraphView;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    sget-object v14, Lcom/twitter/android/widget/TimeGraphView;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v14, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/widget/cx;-><init>(JJLjava/lang/String;Ljava/lang/String;)V

    aput-object v2, v13, v9

    const/4 v2, 0x5

    const/4 v3, 0x1

    invoke-virtual {v10, v2, v3}, Ljava/util/Calendar;->add(II)V

    add-int/lit8 v2, v9, 0x1

    move v9, v2

    goto :goto_0

    :cond_0
    new-instance v8, Ljava/util/Date;

    invoke-virtual {v10}, Ljava/util/Calendar;->getTimeInMillis()J

    move-result-wide v2

    invoke-direct {v8, v2, v3}, Ljava/util/Date;-><init>(J)V

    new-instance v2, Lcom/twitter/android/widget/cx;

    const-wide/16 v3, 0x0

    const-wide/16 v5, 0x0

    sget-object v7, Lcom/twitter/android/widget/TimeGraphView;->a:Ljava/text/SimpleDateFormat;

    invoke-virtual {v7, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    sget-object v9, Lcom/twitter/android/widget/TimeGraphView;->b:Ljava/text/SimpleDateFormat;

    invoke-virtual {v9, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/twitter/android/widget/cx;-><init>(JJLjava/lang/String;Ljava/lang/String;)V

    aput-object v2, v13, v12

    move-object/from16 v0, p0

    iput-object v13, v0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    move-object/from16 v0, p0

    iput v11, v0, Lcom/twitter/android/widget/TimeGraphView;->w:I

    aget-object v2, v13, v12

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->B:Lcom/twitter/android/widget/cy;

    return-void
.end method


# virtual methods
.method public getActiveSegmentX()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->B:Lcom/twitter/android/widget/cy;

    invoke-interface {v0}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v0

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method public getActiveSelection()Lcom/twitter/android/widget/cy;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->B:Lcom/twitter/android/widget/cy;

    return-object v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 14

    const/4 v6, 0x0

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    iget v8, p0, Lcom/twitter/android/widget/TimeGraphView;->g:I

    iget v0, p0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    iget v1, p0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    add-int/2addr v0, v1

    iget v1, p0, Lcom/twitter/android/widget/TimeGraphView;->h:I

    add-int/2addr v0, v1

    iget v9, p0, Lcom/twitter/android/widget/TimeGraphView;->i:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/TimeGraphView;->getHeight()I

    move-result v1

    add-int/2addr v0, v8

    sub-int v10, v1, v0

    iget v11, p0, Lcom/twitter/android/widget/TimeGraphView;->m:I

    iget-object v5, p0, Lcom/twitter/android/widget/TimeGraphView;->c:Landroid/graphics/Paint;

    iget v0, p0, Lcom/twitter/android/widget/TimeGraphView;->j:I

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v0, 0x3f000000    # 0.5f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v12, p0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    array-length v13, v12

    move v7, v6

    :goto_0
    if-ge v7, v13, :cond_0

    aget-object v0, v12, v7

    invoke-virtual {v0}, Lcom/twitter/android/widget/cx;->c()Landroid/graphics/Rect;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    int-to-float v2, v8

    iget v0, v0, Landroid/graphics/Rect;->right:I

    int-to-float v3, v0

    add-int v0, v8, v10

    int-to-float v4, v0

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->x:[Lcom/twitter/android/widget/cw;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->d:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->v:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->e:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Path;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    if-eqz v0, :cond_3

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {v5, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    array-length v2, v1

    move v0, v6

    :goto_3
    if-ge v0, v2, :cond_3

    aget-object v3, v1, v0

    iget v4, p0, Lcom/twitter/android/widget/TimeGraphView;->n:I

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v4, v3, Lcom/twitter/android/widget/cw;->c:F

    iget v7, v3, Lcom/twitter/android/widget/cw;->d:F

    int-to-float v12, v11

    invoke-virtual {p1, v4, v7, v12, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    iget v4, p0, Lcom/twitter/android/widget/TimeGraphView;->o:I

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    sget-object v4, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v4, v3, Lcom/twitter/android/widget/cw;->c:F

    iget v3, v3, Lcom/twitter/android/widget/cw;->d:F

    int-to-float v7, v11

    invoke-virtual {p1, v4, v3, v7, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    invoke-interface {v0}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->d:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/widget/TimeGraphView;->B:Lcom/twitter/android/widget/cy;

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->d:Landroid/graphics/Paint;

    invoke-interface {v0, p1, v5, v1}, Lcom/twitter/android/widget/cy;->a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    array-length v2, v1

    move v0, v6

    :goto_4
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    invoke-virtual {v3}, Lcom/twitter/android/widget/cx;->c()Landroid/graphics/Rect;

    move-result-object v4

    iget-object v5, v3, Lcom/twitter/android/widget/cx;->a:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v5

    iget v6, v4, Landroid/graphics/Rect;->left:I

    div-int/lit8 v7, v9, 0x2

    add-int/2addr v6, v7

    int-to-float v6, v6

    add-int v7, v8, v10

    iget v11, p0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    add-int/2addr v7, v11

    int-to-float v7, v7

    iget-object v11, p0, Lcom/twitter/android/widget/TimeGraphView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v5, v6, v7, v11}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    iget-object v3, v3, Lcom/twitter/android/widget/cx;->b:Ljava/lang/String;

    iget v4, v4, Landroid/graphics/Rect;->left:I

    div-int/lit8 v5, v9, 0x2

    add-int/2addr v4, v5

    int-to-float v4, v4

    add-int v5, v8, v10

    iget v6, p0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    add-int/2addr v5, v6

    int-to-float v5, v5

    iget-object v6, p0, Lcom/twitter/android/widget/TimeGraphView;->f:Landroid/graphics/Paint;

    invoke-virtual {p1, v3, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5

    const/high16 v4, 0x40000000    # 2.0f

    iget v0, p0, Lcom/twitter/android/widget/TimeGraphView;->g:I

    iget v1, p0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    iget v2, p0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/twitter/android/widget/TimeGraphView;->h:I

    add-int/2addr v1, v2

    iget v2, p0, Lcom/twitter/android/widget/TimeGraphView;->i:I

    iget v3, p0, Lcom/twitter/android/widget/TimeGraphView;->w:I

    mul-int/2addr v2, v3

    iget-object v3, p0, Lcom/twitter/android/widget/TimeGraphView;->x:[Lcom/twitter/android/widget/cw;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/twitter/android/widget/TimeGraphView;->p:I

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->d:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/twitter/android/widget/TimeGraphView;->s:Landroid/graphics/LinearGradient;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    :goto_0
    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    invoke-super {p0, v1, v0}, Landroid/view/View;->onMeasure(II)V

    return-void

    :cond_0
    iget v3, p0, Lcom/twitter/android/widget/TimeGraphView;->q:I

    add-int/2addr v0, v3

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->d:Landroid/graphics/Paint;

    iget-object v3, p0, Lcom/twitter/android/widget/TimeGraphView;->t:Landroid/graphics/LinearGradient;

    invoke-virtual {v1, v3}, Landroid/graphics/Paint;->setShader(Landroid/graphics/Shader;)Landroid/graphics/Shader;

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 25

    if-lez p1, :cond_4

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/android/widget/TimeGraphView;->g:I

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/TimeGraphView;->k:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->l:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->h:I

    add-int/2addr v2, v3

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/android/widget/TimeGraphView;->r:F

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/android/widget/TimeGraphView;->i:I

    add-int/2addr v2, v8

    sub-int v11, p2, v2

    add-int v2, v11, v8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-static {v3}, Lcom/twitter/android/widget/cx;->a(Lcom/twitter/android/widget/cx;)J

    move-result-wide v12

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/widget/TimeGraphView;->x:[Lcom/twitter/android/widget/cw;

    if-eqz v14, :cond_2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/widget/TimeGraphView;->i:I

    mul-int/lit8 v3, v3, 0xc

    int-to-float v15, v3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/widget/TimeGraphView;->v:Ljava/util/ArrayList;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/widget/TimeGraphView;->u:Ljava/util/ArrayList;

    move-object/from16 v17, v0

    const/4 v7, 0x0

    int-to-float v6, v2

    const/4 v5, 0x0

    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->clear()V

    new-instance v4, Landroid/graphics/Path;

    invoke-direct {v4}, Landroid/graphics/Path;-><init>()V

    int-to-float v3, v2

    invoke-virtual {v4, v7, v3}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->clear()V

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    move/from16 v0, p2

    int-to-float v0, v0

    move/from16 v18, v0

    move/from16 v0, v18

    invoke-virtual {v3, v7, v0}, Landroid/graphics/Path;->moveTo(FF)V

    int-to-float v2, v2

    invoke-virtual {v3, v7, v2}, Landroid/graphics/Path;->lineTo(FF)V

    array-length v0, v14

    move/from16 v18, v0

    const/4 v2, 0x0

    move/from16 v24, v2

    move-object v2, v3

    move-object v3, v4

    move v4, v5

    move/from16 v5, v24

    :goto_0
    move/from16 v0, v18

    if-ge v5, v0, :cond_1

    aget-object v19, v14, v5

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/twitter/android/widget/cw;->a:J

    move-wide/from16 v20, v0

    sub-long v20, v20, v12

    move-wide/from16 v0, v20

    long-to-float v0, v0

    move/from16 v20, v0

    mul-float v20, v20, v9

    const v21, 0x47a8c000    # 86400.0f

    div-float v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/twitter/android/widget/cw;->c:F

    int-to-float v0, v8

    move/from16 v20, v0

    int-to-float v0, v11

    move/from16 v21, v0

    const/high16 v22, 0x3f800000    # 1.0f

    move-object/from16 v0, v19

    iget v0, v0, Lcom/twitter/android/widget/cw;->b:F

    move/from16 v23, v0

    sub-float v22, v22, v23

    mul-float v21, v21, v22

    add-float v20, v20, v21

    move/from16 v0, v20

    move-object/from16 v1, v19

    iput v0, v1, Lcom/twitter/android/widget/cw;->d:F

    move-object/from16 v0, v19

    iget v0, v0, Lcom/twitter/android/widget/cw;->c:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/twitter/android/widget/cw;->d:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, v19

    iget v0, v0, Lcom/twitter/android/widget/cw;->c:F

    move/from16 v20, v0

    move-object/from16 v0, v19

    iget v0, v0, Lcom/twitter/android/widget/cw;->d:F

    move/from16 v21, v0

    move/from16 v0, v20

    move/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, v19

    iget v0, v0, Lcom/twitter/android/widget/cw;->c:F

    move/from16 v20, v0

    sub-float v20, v20, v4

    cmpl-float v20, v20, v15

    if-lez v20, :cond_0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v3, Landroid/graphics/Path;

    invoke-direct {v3}, Landroid/graphics/Path;-><init>()V

    invoke-virtual {v3, v7, v6}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, v19

    iget v4, v0, Lcom/twitter/android/widget/cw;->c:F

    move-object/from16 v0, v19

    iget v6, v0, Lcom/twitter/android/widget/cw;->d:F

    invoke-virtual {v3, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, v19

    iget v4, v0, Lcom/twitter/android/widget/cw;->c:F

    move/from16 v0, p2

    int-to-float v6, v0

    invoke-virtual {v2, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Landroid/graphics/Path;

    invoke-direct {v2}, Landroid/graphics/Path;-><init>()V

    move-object/from16 v0, v19

    iget v4, v0, Lcom/twitter/android/widget/cw;->c:F

    move/from16 v0, p2

    int-to-float v6, v0

    invoke-virtual {v2, v4, v6}, Landroid/graphics/Path;->moveTo(FF)V

    move-object/from16 v0, v19

    iget v4, v0, Lcom/twitter/android/widget/cw;->c:F

    move-object/from16 v0, v19

    iget v6, v0, Lcom/twitter/android/widget/cw;->d:F

    invoke-virtual {v2, v4, v6}, Landroid/graphics/Path;->lineTo(FF)V

    move-object/from16 v0, v19

    iget v4, v0, Lcom/twitter/android/widget/cw;->c:F

    :cond_0
    move-object/from16 v0, v19

    iget v7, v0, Lcom/twitter/android/widget/cw;->c:F

    move-object/from16 v0, v19

    iget v6, v0, Lcom/twitter/android/widget/cw;->d:F

    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_0

    :cond_1
    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move/from16 v0, p2

    int-to-float v3, v0

    invoke-virtual {v2, v7, v3}, Landroid/graphics/Path;->lineTo(FF)V

    invoke-virtual {v2}, Landroid/graphics/Path;->close()V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_3

    aget-object v6, v4, v2

    invoke-virtual {v6}, Lcom/twitter/android/widget/cx;->c()Landroid/graphics/Rect;

    move-result-object v6

    const/4 v7, 0x0

    add-int v14, v3, v10

    move/from16 v0, p2

    invoke-virtual {v6, v3, v7, v14, v0}, Landroid/graphics/Rect;->set(IIII)V

    add-int/2addr v3, v10

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    if-eqz v2, :cond_4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    array-length v4, v3

    const/4 v2, 0x0

    :goto_2
    if-ge v2, v4, :cond_4

    aget-object v5, v3, v2

    iget-wide v6, v5, Lcom/twitter/android/widget/cv;->a:J

    sub-long/2addr v6, v12

    long-to-float v6, v6

    mul-float/2addr v6, v9

    const v7, 0x47a8c000    # 86400.0f

    div-float/2addr v6, v7

    iput v6, v5, Lcom/twitter/android/widget/cv;->c:F

    int-to-float v6, v8

    int-to-float v7, v11

    const/high16 v10, 0x3f800000    # 1.0f

    iget v14, v5, Lcom/twitter/android/widget/cv;->b:F

    sub-float/2addr v10, v14

    mul-float/2addr v7, v10

    add-float/2addr v6, v7

    iput v6, v5, Lcom/twitter/android/widget/cv;->d:F

    invoke-virtual {v5}, Lcom/twitter/android/widget/cv;->c()Landroid/graphics/Rect;

    move-result-object v6

    iget v7, v5, Lcom/twitter/android/widget/cv;->c:F

    float-to-int v7, v7

    add-int/lit8 v7, v7, -0x14

    iget v10, v5, Lcom/twitter/android/widget/cv;->d:F

    float-to-int v10, v10

    add-int/lit8 v10, v10, -0x14

    iget v14, v5, Lcom/twitter/android/widget/cv;->c:F

    float-to-int v14, v14

    add-int/lit8 v14, v14, 0x14

    iget v15, v5, Lcom/twitter/android/widget/cv;->d:F

    float-to-int v15, v15

    add-int/lit8 v15, v15, 0x14

    invoke-virtual {v6, v7, v10, v14, v15}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {v5}, Lcom/twitter/android/widget/cv;->d()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v5}, Lcom/twitter/android/widget/cv;->a()J

    move-result-wide v14

    sub-long/2addr v14, v12

    long-to-float v7, v14

    mul-float/2addr v7, v9

    const v10, 0x47a8c000    # 86400.0f

    div-float/2addr v7, v10

    float-to-int v7, v7

    const/4 v10, 0x0

    invoke-virtual {v5}, Lcom/twitter/android/widget/cv;->b()J

    move-result-wide v14

    sub-long/2addr v14, v12

    long-to-float v5, v14

    mul-float/2addr v5, v9

    const v14, 0x47a8c000    # 86400.0f

    div-float/2addr v5, v14

    float-to-int v5, v5

    move/from16 v0, p2

    invoke-virtual {v6, v7, v10, v5, v0}, Landroid/graphics/Rect;->set(IIII)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_4
    invoke-super/range {p0 .. p4}, Landroid/view/View;->onSizeChanged(IIII)V

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v5, 0x0

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/widget/TimeGraphView;->isEnabled()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v2, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v3, v1

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v4

    and-int/lit16 v4, v4, 0xff

    packed-switch v4, :pswitch_data_0

    :cond_1
    :goto_1
    invoke-super {p0, p1}, Landroid/view/View;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    if-eqz v1, :cond_2

    iget-object v4, p0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    array-length v5, v4

    move v1, v0

    :goto_2
    if-ge v1, v5, :cond_7

    aget-object v6, v4, v1

    invoke-interface {v6}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v7

    invoke-virtual {v7, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v8

    if-eqz v8, :cond_3

    iput-object v6, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    invoke-virtual {p0, v7}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    const/4 v1, 0x1

    :goto_3
    if-nez v1, :cond_1

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    array-length v4, v1

    :goto_4
    if-ge v0, v4, :cond_1

    aget-object v5, v1, v0

    invoke-interface {v5}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_4

    iput-object v5, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    invoke-virtual {p0, v6}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_1

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :pswitch_1
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/widget/TimeGraphView;->C:Lcom/twitter/android/widget/cz;

    if-eqz v4, :cond_5

    invoke-virtual {v0, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/widget/TimeGraphView;->C:Lcom/twitter/android/widget/cz;

    invoke-interface {v2, v1}, Lcom/twitter/android/widget/cz;->a(Lcom/twitter/android/widget/cy;)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/widget/TimeGraphView;->B:Lcom/twitter/android/widget/cy;

    invoke-interface {v2}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    iput-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->B:Lcom/twitter/android/widget/cy;

    :cond_5
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    iput-object v5, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    goto :goto_1

    :pswitch_2
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TimeGraphView;->z:[Lcom/twitter/android/widget/cx;

    array-length v4, v1

    :goto_5
    if-ge v0, v4, :cond_1

    aget-object v5, v1, v0

    invoke-interface {v5}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v7

    if-eqz v7, :cond_6

    iput-object v5, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    invoke-virtual {p0, v6}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    goto/16 :goto_1

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    :pswitch_3
    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/twitter/android/widget/cy;->c()Landroid/graphics/Rect;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TimeGraphView;->invalidate(Landroid/graphics/Rect;)V

    iput-object v5, p0, Lcom/twitter/android/widget/TimeGraphView;->A:Lcom/twitter/android/widget/cy;

    goto/16 :goto_1

    :cond_7
    move v1, v0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public setGraph([Lcom/twitter/android/widget/cw;)V
    .locals 4

    iput-object p1, p0, Lcom/twitter/android/widget/TimeGraphView;->x:[Lcom/twitter/android/widget/cw;

    const/4 v0, 0x0

    aget-object v0, p1, v0

    iget-wide v0, v0, Lcom/twitter/android/widget/cw;->a:J

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/widget/TimeGraphView;->setUpSegments(J)V

    return-void
.end method

.method public setHighlights([Lcom/twitter/android/widget/cv;)V
    .locals 4

    array-length v1, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p1, v0

    iget v3, p0, Lcom/twitter/android/widget/TimeGraphView;->m:I

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/cv;->a(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/widget/TimeGraphView;->y:[Lcom/twitter/android/widget/cv;

    return-void
.end method

.method public setOnGraphClickListener(Lcom/twitter/android/widget/cz;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/TimeGraphView;->C:Lcom/twitter/android/widget/cz;

    return-void
.end method
