.class public Lcom/twitter/android/widget/e;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Landroid/widget/TextView;

.field public final b:Lcom/twitter/internal/android/widget/ShadowTextView;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:Ljava/lang/String;

.field public h:Ljava/lang/String;

.field public i:Ljava/lang/String;

.field public j:Z

.field private final k:Landroid/view/View;

.field private final l:Landroid/widget/TextView;

.field private final m:Landroid/widget/TextView;

.field private final n:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/widget/e;->k:Landroid/view/View;

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/e;->l:Landroid/widget/TextView;

    const v0, 0x7f0900dd    # com.twitter.android.R.id.description

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/e;->m:Landroid/widget/TextView;

    const v0, 0x7f0900de    # com.twitter.android.R.id.metadata

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/e;->n:Landroid/widget/TextView;

    const v0, 0x7f0900dc    # com.twitter.android.R.id.owner_byline

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/e;->a:Landroid/widget/TextView;

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ShadowTextView;

    iput-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;J)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-wide v3, p0, Lcom/twitter/android/widget/e;->f:J

    cmp-long v0, v3, p2

    if-eqz v0, :cond_1

    invoke-static {}, Lgj;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ShadowTextView;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/twitter/android/widget/e;->j:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    const v2, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ShadowTextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ShadowTextView;->setChecked(Z)V

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    const v1, 0x7f0f019f    # com.twitter.android.R.string.follow

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ShadowTextView;->setText(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ShadowTextView;->setChecked(Z)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/e;->b:Lcom/twitter/internal/android/widget/ShadowTextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ShadowTextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 7

    const/4 v5, 0x2

    const/16 v6, 0x8

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x5

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$TwitterList;

    iget v0, v0, Lcom/twitter/library/api/TwitterTopic$TwitterList;->followStatus:I

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/widget/e;->j:Z

    invoke-interface {p2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/e;->c:Ljava/lang/String;

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/e;->d:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/e;->e:Ljava/lang/String;

    const/4 v0, 0x4

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/e;->i:Ljava/lang/String;

    const/4 v0, 0x6

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/widget/e;->f:J

    const/4 v0, 0x7

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/e;->g:Ljava/lang/String;

    invoke-interface {p2, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/e;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/widget/e;->a:Landroid/widget/TextView;

    const v3, 0x7f0f0213    # com.twitter.android.R.string.lists_by_username

    new-array v4, v5, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/twitter/android/widget/e;->g:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-object v5, p0, Lcom/twitter/android/widget/e;->h:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {p1, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/e;->l:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/widget/e;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/e;->m:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/e;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/widget/e;->m:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/widget/e;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/widget/e;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_0
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/e;->n:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/e;->n:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/e;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/e;->m:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/e;->a:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/e;->a:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method
