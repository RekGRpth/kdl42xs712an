.class Lcom/twitter/android/widget/dh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/TweetDetailView;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/TweetDetailView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/dh;->a:Lcom/twitter/android/widget/TweetDetailView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09024d    # com.twitter.android.R.id.retweets_stat

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/dh;->a:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v0, v0, Lcom/twitter/android/widget/TweetDetailView;->l:Lcom/twitter/android/widget/di;

    const/16 v1, 0xc

    iget-object v2, p0, Lcom/twitter/android/widget/dh;->a:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v2, v2, Lcom/twitter/android/widget/TweetDetailView;->k:Lcom/twitter/library/api/ActivitySummary;

    iget-object v2, v2, Lcom/twitter/library/api/ActivitySummary;->e:[J

    invoke-interface {v0, p1, v1, v2}, Lcom/twitter/android/widget/di;->a(Landroid/view/View;I[J)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f09024c    # com.twitter.android.R.id.favorites_stat

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dh;->a:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v0, v0, Lcom/twitter/android/widget/TweetDetailView;->l:Lcom/twitter/android/widget/di;

    const/16 v1, 0xb

    iget-object v2, p0, Lcom/twitter/android/widget/dh;->a:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v2, v2, Lcom/twitter/android/widget/TweetDetailView;->k:Lcom/twitter/library/api/ActivitySummary;

    iget-object v2, v2, Lcom/twitter/library/api/ActivitySummary;->d:[J

    invoke-interface {v0, p1, v1, v2}, Lcom/twitter/android/widget/di;->a(Landroid/view/View;I[J)V

    goto :goto_0
.end method
