.class public Lcom/twitter/android/widget/TwitterAccessPreference;
.super Landroid/preference/DialogPreference;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/RadioGroup$OnCheckedChangeListener;


# instance fields
.field a:Landroid/widget/RadioGroup;

.field b:Landroid/widget/Button;

.field c:Landroid/widget/EditText;

.field d:Landroid/widget/LinearLayout;

.field e:Landroid/widget/RadioGroup;

.field private final f:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2

    invoke-direct {p0, p1, p2}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const v0, 0x7f030167    # com.twitter.android.R.layout.twitter_access_preference

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->setDialogLayoutResource(I)V

    invoke-static {p1}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/widget/TwitterAccessPreference;->setSummary(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->f:Landroid/content/SharedPreferences;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/TwitterAccessPreference;)Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->f:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static a(Ljava/lang/String;)Lcom/twitter/library/api/ad;
    .locals 5

    const/4 v1, 0x0

    :try_start_0
    new-instance v0, Ljava/net/URL;

    invoke-direct {v0, p0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v0

    check-cast v0, Ljava/net/HttpURLConnection;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/net/ProtocolException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/16 v2, 0x2710

    :try_start_1
    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    const/16 v2, 0x3a98

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    const-string/jumbo v2, "GET"

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Ljava/net/HttpURLConnection;->setDoInput(Z)V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->connect()V

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_1

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/library/api/ap;->m(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/api/ClientConfiguration;

    move-result-object v2

    iget-object v1, v2, Lcom/twitter/library/api/ClientConfiguration;->b:Lcom/twitter/library/api/ad;
    :try_end_1
    .catch Ljava/net/MalformedURLException; {:try_start_1 .. :try_end_1} :catch_5
    .catch Ljava/net/ProtocolException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    :goto_1
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    move-object v0, v1

    goto :goto_0

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_2
    if-eqz v0, :cond_4

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_4
    move-object v0, v1

    goto :goto_0

    :catch_2
    move-exception v0

    move-object v0, v1

    :goto_3
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_5
    move-object v0, v1

    goto :goto_0

    :catchall_0
    move-exception v0

    :goto_4
    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_6
    throw v0

    :catchall_1
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    goto :goto_4

    :catch_3
    move-exception v2

    goto :goto_3

    :catch_4
    move-exception v2

    goto :goto_2

    :catch_5
    move-exception v2

    goto :goto_1
.end method

.method private a(Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "twitter_access_config"

    invoke-interface {p1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Twitter Access is currently On"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "Twitter Access is currently Off"

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/widget/TwitterAccessPreference;Landroid/content/SharedPreferences;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 5

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v0, "twitter_access_config"

    invoke-interface {v1, v0}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v2

    const v0, 0x7f0902b3    # com.twitter.android.R.id.twitter_access_carrier

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const-string/jumbo v3, "twitter_access_carrier"

    const-string/jumbo v4, "twitter_test"

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    iput-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->c:Landroid/widget/EditText;

    const v1, 0x7f0902b0    # com.twitter.android.R.id.twitter_access_group

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/RadioGroup;

    invoke-virtual {v1, p0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iput-object v1, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->e:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setEnabled(Z)V

    if-eqz v2, :cond_0

    const v0, 0x7f0902b2    # com.twitter.android.R.id.twitter_access_on

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    :goto_0
    new-instance v0, Lcom/twitter/android/widget/dp;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/dp;-><init>(Lcom/twitter/android/widget/TwitterAccessPreference;)V

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->setOnCheckedChangeListener(Landroid/widget/RadioGroup$OnCheckedChangeListener;)V

    iput-object v1, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->a:Landroid/widget/RadioGroup;

    const v0, 0x7f090155    # com.twitter.android.R.id.validation_progress

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->d:Landroid/widget/LinearLayout;

    return-void

    :cond_0
    const v0, 0x7f0902b1    # com.twitter.android.R.id.twitter_access_off

    invoke-virtual {v1, v0}, Landroid/widget/RadioGroup;->check(I)V

    goto :goto_0
.end method

.method public onCheckedChanged(Landroid/widget/RadioGroup;I)V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v1, 0x7f0902b2    # com.twitter.android.R.id.twitter_access_on

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/android/widget/dq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/widget/dq;-><init>(Lcom/twitter/android/widget/TwitterAccessPreference;Lcom/twitter/android/widget/dp;)V

    new-array v1, v2, [Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->c:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/dq;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/widget/TwitterAccessPreference;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string/jumbo v1, "TwitterAccess is turned off"

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    const/16 v1, 0x30

    invoke-virtual {v0, v1, v3, v3}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TwitterAccessPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "twitter_access_config"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "twitter_access_carrier"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/TwitterAccessPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    instance-of v1, v0, Landroid/app/AlertDialog;

    if-eqz v1, :cond_0

    check-cast v0, Landroid/app/AlertDialog;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/widget/TwitterAccessPreference;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    return-void
.end method
