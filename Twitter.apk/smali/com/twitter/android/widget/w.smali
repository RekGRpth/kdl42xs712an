.class public Lcom/twitter/android/widget/w;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:J

.field public b:Landroid/net/Uri;

.field public c:Z

.field d:Ljava/util/concurrent/Future;

.field private final e:Lcom/twitter/android/widget/BadgedImageView;

.field private final f:Landroid/view/View;

.field private final g:Landroid/view/View;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/BadgedImageView;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/widget/w;->e:Lcom/twitter/android/widget/BadgedImageView;

    const v0, 0x7f09018b    # com.twitter.android.R.id.gallery_image_selected_mask

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/BadgedImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/w;->f:Landroid/view/View;

    const v0, 0x7f09018d    # com.twitter.android.R.id.gallery_image_disabled_mask

    invoke-virtual {p1, v0}, Lcom/twitter/android/widget/BadgedImageView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/w;->g:Landroid/view/View;

    return-void
.end method


# virtual methods
.method a(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/w;->e:Lcom/twitter/android/widget/BadgedImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/BadgedImageView;->setImageResource(I)V

    return-void
.end method

.method a(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/w;->e:Lcom/twitter/android/widget/BadgedImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/BadgedImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method a(Z)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/widget/w;->g:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/w;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/w;->g:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method c()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/w;->f:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/w;->g:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method d()V
    .locals 2

    const/4 v1, 0x4

    iget-object v0, p0, Lcom/twitter/android/widget/w;->f:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/w;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method
