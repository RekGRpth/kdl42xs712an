.class public Lcom/twitter/android/widget/cv;
.super Lcom/twitter/android/widget/cw;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/cy;


# instance fields
.field private final e:J

.field private final f:J

.field private final g:Landroid/graphics/Rect;

.field private final h:Landroid/graphics/Rect;

.field private i:I


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/widget/cv;->e:J

    return-wide v0
.end method

.method a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/widget/cv;->i:I

    return-void
.end method

.method public a(Landroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Paint;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/cv;->h:Landroid/graphics/Rect;

    invoke-virtual {p1, v0, p3}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    const/4 v0, -0x1

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setColor(I)V

    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    sget-object v0, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lcom/twitter/android/widget/cv;->c:F

    iget v1, p0, Lcom/twitter/android/widget/cv;->d:F

    iget v2, p0, Lcom/twitter/android/widget/cv;->i:I

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    sget-object v0, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {p2, v0}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget v0, p0, Lcom/twitter/android/widget/cv;->c:F

    iget v1, p0, Lcom/twitter/android/widget/cv;->d:F

    iget v2, p0, Lcom/twitter/android/widget/cv;->i:I

    add-int/lit8 v2, v2, 0x2

    int-to-float v2, v2

    invoke-virtual {p1, v0, v1, v2, p2}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    return-void
.end method

.method public b()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/widget/cv;->f:J

    return-wide v0
.end method

.method public c()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/cv;->g:Landroid/graphics/Rect;

    return-object v0
.end method

.method d()Landroid/graphics/Rect;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/cv;->h:Landroid/graphics/Rect;

    return-object v0
.end method
