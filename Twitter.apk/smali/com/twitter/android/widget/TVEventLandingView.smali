.class public Lcom/twitter/android/widget/TVEventLandingView;
.super Lcom/twitter/android/widget/EventView;
.source "Twttr"


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/EventView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/EventView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/EventView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 23

    const/16 v22, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move-wide/from16 v11, p9

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move/from16 v17, p15

    move/from16 v18, p16

    move-object/from16 v19, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    invoke-super/range {v2 .. v22}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p14 .. p14}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterTopic$TvEvent;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/TVEventLandingView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    new-instance v4, Ljava/text/SimpleDateFormat;

    const-string/jumbo v5, "hh:mm a"

    invoke-direct {v4, v5}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    const v5, 0x7f0f015d    # com.twitter.android.R.string.event_card_airtime

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    new-instance v7, Ljava/util/Date;

    move-wide/from16 v0, p9

    invoke-direct {v7, v0, v1}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v5, v6

    const/4 v4, 0x1

    iget-object v6, v2, Lcom/twitter/library/api/TwitterTopic$TvEvent;->channel:Ljava/lang/String;

    aput-object v6, v5, v4

    invoke-static {v3, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TVEventLandingView;->b:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/twitter/library/api/TwitterTopic$TvEvent;->seriesTitle:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/twitter/android/widget/TVEventLandingView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TVEventLandingView;->c:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeTitle:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/twitter/android/widget/TVEventLandingView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/TVEventLandingView;->d:Landroid/widget/TextView;

    iget-object v5, v2, Lcom/twitter/library/api/TwitterTopic$TvEvent;->channel:Ljava/lang/String;

    if-eqz v5, :cond_0

    const-wide/16 v5, 0x0

    cmp-long v5, p9, v5

    if-lez v5, :cond_0

    :goto_0
    invoke-static {v4, v3}, Lcom/twitter/android/widget/TVEventLandingView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/TVEventLandingView;->e:Landroid/widget/TextView;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterTopic$TvEvent;->episodeDescription:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/twitter/android/widget/TVEventLandingView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    return-void

    :cond_0
    const/4 v3, 0x0

    goto :goto_0
.end method

.method protected onFinishInflate()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/widget/EventView;->onFinishInflate()V

    const v0, 0x7f090164    # com.twitter.android.R.id.show_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TVEventLandingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TVEventLandingView;->b:Landroid/widget/TextView;

    const v0, 0x7f090165    # com.twitter.android.R.id.episode_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TVEventLandingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TVEventLandingView;->c:Landroid/widget/TextView;

    const v0, 0x7f090166    # com.twitter.android.R.id.air_time

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TVEventLandingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TVEventLandingView;->d:Landroid/widget/TextView;

    const v0, 0x7f0900dd    # com.twitter.android.R.id.description

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/TVEventLandingView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/TVEventLandingView;->e:Landroid/widget/TextView;

    return-void
.end method
