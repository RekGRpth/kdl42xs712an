.class public Lcom/twitter/android/widget/dn;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V
    .locals 13

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x6

    new-array v12, v1, [I

    fill-array-data v12, :array_0

    const v4, 0x7f020105    # com.twitter.android.R.drawable.ic_action_rt_on

    const v5, 0x7f020101    # com.twitter.android.R.drawable.ic_action_rt_off

    const v1, 0x7f0f007d    # com.twitter.android.R.string.button_status_retweeted

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const v1, 0x7f0f005e    # com.twitter.android.R.string.button_action_retweet

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f0200f1    # com.twitter.android.R.drawable.ic_action_fave_on

    const v9, 0x7f0200ec    # com.twitter.android.R.drawable.ic_action_fave_off

    const v1, 0x7f0f007c    # com.twitter.android.R.string.button_status_favorited

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    const v1, 0x7f0f0058    # com.twitter.android.R.string.button_action_fave

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    move-object v0, p1

    move-object v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-static/range {v0 .. v12}, Lcom/twitter/android/widget/dn;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IILjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;[I)V

    return-void

    :array_0
    .array-data 4
        0x7f09006d    # com.twitter.android.R.id.reply
        0x7f09006e    # com.twitter.android.R.id.retweet
        0x7f09006f    # com.twitter.android.R.id.favorite
        0x7f090070    # com.twitter.android.R.id.share
        0x7f090071    # com.twitter.android.R.id.delete
        0x7f0901a3    # com.twitter.android.R.id.dismiss
    .end array-data
.end method

.method public static a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;IILjava/lang/String;Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;[I)V
    .locals 11

    move-object/from16 v0, p12

    array-length v5, v0

    const/4 v1, 0x0

    move v4, v1

    :goto_0
    if-ge v4, v5, :cond_16

    aget v6, p12, v4

    invoke-virtual {p2, v6}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    if-nez v1, :cond_0

    :goto_1
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_0

    :cond_0
    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->q:J

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    cmp-long v2, v2, v7

    if-nez v2, :cond_2

    const/4 v2, 0x1

    :goto_2
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v7

    const-wide/16 v9, 0x0

    cmp-long v3, v7, v9

    if-lez v3, :cond_3

    const/4 v3, 0x1

    :goto_3
    const v7, 0x7f09006e    # com.twitter.android.R.id.retweet

    if-ne v6, v7, :cond_b

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    if-nez v3, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    :cond_1
    :goto_4
    invoke-virtual {v1, p3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1

    :cond_2
    const/4 v2, 0x0

    goto :goto_2

    :cond_3
    const/4 v3, 0x0

    goto :goto_3

    :cond_4
    if-eqz v2, :cond_6

    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v2, :cond_5

    invoke-virtual {v1, p4}, Landroid/widget/ImageButton;->setImageResource(I)V

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_5
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_4

    :cond_6
    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->F:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_7

    move v2, p4

    :goto_5
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->F:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_8

    move v2, p4

    :goto_6
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setTag(Ljava/lang/Object;)V

    iget-wide v2, p1, Lcom/twitter/library/provider/Tweet;->F:J

    const-wide/16 v6, 0x0

    cmp-long v2, v2, v6

    if-lez v2, :cond_9

    move-object/from16 v2, p6

    :goto_7
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->m:Z

    if-nez v2, :cond_a

    const/4 v2, 0x1

    :goto_8
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_4

    :cond_7
    move/from16 v2, p5

    goto :goto_5

    :cond_8
    move/from16 v2, p5

    goto :goto_6

    :cond_9
    move-object/from16 v2, p7

    goto :goto_7

    :cond_a
    const/4 v2, 0x0

    goto :goto_8

    :cond_b
    const v7, 0x7f09006f    # com.twitter.android.R.id.favorite

    if-ne v6, v7, :cond_e

    if-nez v3, :cond_c

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto :goto_4

    :cond_c
    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v2, :cond_d

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    move-object/from16 v0, p10

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_d
    move/from16 v0, p9

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    move-object/from16 v0, p11

    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    :cond_e
    const v7, 0x7f090070    # com.twitter.android.R.id.share

    if-ne v6, v7, :cond_11

    if-eqz v3, :cond_10

    if-nez v2, :cond_f

    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->m:Z

    if-nez v2, :cond_10

    :cond_f
    const/4 v2, 0x1

    :goto_9
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_4

    :cond_10
    const/4 v2, 0x0

    goto :goto_9

    :cond_11
    const v7, 0x7f090071    # com.twitter.android.R.id.delete

    if-ne v6, v7, :cond_13

    if-eqz v2, :cond_12

    iget-boolean v2, p1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-nez v2, :cond_12

    const/4 v2, 0x0

    :goto_a
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    :cond_12
    const/16 v2, 0x8

    goto :goto_a

    :cond_13
    const v2, 0x7f0901a3    # com.twitter.android.R.id.dismiss

    if-ne v6, v2, :cond_15

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->G()Z

    move-result v2

    if-eqz v2, :cond_14

    const/4 v2, 0x0

    :goto_b
    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    goto/16 :goto_4

    :cond_14
    const/16 v2, 0x8

    goto :goto_b

    :cond_15
    const v2, 0x7f09006d    # com.twitter.android.R.id.reply

    if-ne v6, v2, :cond_1

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setEnabled(Z)V

    goto/16 :goto_4

    :cond_16
    return-void
.end method
