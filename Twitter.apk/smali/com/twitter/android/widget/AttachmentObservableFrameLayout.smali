.class public Lcom/twitter/android/widget/AttachmentObservableFrameLayout;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/widget/b;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method protected onAttachedToWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onAttachedToWindow()V

    iget-object v0, p0, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->a:Lcom/twitter/android/widget/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->a:Lcom/twitter/android/widget/b;

    invoke-interface {v0}, Lcom/twitter/android/widget/b;->a()V

    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    iget-object v0, p0, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->a:Lcom/twitter/android/widget/b;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->a:Lcom/twitter/android/widget/b;

    invoke-interface {v0}, Lcom/twitter/android/widget/b;->b()V

    :cond_0
    return-void
.end method

.method public setWindowAttachmentListener(Lcom/twitter/android/widget/b;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/AttachmentObservableFrameLayout;->a:Lcom/twitter/android/widget/b;

    return-void
.end method
