.class Lcom/twitter/android/widget/bo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Landroid/view/View;

.field final synthetic c:F

.field final synthetic d:F

.field final synthetic e:Ljava/lang/Runnable;

.field final synthetic f:Lcom/twitter/android/widget/PoiFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/PoiFragment;ZLandroid/view/View;FFLjava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bo;->f:Lcom/twitter/android/widget/PoiFragment;

    iput-boolean p2, p0, Lcom/twitter/android/widget/bo;->a:Z

    iput-object p3, p0, Lcom/twitter/android/widget/bo;->b:Landroid/view/View;

    iput p4, p0, Lcom/twitter/android/widget/bo;->c:F

    iput p5, p0, Lcom/twitter/android/widget/bo;->d:F

    iput-object p6, p0, Lcom/twitter/android/widget/bo;->e:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/widget/bo;->a:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bo;->b:Landroid/view/View;

    iget v1, p0, Lcom/twitter/android/widget/bo;->c:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationX(F)V

    iget-object v0, p0, Lcom/twitter/android/widget/bo;->b:Landroid/view/View;

    iget v1, p0, Lcom/twitter/android/widget/bo;->d:F

    invoke-virtual {v0, v1}, Landroid/view/View;->setTranslationY(F)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/bo;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0

    return-void
.end method
