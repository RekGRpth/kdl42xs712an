.class public Lcom/twitter/android/widget/PoiFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Landroid/widget/AdapterView$OnItemClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/twitter/android/widget/k;


# instance fields
.field private A:Lcom/twitter/library/platform/LocationProducer;

.field private B:Lcom/twitter/library/client/aa;

.field private C:Lcom/twitter/android/util/z;

.field private D:Ljava/util/HashSet;

.field private E:Ljava/util/HashMap;

.field private final a:Ljava/lang/Object;

.field private b:Landroid/widget/ListView;

.field private c:Lcom/twitter/android/widget/bx;

.field private d:Landroid/widget/EditText;

.field private e:Landroid/view/View;

.field private f:Lcom/twitter/android/widget/PoiItemView;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/TextView;

.field private i:Landroid/widget/ProgressBar;

.field private j:Lcom/twitter/android/widget/ComposerLayout;

.field private k:Landroid/view/View;

.field private l:Landroid/view/View;

.field private m:Ljava/util/ArrayList;

.field private n:Ljava/util/ArrayList;

.field private o:Ljava/util/ArrayList;

.field private p:Lcom/twitter/library/api/geo/TwitterPlace;

.field private q:Lcom/twitter/android/widget/by;

.field private r:Z

.field private s:Z

.field private t:I

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Z

.field private x:Z

.field private y:Lcom/twitter/android/widget/bu;

.field private z:Lcom/twitter/android/client/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->o:Ljava/util/ArrayList;

    invoke-static {}, Lcom/twitter/android/util/z;->a()Lcom/twitter/android/util/z;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->C:Lcom/twitter/android/util/z;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->D:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->E:Ljava/util/HashMap;

    return-void
.end method

.method private static a(FI)F
    .locals 1

    int-to-float v0, p1

    cmpl-float v0, p0, v0

    if-lez v0, :cond_0

    int-to-float v0, p1

    div-float v0, p0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    mul-int/2addr v0, p1

    int-to-float p0, v0

    :cond_0
    return p0
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/PoiFragment;->b(I)I

    move-result v0

    return v0
.end method

.method private a(Lcom/twitter/library/api/geo/TwitterPlace;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->C:Lcom/twitter/android/util/z;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/z;->a(Lcom/twitter/library/api/geo/TwitterPlace;)I

    move-result v0

    goto :goto_0
.end method

.method private a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    const v0, 0x7f0300ea    # com.twitter.android.R.layout.poi_fragment

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/view/View;)V

    const v0, 0x7f090212    # com.twitter.android.R.id.query

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    const v0, 0x7f090213    # com.twitter.android.R.id.poi_list

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    if-eqz p3, :cond_0

    const-string/jumbo v0, "poi_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    :goto_0
    new-instance v0, Lcom/twitter/android/widget/bu;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0300eb    # com.twitter.android.R.layout.poi_list_item_view

    iget-object v4, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/twitter/android/widget/bu;-><init>(Lcom/twitter/android/widget/PoiFragment;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-object v1

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/PoiFragment;->k:Landroid/view/View;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    return-object v0
.end method

.method public static a(IZLjava/lang/String;)Lcom/twitter/android/widget/PoiFragment;
    .locals 3

    new-instance v0, Lcom/twitter/android/widget/PoiFragment;

    invoke-direct {v0}, Lcom/twitter/android/widget/PoiFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v2, "poi_mode"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-nez p0, :cond_1

    const-string/jumbo v2, "search_text"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v0

    :cond_1
    const/4 v2, 0x1

    if-ne p0, v2, :cond_0

    const-string/jumbo v2, "location_enabled"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/android/widget/by;)Lcom/twitter/android/widget/by;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/PoiFragment;->q:Lcom/twitter/android/widget/by;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/library/api/geo/TwitterPlace;)Lcom/twitter/library/api/geo/TwitterPlace;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    return-object p1
.end method

.method static synthetic a(FLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 1

    invoke-static {p0, p1}, Lcom/twitter/android/widget/PoiFragment;->b(FLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/Cursor;)V
    .locals 7

    const/4 v6, 0x1

    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_2
    invoke-interface {p1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-interface {p1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v4, p0, Lcom/twitter/android/widget/PoiFragment;->o:Ljava/util/ArrayList;

    new-instance v5, Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-direct {v5, v1, v2, v3, v0}, Lcom/twitter/library/api/geo/TwitterPlace;-><init>(Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_2

    goto :goto_0
.end method

.method private a(Landroid/view/View;)V
    .locals 1

    const v0, 0x1020004    # android.R.id.empty

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->g:Landroid/view/View;

    const v0, 0x7f090046    # com.twitter.android.R.id.list_empty_text

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->h:Landroid/widget/TextView;

    const v0, 0x7f090047    # com.twitter.android.R.id.list_progress

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->i:Landroid/widget/ProgressBar;

    return-void
.end method

.method private a(Landroid/view/View;ILjava/lang/Runnable;FFFFZ)V
    .locals 9
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-le v1, v2, :cond_1

    invoke-virtual {p1, p4}, Landroid/view/View;->setTranslationX(F)V

    invoke-virtual {p1, p6}, Landroid/view/View;->setTranslationY(F)V

    if-eqz p3, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    new-instance v1, Lcom/twitter/android/widget/bo;

    move-object v2, p0

    move/from16 v3, p8

    move-object v4, p1

    move v5, p4

    move v6, p6

    move-object v7, p3

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/widget/bo;-><init>(Lcom/twitter/android/widget/PoiFragment;ZLandroid/view/View;FFLjava/lang/Runnable;)V

    invoke-virtual {v8, v1}, Landroid/view/ViewPropertyAnimator;->setListener(Landroid/animation/Animator$AnimatorListener;)Landroid/view/ViewPropertyAnimator;

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    invoke-virtual {v1, p5}, Landroid/view/ViewPropertyAnimator;->translationX(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v1

    move/from16 v0, p7

    invoke-virtual {v1, v0}, Landroid/view/ViewPropertyAnimator;->translationY(F)Landroid/view/ViewPropertyAnimator;

    :goto_0
    return-void

    :cond_1
    new-instance v1, Landroid/view/animation/TranslateAnimation;

    move/from16 v0, p7

    invoke-direct {v1, p4, p5, p6, v0}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    move/from16 v0, p8

    invoke-virtual {v1, v0}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    int-to-long v2, p2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    if-eqz p3, :cond_2

    new-instance v2, Lcom/twitter/android/widget/bp;

    invoke-direct {v2, p0, p3}, Lcom/twitter/android/widget/bp;-><init>(Lcom/twitter/android/widget/PoiFragment;Ljava/lang/Runnable;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    :cond_2
    invoke-virtual {p1, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method private a(Landroid/view/View;Lcom/twitter/library/api/geo/TwitterPlace;I)V
    .locals 7

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int v4, v0, v1

    move v1, v2

    :goto_0
    invoke-virtual {v3}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    if-ge v1, v0, :cond_4

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v5

    if-eq v5, p1, :cond_1

    add-int v0, v4, v1

    if-ltz v0, :cond_0

    iget-object v6, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v6}, Lcom/twitter/android/widget/bu;->getCount()I

    move-result v6

    if-lt v0, v6, :cond_3

    :cond_0
    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    :goto_1
    iget-object v6, p0, Lcom/twitter/android/widget/PoiFragment;->E:Ljava/util/HashMap;

    invoke-virtual {v5}, Landroid/view/View;->getTop()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v0, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v5}, Landroid/view/View;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    iget-object v6, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/PoiFragment;->b(I)I

    move-result v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "compose:poi:poi_list::flag"

    aput-object v5, v4, v2

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p2, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->h()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->i()I

    move-result v5

    invoke-direct {p0, p2}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/library/api/geo/TwitterPlace;)I

    move-result v6

    invoke-static {v2, v4, v5, v6, p3}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-direct {p0, p2}, Lcom/twitter/android/widget/PoiFragment;->b(Lcom/twitter/library/api/geo/TwitterPlace;)V

    invoke-virtual {v3}, Landroid/widget/ListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/widget/bq;

    invoke-direct {v1, p0, v0, v3}, Lcom/twitter/android/widget/bq;-><init>(Lcom/twitter/android/widget/PoiFragment;Landroid/view/ViewTreeObserver;Landroid/widget/ListView;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void
.end method

.method private a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 10

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    if-gez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v3}, Lcom/twitter/android/widget/bu;->getCount()I

    move-result v3

    if-lt v0, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->d()Z

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/PoiFragment;->b(I)I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    iget-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v5, :cond_4

    iget-object v5, v3, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v6, v6, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_4

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v5

    invoke-direct {v0, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v1, [Ljava/lang/String;

    const-string/jumbo v6, "compose:poi:poi_list:location:deselect"

    aput-object v6, v5, v2

    invoke-virtual {v0, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v5, v5, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->h()Ljava/lang/String;

    move-result-object v6

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->i()I

    move-result v7

    invoke-direct {p0, v3}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/library/api/geo/TwitterPlace;)I

    move-result v3

    invoke-static {v5, v6, v7, v3, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Z)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    iput-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v6, v1, [Ljava/lang/String;

    const-string/jumbo v7, "compose:poi:poi_list:location:select"

    aput-object v7, v6, v2

    invoke-virtual {v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v5

    iget-object v6, v3, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->h()Ljava/lang/String;

    move-result-object v7

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->i()I

    move-result v8

    invoke-direct {p0, v3}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/library/api/geo/TwitterPlace;)I

    move-result v9

    invoke-static {v6, v7, v8, v9, v0}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v5, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/by;

    invoke-direct {p0, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/by;)V

    :goto_2
    invoke-virtual {p0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    invoke-interface {v0, v3, v1}, Lcom/twitter/android/widget/bx;->a(Lcom/twitter/library/api/geo/TwitterPlace;Z)V

    goto/16 :goto_0

    :cond_5
    iget-object v4, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    monitor-enter v4

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    const/4 v5, 0x0

    invoke-virtual {v0, v5, v3}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const-string/jumbo v5, ""

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    iget-object v4, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/bu;->a(Ljava/util/ArrayList;)V

    goto :goto_2

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;ILjava/lang/Runnable;FFFFZ)V
    .locals 0

    invoke-direct/range {p0 .. p8}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/view/View;ILjava/lang/Runnable;FFFFZ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;Lcom/twitter/library/api/geo/TwitterPlace;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/view/View;Lcom/twitter/library/api/geo/TwitterPlace;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/library/api/geo/TwitterPlace;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/library/api/geo/TwitterPlace;I)V

    return-void
.end method

.method private a(Lcom/twitter/android/widget/by;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->q:Lcom/twitter/android/widget/by;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->q:Lcom/twitter/android/widget/by;

    iget-object v0, v0, Lcom/twitter/android/widget/by;->a:Lcom/twitter/android/widget/PoiItemView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiItemView;->setSelected(Z)V

    :cond_0
    iget-object v0, p1, Lcom/twitter/android/widget/by;->a:Lcom/twitter/android/widget/PoiItemView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PoiItemView;->setSelected(Z)V

    iput-object p1, p0, Lcom/twitter/android/widget/PoiFragment;->q:Lcom/twitter/android/widget/by;

    return-void
.end method

.method private a(Lcom/twitter/library/api/geo/TwitterPlace;I)V
    .locals 6

    new-instance v0, Lcom/twitter/android/widget/bz;

    iget-object v1, p1, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->h()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->i()I

    move-result v3

    iget v4, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    if-nez v4, :cond_0

    move v4, p2

    :goto_0
    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/widget/bz;-><init>(Ljava/lang/String;Ljava/lang/String;III)V

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->D:Ljava/util/HashSet;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/library/api/geo/TwitterPlace;)I

    move-result v4

    goto :goto_0
.end method

.method private b(I)I
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return p1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    if-nez p1, :cond_2

    move p1, v0

    goto :goto_0

    :cond_2
    if-gt p1, v0, :cond_0

    add-int/lit8 p1, p1, -0x1

    goto :goto_0
.end method

.method private b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6

    const v5, 0x7f0300eb    # com.twitter.android.R.layout.poi_list_item_view

    const/4 v4, 0x0

    const v0, 0x7f0300ea    # com.twitter.android.R.layout.poi_fragment

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/view/View;)V

    const v0, 0x7f090213    # com.twitter.android.R.id.poi_list

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iput v4, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    if-eqz p3, :cond_5

    const-string/jumbo v0, "poi_selected"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-interface {v0, v1, v4}, Lcom/twitter/android/widget/bx;->a(Lcom/twitter/library/api/geo/TwitterPlace;Z)V

    :cond_0
    const-string/jumbo v0, "poi_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iget v0, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeType:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    :cond_1
    const-string/jumbo v0, "poi_filtered_list"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    :goto_1
    move-object v1, v0

    :goto_2
    invoke-static {}, Lcom/twitter/android/composer/ComposerIntentWrapper;->p()Ljava/lang/Class;

    move-result-object v0

    const-class v3, Lcom/twitter/android/composer/TextFirstComposerActivity;

    if-ne v0, v3, :cond_2

    const v0, 0x7f090211    # com.twitter.android.R.id.holder

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    const/4 v0, 0x0

    invoke-virtual {p1, v5, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    const-string/jumbo v3, "search_footer_view"

    invoke-virtual {v0, v3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    const v3, 0x7f090214    # com.twitter.android.R.id.poi_item_view

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PoiItemView;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->f:Lcom/twitter/android/widget/PoiItemView;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->f:Lcom/twitter/android/widget/PoiItemView;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0b007b    # com.twitter.android.R.color.twitter_blue

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/widget/PoiItemView;->setHighlightTextColor(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    const v0, 0x7f090212    # com.twitter.android.R.id.query

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const v3, 0x7f0f030d    # com.twitter.android.R.string.poi_search_hint

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setHint(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    new-instance v3, Lcom/twitter/android/widget/bh;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/bh;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    new-instance v0, Lcom/twitter/android/widget/ct;

    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    new-instance v4, Lcom/twitter/android/widget/bi;

    invoke-direct {v4, p0}, Lcom/twitter/android/widget/bi;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    invoke-direct {v0, v3, v4}, Lcom/twitter/android/widget/ct;-><init>(Landroid/widget/TextView;Lcom/twitter/android/widget/cu;)V

    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v3, v0}, Landroid/widget/EditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    new-instance v3, Lcom/twitter/android/widget/bj;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/bj;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    new-instance v3, Lcom/twitter/android/widget/bk;

    invoke-direct {v3, p0}, Lcom/twitter/android/widget/bk;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Lcom/twitter/android/widget/bu;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    invoke-direct {v0, p0, v3, v5, v1}, Lcom/twitter/android/widget/bu;-><init>(Lcom/twitter/android/widget/PoiFragment;Landroid/content/Context;ILjava/util/ArrayList;)V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0900df    # com.twitter.android.R.id.root_layout

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ComposerLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->j:Lcom/twitter/android/widget/ComposerLayout;

    return-object v2

    :cond_3
    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    goto/16 :goto_1

    :cond_5
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    move-object v1, v0

    goto/16 :goto_2
.end method

.method static synthetic b(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;)Landroid/view/View;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/PoiFragment;->l:Landroid/view/View;

    return-object p1
.end method

.method private static b(FLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 8

    const/16 v7, 0xa

    const/4 v5, 0x1

    const/4 v6, 0x0

    const/high16 v1, 0x447a0000    # 1000.0f

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    const-string/jumbo v0, " "

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v2, v0}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    float-to-double v0, p0

    const-wide v3, 0x400a3d70a3d70a3dL    # 3.28

    mul-double/2addr v0, v3

    double-to-float v0, v0

    const/high16 v1, 0x44040000    # 528.0f

    cmpl-float v1, v0, v1

    if-lez v1, :cond_0

    const/high16 v1, 0x45a50000    # 5280.0f

    div-float v1, v0, v1

    const v0, 0x7f0f028c    # com.twitter.android.R.string.mile_abbr

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    const/high16 v3, 0x41200000    # 10.0f

    cmpg-float v3, v1, v3

    if-gez v3, :cond_1

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%.1f"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    invoke-static {v0, v7}, Lcom/twitter/android/widget/PoiFragment;->a(FI)F

    move-result v1

    const v0, 0x7f0f01b1    # com.twitter.android.R.string.foot_abbr

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v1, v1

    invoke-static {p1, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_2
    cmpl-float v0, p0, v1

    if-ltz v0, :cond_3

    div-float v1, p0, v1

    const v0, 0x7f0f01fb    # com.twitter.android.R.string.kilometer

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_2
    const/high16 v3, 0x3f800000    # 1.0f

    cmpg-float v3, v1, v3

    if-gez v3, :cond_4

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "%.1f"

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    aput-object v1, v5, v6

    invoke-static {v2, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_3
    invoke-static {p0, v7}, Lcom/twitter/android/widget/PoiFragment;->a(FI)F

    move-result v1

    const v0, 0x7f0f028b    # com.twitter.android.R.string.meter

    invoke-virtual {p1, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    float-to-int v1, v1

    invoke-static {p1, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1
.end method

.method private b(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v0, p3, v0

    if-ltz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v1}, Lcom/twitter/android/widget/bu;->getCount()I

    move-result v1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v4, "selected_place"

    iget-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v5, v0}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "selected_index"

    invoke-virtual {v3, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0
.end method

.method private b(Lcom/twitter/library/api/geo/TwitterPlace;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->C:Lcom/twitter/android/util/z;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/z;->b(Lcom/twitter/library/api/geo/TwitterPlace;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->notifyDataSetChanged()V

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/geo/TwitterPlace;I)Z

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/widget/PoiFragment;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->d()Z

    move-result v0

    return v0
.end method

.method private c()V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_0
    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    if-gtz v0, :cond_2

    iput-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->q:Lcom/twitter/android/widget/by;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    iget v2, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "compose:poi:poi_list:location:select"

    aput-object v3, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v2, v2, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->h()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v4

    invoke-static {v2, v3, v6, v4, v6}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-interface {v0, v1, v6}, Lcom/twitter/android/widget/bx;->a(Lcom/twitter/library/api/geo/TwitterPlace;Z)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/widget/PoiFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->x:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/ComposerLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->j:Lcom/twitter/android/widget/ComposerLayout;

    return-object v0
.end method

.method private d()Z
    .locals 9

    const/4 v1, 0x0

    const/4 v0, 0x1

    const/4 v8, -0x1

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v6

    invoke-direct {v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v6, v0, [Ljava/lang/String;

    const-string/jumbo v7, "compose:poi:poi_list::search"

    aput-object v7, v6, v1

    invoke-virtual {v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const/4 v5, 0x0

    invoke-static {v5, v4, v8, v8, v8}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const-class v4, Lcom/twitter/android/PoiSearchActivity;

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "search_text"

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/widget/PoiFragment;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method static synthetic e(Lcom/twitter/android/widget/PoiFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    return-object v0
.end method

.method private e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method static synthetic f(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/api/geo/TwitterPlace;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    return-object v0
.end method

.method private f()V
    .locals 7

    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/twitter/android/widget/PoiFragment;->s:Z

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->D:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "compose:poi:poi_list:location:results"

    aput-object v2, v1, v3

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->D:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/bz;

    iget-object v3, v0, Lcom/twitter/android/widget/bz;->a:Ljava/lang/String;

    iget-object v4, v0, Lcom/twitter/android/widget/bz;->b:Ljava/lang/String;

    iget v5, v0, Lcom/twitter/android/widget/bz;->c:I

    iget v6, v0, Lcom/twitter/android/widget/bz;->d:I

    iget v0, v0, Lcom/twitter/android/widget/bz;->e:I

    invoke-static {v3, v4, v5, v6, v0}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->D:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto :goto_0
.end method

.method private g()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->B:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method static synthetic g(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bx;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->k:Landroid/view/View;

    return-object v0
.end method

.method private h()Ljava/lang/String;
    .locals 2

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->v:Ljava/lang/String;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    if-nez v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic i(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bu;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/HashMap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->E:Ljava/util/HashMap;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->A:Lcom/twitter/library/platform/LocationProducer;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/widget/PoiFragment;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/widget/PoiFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/widget/PoiFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    return v0
.end method


# virtual methods
.method public a()Lcom/twitter/library/api/geo/TwitterPlace;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(F)V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v1, p1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    invoke-direct {p0, p2}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->C:Lcom/twitter/android/util/z;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/twitter/android/util/z;->b(Ljava/util/List;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/android/widget/bx;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    return-void
.end method

.method public a(Ljava/util/List;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2, p1}, Ljava/util/ArrayList;->addAll(ILjava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->removeAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->c()V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->notifyDataSetChanged()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public a(Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->A:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->e()Z

    move-result v0

    if-eqz p1, :cond_1

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    iget-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/bx;->a(Z)V

    :cond_0
    return-void

    :cond_1
    iput-boolean v4, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iput-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    iput-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->q:Lcom/twitter/android/widget/by;

    iput v4, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 9

    const v8, 0x7f020230    # com.twitter.android.R.drawable.ic_search_hint_dark

    const/4 v5, 0x1

    const/4 v7, -0x1

    const/4 v2, 0x0

    const/4 v6, 0x0

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v3

    invoke-direct {v1, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "compose:poi:poi_list::filter"

    aput-object v4, v3, v6

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v7, v7, v7}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->f:Lcom/twitter/android/widget/PoiItemView;

    const v3, 0x7f0f030e    # com.twitter.android.R.string.poi_server_search

    new-array v4, v5, [Ljava/lang/Object;

    aput-object p1, v4, v6

    invoke-virtual {v1, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/widget/PoiItemView;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const v1, 0x7f020219    # com.twitter.android.R.drawable.ic_recent_search_clear_x

    invoke-virtual {v0, v8, v6, v1, v6}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->getFilter()Landroid/widget/Filter;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/widget/Filter;->filter(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0, v8, v6, v6, v6}, Landroid/widget/EditText;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->e:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeFooterView(Landroid/view/View;)Z

    goto :goto_0
.end method

.method public b(Ljava/util/List;)V
    .locals 3

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    iget v2, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    sub-int v2, v0, v2

    invoke-virtual {v1, v2, v0}, Ljava/util/ArrayList;->subList(II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->clear()V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->c()V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->notifyDataSetChanged()V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public b(Z)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->i:Landroid/widget/ProgressBar;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    if-eqz p1, :cond_1

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->i:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    invoke-interface {v1}, Landroid/widget/ListAdapter;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->h:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public b()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/widget/PoiFragment;->t:I

    sub-int/2addr v0, v1

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public f(Z)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->s:Z

    iput-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->w:Z

    iput-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->x:Z

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    :cond_0
    return-void
.end method

.method public g(Z)V
    .locals 2

    const/4 v1, 0x1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->w:Z

    iput-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->x:Z

    iput-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->s:Z

    return-void
.end method

.method public h(Z)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    iput-boolean v5, p0, Lcom/twitter/android/widget/PoiFragment;->w:Z

    iput-boolean p1, p0, Lcom/twitter/android/widget/PoiFragment;->x:Z

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v1

    if-eqz p1, :cond_0

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "compose:poi:poi_list::expand"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->e()V

    iget-object v3, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->clearFocus()V

    new-array v3, v4, [Ljava/lang/String;

    const-string/jumbo v4, "compose:poi:poi_list::collapse"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 9

    const/4 v2, -0x1

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    if-ne p2, v2, :cond_1

    if-ne p1, v8, :cond_1

    if-eqz p3, :cond_1

    const-string/jumbo v0, "selected_place"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/geo/TwitterPlace;

    const-string/jumbo v1, "selected_index"

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v8, [Ljava/lang/String;

    const-string/jumbo v5, "compose:poi:poi_list:location:select"

    aput-object v5, v4, v7

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, v0, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->h()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v4, v5, v6, v1, v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;III)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iput-boolean v7, p0, Lcom/twitter/android/widget/PoiFragment;->s:Z

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    const-string/jumbo v3, ""

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    iget-object v2, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/bu;->a(Ljava/util/ArrayList;)V

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    invoke-interface {v1, v0, v8}, Lcom/twitter/android/widget/bx;->a(Lcom/twitter/library/api/geo/TwitterPlace;Z)V

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "location_enabled"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    const-string/jumbo v1, "poi_mode"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    const-string/jumbo v1, "search_text"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->v:Ljava/lang/String;

    :cond_0
    if-eqz p1, :cond_1

    const-string/jumbo v0, "location_on"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/LocationProducer;->a(Landroid/content/Context;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->A:Lcom/twitter/library/platform/LocationProducer;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->z:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->B:Lcom/twitter/library/client/aa;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 8

    const/4 v7, 0x1

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ao;->a:Landroid/net/Uri;

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->g()J

    move-result-wide v3

    invoke-static {v2, v3, v4}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/widget/bt;->a:[Ljava/lang/String;

    const-string/jumbo v4, "flag=?"

    new-array v5, v7, [Ljava/lang/String;

    const/4 v6, 0x0

    invoke-static {v7}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x0

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/PoiFragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/widget/PoiFragment;->b(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    :pswitch_1
    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v3, p3, v0

    if-lez v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-virtual {v0}, Lcom/twitter/android/widget/bu;->getCount()I

    move-result v0

    if-ge v3, v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->w:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->y:Lcom/twitter/android/widget/bu;

    invoke-direct {p0, v3}, Lcom/twitter/android/widget/PoiFragment;->b(I)I

    move-result v4

    invoke-virtual {v0, v4}, Lcom/twitter/android/widget/bu;->a(I)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v4

    iget-object v0, v4, Lcom/twitter/library/api/geo/TwitterPlace;->placeInfo:Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;

    iget-object v0, v0, Lcom/twitter/library/api/geo/TwitterPlace$PlaceInfo;->name:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->j:Lcom/twitter/android/widget/ComposerLayout;

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/twitter/android/widget/PoiFragment;->j:Lcom/twitter/android/widget/ComposerLayout;

    invoke-virtual {v5, v2}, Lcom/twitter/android/widget/ComposerLayout;->setDrawerDraggableState(Z)V

    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v0, v4, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    :cond_3
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v0, v4, Lcom/twitter/library/api/geo/TwitterPlace;->placeId:Ljava/lang/String;

    :cond_4
    iput-object p2, p0, Lcom/twitter/android/widget/PoiFragment;->k:Landroid/view/View;

    invoke-static {v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v2, 0x7f080012    # com.twitter.android.R.array.poi_report_items

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->f(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/widget/bm;

    invoke-direct {v2, p0, v4, v3}, Lcom/twitter/android/widget/bm;-><init>(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/library/api/geo/TwitterPlace;I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/widget/bl;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/bl;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    move v0, v1

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/widget/PoiFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->o:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public onResume()V
    .locals 12

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->A:Lcom/twitter/library/platform/LocationProducer;

    invoke-virtual {v0}, Lcom/twitter/library/platform/LocationProducer;->a()Landroid/location/Location;

    move-result-object v5

    if-eqz v5, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->b()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->B:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/widget/ca;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/ca;-><init>(Lcom/twitter/android/widget/PoiFragment;)V

    invoke-virtual {v5}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-virtual {v5}, Landroid/location/Location;->getLongitude()D

    move-result-wide v5

    const/4 v7, 0x0

    const/16 v8, 0xc8

    const-string/jumbo v9, "poi"

    const/4 v10, 0x0

    iget-object v11, p0, Lcom/twitter/android/widget/PoiFragment;->v:Ljava/lang/String;

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/client/ao;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "location_on"

    iget-boolean v1, p0, Lcom/twitter/android/widget/PoiFragment;->r:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "poi_list"

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    iget v0, p0, Lcom/twitter/android/widget/PoiFragment;->u:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "poi_selected"

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->p:Lcom/twitter/library/api/geo/TwitterPlace;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "poi_filtered_list"

    iget-object v1, p0, Lcom/twitter/android/widget/PoiFragment;->n:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v1}, Landroid/widget/AbsListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/bx;->b(Z)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->c:Lcom/twitter/android/widget/bx;

    invoke-interface {v0, v1}, Lcom/twitter/android/widget/bx;->b(Z)V

    goto :goto_0
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->f()V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public t()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->e()V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->x:Z

    iget-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->s:Z

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/widget/PoiFragment;->f()V

    :cond_1
    return-void
.end method

.method public u()V
    .locals 2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/widget/PoiFragment;->w:Z

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->b:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->cancelLongPress()V

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/PoiFragment;->l:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setPressed(Z)V

    :cond_0
    return-void
.end method
