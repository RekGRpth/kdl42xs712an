.class Lcom/twitter/android/widget/bm;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field final synthetic a:Lcom/twitter/library/api/geo/TwitterPlace;

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/widget/PoiFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/library/api/geo/TwitterPlace;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    iput-object p2, p0, Lcom/twitter/android/widget/bm;->a:Lcom/twitter/library/api/geo/TwitterPlace;

    iput p3, p0, Lcom/twitter/android/widget/bm;->b:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 9

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v8, 0x0

    if-nez p3, :cond_4

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->a:Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v1, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->f(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/library/api/geo/TwitterPlace;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/geo/TwitterPlace;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/library/api/geo/TwitterPlace;)Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0, v2}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Lcom/twitter/android/widget/by;)Lcom/twitter/android/widget/by;

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->g(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bx;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->g(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/bx;

    move-result-object v0

    invoke-interface {v0, v2, v8}, Lcom/twitter/android/widget/bx;->a(Lcom/twitter/library/api/geo/TwitterPlace;Z)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->e(Lcom/twitter/android/widget/PoiFragment;)Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090214    # com.twitter.android.R.id.poi_item_view

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/widget/PoiItemView;

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0}, Lcom/twitter/android/widget/PoiFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v0, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/PoiItemView;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;

    move-result-object v0

    const v2, 0x7f0b0004    # com.twitter.android.R.color.border_gray

    invoke-virtual {v5, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    const/16 v2, 0x12c

    new-instance v3, Lcom/twitter/android/widget/bn;

    invoke-direct {v3, p0, v5, v1}, Lcom/twitter/android/widget/bn;-><init>(Lcom/twitter/android/widget/bm;Landroid/content/res/Resources;Lcom/twitter/android/widget/PoiItemView;)V

    iget-object v5, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v5}, Lcom/twitter/android/widget/PoiFragment;->h(Lcom/twitter/android/widget/PoiFragment;)Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getWidth()I

    move-result v5

    neg-int v5, v5

    int-to-float v5, v5

    move v6, v4

    move v7, v4

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/widget/PoiFragment;->a(Lcom/twitter/android/widget/PoiFragment;Landroid/view/View;ILjava/lang/Runnable;FFFFZ)V

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->d(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/ComposerLayout;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->d(Lcom/twitter/android/widget/PoiFragment;)Lcom/twitter/android/widget/ComposerLayout;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->e(Lcom/twitter/android/widget/PoiFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/widget/bm;->c:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/PoiFragment;->e(Lcom/twitter/android/widget/PoiFragment;)Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v8}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v8, 0x1

    :cond_3
    invoke-virtual {v0, v8}, Lcom/twitter/android/widget/ComposerLayout;->setDrawerDraggableState(Z)V

    goto :goto_0

    :cond_4
    invoke-interface {p1}, Landroid/content/DialogInterface;->cancel()V

    goto :goto_0
.end method
