.class Lcom/twitter/android/widget/bf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Z

.field final synthetic c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;IZ)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    iput p2, p0, Lcom/twitter/android/widget/bf;->a:I

    iput-boolean p3, p0, Lcom/twitter/android/widget/bf;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    if-nez p2, :cond_2

    iget v0, p0, Lcom/twitter/android/widget/bf;->a:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    iget v1, p0, Lcom/twitter/android/widget/bf;->a:I

    invoke-static {v0, v1}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;I)I

    iget-object v0, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->c(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    invoke-static {v1}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;)Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    invoke-static {v2}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->b(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    iget-boolean v0, p0, Lcom/twitter/android/widget/bf;->b:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "unsubscribe"

    :goto_0
    invoke-static {v1, v0}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    const-string/jumbo v0, "subscribe"

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    if-ne v0, p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/bf;->c:Lcom/twitter/android/widget/NotificationSettingsDialogFragment;

    const-string/jumbo v1, "cancel"

    invoke-static {v0, v1}, Lcom/twitter/android/widget/NotificationSettingsDialogFragment;->a(Lcom/twitter/android/widget/NotificationSettingsDialogFragment;Ljava/lang/String;)V

    goto :goto_1
.end method
