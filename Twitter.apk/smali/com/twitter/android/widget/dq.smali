.class Lcom/twitter/android/widget/dq;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/TwitterAccessPreference;


# direct methods
.method private constructor <init>(Lcom/twitter/android/widget/TwitterAccessPreference;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/widget/TwitterAccessPreference;Lcom/twitter/android/widget/dp;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/dq;-><init>(Lcom/twitter/android/widget/TwitterAccessPreference;)V

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/String;)Ljava/lang/Boolean;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/TwitterAccessPreference;->a:Landroid/widget/RadioGroup;

    invoke-virtual {v0}, Landroid/widget/RadioGroup;->getCheckedRadioButtonId()I

    move-result v0

    const v3, 0x7f0902b2    # com.twitter.android.R.id.twitter_access_on

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    aget-object v3, p1, v2

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    if-eqz v0, :cond_5

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-virtual {v5}, Lcom/twitter/android/widget/TwitterAccessPreference;->getContext()Landroid/content/Context;

    move-result-object v5

    const v6, 0x7f0f028d    # com.twitter.android.R.string.mobile_config_url

    invoke-virtual {v5, v6}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "&carrier="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Ljava/lang/String;)Lcom/twitter/library/api/ad;

    move-result-object v3

    if-eqz v3, :cond_4

    iget-object v4, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-virtual {v4}, Lcom/twitter/android/widget/TwitterAccessPreference;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-static {v5}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Lcom/twitter/android/widget/TwitterAccessPreference;)Landroid/content/SharedPreferences;

    move-result-object v5

    invoke-virtual {v4, v3, v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/ad;Landroid/content/SharedPreferences;)V

    move v3, v1

    :goto_2
    if-eqz v0, :cond_2

    if-eqz v3, :cond_3

    :cond_2
    move v2, v1

    :cond_3
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v3, v2

    goto :goto_2

    :cond_5
    move v3, v2

    goto :goto_2
.end method

.method protected a(Ljava/lang/Boolean;)V
    .locals 8

    const/16 v7, 0x30

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-static {v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Lcom/twitter/android/widget/TwitterAccessPreference;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TwitterAccessPreference;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    const-string/jumbo v3, "twitter_access_carrier"

    iget-object v4, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v4, v4, Lcom/twitter/android/widget/TwitterAccessPreference;->c:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "TwitterAccess set for "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v4, v4, Lcom/twitter/android/widget/TwitterAccessPreference;->c:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v7, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TwitterAccessPreference;->getDialog()Landroid/app/Dialog;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/app/Dialog;->dismiss()V

    :cond_0
    :goto_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    iget-object v1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v2, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    invoke-static {v2, v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->a(Lcom/twitter/android/widget/TwitterAccessPreference;Landroid/content/SharedPreferences;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/TwitterAccessPreference;->setSummary(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    const-string/jumbo v3, "twitter_access_carrier"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v3, "Invalid carrier for TwitterAccess."

    invoke-static {v1, v3, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1, v7, v5, v5}, Landroid/widget/Toast;->setGravity(III)V

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v1, v1, Lcom/twitter/android/widget/TwitterAccessPreference;->d:Landroid/widget/LinearLayout;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v1, v1, Lcom/twitter/android/widget/TwitterAccessPreference;->e:Landroid/widget/RadioGroup;

    invoke-virtual {v1, v5}, Landroid/widget/RadioGroup;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v1, v1, Lcom/twitter/android/widget/TwitterAccessPreference;->b:Landroid/widget/Button;

    invoke-virtual {v1, v5}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/String;

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/dq;->a([Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/widget/dq;->a(Ljava/lang/Boolean;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    const/16 v1, 0x8

    iget-object v0, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/TwitterAccessPreference;->e:Landroid/widget/RadioGroup;

    invoke-virtual {v0, v1}, Landroid/widget/RadioGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/TwitterAccessPreference;->b:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/dq;->a:Lcom/twitter/android/widget/TwitterAccessPreference;

    iget-object v0, v0, Lcom/twitter/android/widget/TwitterAccessPreference;->d:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    return-void
.end method
