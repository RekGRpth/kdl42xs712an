.class public Lcom/twitter/android/widget/VariableHeightLayout;
.super Landroid/widget/RelativeLayout;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 3

    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    div-int/lit8 v1, v0, 0x2

    const/high16 v2, -0x80000000

    invoke-static {v1, v2}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-super {p0, p1, v2}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/widget/VariableHeightLayout;->setMeasuredDimension(II)V

    return-void
.end method
