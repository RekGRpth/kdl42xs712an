.class public Lcom/twitter/android/widget/dt;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:Ljava/lang/String;

.field private static c:Z

.field private static d:J


# instance fields
.field private e:Z

.field private f:Lcom/twitter/android/client/c;

.field private g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

.field private h:Landroid/view/View;

.field private i:Landroid/view/View;

.field private j:Lcom/twitter/android/widget/Divot;

.field private k:Landroid/view/animation/Animation;

.field private l:Z

.field private m:Lcom/twitter/android/widget/dz;

.field private n:Lcom/twitter/android/widget/dy;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const-string/jumbo v0, "vit_filter_prompt"

    sput-object v0, Lcom/twitter/android/widget/dt;->a:Ljava/lang/String;

    const-string/jumbo v0, "dismiss"

    sput-object v0, Lcom/twitter/android/widget/dt;->b:Ljava/lang/String;

    const/4 v0, 0x0

    sput-boolean v0, Lcom/twitter/android/widget/dt;->c:Z

    const-wide/16 v0, -0x1

    sput-wide v0, Lcom/twitter/android/widget/dt;->d:J

    return-void
.end method

.method public constructor <init>(Lcom/twitter/android/client/c;Lcom/twitter/android/NotificationsBaseTimelineActivity;Landroid/view/View;)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/twitter/android/widget/dt;->l:Z

    iput-object v1, p0, Lcom/twitter/android/widget/dt;->m:Lcom/twitter/android/widget/dz;

    iput-object v1, p0, Lcom/twitter/android/widget/dt;->n:Lcom/twitter/android/widget/dy;

    iput-boolean v0, p0, Lcom/twitter/android/widget/dt;->e:Z

    iput-object p1, p0, Lcom/twitter/android/widget/dt;->f:Lcom/twitter/android/client/c;

    iput-object p2, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    iput-object p3, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    const v1, 0x7f0902d0    # com.twitter.android.R.id.vit_filter_prompt

    invoke-virtual {v0, v1}, Lcom/twitter/android/NotificationsBaseTimelineActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->d()V

    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->e()V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const v1, 0x7f0902d4    # com.twitter.android.R.id.vit_filter_prompt_close_button

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/widget/du;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/du;-><init>(Lcom/twitter/android/widget/dt;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const v1, 0x7f0902d6    # com.twitter.android.R.id.vit_filter_prompt_submit_button

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/twitter/android/widget/dv;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/dv;-><init>(Lcom/twitter/android/widget/dt;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const v1, 0x7f0902d1    # com.twitter.android.R.id.vit_filter_prompt_divot

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/Divot;

    iput-object v0, p0, Lcom/twitter/android/widget/dt;->j:Lcom/twitter/android/widget/Divot;

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    const v1, 0x7f04001c    # com.twitter.android.R.anim.vit_filter_prompt_target_button

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/dt;->k:Landroid/view/animation/Animation;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/widget/dt;)Lcom/twitter/android/widget/dz;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->m:Lcom/twitter/android/widget/dz;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/widget/dt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/dt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v4, "notification:universal:vit_filter_prompt:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ":"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    if-eqz p3, :cond_0

    invoke-virtual {v0, p3}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/widget/dt;->f:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 4

    const/4 v0, 0x0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/client/f;

    sget-object v3, Lcom/twitter/android/widget/dt;->a:Ljava/lang/String;

    invoke-direct {v2, p0, v1, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->ax()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/widget/dt;->b:Ljava/lang/String;

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method static synthetic b(Lcom/twitter/android/widget/dt;)Lcom/twitter/android/NotificationsBaseTimelineActivity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    return-object v0
.end method

.method public static b(J)Z
    .locals 2

    sget-boolean v0, Lcom/twitter/android/widget/dt;->c:Z

    if-eqz v0, :cond_0

    sget-wide v0, Lcom/twitter/android/widget/dt;->d:J

    cmp-long v0, v0, p0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/widget/dt;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    return-object v0
.end method

.method private d()V
    .locals 4

    const/4 v3, 0x1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aw()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    invoke-static {v0}, Lcom/twitter/android/util/ah;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    invoke-static {v0, v3}, Lcom/twitter/android/util/ah;->a(Landroid/content/Context;I)V

    const-string/jumbo v0, ""

    const-string/jumbo v1, "change"

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/widget/dt;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-boolean v3, p0, Lcom/twitter/android/widget/dt;->e:Z

    :cond_0
    return-void
.end method

.method private e()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const v1, 0x7f0902d3    # com.twitter.android.R.id.vit_filter_prompt_title

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const v2, 0x7f0902d5    # com.twitter.android.R.id.vit_filter_prompt_description

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const v3, 0x7f0902d6    # com.twitter.android.R.id.vit_filter_prompt_submit_button

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iget-boolean v3, p0, Lcom/twitter/android/widget/dt;->e:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0f055b    # com.twitter.android.R.string.vit_filter_prompt_default_on_title

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0f055a    # com.twitter.android.R.string.vit_filter_prompt_default_on_description

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0f0559    # com.twitter.android.R.string.vit_filter_prompt_default_on_button

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    :goto_0
    return-void

    :cond_0
    const v3, 0x7f0f0558    # com.twitter.android.R.string.vit_filter_prompt_default_off_title

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0f0557    # com.twitter.android.R.string.vit_filter_prompt_default_off_description

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    const v0, 0x7f0f0556    # com.twitter.android.R.string.vit_filter_prompt_default_off_button

    invoke-virtual {v2, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method private f()V
    .locals 5

    const/4 v3, 0x2

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->j:Lcom/twitter/android/widget/Divot;

    invoke-virtual {v0}, Lcom/twitter/android/widget/Divot;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    new-array v2, v3, [I

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->getLocationOnScreen([I)V

    new-array v3, v3, [I

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->j:Lcom/twitter/android/widget/Divot;

    invoke-virtual {v1}, Lcom/twitter/android/widget/Divot;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->getLocationOnScreen([I)V

    aget v1, v2, v4

    aget v2, v3, v4

    sub-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    iget v3, v0, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    sub-int/2addr v2, v3

    div-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->j:Lcom/twitter/android/widget/Divot;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/Divot;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private g()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/widget/dt;->l:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/widget/dt;->b()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private h()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->n:Lcom/twitter/android/widget/dy;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->n:Lcom/twitter/android/widget/dy;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/dt;->n:Lcom/twitter/android/widget/dy;

    :cond_0
    return-void
.end method


# virtual methods
.method public a()V
    .locals 4

    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->h()V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/widget/dx;

    iget-object v2, p0, Lcom/twitter/android/widget/dt;->j:Lcom/twitter/android/widget/Divot;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/android/widget/dx;-><init>(Lcom/twitter/android/widget/dt;Landroid/view/View;Lcom/twitter/android/widget/du;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->k:Landroid/view/animation/Animation;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->k:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/dt;->c()V

    goto :goto_0
.end method

.method public a(J)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->g()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->h()V

    new-instance v0, Lcom/twitter/android/widget/dy;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/widget/dy;-><init>(Lcom/twitter/android/widget/dt;Lcom/twitter/android/widget/du;)V

    iput-object v0, p0, Lcom/twitter/android/widget/dt;->n:Lcom/twitter/android/widget/dy;

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->n:Lcom/twitter/android/widget/dy;

    invoke-virtual {v0, v1, p1, p2}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/widget/dz;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/dt;->m:Lcom/twitter/android/widget/dz;

    return-void
.end method

.method public a(Z)V
    .locals 4

    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->g()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/widget/dt;->f()V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/widget/dw;

    iget-object v2, p0, Lcom/twitter/android/widget/dt;->j:Lcom/twitter/android/widget/Divot;

    const/4 v3, 0x0

    invoke-direct {v1, p0, v2, v3}, Lcom/twitter/android/widget/dw;-><init>(Lcom/twitter/android/widget/dt;Landroid/view/View;Lcom/twitter/android/widget/du;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->k:Landroid/view/animation/Animation;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setRepeatCount(I)V

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->i:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/widget/dt;->k:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_1
    const/4 v0, 0x1

    sput-boolean v0, Lcom/twitter/android/widget/dt;->c:Z

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    sput-wide v0, Lcom/twitter/android/widget/dt;->d:J

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method public b()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->h:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public c()V
    .locals 5

    const/4 v4, 0x1

    iput-boolean v4, p0, Lcom/twitter/android/widget/dt;->l:Z

    iget-object v0, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    iget-object v2, p0, Lcom/twitter/android/widget/dt;->g:Lcom/twitter/android/NotificationsBaseTimelineActivity;

    sget-object v3, Lcom/twitter/android/widget/dt;->a:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/widget/dt;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void
.end method
