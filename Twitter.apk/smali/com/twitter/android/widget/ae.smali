.class Lcom/twitter/android/widget/ae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/GeoDeciderFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/widget/GeoDeciderFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 0

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 5

    const/4 v4, -0x1

    invoke-static {}, Lcom/twitter/android/widget/GeoDeciderFragment;->b()[I

    move-result-object v0

    invoke-virtual {p1}, Landroid/widget/SeekBar;->getProgress()I

    move-result v1

    aget v0, v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v2, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v2, v2, Lcom/twitter/android/widget/GeoDeciderFragment;->b:Landroid/widget/TextView;

    const-string/jumbo v3, "Update Interval"

    invoke-static {v1, v2, v3, v0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Lcom/twitter/android/widget/GeoDeciderFragment;Landroid/widget/TextView;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v1, v1, Lcom/twitter/android/widget/GeoDeciderFragment;->c:Lcom/twitter/library/platform/LocationProducer;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v1, v1, Lcom/twitter/android/widget/GeoDeciderFragment;->c:Lcom/twitter/library/platform/LocationProducer;

    iget-object v2, p0, Lcom/twitter/android/widget/ae;->a:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-static {v2}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Lcom/twitter/android/widget/GeoDeciderFragment;)Landroid/widget/CompoundButton;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/CompoundButton;->isChecked()Z

    move-result v2

    invoke-virtual {v1, v2, v4, v4, v0}, Lcom/twitter/library/platform/LocationProducer;->a(ZIII)V

    :cond_0
    return-void
.end method
