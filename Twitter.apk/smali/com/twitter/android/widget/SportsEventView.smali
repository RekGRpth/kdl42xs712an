.class public Lcom/twitter/android/widget/SportsEventView;
.super Lcom/twitter/android/widget/EventView;
.source "Twttr"


# instance fields
.field private b:Landroid/widget/TextView;

.field private c:Landroid/widget/TextView;

.field private d:Lcom/twitter/android/widget/SportsTeamRowView;

.field private e:Lcom/twitter/android/widget/SportsTeamRowView;

.field private f:Landroid/widget/LinearLayout;

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/widget/EventView;-><init>(Landroid/content/Context;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/widget/EventView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/widget/EventView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    return-void
.end method


# virtual methods
.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 23

    const/16 v22, 0x0

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move-wide/from16 v11, p9

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    move/from16 v17, p15

    move/from16 v18, p16

    move-object/from16 v19, p17

    move-object/from16 v20, p18

    move-object/from16 v21, p19

    invoke-super/range {v2 .. v22}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p14 .. p14}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    iget-object v3, v2, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->players:Ljava/util/ArrayList;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/SportsEventView;->d:Lcom/twitter/android/widget/SportsTeamRowView;

    move-object/from16 v0, p11

    invoke-virtual {v4, v3, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;Lcom/twitter/library/widget/ap;)V

    iget-object v3, v2, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->players:Ljava/util/ArrayList;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/SportsEventView;->e:Lcom/twitter/android/widget/SportsTeamRowView;

    move-object/from16 v0, p11

    invoke-virtual {v4, v3, v0}, Lcom/twitter/android/widget/SportsTeamRowView;->a(Lcom/twitter/library/api/TwitterTopic$SportsEvent$Player;Lcom/twitter/library/widget/ap;)V

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/widget/SportsEventView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    move-object/from16 v0, p7

    move/from16 v1, p8

    invoke-static {v0, v1, v3}, Lcom/twitter/android/widget/SportsEventView;->a(Ljava/lang/String;ILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/SportsEventView;->c:Landroid/widget/TextView;

    invoke-static {v5, v4}, Lcom/twitter/android/widget/SportsEventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/widget/SportsEventView;->b:Landroid/widget/TextView;

    iget-object v2, v2, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->status:Ljava/lang/String;

    invoke-static {v4, v2}, Lcom/twitter/android/widget/SportsEventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/SportsEventView;->f:Landroid/widget/LinearLayout;

    if-eqz v2, :cond_0

    if-eqz p16, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/SportsEventView;->f:Landroid/widget/LinearLayout;

    const v4, 0x7f0202e4    # com.twitter.android.R.drawable.sports_background

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    move-object/from16 v0, p0

    iget v2, v0, Lcom/twitter/android/widget/SportsEventView;->g:I

    :goto_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/widget/SportsEventView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2, v2, v2, v2}, Landroid/widget/LinearLayout;->setPadding(IIII)V

    :cond_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/widget/SportsEventView;->f:Landroid/widget/LinearLayout;

    const v4, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V
    .locals 2

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V

    invoke-virtual {p0}, Lcom/twitter/android/widget/SportsEventView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {p3, p4, v0}, Lcom/twitter/android/widget/SportsEventView;->a(Ljava/lang/String;ILandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/widget/SportsEventView;->c:Landroid/widget/TextView;

    invoke-static {v1, v0}, Lcom/twitter/android/widget/SportsEventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    invoke-static {p5}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;

    iget-object v1, p0, Lcom/twitter/android/widget/SportsEventView;->b:Landroid/widget/TextView;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterTopic$SportsEvent;->status:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/twitter/android/widget/SportsEventView;->a(Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/widget/EventView;->onFinishInflate()V

    const v0, 0x7f090160    # com.twitter.android.R.id.game_time

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsEventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->b:Landroid/widget/TextView;

    const v0, 0x7f090162    # com.twitter.android.R.id.tweet_count

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsEventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->c:Landroid/widget/TextView;

    const v0, 0x7f09015e    # com.twitter.android.R.id.team_one_row

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsEventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SportsTeamRowView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->d:Lcom/twitter/android/widget/SportsTeamRowView;

    const v0, 0x7f09015f    # com.twitter.android.R.id.team_two_row

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsEventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/SportsTeamRowView;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->e:Lcom/twitter/android/widget/SportsTeamRowView;

    const v0, 0x7f090161    # com.twitter.android.R.id.sport_content

    invoke-virtual {p0, v0}, Lcom/twitter/android/widget/SportsEventView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->f:Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/twitter/android/widget/SportsEventView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0c00c2    # com.twitter.android.R.dimen.sports_margin

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    invoke-static {v1, v2, v0}, Landroid/util/TypedValue;->applyDimension(IFLandroid/util/DisplayMetrics;)F

    move-result v0

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/widget/SportsEventView;->g:I

    return-void
.end method

.method protected setEventImage(Landroid/graphics/Bitmap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->a:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/widget/SportsEventView;->a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;Z)V

    return-void
.end method

.method public setEventImages(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->d:Lcom/twitter/android/widget/SportsTeamRowView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/SportsTeamRowView;->getLogoKey()Lcom/twitter/library/util/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/SportsEventView;->d:Lcom/twitter/android/widget/SportsTeamRowView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/SportsTeamRowView;->getLogoView()Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/widget/SportsEventView;->e:Lcom/twitter/android/widget/SportsTeamRowView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/SportsTeamRowView;->getLogoKey()Lcom/twitter/library/util/m;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/widget/SportsEventView;->e:Lcom/twitter/android/widget/SportsTeamRowView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/SportsTeamRowView;->getLogoView()Landroid/widget/ImageView;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/widget/EventView;->setEventImages(Ljava/util/HashMap;)V

    return-void
.end method
