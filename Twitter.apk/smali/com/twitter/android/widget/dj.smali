.class public Lcom/twitter/android/widget/dj;
.super Landroid/support/v4/widget/ExploreByTouchHelper;
.source "Twttr"


# instance fields
.field a:Landroid/widget/TextView;

.field b:Landroid/view/ViewGroup;

.field c:Lcom/twitter/android/widget/TweetDetailView;

.field private d:Lcom/twitter/library/api/TweetEntities;

.field private e:Ljava/util/List;

.field private final f:Landroid/graphics/Paint;

.field private g:Ljava/util/Comparator;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/twitter/android/widget/TweetDetailView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-direct {p0, p1}, Landroid/support/v4/widget/ExploreByTouchHelper;-><init>(Landroid/view/View;)V

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/dj;->f:Landroid/graphics/Paint;

    iget-object v0, p2, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-static {p2}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/android/widget/TweetDetailView;)Landroid/view/ViewGroup;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/dj;->b:Landroid/view/ViewGroup;

    iput-object p2, p0, Lcom/twitter/android/widget/dj;->c:Lcom/twitter/android/widget/TweetDetailView;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    new-instance v1, Lcom/twitter/android/widget/dk;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/dk;-><init>(Lcom/twitter/android/widget/dj;)V

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    :cond_0
    new-instance v0, Lcom/twitter/android/widget/dl;

    invoke-direct {v0, p0}, Lcom/twitter/android/widget/dl;-><init>(Lcom/twitter/android/widget/dj;)V

    iput-object v0, p0, Lcom/twitter/android/widget/dj;->g:Ljava/util/Comparator;

    return-void
.end method

.method private a()Landroid/graphics/Point;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getLeft()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLeft()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->c:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TweetDetailView;->getLeft()I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTop()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/widget/dj;->b:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    iget-object v2, p0, Lcom/twitter/android/widget/dj;->c:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v2}, Lcom/twitter/android/widget/TweetDetailView;->getTop()I

    move-result v2

    add-int/2addr v1, v2

    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2, v0, v1}, Landroid/graphics/Point;-><init>(II)V

    return-object v2
.end method

.method private a(Ljava/lang/String;Ljava/util/ArrayList;Z)V
    .locals 18

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    invoke-virtual {v1, v6}, Landroid/text/Layout;->getLineStart(I)I

    move-result v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineStart(I)I

    move-result v4

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget v1, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    :goto_0
    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    move v7, v5

    move v11, v6

    move v5, v4

    move v4, v3

    move v3, v1

    :goto_1
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Lcom/twitter/library/api/Entity;

    if-nez p3, :cond_4

    :goto_2
    iget v1, v8, Lcom/twitter/library/api/Entity;->start:I

    if-le v1, v3, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v4, v1, :cond_4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget v3, v1, Lcom/twitter/library/api/UrlEntity;->end:I

    iget v6, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    sub-int/2addr v3, v6

    iget v6, v1, Lcom/twitter/library/api/UrlEntity;->displayEnd:I

    iget v1, v1, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    sub-int v1, v6, v1

    sub-int v1, v3, v1

    add-int/2addr v2, v1

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v4, v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v1, v1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget v3, v1, Lcom/twitter/library/api/UrlEntity;->start:I

    goto :goto_2

    :cond_2
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    goto :goto_0

    :cond_3
    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v3

    goto :goto_2

    :cond_4
    move v12, v2

    move v9, v3

    move v10, v4

    iget v1, v8, Lcom/twitter/library/api/Entity;->start:I

    sub-int v6, v1, v12

    if-eqz p3, :cond_5

    move-object v1, v8

    check-cast v1, Lcom/twitter/library/api/UrlEntity;

    iget-object v1, v1, Lcom/twitter/library/api/UrlEntity;->displayUrl:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v1, v6

    move v15, v1

    :goto_3
    if-ltz v6, :cond_8

    invoke-virtual/range {p1 .. p1}, Ljava/lang/String;->length()I

    move-result v1

    if-gt v15, v1, :cond_8

    move v13, v7

    move v14, v11

    move v11, v5

    :goto_4
    if-lt v6, v11, :cond_6

    add-int/lit8 v14, v14, 0x1

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    add-int/lit8 v2, v14, 0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineStart(I)I

    move-result v5

    move v13, v11

    move v11, v5

    goto :goto_4

    :cond_5
    iget v1, v8, Lcom/twitter/library/api/Entity;->end:I

    sub-int/2addr v1, v12

    move v15, v1

    goto :goto_3

    :cond_6
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v13, v6}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v4

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->f:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v6, v15}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;II)F

    move-result v5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getLineHeight()I

    move-result v1

    mul-int v3, v14, v1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    move-object/from16 v17, v0

    new-instance v1, Lcom/twitter/android/widget/dm;

    float-to-int v2, v4

    add-float/2addr v4, v5

    float-to-int v4, v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getLineHeight()I

    move-result v5

    add-int/2addr v5, v3

    move-object/from16 v0, p1

    invoke-virtual {v0, v6, v15}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v7

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/widget/dm;-><init>(IIIIILjava/lang/String;)V

    move-object/from16 v0, v17

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    if-eqz p3, :cond_7

    check-cast v8, Lcom/twitter/library/api/UrlEntity;

    iget v1, v8, Lcom/twitter/library/api/UrlEntity;->end:I

    iget v2, v8, Lcom/twitter/library/api/UrlEntity;->start:I

    sub-int/2addr v1, v2

    iget v2, v8, Lcom/twitter/library/api/UrlEntity;->displayEnd:I

    iget v3, v8, Lcom/twitter/library/api/UrlEntity;->displayStart:I

    sub-int/2addr v2, v3

    sub-int/2addr v1, v2

    add-int/2addr v12, v1

    move v1, v12

    move v2, v11

    move v3, v13

    move v4, v14

    :goto_5
    move v5, v2

    move v7, v3

    move v11, v4

    move v2, v1

    move v3, v9

    move v4, v10

    goto/16 :goto_1

    :cond_7
    move v1, v12

    move v2, v11

    move v3, v13

    move v4, v14

    goto :goto_5

    :cond_8
    move v1, v12

    move v2, v5

    move v3, v7

    move v4, v11

    goto :goto_5
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/TweetEntities;)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->d:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->f:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->f:Landroid/graphics/Paint;

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/widget/dj;->a(Ljava/lang/String;Ljava/util/ArrayList;Z)V

    iget-object v1, p1, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/widget/dj;->a(Ljava/lang/String;Ljava/util/ArrayList;Z)V

    iget-object v1, p1, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/widget/dj;->a(Ljava/lang/String;Ljava/util/ArrayList;Z)V

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->g:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto :goto_0
.end method

.method protected getVirtualViewAt(FF)I
    .locals 7

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/android/widget/dj;->a()Landroid/graphics/Point;

    move-result-object v0

    iget v1, v0, Landroid/graphics/Point;->x:I

    int-to-float v1, v1

    sub-float v3, p1, v1

    iget v0, v0, Landroid/graphics/Point;->y:I

    int-to-float v0, v0

    sub-float v4, p2, v0

    cmpl-float v0, v3, v5

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getWidth()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v3, v0

    if-gtz v0, :cond_1

    cmpl-float v0, v4, v5

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getHeight()I

    move-result v0

    int-to-float v0, v0

    cmpg-float v0, v4, v0

    if-gtz v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_3

    move v1, v2

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/dm;

    float-to-int v5, v3

    float-to-int v6, v4

    invoke-virtual {v0, v5, v6}, Lcom/twitter/android/widget/dm;->a(II)Z

    move-result v0

    if-eqz v0, :cond_2

    add-int/lit8 v2, v1, 0x1

    :cond_0
    :goto_2
    return v2

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    const/high16 v2, -0x80000000

    goto :goto_2
.end method

.method protected getVisibleVirtualViews(Ljava/util/List;)V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    if-nez v1, :cond_1

    const/high16 v0, -0x80000000

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :cond_1
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    add-int/lit8 v1, v0, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p1, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method protected onPerformActionForVirtualView(IILandroid/os/Bundle;)Z
    .locals 3

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/widget/dj;->c:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->n:Lcom/twitter/library/view/c;

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    add-int/lit8 v2, p1, -0x1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/dm;

    iget-object v0, v0, Lcom/twitter/android/widget/dm;->c:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/twitter/library/view/c;->b(Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_0
    .end packed-switch
.end method

.method protected onPopulateEventForVirtualView(ILandroid/view/accessibility/AccessibilityEvent;)V
    .locals 1

    const-string/jumbo v0, ""

    invoke-virtual {p2, v0}, Landroid/view/accessibility/AccessibilityEvent;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void
.end method

.method protected onPopulateNodeForVirtualView(ILandroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;)V
    .locals 10

    const/4 v8, 0x1

    const/4 v7, 0x0

    invoke-direct {p0}, Lcom/twitter/android/widget/dj;->a()Landroid/graphics/Point;

    move-result-object v3

    if-nez p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v0

    if-eqz v0, :cond_0

    :goto_0
    new-instance v1, Landroid/graphics/Rect;

    iget v2, v3, Landroid/graphics/Point;->x:I

    iget v4, v3, Landroid/graphics/Point;->y:I

    iget v5, v3, Landroid/graphics/Point;->x:I

    iget-object v6, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getWidth()I

    move-result v6

    add-int/2addr v5, v6

    add-int/lit8 v5, v5, 0x1

    iget v3, v3, Landroid/graphics/Point;->y:I

    iget-object v6, p0, Lcom/twitter/android/widget/dj;->a:Landroid/widget/TextView;

    invoke-virtual {v6}, Landroid/widget/TextView;->getHeight()I

    move-result v6

    add-int/2addr v3, v6

    add-int/lit8 v3, v3, 0x1

    invoke-direct {v1, v2, v4, v5, v3}, Landroid/graphics/Rect;-><init>(IIII)V

    move-object v9, v1

    move-object v1, v0

    move-object v0, v9

    :goto_1
    invoke-virtual {p2, v1}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/graphics/Rect;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    :goto_2
    return-void

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/widget/dj;->e:Ljava/util/List;

    add-int/lit8 v1, p1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/dm;

    iget-object v2, v0, Lcom/twitter/android/widget/dm;->c:Ljava/lang/String;

    new-instance v1, Landroid/graphics/Rect;

    iget-object v0, v0, Lcom/twitter/android/widget/dm;->b:Landroid/graphics/Rect;

    invoke-direct {v1, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    iget v0, v3, Landroid/graphics/Point;->x:I

    iget v3, v3, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v0, v3}, Landroid/graphics/Rect;->offset(II)V

    const/16 v0, 0x10

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->addAction(I)V

    move-object v0, v1

    move-object v1, v2

    goto :goto_1

    :cond_2
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, v7, v7, v8, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {p2, v0}, Landroid/support/v4/view/accessibility/AccessibilityNodeInfoCompat;->setBoundsInParent(Landroid/graphics/Rect;)V

    goto :goto_2
.end method
