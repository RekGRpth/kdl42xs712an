.class public Lcom/twitter/android/widget/cl;
.super Landroid/graphics/drawable/Drawable;
.source "Twttr"


# instance fields
.field private a:I

.field private b:[Landroid/graphics/drawable/BitmapDrawable;


# direct methods
.method public constructor <init>([Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0

    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/widget/cl;->a:I

    invoke-virtual {p0}, Lcom/twitter/android/widget/cl;->invalidateSelf()V

    return-void
.end method

.method public a([Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    return-void
.end method

.method public a()[Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method public draw(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    iget v1, p0, Lcom/twitter/android/widget/cl;->a:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method public getOpacity()I
    .locals 1

    const/4 v0, -0x3

    return v0
.end method

.method protected onBoundsChange(Landroid/graphics/Rect;)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setAlpha(I)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 4

    iget-object v1, p0, Lcom/twitter/android/widget/cl;->b:[Landroid/graphics/drawable/BitmapDrawable;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-virtual {v3, p1}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
