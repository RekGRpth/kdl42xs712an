.class Lcom/twitter/android/widget/ca;
.super Lcom/twitter/android/client/ap;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/widget/PoiFragment;

.field private b:Lcom/twitter/android/util/z;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/PoiFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/widget/ca;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {p1}, Lcom/twitter/android/widget/PoiFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/ap;-><init>(Landroid/content/Context;)V

    invoke-static {}, Lcom/twitter/android/util/z;->a()Lcom/twitter/android/util/z;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/widget/ca;->b:Lcom/twitter/android/util/z;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/geo/c;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/ca;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-static {v0}, Lcom/twitter/android/widget/PoiFragment;->o(Lcom/twitter/android/widget/PoiFragment;)I

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/api/geo/c;->h()Ljava/util/List;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/widget/cb;

    invoke-direct {v1, p0}, Lcom/twitter/android/widget/cb;-><init>(Lcom/twitter/android/widget/ca;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    iget-object v1, p0, Lcom/twitter/android/widget/ca;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/PoiFragment;->a(Ljava/util/List;)V

    iget-object v1, p0, Lcom/twitter/android/widget/ca;->a:Lcom/twitter/android/widget/PoiFragment;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PoiFragment;->b(Z)V

    iget-object v1, p0, Lcom/twitter/android/widget/ca;->b:Lcom/twitter/android/util/z;

    invoke-virtual {v1, v0}, Lcom/twitter/android/util/z;->c(Ljava/util/List;)V

    goto :goto_0
.end method
