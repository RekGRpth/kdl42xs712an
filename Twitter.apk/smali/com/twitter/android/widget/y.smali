.class Lcom/twitter/android/widget/y;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field a:Landroid/graphics/Bitmap;

.field private final b:Landroid/net/Uri;

.field private final c:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/widget/t;Landroid/net/Uri;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/widget/y;->b:Landroid/net/Uri;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/widget/y;->c:Ljava/lang/ref/WeakReference;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/widget/y;->a:Landroid/graphics/Bitmap;

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/widget/y;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/t;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/widget/y;->b:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/widget/y;->a:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/t;->a(Landroid/net/Uri;Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
