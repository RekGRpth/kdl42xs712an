.class Lcom/twitter/android/hs;
.super Lcom/twitter/android/client/ap;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/GeoDebugActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/GeoDebugActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hs;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-direct {p0, p1}, Lcom/twitter/android/client/ap;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/geo/b;)V
    .locals 4

    iget-object v2, p1, Lcom/twitter/library/api/geo/b;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/twitter/library/api/geo/b;->h()Ljava/util/List;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/hs;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/model/k;

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    if-lez v3, :cond_0

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/geo/TwitterPlace;

    iget-object v1, v1, Lcom/twitter/library/api/geo/TwitterPlace;->fullName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/k;->a(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/hs;->a:Lcom/twitter/android/GeoDebugActivity;

    iget-object v0, v0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    return-void
.end method
