.class public Lcom/twitter/android/oo;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/android/ud;

.field private b:Landroid/widget/AdapterView$OnItemClickListener;

.field private c:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Lcom/twitter/android/ud;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/oo;->a:Lcom/twitter/android/ud;

    new-instance v0, Lcom/twitter/android/op;

    invoke-direct {v0, p0}, Lcom/twitter/android/op;-><init>(Lcom/twitter/android/oo;)V

    invoke-virtual {p1, v0}, Lcom/twitter/android/ud;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method private a(I)Z
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/content/Intent;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/oo;->c:Landroid/content/Intent;

    return-void
.end method

.method public a(Landroid/widget/AdapterView$OnItemClickListener;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/oo;->b:Landroid/widget/AdapterView$OnItemClickListener;

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/oo;->a:Lcom/twitter/android/ud;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/oo;->a:Lcom/twitter/android/ud;

    invoke-virtual {v0}, Lcom/twitter/android/ud;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/oo;->c:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/oo;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/oo;->c:Landroid/content/Intent;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    int-to-long v0, p1

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/oo;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    if-eqz p2, :cond_1

    move-object v0, p2

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/twitter/android/oo;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const v1, 0x7f0300c0    # com.twitter.android.R.layout.media_thumb_row

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/oo;->a:Lcom/twitter/android/ud;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/twitter/android/oo;->b:Landroid/widget/AdapterView$OnItemClickListener;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/twitter/android/oo;->a:Lcom/twitter/android/ud;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnScrollListener(Lcom/twitter/internal/android/widget/v;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const v1, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f0f0309    # com.twitter.android.R.string.photos_view_all

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method
