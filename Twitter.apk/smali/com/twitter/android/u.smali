.class Lcom/twitter/android/u;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ActivityDetailFragment;


# direct methods
.method private constructor <init>(Lcom/twitter/android/ActivityDetailFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ActivityDetailFragment;Lcom/twitter/android/t;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/u;-><init>(Lcom/twitter/android/ActivityDetailFragment;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJII[Lcom/twitter/library/api/TwitterUser;)V
    .locals 10

    iget-object v3, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v3, p2}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/android/ActivityDetailFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v9

    if-nez v9, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v3, 0xc8

    if-ne p3, v3, :cond_0

    if-lez p10, :cond_0

    const-wide/16 v3, 0x0

    cmp-long v3, p5, v3

    if-lez v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v3}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/android/ActivityDetailFragment;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v3}, Lcom/twitter/android/ActivityDetailFragment;->b(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v3

    move-wide v0, p5

    invoke-virtual {v3, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    iget v4, v9, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-static {v3, v4}, Lcom/twitter/android/ActivityDetailFragment;->a(Lcom/twitter/android/ActivityDetailFragment;I)Lcom/twitter/android/ClusterFollowAdapterData;

    move-result-object v3

    move-wide/from16 v0, p7

    invoke-virtual {v3, v0, v1}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    aget-object v8, p11, v4

    move-wide/from16 v4, p7

    move-wide v6, p5

    invoke-virtual/range {v3 .. v8}, Lcom/twitter/android/ClusterFollowAdapterData;->a(JJLcom/twitter/library/api/TwitterUser;)V

    :goto_1
    iget v3, v9, Lcom/twitter/android/client/PendingRequest;->b:I

    sparse-switch v3, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    iget-object v3, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v3}, Lcom/twitter/android/ActivityDetailFragment;->c(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/aaa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    goto :goto_0

    :cond_2
    move-wide/from16 v0, p7

    move-object/from16 v2, p11

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J[Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_1

    :sswitch_1
    iget-object v3, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v3}, Lcom/twitter/android/ActivityDetailFragment;->d(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/aaa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/android/aaa;->notifyDataSetChanged()V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x8 -> :sswitch_0
        0xd -> :sswitch_1
    .end sparse-switch
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v0}, Lcom/twitter/android/ActivityDetailFragment;->e(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/client/au;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->a()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v1}, Lcom/twitter/android/ActivityDetailFragment;->f(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/client/au;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/au;->a(Z)V

    iget-object v1, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v1}, Lcom/twitter/android/ActivityDetailFragment;->g(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/ye;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/u;->a:Lcom/twitter/android/ActivityDetailFragment;

    invoke-static {v1}, Lcom/twitter/android/ActivityDetailFragment;->g(Lcom/twitter/android/ActivityDetailFragment;)Lcom/twitter/android/ye;

    move-result-object v1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, p1, v0}, Lcom/twitter/android/ye;->a(Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
