.class public Lcom/twitter/android/TweetBoxFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/widget/Filterable;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/android/yr;
.implements Lcom/twitter/internal/android/widget/af;
.implements Lcom/twitter/internal/android/widget/f;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private A:Ljava/util/ArrayList;

.field private B:Z

.field private C:Landroid/text/TextWatcher;

.field private final D:Ljava/util/ArrayList;

.field private E:Lcom/twitter/internal/android/widget/af;

.field private F:Z

.field private G:Ljava/util/HashSet;

.field private H:Ljava/lang/String;

.field private I:Ljava/util/ArrayList;

.field private J:Ljava/util/ArrayList;

.field private K:Ljava/lang/String;

.field private L:Ljava/util/HashSet;

.field private M:Ljava/util/LinkedHashSet;

.field private N:Ljava/lang/String;

.field a:Lcom/twitter/internal/android/widget/PopupEditText;

.field b:Lcom/twitter/android/xa;

.field c:Landroid/support/v4/widget/CursorAdapter;

.field d:Lcom/twitter/android/aac;

.field e:Lcom/twitter/android/ww;

.field f:Ljava/lang/String;

.field g:Lcom/twitter/android/wv;

.field private h:Lcom/twitter/android/client/c;

.field private i:Lcom/twitter/library/client/Session;

.field private j:Z

.field private k:I

.field private l:Lcom/twitter/android/yo;

.field private m:Lcom/twitter/android/wz;

.field private n:Z

.field private o:I

.field private p:Z

.field private q:Lcom/twitter/android/wy;

.field private r:Z

.field private s:[Landroid/os/Parcelable;

.field private t:Z

.field private u:I

.field private v:Ljava/lang/String;

.field private w:Ljava/lang/CharSequence;

.field private x:Ljava/lang/String;

.field private y:[I

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->F:Z

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->G:Ljava/util/HashSet;

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    return-void
.end method

.method private A()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    array-length v0, v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v1, v0, -0x1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    const/4 v2, 0x0

    aget-object v0, v0, v2

    check-cast v0, Lcom/twitter/library/provider/ParcelableTweet;

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->j()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    invoke-interface {v2, v0, v1}, Lcom/twitter/android/wz;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method private B()I
    .locals 6

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/util/x;->m:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->group()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/util/LinkedHashSet;

    invoke-direct {v3}, Ljava/util/LinkedHashSet;-><init>()V

    sget-object v4, Lcom/twitter/library/util/x;->k:Ljava/util/regex/Pattern;

    invoke-virtual {v4, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    :goto_0
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/TweetBoxFragment;->L:Ljava/util/HashSet;

    invoke-virtual {v5, v4}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashSet;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v3, v4}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, 0x2

    add-int/2addr v0, v4

    goto :goto_0

    :cond_0
    iput-object v3, p0, Lcom/twitter/android/TweetBoxFragment;->M:Ljava/util/LinkedHashSet;

    if-lez v0, :cond_1

    add-int/lit8 v0, v0, -0x1

    :cond_1
    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->N:Ljava/lang/String;

    :cond_2
    return v0
.end method

.method private C()I
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->L:Ljava/util/HashSet;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->L:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lko;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->B()I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    sub-int v0, v2, v0

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    iget-boolean v2, p0, Lcom/twitter/android/TweetBoxFragment;->p:Z

    if-lez v0, :cond_3

    iget v3, p0, Lcom/twitter/android/TweetBoxFragment;->k:I

    add-int/2addr v0, v3

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Z)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    :cond_0
    add-int/2addr v0, v1

    :cond_1
    :goto_1
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    if-eqz v2, :cond_1

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Z)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    :cond_4
    add-int/2addr v0, v1

    add-int/lit8 v0, v0, -0x1

    goto :goto_1
.end method

.method private D()V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/MentionContactActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetBoxFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void
.end method

.method private E()V
    .locals 8

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v3

    const/4 v0, 0x0

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v4

    const-class v5, Lcom/twitter/android/widget/ContactSpan;

    invoke-interface {v3, v0, v4, v5}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/widget/ContactSpan;

    new-instance v4, Ljava/util/ArrayList;

    array-length v5, v0

    invoke-direct {v4, v5}, Ljava/util/ArrayList;-><init>(I)V

    :goto_0
    array-length v5, v0

    if-ge v1, v5, :cond_0

    aget-object v5, v0, v1

    new-instance v6, Landroid/util/Pair;

    invoke-interface {v3, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-direct {v6, v5, v7}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private F()V
    .locals 9

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/twitter/android/widget/ContactSpan;

    invoke-interface {v3, v1}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    invoke-virtual {v1}, Lcom/twitter/android/widget/ContactSpan;->a()Lcom/twitter/library/api/TwitterContact;

    move-result-object v6

    iget-object v6, v6, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    add-int/2addr v7, v5

    if-ltz v5, :cond_0

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v8

    if-gt v7, v8, :cond_0

    invoke-interface {v3, v5, v7}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v8

    invoke-interface {v8}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_0

    const/16 v0, 0x21

    invoke-interface {v3, v1, v5, v7, v0}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v5

    add-int/2addr v5, v0

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v7

    if-gt v0, v7, :cond_1

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v7

    if-gt v5, v7, :cond_1

    invoke-interface {v3, v0, v5}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/16 v6, 0x21

    invoke-interface {v3, v1, v0, v5, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_0

    :cond_1
    invoke-interface {v3, v1}, Landroid/text/Editable;->removeSpan(Ljava/lang/Object;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    invoke-interface {v3}, Landroid/text/Editable;->length()I

    move-result v1

    const-class v4, Lcom/twitter/android/widget/ContactSpan;

    invoke-interface {v3, v0, v1, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/widget/ContactSpan;

    array-length v0, v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    invoke-interface {v0}, Lcom/twitter/android/wz;->d()V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static a(IZ)Lcom/twitter/android/TweetBoxFragment;
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "layout"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "show_mention_contact"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    new-instance v1, Lcom/twitter/android/TweetBoxFragment;

    invoke-direct {v1}, Lcom/twitter/android/TweetBoxFragment;-><init>()V

    invoke-virtual {v1, v0}, Lcom/twitter/android/TweetBoxFragment;->setArguments(Landroid/os/Bundle;)V

    return-object v1
.end method

.method static synthetic a(Lcom/twitter/android/TweetBoxFragment;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic a(Lcom/twitter/android/TweetBoxFragment;Ljava/util/List;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Ljava/util/List;)Ljava/lang/String;
    .locals 5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    invoke-interface {p1, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v0, v0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    :cond_0
    const/16 v4, 0x40

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v4, 0x20

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetBoxFragment;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    return-object p1
.end method

.method private a(Landroid/view/View;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v0, 0x7f0f0133    # com.twitter.android.R.string.edit_new_tweet

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private a([Lcom/twitter/library/api/TwitterContact;)V
    .locals 6

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionEnd()I

    move-result v2

    if-lez v2, :cond_0

    add-int/lit8 v0, v2, -0x1

    invoke-interface {v4, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    const/16 v3, 0x20

    if-ne v0, v3, :cond_0

    const/4 v0, 0x1

    :goto_0
    sub-int v3, v2, v0

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->b:Lcom/twitter/android/xa;

    invoke-virtual {v2, v4, v3}, Lcom/twitter/android/xa;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    array-length v5, p1

    if-lez v5, :cond_3

    move v0, v2

    move v2, v3

    :goto_1
    array-length v3, p1

    if-ge v1, v3, :cond_1

    aget-object v3, p1, v1

    iget-object v5, v3, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    invoke-interface {v4, v0, v2, v5}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    iget-object v2, v3, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v2, v0

    new-instance v5, Lcom/twitter/android/widget/ContactSpan;

    invoke-direct {v5, v3}, Lcom/twitter/android/widget/ContactSpan;-><init>(Lcom/twitter/library/api/TwitterContact;)V

    const/16 v3, 0x21

    invoke-interface {v4, v5, v0, v2, v3}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    array-length v3, p1

    add-int/lit8 v3, v3, -0x1

    if-ge v1, v3, :cond_4

    const-string/jumbo v3, ", "

    invoke-interface {v4, v0, v3}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    add-int/lit8 v0, v2, 0x2

    :goto_2
    add-int/lit8 v1, v1, 0x1

    move v2, v0

    goto :goto_1

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    invoke-interface {v0, p1}, Lcom/twitter/android/wz;->a([Lcom/twitter/library/api/TwitterContact;)V

    :cond_2
    :goto_3
    return-void

    :cond_3
    add-int/2addr v0, v3

    const-string/jumbo v1, ""

    invoke-interface {v4, v2, v0, v1}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/TweetBoxFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->r:Z

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetBoxFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/TweetBoxFragment;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/TweetBoxFragment;)[Landroid/os/Parcelable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/TweetBoxFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->x()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/wz;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/TweetBoxFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/TweetBoxFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/TweetBoxFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->A()V

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/TweetBoxFragment;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->K:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/TweetBoxFragment;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TweetBoxFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->B:Z

    return v0
.end method

.method static synthetic l(Lcom/twitter/android/TweetBoxFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->F:Z

    return v0
.end method

.method static synthetic m(Lcom/twitter/android/TweetBoxFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->G:Ljava/util/HashSet;

    return-object v0
.end method

.method private w()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/text/InputFilter;

    const/4 v2, 0x0

    new-instance v3, Lcom/twitter/android/wt;

    invoke-direct {v3, p0}, Lcom/twitter/android/wt;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setFilters([Landroid/text/InputFilter;)V

    return-void
.end method

.method private x()V
    .locals 6

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->l()I

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->z()V

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->y()V

    invoke-static {}, Lkn;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    :cond_2
    :goto_1
    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->A()V

    goto :goto_0

    :cond_3
    iget-boolean v1, p0, Lcom/twitter/android/TweetBoxFragment;->r:Z

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/util/List;)Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v1, ""

    iput-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v1, v0}, Ljava/lang/CharSequence;->charAt(I)C

    move-result v1

    const/16 v2, 0x40

    if-eq v1, v2, :cond_5

    :cond_4
    invoke-virtual {p0, v3}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/CharSequence;)V

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-lt v1, v2, :cond_7

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x5

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v5

    move v2, v0

    move v1, v0

    :goto_2
    if-ge v2, v5, :cond_6

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v0, v0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    add-int/2addr v1, v0

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    :cond_6
    move v0, v1

    :cond_7
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v4, v0, v1}, Landroid/widget/EditText;->setSelection(II)V

    goto :goto_1
.end method

.method private y()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x5

    if-le v2, v3, :cond_1

    :cond_0
    return-void

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private z()V
    .locals 12

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/twitter/library/util/CollectionsUtil;->a()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->L:Ljava/util/HashSet;

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Ljava/util/LinkedHashSet;

    invoke-direct {v6}, Ljava/util/LinkedHashSet;-><init>()V

    iget-object v7, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    const/4 v1, 0x0

    array-length v0, v7

    add-int/lit8 v3, v0, -0x1

    aget-object v0, v7, v3

    check-cast v0, Lcom/twitter/library/provider/ParcelableTweet;

    invoke-static {v0}, Lcom/twitter/library/api/RepliedUser;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/library/api/RepliedUser;

    move-result-object v2

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->e()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->g()J

    move-result-wide v4

    :goto_1
    cmp-long v4, v8, v4

    if-eqz v4, :cond_5

    invoke-virtual {v6, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->e()Z

    move-result v2

    if-eqz v2, :cond_d

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->f()J

    move-result-wide v4

    cmp-long v2, v4, v8

    if-eqz v2, :cond_d

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->i()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_d

    invoke-static {v0}, Lcom/twitter/library/api/RepliedUser;->b(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/library/api/RepliedUser;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    move-object v2, v1

    :cond_2
    :goto_2
    if-ltz v3, :cond_a

    aget-object v0, v7, v3

    check-cast v0, Lcom/twitter/library/provider/ParcelableTweet;

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->p()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/MentionEntity;

    iget-wide v10, v1, Lcom/twitter/library/api/MentionEntity;->userId:J

    cmp-long v5, v10, v8

    if-eqz v5, :cond_3

    invoke-static {v1}, Lcom/twitter/library/api/RepliedUser;->a(Lcom/twitter/library/api/MentionEntity;)Lcom/twitter/library/api/RepliedUser;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->f()J

    move-result-wide v4

    goto :goto_1

    :cond_5
    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v6, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_6
    invoke-static {}, Lkn;->b()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->x()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_7
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/RepliedUser;

    iget-wide v10, v1, Lcom/twitter/library/api/RepliedUser;->userId:J

    cmp-long v5, v10, v8

    if-eqz v5, :cond_7

    invoke-virtual {v6, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_8
    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    if-eqz v0, :cond_9

    iget-object v0, v0, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    if-eqz v0, :cond_9

    iget-wide v4, v0, Lcom/twitter/library/api/CardUser;->userId:J

    cmp-long v1, v8, v4

    if-eqz v1, :cond_9

    new-instance v1, Lcom/twitter/library/api/RepliedUser;

    iget-object v4, v0, Lcom/twitter/library/api/CardUser;->screenName:Ljava/lang/String;

    iget-wide v10, v0, Lcom/twitter/library/api/CardUser;->userId:J

    iget-object v0, v0, Lcom/twitter/library/api/CardUser;->fullName:Ljava/lang/String;

    invoke-direct {v1, v4, v10, v11, v0}, Lcom/twitter/library/api/RepliedUser;-><init>(Ljava/lang/String;JLjava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_9
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_2

    :cond_a
    invoke-virtual {v6}, Ljava/util/LinkedHashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {v6, v2}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    :cond_b
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-static {}, Lko;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v0, v0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_c
    iput-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->L:Ljava/util/HashSet;

    goto/16 :goto_0

    :cond_d
    move-object v2, v1

    goto/16 :goto_2
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->clearFocus()V

    return-void
.end method

.method public a(I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(I)V

    return-void
.end method

.method public a(II)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->z:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    const-string/jumbo v1, " #alert"

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    sub-int/2addr v0, v1

    if-ltz v0, :cond_0

    if-le p1, v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1, v0, v0}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v0, p1, p2}, Lcom/twitter/internal/android/widget/af;->a(II)V

    :cond_1
    return-void

    :cond_2
    if-le p2, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1, p1, v0}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    goto :goto_0
.end method

.method public a(Landroid/os/Parcelable;)V
    .locals 2

    const/4 v0, 0x1

    new-array v0, v0, [Landroid/os/Parcelable;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a([Landroid/os/Parcelable;)V

    return-void
.end method

.method public a(Landroid/text/TextWatcher;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    return-void
.end method

.method public a(Lcom/twitter/android/wz;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    return-void
.end method

.method public a(Lcom/twitter/internal/android/widget/af;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    invoke-virtual {v0, p2}, Lcom/twitter/android/aac;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->j:Z

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    iget-object v4, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    if-ne v0, v4, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    invoke-virtual {v0}, Lcom/twitter/android/aac;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-nez p2, :cond_1

    move v0, v1

    :goto_0
    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    iget-object v4, p0, Lcom/twitter/android/TweetBoxFragment;->e:Lcom/twitter/android/ww;

    if-ne v0, v4, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, ":composition:autocomplete_dropdown:hashtag:select"

    aput-object v1, v0, v2

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    iget-object v4, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, ":composition:autocomplete_dropdown:user:select"

    aput-object v1, v0, v2

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v0, p1, p2}, Lcom/twitter/internal/android/widget/af;->a(Ljava/lang/CharSequence;I)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    new-array v0, v1, [Ljava/lang/String;

    const-string/jumbo v1, ":composition:autocomplete_dropdown:mention_contact:select"

    aput-object v1, v0, v2

    invoke-virtual {v3, v4, v5, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->D()V

    goto :goto_1
.end method

.method public a(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->v:Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->isFocused()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->r:Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/PopupEditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;[I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->x:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/TweetBoxFragment;->y:[I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->l()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_2

    const/4 v0, 0x0

    aget v0, p2, v0

    const/4 v1, 0x1

    aget v1, p2, v1

    if-ltz v0, :cond_2

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    if-gt v1, v2, :cond_2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetBoxFragment;->b(II)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->l()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a(I)V

    goto :goto_0
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-nez v0, :cond_1

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->A:Ljava/util/ArrayList;

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ContactEntity;

    new-instance v4, Lcom/twitter/library/api/TwitterContact;

    iget-object v5, v0, Lcom/twitter/library/api/ContactEntity;->name:Ljava/lang/String;

    iget-object v6, v0, Lcom/twitter/library/api/ContactEntity;->email:Ljava/lang/String;

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/library/api/TwitterContact;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/twitter/android/widget/ContactSpan;

    invoke-direct {v5, v4}, Lcom/twitter/android/widget/ContactSpan;-><init>(Lcom/twitter/library/api/TwitterContact;)V

    iget v4, v0, Lcom/twitter/library/api/ContactEntity;->spanStart:I

    iget v6, v0, Lcom/twitter/library/api/ContactEntity;->spanStart:I

    iget-object v0, v0, Lcom/twitter/library/api/ContactEntity;->name:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v6

    const/16 v6, 0x21

    invoke-interface {v1, v5, v4, v0, v6}, Landroid/text/Editable;->setSpan(Ljava/lang/Object;III)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    new-array v0, v7, [Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterContact;

    invoke-interface {v1, v0}, Lcom/twitter/android/wz;->a([Lcom/twitter/library/api/TwitterContact;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 5

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->b:Lcom/twitter/android/xa;

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->B()J

    move-result-wide v2

    invoke-virtual {v0, v1, p0, v2, v3}, Lcom/twitter/internal/android/widget/PopupEditText;->a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;Landroid/widget/Filterable;J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->b:Lcom/twitter/android/xa;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->B()J

    move-result-wide v3

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/internal/android/widget/PopupEditText;->a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;Landroid/widget/Filterable;J)V

    goto :goto_0
.end method

.method public a([Landroid/os/Parcelable;)V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->s:[Landroid/os/Parcelable;

    iget-boolean v1, p0, Lcom/twitter/android/TweetBoxFragment;->t:Z

    if-eqz v1, :cond_1

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->c()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->clear()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f02fc    # com.twitter.android.R.string.persistent_reply_hint

    const/4 v0, 0x1

    new-array v3, v0, [Ljava/lang/Object;

    aget-object v0, p1, v4

    check-cast v0, Lcom/twitter/library/provider/ParcelableTweet;

    invoke-interface {v0}, Lcom/twitter/library/provider/ParcelableTweet;->j()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->x()V

    :cond_1
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3

    const/4 v2, 0x5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->j:Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->l:Lcom/twitter/android/yo;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/yo;->a(Ljava/lang/String;)V

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(C)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setGravity(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->D:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->E()V

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->F()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    invoke-interface {v0, p1}, Landroid/text/TextWatcher;->afterTextChanged(Landroid/text/Editable;)V

    :cond_2
    return-void

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/PopupEditText;->setGravity(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget-boolean v0, v0, Lcom/twitter/android/client/c;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/PopupEditText;->setGravity(I)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->requestFocus()Z

    return-void
.end method

.method public b(I)V
    .locals 1

    iput p1, p0, Lcom/twitter/android/TweetBoxFragment;->k:I

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->n:Z

    if-eqz v0, :cond_0

    if-lez p1, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->n:Z

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->Y()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    return-void
.end method

.method public b(II)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    return-void
.end method

.method public b(Ljava/lang/CharSequence;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->w:Ljava/lang/CharSequence;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/16 v1, 0x65

    invoke-virtual {v0, p1, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)V
    .locals 5

    iput-object p1, p0, Lcom/twitter/android/TweetBoxFragment;->K:Ljava/lang/String;

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-wide v3, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-static {v2, v1}, Lcom/twitter/android/InReplyToDialogFragment;->a(Ljava/util/ArrayList;Ljava/util/HashSet;)Lcom/twitter/android/InReplyToDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/wx;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/wx;-><init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/InReplyToDialogFragment;->a(Lcom/twitter/android/in;)V

    const-string/jumbo v2, "in_reply_to_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/InReplyToDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    const-string/jumbo v3, "composition"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "reply_dialog"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string/jumbo v3, ""

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string/jumbo v3, "impression"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public b(Z)V
    .locals 8

    const/4 v7, 0x0

    iput-boolean p1, p0, Lcom/twitter/android/TweetBoxFragment;->z:Z

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    const-string/jumbo v1, " #alert"

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionStart()I

    move-result v1

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionEnd()I

    move-result v2

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    if-eqz p1, :cond_1

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " #alert"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    new-instance v5, Landroid/text/SpannableString;

    invoke-direct {v5, v3}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Lcom/twitter/android/TweetBoxFragment$1AlertHashtagSpan;

    invoke-direct {v3, p0}, Lcom/twitter/android/TweetBoxFragment$1AlertHashtagSpan;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    const-string/jumbo v6, " #alert"

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v6

    sub-int v6, v4, v6

    const/16 v7, 0x21

    invoke-virtual {v5, v3, v6, v4, v7}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->w()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v4

    const-string/jumbo v5, " #alert"

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    sub-int v5, v4, v5

    new-array v6, v7, [Landroid/text/InputFilter;

    invoke-virtual {v0, v6}, Lcom/twitter/internal/android/widget/PopupEditText;->setFilters([Landroid/text/InputFilter;)V

    if-ltz v5, :cond_0

    const-string/jumbo v6, " #alert"

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v3, v7, v5}, Ljava/lang/String;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    if-le v1, v5, :cond_2

    invoke-virtual {v0, v5, v5}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    goto :goto_0

    :cond_2
    if-le v2, v5, :cond_3

    invoke-virtual {v0, v1, v5}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(II)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->beforeTextChanged(Ljava/lang/CharSequence;III)V

    :cond_0
    return-void
.end method

.method public c()Landroid/text/Editable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getEditableText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 6

    const/4 v1, 0x0

    const/4 v4, 0x0

    const/4 v2, 0x1

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "@"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    invoke-virtual {v0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    move v1, v2

    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    packed-switch v1, :pswitch_data_0

    move v2, v4

    :cond_0
    :goto_1
    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget v3, p0, Lcom/twitter/android/TweetBoxFragment;->o:I

    const-string/jumbo v4, "compose"

    invoke-virtual {v2, v0, v1, v3, v4}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v0, p1}, Lcom/twitter/internal/android/widget/af;->c(Ljava/lang/CharSequence;)V

    :cond_2
    return-void

    :cond_3
    const-string/jumbo v3, "#"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x2

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v5

    if-le v5, v2, :cond_4

    move v1, v3

    goto :goto_0

    :cond_4
    move-object v0, v1

    move v1, v3

    goto :goto_0

    :cond_5
    sget-object v3, Lcom/twitter/library/util/x;->v:Ljava/util/regex/Pattern;

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->find()Z

    move-result v3

    if-eqz v3, :cond_6

    move v1, v2

    goto :goto_0

    :cond_6
    move-object v0, v1

    move v1, v4

    goto :goto_0

    :pswitch_0
    invoke-static {v0}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v4

    goto :goto_1

    :pswitch_1
    invoke-static {v0}, Lcom/twitter/android/provider/SuggestionsProvider;->c(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    move v2, v4

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public c(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->clearFocus()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v0, v1, p1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public d()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->j:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->length()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public f()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->performLongClick()Z

    move-result v0

    return v0
.end method

.method public g()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->p:Z

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    goto :goto_0
.end method

.method public getFilter()Landroid/widget/Filter;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->g:Lcom/twitter/android/wv;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/wv;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/wv;-><init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->g:Lcom/twitter/android/wv;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->g:Lcom/twitter/android/wv;

    return-object v0
.end method

.method public h()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->p:Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->q()V

    return-void
.end method

.method public i()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public j()Landroid/text/Editable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public k()[I
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v1, 0x2

    new-array v1, v1, [I

    const/4 v2, 0x0

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionStart()I

    move-result v3

    aput v3, v1, v2

    const/4 v2, 0x1

    invoke-virtual {v0}, Landroid/widget/EditText;->getSelectionEnd()I

    move-result v0

    aput v0, v1, v2

    return-object v1
.end method

.method public l()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->length()I

    move-result v0

    return v0
.end method

.method public m()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionStart()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getSelectionEnd()I

    move-result v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->c()Landroid/text/Editable;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    if-lez v0, :cond_0

    add-int/lit8 v4, v0, -0x1

    invoke-interface {v2, v4}, Landroid/text/Editable;->charAt(I)C

    move-result v4

    const/16 v5, 0x40

    if-ne v4, v5, :cond_0

    add-int/lit8 v0, v0, -0x1

    :cond_0
    if-lez v0, :cond_1

    iget-object v4, p0, Lcom/twitter/android/TweetBoxFragment;->b:Lcom/twitter/android/xa;

    invoke-static {v4}, Lcom/twitter/android/xa;->a(Lcom/twitter/android/xa;)Ljava/util/HashSet;

    move-result-object v4

    add-int/lit8 v5, v0, -0x1

    invoke-interface {v2, v5}, Landroid/text/Editable;->charAt(I)C

    move-result v5

    invoke-static {v5}, Ljava/lang/Character;->valueOf(C)Ljava/lang/Character;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    const-string/jumbo v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const-string/jumbo v4, "@"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-interface {v2, v0, v1, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    return-void
.end method

.method public n()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->hasFocus()Z

    move-result v0

    return v0
.end method

.method public o()Ljava/lang/String;
    .locals 1

    invoke-static {}, Lko;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->N:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->N:Ljava/lang/String;

    :goto_0
    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v7, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->A()I

    move-result v2

    iput v2, p0, Lcom/twitter/android/TweetBoxFragment;->o:I

    new-instance v2, Lcom/twitter/android/wy;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/wy;-><init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V

    iput-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->q:Lcom/twitter/android/wy;

    iget-object v2, p0, Lcom/twitter/android/TweetBoxFragment;->q:Lcom/twitter/android/wy;

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aB()Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, 0x7f030168    # com.twitter.android.R.layout.typeahead_user_row_view

    :goto_0
    new-instance v3, Lcom/twitter/android/aac;

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    sget-object v5, Lcom/twitter/library/provider/ax;->i:Landroid/net/Uri;

    invoke-virtual {v5}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v5

    invoke-virtual {v5, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v5

    const-string/jumbo v6, "ownerId"

    invoke-virtual {v5, v6, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    invoke-direct {v3, v4, v2, v0}, Lcom/twitter/android/aac;-><init>(Landroid/app/Activity;Landroid/net/Uri;I)V

    iput-object v3, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    new-instance v0, Lcom/twitter/android/ww;

    invoke-direct {v0, v1}, Lcom/twitter/android/ww;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->e:Lcom/twitter/android/ww;

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->d:Lcom/twitter/android/aac;

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->c:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_0

    iput-boolean v7, p0, Lcom/twitter/android/TweetBoxFragment;->r:Z

    :cond_0
    new-instance v1, Lcom/twitter/android/xa;

    invoke-direct {v1, p0}, Lcom/twitter/android/xa;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    iput-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->b:Lcom/twitter/android/xa;

    invoke-virtual {p0, v7}, Lcom/twitter/android/TweetBoxFragment;->a(Z)V

    new-instance v1, Lcom/twitter/android/wq;

    invoke-direct {v1, p0}, Lcom/twitter/android/wq;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/internal/android/widget/ax;->a(Landroid/content/Context;)Lcom/twitter/internal/android/widget/ax;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/internal/android/widget/ax;->a:Landroid/graphics/Typeface;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setTypeface(Landroid/graphics/Typeface;)V

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->x()V

    iput-boolean v7, p0, Lcom/twitter/android/TweetBoxFragment;->t:Z

    return-void

    :cond_1
    const v0, 0x7f03016d    # com.twitter.android.R.layout.user_dropdown_row_view

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    const/4 v0, 0x1

    if-ne p1, v0, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    if-eqz p3, :cond_1

    const-string/jumbo v0, "contacts"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableArrayExtra(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v4

    array-length v5, v4

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    aget-object v0, v4, v1

    instance-of v6, v0, Lcom/twitter/library/api/TwitterContact;

    if-eqz v6, :cond_0

    check-cast v0, Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    new-array v0, v2, [Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterContact;

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a([Lcom/twitter/library/api/TwitterContact;)V

    :cond_2
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "No arguments provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const-string/jumbo v3, "layout"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/TweetBoxFragment;->u:I

    iget v3, p0, Lcom/twitter/android/TweetBoxFragment;->u:I

    if-nez v3, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "No layout provided"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string/jumbo v3, "show_mention_contact"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->B:Z

    if-eqz p1, :cond_2

    const-string/jumbo v0, "show_link_hint"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_2
    move v0, v2

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->n:Z

    if-eqz p1, :cond_3

    const-string/jumbo v0, "selected_replied_users"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    const-string/jumbo v0, "scribe_page"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->K:Ljava/lang/String;

    :cond_3
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aD()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string/jumbo v0, "android_typeahead_2_2006"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "android_typeahead_2_2006"

    invoke-static {v0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "control_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_6

    :goto_1
    iput-boolean v2, p0, Lcom/twitter/android/TweetBoxFragment;->F:Z

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->F:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcom/twitter/android/wu;

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetBoxFragment;->G:Ljava/util/HashSet;

    invoke-direct {v0, v2, v3}, Lcom/twitter/android/wu;-><init>(Landroid/content/Context;Ljava/util/HashSet;)V

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/twitter/android/wu;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_4
    return-void

    :cond_5
    move v0, v1

    goto :goto_0

    :cond_6
    move v2, v1

    goto :goto_1
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    const/16 v6, 0x65

    const/4 v5, 0x0

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget v0, p0, Lcom/twitter/android/TweetBoxFragment;->u:I

    invoke-virtual {p1, v0, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/yo;

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Lcom/twitter/android/yo;-><init>(Landroid/app/Activity;Lcom/twitter/android/yr;)V

    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->l:Lcom/twitter/android/yo;

    const v0, 0x7f09010b    # com.twitter.android.R.id.tweet_text

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "No edit text found in layout"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Lcom/twitter/android/widget/ar;->a()Landroid/text/Editable$Factory;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setEditableFactory(Landroid/text/Editable$Factory;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-static {}, Lcom/twitter/android/widget/at;->a()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setPopupEditTextListener(Lcom/twitter/internal/android/widget/af;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setBatchListener(Lcom/twitter/internal/android/widget/f;)V

    new-instance v1, Lcom/twitter/android/ws;

    invoke-direct {v1, p0}, Lcom/twitter/android/ws;-><init>(Lcom/twitter/android/TweetBoxFragment;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget-boolean v1, v1, Lcom/twitter/android/client/c;->f:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setGravity(I)V

    :cond_1
    invoke-static {}, Lkn;->a()V

    invoke-static {}, Lko;->a()V

    invoke-static {}, Lkn;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0c0068    # com.twitter.android.R.dimen.font_size_large

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    invoke-virtual {v0, v4, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setTextSize(IF)V

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {v1, v4, v4, v4, v4}, Landroid/view/ViewGroup$MarginLayoutParams;->setMargins(IIII)V

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->requestLayout()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0b0057    # com.twitter.android.R.color.gray

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setHintTextColor(I)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x10

    if-ge v1, v3, :cond_6

    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/PopupEditText;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :cond_2
    :goto_0
    iput-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->v:Ljava/lang/String;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->v:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setHint(Ljava/lang/CharSequence;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->w:Ljava/lang/CharSequence;

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->w:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1, v6}, Lcom/twitter/internal/android/widget/PopupEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->x:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->y:[I

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/lang/String;[I)V

    invoke-direct {p0, v2}, Lcom/twitter/android/TweetBoxFragment;->a(Landroid/view/View;)V

    iget-boolean v0, p0, Lcom/twitter/android/TweetBoxFragment;->z:Z

    if-eqz v0, :cond_4

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->w()V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->A:Ljava/util/ArrayList;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetBoxFragment;->a(Ljava/util/ArrayList;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "in_reply_to_dialog"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/InReplyToDialogFragment;

    if-eqz v0, :cond_5

    new-instance v1, Lcom/twitter/android/wx;

    invoke-direct {v1, p0, v5}, Lcom/twitter/android/wx;-><init>(Lcom/twitter/android/TweetBoxFragment;Lcom/twitter/android/wq;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/InReplyToDialogFragment;->a(Lcom/twitter/android/in;)V

    :cond_5
    return-object v2

    :cond_6
    invoke-virtual {v0, v5}, Lcom/twitter/internal/android/widget/PopupEditText;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v3, 0x7f0f031c    # com.twitter.android.R.string.post_tweet

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v6}, Lcom/twitter/internal/android/widget/PopupEditText;->setImeActionLabel(Ljava/lang/CharSequence;I)V

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->q:Lcom/twitter/android/wy;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    const/16 v0, 0x65

    if-ne p2, v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->C()I

    move-result v0

    if-lez v0, :cond_0

    const/16 v1, 0x8c

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    invoke-interface {v0}, Lcom/twitter/android/wz;->b()V

    :cond_0
    const/4 v0, 0x0

    return v0
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "show_link_hint"

    iget-boolean v1, p0, Lcom/twitter/android/TweetBoxFragment;->n:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "selected_replied_users"

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->J:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "scribe_page"

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->K:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->C:Landroid/text/TextWatcher;

    invoke-interface {v0, p1, p2, p3, p4}, Landroid/text/TextWatcher;->onTextChanged(Ljava/lang/CharSequence;III)V

    :cond_0
    return-void
.end method

.method public p()Ljava/util/ArrayList;
    .locals 7

    invoke-static {}, Lko;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->M:Ljava/util/LinkedHashSet;

    if-eqz v0, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->I:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-object v4, v0, Lcom/twitter/library/api/RepliedUser;->screenName:Ljava/lang/String;

    iget-wide v5, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->M:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    move-object v0, v1

    :goto_2
    return-object v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_2
.end method

.method q()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->a:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->m:Lcom/twitter/android/wz;

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->C()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/twitter/android/wz;->a(I)V

    :cond_0
    return-void
.end method

.method public r()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->f:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->h:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/TweetBoxFragment;->i:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/TweetBoxFragment;->f:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->E:Lcom/twitter/internal/android/widget/af;

    invoke-interface {v0}, Lcom/twitter/internal/android/widget/af;->r()V

    :cond_1
    return-void
.end method

.method public s()Ljava/util/ArrayList;
    .locals 8

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetBoxFragment;->j()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Lcom/twitter/android/widget/ContactSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/widget/ContactSpan;

    new-instance v3, Ljava/util/ArrayList;

    array-length v4, v0

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    array-length v4, v0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v0, v1

    new-instance v6, Lcom/twitter/library/api/ContactEntity;

    invoke-direct {v6}, Lcom/twitter/library/api/ContactEntity;-><init>()V

    invoke-virtual {v5}, Lcom/twitter/android/widget/ContactSpan;->a()Lcom/twitter/library/api/TwitterContact;

    move-result-object v7

    iget-object v7, v7, Lcom/twitter/library/api/TwitterContact;->a:Ljava/lang/String;

    iput-object v7, v6, Lcom/twitter/library/api/ContactEntity;->name:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/twitter/android/widget/ContactSpan;->a()Lcom/twitter/library/api/TwitterContact;

    move-result-object v7

    iget-object v7, v7, Lcom/twitter/library/api/TwitterContact;->b:Ljava/lang/String;

    iput-object v7, v6, Lcom/twitter/library/api/ContactEntity;->email:Ljava/lang/String;

    invoke-interface {v2, v5}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    iput v5, v6, Lcom/twitter/library/api/ContactEntity;->spanStart:I

    invoke-virtual {v3, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public t()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->E()V

    return-void
.end method

.method public u()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetBoxFragment;->F()V

    return-void
.end method

.method public v()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetBoxFragment;->H:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    rsub-int v0, v0, 0x8c

    return v0
.end method
