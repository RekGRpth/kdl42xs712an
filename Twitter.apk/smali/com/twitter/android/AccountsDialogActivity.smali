.class public Lcom/twitter/android/AccountsDialogActivity;
.super Lcom/twitter/android/client/BaseListActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/ar;


# instance fields
.field a:Lcom/twitter/android/client/c;

.field b:Ljava/lang/String;

.field private c:Landroid/net/Uri;

.field private d:Lcom/twitter/android/g;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListActivity;-><init>()V

    return-void
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Lcom/twitter/android/g;

    invoke-virtual {v0}, Lcom/twitter/android/g;->notifyDataSetChanged()V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseListActivity;->onActivityResult(IILandroid/content/Intent;)V

    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_1

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "sb_account_name"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p0, v0}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;Landroid/accounts/Account;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/AccountsDialogActivity;->a:Lcom/twitter/android/client/c;

    const v2, 0x7f8b5

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->c:Landroid/net/Uri;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->c:Landroid/net/Uri;

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/AccountsDialogActivity;->a(Landroid/net/Uri;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 3

    const/4 v2, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09006a    # com.twitter.android.R.id.add_account

    if-ne v0, v1, :cond_1

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/LoginActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "add_account"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/AccountsDialogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f09006b    # com.twitter.android.R.id.settings

    if-ne v0, v1, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->finish()V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 11

    const/16 v10, 0x8

    const/4 v9, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030005    # com.twitter.android.R.layout.accounts

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->setContentView(I)V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/a;->a(Landroid/accounts/AccountManager;)[Landroid/accounts/Account;

    move-result-object v3

    array-length v4, v3

    new-array v5, v4, [Lcom/twitter/android/UserAccount;

    move v0, v2

    :goto_0
    if-ge v0, v4, :cond_0

    new-instance v6, Lcom/twitter/android/UserAccount;

    aget-object v7, v3, v0

    aget-object v8, v3, v0

    invoke-static {v1, v8}, Lcom/twitter/library/util/a;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Lcom/twitter/android/UserAccount;-><init>(Landroid/accounts/Account;Lcom/twitter/library/api/TwitterUser;)V

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    new-instance v0, Lcom/twitter/android/g;

    invoke-direct {v0, p0, p0, v5}, Lcom/twitter/android/g;-><init>(Lcom/twitter/android/AccountsDialogActivity;Landroid/content/Context;[Lcom/twitter/android/UserAccount;)V

    iput-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Lcom/twitter/android/g;

    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->getListView()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Lcom/twitter/android/g;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    const v0, 0x7f09006a    # com.twitter.android.R.id.add_account

    invoke-virtual {p0, v0}, Lcom/twitter/android/AccountsDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const v1, 0x7f09006b    # com.twitter.android.R.id.settings

    invoke-virtual {p0, v1}, Lcom/twitter/android/AccountsDialogActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    if-le v4, v9, :cond_1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v0, v10}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "page"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->c:Landroid/net/Uri;

    const-string/jumbo v0, "account_name"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->b:Ljava/lang/String;

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v9, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->e(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->a:Lcom/twitter/android/client/c;

    return-void

    :cond_1
    invoke-virtual {v1, v10}, Landroid/widget/Button;->setVisibility(I)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->a:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/AccountsDialogActivity;->d:Lcom/twitter/android/g;

    invoke-virtual {v0, p3}, Lcom/twitter/android/g;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UserAccount;

    new-instance v1, Lcom/twitter/library/util/z;

    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/twitter/library/util/z;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lcom/twitter/library/util/z;->a()V

    const/4 v1, 0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "account"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/AccountsDialogActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/AccountsDialogActivity;->finish()V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListActivity;->onResume()V

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/StartActivity;->a(Landroid/app/Activity;)V

    :cond_0
    return-void
.end method
