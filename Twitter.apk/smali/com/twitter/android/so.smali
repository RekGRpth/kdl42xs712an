.class public Lcom/twitter/android/so;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ob;


# instance fields
.field final synthetic a:Lcom/twitter/android/SearchResultsFragment;

.field private final b:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Lcom/twitter/android/SearchResultsFragment;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/so;->b:Ljava/util/HashSet;

    return-void
.end method

.method private a(Lcom/twitter/library/provider/Tweet;)V
    .locals 3

    iget-object v0, p1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/so;->b:Ljava/util/HashSet;

    iget-object v2, v0, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->o(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V
    .locals 8

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/sk;

    iget-object v1, v0, Lcom/twitter/android/sk;->l:Lcom/twitter/android/sp;

    iget-object v5, v0, Lcom/twitter/android/sk;->j:Lcom/twitter/android/widget/TopicView;

    const-string/jumbo v2, "page"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iget-wide v3, v1, Lcom/twitter/android/sp;->a:J

    const-wide/16 v6, 0xa

    mul-long/2addr v3, v6

    int-to-long v6, v2

    add-long/2addr v3, v6

    iget-object v6, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v6}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/SearchResultsFragment;)Ljava/util/HashSet;

    move-result-object v6

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget v3, v1, Lcom/twitter/android/sp;->b:I

    packed-switch v3, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    const-string/jumbo v1, "position"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->n(Lcom/twitter/android/SearchResultsFragment;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void

    :pswitch_1
    iget-object v0, v0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/so;->a(Lcom/twitter/library/provider/Tweet;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->b(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const-string/jumbo v1, "reason_text"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->B:Ljava/lang/String;

    goto :goto_0

    :pswitch_2
    iget-object v0, v0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/so;->a(Lcom/twitter/library/provider/Tweet;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->c(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    const-string/jumbo v3, "news"

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, v0, Lcom/twitter/android/sk;->a:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/so;->a(Lcom/twitter/library/provider/Tweet;)V

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->d(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    const-string/jumbo v3, "highlight"

    invoke-static {v1, v0, v2, v3}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    check-cast p1, Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/so;->b:Ljava/util/HashSet;

    iget-object v3, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->e(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v3, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/widget/UserView;->getUserId()J

    move-result-wide v2

    const/4 v0, 0x0

    const-string/jumbo v4, "list"

    invoke-static {v2, v3, v1, v0, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, v0, Lcom/twitter/android/sk;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getAdapter()Landroid/support/v4/view/PagerAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yu;

    invoke-virtual {v0, v2}, Lcom/twitter/android/yu;->b(I)Lcom/twitter/android/yz;

    move-result-object v0

    iget v1, v0, Lcom/twitter/android/yz;->a:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    iget-object v1, v0, Lcom/twitter/android/yz;->e:Lcom/twitter/library/api/PromotedContent;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/so;->b:Ljava/util/HashSet;

    iget-object v2, v0, Lcom/twitter/android/yz;->e:Lcom/twitter/library/api/PromotedContent;

    iget-object v2, v2, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->f(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    iget-object v3, v0, Lcom/twitter/android/yz;->e:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_2
    iget-wide v1, v0, Lcom/twitter/android/yz;->d:J

    iget-object v0, v0, Lcom/twitter/android/yz;->e:Lcom/twitter/library/api/PromotedContent;

    const/4 v3, 0x0

    const-string/jumbo v4, "list"

    invoke-static {v1, v2, v0, v3, v4}, Lcom/twitter/library/scribe/ScribeItem;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_0

    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    :pswitch_6
    iget-object v0, v1, Lcom/twitter/android/sp;->i:Ljava/lang/String;

    const-string/jumbo v1, "related_query"

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_7
    iget-object v0, v1, Lcom/twitter/android/sp;->h:Lcom/twitter/library/api/TwitterSearchSuggestion;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterSearchSuggestion;->query:Ljava/lang/String;

    const-string/jumbo v1, "spelling_correction"

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_8
    new-instance v0, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    const/16 v1, 0x10

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->c:I

    goto/16 :goto_0

    :pswitch_9
    instance-of v0, v5, Lcom/twitter/android/widget/CollectionView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SearchResultsFragment;->M()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "universal_top"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->h(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->g(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "search"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v0, v4, v5

    const/4 v0, 0x2

    const-string/jumbo v5, "timeline_gallery"

    aput-object v5, v4, v0

    const/4 v0, 0x3

    const-string/jumbo v5, ""

    aput-object v5, v4, v0

    const/4 v0, 0x4

    const-string/jumbo v5, "impression"

    aput-object v5, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_5
    new-instance v6, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v6}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    const/16 v0, 0x10

    iput v0, v6, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iget-object v0, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v0}, Lcom/twitter/android/SearchResultsFragment;->i(Lcom/twitter/android/SearchResultsFragment;)Z

    move-result v0

    if-nez v0, :cond_6

    const-string/jumbo v0, "single"

    iput-object v0, v6, Lcom/twitter/library/scribe/ScribeItem;->v:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v0}, Lcom/twitter/android/SearchResultsFragment;->j(Lcom/twitter/android/SearchResultsFragment;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    move-object v0, v6

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-virtual {v5}, Lcom/twitter/android/widget/TopicView;->getTopicType()I

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/SearchResultsFragment;->a(Lcom/twitter/android/SearchResultsFragment;I)I

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->p()Z

    move-result v0

    if-eqz v0, :cond_7

    instance-of v0, v5, Lcom/twitter/android/widget/EventView;

    if-eqz v0, :cond_7

    move-object v0, v5

    check-cast v0, Lcom/twitter/android/widget/EventView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/EventView;->getViewUrl()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SearchResultsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v5}, Lcom/twitter/android/widget/TopicView;->getTopicId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->k(Lcom/twitter/android/SearchResultsFragment;)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-virtual {v3}, Lcom/twitter/android/SearchResultsFragment;->C()Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v4, "show"

    check-cast v5, Lcom/twitter/android/widget/EventView;

    invoke-virtual {v5}, Lcom/twitter/android/widget/EventView;->getTweetCount()I

    move-result v5

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/widget/EventView;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    move-object v0, v6

    goto/16 :goto_0

    :pswitch_a
    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->l(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "search:universal_top::recommendation:show"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v2}, Lcom/twitter/android/SearchResultsFragment;->j(Lcom/twitter/android/SearchResultsFragment;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    iget-object v2, v2, Lcom/twitter/android/SearchResultsFragment;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    const/16 v2, 0xc

    iput v2, v1, Lcom/twitter/library/scribe/ScribeItem;->c:I

    iget-object v2, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    iget-object v2, v2, Lcom/twitter/android/SearchResultsFragment;->o:Ljava/lang/String;

    iput-object v2, v1, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/so;->a:Lcom/twitter/android/SearchResultsFragment;

    invoke-static {v1}, Lcom/twitter/android/SearchResultsFragment;->m(Lcom/twitter/android/SearchResultsFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_7
    move-object v0, v6

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_4
        :pswitch_7
        :pswitch_6
        :pswitch_2
        :pswitch_0
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_a
        :pswitch_0
        :pswitch_9
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method
