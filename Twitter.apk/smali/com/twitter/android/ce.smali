.class Lcom/twitter/android/ce;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/CardPreviewerFragment;)V
    .locals 1

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/ce;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private a(Lcom/twitter/android/CardPreviewerFragment;Lcom/twitter/library/network/aa;Lcom/twitter/library/network/n;)Lcom/twitter/library/card/instance/CardInstanceData;
    .locals 10

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/twitter/android/CardPreviewerFragment;->c(Lcom/twitter/android/CardPreviewerFragment;)Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/android/ca;

    invoke-direct {v4, p1, v3}, Lcom/twitter/android/ca;-><init>(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    if-nez v5, :cond_0

    :goto_0
    return-object v2

    :cond_0
    invoke-virtual {v5}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move v0, v1

    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " GET"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V

    new-instance v8, Lcom/twitter/library/network/d;

    invoke-direct {v8, v6, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v8, p3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v8

    invoke-virtual {v8, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v8

    iget v8, v8, Lcom/twitter/internal/network/k;->a:I

    const/16 v9, 0xc8

    if-ne v8, v9, :cond_1

    iget-object v0, v4, Lcom/twitter/android/ca;->a:Lcom/twitter/library/card/instance/CardInstanceData;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " HTTP 200 OK"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V

    :goto_2
    move-object v2, v0

    goto :goto_0

    :cond_1
    const/16 v9, 0xca

    if-ne v8, v9, :cond_3

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " HTTP 202 Retry"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {p1, v8}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;)V

    const/16 v8, 0xf

    if-ge v0, v8, :cond_2

    add-int/lit8 v0, v0, 0x1

    const-wide/16 v8, 0x7d0

    :try_start_0
    invoke-static {v8, v9}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v8

    goto :goto_1

    :cond_2
    const v0, 0x7f0f00fb    # com.twitter.android.R.string.developer_card_previewer_error_timeout

    invoke-virtual {v5, v0}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v3, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_2

    :cond_3
    const v0, 0x7f0f00f9    # com.twitter.android.R.string.developer_card_previewer_error_connection

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v1

    invoke-virtual {v5, v0, v4}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v3, v0}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Ljava/lang/String;Ljava/lang/String;)V

    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/library/card/instance/CardInstanceData;
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/ce;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CardPreviewerFragment;

    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    if-nez v2, :cond_1

    move-object v0, v1

    goto :goto_0

    :cond_1
    invoke-virtual {v2}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-static {v1}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v1

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/network/n;

    invoke-direct {v3, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-direct {p0, v0, v1, v3}, Lcom/twitter/android/ce;->a(Lcom/twitter/android/CardPreviewerFragment;Lcom/twitter/library/network/aa;Lcom/twitter/library/network/n;)Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/card/instance/CardInstanceData;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ce;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CardPreviewerFragment;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/CardPreviewerFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-eqz v1, :cond_0

    if-eqz p1, :cond_2

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->g(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iput-object p1, v2, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/card/Card;)V

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->g(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iput-wide v3, v2, Lcom/twitter/library/provider/Tweet;->q:J

    new-instance v1, Lcom/twitter/library/api/TwitterStatusCard;

    invoke-direct {v1}, Lcom/twitter/library/api/TwitterStatusCard;-><init>()V

    iput-object v1, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v1, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iput-object p1, v1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/card/instance/CardInstanceData;)V

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->i(Lcom/twitter/android/CardPreviewerFragment;)Landroid/view/ViewGroup;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/cf;->a(Landroid/view/ViewGroup;)V

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v1

    const v2, 0x7f030024    # com.twitter.android.R.layout.card_view

    invoke-virtual {v1, v2}, Lcom/twitter/android/cf;->a(I)V

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v2

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->j(Lcom/twitter/android/CardPreviewerFragment;)Z

    move-result v3

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v4

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;ZLcom/twitter/library/card/j;Lcom/twitter/android/card/q;)Lcom/twitter/android/card/p;

    move-result-object v1

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/cf;->a(Lcom/twitter/android/card/p;)V

    goto :goto_0

    :cond_2
    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->k(Lcom/twitter/android/CardPreviewerFragment;)V

    goto :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ce;->a([Ljava/lang/Void;)Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ce;->a(Lcom/twitter/library/card/instance/CardInstanceData;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/ce;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/CardPreviewerFragment;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/CardPreviewerFragment;->a(Lcom/twitter/android/CardPreviewerFragment;Z)V

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->d(Lcom/twitter/android/CardPreviewerFragment;)V

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->e(Lcom/twitter/android/CardPreviewerFragment;)V

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->f(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/android/cf;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/twitter/android/cf;->a(Lcom/twitter/library/card/Card;)V

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->g(Lcom/twitter/android/CardPreviewerFragment;)Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iput-object v2, v1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-static {v0}, Lcom/twitter/android/CardPreviewerFragment;->h(Lcom/twitter/android/CardPreviewerFragment;)V

    goto :goto_0
.end method
