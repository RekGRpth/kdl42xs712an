.class final Lcom/twitter/android/ut;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/zc;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/ui;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ut;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)J
    .locals 2

    sget v0, Lcom/twitter/library/provider/cc;->q:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public a(Landroid/database/Cursor;I)Lcom/twitter/library/scribe/ScribeItem;
    .locals 6

    sget v0, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TimelineScribeContent;

    invoke-virtual {p0, p1}, Lcom/twitter/android/ut;->a(Landroid/database/Cursor;)J

    move-result-wide v2

    const-string/jumbo v1, "user_gallery"

    const/4 v4, 0x3

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    return-object v0
.end method

.method public b(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->r:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->s:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public d(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->u:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e(Landroid/database/Cursor;)Ljava/lang/CharSequence;
    .locals 3

    const v2, -0x777778

    sget v0, Lcom/twitter/library/provider/cc;->v:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    sget v0, Lcom/twitter/library/provider/cc;->w:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_0

    invoke-static {v1, v0, v2, v2}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;II)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public f(Landroid/database/Cursor;)Z
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->x:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g(Landroid/database/Cursor;)Z
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->x:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h(Landroid/database/Cursor;)Lcom/twitter/library/api/PromotedContent;
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->L:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/PromotedContent;

    return-object v0
.end method

.method public i(Landroid/database/Cursor;)I
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->y:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public j(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->B:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public k(Landroid/database/Cursor;)I
    .locals 1

    sget v0, Lcom/twitter/library/provider/cc;->A:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public l(Landroid/database/Cursor;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
