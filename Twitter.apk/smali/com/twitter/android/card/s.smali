.class public Lcom/twitter/android/card/s;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Landroid/content/Context;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/high16 v0, 0x500000

    const/high16 v1, 0xa00000

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/card/s;-><init>(Landroid/content/Context;I)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/card/s;->a:Landroid/content/Context;

    iput p2, p0, Lcom/twitter/android/card/s;->b:I

    return-void
.end method

.method private a()Lld;
    .locals 7

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/twitter/android/card/s;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;)Ljava/io/File;

    move-result-object v2

    if-nez v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/card/s;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    iget v1, p0, Lcom/twitter/android/card/s;->b:I

    const/high16 v3, 0x500000

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    :goto_0
    new-instance v3, Ljava/io/File;

    const-string/jumbo v4, "card"

    invoke-direct {v3, v2, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const/4 v2, 0x3

    const/4 v4, 0x1

    int-to-long v5, v1

    invoke-static {v3, v2, v4, v5, v6}, Lld;->a(Ljava/io/File;IIJ)Lld;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_0
    iget v1, p0, Lcom/twitter/android/card/s;->b:I

    const/high16 v3, 0xa00000

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_1
.end method

.method private a(Lld;)V
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Lld;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/twitter/library/card/Card;
    .locals 6

    const/4 v1, 0x0

    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    move-object v0, v1

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/twitter/android/card/s;->a()Lld;

    move-result-object v4

    if-eqz v4, :cond_2

    invoke-static {p1}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    const/4 v2, 0x0

    :try_start_2
    invoke-virtual {v4, v0}, Lld;->a(Ljava/lang/String;)Lli;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move-result-object v3

    if-eqz v3, :cond_1

    :try_start_3
    new-instance v2, Ljava/io/ObjectInputStream;

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Lli;->a(I)Ljava/io/InputStream;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/ClassNotFoundException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    :try_start_4
    invoke-virtual {v2}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/Card;

    iget v5, p0, Lcom/twitter/android/card/s;->c:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/twitter/android/card/s;->c:I
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/lang/ClassNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catchall {:try_start_4 .. :try_end_4} :catchall_3

    :try_start_5
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/card/s;->a(Lld;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    :try_start_6
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/card/s;->a(Lld;)V

    :cond_2
    :goto_1
    iget v0, p0, Lcom/twitter/android/card/s;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/card/s;->d:I

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v0

    move-object v0, v1

    move-object v2, v1

    :goto_2
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/card/s;->a(Lld;)V

    goto :goto_1

    :catchall_1
    move-exception v0

    move-object v2, v1

    move-object v3, v1

    :goto_3
    invoke-static {v2}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-static {v3}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    invoke-direct {p0, v4}, Lcom/twitter/android/card/s;->a(Lld;)V

    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catchall_2
    move-exception v0

    move-object v2, v1

    goto :goto_3

    :catchall_3
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    move-object v0, v1

    move-object v2, v3

    goto :goto_2

    :catch_2
    move-exception v0

    move-object v0, v2

    move-object v2, v3

    goto :goto_2
.end method

.method public a(Ljava/lang/String;Lcom/twitter/library/card/Card;)V
    .locals 7

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/card/s;->a()Lld;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/twitter/library/util/Util;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    :try_start_0
    invoke-virtual {v3, v2}, Lld;->b(Ljava/lang/String;)Llf;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_6

    :try_start_1
    new-instance v0, Ljava/io/ObjectOutputStream;

    new-instance v4, Ljava/io/BufferedOutputStream;

    const/4 v5, 0x0

    invoke-virtual {v2, v5}, Llf;->a(I)Ljava/io/OutputStream;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/io/BufferedOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-direct {v0, v4}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_4
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    invoke-virtual {v0, p2}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v0}, Ljava/io/ObjectOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    const/4 v1, 0x1

    :goto_1
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    if-eqz v2, :cond_2

    if-eqz v1, :cond_3

    :try_start_3
    invoke-virtual {v2}, Llf;->a()V

    invoke-virtual {v3}, Lld;->b()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :cond_2
    :goto_2
    invoke-direct {p0, v3}, Lcom/twitter/android/card/s;->a(Lld;)V

    goto :goto_0

    :cond_3
    :try_start_4
    invoke-virtual {v2}, Llf;->b()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_2

    :catch_1
    move-exception v0

    move-object v0, v1

    :goto_3
    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    if-eqz v1, :cond_4

    :try_start_5
    invoke-virtual {v1}, Llf;->b()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    :cond_4
    :goto_4
    invoke-direct {p0, v3}, Lcom/twitter/android/card/s;->a(Lld;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    move-object v2, v1

    :goto_5
    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    if-eqz v2, :cond_5

    :try_start_6
    invoke-virtual {v2}, Llf;->b()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_2

    :cond_5
    :goto_6
    invoke-direct {p0, v3}, Lcom/twitter/android/card/s;->a(Lld;)V

    throw v0

    :catch_2
    move-exception v1

    goto :goto_6

    :catchall_1
    move-exception v0

    goto :goto_5

    :catchall_2
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    goto :goto_5

    :catch_3
    move-exception v0

    goto :goto_4

    :catch_4
    move-exception v0

    move-object v0, v1

    move-object v1, v2

    goto :goto_3

    :catch_5
    move-exception v1

    move-object v1, v2

    goto :goto_3

    :cond_6
    move-object v6, v1

    move v1, v0

    move-object v0, v6

    goto :goto_1
.end method
