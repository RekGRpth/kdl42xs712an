.class Lcom/twitter/android/card/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Landroid/graphics/drawable/Drawable;

.field final synthetic d:I

.field final synthetic e:Lcom/twitter/android/card/b;


# direct methods
.method constructor <init>(Lcom/twitter/android/card/b;ZZLandroid/graphics/drawable/Drawable;I)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    iput-boolean p2, p0, Lcom/twitter/android/card/c;->a:Z

    iput-boolean p3, p0, Lcom/twitter/android/card/c;->b:Z

    iput-object p4, p0, Lcom/twitter/android/card/c;->c:Landroid/graphics/drawable/Drawable;

    iput p5, p0, Lcom/twitter/android/card/c;->d:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    iget-object v0, v0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    iget-boolean v1, p0, Lcom/twitter/android/card/c;->a:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    iget-object v0, v0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    iget-boolean v1, p0, Lcom/twitter/android/card/c;->b:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    iget-object v0, v0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    iget-object v1, p0, Lcom/twitter/android/card/c;->c:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2, v2, v2}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setCompoundDrawablesWithIntrinsicBounds(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    invoke-static {v0}, Lcom/twitter/android/card/b;->a(Lcom/twitter/android/card/b;)Lcom/twitter/library/card/element/FollowButtonElement;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    sget-object v1, Lcom/twitter/library/card/element/FollowButtonElement$Kind;->b:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    iget-object v0, v0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    iget v1, p0, Lcom/twitter/android/card/c;->d:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/c;->e:Lcom/twitter/android/card/b;

    iget-object v0, v0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
