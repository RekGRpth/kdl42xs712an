.class public Lcom/twitter/android/card/i;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/util/LinkedList;

.field private b:I

.field private c:I

.field private d:I


# direct methods
.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/card/i;->a:Ljava/util/LinkedList;

    iput p1, p0, Lcom/twitter/android/card/i;->b:I

    return-void
.end method


# virtual methods
.method public declared-synchronized a(Ljava/lang/String;)Lcom/twitter/library/card/Card;
    .locals 3

    monitor-enter p0

    if-eqz p1, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/card/i;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/card/i;->a:Ljava/util/LinkedList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/ListIterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/ListIterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/card/j;

    iget-object v2, v0, Lcom/twitter/android/card/j;->a:Ljava/lang/String;

    invoke-virtual {v2, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/ListIterator;->remove()V

    iget v1, p0, Lcom/twitter/android/card/i;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/android/card/i;->c:I

    iget-object v0, v0, Lcom/twitter/android/card/j;->b:Lcom/twitter/library/card/Card;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_0
    monitor-exit p0

    return-object v0

    :cond_1
    :try_start_1
    iget v0, p0, Lcom/twitter/android/card/i;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/twitter/android/card/i;->d:I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(Ljava/lang/String;Lcom/twitter/library/card/Card;)V
    .locals 2

    monitor-enter p0

    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    :try_start_0
    iget v0, p0, Lcom/twitter/android/card/i;->b:I

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/card/i;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/card/i;->b:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/i;->a:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/i;->a:Ljava/util/LinkedList;

    new-instance v1, Lcom/twitter/android/card/j;

    invoke-direct {v1, p1, p2}, Lcom/twitter/android/card/j;-><init>(Ljava/lang/String;Lcom/twitter/library/card/Card;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
