.class public Lcom/twitter/android/card/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/card/element/a;


# static fields
.field private static b:I

.field private static c:I

.field private static d:Lcom/twitter/android/card/n;


# instance fields
.field volatile a:Lcom/twitter/library/card/Card;

.field private e:Landroid/content/Context;

.field private f:Lcom/twitter/android/card/s;

.field private g:Lcom/twitter/android/card/i;

.field private h:Lcom/twitter/android/card/r;

.field private i:I

.field private j:I

.field private k:Ljava/lang/Integer;

.field private l:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    sput v0, Lcom/twitter/android/card/n;->b:I

    const/4 v0, 0x5

    sput v0, Lcom/twitter/android/card/n;->c:I

    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/16 v1, 0x2710

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/card/n;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    iput-object p1, p0, Lcom/twitter/android/card/n;->e:Landroid/content/Context;

    new-instance v0, Lcom/twitter/android/card/s;

    invoke-direct {v0, p1}, Lcom/twitter/android/card/s;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/card/n;->f:Lcom/twitter/android/card/s;

    new-instance v0, Lcom/twitter/android/card/i;

    sget v1, Lcom/twitter/android/card/n;->b:I

    invoke-direct {v0, v1}, Lcom/twitter/android/card/i;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/card/n;->g:Lcom/twitter/android/card/i;

    new-instance v0, Lcom/twitter/android/card/r;

    sget v1, Lcom/twitter/android/card/n;->c:I

    invoke-direct {v0, v1}, Lcom/twitter/android/card/r;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/card/n;->h:Lcom/twitter/android/card/r;

    invoke-static {p0}, Lcom/twitter/library/card/element/Element;->a(Lcom/twitter/library/card/element/a;)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/n;->e:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "debug_show_card_overlay"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/card/Card;->a(Z)V

    :cond_0
    return-void
.end method

.method public static a()Lcom/twitter/android/card/n;
    .locals 1

    sget-object v0, Lcom/twitter/android/card/n;->d:Lcom/twitter/android/card/n;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 1

    new-instance v0, Lcom/twitter/android/card/n;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/n;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/twitter/android/card/n;->d:Lcom/twitter/android/card/n;

    return-void
.end method

.method public static c(I)Z
    .locals 1

    const/16 v0, 0x9

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Z
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Lcom/twitter/android/card/n;->c(I)Z

    move-result v0

    return v0
.end method

.method public static d(I)Z
    .locals 1

    const/16 v0, 0x10

    if-lt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static e()Z
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-static {v0}, Lcom/twitter/android/card/n;->d(I)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/card/k;ZLcom/twitter/library/card/j;Lcom/twitter/android/card/q;)Lcom/twitter/android/card/p;
    .locals 8

    new-instance v0, Lcom/twitter/android/card/k;

    iget-object v1, p0, Lcom/twitter/android/card/n;->f:Lcom/twitter/android/card/s;

    iget-object v2, p0, Lcom/twitter/android/card/n;->g:Lcom/twitter/android/card/i;

    iget-object v3, p0, Lcom/twitter/android/card/n;->h:Lcom/twitter/android/card/r;

    new-instance v7, Lcom/twitter/android/card/o;

    invoke-direct {v7, p0, p4}, Lcom/twitter/android/card/o;-><init>(Lcom/twitter/android/card/n;Lcom/twitter/android/card/q;)V

    move-object v4, p1

    move v5, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/card/k;-><init>(Lcom/twitter/android/card/s;Lcom/twitter/android/card/i;Lcom/twitter/android/card/r;Lcom/twitter/library/card/k;ZLcom/twitter/library/card/j;Lcom/twitter/android/card/m;)V

    new-instance v1, Lcom/twitter/android/card/p;

    invoke-direct {v1, v0}, Lcom/twitter/android/card/p;-><init>(Lcom/twitter/android/card/k;)V

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/k;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-object v1
.end method

.method public a(I)Lcom/twitter/library/card/CardState;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/n;->h:Lcom/twitter/android/card/r;

    invoke-virtual {v0, p1}, Lcom/twitter/android/card/r;->a(I)Lcom/twitter/library/card/CardState;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)Lcom/twitter/library/card/element/d;
    .locals 1

    new-instance v0, Lcom/twitter/android/card/b;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/card/b;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)Lcom/twitter/library/card/element/h;
    .locals 3

    invoke-virtual {p2}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->c()Z

    move-result v1

    const/4 v0, 0x0

    if-nez v1, :cond_0

    iget-object v1, p2, Lcom/twitter/library/card/element/Player;->htmlUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->l(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/twitter/library/util/Util;->b()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Lcom/twitter/android/card/ab;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/card/ab;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v1, p2, Lcom/twitter/library/card/element/Player;->streamUrl:Ljava/lang/String;

    invoke-static {v1}, Lcom/twitter/library/util/Util;->m(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v0, Lcom/twitter/library/amplify/o;

    invoke-direct {v0, p1, p2}, Lcom/twitter/library/amplify/o;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "animated_gif"

    invoke-virtual {p2}, Lcom/twitter/library/card/element/Player;->d()Lcom/twitter/library/card/Card;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/card/Card;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/android/card/n;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/android/card/n;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/android/card/w;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/card/w;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lcom/twitter/android/card/aa;

    invoke-direct {v0, p1, p2}, Lcom/twitter/android/card/aa;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    goto :goto_0
.end method

.method public a(ILcom/twitter/library/card/CardState;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/n;->h:Lcom/twitter/android/card/r;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/card/r;->a(ILcom/twitter/library/card/CardState;)V

    return-void
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/n;->a:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/n;->a:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->h()V

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/j;)V

    iput-object p1, p0, Lcom/twitter/android/card/n;->a:Lcom/twitter/library/card/Card;

    return-void
.end method

.method public a(Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V
    .locals 2

    if-eqz p2, :cond_0

    new-instance v0, Lcom/twitter/android/card/t;

    iget-object v1, p0, Lcom/twitter/android/card/n;->g:Lcom/twitter/android/card/i;

    invoke-direct {v0, v1, p1, p2}, Lcom/twitter/android/card/t;-><init>(Lcom/twitter/android/card/i;Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/t;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method public a(II)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/card/n;->i:I

    if-nez v0, :cond_0

    iput p1, p0, Lcom/twitter/android/card/n;->i:I

    iput p2, p0, Lcom/twitter/android/card/n;->j:I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/n;->l:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndIncrement()I

    move-result v0

    return v0
.end method

.method public b(I)Ljava/lang/Integer;
    .locals 3

    const/4 v1, 0x0

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/android/card/n;->i:I

    if-ne v0, p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/n;->k:Ljava/lang/Integer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/n;->k:Ljava/lang/Integer;

    iput v2, p0, Lcom/twitter/android/card/n;->i:I

    iput v2, p0, Lcom/twitter/android/card/n;->j:I

    iput-object v1, p0, Lcom/twitter/android/card/n;->k:Ljava/lang/Integer;

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method public b(II)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/card/n;->j:I

    if-ne p1, v0, :cond_0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/n;->k:Ljava/lang/Integer;

    :cond_0
    return-void
.end method

.method public c()Lcom/twitter/library/card/Card;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/n;->a:Lcom/twitter/library/card/Card;

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/card/n;->a:Lcom/twitter/library/card/Card;

    return-object v0
.end method
