.class public Lcom/twitter/android/card/PlayerDelegateVineView;
.super Lcom/twitter/library/card/element/PlayerDelegateView;
.source "Twttr"


# instance fields
.field public a:Lco/vine/android/player/SdkVideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/PlayerDelegateView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    new-instance v0, Lco/vine/android/player/SdkVideoView;

    invoke-direct {v0, p1}, Lco/vine/android/player/SdkVideoView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/card/PlayerDelegateVineView;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/PlayerDelegateVineView;->a(Landroid/content/Context;)Z

    return-void
.end method


# virtual methods
.method public B_()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/PlayerDelegateView;->B_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/PlayerDelegateVineView;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/PlayerDelegateVineView;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/twitter/library/card/element/PlayerDelegateView;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/twitter/android/card/PlayerDelegateVineView;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0, p2, p3, p4, p5}, Lco/vine/android/player/SdkVideoView;->layout(IIII)V

    goto :goto_0
.end method
