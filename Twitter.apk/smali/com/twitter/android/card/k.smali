.class public Lcom/twitter/android/card/k;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/card/s;

.field private b:Lcom/twitter/android/card/i;

.field private c:Lcom/twitter/android/card/r;

.field private d:Lcom/twitter/library/card/k;

.field private e:Lcom/twitter/library/card/j;

.field private f:Ljava/lang/String;

.field private g:Lcom/twitter/android/card/m;

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/card/s;Lcom/twitter/android/card/i;Lcom/twitter/android/card/r;Lcom/twitter/library/card/k;ZLcom/twitter/library/card/j;Lcom/twitter/android/card/m;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/card/k;->a:Lcom/twitter/android/card/s;

    iput-object p2, p0, Lcom/twitter/android/card/k;->b:Lcom/twitter/android/card/i;

    iput-object p3, p0, Lcom/twitter/android/card/k;->c:Lcom/twitter/android/card/r;

    iput-object p4, p0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    iput-object p6, p0, Lcom/twitter/android/card/k;->e:Lcom/twitter/library/card/j;

    iput-boolean p5, p0, Lcom/twitter/android/card/k;->h:Z

    iput-object p7, p0, Lcom/twitter/android/card/k;->g:Lcom/twitter/android/card/m;

    return-void
.end method

.method static synthetic a(Ljava/io/InputStream;)Lcom/twitter/library/card/Card;
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/card/k;->b(Ljava/io/InputStream;)Lcom/twitter/library/card/Card;

    move-result-object v0

    return-object v0
.end method

.method private static b(Ljava/io/InputStream;)Lcom/twitter/library/card/Card;
    .locals 2

    const/4 v1, 0x0

    :try_start_0
    invoke-static {p0}, Lcom/twitter/library/api/ap;->a(Ljava/io/InputStream;)Lcom/fasterxml/jackson/core/JsonParser;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/card/l;->a(Lcom/fasterxml/jackson/core/JsonParser;)Lcom/twitter/library/card/Card;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    return-object v0

    :catchall_0
    move-exception v0

    invoke-static {v1}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    throw v0
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/library/card/Card;
    .locals 17

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/card/k;->isCancelled()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v1}, Lcom/twitter/library/card/k;->t()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v13

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v1}, Lcom/twitter/library/card/k;->q()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v3}, Lcom/twitter/library/card/k;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v1, v1, Lcom/twitter/library/card/instance/CardInstanceData;->forwardCardTypeURL:Ljava/lang/String;

    :goto_1
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v3}, Lcom/twitter/library/card/k;->r()Landroid/view/ViewGroup;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v4}, Lcom/twitter/library/card/k;->s()I

    move-result v4

    invoke-static {v2}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v7}, Lcom/twitter/library/card/k;->n()Lcom/twitter/library/provider/Tweet;

    move-result-object v7

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v8}, Lcom/twitter/library/card/k;->u()Lcom/twitter/library/util/aa;

    move-result-object v8

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v9}, Lcom/twitter/library/card/k;->v()Lcom/twitter/library/util/as;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/twitter/android/card/k;->c:Lcom/twitter/android/card/r;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v12}, Lcom/twitter/library/card/k;->m()I

    move-result v12

    invoke-virtual {v10, v12}, Lcom/twitter/android/card/r;->a(I)Lcom/twitter/library/card/CardState;

    move-result-object v10

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/card/k;->b:Lcom/twitter/android/card/i;

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v12}, Lcom/twitter/library/card/k;->c()Z

    move-result v12

    if-eqz v12, :cond_3

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->b:Lcom/twitter/android/card/i;

    invoke-virtual {v11, v1}, Lcom/twitter/android/card/i;->a(Ljava/lang/String;)Lcom/twitter/library/card/Card;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/card/k;->isCancelled()Z

    move-result v12

    if-eqz v12, :cond_3

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_2
    iget-object v1, v1, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    goto :goto_1

    :cond_3
    if-nez v11, :cond_c

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/card/k;->a:Lcom/twitter/android/card/s;

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v12}, Lcom/twitter/library/card/k;->d()Z

    move-result v12

    if-eqz v12, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->a:Lcom/twitter/android/card/s;

    invoke-virtual {v11, v1}, Lcom/twitter/android/card/s;->a(Ljava/lang/String;)Lcom/twitter/library/card/Card;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/card/k;->isCancelled()Z

    move-result v12

    if-eqz v12, :cond_4

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_4
    move-object v12, v11

    if-nez v12, :cond_b

    const/4 v11, 0x0

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/twitter/android/card/k;->h:Z

    if-eqz v14, :cond_5

    invoke-static {v13}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v14

    invoke-virtual {v14}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v14

    if-eqz v14, :cond_5

    invoke-static {v1}, Lcom/twitter/library/util/Util;->b(Ljava/lang/String;)Ljava/net/URI;

    move-result-object v15

    if-eqz v15, :cond_5

    invoke-virtual {v15}, Ljava/net/URI;->getHost()Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_5

    invoke-virtual {v15}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v15

    const-string/jumbo v16, ".twitter.com"

    invoke-virtual/range {v15 .. v16}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v15

    if-eqz v15, :cond_5

    invoke-virtual {v14}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v14

    new-instance v11, Lcom/twitter/library/network/n;

    invoke-direct {v11, v14}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    :cond_5
    new-instance v14, Lcom/twitter/android/card/l;

    invoke-direct {v14, v1}, Lcom/twitter/android/card/l;-><init>(Ljava/lang/String;)V

    new-instance v15, Lcom/twitter/library/network/d;

    invoke-direct {v15, v13, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v15, v11}, Lcom/twitter/library/network/d;->b(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v11

    invoke-virtual {v11, v14}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v11

    invoke-virtual {v11}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v11

    invoke-virtual {v11}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v11

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/card/k;->isCancelled()Z

    move-result v13

    if-eqz v13, :cond_6

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_6
    invoke-virtual {v11}, Lcom/twitter/internal/network/HttpOperation;->h()Ljava/net/URI;

    move-result-object v13

    invoke-virtual {v13}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-virtual {v11}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v15

    if-nez v15, :cond_7

    invoke-virtual {v11}, Lcom/twitter/internal/network/HttpOperation;->l()Lcom/twitter/internal/network/k;

    move-result-object v1

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    iget v14, v1, Lcom/twitter/internal/network/k;->a:I

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v14, " "

    invoke-virtual {v11, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget-object v1, v1, Lcom/twitter/internal/network/k;->b:Ljava/lang/String;

    invoke-virtual {v11, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, " "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/card/k;->f:Ljava/lang/String;

    move-object v1, v12

    :goto_2
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/card/k;->isCancelled()Z

    move-result v11

    if-eqz v11, :cond_a

    const/4 v1, 0x0

    goto/16 :goto_0

    :cond_7
    iget-object v11, v14, Lcom/twitter/android/card/l;->a:Lcom/twitter/library/card/Card;

    if-nez v11, :cond_8

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v11, v14, Lcom/twitter/android/card/l;->b:Ljava/lang/String;

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v11, " "

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/twitter/android/card/k;->f:Ljava/lang/String;

    move-object v1, v12

    goto :goto_2

    :cond_8
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->a:Lcom/twitter/android/card/s;

    if-eqz v11, :cond_9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v11}, Lcom/twitter/library/card/k;->d()Z

    move-result v11

    if-eqz v11, :cond_9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->a:Lcom/twitter/android/card/s;

    iget-object v12, v14, Lcom/twitter/android/card/l;->a:Lcom/twitter/library/card/Card;

    invoke-virtual {v11, v1, v12}, Lcom/twitter/android/card/s;->a(Ljava/lang/String;Lcom/twitter/library/card/Card;)V

    :cond_9
    iget-object v1, v14, Lcom/twitter/android/card/l;->a:Lcom/twitter/library/card/Card;

    goto :goto_2

    :cond_a
    if-eqz v1, :cond_0

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->d:Lcom/twitter/library/card/k;

    invoke-interface {v11}, Lcom/twitter/library/card/k;->b()Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/card/k;->e:Lcom/twitter/library/card/j;

    invoke-virtual/range {v1 .. v12}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Landroid/view/ViewGroup;IJLcom/twitter/library/provider/Tweet;Lcom/twitter/library/util/aa;Lcom/twitter/library/util/as;Lcom/twitter/library/card/CardState;Lcom/twitter/library/card/j;Z)Lcom/twitter/library/card/Card;

    move-result-object v1

    goto/16 :goto_0

    :cond_b
    move-object v1, v12

    goto :goto_2

    :cond_c
    move-object v1, v11

    goto :goto_2
.end method

.method protected a(Lcom/twitter/library/card/Card;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/k;->g:Lcom/twitter/android/card/m;

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/card/k;->g:Lcom/twitter/android/card/m;

    invoke-interface {v0, p1}, Lcom/twitter/android/card/m;->a(Lcom/twitter/library/card/Card;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/k;->g:Lcom/twitter/android/card/m;

    iget-object v1, p0, Lcom/twitter/android/card/k;->f:Ljava/lang/String;

    invoke-interface {v0, v1}, Lcom/twitter/android/card/m;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected b(Lcom/twitter/library/card/Card;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/k;->g:Lcom/twitter/android/card/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/k;->g:Lcom/twitter/android/card/m;

    invoke-interface {v0}, Lcom/twitter/android/card/m;->a()V

    :cond_0
    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/k;->a([Ljava/lang/Void;)Lcom/twitter/library/card/Card;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onCancelled(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/card/Card;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/k;->b(Lcom/twitter/library/card/Card;)V

    return-void
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/card/Card;

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/k;->a(Lcom/twitter/library/card/Card;)V

    return-void
.end method
