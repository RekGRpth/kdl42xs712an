.class public abstract Lcom/twitter/android/card/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/card/h;
.implements Lcom/twitter/android/client/a;
.implements Lcom/twitter/library/amplify/h;
.implements Lcom/twitter/library/card/j;
.implements Lcom/twitter/library/card/k;


# instance fields
.field protected b:I

.field protected c:Ljava/lang/ref/WeakReference;

.field protected d:Lcom/twitter/library/provider/Tweet;

.field protected e:Lcom/twitter/android/card/p;

.field protected f:Lcom/twitter/library/card/Card;

.field protected g:Lcom/twitter/library/scribe/ScribeAssociation;

.field protected h:Lcom/twitter/library/scribe/ScribeAssociation;

.field protected i:Landroid/content/Context;

.field protected j:Lcom/twitter/android/client/c;

.field protected final k:Lcom/twitter/library/client/aa;

.field protected l:Lcom/twitter/library/card/instance/CardInstanceData;

.field protected m:Landroid/view/ViewGroup;

.field protected n:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    iget-object v0, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    return-void
.end method

.method private A()V
    .locals 3

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->l:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v1, v1, Lcom/twitter/library/card/instance/CardInstanceData;->cardTypeURL:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/card/a;->b:I

    return-void
.end method

.method private a(Lcom/twitter/library/api/PromotedEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    return-void
.end method

.method private i(Ljava/lang/String;)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/twitter/android/card/a;->j(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeLog;->c()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/card/a;->j(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    const-string/jumbo v3, "app_download_client_event"

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v3, "4"

    invoke-virtual {v2, v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "3"

    invoke-virtual {v2, v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method private j(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "tweet::tweet:"

    aput-object v3, v2, v4

    aput-object p1, v2, v5

    invoke-static {v2}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "tweet::tweet::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v6, v1, v2, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0
.end method

.method private k(Ljava/lang/String;)V
    .locals 3

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "open_link"

    invoke-virtual {p0, v1}, Lcom/twitter/android/card/a;->f(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v0, p1, v1, v2}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Ljava/lang/String;J)V

    goto :goto_0
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/card/a;->n:I

    return-void
.end method

.method public a(IILandroid/content/Intent;)V
    .locals 1

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/card/n;->b(II)V

    invoke-virtual {p0}, Lcom/twitter/android/card/a;->w()V

    return-void
.end method

.method public a(J)V
    .locals 1

    const-string/jumbo v0, "follow"

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/card/a;->a(JLjava/lang/String;)V

    return-void
.end method

.method protected a(JLjava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p3}, Lcom/twitter/android/card/a;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->b()Lcom/twitter/library/card/CardState;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/card/CardState;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/card/a;->b:I

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/card/n;->a(ILcom/twitter/library/card/CardState;)V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/ViewGroup;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/a;->m:Landroid/view/ViewGroup;

    return-void
.end method

.method public a(Lcom/twitter/android/card/p;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/a;->e:Lcom/twitter/android/card/p;

    return-void
.end method

.method protected a(Lcom/twitter/android/client/al;Ljava/util/HashMap;Ljava/util/ArrayList;)V
    .locals 5

    if-nez p2, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {p0}, Lcom/twitter/android/card/a;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-virtual {p1, v2, v3}, Lcom/twitter/android/client/al;->a(J)Z

    move-result v4

    if-nez v4, :cond_2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-virtual {p1, v2, v3, v0}, Lcom/twitter/android/client/al;->a(JI)V

    goto :goto_0

    :cond_3
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {p3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    invoke-virtual {p1, v2, v3}, Lcom/twitter/android/client/al;->a(J)Z

    move-result v4

    if-nez v4, :cond_2

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-virtual {p1, v2, v3, v0}, Lcom/twitter/android/client/al;->a(JI)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    iget-object v0, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/element/Element;II)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    iget-object v0, v0, Lcom/twitter/library/card/Card;->actions:Lcom/twitter/library/card/property/a;

    invoke-virtual {v0, p3}, Lcom/twitter/library/card/property/a;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/card/property/Action;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/library/card/property/Action;->scribeAction:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/twitter/library/card/property/Action;->scribeElement:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/card/a;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/card/a;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;)V
    .locals 8

    const/4 v7, 0x0

    new-instance v0, Lcom/twitter/android/card/f;

    iget-object v1, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    move-object v4, p1

    move-object v5, p2

    move-object v6, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/card/f;-><init>(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/Card;Lcom/twitter/library/card/element/Element;Lcom/twitter/library/card/property/Action;Lcom/twitter/android/card/h;Z)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public a(Lcom/twitter/library/card/element/Player;Z)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/card/instance/CardInstanceData;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/a;->l:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-direct {p0}, Lcom/twitter/android/card/a;->A()V

    invoke-virtual {p0}, Lcom/twitter/android/card/a;->x()V

    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0, p1}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/provider/Tweet;ZZ)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 9

    const/4 v5, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    const-string/jumbo v1, "open_link"

    invoke-virtual {p0, v1}, Lcom/twitter/android/card/a;->f(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->t()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2, p1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_2

    iget-object v3, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    if-eqz v3, :cond_2

    const-string/jumbo v3, "impression_id"

    iget-object v1, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-static {v2, v3, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v6, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    new-instance v7, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v7, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v8, "tweet:::platform_promotion_card:open_link"

    aput-object v8, v3, v4

    invoke-virtual {v7, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v7, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v5, v4, v7, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, p1, v5}, Lcom/twitter/library/scribe/ScribeLog;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v5}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v6, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v1

    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    iget-object v4, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v5

    const-string/jumbo v6, "X-Card-Click"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v8, "platform_key="

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v4}, Lcom/twitter/library/network/aa;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v7, "&url="

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v5, Lcom/twitter/library/card/instance/CardInstanceData;->url:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "token"

    invoke-virtual {v2, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "headers"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v7, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v2, p1

    move-object v6, v5

    invoke-static/range {v0 .. v7}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;)V

    goto/16 :goto_0
.end method

.method public a(Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;Lcom/twitter/internal/network/k;)V
    .locals 4

    if-nez p2, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, v0, v1}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v3, "api::::card_request"

    aput-object v3, v0, v1

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Lcom/twitter/internal/network/k;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_1

    new-instance v2, Lcom/twitter/library/util/MediaDescriptor;

    const/4 v3, 0x1

    invoke-direct {v2, p2, v3}, Lcom/twitter/library/util/MediaDescriptor;-><init>(Ljava/lang/String;Z)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_2

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/MediaPlayerActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "image_url"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "aud"

    invoke-virtual {v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "is_looping"

    invoke-virtual {v3, v4, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "simple_controls"

    invoke-virtual {v3, v4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "player_url"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const-string/jumbo v4, "player_stream_urls"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "tweet"

    iget-object v4, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "video_position"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v3, "video_index"

    invoke-virtual {v1, v3, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    :try_start_0
    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    const v1, 0x7f0f0514    # com.twitter.android.R.string.unsupported_feature

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0
.end method

.method public a(Ljava/util/ArrayList;I)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "click"

    invoke-virtual {p0, v1}, Lcom/twitter/android/card/a;->g(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v1}, Lcom/twitter/android/card/a;->a(Lcom/twitter/library/api/PromotedEvent;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/GalleryActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "li"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "list_starting_index"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public b(J)V
    .locals 1

    const-string/jumbo v0, "unfollow"

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/card/a;->a(JLjava/lang/String;)V

    return-void
.end method

.method public b(Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/card/a;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    const-string/jumbo v2, "share"

    invoke-direct {p0, v2}, Lcom/twitter/android/card/a;->i(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, p1, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "share"

    invoke-direct {p0, v1}, Lcom/twitter/android/card/a;->i(Ljava/lang/String;)V

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.SEND"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "text/plain"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setType(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-static {v1, p2}, Landroid/content/Intent;->createChooser(Landroid/content/Intent;Ljava/lang/CharSequence;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public c(J)V
    .locals 7

    const/4 v6, 0x1

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "profile_click"

    invoke-virtual {p0, v1}, Lcom/twitter/android/card/a;->g(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->d:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v1}, Lcom/twitter/android/card/a;->a(Lcom/twitter/library/api/PromotedEvent;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "pc"

    iget-object v3, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v3, v3, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/card/a;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v2, :cond_2

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v5, p0, Lcom/twitter/android/card/a;->h:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v6, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_1
    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v2, :cond_1

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v5, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v6, v3, v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_1
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "share"

    invoke-direct {p0, v1}, Lcom/twitter/android/card/a;->i(Ljava/lang/String;)V

    invoke-static {v0, p1}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "open_app"

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/a;->f(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->m:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/card/a;->a(Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-direct {p0, p1}, Lcom/twitter/android/card/a;->k(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/pm/PackageManager;->getLaunchIntentForPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public d(J)V
    .locals 1

    const-string/jumbo v0, "unblock"

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/card/a;->a(JLjava/lang/String;)V

    return-void
.end method

.method public d(Ljava/lang/String;)V
    .locals 4

    const-string/jumbo v0, "install_app"

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/a;->f(Ljava/lang/String;)V

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->n:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v0}, Lcom/twitter/android/card/a;->a(Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/twitter/library/util/v;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string/jumbo v2, "android.intent.action.VIEW"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    iget-object v2, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->queryIntentActivities(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0, v0}, Lcom/twitter/android/card/a;->k(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/card/n;->b()I

    move-result v3

    iget v4, p0, Lcom/twitter/android/card/a;->b:I

    invoke-virtual {v2, v4, v3}, Lcom/twitter/android/card/n;->a(II)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v4}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v1, v0, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/app/Activity;I)V

    goto :goto_0
.end method

.method protected e(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-static {v1}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object v5, v3, v0

    const/4 v0, 0x2

    aput-object v1, v3, v0

    const/4 v0, 0x3

    aput-object p1, v3, v0

    const/4 v0, 0x4

    aput-object p2, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->i:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2, v3, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->g:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "tweet"

    goto :goto_0
.end method

.method public e()V
    .locals 0

    return-void
.end method

.method public e(Ljava/lang/String;)V
    .locals 2

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "tel:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/card/a;->k(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 0

    return-void
.end method

.method public f(Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/a;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeLog;->c()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v2, v0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->g()Lcom/twitter/library/api/b;

    move-result-object v4

    const/4 v0, 0x0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/a;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    const-string/jumbo v2, "app_download_client_event"

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v3, :cond_2

    const-string/jumbo v2, "4"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_2
    if-eqz v0, :cond_3

    const-string/jumbo v2, "3"

    invoke-virtual {v1, v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_3
    if-eqz v4, :cond_4

    const-string/jumbo v0, "6"

    invoke-virtual {v4}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v4}, Lcom/twitter/library/api/b;->b()Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Z)Lcom/twitter/library/scribe/ScribeLog;

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method public g()V
    .locals 0

    return-void
.end method

.method public g(Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/card/a;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method protected h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "platform_forward_card"

    :goto_0
    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/card/a;->e(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "platform_card"

    goto :goto_0
.end method

.method public h()V
    .locals 0

    return-void
.end method

.method public i()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->t()V

    :cond_0
    return-void
.end method

.method public j()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v1, "click"

    invoke-virtual {p0, v1}, Lcom/twitter/android/card/a;->g(Ljava/lang/String;)V

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    invoke-direct {p0, v1}, Lcom/twitter/android/card/a;->a(Lcom/twitter/library/api/PromotedEvent;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AmplifyMediaPlayerActivity;

    invoke-direct {v1, v0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "tw"

    iget-object v3, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public k()V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/Activity;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    iget-object v2, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/card/a;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    iget-object v5, v5, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iget-object v6, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    iget-object v6, v6, Lcom/twitter/android/client/c;->c:Lcom/twitter/library/util/as;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/card/Card;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->j()V

    invoke-virtual {p0}, Lcom/twitter/android/card/a;->w()V

    goto :goto_0
.end method

.method public l()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->o()V

    :cond_0
    return-void
.end method

.method public m()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/card/a;->b:I

    return v0
.end method

.method public n()Lcom/twitter/library/provider/Tweet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->d:Lcom/twitter/library/provider/Tweet;

    return-object v0
.end method

.method public o()Lcom/twitter/android/card/p;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->e:Lcom/twitter/android/card/p;

    return-object v0
.end method

.method public p()Lcom/twitter/library/card/Card;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    return-object v0
.end method

.method public q()Lcom/twitter/library/card/instance/CardInstanceData;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->l:Lcom/twitter/library/card/instance/CardInstanceData;

    return-object v0
.end method

.method public r()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->m:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public s()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/card/a;->n:I

    return v0
.end method

.method public t()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->c:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public u()Lcom/twitter/library/util/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    return-object v0
.end method

.method public v()Lcom/twitter/library/util/as;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/a;->j:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->c:Lcom/twitter/library/util/as;

    return-object v0
.end method

.method public w()V
    .locals 2

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    if-eqz v1, :cond_0

    iget v1, p0, Lcom/twitter/android/card/a;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/n;->b(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/card/a;->f:Lcom/twitter/library/card/Card;

    invoke-virtual {v1, v0}, Lcom/twitter/library/card/Card;->b(Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected x()V
    .locals 3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {}, Lcom/twitter/android/client/al;->a()Lcom/twitter/android/client/al;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/card/a;->l:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v2, v2, Lcom/twitter/library/card/instance/CardInstanceData;->users:Ljava/util/HashMap;

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/card/a;->a(Lcom/twitter/android/client/al;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    iget-object v2, p0, Lcom/twitter/android/card/a;->l:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v2, v2, Lcom/twitter/library/card/instance/CardInstanceData;->forwardUsers:Ljava/util/HashMap;

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/card/a;->a(Lcom/twitter/android/client/al;Ljava/util/HashMap;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/al;->a(Ljava/util/ArrayList;)V

    return-void
.end method

.method public y()V
    .locals 0

    return-void
.end method

.method public z()V
    .locals 0

    return-void
.end method
