.class public Lcom/twitter/android/card/PlayerDelegateAnimatedGifView;
.super Lcom/twitter/library/card/element/PlayerDelegateView;
.source "Twttr"


# instance fields
.field public a:Landroid/widget/VideoView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/PlayerDelegateView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/Player;)V

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->prepare()V

    :cond_0
    new-instance v0, Landroid/widget/VideoView;

    invoke-direct {v0, p1}, Landroid/widget/VideoView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/card/PlayerDelegateAnimatedGifView;->a:Landroid/widget/VideoView;

    return-void
.end method


# virtual methods
.method public B_()Z
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/PlayerDelegateView;->B_()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/PlayerDelegateAnimatedGifView;->a:Landroid/widget/VideoView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/PlayerDelegateAnimatedGifView;->addView(Landroid/view/View;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super/range {p0 .. p5}, Lcom/twitter/library/card/element/PlayerDelegateView;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/twitter/android/card/PlayerDelegateAnimatedGifView;->a:Landroid/widget/VideoView;

    invoke-virtual {v0, p2, p3, p4, p5}, Landroid/widget/VideoView;->layout(IIII)V

    goto :goto_0
.end method
