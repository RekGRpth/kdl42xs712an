.class public Lcom/twitter/android/card/b;
.super Lcom/twitter/library/card/element/d;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/client/an;


# instance fields
.field protected a:Lcom/twitter/android/card/AppFollowButtonDelegateView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/library/card/element/d;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/card/b;)Lcom/twitter/library/card/element/FollowButtonElement;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/card/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/card/b;->l()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/card/b;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/card/b;->k()V

    return-void
.end method

.method private i()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    iget-object v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    sget-object v1, Lcom/twitter/library/card/element/FollowButtonElement$Kind;->b:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/android/card/d;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/d;-><init>(Lcom/twitter/android/card/b;)V

    const v1, 0x7f0f052a    # com.twitter.android.R.string.users_destroy_friendship

    const v2, 0x7f0f052c    # com.twitter.android.R.string.users_destroy_friendship_question

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/card/b;->a(IILandroid/content/DialogInterface$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/card/b;->l()V

    goto :goto_0
.end method

.method private j()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/FollowButtonElement;->a()Lcom/twitter/library/card/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/card/Card;->f()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v4, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v0, v1, v5, v3}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v2}, Lcom/twitter/library/card/Card;->e()Lcom/twitter/library/card/j;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, v0, v1}, Lcom/twitter/library/card/j;->a(J)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/FollowButtonElement;->a()Lcom/twitter/library/card/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/card/Card;->f()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v4, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v3}, Lcom/twitter/android/client/c;->d(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v2}, Lcom/twitter/library/card/Card;->e()Lcom/twitter/library/card/j;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, v0, v1}, Lcom/twitter/library/card/j;->d(J)V

    :cond_0
    return-void
.end method

.method private l()V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v2}, Lcom/twitter/library/card/element/FollowButtonElement;->a()Lcom/twitter/library/card/Card;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/card/Card;->f()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    iget-object v4, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-static {v4}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-virtual {v4, v0, v1, v3}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    invoke-virtual {v2}, Lcom/twitter/library/card/Card;->e()Lcom/twitter/library/card/j;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-interface {v2, v0, v1}, Lcom/twitter/library/card/j;->b(J)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/library/card/property/Vector2F;)F
    .locals 5

    const/4 v0, 0x0

    const/high16 v4, -0x80000000

    iget-object v1, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v1}, Lcom/twitter/library/card/element/FollowButtonElement;->A()Landroid/view/View;

    move-result-object v2

    iget v1, p2, Lcom/twitter/library/card/property/Vector2F;->x:F

    float-to-int v1, v1

    iget v3, p2, Lcom/twitter/library/card/property/Vector2F;->y:F

    float-to-int v3, v3

    if-eqz v1, :cond_1

    or-int/2addr v1, v4

    :goto_0
    if-eqz v3, :cond_0

    or-int v0, v4, v3

    :cond_0
    invoke-virtual {v2, v1, v0}, Landroid/view/View;->measure(II)V

    if-nez p1, :cond_2

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v0

    int-to-float v0, v0

    :goto_1
    return v0

    :cond_1
    move v1, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v0

    int-to-float v0, v0

    goto :goto_1
.end method

.method public a()Landroid/view/View;
    .locals 10

    const v9, 0x7f0b007b    # com.twitter.android.R.color.twitter_blue

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/content/res/ColorStateList;

    new-array v2, v8, [[I

    new-array v3, v6, [I

    const v4, -0x10100a0

    aput v4, v3, v5

    aput-object v3, v2, v5

    new-array v3, v6, [I

    const v4, 0x10100a7    # android.R.attr.state_pressed

    aput v4, v3, v5

    aput-object v3, v2, v6

    new-array v3, v6, [I

    const v4, 0x10100a0    # android.R.attr.state_checked

    aput v4, v3, v5

    aput-object v3, v2, v7

    new-array v3, v8, [I

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    aput v4, v3, v5

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    aput v4, v3, v6

    const v4, 0x7f0b007e    # com.twitter.android.R.color.white

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    aput v4, v3, v7

    invoke-direct {v1, v2, v3}, Landroid/content/res/ColorStateList;-><init>([[I[I)V

    new-instance v2, Lcom/twitter/android/card/AppFollowButtonDelegateView;

    iget-object v3, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-direct {v2, v3, v4}, Lcom/twitter/android/card/AppFollowButtonDelegateView;-><init>(Landroid/content/Context;Lcom/twitter/library/card/element/FollowButtonElement;)V

    iput-object v2, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    iget-object v2, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v2, v1}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setTextColor(Landroid/content/res/ColorStateList;)V

    iget-object v1, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v1, p0}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    const v2, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    const v1, 0x7f0c0017    # com.twitter.android.R.dimen.btn_padding

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    const v2, 0x7f0c0018    # com.twitter.android.R.dimen.btn_padding_horiz

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v2, v0, v1, v0, v1}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setPadding(IIII)V

    iget-object v0, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v0, v5}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setIncludeFontPadding(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->c()V

    iget-object v0, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    return-object v0
.end method

.method protected a(I)V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {p1}, Lcom/twitter/library/provider/ay;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v0

    if-eqz v0, :cond_1

    const v1, 0x7f0f0508    # com.twitter.android.R.string.unblock

    const v0, 0x7f020126    # com.twitter.android.R.drawable.ic_blocked_default

    :goto_1
    invoke-virtual {p0, v3, v2, v1, v0}, Lcom/twitter/android/card/b;->a(ZZII)V

    goto :goto_0

    :cond_1
    invoke-static {p1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const v1, 0x7f0f050b    # com.twitter.android.R.string.unfollow

    const v0, 0x7f0201a6    # com.twitter.android.R.drawable.ic_follow_action_checked

    move v2, v3

    goto :goto_1

    :cond_2
    const v1, 0x7f0f019f    # com.twitter.android.R.string.follow

    const v0, 0x7f0201a7    # com.twitter.android.R.drawable.ic_follow_action_default

    goto :goto_1

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/card/b;->b()V

    goto :goto_0
.end method

.method protected a(IILandroid/content/DialogInterface$OnClickListener;)V
    .locals 5

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, p1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->g()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    if-eqz v2, :cond_0

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-virtual {v2}, Lcom/twitter/library/api/TwitterUser;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v4

    invoke-virtual {v1, p2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    :cond_0
    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1, p3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method public a(JI)V
    .locals 1

    invoke-virtual {p0, p3}, Lcom/twitter/android/card/b;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FollowButtonElement;->K()V

    return-void
.end method

.method public a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/library/card/element/d;->a(JLcom/twitter/library/util/aa;Lcom/twitter/library/util/as;)V

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/android/client/al;->a()Lcom/twitter/android/client/al;

    move-result-object v2

    invoke-virtual {v2, v0, v1, p0}, Lcom/twitter/android/client/al;->a(JLcom/twitter/android/client/an;)V

    return-void
.end method

.method protected a(ZZII)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p4}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    new-instance v0, Lcom/twitter/android/card/c;

    move-object v1, p0

    move v2, p1

    move v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/card/c;-><init>(Lcom/twitter/android/card/b;ZZLandroid/graphics/drawable/Drawable;I)V

    iget-object v1, p0, Lcom/twitter/android/card/b;->a:Lcom/twitter/android/card/AppFollowButtonDelegateView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/card/AppFollowButtonDelegateView;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected b()V
    .locals 3

    const/4 v2, 0x0

    const v0, 0x7f0f019f    # com.twitter.android.R.string.follow

    const v1, 0x7f0201a7    # com.twitter.android.R.drawable.ic_follow_action_default

    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/twitter/android/card/b;->a(ZZII)V

    return-void
.end method

.method protected c()V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/android/client/al;->a()Lcom/twitter/android/client/al;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/client/al;->b(J)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/card/b;->a(I)V

    return-void
.end method

.method public d()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/library/card/element/d;->d()V

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->c()V

    iget-object v0, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    invoke-virtual {v0}, Lcom/twitter/library/card/element/FollowButtonElement;->K()V

    return-void
.end method

.method protected e()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/b;->c:Lcom/twitter/library/card/element/FollowButtonElement;

    iget-object v0, v0, Lcom/twitter/library/card/element/FollowButtonElement;->kind:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    sget-object v1, Lcom/twitter/library/card/element/FollowButtonElement$Kind;->b:Lcom/twitter/library/card/element/FollowButtonElement$Kind;

    if-ne v0, v1, :cond_0

    new-instance v0, Lcom/twitter/android/card/e;

    invoke-direct {v0, p0}, Lcom/twitter/android/card/e;-><init>(Lcom/twitter/android/card/b;)V

    const v1, 0x7f0f0547    # com.twitter.android.R.string.users_unblock

    const v2, 0x7f0f0549    # com.twitter.android.R.string.users_unblock_question

    invoke-virtual {p0, v1, v2, v0}, Lcom/twitter/android/card/b;->a(IILandroid/content/DialogInterface$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/card/b;->k()V

    goto :goto_0
.end method

.method public f()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/library/card/element/d;->f()V

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/android/client/al;->a()Lcom/twitter/android/client/al;

    move-result-object v2

    invoke-virtual {v2, v0, v1, p0}, Lcom/twitter/android/client/al;->b(JLcom/twitter/android/client/an;)V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/card/b;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->h()J

    move-result-wide v0

    invoke-static {}, Lcom/twitter/android/client/al;->a()Lcom/twitter/android/client/al;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/client/al;->b(J)I

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/card/b;->i()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {v0}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/card/b;->e()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/card/b;->j()V

    goto :goto_0
.end method
