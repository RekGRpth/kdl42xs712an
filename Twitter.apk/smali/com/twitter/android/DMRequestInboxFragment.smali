.class public Lcom/twitter/android/DMRequestInboxFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"


# instance fields
.field final a:Ljava/util/HashMap;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMRequestInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/DMRequestInboxFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/DMRequestInboxFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/DMRequestInboxFragment;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/DMRequestInboxFragment;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 3

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMRequestInboxFragment;->b(I)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMRequestInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/ei;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/ei;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMRequestInboxFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->P()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DMRequestInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMRequestInboxFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->p()V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/DMRequestInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMRequestInboxFragment;->b(I)V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMRequestInboxFragment;->a(Z)V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/DMRequestInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ei;

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/ek;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/twitter/android/ek;-><init>(Lcom/twitter/android/DMRequestInboxFragment;Lcom/twitter/android/ej;)V

    iget-object v4, p0, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/ei;-><init>(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/a;Ljava/util/Map;)V

    iput-object v0, p0, Lcom/twitter/android/DMRequestInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DMRequestInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2

    const/4 v0, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/DMRequestInboxFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMRequestInboxFragment;->l(Z)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "request_states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "request_states"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/Map;

    iget-object v1, p0, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->putAll(Ljava/util/Map;)V

    :cond_0
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ad;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/DMRequestInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/ad;->b:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/DMRequestInboxFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/DMRequestInboxFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "request_states"

    iget-object v1, p0, Lcom/twitter/android/DMRequestInboxFragment;->a:Ljava/util/HashMap;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    return-void
.end method
