.class public Lcom/twitter/android/TweetFragment;
.super Lcom/twitter/android/TweetListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnLongClickListener;
.implements Lcom/twitter/android/AttachMediaListener;
.implements Lcom/twitter/android/card/q;
.implements Lcom/twitter/android/ru;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/android/widget/di;
.implements Lcom/twitter/library/view/c;


# static fields
.field private static final o:Landroid/support/v4/util/LruCache;


# instance fields
.field private J:Z

.field private K:Landroid/view/View;

.field private L:Landroid/widget/ImageView;

.field private U:Landroid/view/View;

.field private V:Landroid/view/View;

.field private W:Landroid/net/Uri;

.field private X:Z

.field private Y:J

.field private Z:Lcom/twitter/android/UserPresenceFragment;

.field a:Lcom/twitter/library/client/Session;

.field private aa:I

.field private ab:J

.field private ac:I

.field private ad:Lcom/twitter/android/tq;

.field private ae:Lcom/twitter/library/scribe/ScribeItem;

.field private af:J

.field private ag:Lcom/twitter/android/PhotoSelectHelper;

.field private ah:Lcom/twitter/android/xm;

.field private ai:Lcom/twitter/android/xl;

.field private aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

.field b:Lcom/twitter/library/provider/Tweet;

.field c:Lcom/twitter/library/provider/Tweet;

.field d:Lcom/twitter/library/api/TweetClassicCard;

.field e:Lcom/twitter/android/xn;

.field f:Lcom/twitter/android/widget/TweetDetailView;

.field g:Z

.field h:Lcom/twitter/library/api/ActivitySummary;

.field i:Lcom/twitter/android/xq;

.field j:Lcom/twitter/android/yb;

.field k:Lcom/twitter/library/scribe/ScribeAssociation;

.field l:Lcom/twitter/library/widget/PageableListView;

.field m:Lcom/twitter/android/PostStorage$MediaItem;

.field n:Ljava/lang/String;

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Landroid/view/View;

.field private t:I

.field private u:Landroid/widget/ImageButton;

.field private v:Landroid/widget/ImageButton;

.field private w:Landroid/view/ViewGroup;

.field private x:Landroid/content/Context;

.field private y:Z

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x64

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    sput-object v0, Lcom/twitter/android/TweetFragment;->o:Landroid/support/v4/util/LruCache;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/android/TweetListFragment;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "en_act"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->setArguments(Landroid/os/Bundle;)V

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/TweetFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic B(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic C(Lcom/twitter/android/TweetFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/TweetFragment;->aa:I

    return v0
.end method

.method private C()Z
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/PageableListView;->getChildCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/PageableListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/PageableListView;->getLastVisiblePosition()I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v3}, Lcom/twitter/android/xq;->getCount()I

    move-result v3

    if-ne v2, v3, :cond_2

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/PageableListView;->getHeight()I

    move-result v2

    if-le v1, v2, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/PageableListView;->getLastVisiblePosition()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v2}, Lcom/twitter/android/xq;->getCount()I

    move-result v2

    if-le v1, v2, :cond_0

    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static synthetic D(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method private D()V
    .locals 10

    const/4 v1, 0x1

    const/4 v9, 0x4

    const/4 v2, 0x0

    invoke-static {}, Lkl;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getHeight()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v2}, Lcom/twitter/android/widget/TweetDetailView;->getTop()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->setActionBarBottom(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v0, v0, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getHeight()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getBottom()I

    move-result v3

    sub-int v0, v3, v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getView()Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getTop()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v4}, Lcom/twitter/android/widget/TweetDetailView;->getTop()I

    move-result v4

    add-int/2addr v3, v4

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v4, v4, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v4}, Landroid/view/ViewGroup;->getTop()I

    move-result v4

    add-int/2addr v4, v3

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v5, v5, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    invoke-virtual {v5}, Landroid/view/ViewGroup;->getTop()I

    move-result v5

    add-int/2addr v5, v3

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v6, 0x7f090297    # com.twitter.android.R.id.convo_reply_placeholder

    invoke-virtual {v3, v6}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v7, v3, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v8, v3, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    if-gt v4, v0, :cond_3

    move v3, v1

    :goto_1
    if-lt v5, v0, :cond_4

    move v0, v1

    :goto_2
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-eqz v4, :cond_2

    invoke-virtual {v8}, Landroid/view/View;->getVisibility()I

    move-result v4

    if-nez v4, :cond_5

    :cond_2
    :goto_3
    if-eqz v1, :cond_6

    if-nez v3, :cond_0

    if-nez v0, :cond_0

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_3
    move v3, v2

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :cond_5
    move v1, v2

    goto :goto_3

    :cond_6
    if-eqz v3, :cond_7

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v8, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    :cond_7
    if-eqz v0, :cond_0

    invoke-virtual {v6, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v8, v9}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v7, v2}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method static synthetic E(Lcom/twitter/android/TweetFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/TweetFragment;->ac:I

    return v0
.end method

.method static synthetic F(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/tq;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ad:Lcom/twitter/android/tq;

    return-object v0
.end method

.method private F()V
    .locals 4

    const/16 v3, 0x8

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f090297    # com.twitter.android.R.id.convo_reply_placeholder

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v1, v1, Lcom/twitter/android/widget/TweetDetailView;->e:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v2, v2, Lcom/twitter/android/widget/TweetDetailView;->d:Landroid/view/ViewGroup;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method static synthetic G(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private G()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v1

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-nez v2, :cond_0

    iget-object v2, v0, Lcom/twitter/library/api/TwitterStatusCard;->classicCard:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v2, :cond_1

    :cond_0
    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/TwitterStatusCard;)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_3

    :cond_1
    if-nez v1, :cond_3

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/TweetFragment;->r:Z

    if-nez v2, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->ai()Z

    move-result v2

    if-eqz v2, :cond_4

    new-instance v0, Lcom/twitter/android/xf;

    invoke-direct {v0, p0}, Lcom/twitter/android/xf;-><init>(Lcom/twitter/android/TweetFragment;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_4
    iget-boolean v2, p0, Lcom/twitter/android/TweetFragment;->y:Z

    if-nez v2, :cond_6

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->H()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v1, :cond_5

    iget-boolean v1, v1, Lcom/twitter/library/api/UserSettings;->k:Z

    if-nez v1, :cond_6

    :cond_5
    new-instance v0, Lcom/twitter/android/xg;

    invoke-direct {v0, p0}, Lcom/twitter/android/xg;-><init>(Lcom/twitter/android/TweetFragment;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/view/View$OnClickListener;)V

    goto :goto_0

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/client/c;)V

    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v1, :cond_2

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/TwitterStatusCard;)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/android/card/n;->c()Lcom/twitter/library/card/Card;

    move-result-object v2

    if-eqz v2, :cond_7

    const/4 v0, 0x1

    invoke-direct {p0, v2, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/card/Card;Z)V

    goto :goto_0

    :cond_7
    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    invoke-virtual {v2, v0}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/card/instance/CardInstanceData;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, v2}, Lcom/twitter/android/xn;->a(Landroid/view/ViewGroup;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const v2, 0x7f030024    # com.twitter.android.R.layout.card_view

    invoke-virtual {v0, v2}, Lcom/twitter/android/xn;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {v1, v0, v2, v3, p0}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;ZLcom/twitter/library/card/j;Lcom/twitter/android/card/q;)Lcom/twitter/android/card/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {v1, v0}, Lcom/twitter/android/xn;->a(Lcom/twitter/android/card/p;)V

    goto/16 :goto_0
.end method

.method static synthetic H(Lcom/twitter/android/TweetFragment;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->w:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private H()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    iget-object v0, v0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->U:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->U:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->V:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->V:Landroid/view/View;

    new-instance v1, Lcom/twitter/android/xh;

    invoke-direct {v1, p0}, Lcom/twitter/android/xh;-><init>(Lcom/twitter/android/TweetFragment;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->setBackgroundColor(I)V

    return-void
.end method

.method static synthetic I(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private I()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->W:Landroid/net/Uri;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Lcom/twitter/android/PostStorage$MediaItem;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->W:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->W:Landroid/net/Uri;

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/android/PostStorage$MediaItem;-><init>(Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ag:Lcom/twitter/android/PhotoSelectHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PostStorage$MediaItem;Z)V

    goto :goto_0
.end method

.method private J()V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f030078    # com.twitter.android.R.layout.footer_container

    invoke-static {v0, v1, v3}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v3, v2}, Lcom/twitter/library/widget/PageableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    new-instance v0, Lcom/twitter/android/UserPresenceFragment;

    invoke-direct {v0}, Lcom/twitter/android/UserPresenceFragment;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->Z:Lcom/twitter/android/UserPresenceFragment;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f090185    # com.twitter.android.R.id.frame_container

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->Z:Lcom/twitter/android/UserPresenceFragment;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    return-void
.end method

.method static synthetic J(Lcom/twitter/android/TweetFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->X:Z

    return v0
.end method

.method static synthetic K(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private K()V
    .locals 8

    iget-object v7, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-boolean v0, v7, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-wide v2, v7, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v4, v7, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x0

    iput-boolean v1, v7, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    const v2, 0x7f0200ec    # com.twitter.android.R.drawable.ic_action_fave_off

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f0058    # com.twitter.android.R.string.button_action_fave

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string/jumbo v1, "unfavorite"

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    const-string/jumbo v1, "unfavorite"

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-wide v2, v7, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, v7, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v6, v7, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v7, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    const v2, 0x7f0200f1    # com.twitter.android.R.drawable.ic_action_fave_on

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f007c    # com.twitter.android.R.string.button_status_favorited

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    const-string/jumbo v1, "favorite"

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    const-string/jumbo v1, "favorite"

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const v2, 0x7f0f012b    # com.twitter.android.R.string.dt2f_dialog_text

    invoke-static {v1, v2}, Ljy;->a(Landroid/content/Context;I)V

    goto :goto_0
.end method

.method static synthetic L(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private L()V
    .locals 6

    const/16 v0, 0x65

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v5

    move-object v3, p0

    move-object v4, p0

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/rs;->a(ILcom/twitter/library/provider/Tweet;ZLandroid/support/v4/app/Fragment;Lcom/twitter/android/ru;Landroid/support/v4/app/FragmentActivity;)V

    return-void
.end method

.method private M()V
    .locals 8

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, v3, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->h:J

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v6, v6, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static/range {v0 .. v7}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JJ)V

    const-string/jumbo v0, "share"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    const-string/jumbo v0, "share"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic M(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->L()V

    return-void
.end method

.method private N()V
    .locals 9

    const/4 v8, 0x1

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-boolean v1, v1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->d:[J

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->d:[J

    array-length v1, v1

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v4, v1, Lcom/twitter/library/api/ActivitySummary;->d:[J

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-wide v6, v4, v1

    cmp-long v6, v6, v2

    if-nez v6, :cond_2

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iput-boolean v8, v1, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    const v4, 0x7f0200f1    # com.twitter.android.R.drawable.ic_action_fave_on

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-boolean v1, v1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->e:[J

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->e:[J

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iget-object v1, v1, Lcom/twitter/library/api/ActivitySummary;->e:[J

    array-length v4, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-wide v5, v1, v0

    cmp-long v5, v5, v2

    if-nez v5, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iput-boolean v8, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->v:Landroid/widget/ImageButton;

    const v1, 0x7f020105    # com.twitter.android.R.drawable.ic_action_rt_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method static synthetic N(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->M()V

    return-void
.end method

.method static synthetic O(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method private O()V
    .locals 7

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget v0, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v0, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v0, v3, v5

    if-nez v0, :cond_2

    move v0, v1

    :goto_0
    if-nez v0, :cond_1

    if-eqz v2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->J:Z

    if-eqz v0, :cond_1

    :cond_0
    iput-boolean v1, p0, Lcom/twitter/android/TweetFragment;->J:Z

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p0, v2}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/view/View$OnClickListener;Z)V

    :cond_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic P(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method static synthetic Q(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic R(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/xl;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/api/TwitterStatusCard;)Ljava/lang/Boolean;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/TwitterStatusCard;)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/api/TwitterStatusCard;)Ljava/lang/Boolean;
    .locals 2

    iget-object v0, p1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    iget-object v0, v0, Lcom/twitter/library/card/instance/CardInstanceData;->name:Ljava/lang/String;

    const-string/jumbo v1, "amplify"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->al()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private a(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/provider/Tweet;J)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    iget v0, p1, Lcom/twitter/library/api/TweetClassicCard;->type:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "platform_photo_card"

    :goto_1
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet::"

    aput-object v2, v1, v4

    aput-object v0, v1, v5

    const/4 v0, 0x2

    const-string/jumbo v2, "profile_click"

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v5, [Ljava/lang/String;

    aput-object v0, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p3, p4, v6, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/library/api/TweetClassicCard;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "platform_amplify_card"

    goto :goto_1

    :cond_0
    const-string/jumbo v0, "platform_player_card"

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "platform_summary_card"

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "platform_promotion_card"

    goto :goto_1

    :pswitch_4
    const-string/jumbo v0, "platform_promotion_card"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private a(Lcom/twitter/library/card/Card;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {v0, p1}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {p1, v0}, Lcom/twitter/library/card/Card;->a(Lcom/twitter/library/card/j;)V

    invoke-virtual {p1}, Lcom/twitter/library/card/Card;->j()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->h()V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->setCard(Lcom/twitter/library/card/Card;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {v0}, Lcom/twitter/android/xn;->w()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->g()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->e()V

    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/card/Card;->x()V

    :cond_2
    return-void
.end method

.method private static a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 4

    const/4 v3, 0x3

    iget-object v0, p1, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/api/TweetClassicCard;->siteUser:Lcom/twitter/library/api/CardUser;

    iget-wide v1, v1, Lcom/twitter/library/api/CardUser;->userId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->b(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    iget-object v0, p1, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    if-eqz v0, :cond_1

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/library/api/TweetClassicCard;->authorUser:Lcom/twitter/library/api/CardUser;

    iget-wide v1, v1, Lcom/twitter/library/api/CardUser;->userId:J

    invoke-static {v1, v2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->b(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;)V
    .locals 5

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const-string/jumbo v4, ""

    aput-object v4, v3, v0

    const/4 v0, 0x2

    const-string/jumbo v4, "media_forward:platform_photo_card"

    aput-object v4, v3, v0

    const/4 v0, 0x3

    aput-object p1, v3, v0

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->x:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p2, v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-string/jumbo v0, "tweet"

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v1, v5, [Ljava/lang/String;

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "tweet"

    aput-object v3, v2, v4

    aput-object v6, v2, v5

    const/4 v3, 0x2

    aput-object p1, v2, v3

    const/4 v3, 0x3

    aput-object p2, v2, v3

    const/4 v3, 0x4

    aput-object p3, v2, v3

    invoke-static {v2}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/String;

    const-string/jumbo v2, "tweet::tweet::impression"

    aput-object v2, v1, v4

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v6, v1, v2, v6}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->c_(I)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/TweetFragment;->r:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;Ljava/lang/String;I)V
    .locals 0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->i(I)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/TweetFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/TweetFragment;->y:Z

    return p1
.end method

.method static synthetic c(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/UserPresenceFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->Z:Lcom/twitter/android/UserPresenceFragment;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/TweetFragment;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->C()Z

    move-result v0

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method private f(Ljava/lang/String;)V
    .locals 6

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    :goto_0
    const-string/jumbo v2, "association"

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v3, v4, v5, v0}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "pc"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/TweetFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->z:Z

    return v0
.end method

.method static synthetic g(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private g(Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeLog;->c()Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->g()Lcom/twitter/library/api/b;

    move-result-object v3

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/TweetFragment;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v4

    const-string/jumbo v5, "app_download_client_event"

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v5, "4"

    invoke-virtual {v4, v5, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {v0, v2}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "3"

    invoke-virtual {v4, v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    if-eqz v3, :cond_2

    const-string/jumbo v0, "6"

    invoke-virtual {v3}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v3}, Lcom/twitter/library/api/b;->b()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Z)Lcom/twitter/library/scribe/ScribeLog;

    :cond_2
    invoke-virtual {v1, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0
.end method

.method private h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {v0}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v7, [Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "tweet"

    aput-object v4, v3, v6

    aput-object v5, v3, v7

    const/4 v4, 0x2

    aput-object v0, v3, v4

    const/4 v0, 0x3

    aput-object v5, v3, v0

    const/4 v0, 0x4

    aput-object p1, v3, v0

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    new-array v1, v7, [Ljava/lang/String;

    const-string/jumbo v2, "tweet::tweet::impression"

    aput-object v2, v1, v6

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v5, v1, v2, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0
.end method

.method static synthetic h(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->G()V

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic i(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private i(Ljava/lang/String;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetActivity;

    iget v0, v0, Lcom/twitter/android/TweetActivity;->g:I

    invoke-static {v0, p1}, Lgr;->a(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    aput-object v0, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method static synthetic j(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/TweetFragment;)Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->W:Landroid/net/Uri;

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/TweetFragment;Ljava/lang/String;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic m(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->D()V

    return-void
.end method

.method static synthetic n(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->K()V

    return-void
.end method

.method static synthetic o(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic p(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic q(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/au;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->M:Lcom/twitter/android/client/au;

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/au;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->M:Lcom/twitter/android/client/au;

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/TweetFragment;)Landroid/widget/ImageButton;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic w(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->N()V

    return-void
.end method

.method static synthetic z(Lcom/twitter/android/TweetFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->O()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/xn;->a(Lcom/twitter/android/card/p;)V

    return-void
.end method

.method public a(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 1

    const-string/jumbo v0, "quote"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    const-string/jumbo v0, "quote"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    return-void
.end method

.method public a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V
    .locals 2

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->v:Landroid/widget/ImageButton;

    const v1, 0x7f020101    # com.twitter.android.R.drawable.ic_action_rt_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    const-string/jumbo v0, "unretweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string/jumbo v0, "retweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    const-string/jumbo v0, "retweet"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    invoke-virtual {p0, p6}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v1, -0x1

    if-ne p3, v1, :cond_0

    const-string/jumbo v1, "delete"

    invoke-direct {p0, v1}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v2, "status_id"

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const/4 v2, 0x1

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->finish()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x66
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->K:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->L:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 10

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {v3}, Lcom/twitter/android/xq;->getCount()I

    move-result v0

    if-ne v0, v1, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    :cond_1
    new-instance v2, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v2, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2, v4}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iput v1, v2, Lcom/twitter/library/provider/Tweet;->v:I

    :goto_1
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v3, v0}, Lcom/twitter/android/xq;->a(Ljava/util/ArrayList;)V

    invoke-virtual {v3}, Lcom/twitter/android/xq;->notifyDataSetChanged()V

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->s()V

    goto :goto_0

    :cond_3
    const/4 v4, 0x2

    iput v4, v2, Lcom/twitter/library/provider/Tweet;->v:I

    goto :goto_1

    :pswitch_1
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v3, v0}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    new-instance v4, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v4, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v4

    if-nez v4, :cond_4

    invoke-virtual {v2, v0}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    :cond_4
    :goto_2
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_5

    new-instance v4, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v4, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v1, v4}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_6

    :cond_5
    if-lez v0, :cond_0

    invoke-virtual {v3}, Lcom/twitter/android/xq;->notifyDataSetChanged()V

    invoke-virtual {v2}, Lcom/twitter/library/widget/PageableListView;->getHeaderViewsCount()I

    move-result v0

    invoke-virtual {v3, v1}, Lcom/twitter/android/xq;->a(Lcom/twitter/library/provider/Tweet;)I

    move-result v1

    add-int/2addr v0, v1

    invoke-virtual {v2}, Lcom/twitter/library/widget/PageableListView;->getLoadingHeaderHeight()I

    move-result v1

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/widget/PageableListView;->setSelectionFromTop(II)V

    goto :goto_0

    :cond_6
    invoke-virtual {v3, v0, v4}, Lcom/twitter/android/xq;->a(ILcom/twitter/library/provider/Tweet;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :pswitch_2
    if-eqz p2, :cond_0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v2

    const/4 v4, 0x4

    if-ne v2, v4, :cond_a

    :goto_3
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->C()Z

    move-result v2

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v4}, Lcom/twitter/android/xq;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v3, v4}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v4

    invoke-interface {p2}, Landroid/database/Cursor;->moveToLast()Z

    move-result v5

    if-eqz v5, :cond_0

    :cond_7
    new-instance v5, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v5, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v4, v5}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/twitter/android/TweetFragment;->Y:J

    cmp-long v6, v6, v8

    if-nez v6, :cond_b

    :cond_8
    :goto_4
    if-lez v0, :cond_9

    invoke-virtual {v3}, Lcom/twitter/android/xq;->notifyDataSetChanged()V

    :cond_9
    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    if-eqz v3, :cond_0

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    invoke-interface {v1, v0, v2}, Lcom/twitter/android/xl;->a(IZ)V

    goto/16 :goto_0

    :cond_a
    move v1, v0

    goto :goto_3

    :cond_b
    invoke-virtual {v3}, Lcom/twitter/android/xq;->getCount()I

    move-result v6

    sub-int/2addr v6, v0

    invoke-virtual {v3, v6, v5}, Lcom/twitter/android/xq;->a(ILcom/twitter/library/provider/Tweet;)V

    add-int/lit8 v0, v0, 0x1

    invoke-interface {p2}, Landroid/database/Cursor;->moveToPrevious()Z

    move-result v5

    if-nez v5, :cond_7

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/view/View;I[J)V
    .locals 6

    const/16 v0, 0xc

    const/16 v1, 0xb

    if-ne p2, v0, :cond_0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/UsersActivity;

    invoke-direct {v2, v1, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "owner_id"

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "tag"

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "type"

    invoke-virtual {v2, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "user_ids"

    invoke-virtual {v2, v3, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[J)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "cluster_follow"

    invoke-static {v1, v0}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v1

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "cluster_follow_experiment"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    if-ne p2, v1, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3

    invoke-super/range {p0 .. p5}, Lcom/twitter/android/TweetListFragment;->a(Landroid/widget/ListView;Landroid/view/View;IJ)V

    add-int/lit8 v0, p3, -0x1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1, v2}, Lcom/twitter/android/xq;->a(Lcom/twitter/library/provider/Tweet;)I

    move-result v1

    if-ge v0, v1, :cond_0

    const-string/jumbo v0, "parent_tweet"

    :goto_0
    const-string/jumbo v1, ""

    const-string/jumbo v2, "click"

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {p4, p5, v1, v2}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->startActivityForResult(Landroid/content/Intent;I)V

    return-void

    :cond_0
    const-string/jumbo v0, "child_tweet"

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/AttachMediaListener$MediaAttachFailure;Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->K:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage$MediaItem;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->m:Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iput v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->c:I

    iget-object v0, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->K:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getWidth()I

    move-result v2

    int-to-float v3, v2

    div-float v0, v3, v0

    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->L:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->L:Landroid/widget/ImageView;

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->e:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public a(Lcom/twitter/android/xl;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    return-void
.end method

.method public a(Lcom/twitter/library/api/MediaEntity;)V
    .locals 3

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->k:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method a(Lcom/twitter/library/api/PromotedEvent;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, p1, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/api/Promotion;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p1, Lcom/twitter/library/api/Promotion;->targetUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/yb;->a(Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/provider/Tweet;Ljava/util/HashMap;Lcom/twitter/library/scribe/ScribeAssociation;)Z

    return-void
.end method

.method public a(Lcom/twitter/library/api/TweetClassicCard;)V
    .locals 3

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->k:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method a(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/provider/Tweet;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget v0, p1, Lcom/twitter/library/api/TweetClassicCard;->type:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "platform_photo_card"

    :goto_1
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "tweet::"

    aput-object v2, v1, v5

    aput-object v0, v1, v6

    const/4 v0, 0x2

    const-string/jumbo v2, "show"

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v6, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, v4, p2, v4, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/scribe/ScribeLog;Lcom/twitter/library/api/TweetClassicCard;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p2}, Lcom/twitter/library/provider/Tweet;->Q()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "platform_amplify_card"

    goto :goto_1

    :cond_0
    const-string/jumbo v0, "platform_player_card"

    goto :goto_1

    :pswitch_2
    const-string/jumbo v0, "platform_summary_card"

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/api/UrlEntity;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/UrlEntity;Ljava/util/HashMap;Z)V

    return-void
.end method

.method public a(Lcom/twitter/library/api/UrlEntity;Ljava/util/HashMap;Z)V
    .locals 9

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const-string/jumbo v1, "open_link"

    invoke-virtual {v0, v1}, Lcom/twitter/android/xn;->f(Ljava/lang/String;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->t()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p1, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->h()Lcom/twitter/library/network/OAuthToken;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v5, p2

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/yb;->a(Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/library/provider/Tweet;Ljava/util/HashMap;Lcom/twitter/library/scribe/ScribeAssociation;)Z

    :goto_0
    return-void

    :cond_1
    if-nez p3, :cond_2

    const-string/jumbo v5, "::tweet::open_link"

    const-string/jumbo v6, "tweet::tweet::impression"

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    iget-object v7, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    move-object v2, p1

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/client/be;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/UrlEntity;JLjava/lang/String;Ljava/lang/String;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move-object v6, v8

    move-object v5, v8

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/card/Card;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/xn;->a(Lcom/twitter/android/card/p;)V

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/card/Card;Z)V

    return-void
.end method

.method a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/Session;Ljava/lang/String;IIJLcom/twitter/library/scribe/ScribeItem;J)V
    .locals 18

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    if-nez v3, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    :cond_0
    move-object/from16 v0, p3

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/TweetFragment;->n:Ljava/lang/String;

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/library/api/TweetClassicCard;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3, v4}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/provider/Tweet;)V

    move/from16 v0, p4

    move-object/from16 v1, p0

    iput v0, v1, Lcom/twitter/android/TweetFragment;->aa:I

    move/from16 v0, p5

    move-object/from16 v1, p0

    iput v0, v1, Lcom/twitter/android/TweetFragment;->ac:I

    move-wide/from16 v0, p6

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/twitter/android/TweetFragment;->ab:J

    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/twitter/android/TweetFragment;->ae:Lcom/twitter/library/scribe/ScribeItem;

    move-wide/from16 v0, p9

    move-object/from16 v2, p0

    iput-wide v0, v2, Lcom/twitter/android/TweetFragment;->af:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->ag:Lcom/twitter/android/PhotoSelectHelper;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/library/client/Session;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->ad:Lcom/twitter/android/tq;

    if-nez v3, :cond_b

    new-instance v3, Lcom/twitter/android/tq;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    move-object/from16 v0, p0

    iget-wide v5, v0, Lcom/twitter/android/TweetFragment;->ab:J

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/TweetFragment;->aa:I

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/TweetFragment;->ae:Lcom/twitter/library/scribe/ScribeItem;

    move-object/from16 v0, p0

    iget-wide v9, v0, Lcom/twitter/android/TweetFragment;->af:J

    invoke-direct/range {v3 .. v10}, Lcom/twitter/android/tq;-><init>(Landroid/content/Context;JILcom/twitter/library/scribe/ScribeItem;J)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/twitter/android/TweetFragment;->ad:Lcom/twitter/android/tq;

    :goto_0
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v3

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/android/TweetFragment;->p:Z

    if-nez v4, :cond_1

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/TweetFragment;->p:Z

    sget-object v4, Lcom/twitter/library/api/PromotedEvent;->g:Lcom/twitter/library/api/PromotedEvent;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->d()Z

    move-result v13

    if-eqz v13, :cond_2

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v4

    invoke-virtual {v4}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/TweetFragment;->w:Landroid/view/ViewGroup;

    move-object/from16 v0, p2

    move-object/from16 v1, p1

    move-object/from16 v2, p0

    invoke-static {v4, v0, v1, v5, v2}, Lcom/twitter/android/widget/dn;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V

    :cond_2
    new-instance v4, Lcom/twitter/android/xq;

    iget-object v5, v3, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5, v6}, Lcom/twitter/android/xq;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/ap;Lcom/twitter/library/provider/Tweet;)V

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    move-object/from16 v0, p0

    invoke-virtual {v4, v0}, Lcom/twitter/android/xq;->a(Lcom/twitter/android/ob;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v4}, Lcom/twitter/library/widget/PageableListView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v5

    new-instance v6, Lcom/twitter/android/xe;

    move-object/from16 v0, p0

    invoke-direct {v6, v0}, Lcom/twitter/android/xe;-><init>(Lcom/twitter/android/TweetFragment;)V

    invoke-virtual {v5, v6}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v14

    invoke-virtual {v4}, Lcom/twitter/library/widget/PageableListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v5

    if-nez v5, :cond_6

    invoke-static {}, Lkl;->f()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-virtual {v4}, Lcom/twitter/library/widget/PageableListView;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-static {v5}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f03004c    # com.twitter.android.R.layout.divider

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v4, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/twitter/library/widget/PageableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_3
    invoke-virtual {v4}, Lcom/twitter/library/widget/PageableListView;->b()V

    if-eqz v14, :cond_5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/TweetFragment;->s:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/TweetFragment;->s:Landroid/view/View;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v6, v7}, Lcom/twitter/library/widget/PageableListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    :cond_4
    invoke-virtual {v4}, Lcom/twitter/library/widget/PageableListView;->a()V

    :cond_5
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/PageableListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :cond_6
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/TweetFragment;->g:Z

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->q()Lcom/twitter/android/xo;

    move-result-object v15

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/twitter/android/TweetFragment;->q:Z

    if-nez v4, :cond_7

    iget-wide v4, v15, Lcom/twitter/android/xo;->a:J

    const-wide/32 v6, 0x493e0

    add-long/2addr v4, v6

    cmp-long v4, v16, v4

    if-gez v4, :cond_c

    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v4, v5, v6, v0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :goto_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    if-eqz v4, :cond_d

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    iput-object v4, v3, Lcom/twitter/library/provider/Tweet;->U:Lcom/twitter/library/api/ActivitySummary;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    move-object/from16 v0, p0

    invoke-virtual {v3, v4, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Lcom/twitter/library/api/ActivitySummary;Lcom/twitter/android/widget/di;)V

    :cond_8
    :goto_2
    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->G()V

    if-eqz v14, :cond_9

    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/TweetFragment;->e(Z)V

    :cond_9
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/android/TweetFragment;->X:Z

    if-eqz v3, :cond_a

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    if-eqz v3, :cond_a

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->H()V

    invoke-direct/range {p0 .. p0}, Lcom/twitter/android/TweetFragment;->I()V

    :cond_a
    return-void

    :cond_b
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/TweetFragment;->ad:Lcom/twitter/android/tq;

    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/twitter/android/TweetFragment;->ab:J

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/TweetFragment;->aa:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/TweetFragment;->ae:Lcom/twitter/library/scribe/ScribeItem;

    move-object/from16 v0, p0

    iget-wide v8, v0, Lcom/twitter/android/TweetFragment;->af:J

    invoke-virtual/range {v3 .. v9}, Lcom/twitter/android/tq;->a(JILcom/twitter/library/scribe/ScribeItem;J)V

    goto/16 :goto_0

    :cond_c
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/twitter/android/TweetFragment;->q:Z

    move-object/from16 v0, p1

    iget-wide v5, v0, Lcom/twitter/library/provider/Tweet;->o:J

    const-wide/16 v7, 0x0

    const-wide/16 v9, 0x0

    move-object/from16 v0, p1

    iget-wide v11, v0, Lcom/twitter/library/provider/Tweet;->j:J

    move-object/from16 v4, p2

    invoke-virtual/range {v3 .. v12}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJJJ)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    move-wide/from16 v0, v16

    iput-wide v0, v15, Lcom/twitter/android/xo;->a:J

    goto :goto_1

    :cond_d
    if-eqz v13, :cond_8

    move-object/from16 v0, p1

    iget-wide v4, v0, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v3, v4, v5}, Lcom/twitter/android/client/c;->i(J)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    goto :goto_2
.end method

.method public a(Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    invoke-virtual {v0, p1}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {v0, p1}, Lcom/twitter/android/xn;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v0, p2}, Lcom/twitter/android/xq;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/xn;->a(Lcom/twitter/android/card/p;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->h()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->e()V

    :cond_0
    return-void
.end method

.method public a(Ljava/util/ArrayList;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v6, :cond_0

    invoke-virtual {p1, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/RepliedUser;

    iget-wide v0, v0, Lcom/twitter/library/api/RepliedUser;->userId:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->c(J)V

    const-string/jumbo v0, "profile_click"

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "tweet::tweet:replied_to"

    aput-object v4, v3, v5

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-static {p1}, Lcom/twitter/android/InReplyToDialogFragment;->a(Ljava/util/ArrayList;)Lcom/twitter/android/InReplyToDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/xk;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/xk;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/android/InReplyToDialogFragment;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/InReplyToDialogFragment;->a(Lcom/twitter/android/in;)V

    const-string/jumbo v2, "reply_addressed_to_dialog"

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/InReplyToDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    const-string/jumbo v0, "click"

    goto :goto_0
.end method

.method public a(Ljava/util/List;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_1

    invoke-static {v0, v1, v1}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/MediaEntity;->a(Ljava/util/List;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->a()V

    :cond_1
    return-void
.end method

.method public a(ZZ)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    if-eqz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v0}, Lcom/twitter/android/xq;->getCount()I

    move-result v2

    if-eqz p2, :cond_2

    const/4 v0, 0x2

    :goto_1
    add-int/2addr v0, v2

    if-eqz p1, :cond_3

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_3

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/PageableListView;->smoothScrollToPosition(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/PageableListView;->setSelection(I)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->a_()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v0}, Lcom/twitter/android/xq;->a()V

    :cond_0
    return-void
.end method

.method public b(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweet::retweet_dialog::dismiss"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public b(J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, p2, v2}, Lcom/twitter/android/yb;->a(Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method public b(Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/TweetFragment;->W:Landroid/net/Uri;

    return-void
.end method

.method public b(Ljava/lang/String;)V
    .locals 12

    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x0

    const/4 v8, 0x1

    const/4 v7, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->X:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v8, :cond_0

    invoke-virtual {p1, v9}, Ljava/lang/String;->charAt(I)C

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {v4}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x23

    if-ne v0, v5, :cond_2

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->e:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v5, "hashtag_click"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    const-string/jumbo v5, "hashtag"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v5, "tweet"

    aput-object v5, v3, v9

    aput-object v7, v3, v8

    aput-object v4, v3, v10

    const-string/jumbo v4, "hashtag"

    aput-object v4, v3, v11

    const/4 v4, 0x4

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v7, v2, v3, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_2
    const/16 v5, 0x24

    if-ne v0, v5, :cond_3

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->p:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    new-instance v0, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/SearchActivity;

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "query"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "q_source"

    const-string/jumbo v5, "cashtag_click"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_context"

    const-string/jumbo v5, "cashtag"

    invoke-virtual {v0, v1, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v5, "tweet"

    aput-object v5, v3, v9

    aput-object v7, v3, v8

    aput-object v4, v3, v10

    const-string/jumbo v4, "cashtag"

    aput-object v4, v3, v11

    const/4 v4, 0x4

    const-string/jumbo v5, "search"

    aput-object v5, v3, v4

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v7, v2, v3, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_3
    const/16 v1, 0x40

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    invoke-virtual {p1, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->f(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v8, [Ljava/lang/String;

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v6, "tweet"

    aput-object v6, v3, v9

    aput-object v7, v3, v8

    aput-object v4, v3, v10

    aput-object v7, v3, v11

    const/4 v4, 0x4

    const-string/jumbo v6, "mention_click"

    aput-object v6, v3, v4

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    invoke-virtual {v5, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v2, v7, v3, v4, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-static {v0}, Lcom/twitter/library/scribe/ScribeItem;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0
.end method

.method protected c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;
    .locals 1

    instance-of v0, p1, Lcom/twitter/library/widget/TweetView;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/library/widget/TweetView;

    :goto_0
    return-object p1

    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public c(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "tweet::retweet_dialog::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public c(J)V
    .locals 6

    const/4 v5, 0x1

    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pc"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v2, v2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->k:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v5, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :goto_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->startActivity(Landroid/content/Intent;)V

    return-void

    :cond_0
    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v5, v2, v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->a(IJLcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public c(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    const v1, 0x7f09029b    # com.twitter.android.R.id.onboarding_tweet_instructions_text

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method public d(J)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-eqz v0, :cond_0

    iput-wide p1, p0, Lcom/twitter/android/TweetFragment;->Y:J

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/xq;->a(J)V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->X:Z

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->r()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iget-boolean v2, v1, Lcom/twitter/library/provider/Tweet;->l:Z

    if-nez v2, :cond_4

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/library/provider/Tweet;)V

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/android/xj;

    invoke-direct {v0, p0}, Lcom/twitter/android/xj;-><init>(Lcom/twitter/android/TweetFragment;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/Runnable;)V

    :cond_1
    :goto_1
    return-void

    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->K()V

    goto :goto_1

    :cond_4
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/TweetDetailView;->a(Ljava/lang/Runnable;)V

    goto :goto_1
.end method

.method public e(Z)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getHeaderViewsCount()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2, v3}, Lcom/twitter/android/xq;->a(Lcom/twitter/library/provider/Tweet;)I

    move-result v2

    add-int/2addr v1, v2

    if-eqz p1, :cond_2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0xb

    if-lt v2, v3, :cond_2

    iget v2, p0, Lcom/twitter/android/TweetFragment;->t:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/PageableListView;->smoothScrollToPositionFromTop(II)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/twitter/android/TweetFragment;->t:I

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/PageableListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method public f(Z)V
    .locals 2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/twitter/android/TweetFragment;->z:Z

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->g(Z)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    goto :goto_0
.end method

.method public g(Z)Z
    .locals 12

    const/4 v11, 0x2

    if-eqz p1, :cond_0

    const/4 v0, 0x4

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    invoke-virtual {p0, v11}, Lcom/twitter/android/TweetFragment;->c_(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v1}, Lcom/twitter/android/xq;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->q()Lcom/twitter/android/xo;

    move-result-object v2

    iput-wide v0, v2, Lcom/twitter/android/xo;->b:J

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->o:J

    const-wide/16 v6, 0x0

    iget-object v8, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v8, v8, Lcom/twitter/library/provider/Tweet;->j:J

    move v10, p1

    invoke-virtual/range {v0 .. v10}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJJJZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v11}, Lcom/twitter/android/TweetFragment;->a(Ljava/lang/String;I)V

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public h(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/TweetFragment;->X:Z

    return-void
.end method

.method protected k()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->g:Z

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->k()V

    :cond_0
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    const/4 v0, 0x1

    if-eq p1, v0, :cond_0

    const/4 v0, 0x5

    if-ne p1, v0, :cond_3

    :cond_0
    if-eqz p3, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    const-string/jumbo v1, "rt"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "rt"

    iget-boolean v2, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    iget-boolean v1, v0, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->v:Landroid/widget/ImageButton;

    const v2, 0x7f020105    # com.twitter.android.R.drawable.ic_action_rt_on

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_1
    :goto_0
    const-string/jumbo v1, "fav"

    invoke-virtual {p3, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v1, "fav"

    iget-boolean v2, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    invoke-virtual {p3, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    iget-boolean v0, v0, Lcom/twitter/library/provider/Tweet;->l:Z

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    const v1, 0x7f0200f1    # com.twitter.android.R.drawable.ic_action_fave_on

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    :cond_2
    :goto_1
    const-string/jumbo v0, "tags"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "tags"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Ljava/util/List;)V

    :cond_3
    return-void

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->v:Landroid/widget/ImageButton;

    const v2, 0x7f020101    # com.twitter.android.R.drawable.ic_action_rt_off

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    const v1, 0x7f0200ec    # com.twitter.android.R.drawable.ic_action_fave_off

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setImageResource(I)V

    goto :goto_1
.end method

.method public onClick(Landroid/view/View;)V
    .locals 11

    const v4, 0x7f09006d    # com.twitter.android.R.id.reply

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->X:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TweetActivity;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    if-ne v2, v4, :cond_2

    invoke-virtual {p0, v9}, Lcom/twitter/android/TweetFragment;->j(I)I

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {v3}, Lcom/twitter/library/provider/Tweet;->b(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v3

    if-ne v2, v4, :cond_4

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    if-eqz v2, :cond_3

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    invoke-interface {v0, v1}, Lcom/twitter/android/xl;->a(Lcom/twitter/library/provider/Tweet;)V

    :goto_2
    const-string/jumbo v0, "reply"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    const-string/jumbo v0, "reply"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v8}, Lcom/twitter/android/TweetFragment;->j(I)I

    goto :goto_1

    :cond_3
    sget-object v2, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->c:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v0, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Lcom/twitter/library/provider/ParcelableTweet;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->k()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->l()[I

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/TweetActivity;->m()V

    invoke-virtual {v1, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    goto :goto_2

    :cond_4
    const v0, 0x7f09006f    # com.twitter.android.R.id.favorite

    if-ne v2, v0, :cond_5

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->K()V

    goto :goto_0

    :cond_5
    const v0, 0x7f09006e    # com.twitter.android.R.id.retweet

    if-ne v2, v0, :cond_6

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->L()V

    goto :goto_0

    :cond_6
    const v0, 0x7f090071    # com.twitter.android.R.id.delete

    if-ne v2, v0, :cond_7

    const/16 v0, 0x66

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f04df    # com.twitter.android.R.string.tweets_delete_status

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f04de    # com.twitter.android.R.string.tweets_delete_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v9}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto/16 :goto_0

    :cond_7
    const v0, 0x7f090070    # com.twitter.android.R.id.share

    if-ne v2, v0, :cond_8

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->M()V

    goto/16 :goto_0

    :cond_8
    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    if-ne v2, v0, :cond_9

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->c:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->c(J)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-direct {v4, v5, v6}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v7, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, "avatar"

    const-string/jumbo v8, "profile_click"

    invoke-static {v6, v3, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v9

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v0, v1, v10, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto/16 :goto_0

    :cond_9
    const v0, 0x7f09008a    # com.twitter.android.R.id.promoted_tweet

    if-ne v2, v0, :cond_b

    sget-object v0, Lcom/twitter/library/api/PromotedEvent;->j:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    invoke-virtual {v1}, Lcom/twitter/library/provider/Tweet;->I()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-boolean v0, v1, Lcom/twitter/library/provider/Tweet;->r:Z

    if-eqz v0, :cond_a

    iget-wide v0, v1, Lcom/twitter/library/provider/Tweet;->n:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->c(J)V

    goto/16 :goto_0

    :cond_a
    iget-wide v0, v1, Lcom/twitter/library/provider/Tweet;->q:J

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetFragment;->c(J)V

    goto/16 :goto_0

    :cond_b
    const v0, 0x7f0900ba    # com.twitter.android.R.id.site_user

    if-ne v2, v0, :cond_d

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/library/api/TweetClassicCard;

    if-eqz v0, :cond_c

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->d:Lcom/twitter/library/api/TweetClassicCard;

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/TweetClassicCard;Lcom/twitter/library/provider/Tweet;J)V

    :cond_c
    invoke-virtual {p0, v2, v3}, Lcom/twitter/android/TweetFragment;->c(J)V

    goto/16 :goto_0

    :cond_d
    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    if-ne v2, v0, :cond_10

    check-cast p1, Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {p1}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_e

    const-string/jumbo v0, "unfollow"

    :goto_3
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    if-eqz v1, :cond_f

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iput v9, v1, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v1, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v2, v4, v5, v1}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    :goto_4
    invoke-virtual {p1}, Lcom/twitter/library/widget/ActionButton;->toggle()V

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v1, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "tweet"

    aput-object v6, v5, v9

    aput-object v10, v5, v7

    aput-object v3, v5, v8

    const/4 v3, 0x3

    aput-object v10, v5, v3

    const/4 v3, 0x4

    aput-object v0, v5, v3

    invoke-static {v5}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v9

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v5, v5, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v3, v4, v5, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v3, v4, v5, v10}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->i(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_e
    const-string/jumbo v0, "follow"

    goto :goto_3

    :cond_f
    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iput v7, v1, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v4, v1, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v1, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v2, v4, v5, v9, v1}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    goto :goto_4

    :cond_10
    const v0, 0x7f0902ac    # com.twitter.android.R.id.tweet_translation_link

    if-ne v2, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/provider/Tweet;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->c()Z

    move-result v0

    if-nez v0, :cond_11

    const-string/jumbo v0, "translation_button"

    invoke-direct {p0, v0}, Lcom/twitter/android/TweetFragment;->g(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->b(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->d(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_11
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->d()V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9

    const-wide/16 v7, -0x1

    const/4 v6, 0x0

    const/4 v4, -0x1

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "tweet"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/library/scribe/ScribeAssociation;)V

    new-instance v0, Lcom/twitter/android/xp;

    invoke-direct {v0, p0}, Lcom/twitter/android/xp;-><init>(Lcom/twitter/android/TweetFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->O:Lcom/twitter/library/client/j;

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-string/jumbo v2, "composition"

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$MediaType;->d:Ljava/util/EnumSet;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/twitter/android/PhotoSelectHelper;-><init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->ag:Lcom/twitter/android/PhotoSelectHelper;

    if-eqz p1, :cond_0

    const-string/jumbo v0, "as"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ActivitySummary;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    const-string/jumbo v0, "f"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->q:Z

    const-string/jumbo v0, "dw"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->r:Z

    const-string/jumbo v0, "fss"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetFragment;->J:Z

    const-string/jumbo v0, "social_context_type"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetFragment;->aa:I

    const-string/jumbo v0, "social_context_user_count"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetFragment;->ac:I

    const-string/jumbo v0, "activity_row_id"

    invoke-virtual {p1, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TweetFragment;->ab:J

    const-string/jumbo v0, "scribe_item"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->ae:Lcom/twitter/library/scribe/ScribeItem;

    const-string/jumbo v0, "magic_rec_id"

    invoke-virtual {p1, v0, v7, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/TweetFragment;->af:J

    const-string/jumbo v0, "in_reply_to_tweet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0029    # com.twitter.android.R.dimen.card_inset

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/TweetFragment;->t:I

    new-instance v0, Lcom/twitter/android/xc;

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v2, "tweet"

    const-string/jumbo v4, "avatar"

    const-string/jumbo v5, "profile_click"

    invoke-static {v1, v2, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v5, "tweet:stream:tweet:link:open_link"

    move-object v1, p0

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/xc;-><init>(Lcom/twitter/android/TweetFragment;Landroid/support/v4/app/Fragment;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    invoke-virtual {v0, v6}, Lcom/twitter/android/yb;->a(Z)V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/TweetFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->x:Landroid/content/Context;

    new-instance v1, Lcom/twitter/android/xn;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/xn;-><init>(Lcom/twitter/android/TweetFragment;Landroid/app/Activity;)V

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/scribe/ScribeAssociation;)V

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/android/client/a;)V

    invoke-static {}, Lkl;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iput-boolean v6, p0, Lcom/twitter/android/TweetFragment;->N:Z

    :cond_1
    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v4

    :pswitch_0
    sget-object v0, Lcom/twitter/library/provider/at;->l:Landroid/net/Uri;

    move-object v2, v0

    :goto_1
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v5, v3, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    const-string/jumbo v3, "ownerId"

    iget-object v5, p0, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v3, v5}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/Tweet;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v4, v0

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/twitter/library/provider/at;->m:Landroid/net/Uri;

    move-object v2, v0

    goto :goto_1

    :pswitch_2
    sget-object v0, Lcom/twitter/library/provider/at;->n:Landroid/net/Uri;

    move-object v2, v0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    const v0, 0x7f030040    # com.twitter.android.R.layout.conversation_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/TweetFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/PageableListView;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-static {}, Lkn;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v2, 0x7f0b0052    # com.twitter.android.R.color.faint_gray

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_0
    invoke-static {}, Lkn;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->J()V

    :cond_1
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onDestroy()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/TweetFragment;->b(ILcom/twitter/library/util/ar;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ag:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/TweetFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f090036    # com.twitter.android.R.id.content

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Ljava/lang/String;)V

    const v2, 0x7f0f00da    # com.twitter.android.R.string.copied_to_clipboard

    invoke-static {v1, v2, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public onPause()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    invoke-virtual {v0}, Lcom/twitter/android/xm;->b()V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onPause()V

    return-void
.end method

.method public onResume()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onResume()V

    invoke-static {}, Lkn;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->C()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->Z:Lcom/twitter/android/UserPresenceFragment;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/UserPresenceFragment;->b(J)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    invoke-virtual {v0}, Lcom/twitter/android/xm;->a()V

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/TweetListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "as"

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->h:Lcom/twitter/library/api/ActivitySummary;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "f"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->q:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "dw"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->r:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "fss"

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->J:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "social_context_type"

    iget v1, p0, Lcom/twitter/android/TweetFragment;->aa:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "social_context_user_count"

    iget v1, p0, Lcom/twitter/android/TweetFragment;->ac:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "activity_row_id"

    iget-wide v1, p0, Lcom/twitter/android/TweetFragment;->ab:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "scribe_item"

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->ae:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "magic_rec_id"

    iget-wide v1, p0, Lcom/twitter/android/TweetFragment;->af:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "in_reply_to_tweet"

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->c:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/TweetDetailView;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method public onStop()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/TweetListFragment;->onStop()V

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "tweet::stream::results"

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/TweetFragment;->a(JLjava/lang/String;)V

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TweetListFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    iget-object v3, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v3, v5}, Lcom/twitter/library/widget/PageableListView;->setScrollingCacheEnabled(Z)V

    invoke-virtual {v3, v5}, Lcom/twitter/library/widget/PageableListView;->setCacheColorHint(I)V

    new-instance v0, Lcom/twitter/android/xd;

    invoke-direct {v0, p0, v3}, Lcom/twitter/android/xd;-><init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/PageableListView;)V

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/PageableListView;->setOnPageScrollListener(Lcom/twitter/library/widget/k;)V

    invoke-virtual {v3}, Lcom/twitter/library/widget/PageableListView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    const v0, 0x7f03015d    # com.twitter.android.R.layout.tweet_detail_view

    invoke-virtual {v4, v0, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TweetDetailView;

    iget-boolean v1, p0, Lcom/twitter/android/TweetFragment;->X:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->a:Landroid/widget/ImageView;

    invoke-virtual {v1, p0}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v1, Lcom/twitter/android/widget/dj;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/widget/dj;-><init>(Landroid/view/View;Lcom/twitter/android/widget/TweetDetailView;)V

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v2, v1}, Lcom/twitter/library/widget/PageableListView;->setExploreByTouchHelper(Landroid/support/v4/widget/ExploreByTouchHelper;)V

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->f:Landroid/widget/TextView;

    invoke-virtual {v1, p0}, Landroid/widget/TextView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->b:Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {v1, p0}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/TweetDetailView;->setTranslationButtonClickListener(Landroid/view/View$OnClickListener;)V

    if-eqz p2, :cond_1

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/TweetDetailView;->b(Landroid/os/Bundle;)V

    :cond_1
    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->b()V

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->c:Landroid/view/ViewGroup;

    const v2, 0x7f09006c    # com.twitter.android.R.id.actionbar

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    const v2, 0x7f09006f    # com.twitter.android.R.id.favorite

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/twitter/android/TweetFragment;->u:Landroid/widget/ImageButton;

    const v2, 0x7f09006e    # com.twitter.android.R.id.retweet

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/twitter/android/TweetFragment;->v:Landroid/widget/ImageButton;

    const v2, 0x7f0300db    # com.twitter.android.R.layout.pad_view

    invoke-virtual {v4, v2, v3, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/TweetFragment;->s:Landroid/view/View;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->w:Landroid/view/ViewGroup;

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->h:Landroid/view/View;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->K:Landroid/view/View;

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->h:Landroid/view/View;

    const v2, 0x7f090029    # com.twitter.android.R.id.photo

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->L:Landroid/widget/ImageView;

    iget-object v1, v0, Lcom/twitter/android/widget/TweetDetailView;->i:Landroid/view/View;

    iput-object v1, p0, Lcom/twitter/android/TweetFragment;->U:Landroid/view/View;

    iget-object v0, v0, Lcom/twitter/android/widget/TweetDetailView;->j:Landroid/view/View;

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->V:Landroid/view/View;

    invoke-static {}, Lkl;->c()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->f()V

    :cond_2
    return-void
.end method

.method q()Lcom/twitter/android/xo;
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v0, Lcom/twitter/library/provider/Tweet;->o:J

    sget-object v0, Lcom/twitter/android/TweetFragment;->o:Landroid/support/v4/util/LruCache;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/xo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/xo;

    invoke-direct {v0}, Lcom/twitter/android/xo;-><init>()V

    sget-object v3, Lcom/twitter/android/TweetFragment;->o:Landroid/support/v4/util/LruCache;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0
.end method

.method r()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getMeasuredHeight()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getMeasuredHeight()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getMeasuredHeight()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TweetDetailView;->getMeasuredHeight()I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->s:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v2, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iget v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    if-eq v0, v2, :cond_0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->s:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method s()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/TweetFragment;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->b()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->r()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    iget-object v2, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v4}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    invoke-virtual {v0}, Lcom/twitter/android/xq;->getCount()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v0, v3}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    invoke-virtual {v0}, Lcom/twitter/android/xq;->notifyDataSetChanged()V

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v4}, Lcom/twitter/android/TweetFragment;->e(Z)V

    :cond_2
    iput-boolean v4, p0, Lcom/twitter/android/TweetFragment;->g:Z

    const-string/jumbo v0, "tweet:complete"

    invoke-virtual {p0}, Lcom/twitter/android/TweetFragment;->at()Lcom/twitter/library/metrics/h;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/metrics/o;->a(Ljava/lang/String;Lcom/twitter/library/metrics/h;)Lcom/twitter/library/metrics/o;

    move-result-object v0

    iget-wide v3, p0, Lcom/twitter/android/TweetFragment;->Q:J

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/metrics/o;->b(J)V

    invoke-virtual {v0}, Lcom/twitter/library/metrics/o;->j()V

    iget-object v0, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    if-eqz v0, :cond_3

    iget-object v0, v2, Lcom/twitter/library/provider/Tweet;->O:Lcom/twitter/library/api/TwitterStatusCard;

    iget-object v0, v0, Lcom/twitter/library/api/TwitterStatusCard;->cardInstanceData:Lcom/twitter/library/card/instance/CardInstanceData;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    const-string/jumbo v1, "impression"

    invoke-virtual {v0, v1}, Lcom/twitter/android/xn;->f(Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ai:Lcom/twitter/android/xl;

    invoke-interface {v0}, Lcom/twitter/android/xl;->f()V

    invoke-static {}, Lkn;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/xm;

    invoke-direct {v0, p0}, Lcom/twitter/android/xm;-><init>(Lcom/twitter/android/TweetFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->ah:Lcom/twitter/android/xm;

    invoke-virtual {v0}, Lcom/twitter/android/xm;->a()V

    goto :goto_0
.end method

.method public u()V
    .locals 4

    const/4 v3, 0x1

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->l:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getHeaderViewsCount()I

    move-result v1

    invoke-virtual {v0}, Lcom/twitter/library/widget/PageableListView;->getCount()I

    move-result v2

    add-int/2addr v1, v2

    add-int/lit8 v1, v1, -0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/widget/PageableListView;->setSelectionFromTop(II)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    iput-boolean v3, p0, Lcom/twitter/android/TweetFragment;->z:Z

    return-void
.end method

.method public v()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/xi;

    invoke-direct {v0, p0}, Lcom/twitter/android/xi;-><init>(Lcom/twitter/android/TweetFragment;)V

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    :cond_0
    return-void
.end method

.method public y()V
    .locals 2

    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/TweetFragment;->F()V

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->removeOnScrollChangedListener(Landroid/view/ViewTreeObserver$OnScrollChangedListener;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/TweetFragment;->aj:Landroid/view/ViewTreeObserver$OnScrollChangedListener;

    :cond_0
    return-void
.end method

.method public z()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v0}, Lcom/twitter/android/xq;->notifyDataSetChanged()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetFragment;->e:Lcom/twitter/android/xn;

    iget-object v1, p0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/provider/Tweet;)V

    return-void
.end method
