.class Lcom/twitter/android/qz;
.super Lcom/twitter/android/widget/ah;
.source "Twttr"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/android/to;

.field private final d:Lcom/twitter/android/client/c;

.field private final e:I

.field private final f:I

.field private final g:Lcom/twitter/library/client/aa;

.field private h:Lcom/twitter/library/api/TwitterUser;

.field private i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/rh;II)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, p2, p4}, Lcom/twitter/android/widget/ah;-><init>(Landroid/widget/ListAdapter;I)V

    iput-boolean v3, p0, Lcom/twitter/android/qz;->i:Z

    const-string/jumbo v0, "android_wtf_show_bio_1605"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_wtf_show_bio_1605"

    new-array v1, v4, [Ljava/lang/String;

    const-string/jumbo v2, "show_bio"

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {p2, v0}, Lcom/twitter/android/rh;->c(Z)V

    iput-object p1, p0, Lcom/twitter/android/qz;->b:Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/UsersActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "com.twitter.android.intent.action.FOLLOW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0xa

    if-eq v1, p3, :cond_0

    const-string/jumbo v1, "cluster_follow"

    iget-object v2, p0, Lcom/twitter/android/qz;->b:Landroid/content/Context;

    invoke-static {v2, v5}, Lcom/twitter/android/util/c;->a(Landroid/content/Context;I)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "cluster_follow_experiment"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    new-instance v1, Lcom/twitter/android/to;

    const v2, 0x7f0f0332    # com.twitter.android.R.string.profile_view_more

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/to;-><init>(Ljava/lang/String;Landroid/content/Intent;)V

    iput-object v1, p0, Lcom/twitter/android/qz;->c:Lcom/twitter/android/to;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qz;->d:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/qz;->g:Lcom/twitter/library/client/aa;

    iput p3, p0, Lcom/twitter/android/qz;->e:I

    and-int/lit8 v0, p4, 0x2

    if-eqz v0, :cond_1

    iput v4, p0, Lcom/twitter/android/qz;->f:I

    :goto_0
    return-void

    :cond_1
    iput v3, p0, Lcom/twitter/android/qz;->f:I

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    iget v0, p0, Lcom/twitter/android/qz;->e:I

    sparse-switch v0, :sswitch_data_0

    const/4 v0, 0x0

    invoke-static {p1, p2, v0}, Lcom/twitter/android/qz;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/qz;->b:Landroid/content/Context;

    const v1, 0x7f0f0560    # com.twitter.android.R.string.who_to_follow_title

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/twitter/android/qz;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/qz;->b:Landroid/content/Context;

    const v1, 0x7f0f0324    # com.twitter.android.R.string.profile_follow_recommendations

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, v0}, Lcom/twitter/android/qz;->a(Landroid/view/View;Landroid/view/ViewGroup;Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xa -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method protected a()Ljava/lang/Object;
    .locals 1

    sget-object v0, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    return-object v0
.end method

.method protected a(I)Ljava/lang/Object;
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/qz;->h:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/qz;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/rh;

    invoke-virtual {v0, p1}, Lcom/twitter/android/rh;->getItemId(I)J

    move-result-wide v2

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/qz;->b:Landroid/content/Context;

    const-class v4, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v0, v1, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "type"

    const/16 v4, 0xa

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    new-instance v4, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v4}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v5, 0x5

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/qz;->h:Lcom/twitter/library/api/TwitterUser;

    iget-wide v5, v5, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v5, v6}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v4

    const-string/jumbo v5, "profile"

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v4

    const-string/jumbo v5, "similar_to"

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/qz;->a:Landroid/widget/ListAdapter;

    check-cast v0, Lcom/twitter/android/rh;

    iget-object v0, v0, Lcom/twitter/android/rh;->g:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, v2, v3}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/qz;->h:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qz;->h:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/TwitterUser;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/qz;->c:Lcom/twitter/android/to;

    iget-object v0, v0, Lcom/twitter/android/to;->c:Landroid/content/Intent;

    const-string/jumbo v1, "username"

    iget-object v2, p1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "owner_id"

    iget-wide v2, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iput-object p1, p0, Lcom/twitter/android/qz;->h:Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {p0}, Lcom/twitter/android/qz;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method protected b(Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const v0, 0x7f030136    # com.twitter.android.R.layout.section_simple_row_view

    iget-object v1, p0, Lcom/twitter/android/qz;->c:Lcom/twitter/android/to;

    iget-object v2, p0, Lcom/twitter/android/qz;->d:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->V()F

    move-result v2

    invoke-static {v0, p1, p2, v1, v2}, Lcom/twitter/android/tp;->a(ILandroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/to;F)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/qz;->c:Lcom/twitter/android/to;

    iget-object v0, v0, Lcom/twitter/android/to;->c:Landroid/content/Intent;

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    iget v0, p0, Lcom/twitter/android/qz;->e:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/qz;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/qz;->d:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/qz;->g:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "profile:similar_to:::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v6, p0, Lcom/twitter/android/qz;->i:Z

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/widget/ah;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method
