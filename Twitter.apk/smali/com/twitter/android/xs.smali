.class Lcom/twitter/android/xs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/view/View;

.field final synthetic b:Lcom/twitter/library/provider/Tweet;

.field final synthetic c:Lcom/twitter/android/xr;


# direct methods
.method constructor <init>(Lcom/twitter/android/xr;Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xs;->c:Lcom/twitter/android/xr;

    iput-object p2, p0, Lcom/twitter/android/xs;->a:Landroid/view/View;

    iput-object p3, p0, Lcom/twitter/android/xs;->b:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/xs;->c:Lcom/twitter/android/xr;

    invoke-static {v0}, Lcom/twitter/android/xr;->a(Lcom/twitter/android/xr;)Lcom/twitter/android/widget/aj;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/xs;->a:Landroid/view/View;

    iget-object v2, p0, Lcom/twitter/android/xs;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/aj;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V

    iget-object v0, p0, Lcom/twitter/android/xs;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
