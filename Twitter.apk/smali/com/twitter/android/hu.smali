.class Lcom/twitter/android/hu;
.super Landroid/support/v4/app/DialogFragment;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/GeoDebugActivity;

.field private b:Lcom/google/android/gms/maps/model/k;

.field private c:Landroid/location/Location;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/EditText;

.field private f:Landroid/widget/Spinner;

.field private g:Landroid/widget/Spinner;

.field private h:Lcom/twitter/android/ht;


# direct methods
.method public constructor <init>(Lcom/twitter/android/GeoDebugActivity;Lcom/google/android/gms/maps/model/k;Landroid/location/Location;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hu;->a:Lcom/twitter/android/GeoDebugActivity;

    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/hu;->b:Lcom/google/android/gms/maps/model/k;

    iput-object p3, p0, Lcom/twitter/android/hu;->c:Landroid/location/Location;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/hu;)Landroid/widget/Spinner;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->g:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/hu;)Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->c:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/hu;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->d:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/hu;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->e:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/hu;)Landroid/widget/Spinner;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->f:Landroid/widget/Spinner;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/hu;)Lcom/twitter/android/ht;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->h:Lcom/twitter/android/ht;

    return-object v0
.end method

.method static synthetic g(Lcom/twitter/android/hu;)Lcom/google/android/gms/maps/model/k;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/hu;->b:Lcom/google/android/gms/maps/model/k;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/android/ht;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hu;->h:Lcom/twitter/android/ht;

    return-void
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    const v6, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    const v5, 0x1090008    # android.R.layout.simple_spinner_item

    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/twitter/android/hu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/hu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030084    # com.twitter.android.R.layout.geo_location_dialog

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v0, 0x7f09019b    # com.twitter.android.R.id.geo_header

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/hu;->d:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/hu;->d:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/hu;->c:Landroid/location/Location;

    invoke-static {v3}, Lcom/twitter/library/platform/h;->b(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v0, 0x7f09019d    # com.twitter.android.R.id.input

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/hu;->e:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/hu;->e:Landroid/widget/EditText;

    new-instance v3, Lcom/twitter/android/hv;

    invoke-direct {v3, p0}, Lcom/twitter/android/hv;-><init>(Lcom/twitter/android/hu;)V

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/hu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const-string/jumbo v0, "gps"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "network"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x10

    if-gt v0, v4, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/hu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/platform/p;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const-string/jumbo v0, "fused"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    :cond_1
    const v0, 0x7f09019e    # com.twitter.android.R.id.source

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/twitter/android/hu;->f:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/twitter/android/hu;->f:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/hu;->f:Landroid/widget/Spinner;

    new-instance v3, Lcom/twitter/android/hw;

    invoke-direct {v3, p0}, Lcom/twitter/android/hw;-><init>(Lcom/twitter/android/hu;)V

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    new-instance v3, Landroid/widget/ArrayAdapter;

    invoke-virtual {p0}, Lcom/twitter/android/hu;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-direct {v3, v0, v5}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I)V

    invoke-virtual {v3, v6}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    const-string/jumbo v0, "Latitude"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "Longitude"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "H-Accuracy"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "Altitude"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "Speed"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "Bearing"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "Timestamp"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const-string/jumbo v0, "Source"

    invoke-virtual {v3, v0}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    const v0, 0x7f09019c    # com.twitter.android.R.id.location_fields

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/twitter/android/hu;->g:Landroid/widget/Spinner;

    iget-object v0, p0, Lcom/twitter/android/hu;->g:Landroid/widget/Spinner;

    invoke-virtual {v0, v3}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/hu;->g:Landroid/widget/Spinner;

    new-instance v2, Lcom/twitter/android/hx;

    invoke-direct {v2, p0}, Lcom/twitter/android/hx;-><init>(Lcom/twitter/android/hu;)V

    invoke-virtual {v0, v2}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    const-string/jumbo v0, "Edit Location"

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v2, "Update"

    new-instance v3, Lcom/twitter/android/ia;

    invoke-direct {v3, p0}, Lcom/twitter/android/ia;-><init>(Lcom/twitter/android/hu;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const-string/jumbo v2, "Delete"

    new-instance v3, Lcom/twitter/android/hz;

    invoke-direct {v3, p0}, Lcom/twitter/android/hz;-><init>(Lcom/twitter/android/hu;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v2, 0x7f0f0089    # com.twitter.android.R.string.cancel

    new-instance v3, Lcom/twitter/android/hy;

    invoke-direct {v3, p0}, Lcom/twitter/android/hy;-><init>(Lcom/twitter/android/hu;)V

    invoke-virtual {v0, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
