.class public Lcom/twitter/android/ChangeScreenNameActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:Landroid/widget/EditText;

.field private b:Landroid/widget/Button;

.field private c:Lcom/twitter/library/client/Session;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private f()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/4 v1, 0x6

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->length()I

    move-result v0

    const/16 v1, 0xf

    if-gt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->a:Landroid/widget/EditText;

    invoke-static {p0, v0, v4}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangeScreenNameActivity;->d:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ChangeScreenNameActivity;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ChangeScreenNameActivity;->c:Lcom/twitter/library/client/Session;

    iget-object v3, p0, Lcom/twitter/android/ChangeScreenNameActivity;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v3

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/UserSettings;ZLjava/lang/String;)Ljava/lang/String;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v1, 0x7f0f029a    # com.twitter.android.R.string.new_screen_name_same_as_old

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030028    # com.twitter.android.R.layout.change_screen_name

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->d:Ljava/lang/String;

    new-instance v0, Lcom/twitter/android/cn;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/cn;-><init>(Lcom/twitter/android/ChangeScreenNameActivity;Lcom/twitter/android/cm;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangeScreenNameActivity;->a(Lcom/twitter/library/client/j;)V

    const v0, 0x7f0900d2    # com.twitter.android.R.id.screen_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangeScreenNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->a:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    const v0, 0x7f0900d3    # com.twitter.android.R.id.update_screen_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/ChangeScreenNameActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->b:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->b:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangeScreenNameActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ChangeScreenNameActivity;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v1, p0, Lcom/twitter/android/ChangeScreenNameActivity;->b:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ChangeScreenNameActivity;->c:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0900d3    # com.twitter.android.R.id.update_screen_name

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ChangeScreenNameActivity;->g()V

    :cond_0
    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
