.class Lcom/twitter/android/vp;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field final synthetic a:Lcom/twitter/library/client/Session;

.field final synthetic b:Lcom/twitter/library/provider/Tweet;

.field final synthetic c:Landroid/support/v4/app/FragmentActivity;

.field final synthetic d:Lcom/twitter/android/vn;


# direct methods
.method constructor <init>(Lcom/twitter/android/vn;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/support/v4/app/FragmentActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vp;->d:Lcom/twitter/android/vn;

    iput-object p2, p0, Lcom/twitter/android/vp;->a:Lcom/twitter/library/client/Session;

    iput-object p3, p0, Lcom/twitter/android/vp;->b:Lcom/twitter/library/provider/Tweet;

    iput-object p4, p0, Lcom/twitter/android/vp;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 4

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vp;->d:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/vp;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/vp;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vp;->d:Lcom/twitter/android/vn;

    iget-object v2, p0, Lcom/twitter/android/vp;->c:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/vn;->a(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/vp;->d:Lcom/twitter/android/vn;

    const-string/jumbo v1, "delete"

    iget-object v2, p0, Lcom/twitter/android/vp;->b:Lcom/twitter/library/provider/Tweet;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    :cond_0
    return-void
.end method
