.class public Lcom/twitter/android/vs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private final a:Lcom/twitter/library/client/aa;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/android/vu;

.field private d:Lcom/twitter/android/vt;

.field private e:Lcom/twitter/android/vt;

.field private f:J

.field private g:Lcom/twitter/android/yd;

.field private h:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/vu;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/twitter/android/vs;->f:J

    if-nez p2, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "You must set an OnActionClickListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/vs;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/vs;->a:Lcom/twitter/library/client/aa;

    iput-object p2, p0, Lcom/twitter/android/vs;->c:Lcom/twitter/android/vu;

    new-instance v0, Lcom/twitter/android/vt;

    const v1, 0x7f040008    # com.twitter.android.R.anim.fade_out

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/vt;-><init>(Landroid/view/animation/Animation;Z)V

    iput-object v0, p0, Lcom/twitter/android/vs;->e:Lcom/twitter/android/vt;

    new-instance v0, Lcom/twitter/android/vt;

    const v1, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p1, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/vt;-><init>(Landroid/view/animation/Animation;Z)V

    iput-object v0, p0, Lcom/twitter/android/vs;->d:Lcom/twitter/android/vt;

    return-void
.end method

.method private static a(Landroid/view/View;)Lcom/twitter/library/view/TweetActionType;
    .locals 2

    invoke-virtual {p0}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09006f    # com.twitter.android.R.id.favorite

    if-ne v0, v1, :cond_0

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->b:Lcom/twitter/library/view/TweetActionType;

    :goto_0
    return-object v0

    :cond_0
    const v1, 0x7f09006e    # com.twitter.android.R.id.retweet

    if-ne v0, v1, :cond_1

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->c:Lcom/twitter/library/view/TweetActionType;

    goto :goto_0

    :cond_1
    const v1, 0x7f09006d    # com.twitter.android.R.id.reply

    if-ne v0, v1, :cond_2

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->d:Lcom/twitter/library/view/TweetActionType;

    goto :goto_0

    :cond_2
    const v1, 0x7f090071    # com.twitter.android.R.id.delete

    if-ne v0, v1, :cond_3

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->f:Lcom/twitter/library/view/TweetActionType;

    goto :goto_0

    :cond_3
    const v1, 0x7f090070    # com.twitter.android.R.id.share

    if-ne v0, v1, :cond_4

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->g:Lcom/twitter/library/view/TweetActionType;

    goto :goto_0

    :cond_4
    const v1, 0x7f0901a3    # com.twitter.android.R.id.dismiss

    if-ne v0, v1, :cond_5

    sget-object v0, Lcom/twitter/library/view/TweetActionType;->h:Lcom/twitter/library/view/TweetActionType;

    goto :goto_0

    :cond_5
    sget-object v0, Lcom/twitter/library/view/TweetActionType;->a:Lcom/twitter/library/view/TweetActionType;

    goto :goto_0
.end method

.method private a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/vt;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/twitter/android/vs;->c:Lcom/twitter/android/vu;

    invoke-interface {v0}, Lcom/twitter/android/vu;->b()V

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/vs;->a(Landroid/view/View;Z)V

    invoke-virtual {p3, p1, p2}, Lcom/twitter/android/vt;->a(Landroid/view/View;Landroid/view/ViewGroup;)V

    return-void
.end method

.method private a(Landroid/view/View;Z)V
    .locals 2

    const/4 v1, 0x0

    if-eqz p2, :cond_0

    invoke-virtual {p1, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x4

    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p1, v1}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method

.method private b(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/vt;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getHeight()I

    move-result v0

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setMinimumHeight(I)V

    iget-object v0, p0, Lcom/twitter/android/vs;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/vs;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    iget-object v2, v2, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    invoke-static {v0, v1, v2, p2, p0}, Lcom/twitter/android/widget/dn;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Landroid/view/ViewGroup;Landroid/view/View$OnClickListener;)V

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    iget-boolean v0, p0, Lcom/twitter/android/vs;->h:Z

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/twitter/library/util/Util;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vs;->c:Lcom/twitter/android/vu;

    invoke-interface {v0}, Lcom/twitter/android/vu;->a()V

    invoke-virtual {p3, p1, p2}, Lcom/twitter/android/vt;->a(Landroid/view/View;Landroid/view/ViewGroup;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1, v3}, Lcom/twitter/android/vs;->a(Landroid/view/View;Z)V

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-lez v0, :cond_0

    invoke-virtual {p2, v3}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->sendAccessibilityEvent(I)V

    goto :goto_0
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/vs;->f:J

    return-wide v0
.end method

.method public a(J)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/vs;->h:Z

    iput-wide p1, p0, Lcom/twitter/android/vs;->f:J

    return-void
.end method

.method public a(Lcom/twitter/android/yd;J)V
    .locals 3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/vs;->h:Z

    iget-wide v0, p0, Lcom/twitter/android/vs;->f:J

    cmp-long v0, v0, p2

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/vs;->b()V

    iput-wide p2, p0, Lcom/twitter/android/vs;->f:J

    iput-object p1, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    iget-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/twitter/android/yd;->b:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    :cond_0
    iget-object v0, p1, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v1, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/vs;->e:Lcom/twitter/android/vt;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/vs;->b(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/vt;)V

    :cond_1
    return-void
.end method

.method public b()V
    .locals 5

    const-wide/high16 v3, -0x8000000000000000L

    iget-wide v0, p0, Lcom/twitter/android/vs;->f:J

    cmp-long v0, v0, v3

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v0, v0, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/vs;->d:Lcom/twitter/android/vt;

    invoke-direct {p0, v1, v0, v2}, Lcom/twitter/android/vs;->a(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/vt;)V

    :cond_0
    iput-wide v3, p0, Lcom/twitter/android/vs;->f:J

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    :cond_1
    return-void
.end method

.method public b(Lcom/twitter/android/yd;J)V
    .locals 4

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/twitter/android/vs;->f:J

    cmp-long v0, p2, v2

    if-nez v0, :cond_3

    iput-object p1, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    const/4 v0, 0x0

    iget-object v2, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    if-nez v2, :cond_0

    iget-object v0, p1, Lcom/twitter/android/yd;->b:Landroid/view/ViewStub;

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    move v0, v1

    :cond_0
    if-nez v0, :cond_1

    iget-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p1, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v1, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/vs;->e:Lcom/twitter/android/vt;

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/vs;->b(Landroid/view/View;Landroid/view/ViewGroup;Lcom/twitter/android/vt;)V

    :cond_2
    :goto_0
    return-void

    :cond_3
    iget-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getVisibility()I

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p1, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/vs;->a(Landroid/view/View;Z)V

    iget-object v0, p1, Lcom/twitter/android/yd;->c:Landroid/view/ViewGroup;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vs;->g:Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/vs;->c:Lcom/twitter/android/vu;

    invoke-static {p1}, Lcom/twitter/android/vs;->a(Landroid/view/View;)Lcom/twitter/library/view/TweetActionType;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Lcom/twitter/android/vu;->a(Lcom/twitter/library/view/TweetActionType;Lcom/twitter/library/provider/Tweet;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/vs;->b()V

    :cond_0
    return-void
.end method
