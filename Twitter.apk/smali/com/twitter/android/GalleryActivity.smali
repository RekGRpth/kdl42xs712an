.class public Lcom/twitter/android/GalleryActivity;
.super Lcom/twitter/android/MediaTweetActivity;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private A:Lcom/twitter/android/hd;

.field private B:Lcom/twitter/library/widget/SlidingPanel;

.field private C:Z

.field private E:Z

.field private F:Z

.field private G:J

.field private H:Lcom/twitter/android/hk;

.field a:Landroid/view/animation/Animation;

.field b:Landroid/view/animation/Animation;

.field c:Lcom/twitter/android/gy;

.field d:Lcom/twitter/library/provider/Tweet;

.field e:Lcom/twitter/library/api/MediaEntity;

.field f:Z

.field private g:Landroid/content/Intent;

.field private h:Lcom/twitter/library/widget/TweetView;

.field private i:Landroid/support/v4/view/ViewPager;

.field private j:J

.field private k:Z

.field private l:I

.field private m:Z

.field private n:Landroid/view/ViewGroup;

.field private o:Landroid/widget/TextView;

.field private p:Lcom/twitter/library/client/Session;

.field private q:I

.field private r:[Ljava/lang/String;

.field private s:Lcom/twitter/library/scribe/ScribeAssociation;

.field private t:Lcom/twitter/library/scribe/ScribeItem;

.field private u:Lcom/twitter/android/hb;

.field private v:Z

.field private w:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

.field private x:I

.field private y:I

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/twitter/android/MediaTweetActivity;-><init>()V

    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/twitter/android/GalleryActivity;->j:J

    iput v3, p0, Lcom/twitter/android/GalleryActivity;->l:I

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->m:Z

    iput v3, p0, Lcom/twitter/android/GalleryActivity;->q:I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->C:Z

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->E:Z

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->F:Z

    return-void
.end method

.method private a(Lip;)Landroid/os/Bundle;
    .locals 4

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "user_id"

    invoke-virtual {p1}, Lip;->g()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    invoke-virtual {p1}, Lip;->h()Lcom/twitter/library/api/PromotedContent;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "impression_id"

    iget-object v3, v1, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "earned"

    invoke-virtual {v1}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    const-string/jumbo v1, "age_before_timestamp"

    invoke-virtual {p1}, Lip;->f()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;)Landroid/support/v4/view/ViewPager;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method private a(IJLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:[Ljava/lang/String;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4, p5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->t:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method

.method private a(ILcom/twitter/library/provider/Tweet;)V
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:[Ljava/lang/String;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    const/16 v0, 0x9

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:[Ljava/lang/String;

    aget-object v0, v0, p1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v5, p2, v2, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->t:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    return-void
.end method

.method private a(JLjava/util/List;Ljava/util/Set;)V
    .locals 10

    const/4 v9, 0x0

    new-instance v0, Ljc;

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    move-object v1, p0

    move-wide v3, p1

    move-object v7, p3

    move-object v8, p4

    invoke-direct/range {v0 .. v8}, Ljc;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JJLjava/util/List;Ljava/util/Set;)V

    invoke-virtual {p0, v0, v9, v9}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/library/service/b;II)Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;IJLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/twitter/android/GalleryActivity;->a(IJLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;ILcom/twitter/library/provider/Tweet;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/library/provider/Tweet;)V

    return-void
.end method

.method private a(Lcom/twitter/library/provider/Tweet;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->h:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/TweetView;->setTweet(Lcom/twitter/library/provider/Tweet;)V

    iput-object p1, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->V()V

    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->f()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/GalleryActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/GalleryActivity;->C:Z

    return p1
.end method

.method static synthetic b(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/widget/SlidingPanel;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->B:Lcom/twitter/library/widget/SlidingPanel;

    return-object v0
.end method

.method private b(I)Ljava/lang/String;
    .locals 11

    const/4 v10, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v1, 0x0

    const-string/jumbo v0, ""

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->r:[Ljava/lang/String;

    :goto_0
    return-object v1

    :pswitch_0
    const-string/jumbo v1, "profile"

    :goto_1
    const/16 v2, 0x9

    new-array v2, v2, [Ljava/lang/String;

    new-array v3, v10, [Ljava/lang/String;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "next:click"

    aput-object v4, v3, v9

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v6

    new-array v3, v10, [Ljava/lang/String;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "prev:click"

    aput-object v4, v3, v9

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v7

    new-array v3, v10, [Ljava/lang/String;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "photo:impression"

    aput-object v4, v3, v9

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v8

    new-array v3, v10, [Ljava/lang/String;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "tweet:click"

    aput-object v4, v3, v9

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v9

    new-array v3, v10, [Ljava/lang/String;

    aput-object v1, v3, v6

    aput-object v0, v3, v7

    const-string/jumbo v4, "gallery"

    aput-object v4, v3, v8

    const-string/jumbo v4, "tweet:double_click"

    aput-object v4, v3, v9

    invoke-static {v3}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v10

    const/4 v3, 0x5

    new-array v4, v10, [Ljava/lang/String;

    aput-object v1, v4, v6

    aput-object v0, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "media_tag_summary:click"

    aput-object v5, v4, v9

    invoke-static {v4}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x6

    new-array v4, v10, [Ljava/lang/String;

    aput-object v1, v4, v6

    aput-object v0, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "remove_my_media_tag:click"

    aput-object v5, v4, v9

    invoke-static {v4}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x7

    new-array v4, v10, [Ljava/lang/String;

    aput-object v1, v4, v6

    aput-object v0, v4, v7

    const-string/jumbo v5, "gallery"

    aput-object v5, v4, v8

    const-string/jumbo v5, "media_tagged_user:follow"

    aput-object v5, v4, v9

    invoke-static {v4}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/16 v3, 0x8

    new-array v4, v10, [Ljava/lang/String;

    aput-object v1, v4, v6

    aput-object v0, v4, v7

    const-string/jumbo v0, "gallery"

    aput-object v0, v4, v8

    const-string/jumbo v0, "media_tagged_user:unfollow"

    aput-object v0, v4, v9

    invoke-static {v4}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v3

    iput-object v2, p0, Lcom/twitter/android/GalleryActivity;->r:[Ljava/lang/String;

    goto/16 :goto_0

    :pswitch_1
    const-string/jumbo v1, "events"

    goto/16 :goto_1

    :pswitch_2
    const-string/jumbo v1, "search"

    goto/16 :goto_1

    :pswitch_3
    const-string/jumbo v1, "search"

    const-string/jumbo v0, "cluster"

    goto/16 :goto_1

    :pswitch_4
    const-string/jumbo v1, "home"

    goto/16 :goto_1

    :pswitch_5
    const-string/jumbo v1, "tweet"

    goto/16 :goto_1

    :pswitch_6
    const-string/jumbo v1, "profile_tweets"

    goto/16 :goto_1

    :pswitch_7
    const-string/jumbo v1, "list"

    goto/16 :goto_1

    :pswitch_8
    const-string/jumbo v1, "favorites"

    goto/16 :goto_1

    :pswitch_9
    const-string/jumbo v1, "network_activity"

    goto/16 :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method private b(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v0}, Lcom/twitter/android/gy;->a()Lcom/twitter/android/hi;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    if-eqz v1, :cond_0

    iget-object v0, v0, Lcom/twitter/android/hi;->a:Lcom/twitter/library/widget/MultiTouchImageView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/MultiTouchImageView;->setPositionLocked(Z)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/service/b;II)Z
    .locals 1

    invoke-virtual {p0, p1, p2, p3}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/library/service/b;II)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/hd;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/GalleryActivity;)Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    return-object v0
.end method

.method private f()V
    .locals 6

    const/4 v5, -0x2

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->S()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0, v4, v4}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/u;->a(Ljava/util/List;)Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v2, v1}, Lcom/twitter/android/hd;->a(Ljava/util/List;)V

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/widget/TextView;

    new-instance v3, Lcom/twitter/android/gv;

    invoke-direct {v3, p0, v1, v0}, Lcom/twitter/android/gv;-><init>(Lcom/twitter/android/GalleryActivity;Ljava/util/List;Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/widget/TextView;

    const v2, 0x7f0201f5    # com.twitter.android.R.drawable.ic_photo_tag_tweet_detail

    invoke-static {p0, v1, v2}, Lcom/twitter/library/util/u;->a(Landroid/content/Context;Ljava/util/List;I)Ljava/lang/CharSequence;

    move-result-object v1

    sget-object v2, Landroid/widget/TextView$BufferType;->SPANNABLE:Landroid/widget/TextView$BufferType;

    invoke-virtual {v0, v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;Landroid/widget/TextView$BufferType;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v5, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    iget v1, p0, Lcom/twitter/android/GalleryActivity;->x:I

    iget v2, p0, Lcom/twitter/android/GalleryActivity;->y:I

    iget v3, p0, Lcom/twitter/android/GalleryActivity;->z:I

    invoke-virtual {v0, v1, v2, v4, v3}, Landroid/widget/LinearLayout$LayoutParams;->setMargins(IIII)V

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method private g()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->h:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/android/rg;->TweetView:[I

    const v3, 0x7f01005a    # com.twitter.android.R.attr.tweetViewStyle

    invoke-virtual {p0, v1, v2, v3, v5}, Lcom/twitter/android/GalleryActivity;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v1

    const/16 v2, 0x2d

    invoke-virtual {v1, v2, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v2

    const/16 v3, 0x31

    const v4, 0x7f0c00d4    # com.twitter.android.R.dimen.user_image_size

    invoke-virtual {v1, v3, v4}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v3

    const/16 v4, 0x2f

    invoke-virtual {v1, v4, v5}, Landroid/content/res/TypedArray;->getDimensionPixelOffset(II)I

    move-result v1

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->h:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4}, Lcom/twitter/library/widget/TweetView;->getPaddingLeft()I

    move-result v4

    add-int/2addr v2, v4

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->x:I

    const v1, 0x7f0c0071    # com.twitter.android.R.dimen.gallery_activity_media_tag_with_tweet_margin_top

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->y:I

    :goto_0
    const v1, 0x7f0c006e    # com.twitter.android.R.dimen.gallery_activity_media_tag_margin_bottom

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/GalleryActivity;->z:I

    return-void

    :cond_0
    const v1, 0x7f0c006f    # com.twitter.android.R.dimen.gallery_activity_media_tag_margin_left

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->x:I

    const v1, 0x7f0c0070    # com.twitter.android.R.dimen.gallery_activity_media_tag_margin_top

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->y:I

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/android/GalleryActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    return v0
.end method

.method static synthetic h(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    return-object v0
.end method

.method private h()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->m:Z

    if-nez v0, :cond_0

    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/library/provider/Tweet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->m:Z

    :cond_0
    return-void
.end method

.method static synthetic i(Lcom/twitter/android/GalleryActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->k()V

    return-void
.end method

.method static synthetic j(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 5

    const/4 v4, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->F:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    const v0, 0x7f040016    # com.twitter.android.R.anim.scale_up

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    new-instance v1, Lgf;

    const/high16 v2, 0x42200000    # 40.0f

    const/high16 v3, 0x40f00000    # 7.5f

    invoke-direct {v1, v2, v3}, Lgf;-><init>(FF)V

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->startAnimation(Landroid/view/animation/Animation;)V

    iput-boolean v4, p0, Lcom/twitter/android/GalleryActivity;->F:Z

    :cond_0
    return-void
.end method

.method static synthetic l(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    return-object v0
.end method

.method private l()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    new-instance v1, Lcom/twitter/android/gw;

    invoke-direct {v1, p0}, Lcom/twitter/android/gw;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    :cond_0
    return-void
.end method

.method static synthetic m(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/GalleryActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->C:Z

    return v0
.end method

.method static synthetic o(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a()Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->a:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lcom/twitter/library/client/App;->r()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "tablet_gallery_gestures_2100"

    invoke-static {v2}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v2, "tablet_gallery_custom_transitions_1897"

    invoke-static {v2}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v2, "tablet_gallery_custom_transitions_1897"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "no_custom_transitions"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->E:Z

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->E:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_0

    const v0, 0x7f040011    # com.twitter.android.R.anim.quicker_fade_in

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->overridePendingTransition(II)V

    :cond_0
    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v2, 0x7f0300e3    # com.twitter.android.R.layout.photo_pager

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public a(I)V
    .locals 4

    iget v0, p0, Lcom/twitter/android/GalleryActivity;->l:I

    if-ne v0, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput p1, p0, Lcom/twitter/android/GalleryActivity;->l:I

    if-nez p1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v2

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_4

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3, p1}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0}, Lcom/twitter/android/hd;->a()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->c()V

    goto :goto_1

    :cond_4
    if-nez p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->B:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->requestFocus()Z

    goto :goto_0
.end method

.method protected a(IILcom/twitter/library/service/b;)V
    .locals 6

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/MediaTweetActivity;->a(IILcom/twitter/library/service/b;)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    move-object v0, p3

    check-cast v0, Ljc;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {v0}, Ljc;->e()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/e;

    invoke-virtual {v1}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0f026e    # com.twitter.android.R.string.media_tag_delete_success

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    const-string/jumbo v3, "tags"

    new-instance v4, Ljava/util/ArrayList;

    iget-object v5, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {v5}, Lcom/twitter/library/util/s;->a(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v5

    invoke-virtual {v0}, Ljc;->f()Ljava/util/Set;

    move-result-object v0

    invoke-static {v5, v0}, Lcom/twitter/library/util/u;->a(Ljava/util/List;Ljava/util/Set;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move v0, v1

    :goto_1
    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_1
    const v0, 0x7f0f026d    # com.twitter.android.R.string.media_tag_delete_error

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    const v2, 0x7f090329    # com.twitter.android.R.id.remove_tag

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lhn;->b(Z)Lhn;

    goto :goto_1

    :pswitch_2
    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-nez v1, :cond_0

    check-cast p3, Lip;

    invoke-virtual {p3}, Lip;->g()J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v3, p1, v1, v2}, Lcom/twitter/android/hd;->a(IJ)V

    invoke-direct {p0, p3}, Lcom/twitter/android/GalleryActivity;->a(Lip;)Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    invoke-virtual {p3}, Lip;->e()[I

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v2, v0, v3, v4, v1}, Lcom/twitter/android/client/c;->a(I[ILcom/twitter/library/client/Session;Landroid/os/Bundle;)V

    goto/16 :goto_0

    :pswitch_3
    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-nez v0, :cond_0

    check-cast p3, Liq;

    invoke-virtual {p3}, Liq;->e()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v2, p1, v0, v1}, Lcom/twitter/android/hd;->a(IJ)V

    goto/16 :goto_0

    :cond_2
    move v0, v1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 4

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    const v1, 0x7f090329    # com.twitter.android.R.id.remove_tag

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->u:J

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->o()Ljava/util/ArrayList;

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/s;->b(Ljava/util/Collection;)Ljava/util/List;

    move-result-object v3

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/twitter/android/GalleryActivity;->a(JLjava/util/List;Ljava/util/Set;)V

    const/4 v0, 0x6

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/library/provider/Tweet;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->C:Z

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 13

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v5

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v6

    const-string/jumbo v0, "association"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v0, "dm"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    const v0, 0x7f09020c    # com.twitter.android.R.id.sliding_panel

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SlidingPanel;

    new-instance v1, Lcom/twitter/android/hd;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/hd;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/library/widget/SlidingPanel;)V

    iput-object v1, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->b(I)V

    new-instance v2, Lcom/twitter/android/hc;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/hc;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/hd;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/SlidingPanel;->setPanelSlideListener(Lcom/twitter/library/widget/o;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setClipChildren(Z)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setFadeMode(I)V

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setCoveredFadeColor(I)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->B:Lcom/twitter/library/widget/SlidingPanel;

    const/4 v3, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const-string/jumbo v0, "li"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string/jumbo v0, "li"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v3

    const-string/jumbo v0, "list_starting_index"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string/jumbo v0, "list_starting_index"

    const/4 v1, 0x0

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    move-object v1, v2

    move v2, v0

    :goto_1
    const-string/jumbo v0, "etc"

    const/4 v4, 0x1

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    const-string/jumbo v0, "context"

    const/4 v4, -0x1

    invoke-virtual {v6, v0, v4}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity;->b(I)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v0, "item"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->t:Lcom/twitter/library/scribe/ScribeItem;

    new-instance v0, Lcom/twitter/android/gy;

    invoke-direct {v0, p0}, Lcom/twitter/android/gy;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    const/4 v4, 0x3

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v9, 0x7f0c0080    # com.twitter.android.R.dimen.list_row_padding

    invoke-virtual {v4, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-virtual {v0, v4}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/twitter/android/ha;

    const/4 v4, 0x0

    invoke-direct {v0, p0, v4}, Lcom/twitter/android/ha;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/gt;)V

    new-instance v9, Landroid/support/v4/view/GestureDetectorCompat;

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    invoke-direct {v9, v4, v0}, Landroid/support/v4/view/GestureDetectorCompat;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    const/4 v0, 0x0

    invoke-virtual {v9, v0}, Landroid/support/v4/view/GestureDetectorCompat;->setIsLongpressEnabled(Z)V

    invoke-static {}, Lcom/twitter/library/client/App;->r()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string/jumbo v0, "tablet_gallery_gestures_2100"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v10, 0x0

    const-string/jumbo v11, "no_gestures"

    aput-object v11, v4, v10

    invoke-static {v0, v4}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    new-instance v0, Landroid/view/ScaleGestureDetector;

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    new-instance v10, Lcom/twitter/android/hj;

    const/4 v11, 0x0

    invoke-direct {v10, p0, v11}, Lcom/twitter/android/hj;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/gt;)V

    invoke-direct {v0, v4, v10}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    move-object v4, v0

    :goto_2
    const v0, 0x7f09020d    # com.twitter.android.R.id.gallery_control

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    new-instance v10, Lcom/twitter/android/gt;

    invoke-direct {v10, p0, v9, v4}, Lcom/twitter/android/gt;-><init>(Lcom/twitter/android/GalleryActivity;Landroid/support/v4/view/GestureDetectorCompat;Landroid/view/ScaleGestureDetector;)V

    invoke-virtual {v0, v10}, Landroid/view/ViewGroup;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->l()V

    const v0, 0x7f09020e    # com.twitter.android.R.id.media_tags

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->o:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    const v4, 0x7f09020f    # com.twitter.android.R.id.media_tweet

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/TweetView;

    const/4 v4, 0x1

    iget-boolean v9, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v9, :cond_c

    const/4 v4, 0x0

    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    :cond_0
    :goto_3
    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    iget-object v9, v5, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    invoke-virtual {v0, v9}, Lcom/twitter/library/widget/TweetView;->setProvider(Lcom/twitter/library/widget/ap;)V

    iget-boolean v9, v5, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v0, v9}, Lcom/twitter/library/widget/TweetView;->setRenderRTL(Z)V

    const/4 v9, 0x1

    invoke-virtual {v0, v9}, Lcom/twitter/library/widget/TweetView;->setHideMediaTagSummary(Z)V

    if-eqz v7, :cond_1

    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/TweetView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->h:Lcom/twitter/library/widget/TweetView;

    new-instance v7, Lcom/twitter/android/da;

    invoke-direct {v7, p0}, Lcom/twitter/android/da;-><init>(Lcom/twitter/android/db;)V

    const v9, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p0, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    const-wide/16 v10, 0x96

    invoke-virtual {v9, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    iput-object v9, p0, Lcom/twitter/android/GalleryActivity;->a:Landroid/view/animation/Animation;

    const v9, 0x7f040008    # com.twitter.android.R.anim.fade_out

    invoke-static {p0, v9}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    const/4 v7, 0x1

    invoke-virtual {v9, v7}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    const-wide/16 v10, 0x96

    invoke-virtual {v9, v10, v11}, Landroid/view/animation/Animation;->setDuration(J)V

    iput-object v9, p0, Lcom/twitter/android/GalleryActivity;->b:Landroid/view/animation/Animation;

    new-instance v7, Lcom/twitter/android/hk;

    const/4 v9, 0x0

    invoke-direct {v7, p0, p0, v9}, Lcom/twitter/android/hk;-><init>(Lcom/twitter/android/GalleryActivity;Lcom/twitter/android/MediaTweetActivity;Lcom/twitter/android/gt;)V

    iput-object v7, p0, Lcom/twitter/android/GalleryActivity;->H:Lcom/twitter/android/hk;

    iget-object v7, p0, Lcom/twitter/android/GalleryActivity;->H:Lcom/twitter/android/hk;

    invoke-virtual {v5, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    const/4 v7, 0x1

    invoke-virtual {v5, v7, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    if-eqz v1, :cond_d

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_2
    :goto_4
    if-nez p1, :cond_13

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    const-string/jumbo v1, "tagged_user_list"

    const/4 v2, 0x0

    invoke-virtual {v6, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    invoke-static {v0, v1}, Lcom/twitter/android/hd;->a(Lcom/twitter/android/hd;Z)Z

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    if-nez v0, :cond_12

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    :goto_5
    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    :goto_6
    if-eqz v4, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    if-nez v0, :cond_3

    new-instance v0, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    invoke-direct {v0}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;-><init>()V

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "association"

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v2, "gallery"

    iget-boolean v3, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v2, "page"

    invoke-virtual {v1, v2, v8}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v2, "section"

    const-string/jumbo v3, ""

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    const-string/jumbo v2, "component"

    const-string/jumbo v3, "gallery"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putCharSequence(Ljava/lang/String;Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v2, v3, v0}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    invoke-virtual {v0, v1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->setArguments(Landroid/os/Bundle;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->w:Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getLastCustomNonConfigurationInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/hb;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    invoke-virtual {v0, p0}, Lcom/twitter/android/hb;->a(Lcom/twitter/android/client/BaseFragmentActivity;)V

    :cond_4
    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->g()V

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->E:Z

    if-eqz v0, :cond_14

    if-nez p1, :cond_5

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->F:Z

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setVisibility(I)V

    :cond_5
    :goto_7
    return-void

    :cond_6
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v0, :cond_7

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    const-string/jumbo v0, "media"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    move-object v12, v2

    move v2, v1

    move-object v1, v12

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v0, "tw"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_9

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v0, "tw"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/provider/Tweet;

    const-string/jumbo v4, "uri"

    iget-wide v7, v0, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v9

    invoke-static {v7, v8, v9, v10}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v4, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "projection"

    sget-object v4, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    invoke-virtual {v2, v0, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    const-string/jumbo v0, "media"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "media"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    :cond_8
    move-object v12, v2

    move v2, v1

    move-object v1, v12

    goto/16 :goto_1

    :cond_9
    const-string/jumbo v0, "statusId"

    invoke-virtual {v6, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "statusId"

    const-wide/16 v7, 0x0

    invoke-virtual {v6, v2, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/twitter/android/GalleryActivity;->G:J

    const-string/jumbo v2, "uri"

    iget-wide v7, p0, Lcom/twitter/android/GalleryActivity;->G:J

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v9

    invoke-static {v7, v8, v9, v10}, Lcom/twitter/library/provider/w;->b(JJ)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v2, "projection"

    sget-object v4, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    move v2, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_a
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "uri"

    invoke-virtual {v6}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v2, "projection"

    const-string/jumbo v4, "prj"

    invoke-virtual {v6, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string/jumbo v2, "selection"

    const-string/jumbo v4, "sel"

    invoke-virtual {v6, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "selectionArgs"

    const-string/jumbo v4, "selArgs"

    invoke-virtual {v6, v4}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    const-string/jumbo v2, "orderBy"

    const-string/jumbo v4, "orderBy"

    invoke-virtual {v6, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "id"

    const-wide/high16 v7, -0x8000000000000000L

    invoke-virtual {v6, v2, v7, v8}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v7

    iput-wide v7, p0, Lcom/twitter/android/GalleryActivity;->j:J

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    move v2, v1

    move-object v1, v0

    goto/16 :goto_1

    :cond_b
    const/4 v0, 0x0

    move-object v4, v0

    goto/16 :goto_2

    :cond_c
    const-string/jumbo v9, "tw"

    invoke-virtual {v6, v9}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    const/16 v9, 0x8

    invoke-virtual {v0, v9}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    goto/16 :goto_3

    :cond_d
    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v1, :cond_e

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/gy;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V

    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->h()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(Z)V

    const v0, 0x7f0f01d0    # com.twitter.android.R.string.home_direct_messages

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(I)V

    goto/16 :goto_4

    :cond_e
    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-eqz v1, :cond_f

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/gy;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/library/provider/Tweet;)V

    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->h()V

    const v0, 0x7f0f04d7    # com.twitter.android.R.string.tweet_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(I)V

    goto/16 :goto_4

    :cond_f
    if-eqz v3, :cond_2

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v1, v3}, Lcom/twitter/android/gy;->b(Ljava/util/List;)V

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setVisibility(I)V

    const/4 v4, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_10

    const v0, 0x7f0f04d7    # com.twitter.android.R.string.tweet_title

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->setTitle(I)V

    goto/16 :goto_4

    :cond_10
    if-nez v2, :cond_11

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->onPageSelected(I)V

    :cond_11
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    goto/16 :goto_4

    :cond_12
    const/4 v0, 0x0

    goto/16 :goto_5

    :cond_13
    const-string/jumbo v0, "cv"

    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    const-string/jumbo v0, "result_intent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/hd;->b(Landroid/os/Bundle;)V

    goto/16 :goto_6

    :cond_14
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f0b0003    # com.twitter.android.R.color.black

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setBackgroundResource(I)V

    goto/16 :goto_7

    :cond_15
    move v0, v1

    goto/16 :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 12

    const-wide/high16 v10, -0x8000000000000000L

    const/4 v3, 0x0

    check-cast p1, Lcom/twitter/android/hl;

    invoke-virtual {p1}, Lcom/twitter/android/hl;->a()Ljava/util/List;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v0, v4}, Lcom/twitter/android/gy;->a(Ljava/util/List;)V

    iget v0, p0, Lcom/twitter/android/GalleryActivity;->q:I

    invoke-static {v0, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iget-wide v0, p0, Lcom/twitter/android/GalleryActivity;->j:J

    cmp-long v0, v0, v10

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    move v1, v3

    :goto_0
    if-ge v1, v5, :cond_5

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gz;

    iget-object v0, v0, Lcom/twitter/android/gz;->a:Lcom/twitter/library/provider/Tweet;

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->C:J

    iget-wide v8, p0, Lcom/twitter/android/GalleryActivity;->j:J

    cmp-long v0, v6, v8

    if-nez v0, :cond_1

    iput-wide v10, p0, Lcom/twitter/android/GalleryActivity;->j:J

    move v0, v1

    :goto_1
    move v2, v0

    :cond_0
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v2, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(IZ)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->onPageSelected(I)V

    invoke-direct {p0}, Lcom/twitter/android/GalleryActivity;->h()V

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_4

    iget v0, p0, Lcom/twitter/android/GalleryActivity;->q:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_4

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    invoke-static {v0}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/api/MediaEntity;)Lcom/twitter/library/util/m;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v6

    move v1, v3

    :goto_3
    if-ge v1, v6, :cond_0

    invoke-interface {v4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gz;

    iget-object v0, v0, Lcom/twitter/android/gz;->b:Lcom/twitter/library/util/m;

    invoke-virtual {v5, v0}, Lcom/twitter/library/util/m;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput v1, p0, Lcom/twitter/android/GalleryActivity;->q:I

    move v2, v1

    goto :goto_2

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    :cond_4
    if-eqz p2, :cond_0

    invoke-interface {p2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljs;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v0, p0, v1, v4, v5}, Ljs;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V

    iget-wide v4, p0, Lcom/twitter/android/GalleryActivity;->G:J

    invoke-virtual {v0, v4, v5}, Ljs;->a(J)Ljs;

    move-result-object v0

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Ljs;->c(I)Lcom/twitter/library/service/b;

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->c(Lcom/twitter/library/service/b;)Lcom/twitter/library/service/b;

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1, v3}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_2

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->y()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->h:Lcom/twitter/library/widget/TweetView;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    :cond_0
    return-void
.end method

.method public a(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->b(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_1
    :goto_1
    iput-boolean p1, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->b:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0}, Lcom/twitter/android/hd;->a()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(Landroid/view/View;)V

    goto :goto_1
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 9

    const v8, 0x7f09030e    # com.twitter.android.R.id.tweet_photo

    const v7, 0x7f090071    # com.twitter.android.R.id.delete

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v3}, Lcom/twitter/android/gy;->a()Lcom/twitter/android/hi;

    move-result-object v3

    if-eqz v3, :cond_7

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->f:Z

    if-nez v0, :cond_8

    invoke-static {}, Lcom/twitter/library/client/App;->r()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "android_tablet_media_pivot_1883"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_tablet_media_pivot_1883"

    new-array v4, v1, [Ljava/lang/String;

    const-string/jumbo v5, "photo_pivot"

    aput-object v5, v4, v2

    invoke-static {v0, v4}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v0, v1

    :goto_0
    const v4, 0x7f09030d    # com.twitter.android.R.id.photo_pivot

    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    invoke-virtual {v4, v0}, Lhn;->b(Z)Lhn;

    iget-object v0, v3, Lcom/twitter/android/hi;->f:Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    const v3, 0x7f090141    # com.twitter.android.R.id.save

    invoke-virtual {p1, v3}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    invoke-virtual {v3}, Lcom/twitter/android/hb;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move v3, v1

    :goto_2
    invoke-virtual {v4, v3}, Lhn;->c(Z)Lhn;

    iget-boolean v3, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v3, :cond_3

    invoke-virtual {p1, v8}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    invoke-virtual {p1, v7}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v2}, Lhn;->b(Z)Lhn;

    :goto_3
    return v1

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-nez v3, :cond_4

    invoke-virtual {p1, v7}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v3

    invoke-virtual {v3, v2}, Lhn;->b(Z)Lhn;

    :goto_4
    invoke-virtual {p1, v8}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v2

    invoke-virtual {v2, v0}, Lhn;->c(Z)Lhn;

    goto :goto_3

    :cond_4
    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-static {v3, v2, v2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Lcom/twitter/library/util/u;->a(Ljava/util/List;J)Z

    move-result v3

    const v4, 0x7f090329    # com.twitter.android.R.id.remove_tag

    invoke-virtual {p1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    invoke-virtual {v4, v3}, Lhn;->b(Z)Lhn;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->q:J

    iget-object v5, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-nez v3, :cond_6

    move v3, v1

    :goto_5
    invoke-virtual {p1, v7}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-boolean v3, v3, Lcom/twitter/library/provider/Tweet;->r:Z

    if-nez v3, :cond_5

    move v2, v1

    :cond_5
    invoke-virtual {v4, v2}, Lhn;->b(Z)Lhn;

    goto :goto_4

    :cond_6
    move v3, v2

    goto :goto_5

    :cond_7
    move v1, v0

    goto :goto_3

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/MediaTweetActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f11000e    # com.twitter.android.R.menu.gallery_menu

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f11001d    # com.twitter.android.R.menu.remove_tag

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f110008    # com.twitter.android.R.menu.delete

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090141    # com.twitter.android.R.id.save

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    invoke-virtual {v0}, Lcom/twitter/android/hb;->b()V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    invoke-virtual {v0, v2}, Lcom/twitter/android/hb;->cancel(Z)Z

    :cond_0
    new-instance v0, Lcom/twitter/android/hb;

    invoke-direct {v0, p0}, Lcom/twitter/android/hb;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    new-array v1, v2, [Lcom/twitter/android/hi;

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v3}, Lcom/twitter/android/gy;->a()Lcom/twitter/android/hi;

    move-result-object v3

    aput-object v3, v1, v5

    invoke-virtual {v0, v1}, Lcom/twitter/android/hb;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    move v0, v2

    :goto_0
    return v0

    :cond_1
    const v1, 0x7f09030e    # com.twitter.android.R.id.tweet_photo

    if-ne v0, v1, :cond_6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    iget-object v1, v0, Lcom/twitter/library/api/MediaEntity;->expandedUrl:Ljava/lang/String;

    :cond_2
    :goto_1
    if-eqz v1, :cond_7

    const/4 v0, 0x2

    new-array v0, v0, [I

    aput v5, v0, v2

    aput v5, v0, v5

    invoke-static {p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1, v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->p:Lcom/twitter/library/client/Session;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->b(Landroid/content/Context;)V

    move v0, v2

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v3

    if-eqz v0, :cond_5

    iget-object v1, v0, Lcom/twitter/library/api/TweetClassicCard;->url:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->urls:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/UrlEntity;

    iget-object v4, v0, Lcom/twitter/library/api/UrlEntity;->url:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    iget-object v0, v0, Lcom/twitter/library/api/UrlEntity;->expandedUrl:Ljava/lang/String;

    :goto_2
    move-object v1, v0

    goto :goto_1

    :cond_5
    if-eqz v3, :cond_2

    iget-object v1, v3, Lcom/twitter/library/api/MediaEntity;->expandedUrl:Ljava/lang/String;

    goto :goto_1

    :cond_6
    const v1, 0x7f090329    # com.twitter.android.R.id.remove_tag

    if-ne v0, v1, :cond_8

    invoke-static {v2}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f034c    # com.twitter.android.R.string.remove_self_media_tag_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0089    # com.twitter.android.R.string.cancel

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    :cond_7
    :goto_3
    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->a(Lhn;)Z

    move-result v0

    goto/16 :goto_0

    :cond_8
    const v1, 0x7f090071    # com.twitter.android.R.id.delete

    if-ne v0, v1, :cond_9

    invoke-virtual {p0, v2}, Lcom/twitter/android/GalleryActivity;->showDialog(I)V

    goto :goto_3

    :cond_9
    const v1, 0x7f09030d    # com.twitter.android.R.id.photo_pivot

    if-ne v0, v1, :cond_7

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-eqz v0, :cond_7

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/PhotoGridActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "user_id"

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->g()J

    move-result-wide v3

    invoke-virtual {v0, v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_name"

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v3}, Lcom/twitter/library/provider/Tweet;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_3

    :cond_a
    move-object v0, v1

    goto :goto_2
.end method

.method public b()Landroid/view/animation/Animation;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->b:Landroid/view/animation/Animation;

    return-object v0
.end method

.method public c()Landroid/content/Intent;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    return-object v0
.end method

.method public finish()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->setResult(ILandroid/content/Intent;)V

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->finish()V

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->E:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/GalleryActivity;->b(Z)V

    const/4 v0, 0x0

    const v1, 0x7f040010    # com.twitter.android.R.anim.quick_fade_out

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/GalleryActivity;->overridePendingTransition(II)V

    :cond_1
    return-void
.end method

.method protected j()V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->onBackPressed()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->j()V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    const-wide/16 v2, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const/16 v0, 0xa

    if-ne v0, p2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->finish()V

    goto :goto_0

    :pswitch_2
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    if-eqz p3, :cond_0

    const-string/jumbo v0, "user_id"

    invoke-virtual {p3, v0, v2, v3}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-lez v2, :cond_0

    const-string/jumbo v2, "friendship"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "friendship"

    const/4 v3, 0x0

    invoke-virtual {p3, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v3, v0, v1, v2}, Lcom/twitter/android/hd;->a(JI)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0}, Lcom/twitter/android/hd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/hd;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->onBackPressed()V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v4, 0x3

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->s:Lcom/twitter/library/scribe/ScribeAssociation;

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/TweetActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "tw"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "association"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/twitter/android/GalleryActivity;->startActivityForResult(Landroid/content/Intent;I)V

    invoke-direct {p0, v4, v0}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/library/provider/Tweet;)V

    return-void
.end method

.method public onCreateDialog(I)Landroid/app/Dialog;
    .locals 4

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-object v1

    :pswitch_0
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    if-nez v0, :cond_0

    const-string/jumbo v0, "ANATOMY-371: attempting to delete a null DM photo"

    :goto_1
    new-instance v2, Ljava/lang/IllegalStateException;

    invoke-direct {v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    move-object v0, v1

    :goto_2
    move-object v1, v0

    goto :goto_0

    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "ANATOMY-371: attempting to delete DM photo #"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    iget-wide v2, v2, Lcom/twitter/library/api/MediaEntity;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "ANATOMY-371: attempting to delete a photo but mTweet is null"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-static {p0, v0, v1, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Lcom/twitter/android/MediaTweetActivity;JI)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_2

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->v:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "ANATOMY-371: attempting to save a null DM photo"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->e:Lcom/twitter/library/api/MediaEntity;

    iget-wide v0, v0, Lcom/twitter/library/api/MediaEntity;->id:J

    invoke-static {p0, v0, v1, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Lcom/twitter/android/MediaTweetActivity;JI)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_5

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v2, "ANATOMY-371: attempting to save a photo but mTweet is null"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-static {p0, v0, v1, p1}, Lcom/twitter/android/MediaTweetActivity$MediaActionBarFragment;->a(Lcom/twitter/android/MediaTweetActivity;JI)Landroid/app/Dialog;

    move-result-object v1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const-string/jumbo v0, "uri"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;

    const-string/jumbo v0, "projection"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    const-string/jumbo v0, "selection"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string/jumbo v0, "selectionArgs"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const-string/jumbo v0, "orderBy"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    new-instance v0, Lcom/twitter/android/hl;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/hl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method protected onDestroy()V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    invoke-super {p0}, Lcom/twitter/android/MediaTweetActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->H:Lcom/twitter/android/hk;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->n:Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0}, Lcom/twitter/android/hd;->b()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/GalleryActivity;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/gy;->a(Ljava/util/List;)V

    return-void
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 0

    return-void
.end method

.method public onPageSelected(I)V
    .locals 7

    const/4 v2, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->c:Lcom/twitter/android/gy;

    invoke-virtual {v0}, Lcom/twitter/android/gy;->getCount()I

    move-result v3

    if-lez v3, :cond_1

    if-le v3, v1, :cond_2

    const v4, 0x7f0f0304    # com.twitter.android.R.string.photo_pager_title_format

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    add-int/lit8 v6, p1, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    invoke-virtual {p0, v4, v5}, Lcom/twitter/android/GalleryActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/twitter/android/GalleryActivity;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    invoke-virtual {v0, p1}, Lcom/twitter/android/gy;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    if-eqz v3, :cond_1

    iget v4, p0, Lcom/twitter/android/GalleryActivity;->q:I

    iget-boolean v5, p0, Lcom/twitter/android/GalleryActivity;->C:Z

    if-eq p1, v4, :cond_3

    move v0, v1

    :goto_1
    or-int/2addr v0, v5

    iput-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->C:Z

    invoke-direct {p0, v3}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/library/provider/Tweet;)V

    const/4 v0, -0x1

    if-eq v4, v0, :cond_1

    add-int/lit8 v0, v4, 0x1

    if-ne p1, v0, :cond_4

    invoke-direct {p0, v2, v3}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/library/provider/Tweet;)V

    :cond_0
    :goto_2
    if-eq v4, p1, :cond_1

    iget-object v0, v3, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/GalleryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_1
    iput p1, p0, Lcom/twitter/android/GalleryActivity;->q:I

    return-void

    :cond_2
    const v3, 0x7f0f04d7    # com.twitter.android.R.string.tweet_title

    invoke-virtual {p0, v3}, Lcom/twitter/android/GalleryActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/twitter/android/GalleryActivity;->setTitle(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_1

    :cond_4
    add-int/lit8 v0, v4, -0x1

    if-ne p1, v0, :cond_0

    invoke-direct {p0, v1, v3}, Lcom/twitter/android/GalleryActivity;->a(ILcom/twitter/library/provider/Tweet;)V

    goto :goto_2
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->onPostCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->B:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getPanelState()I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->B:Lcom/twitter/library/widget/SlidingPanel;

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/gu;

    invoke-direct {v1, p0}, Lcom/twitter/android/gu;-><init>(Lcom/twitter/android/GalleryActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(I)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0}, Lcom/twitter/android/hd;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/hd;->a(Z)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/twitter/android/GalleryActivity;->a(I)V

    goto :goto_1
.end method

.method public onRetainCustomNonConfigurationInstance()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    invoke-virtual {v0}, Lcom/twitter/android/hb;->b()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->u:Lcom/twitter/android/hb;

    return-object v0
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/MediaTweetActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "result_intent"

    iget-object v1, p0, Lcom/twitter/android/GalleryActivity;->g:Landroid/content/Intent;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v0, "cv"

    iget-boolean v1, p0, Lcom/twitter/android/GalleryActivity;->k:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/GalleryActivity;->A:Lcom/twitter/android/hd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/hd;->a(Landroid/os/Bundle;)V

    return-void
.end method
