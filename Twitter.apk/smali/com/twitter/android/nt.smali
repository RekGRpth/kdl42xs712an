.class public Lcom/twitter/android/nt;
.super Lcom/twitter/android/h;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLongClickListener;


# instance fields
.field private final w:[I

.field private final x:[I

.field private final y:[I

.field private final z:Lcom/twitter/android/vs;


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/android/vs;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/o;)V
    .locals 10

    invoke-virtual {p3}, Lcom/twitter/android/client/c;->aa()Z

    move-result v3

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    move-object v5, p4

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p9

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/twitter/android/h;-><init>(Landroid/content/Context;IZLcom/twitter/android/client/c;Lcom/twitter/library/widget/ap;Lcom/twitter/library/widget/aa;Lcom/twitter/library/view/c;Lcom/twitter/android/o;Lcom/twitter/library/util/FriendshipCache;)V

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/twitter/android/nt;->w:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/twitter/android/nt;->x:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    iput-object v0, p0, Lcom/twitter/android/nt;->y:[I

    iput-object p5, p0, Lcom/twitter/android/nt;->z:Lcom/twitter/android/vs;

    return-void

    nop

    :array_0
    .array-data 4
        0x0
        0x7f0f01a5    # com.twitter.android.R.string.followed_one
        0x7f0f01a8    # com.twitter.android.R.string.followed_two
        0x7f0f01a6    # com.twitter.android.R.string.followed_other
        0x7f0f01a7    # com.twitter.android.R.string.followed_people
        0x7f0f01a7    # com.twitter.android.R.string.followed_people
    .end array-data

    :array_1
    .array-data 4
        0x0
        0x7f0f0179    # com.twitter.android.R.string.favorited_tweet_one
        0x7f0f017b    # com.twitter.android.R.string.favorited_tweet_two
        0x7f0f017a    # com.twitter.android.R.string.favorited_tweet_other
        0x7f0f017d    # com.twitter.android.R.string.favorited_tweets_two_short
        0x7f0f017c    # com.twitter.android.R.string.favorited_tweets_short
    .end array-data

    :array_2
    .array-data 4
        0x0
        0x7f0f0373    # com.twitter.android.R.string.retweeted_tweet_one
        0x7f0f0375    # com.twitter.android.R.string.retweeted_tweet_two
        0x7f0f0374    # com.twitter.android.R.string.retweeted_tweet_other
        0x7f0f0377    # com.twitter.android.R.string.retweeted_tweets_two_short
        0x7f0f0376    # com.twitter.android.R.string.retweeted_tweets_short
    .end array-data
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/view/View;Landroid/database/Cursor;)Landroid/content/Intent;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/content/Context;IZLandroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)Landroid/view/View;
    .locals 7

    const/4 v3, 0x0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    move v1, v3

    :goto_0
    if-ge v1, p4, :cond_0

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/nt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v4}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    iget-object v0, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/TweetView;->setShowSocialBadge(Z)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v4, p6}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v4, p7}, Landroid/view/View;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v5, p0, Lcom/twitter/android/nt;->u:[I

    iget-object v6, p0, Lcom/twitter/android/nt;->g:Ljava/util/ArrayList;

    move-object v0, p1

    move-object v1, p3

    move v3, p5

    move-object v4, p6

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/n;->a(Landroid/view/LayoutInflater;Landroid/content/Context;Ljava/util/ArrayList;ZLandroid/view/View$OnClickListener;[ILjava/util/ArrayList;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/content/res/Resources;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;)V
    .locals 5

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/q;

    const v1, 0x7f0c000a    # com.twitter.android.R.dimen.activity_header_margin_top

    invoke-virtual {p2, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v2, 0x7f0c0080    # com.twitter.android.R.dimen.list_row_padding

    invoke-virtual {p2, v2}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v2

    invoke-virtual {p1, v2, v1, v4, v1}, Landroid/view/View;->setPadding(IIII)V

    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    iget-object v1, p4, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    if-nez v1, :cond_1

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v3, v3}, Lcom/twitter/android/nt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    const v1, 0x7f0f020b    # com.twitter.android.R.string.list_created

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v3, v2, v4

    const/4 v3, 0x1

    iget-object v4, p4, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {p2, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/nt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/nt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;Landroid/content/res/Resources;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;I)V
    .locals 7

    const/4 v2, 0x0

    const/4 v6, 0x0

    const/4 v5, 0x2

    const/4 v4, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/n;

    iget-object v1, v0, Lcom/twitter/android/n;->a:Landroid/view/View;

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/twitter/android/n;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/q;

    if-eqz p3, :cond_0

    iget-object v1, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-eqz p4, :cond_0

    iget-object v1, p4, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    if-eqz v1, :cond_0

    if-nez p5, :cond_2

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    invoke-virtual {p0, v0, v2, v2}, Lcom/twitter/android/nt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-ne p5, v4, :cond_3

    iget-object v0, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    const v1, 0x7f0f020d    # com.twitter.android.R.string.listed_one

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p4, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p2, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/nt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/nt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :cond_3
    if-ne p5, v5, :cond_4

    iget-object v0, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    const v1, 0x7f0f020f    # com.twitter.android.R.string.listed_two_short

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v3, v2, v6

    iget-object v3, p4, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p2, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/nt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/nt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0

    :cond_4
    iget-object v0, v0, Lcom/twitter/android/q;->e:Landroid/widget/TextView;

    const v1, 0x7f0f020e    # com.twitter.android.R.string.listed_other

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    iget-object v3, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    aput-object v3, v2, v6

    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v4

    iget-object v3, p4, Lcom/twitter/library/provider/ActivityDataList;->name:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {p2, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/nt;->s:[Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/nt;->a(Landroid/widget/TextView;Ljava/lang/String;[Lcom/twitter/internal/android/widget/TypefacesSpan;)V

    goto :goto_0
.end method

.method public a(Landroid/view/View;Ljava/util/ArrayList;IIJLcom/twitter/android/vs;ILandroid/content/res/Resources;I)V
    .locals 20

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    move-object/from16 v17, v4

    check-cast v17, Lcom/twitter/android/n;

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v4

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v19

    const/4 v4, 0x0

    move/from16 v18, v4

    :goto_0
    move/from16 v0, v18

    move/from16 v1, v19

    if-ge v0, v1, :cond_0

    move-object/from16 v0, p2

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/twitter/library/provider/Tweet;

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/n;->b:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    const/4 v11, -0x1

    move-object/from16 v4, p0

    move-wide/from16 v7, p5

    move/from16 v9, p8

    move/from16 v10, p10

    invoke-virtual/range {v4 .. v11}, Lcom/twitter/android/nt;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;JIII)V

    const/4 v9, 0x0

    const/4 v10, 0x0

    const-wide/16 v15, -0x1

    move-object/from16 v7, p0

    move-object v8, v6

    move-wide/from16 v11, p5

    move/from16 v13, p8

    move/from16 v14, p10

    invoke-virtual/range {v7 .. v16}, Lcom/twitter/android/nt;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;JIIJ)V

    invoke-virtual {v5}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/j;

    iget-wide v5, v6, Lcom/twitter/library/provider/Tweet;->u:J

    move-object/from16 v0, p7

    invoke-virtual {v0, v4, v5, v6}, Lcom/twitter/android/vs;->b(Lcom/twitter/android/yd;J)V

    add-int/lit8 v4, v18, 0x1

    move/from16 v18, v4

    goto :goto_0

    :cond_0
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/n;->a:Landroid/view/View;

    if-eqz v4, :cond_1

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/n;->a:Landroid/view/View;

    move/from16 v0, p4

    move-wide/from16 v1, p5

    move/from16 v3, p8

    invoke-static {v4, v0, v1, v2, v3}, Lcom/twitter/android/q;->a(Landroid/view/View;IJI)V

    :cond_1
    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/n;->c:Landroid/view/View;

    if-eqz v4, :cond_2

    move-object/from16 v0, v17

    iget-object v4, v0, Lcom/twitter/android/n;->c:Landroid/view/View;

    move/from16 v5, p3

    move-wide/from16 v6, p5

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-static/range {v4 .. v9}, Lcom/twitter/android/k;->a(Landroid/view/View;IJILandroid/content/res/Resources;)V

    :cond_2
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 21

    move-object/from16 v2, p3

    check-cast v2, Lcom/twitter/android/ActivityCursor;

    const/4 v3, 0x1

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const/4 v3, 0x0

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    packed-switch v8, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual/range {p0 .. p3}, Lcom/twitter/android/nt;->a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    goto :goto_0

    :pswitch_2
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/nt;->w:[I

    const/4 v10, 0x0

    move-object/from16 v0, p0

    iget-wide v11, v0, Lcom/twitter/android/nt;->q:J

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v5, p3

    invoke-virtual/range {v2 .. v12}, Lcom/twitter/android/nt;->a(Landroid/view/View;Landroid/content/res/Resources;Landroid/database/Cursor;JI[IZJ)V

    goto :goto_0

    :pswitch_3
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/nt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/nt;->a(Landroid/database/Cursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v5

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/nt;->a(Ljava/util/ArrayList;)I

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    move-object v3, v2

    :goto_1
    move-object/from16 v0, p0

    invoke-virtual {v0, v5}, Lcom/twitter/android/nt;->a(Ljava/util/ArrayList;)I

    move-result v2

    if-nez v2, :cond_1

    const/4 v5, 0x0

    :goto_2
    const v2, 0x7f020118    # com.twitter.android.R.drawable.ic_activity_list_default

    move-object/from16 v0, p1

    invoke-static {v0, v2, v6, v7, v8}, Lcom/twitter/android/q;->a(Landroid/view/View;IJI)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v4, v3, v5}, Lcom/twitter/android/nt;->a(Landroid/view/View;Landroid/content/res/Resources;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    const-wide/16 v10, -0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/nt;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;JIIJ)V

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    move-object v3, v2

    goto :goto_1

    :cond_1
    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/ActivityDataList;

    move-object v5, v2

    goto :goto_2

    :pswitch_4
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/nt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v3

    sget-object v5, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v5}, Lcom/twitter/android/nt;->a(Landroid/database/Cursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v5

    sget-object v9, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v9}, Lcom/twitter/android/nt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v11

    const/4 v2, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const/4 v2, 0x0

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/twitter/library/provider/ActivityDataList;

    const v13, 0x7f020118    # com.twitter.android.R.drawable.ic_activity_list_default

    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/twitter/android/nt;->q:J

    move-wide/from16 v18, v0

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-wide v14, v6

    move/from16 v16, v8

    move-object/from16 v17, v4

    invoke-virtual/range {v9 .. v19}, Lcom/twitter/android/nt;->a(Landroid/view/View;Ljava/util/ArrayList;IIJILandroid/content/res/Resources;J)V

    const/4 v2, 0x0

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/twitter/library/api/TwitterUser;

    move-object/from16 v13, p0

    move-object/from16 v14, p1

    move-object v15, v4

    move-object/from16 v17, v5

    move/from16 v18, v12

    invoke-virtual/range {v13 .. v18}, Lcom/twitter/android/nt;->a(Landroid/view/View;Landroid/content/res/Resources;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;I)V

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v9

    const-wide/16 v10, -0x1

    move-object/from16 v2, p0

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/nt;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;JIIJ)V

    goto/16 :goto_0

    :pswitch_5
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/nt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v20

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/nt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v11

    const/16 v2, 0x9

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const v13, 0x7f02011c    # com.twitter.android.R.drawable.ic_activity_rt_default

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nt;->z:Lcom/twitter/android/vs;

    move-object/from16 v16, v0

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v19

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-wide v14, v6

    move/from16 v17, v8

    move-object/from16 v18, v4

    invoke-virtual/range {v9 .. v19}, Lcom/twitter/android/nt;->a(Landroid/view/View;Ljava/util/ArrayList;IIJLcom/twitter/android/vs;ILandroid/content/res/Resources;I)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/nt;->y:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/nt;->y:[I

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v8

    const/4 v9, 0x2

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, v20

    move v10, v12

    invoke-virtual/range {v2 .. v10}, Lcom/twitter/android/nt;->a(Landroid/view/View;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V

    goto/16 :goto_0

    :pswitch_6
    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/nt;->a(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v20

    sget-object v3, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    move-object/from16 v0, p0

    invoke-virtual {v0, v2, v3}, Lcom/twitter/android/nt;->b(Lcom/twitter/android/ActivityCursor;Lcom/twitter/android/ActivityCursor$ObjectField;)Ljava/util/ArrayList;

    move-result-object v11

    if-eqz v20, :cond_2

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    const/16 v3, 0x10

    iput v3, v2, Lcom/twitter/library/provider/Tweet;->P:I

    const/4 v3, 0x0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterUser;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/provider/Tweet;->Q:Ljava/lang/String;

    goto :goto_3

    :cond_2
    const/4 v2, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    const v13, 0x7f020114    # com.twitter.android.R.drawable.ic_activity_fave_default

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nt;->z:Lcom/twitter/android/vs;

    move-object/from16 v16, v0

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v19

    move-object/from16 v9, p0

    move-object/from16 v10, p1

    move-wide v14, v6

    move/from16 v17, v8

    move-object/from16 v18, v4

    invoke-virtual/range {v9 .. v19}, Lcom/twitter/android/nt;->a(Landroid/view/View;Ljava/util/ArrayList;IIJLcom/twitter/android/vs;ILandroid/content/res/Resources;I)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/nt;->x:[I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/nt;->x:[I

    if-nez v20, :cond_3

    const/4 v8, 0x0

    :goto_4
    const/4 v9, 0x2

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, v20

    move v10, v12

    invoke-virtual/range {v2 .. v10}, Lcom/twitter/android/nt;->a(Landroid/view/View;Landroid/content/res/Resources;[I[ILjava/util/ArrayList;III)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->size()I

    move-result v8

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public e()I
    .locals 1

    const/16 v0, 0x8

    return v0
.end method

.method public getItemViewType(I)I
    .locals 7

    const/4 v4, 0x2

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/h;->getItemViewType(I)I

    move-result v2

    const/4 v0, -0x1

    if-eq v2, v0, :cond_0

    move v0, v2

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/nt;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ActivityCursor;

    invoke-virtual {v0, v3}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_1
    :pswitch_0
    move v0, v2

    goto :goto_0

    :pswitch_1
    if-ne v1, v6, :cond_2

    const/16 v1, 0x9

    :goto_1
    invoke-virtual {v0, v1}, Lcom/twitter/android/ActivityCursor;->getInt(I)I

    move-result v0

    if-lez v0, :cond_1

    invoke-static {v0}, Lcom/twitter/android/nt;->a(I)I

    move-result v1

    if-ne v1, v3, :cond_3

    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    const/4 v1, 0x6

    goto :goto_1

    :cond_3
    if-ne v1, v4, :cond_4

    move v0, v3

    goto :goto_0

    :cond_4
    if-ne v1, v5, :cond_5

    if-ne v0, v5, :cond_5

    move v0, v4

    goto :goto_0

    :cond_5
    if-ne v1, v6, :cond_6

    move v0, v5

    goto :goto_0

    :cond_6
    move v0, v6

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v5, 0x6

    const/4 v0, 0x1

    const/4 v2, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v3, 0x0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :pswitch_0
    move-object v0, v3

    :goto_0
    return-object v0

    :pswitch_1
    invoke-virtual {p0, p1, p2, p3, v1}, Lcom/twitter/android/nt;->a(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;Landroid/view/LayoutInflater;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/twitter/android/nt;->a(I)I

    move-result v3

    if-le v4, v3, :cond_0

    move v4, v0

    :goto_1
    move-object v0, p0

    move-object v2, p1

    move-object v5, p0

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/nt;->a(Landroid/view/LayoutInflater;Landroid/content/Context;IZLandroid/view/View$OnClickListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_0
    move v4, v2

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/twitter/android/nt;->u:[I

    aget v0, v0, v2

    invoke-static {v1, p0, v0}, Lcom/twitter/android/q;->a(Landroid/view/LayoutInflater;Landroid/view/View$OnClickListener;I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-interface {p2, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/twitter/android/nt;->a(I)I

    move-result v4

    if-le v3, v4, :cond_1

    move v5, v0

    :goto_2
    iget-object v6, p0, Lcom/twitter/android/nt;->j:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v2, p3

    move-object v3, p1

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/nt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/content/Context;IZLandroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_1
    move v5, v2

    goto :goto_2

    :pswitch_5
    const/16 v3, 0x9

    invoke-interface {p2, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/twitter/android/nt;->a(I)I

    move-result v4

    if-le v3, v4, :cond_2

    move v5, v0

    :goto_3
    iget-object v6, p0, Lcom/twitter/android/nt;->j:Landroid/view/View$OnClickListener;

    move-object v0, p0

    move-object v2, p3

    move-object v3, p1

    move-object v7, p0

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/nt;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/content/Context;IZLandroid/view/View$OnClickListener;Landroid/view/View$OnLongClickListener;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v5, v2

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_5
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public onLongClick(Landroid/view/View;)Z
    .locals 4

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/android/j;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/j;

    iget-object v1, p0, Lcom/twitter/android/nt;->z:Lcom/twitter/android/vs;

    iget-object v2, v0, Lcom/twitter/android/j;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v2}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->u:J

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/vs;->a(Lcom/twitter/android/yd;J)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
