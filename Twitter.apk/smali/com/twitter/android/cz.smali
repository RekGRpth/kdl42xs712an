.class public Lcom/twitter/android/cz;
.super Lcom/twitter/android/iq;
.source "Twttr"


# instance fields
.field private b:Lcom/twitter/library/widget/i;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/twitter/library/widget/i;)V
    .locals 7

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    invoke-virtual {v0}, Lcom/twitter/android/FollowFlowController;->g()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move v3, v2

    move-object v4, p2

    move v6, v2

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/iq;-><init>(Landroid/content/Context;IILjava/util/ArrayList;Ljava/lang/String;Z)V

    iput v2, p0, Lcom/twitter/android/cz;->a:I

    iput-object p3, p0, Lcom/twitter/android/cz;->b:Lcom/twitter/library/widget/i;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/library/api/TwitterContact;
    .locals 1

    iget v0, p0, Lcom/twitter/android/cz;->a:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/twitter/android/cz;->b(I)Lcom/twitter/library/api/TwitterContact;

    move-result-object v0

    return-object v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/iq;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iget v0, p0, Lcom/twitter/android/cz;->a:I

    if-lt p1, v0, :cond_0

    const v0, 0x7f090034    # com.twitter.android.R.id.check_item

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    move-object v0, v1

    check-cast v0, Lcom/twitter/library/widget/InviteView;

    iget-object v2, p0, Lcom/twitter/android/cz;->b:Lcom/twitter/library/widget/i;

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/InviteView;->setOnViewClickListener(Lcom/twitter/library/widget/i;)V

    :cond_0
    return-object v1
.end method
