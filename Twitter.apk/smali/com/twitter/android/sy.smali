.class Lcom/twitter/android/sy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/SecuritySettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/SecuritySettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->n(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/SecuritySettingsActivity;->m(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "settings:login_verification:unenroll:ok:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    iget-object v1, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    const v2, 0x7f0f0256    # com.twitter.android.R.string.login_verification_unenrolling

    invoke-virtual {v1, v2}, Lcom/twitter/android/SecuritySettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/SecuritySettingsActivity;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/SecuritySettingsActivity;->p(Lcom/twitter/android/SecuritySettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-virtual {v1}, Lcom/twitter/android/SecuritySettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/SecuritySettingsActivity;->c(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/sy;->a:Lcom/twitter/android/SecuritySettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/SecuritySettingsActivity;->o(Lcom/twitter/android/SecuritySettingsActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    return-void
.end method
