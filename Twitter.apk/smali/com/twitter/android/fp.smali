.class Lcom/twitter/android/fp;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/EventSearchActivity;

.field private b:Landroid/graphics/Bitmap;

.field private c:Landroid/content/res/Resources;


# direct methods
.method public constructor <init>(Lcom/twitter/android/EventSearchActivity;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/android/EventSearchActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/fp;->c:Landroid/content/res/Resources;

    return-void
.end method


# virtual methods
.method protected a([Landroid/graphics/drawable/BitmapDrawable;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    if-eqz p1, :cond_3

    :try_start_0
    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    const/4 v3, 0x0

    aget-object v3, p1, v3

    invoke-virtual {v3}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Landroid/graphics/Bitmap;Z)V

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    new-instance v3, Lcom/twitter/android/widget/cl;

    invoke-direct {v3, p1}, Lcom/twitter/android/widget/cl;-><init>([Landroid/graphics/drawable/BitmapDrawable;)V

    invoke-static {v2, v3}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Lcom/twitter/android/widget/cl;)Lcom/twitter/android/widget/cl;

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v2}, Lcom/twitter/android/EventSearchActivity;->h(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/cl;

    move-result-object v2

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/cl;->a(I)V

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v2}, Lcom/twitter/android/EventSearchActivity;->i(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/TopicView;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v3}, Lcom/twitter/android/EventSearchActivity;->h(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/cl;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/TopicView;->setCustomTopicDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/twitter/android/EventSearchActivity;->b(Lcom/twitter/android/EventSearchActivity;I)I
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    move v1, v0

    :cond_0
    if-eqz v1, :cond_1

    :try_start_1
    iget-object v0, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    iget-object v1, p0, Lcom/twitter/android/fp;->b:Landroid/graphics/Bitmap;

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Landroid/graphics/Bitmap;Z)V
    :try_end_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Lcom/twitter/android/fp;)Lcom/twitter/android/fp;

    return-void

    :catch_0
    move-exception v2

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v2}, Lcom/twitter/android/EventSearchActivity;->h(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/cl;

    move-result-object v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v2}, Lcom/twitter/android/EventSearchActivity;->h(Lcom/twitter/android/EventSearchActivity;)Lcom/twitter/android/widget/cl;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/twitter/android/widget/cl;->a([Landroid/graphics/drawable/BitmapDrawable;)V

    iget-object v2, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v2, v5}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Lcom/twitter/android/widget/cl;)Lcom/twitter/android/widget/cl;

    :cond_2
    :goto_2
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    if-ge v0, v2, :cond_0

    aget-object v2, p1, v0

    if-eqz v2, :cond_0

    aget-object v2, p1, v0

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->recycle()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :catch_1
    move-exception v0

    iget-object v0, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0, v5}, Lcom/twitter/android/EventSearchActivity;->a(Lcom/twitter/android/EventSearchActivity;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method protected varargs a([Landroid/graphics/Bitmap;)[Landroid/graphics/drawable/BitmapDrawable;
    .locals 9

    const/high16 v5, 0x40a00000    # 5.0f

    const/4 v1, 0x0

    aget-object v0, p1, v1

    iput-object v0, p0, Lcom/twitter/android/fp;->b:Landroid/graphics/Bitmap;

    const/4 v2, 0x5

    new-array v2, v2, [Landroid/graphics/drawable/BitmapDrawable;

    const/4 v3, 0x4

    :try_start_0
    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v6, p0, Lcom/twitter/android/fp;->c:Landroid/content/res/Resources;

    invoke-direct {v4, v6, v0}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v3}, Lcom/twitter/android/EventSearchActivity;->g(Lcom/twitter/android/EventSearchActivity;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    div-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    div-int/lit8 v4, v4, 0x2

    const/4 v6, 0x1

    invoke-static {v0, v3, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :cond_0
    const/4 v3, 0x3

    move v4, v5

    :goto_0
    if-ltz v3, :cond_1

    new-instance v6, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v7, p0, Lcom/twitter/android/fp;->c:Landroid/content/res/Resources;

    iget-object v8, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v8, v0, v4}, Lcom/twitter/android/util/ae;->a(Landroid/content/Context;Landroid/graphics/Bitmap;F)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-direct {v6, v7, v8}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v6, v2, v3

    add-int/lit8 v3, v3, -0x1

    add-float/2addr v4, v5

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/fp;->a:Lcom/twitter/android/EventSearchActivity;

    invoke-static {v0}, Lcom/twitter/android/EventSearchActivity;->g(Lcom/twitter/android/EventSearchActivity;)Z

    move-result v0

    if-nez v0, :cond_3

    const/4 v0, 0x0

    aget-object v0, v2, v0

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    mul-int/lit8 v3, v3, 0x2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    mul-int/lit8 v4, v4, 0x2

    const/4 v5, 0x1

    invoke-static {v0, v3, v4, v5}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v3

    if-eq v3, v0, :cond_2

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_2
    const/4 v0, 0x0

    new-instance v4, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v5, p0, Lcom/twitter/android/fp;->c:Landroid/content/res/Resources;

    invoke-direct {v4, v5, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    aput-object v4, v2, v0
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_3
    move-object v0, v2

    :goto_1
    return-object v0

    :catch_0
    move-exception v0

    move v0, v1

    :goto_2
    array-length v1, v2

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_4

    aget-object v1, v2, v0

    if-nez v1, :cond_5

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    :cond_5
    aget-object v1, v2, v0

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/fp;->a([Landroid/graphics/Bitmap;)[Landroid/graphics/drawable/BitmapDrawable;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0, p1}, Lcom/twitter/android/fp;->a([Landroid/graphics/drawable/BitmapDrawable;)V

    return-void
.end method
