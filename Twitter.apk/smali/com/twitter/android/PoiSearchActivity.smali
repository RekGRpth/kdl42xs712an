.class public Lcom/twitter/android/PoiSearchActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Lcom/twitter/android/widget/PoiFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/library/client/e;)V
    .locals 4

    const v3, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/PoiSearchActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "search_text"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PoiSearchActivity;->setTitle(Ljava/lang/CharSequence;)V

    if-nez p1, :cond_0

    invoke-static {v2, v2, v0}, Lcom/twitter/android/widget/PoiFragment;->a(IZLjava/lang/String;)Lcom/twitter/android/widget/PoiFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PoiSearchActivity;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {p0}, Lcom/twitter/android/PoiSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/PoiSearchActivity;->a:Lcom/twitter/android/widget/PoiFragment;

    invoke-virtual {v0, v3, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PoiSearchActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PoiFragment;

    iput-object v0, p0, Lcom/twitter/android/PoiSearchActivity;->a:Lcom/twitter/android/widget/PoiFragment;

    goto :goto_0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/PoiSearchActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/PoiSearchActivity;->finish()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method
