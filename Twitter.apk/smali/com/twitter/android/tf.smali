.class Lcom/twitter/android/tf;
.super Landroid/widget/ArrayAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/SettingsDialogActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/SettingsDialogActivity;Landroid/content/Context;[Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/tf;->a:Lcom/twitter/android/SettingsDialogActivity;

    const/4 v0, 0x0

    invoke-direct {p0, p2, v0, p3}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    if-nez p2, :cond_0

    invoke-virtual {p3}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f030139    # com.twitter.android.R.layout.settings_row_view

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    :cond_0
    const v0, 0x7f09026a    # com.twitter.android.R.id.settings_item

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/TypefacesTextView;

    invoke-virtual {p0, p1}, Lcom/twitter/android/tf;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/TypefacesTextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method
