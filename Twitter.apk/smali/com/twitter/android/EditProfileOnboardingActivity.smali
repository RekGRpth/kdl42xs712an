.class public Lcom/twitter/android/EditProfileOnboardingActivity;
.super Lcom/twitter/android/BaseEditProfileActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;


# instance fields
.field private A:Landroid/widget/ImageView;

.field private B:Landroid/widget/ImageView;

.field private C:I

.field private E:Z

.field private F:Z

.field private G:Z

.field private u:Landroid/widget/TextView;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/FrameLayout;

.field private x:Landroid/widget/RelativeLayout;

.field private y:Lcom/twitter/android/widget/VariableHeightLayout;

.field private z:Lcom/twitter/internal/android/widget/ToolBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/BaseEditProfileActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EditProfileOnboardingActivity;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/EditProfileOnboardingActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    return p1
.end method

.method private a(Lcom/twitter/library/api/TwitterUser;)Z
    .locals 2

    iget-object v0, p1, Lcom/twitter/library/api/TwitterUser;->profileImagePath:Ljava/lang/String;

    const-string/jumbo v1, "/sticky/default_profile_images/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/EditProfileOnboardingActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->v:Landroid/view/View;

    return-object v0
.end method

.method private b(I)V
    .locals 5

    const v4, 0x7f02020b    # com.twitter.android.R.drawable.ic_profile_photo_add

    const v3, 0x106000d    # android.R.color.transparent

    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->p()V

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->w:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f020002    # com.twitter.android.R.drawable.add_profile_photo_overlay

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->y:Lcom/twitter/android/widget/VariableHeightLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02004e    # com.twitter.android.R.drawable.border_edit_profile_onboarding_header

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/VariableHeightLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->y:Lcom/twitter/android/widget/VariableHeightLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02003e    # com.twitter.android.R.drawable.bg_profile_edit

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/VariableHeightLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->B:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->x:Landroid/widget/RelativeLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic c(Lcom/twitter/android/EditProfileOnboardingActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/EditProfileOnboardingActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/EditProfileOnboardingActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->r()V

    return-void
.end method

.method private o()V
    .locals 7

    const/4 v6, 0x1

    const v0, 0x7f0f02da    # com.twitter.android.R.string.onboarding_tweet_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    const v1, 0x7f0f02d8    # com.twitter.android.R.string.onboarding_tweet_hashtag

    invoke-virtual {p0, v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v2}, Lcom/twitter/library/provider/Tweet;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    iget-wide v4, v3, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-wide v4, v2, Lcom/twitter/library/provider/Tweet;->n:J

    iget-object v4, v3, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v4, v2, Lcom/twitter/library/provider/Tweet;->p:Ljava/lang/String;

    iget-object v4, v3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    iput-object v4, v2, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    iput-object v4, v2, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iput-wide v4, v2, Lcom/twitter/library/provider/Tweet;->h:J

    iput-object v0, v2, Lcom/twitter/library/provider/Tweet;->d:Ljava/lang/String;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    iput-object v3, v2, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    new-instance v3, Lcom/twitter/library/api/HashtagEntity;

    invoke-direct {v3}, Lcom/twitter/library/api/HashtagEntity;-><init>()V

    invoke-virtual {v1, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/twitter/library/api/HashtagEntity;->text:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    iput v0, v3, Lcom/twitter/library/api/HashtagEntity;->start:I

    iget v0, v3, Lcom/twitter/library/api/HashtagEntity;->start:I

    iget-object v1, v3, Lcom/twitter/library/api/HashtagEntity;->text:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/2addr v0, v1

    add-int/lit8 v0, v0, 0x1

    iput v0, v3, Lcom/twitter/library/api/HashtagEntity;->end:I

    new-instance v0, Lcom/twitter/library/api/TweetEntities;

    invoke-direct {v0}, Lcom/twitter/library/api/TweetEntities;-><init>()V

    iput-object v0, v2, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v2, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, v0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    iget-object v0, v2, Lcom/twitter/library/provider/Tweet;->A:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "tw"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "holy_tweet_instructions"

    const v2, 0x7f0f02d9    # com.twitter.android.R.string.onboarding_tweet_instructions

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "mode"

    invoke-virtual {v0, v1, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->d:Landroid/net/Uri;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "media_uri"

    iget-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->d:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method private p()V
    .locals 5

    const v4, 0x7f0202b0    # com.twitter.android.R.drawable.icn_profile_photo_edit

    const v3, 0x7f0200d3    # com.twitter.android.R.drawable.edit_profile_icon

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->y:Lcom/twitter/android/widget/VariableHeightLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02004e    # com.twitter.android.R.drawable.border_edit_profile_onboarding_header

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/VariableHeightLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->B:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->A:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->B:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->w:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0200d4    # com.twitter.android.R.drawable.edit_profile_overlay

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->x:Landroid/widget/RelativeLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setVisibility(I)V

    return-void
.end method

.method private q()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-boolean v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->G:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:edit_profile::avatar:egg"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->k()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->i:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:edit_profile::avatar:prefill_changed"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    new-instance v2, Lcom/twitter/android/ay;

    invoke-direct {v2, p0}, Lcom/twitter/android/ay;-><init>(Lcom/twitter/android/BaseEditProfileActivity;)V

    new-array v3, v5, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/twitter/android/ay;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "welcome::::complete"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->e()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->o()V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->j()V

    goto :goto_0
.end method

.method private r()V
    .locals 2

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->i:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->m:Lcom/twitter/library/util/m;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->m:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->G:Z

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getId()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(Landroid/graphics/Bitmap;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030062    # com.twitter.android.R.layout.edit_profile_onboarding

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method protected a()Lcom/twitter/library/scribe/ScribeAssociation;
    .locals 2

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const-string/jumbo v1, "welcome"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    const-string/jumbo v1, "edit_profile"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 9

    const/4 v8, 0x0

    const/4 v5, 0x1

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/content/DialogInterface;II)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->p()V

    if-eq p3, v5, :cond_1

    if-nez p3, :cond_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    const-string/jumbo v4, "welcome:edit_profile::avatar:edit"

    aput-object v4, v3, v8

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v5, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->p()V

    if-eq p3, v5, :cond_2

    if-nez p3, :cond_0

    :cond_2
    iput-boolean v5, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->F:Z

    goto :goto_0

    :pswitch_3
    const/4 v2, -0x1

    if-ne p3, v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->d_()V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->a()Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v4

    const-string/jumbo v5, "add_avatar_dialog"

    const-string/jumbo v6, "add_photo"

    const-string/jumbo v7, "click"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    const/4 v2, -0x2

    if-ne p3, v2, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->q()V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->a()Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v4

    const-string/jumbo v5, "add_avatar_dialog"

    const-string/jumbo v6, "skip"

    const-string/jumbo v7, "click"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v8

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected a(Landroid/content/Intent;)V
    .locals 2

    const/4 v1, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->setResult(I)V

    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->o()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(I)V

    goto :goto_0
.end method

.method protected a(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method a(Landroid/graphics/Bitmap;I)V
    .locals 3

    const v0, 0x7f090148    # com.twitter.android.R.id.header_image

    if-ne p2, v0, :cond_0

    if-eqz p1, :cond_1

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->y:Lcom/twitter/android/widget/VariableHeightLayout;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f02004e    # com.twitter.android.R.drawable.border_edit_profile_onboarding_header

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/VariableHeightLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->n:Landroid/widget/ImageView;

    const v1, 0x106000d    # android.R.color.transparent

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 3

    new-instance v0, Lcom/twitter/android/fk;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/fk;-><init>(Lcom/twitter/android/EditProfileOnboardingActivity;Lcom/twitter/android/fi;)V

    const/4 v1, 0x1

    new-array v1, v1, [Landroid/net/Uri;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/fk;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 9

    const/4 v8, 0x0

    const/16 v7, 0x8

    const/4 v3, 0x1

    const/4 v6, 0x0

    const-string/jumbo v0, "android_edit_profile_nux_1267"

    new-array v1, v3, [Ljava/lang/String;

    const-string/jumbo v2, "edit_profile_prefill"

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v3, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->i:Z

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v1, 0x7f0900f1    # com.twitter.android.R.id.user_name

    invoke-virtual {p0, v1}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f090142    # com.twitter.android.R.id.avatar_container

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->w:Landroid/widget/FrameLayout;

    const v2, 0x7f09007c    # com.twitter.android.R.id.header_container

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/widget/VariableHeightLayout;

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->y:Lcom/twitter/android/widget/VariableHeightLayout;

    const v2, 0x7f090044    # com.twitter.android.R.id.header_content

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout;

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->x:Landroid/widget/RelativeLayout;

    const v2, 0x7f09014a    # com.twitter.android.R.id.avatar_icon

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->A:Landroid/widget/ImageView;

    const v2, 0x7f090149    # com.twitter.android.R.id.header_icon

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->B:Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    const v3, 0x7f03003a    # com.twitter.android.R.layout.composer_title

    invoke-virtual {v2, v3, v8, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v2, 0x7f0900f4    # com.twitter.android.R.id.count

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    const v2, 0x7f090037    # com.twitter.android.R.id.divider

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->v:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    new-instance v4, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v5, 0x5

    invoke-direct {v4, v5}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(I)V

    invoke-virtual {v2, v3, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    iget-object v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getCurrentTextColor()I

    move-result v2

    iput v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->C:I

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v2

    iget-object v3, v2, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "@"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, v2, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, v2, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->r:Ljava/lang/String;

    iget-object v0, v2, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->t:Ljava/lang/String;

    iget-object v0, v2, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->s:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->i:Z

    if-eqz v0, :cond_1

    invoke-direct {p0, v2}, Lcom/twitter/android/EditProfileOnboardingActivity;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Lcom/twitter/android/fj;

    invoke-direct {v0, p0, v8}, Lcom/twitter/android/fj;-><init>(Lcom/twitter/android/EditProfileOnboardingActivity;Lcom/twitter/android/fi;)V

    new-array v1, v6, [Landroid/net/Uri;

    invoke-virtual {v0, v1}, Lcom/twitter/android/fj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_1
    :goto_0
    if-nez p1, :cond_3

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->p:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->r:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    rsub-int v0, v0, 0xa0

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->p:Landroid/widget/EditText;

    new-instance v1, Lcom/twitter/android/fi;

    invoke-direct {v1, p0}, Lcom/twitter/android/fi;-><init>(Lcom/twitter/android/EditProfileOnboardingActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    const v0, 0x7f0f0135    # com.twitter.android.R.string.edit_profile

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/ToolBar;->b()V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-void

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->r()V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "avatar_added"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    const-string/jumbo v0, "header_added"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->F:Z

    goto :goto_1
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    const v0, 0x7f090328    # com.twitter.android.R.id.menu_finish

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    invoke-super {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    iput-object p2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->z:Lcom/twitter/internal/android/widget/ToolBar;

    const v0, 0x7f11001b    # com.twitter.android.R.menu.onboarding

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v0

    return v0
.end method

.method public a(Lhn;)Z
    .locals 3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090328    # com.twitter.android.R.id.menu_finish

    if-ne v1, v2, :cond_1

    iget-boolean v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    if-nez v1, :cond_0

    const-string/jumbo v1, "android_edit_profile_nux_1267"

    invoke-static {v1}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "edit_profile_dialog"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->n()V

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->q()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 5

    const v4, 0x7f090328    # com.twitter.android.R.id.menu_finish

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->p:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v0

    rsub-int v0, v0, 0xa0

    if-gez v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    const/high16 v2, -0x10000

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->z:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->z:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    invoke-virtual {v1, v3}, Lhn;->b(Z)Lhn;

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->v:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->u:Landroid/widget/TextView;

    iget v2, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->C:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->z:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->z:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v1, v4}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lhn;->b(Z)Lhn;

    iget-object v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->v:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method protected g()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->profileUrl:Ljava/lang/String;

    return-object v0
.end method

.method protected h()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-object v0, v0, Lcom/twitter/library/api/TwitterUser;->location:Ljava/lang/String;

    return-object v0
.end method

.method protected m()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->l()Z

    move-result v0

    return v0
.end method

.method protected n()V
    .locals 9

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->a()Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v5

    const-string/jumbo v6, "add_avatar_dialog"

    const-string/jumbo v7, ""

    const-string/jumbo v8, "impression"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/4 v0, 0x4

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f044f    # com.twitter.android.R.string.skip_avatar_confirmation

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0019    # com.twitter.android.R.string.add_photo

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f044e    # com.twitter.android.R.string.skip

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/EditProfileOnboardingActivity;->p()V

    return-void
.end method

.method public onClickHandler(Landroid/view/View;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->onClickHandler(Landroid/view/View;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09007c    # com.twitter.android.R.id.header_container

    if-ne v0, v1, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->b(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090142    # com.twitter.android.R.id.avatar_container

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/EditProfileOnboardingActivity;->b(I)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/BaseEditProfileActivity;->onResume()V

    const-string/jumbo v0, "android_edit_profile_nux_1267"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/BaseEditProfileActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "avatar_added"

    iget-boolean v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->E:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "header_added"

    iget-boolean v1, p0, Lcom/twitter/android/EditProfileOnboardingActivity;->F:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
