.class public Lcom/twitter/android/rs;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Z


# direct methods
.method public static a(ILcom/twitter/library/provider/Tweet;ZLandroid/support/v4/app/Fragment;Lcom/twitter/android/ru;Landroid/support/v4/app/FragmentActivity;)V
    .locals 13

    invoke-static/range {p5 .. p5}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-static/range {p5 .. p5}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    invoke-virtual {p1, v8, v9}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v2

    const-string/jumbo v1, "android_rt_action_1837"

    invoke-static {v1}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v1, "android_rt_action_1837"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "one_tap"

    aput-object v7, v5, v6

    invoke-static {v1, v5}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/twitter/android/rt;

    move-object v5, p1

    move-object/from16 v6, p4

    move v7, p0

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/rt;-><init>(ZLcom/twitter/android/client/c;Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;Lcom/twitter/android/ru;IJ)V

    move-object/from16 v0, p5

    invoke-static {v0, v1}, Lcom/twitter/android/rs;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Z

    move-result v2

    if-nez v2, :cond_0

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-interface {v1, v2, v3}, Landroid/content/DialogInterface$OnClickListener;->onClick(Landroid/content/DialogInterface;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    move v7, p0

    move-object v10, p1

    move v11, v2

    move v12, p2

    invoke-static/range {v7 .. v12}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(IJLcom/twitter/library/provider/ParcelableTweet;ZZ)Lcom/twitter/android/widget/RetweetDialogFragment;

    move-result-object v1

    if-eqz p3, :cond_2

    const/4 v2, 0x0

    move-object/from16 v0, p3

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/widget/RetweetDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    :cond_2
    if-eqz p4, :cond_3

    move-object/from16 v0, p4

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(Lcom/twitter/android/ru;)V

    :cond_3
    invoke-virtual/range {p5 .. p5}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/RetweetDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    goto :goto_0
.end method

.method private static a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    sget-boolean v2, Lcom/twitter/android/rs;->a:Z

    if-nez v2, :cond_1

    const-string/jumbo v2, "one_tap_retweet_prefs"

    invoke-virtual {p0, v2, v0}, Landroid/app/Activity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "has_shown_notification"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v3, 0x7f0f02ea    # com.twitter.android.R.string.one_tap_retweet_notification_title

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0f02e9    # com.twitter.android.R.string.one_tap_retweet_notification_message

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    const v3, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v3, p1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v2, "has_shown_notification"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    move v0, v1

    :cond_0
    sput-boolean v1, Lcom/twitter/android/rs;->a:Z

    :cond_1
    return v0
.end method
