.class Lcom/twitter/android/si;
.super Lcom/twitter/library/view/g;
.source "Twttr"


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/internal/android/widget/GroupedRowView;

.field final synthetic c:Lcom/twitter/android/widget/PipView;

.field final synthetic d:Lcom/twitter/android/sh;


# direct methods
.method constructor <init>(Lcom/twitter/android/sh;Landroid/view/ViewParent;IILcom/twitter/internal/android/widget/GroupedRowView;Lcom/twitter/android/widget/PipView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/si;->d:Lcom/twitter/android/sh;

    iput p4, p0, Lcom/twitter/android/si;->a:I

    iput-object p5, p0, Lcom/twitter/android/si;->b:Lcom/twitter/internal/android/widget/GroupedRowView;

    iput-object p6, p0, Lcom/twitter/android/si;->c:Lcom/twitter/android/widget/PipView;

    invoke-direct {p0, p2, p3}, Lcom/twitter/library/view/g;-><init>(Landroid/view/ViewParent;I)V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/si;->d:Lcom/twitter/android/sh;

    invoke-static {v0}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sh;)Lcom/twitter/android/ob;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v1, "position"

    iget v2, p0, Lcom/twitter/android/si;->a:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "page"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/si;->d:Lcom/twitter/android/sh;

    invoke-static {v1}, Lcom/twitter/android/sh;->a(Lcom/twitter/android/sh;)Lcom/twitter/android/ob;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/si;->b:Lcom/twitter/internal/android/widget/GroupedRowView;

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3, v0}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/si;->c:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    invoke-super {p0, p1}, Lcom/twitter/library/view/g;->onPageSelected(I)V

    return-void
.end method
