.class public Lcom/twitter/android/TabsAdapter;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/view/ViewPager$OnPageChangeListener;


# instance fields
.field protected final a:Lcom/twitter/android/client/BaseFragmentActivity;

.field protected final b:Landroid/widget/TabHost;

.field protected final c:Landroid/support/v4/view/ViewPager;

.field protected final d:Ljava/util/ArrayList;

.field protected e:I

.field private final f:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

.field private final g:Lcom/twitter/android/ua;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseFragmentActivity;Landroid/widget/TabHost;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/ViewPagerScrollBar;)V
    .locals 2

    invoke-virtual {p1}, Lcom/twitter/android/client/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/TabsAdapter;->e:I

    iput-object p1, p0, Lcom/twitter/android/TabsAdapter;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    iput-object p2, p0, Lcom/twitter/android/TabsAdapter;->b:Landroid/widget/TabHost;

    iput-object p3, p0, Lcom/twitter/android/TabsAdapter;->c:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, p0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    iput-object p4, p0, Lcom/twitter/android/TabsAdapter;->f:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    new-instance v0, Lcom/twitter/android/ua;

    iget-object v1, p0, Lcom/twitter/android/TabsAdapter;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-direct {v0, v1}, Lcom/twitter/android/ua;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    iput-object v0, p0, Lcom/twitter/android/TabsAdapter;->g:Lcom/twitter/android/ua;

    return-void
.end method


# virtual methods
.method public a(I)Lcom/twitter/android/ub;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ub;

    return-object v0
.end method

.method public a(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V
    .locals 2

    new-instance v0, Lcom/twitter/android/ub;

    invoke-virtual {p1}, Landroid/widget/TabHost$TabSpec;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p2, p3, v1}, Lcom/twitter/android/ub;-><init>(Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/TabsAdapter;->g:Lcom/twitter/android/ua;

    invoke-virtual {p1, v1}, Landroid/widget/TabHost$TabSpec;->setContent(Landroid/widget/TabHost$TabContentFactory;)Landroid/widget/TabHost$TabSpec;

    iget-object v1, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->b:Landroid/widget/TabHost;

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->addTab(Landroid/widget/TabHost$TabSpec;)V

    invoke-virtual {p0}, Lcom/twitter/android/TabsAdapter;->notifyDataSetChanged()V

    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    iget-object v1, p0, Lcom/twitter/android/TabsAdapter;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ub;

    iget-object v0, v0, Lcom/twitter/android/ub;->a:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ub;

    iget-object v0, v0, Lcom/twitter/android/ub;->b:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public getItemPosition(Ljava/lang/Object;)I
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ub;

    iget-object v2, p0, Lcom/twitter/android/TabsAdapter;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v2}, Lcom/twitter/android/client/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/ub;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, -0x2

    goto :goto_0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/FragmentPagerAdapter;->instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v1, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ub;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ub;->a(Landroid/support/v4/app/Fragment;)V

    return-object v0
.end method

.method public onPageScrollStateChanged(I)V
    .locals 0

    return-void
.end method

.method public onPageScrolled(IFI)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->c:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->f:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    neg-int v1, p3

    invoke-virtual {v0, p1, v1}, Lcom/twitter/internal/android/widget/ViewPagerScrollBar;->a(II)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->f:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    invoke-virtual {v0, p1, p3}, Lcom/twitter/internal/android/widget/ViewPagerScrollBar;->a(II)V

    goto :goto_0
.end method

.method public onPageSelected(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->b:Landroid/widget/TabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getTabWidget()Landroid/widget/TabWidget;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TabWidget;->getDescendantFocusability()I

    move-result v1

    const/high16 v2, 0x60000

    invoke-virtual {v0, v2}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    iget-object v2, p0, Lcom/twitter/android/TabsAdapter;->b:Landroid/widget/TabHost;

    invoke-virtual {v2}, Landroid/widget/TabHost;->getCurrentTab()I

    move-result v2

    if-eq p1, v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/TabsAdapter;->b:Landroid/widget/TabHost;

    invoke-virtual {v2, p1}, Landroid/widget/TabHost;->setCurrentTab(I)V

    :cond_0
    invoke-virtual {v0, v1}, Landroid/widget/TabWidget;->setDescendantFocusability(I)V

    iput p1, p0, Lcom/twitter/android/TabsAdapter;->e:I

    return-void
.end method

.method public restoreState(Landroid/os/Parcelable;Ljava/lang/ClassLoader;)V
    .locals 4

    check-cast p1, Lcom/twitter/android/TabsAdapter$TabSavedState;

    iget-object v2, p1, Lcom/twitter/android/TabsAdapter$TabSavedState;->a:[Ljava/lang/String;

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v2

    if-ge v1, v0, :cond_1

    aget-object v0, v2, v1

    iget-object v3, p0, Lcom/twitter/android/TabsAdapter;->a:Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v3}, Lcom/twitter/android/client/BaseFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ub;

    invoke-virtual {v0, v3}, Lcom/twitter/android/ub;->a(Landroid/support/v4/app/Fragment;)V

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method public saveState()Landroid/os/Parcelable;
    .locals 2

    new-instance v0, Lcom/twitter/android/TabsAdapter$TabSavedState;

    iget-object v1, p0, Lcom/twitter/android/TabsAdapter;->d:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/twitter/android/TabsAdapter$TabSavedState;-><init>(Ljava/util/ArrayList;)V

    return-object v0
.end method
