.class Lcom/twitter/android/el;
.super Ljava/lang/Thread;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# static fields
.field private static final a:Lcom/twitter/library/service/c;


# instance fields
.field private final b:J

.field private final c:Lcom/twitter/library/client/Session;

.field private final d:Lcom/twitter/library/service/b;

.field private final e:Lcom/twitter/library/client/w;

.field private f:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/em;

    invoke-direct {v0}, Lcom/twitter/android/em;-><init>()V

    sput-object v0, Lcom/twitter/android/el;->a:Lcom/twitter/library/service/c;

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;)V
    .locals 2

    const-wide/16 v0, 0x1388

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/twitter/android/el;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;J)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;J)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/el;->f:Z

    iput-object p2, p0, Lcom/twitter/android/el;->c:Lcom/twitter/library/client/Session;

    iput-wide p3, p0, Lcom/twitter/android/el;->b:J

    invoke-static {p1}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/el;->e:Lcom/twitter/library/client/w;

    new-instance v0, Lcom/twitter/library/api/conversations/ac;

    iget-object v1, p0, Lcom/twitter/android/el;->c:Lcom/twitter/library/client/Session;

    invoke-direct {v0, p1, v1}, Lcom/twitter/library/api/conversations/ac;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/el;->d:Lcom/twitter/library/service/b;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/el;->f:Z

    return-void
.end method

.method public run()V
    .locals 5

    :goto_0
    :try_start_0
    iget-boolean v0, p0, Lcom/twitter/android/el;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/el;->e:Lcom/twitter/library/client/w;

    iget-object v1, p0, Lcom/twitter/android/el;->d:Lcom/twitter/library/service/b;

    const/4 v2, 0x0

    const/4 v3, 0x0

    sget-object v4, Lcom/twitter/android/el;->a:Lcom/twitter/library/service/c;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z

    iget-wide v0, p0, Lcom/twitter/android/el;->b:J

    invoke-static {v0, v1}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    :cond_0
    return-void
.end method
