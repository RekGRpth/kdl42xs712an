.class final Lcom/twitter/android/ls;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MediaTweetActivity;

.field final synthetic b:J


# direct methods
.method constructor <init>(Lcom/twitter/android/MediaTweetActivity;J)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    iput-wide p2, p0, Lcom/twitter/android/ls;->b:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v0}, Lcom/twitter/android/MediaTweetActivity;->c(Lcom/twitter/android/MediaTweetActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-static {v1}, Lcom/twitter/android/MediaTweetActivity;->d(Lcom/twitter/android/MediaTweetActivity;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v3, p0, Lcom/twitter/android/ls;->b:J

    invoke-virtual {v0, v1, v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/twitter/android/MediaTweetActivity;->f(Lcom/twitter/android/MediaTweetActivity;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTweetActivity;->c()Landroid/content/Intent;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "deleted"

    iget-wide v2, p0, Lcom/twitter/android/ls;->b:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/MediaTweetActivity;->setResult(ILandroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ls;->a:Lcom/twitter/android/MediaTweetActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTweetActivity;->finish()V

    return-void
.end method
