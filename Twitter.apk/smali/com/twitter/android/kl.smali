.class Lcom/twitter/android/kl;
.super Lcom/twitter/library/client/z;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/z;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->o(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/twitter/android/MainActivity;->b(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->p(Lcom/twitter/android/MainActivity;)V

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/kr;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v4, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v4}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v4

    invoke-virtual {v0, v4}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/BaseListFragment;->f(J)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MainActivity;->f()V

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MainActivity;->d()V

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/MainActivity;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MainActivity;->V()V

    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/kl;->a:Lcom/twitter/android/MainActivity;

    iget-object v0, v0, Lcom/twitter/android/MainActivity;->i:Lcom/twitter/android/kd;

    invoke-virtual {v0, p1}, Lcom/twitter/android/kd;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    return-void
.end method
