.class public Lcom/twitter/android/ud;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/av;
.implements Lcom/twitter/internal/android/widget/v;


# static fields
.field public static final a:[Ljava/lang/String;


# instance fields
.field private final b:Landroid/support/v4/util/LruCache;

.field private final c:Lcom/twitter/library/util/aa;

.field private final d:I

.field private final e:I

.field private final f:Ljava/util/ArrayList;

.field private final g:Lcom/twitter/library/client/aa;

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "ref_id"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "entities"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/ud;->a:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ZLcom/twitter/library/util/aa;)V
    .locals 3

    const/4 v2, 0x2

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, v2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    new-instance v0, Landroid/support/v4/util/LruCache;

    const/16 v1, 0x32

    invoke-direct {v0, v1}, Landroid/support/v4/util/LruCache;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/ud;->b:Landroid/support/v4/util/LruCache;

    iput-object p3, p0, Lcom/twitter/android/ud;->c:Lcom/twitter/library/util/aa;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ud;->g:Lcom/twitter/library/client/aa;

    if-eqz p2, :cond_0

    const/16 v0, 0x15

    iput v0, p0, Lcom/twitter/android/ud;->d:I

    const/16 v0, 0x10

    iput v0, p0, Lcom/twitter/android/ud;->e:I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/twitter/android/ud;->d:I

    iput v2, p0, Lcom/twitter/android/ud;->e:I

    goto :goto_0
.end method

.method private a(Landroid/database/Cursor;)J
    .locals 2

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/android/ud;->d:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 11

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/ud;->g:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    iget-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v6, v0

    :goto_0
    if-ltz v6, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v6, -0x1

    move v6, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v9

    move v4, v5

    :goto_1
    if-ge v4, v9, :cond_0

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/uf;

    iget-boolean v3, v2, Lcom/twitter/android/uf;->c:Z

    if-eqz v3, :cond_2

    iput-boolean v5, v2, Lcom/twitter/android/uf;->c:Z

    iget-object v3, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/twitter/android/ud;->c:Lcom/twitter/library/util/aa;

    iget-object v10, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    invoke-virtual {v3, v7, v8, v10}, Lcom/twitter/library/util/aa;->b(JLcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/twitter/library/util/ae;->b()Z

    move-result v10

    if-eqz v10, :cond_2

    iget-object v3, v3, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_3
    return-void
.end method

.method private b(Landroid/database/Cursor;)Ljava/util/List;
    .locals 5

    const/4 v4, 0x0

    invoke-direct {p0, p1}, Lcom/twitter/android/ud;->a(Landroid/database/Cursor;)J

    move-result-wide v2

    iget-object v0, p0, Lcom/twitter/android/ud;->b:Landroid/support/v4/util/LruCache;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/SoftReference;

    const/4 v1, 0x0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Ljava/lang/ref/SoftReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    :goto_0
    if-nez v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ud;->e:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_1

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-static {v0, v4, v4}, Lcom/twitter/library/util/s;->a(Ljava/util/Collection;II)Ljava/util/List;

    move-result-object v0

    :cond_0
    :goto_1
    iget-object v1, p0, Lcom/twitter/android/ud;->b:Landroid/support/v4/util/LruCache;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    new-instance v3, Ljava/lang/ref/SoftReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/SoftReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-object v0

    :cond_1
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/twitter/internal/android/widget/HorizontalListView;Z)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/twitter/android/ud;->c(Z)V

    return-void
.end method

.method public a(Lcom/twitter/library/util/aa;Ljava/util/HashMap;)V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v5, v0

    :goto_0
    if-ltz v5, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    add-int/lit8 v0, v5, -0x1

    move v5, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v6

    const/4 v1, 0x0

    move v4, v1

    :goto_1
    if-ge v4, v6, :cond_0

    invoke-virtual {v0, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/uf;

    iget-object v3, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    if-eqz v3, :cond_2

    iget-object v3, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    invoke-virtual {p2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/util/ae;

    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/twitter/library/util/ae;->b()Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v3, v3, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v3, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    iput-object v1, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    :cond_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    goto :goto_1

    :cond_3
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 12

    const/4 v5, 0x0

    const/4 v11, 0x0

    check-cast p1, Landroid/view/ViewGroup;

    invoke-direct {p0, p3}, Lcom/twitter/android/ud;->b(Landroid/database/Cursor;)Ljava/util/List;

    move-result-object v6

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v7

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v0

    invoke-static {v0, v7}, Ljava/lang/Math;->min(II)I

    move-result v3

    move v4, v5

    :goto_0
    if-ge v4, v3, :cond_4

    invoke-interface {v6, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    invoke-virtual {p1, v4}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/uf;

    iget-object v0, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iget-object v8, v2, Lcom/twitter/android/uf;->b:Ljava/lang/String;

    iput-object v0, v2, Lcom/twitter/android/uf;->b:Ljava/lang/String;

    if-eqz v8, :cond_0

    invoke-virtual {v1}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v9

    if-eqz v9, :cond_0

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_1

    :cond_0
    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v8

    new-instance v9, Lcom/twitter/library/util/m;

    iget v10, v8, Landroid/view/ViewGroup$LayoutParams;->width:I

    iget v8, v8, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {v9, v0, v10, v8}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    iput-object v9, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    iget-boolean v0, p0, Lcom/twitter/android/ud;->h:Z

    iput-boolean v0, v2, Lcom/twitter/android/uf;->c:Z

    iget-boolean v0, p0, Lcom/twitter/android/ud;->h:Z

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ud;->c:Lcom/twitter/library/util/aa;

    iget-object v8, p0, Lcom/twitter/android/ud;->g:Lcom/twitter/library/client/aa;

    invoke-virtual {v8}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v8

    invoke-virtual {v8}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    iget-object v10, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v8, v9, v10}, Lcom/twitter/library/util/aa;->b(JLcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v8

    if-eqz v8, :cond_2

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iput-object v11, v2, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    :cond_1
    :goto_1
    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    :cond_2
    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_3
    invoke-virtual {v1, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_1

    :cond_4
    move v1, v3

    :goto_2
    if-ge v1, v7, :cond_5

    invoke-virtual {p1, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v11}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/uf;

    iput-object v11, v0, Lcom/twitter/android/uf;->a:Lcom/twitter/library/util/m;

    iput-object v11, v0, Lcom/twitter/android/uf;->b:Ljava/lang/String;

    iput-boolean v5, v0, Lcom/twitter/android/uf;->c:Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_5
    return-void
.end method

.method public c(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ud;->h:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ud;->h:Z

    iget-boolean v0, p0, Lcom/twitter/android/ud;->h:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ud;->a()V

    :cond_0
    return-void
.end method

.method public getItemId(I)J
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/twitter/android/ud;->a(Landroid/database/Cursor;)J

    move-result-wide v0

    return-wide v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 6

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f0300bf    # com.twitter.android.R.layout.media_thumb_group

    invoke-virtual {v0, v2, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-object v2, p0, Lcom/twitter/android/ud;->f:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    new-instance v4, Lcom/twitter/android/uf;

    const/4 v5, 0x0

    invoke-direct {v4, v5}, Lcom/twitter/android/uf;-><init>(Lcom/twitter/android/ue;)V

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    return-object v0
.end method
