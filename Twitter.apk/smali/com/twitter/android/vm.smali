.class Lcom/twitter/android/vm;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TrendsActivity;

.field private final b:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/twitter/android/TrendsActivity;Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/vm;->b:Landroid/content/Context;

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/TrendsActivity;Landroid/content/Context;Lcom/twitter/android/vj;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/vm;-><init>(Lcom/twitter/android/TrendsActivity;Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Lcom/twitter/library/api/UserSettings;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/android/TrendsActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v2}, Lcom/twitter/android/TrendsActivity;->g(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/TrendsActivity;->V()V

    iget-object v0, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    iget-object v1, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->h(Lcom/twitter/android/TrendsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/android/TrendsActivity;Lcom/twitter/library/api/UserSettings;)V

    iget-object v0, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v0}, Lcom/twitter/android/TrendsActivity;->a(Lcom/twitter/android/TrendsActivity;)Landroid/support/v4/view/ViewPager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-static {v1}, Lcom/twitter/android/TrendsActivity;->b(Lcom/twitter/android/TrendsActivity;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v1, p0, Lcom/twitter/android/vm;->a:Lcom/twitter/android/TrendsActivity;

    invoke-virtual {v1}, Lcom/twitter/android/TrendsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    instance-of v1, v0, Lcom/twitter/android/TrendsFragment;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/TrendsFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TrendsFragment;->G()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/vm;->b:Landroid/content/Context;

    const v1, 0x7f0f04c6    # com.twitter.android.R.string.trends_update_settings_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
