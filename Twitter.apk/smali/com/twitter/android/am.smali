.class Lcom/twitter/android/am;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/service/i;


# instance fields
.field final synthetic a:Lcom/twitter/android/AuthorizeAppActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/AuthorizeAppActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/am;->a:Lcom/twitter/android/AuthorizeAppActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILcom/twitter/library/network/OAuthToken;Ljava/lang/String;J)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/am;->a:Lcom/twitter/android/AuthorizeAppActivity;

    invoke-static {v0}, Lcom/twitter/android/AuthorizeAppActivity;->a(Lcom/twitter/android/AuthorizeAppActivity;)V

    sparse-switch p1, :sswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/am;->a:Lcom/twitter/android/AuthorizeAppActivity;

    const v1, 0x7f0f000c    # com.twitter.android.R.string.action_error

    invoke-virtual {v0, v1}, Lcom/twitter/android/AuthorizeAppActivity;->a(I)V

    :goto_0
    return-void

    :sswitch_0
    if-eqz p2, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "tk"

    iget-object v2, p2, Lcom/twitter/library/network/OAuthToken;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "ts"

    iget-object v2, p2, Lcom/twitter/library/network/OAuthToken;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "screen_name"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/am;->a:Lcom/twitter/android/AuthorizeAppActivity;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/AuthorizeAppActivity;->setResult(ILandroid/content/Intent;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/am;->a:Lcom/twitter/android/AuthorizeAppActivity;

    invoke-virtual {v0}, Lcom/twitter/android/AuthorizeAppActivity;->finish()V

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/am;->a:Lcom/twitter/android/AuthorizeAppActivity;

    const v1, 0x7f0f0042    # com.twitter.android.R.string.authorize_app_auth_error

    invoke-virtual {v0, v1}, Lcom/twitter/android/AuthorizeAppActivity;->a(I)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x191 -> :sswitch_1
        0x193 -> :sswitch_1
    .end sparse-switch
.end method
