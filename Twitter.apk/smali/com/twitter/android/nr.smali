.class Lcom/twitter/android/nr;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/NearbyFragment;

.field private b:D

.field private c:D

.field private d:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Lcom/twitter/android/NearbyFragment;Lcom/google/android/gms/maps/model/LatLngBounds;)V
    .locals 6

    const-wide/high16 v4, 0x4018000000000000L    # 6.0

    iput-object p1, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/nr;->d:Ljava/util/HashMap;

    iget-object v0, p2, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-object v2, p2, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->a:D

    sub-double/2addr v0, v2

    div-double/2addr v0, v4

    iput-wide v0, p0, Lcom/twitter/android/nr;->b:D

    iget-object v0, p2, Lcom/google/android/gms/maps/model/LatLngBounds;->a:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v0, v0, Lcom/google/android/gms/maps/model/LatLng;->b:D

    iget-object v2, p2, Lcom/google/android/gms/maps/model/LatLngBounds;->b:Lcom/google/android/gms/maps/model/LatLng;

    iget-wide v2, v2, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static {p1, v0, v1, v2, v3}, Lcom/twitter/android/NearbyFragment;->a(Lcom/twitter/android/NearbyFragment;DD)D

    move-result-wide v0

    div-double/2addr v0, v4

    iput-wide v0, p0, Lcom/twitter/android/nr;->c:D

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/util/HashMap;
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->o(Lcom/twitter/android/NearbyFragment;)Lcom/twitter/android/nd;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/nr;->b:D

    iget-wide v3, p0, Lcom/twitter/android/nr;->c:D

    iget-object v5, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-virtual {v5}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget-object v6, p0, Lcom/twitter/android/nr;->d:Ljava/util/HashMap;

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/nd;->a(DDLandroid/content/res/Resources;Ljava/util/HashMap;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method protected a(Ljava/util/HashMap;)V
    .locals 7

    const/4 v6, 0x1

    if-nez p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    iget-object v2, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->a(Lcom/twitter/android/NearbyFragment;)Lcom/google/android/gms/maps/c;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/k;

    move-result-object v4

    iget-object v2, p0, Lcom/twitter/android/nr;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v2}, Lcom/twitter/android/NearbyFragment;->p(Lcom/twitter/android/NearbyFragment;)Lcom/twitter/android/client/c;

    move-result-object v5

    iget-object v2, p0, Lcom/twitter/android/nr;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v6, :cond_2

    iget-object v0, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-virtual {v0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v2, v6}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    :goto_2
    invoke-virtual {v4, v0}, Lcom/google/android/gms/maps/model/k;->a(Lcom/google/android/gms/maps/model/a;)V

    iget-object v0, p0, Lcom/twitter/android/nr;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_3
    invoke-virtual {v4, v6}, Lcom/google/android/gms/maps/model/k;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->r(Lcom/twitter/android/NearbyFragment;)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v4, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-virtual {v0}, Lcom/twitter/android/NearbyFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(Landroid/graphics/Bitmap;)Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->q(Lcom/twitter/android/NearbyFragment;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/nr;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v0, v2, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/nr;->a:Lcom/twitter/android/NearbyFragment;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/NearbyFragment;->c(Lcom/twitter/android/NearbyFragment;Z)Z

    goto/16 :goto_0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/nr;->a([Ljava/lang/Void;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/util/HashMap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/nr;->a(Ljava/util/HashMap;)V

    return-void
.end method
