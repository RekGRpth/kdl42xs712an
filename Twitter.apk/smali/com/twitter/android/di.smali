.class Lcom/twitter/android/di;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/DMConversationActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/DMConversationActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/di;->a:Lcom/twitter/android/DMConversationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    new-instance v0, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v1, p0, Lcom/twitter/android/di;->a:Lcom/twitter/android/DMConversationActivity;

    invoke-static {v1}, Lcom/twitter/android/DMConversationActivity;->a(Lcom/twitter/android/DMConversationActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string/jumbo v3, "app:twitter_service:direct_messages::discard_dm"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/di;->a:Lcom/twitter/android/DMConversationActivity;

    invoke-static {v0}, Lcom/twitter/library/telephony/TelephonyUtil;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "connected"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/di;->a:Lcom/twitter/android/DMConversationActivity;

    invoke-static {v0}, Lcom/twitter/android/DMConversationActivity;->b(Lcom/twitter/android/DMConversationActivity;)Landroid/widget/RelativeLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/RelativeLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "has_media"

    :goto_1
    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->h(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v0, p0, Lcom/twitter/android/di;->a:Lcom/twitter/android/DMConversationActivity;

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/di;->a:Lcom/twitter/android/DMConversationActivity;

    invoke-static {v0}, Lcom/twitter/android/DMConversationActivity;->c(Lcom/twitter/android/DMConversationActivity;)V

    return-void

    :cond_0
    const-string/jumbo v0, "disconnected"

    goto :goto_0

    :cond_1
    const-string/jumbo v0, "no_media"

    goto :goto_1
.end method
