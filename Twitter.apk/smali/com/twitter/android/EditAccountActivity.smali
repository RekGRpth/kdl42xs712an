.class public Lcom/twitter/android/EditAccountActivity;
.super Lcom/twitter/android/BaseSignUpActivity;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnFocusChangeListener;


# static fields
.field private static final r:Z


# instance fields
.field private A:Ljava/lang/String;

.field private B:Ljava/lang/String;

.field private C:Ljava/lang/String;

.field private E:Lcom/twitter/android/client/c;

.field private F:I

.field private s:[Ljava/lang/String;

.field private t:I

.field private u:Lcom/twitter/android/util/r;

.field private v:Landroid/widget/Button;

.field private w:Landroid/widget/Button;

.field private x:Ljava/lang/String;

.field private y:Ljava/lang/String;

.field private z:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "EditAccountActivity"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/EditAccountActivity;->r:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/BaseSignUpActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/EditAccountActivity;I)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/EditAccountActivity;->F:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/EditAccountActivity;->F:I

    return v0
.end method

.method static synthetic a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;)V
    .locals 8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->u:Lcom/twitter/android/util/r;

    invoke-interface {v0}, Lcom/twitter/android/util/r;->s()Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/r;->r()Lcom/twitter/android/util/r;

    new-instance v0, Lcom/twitter/library/client/v;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->y:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/EditAccountActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v4}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/client/v;-><init>(Landroid/net/Uri;Landroid/net/Uri;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, p1, v0}, Lcom/twitter/android/client/cd;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/v;)Ljava/lang/String;

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->removeDialog(I)V

    invoke-virtual {p0}, Lcom/twitter/android/EditAccountActivity;->d()V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 9

    const/4 v0, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    :goto_0
    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->l()Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v5

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    move v8, v7

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    iget-object v3, p0, Lcom/twitter/android/EditAccountActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v4

    move-object v3, p1

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->d(Ljava/lang/String;)V

    return-void

    :cond_0
    move-object v2, v0

    goto :goto_0

    :cond_1
    move-object v5, v0

    goto :goto_1

    :cond_2
    move v8, v6

    goto :goto_2
.end method

.method static synthetic a(Lcom/twitter/android/EditAccountActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/EditAccountActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/EditAccountActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/EditAccountActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/util/r;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->u:Lcom/twitter/android/util/r;

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/EditAccountActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/EditAccountActivity;->F:I

    return v0
.end method

.method static synthetic e(Lcom/twitter/android/EditAccountActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->C:Ljava/lang/String;

    return-object v0
.end method

.method private f()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->a:Landroid/widget/EditText;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditAccountActivity;->a(Landroid/widget/EditText;Z)V

    iput v1, p0, Lcom/twitter/android/EditAccountActivity;->k:I

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->a:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->g:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/EditAccountActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/EditAccountActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->m()Z

    move-result v0

    return v0
.end method

.method private g()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->b:Landroid/widget/EditText;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditAccountActivity;->a(Landroid/widget/EditText;Z)V

    iput v1, p0, Lcom/twitter/android/EditAccountActivity;->l:I

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->b:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->h:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/EditAccountActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/EditAccountActivity;)Z
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->l()Z

    move-result v0

    return v0
.end method

.method private h()V
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditAccountActivity;->a(Landroid/widget/EditText;Z)V

    iput v1, p0, Lcom/twitter/android/EditAccountActivity;->m:I

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->i:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/EditAccountActivity;->a(Landroid/widget/EditText;Landroid/widget/TextView;Ljava/lang/String;)V

    return-void
.end method

.method private k()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->x:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->A:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->z:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private n()V
    .locals 7

    const/4 v6, -0x2

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300d8    # com.twitter.android.R.layout.nux_progress_indicator

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09000b    # com.twitter.android.R.id.indicator

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/twitter/android/EditAccountActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0f02d3    # com.twitter.android.R.string.nux_progress

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p0, Lcom/twitter/android/EditAccountActivity;->t:I

    add-int/lit8 v5, v5, 0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/EditAccountActivity;->s:[Ljava/lang/String;

    array-length v5, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/EditAccountActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v6, v6, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(III)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    return-void
.end method

.method private o()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->showDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->u:Lcom/twitter/android/util/r;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/util/r;->a(Ljava/lang/String;)Lcom/twitter/android/util/r;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/util/r;->q()Lcom/twitter/android/util/r;

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/EditAccountActivity;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03005e    # com.twitter.android.R.layout.edit_account

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(I)V

    return-object v0
.end method

.method a()V
    .locals 2

    const/4 v1, 0x1

    iget v0, p0, Lcom/twitter/android/EditAccountActivity;->k:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/EditAccountActivity;->l:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/EditAccountActivity;->m:I

    if-ne v0, v1, :cond_0

    iget v0, p0, Lcom/twitter/android/EditAccountActivity;->n:I

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->v:Landroid/widget/Button;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->w:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v0, v6, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account::::impression"

    aput-object v4, v0, v5

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f09011d    # com.twitter.android.R.id.next

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/EditAccountActivity;->v:Landroid/widget/Button;

    const v0, 0x7f090141    # com.twitter.android.R.id.save

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/EditAccountActivity;->w:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->v:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->w:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    const v1, 0x7f0900c7    # com.twitter.android.R.id.email

    invoke-virtual {p0, v1}, Lcom/twitter/android/EditAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    const v2, 0x7f090098    # com.twitter.android.R.id.username

    invoke-virtual {p0, v2}, Lcom/twitter/android/EditAccountActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/EditText;

    invoke-virtual {p0}, Lcom/twitter/android/EditAccountActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    if-nez p1, :cond_3

    const-string/jumbo v4, "fullname"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->x:Ljava/lang/String;

    const-string/jumbo v4, "email"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->A:Ljava/lang/String;

    const-string/jumbo v4, "avatar_uri"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->y:Ljava/lang/String;

    const-string/jumbo v4, "username"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->z:Ljava/lang/String;

    const-string/jumbo v4, "password"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    const-string/jumbo v4, "default_password"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->C:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/twitter/android/EditAccountActivity;->C:Ljava/lang/String;

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    :cond_0
    iput v5, p0, Lcom/twitter/android/EditAccountActivity;->F:I

    :goto_0
    invoke-static {p0}, Lcom/twitter/android/util/q;->a(Landroid/content/Context;)Lcom/twitter/android/util/r;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->u:Lcom/twitter/android/util/r;

    iget-object v4, p0, Lcom/twitter/android/EditAccountActivity;->x:Ljava/lang/String;

    invoke-virtual {v0, v4}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->z:Ljava/lang/String;

    invoke-virtual {v2, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->A:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseSignUpActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->f()V

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->g()V

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->h()V

    new-instance v0, Lcom/twitter/android/fh;

    invoke-direct {v0, p0}, Lcom/twitter/android/fh;-><init>(Lcom/twitter/android/EditAccountActivity;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->d:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v0}, Lcom/twitter/android/util/d;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    const v0, 0x7f0f0438    # com.twitter.android.R.string.signup_addr_book_and_phone_text

    new-array v1, v6, [Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->f:Lcom/twitter/android/util/d;

    invoke-interface {v2}, Lcom/twitter/android/util/d;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/EditAccountActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->e:Landroid/widget/CheckBox;

    invoke-virtual {v1, v0}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    :cond_1
    const-string/jumbo v0, "follow flow"

    invoke-virtual {v3, v0}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/EditAccountActivity;->s:[Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->s:[Ljava/lang/String;

    if-nez v0, :cond_2

    new-array v0, v5, [Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/EditAccountActivity;->s:[Ljava/lang/String;

    :cond_2
    const-string/jumbo v0, "follow flow progress"

    invoke-virtual {v3, v0, v5}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/EditAccountActivity;->t:I

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->n()V

    return-void

    :cond_3
    const-string/jumbo v4, "fullname"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->x:Ljava/lang/String;

    const-string/jumbo v4, "email"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->A:Ljava/lang/String;

    const-string/jumbo v4, "avatar_uri"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->y:Ljava/lang/String;

    const-string/jumbo v4, "username"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->z:Ljava/lang/String;

    const-string/jumbo v4, "password"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    const-string/jumbo v4, "default_password"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/EditAccountActivity;->C:Ljava/lang/String;

    const-string/jumbo v4, "password_retry_count"

    invoke-virtual {p1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iput v4, p0, Lcom/twitter/android/EditAccountActivity;->F:I

    goto/16 :goto_0

    :cond_4
    const v0, 0x7f0f0439    # com.twitter.android.R.string.signup_addr_book_no_phone_text

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->a:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->b:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->c:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/EditAccountActivity;->x:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->f()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->A:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->g()V

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/EditAccountActivity;->z:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->h()V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->afterTextChanged(Landroid/text/Editable;)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->onClick(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/twitter/android/EditAccountActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->k()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::full_name:change"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->m()Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::username:change"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->l()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::email:change"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_2
    iget-object v2, p0, Lcom/twitter/android/EditAccountActivity;->E:Lcom/twitter/android/client/c;

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::next_button:click"

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, v1, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090141    # com.twitter.android.R.id.save

    if-ne v0, v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->o()V

    :cond_3
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 3

    const/4 v2, 0x0

    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    new-instance v0, Landroid/app/ProgressDialog;

    invoke-direct {v0, p0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    const v1, 0x7f0f038a    # com.twitter.android.R.string.saving

    invoke-virtual {p0, v1}, Lcom/twitter/android/EditAccountActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-nez p2, :cond_0

    const v1, 0x7f090097    # com.twitter.android.R.id.name

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->k()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->f()V

    :goto_0
    return-void

    :cond_0
    if-nez p2, :cond_1

    const v1, 0x7f0900c7    # com.twitter.android.R.id.email

    if-ne v0, v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->l()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->g()V

    goto :goto_0

    :cond_1
    if-nez p2, :cond_2

    const v1, 0x7f090098    # com.twitter.android.R.id.username

    if-ne v0, v1, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->m()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/EditAccountActivity;->h()V

    goto :goto_0

    :cond_2
    invoke-super {p0, p1, p2}, Lcom/twitter/android/BaseSignUpActivity;->onFocusChange(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/BaseSignUpActivity;->onPause()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditAccountActivity;->removeDialog(I)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/BaseSignUpActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "fullname"

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->x:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "email"

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->A:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "username"

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->z:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "password"

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->B:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "default_password"

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->C:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "avatar_uri"

    iget-object v1, p0, Lcom/twitter/android/EditAccountActivity;->y:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "password_retry_count"

    iget v1, p0, Lcom/twitter/android/EditAccountActivity;->F:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method
