.class public Lcom/twitter/android/GeoDebugActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/google/android/gms/maps/k;
.implements Lcom/twitter/android/widget/ag;
.implements Lcom/twitter/library/platform/i;


# instance fields
.field a:Ljava/util/HashMap;

.field b:Ljava/util/HashMap;

.field c:Lcom/twitter/library/platform/m;

.field private d:Landroid/widget/Button;

.field private e:Lcom/twitter/android/widget/GeoDeciderFragment;

.field private f:Lcom/google/android/gms/maps/SupportMapFragment;

.field private g:Landroid/support/v4/app/Fragment;

.field private h:Lcom/google/android/gms/maps/c;

.field private i:Lcom/google/android/gms/maps/model/k;

.field private j:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/maps/model/LatLng;Landroid/location/Location;)Lcom/twitter/android/ib;
    .locals 12

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v0}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {p0, p1}, Lcom/twitter/android/GeoDebugActivity;->b(Lcom/google/android/gms/maps/model/LatLng;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Ljava/lang/String;)Lcom/google/android/gms/maps/model/MarkerOptions;

    iget-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/k;

    move-result-object v11

    new-instance v2, Lcom/twitter/android/hs;

    invoke-direct {v2, p0}, Lcom/twitter/android/hs;-><init>(Lcom/twitter/android/GeoDebugActivity;)V

    iget-wide v3, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    iget-wide v5, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {p2}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x1

    const/4 v10, 0x0

    move-object v0, p0

    move-object v9, v1

    invoke-static/range {v0 .. v10}, Lcom/twitter/android/client/ao;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;

    move-result-object v2

    if-eqz v11, :cond_1

    new-instance v0, Lcom/twitter/android/ib;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ib;-><init>(Lcom/twitter/android/GeoDebugActivity;Lcom/twitter/android/hn;)V

    iput-object p2, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    new-instance v1, Lcom/google/android/gms/maps/model/CircleOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/CircleOptions;-><init>()V

    invoke-virtual {v1, p1}, Lcom/google/android/gms/maps/model/CircleOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/maps/model/CircleOptions;->a(D)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    const v3, 0x3355acee

    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/CircleOptions;->b(I)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    const/high16 v3, 0x40400000    # 3.0f

    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/CircleOptions;->a(F)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    const v3, -0x33ba5c1b    # -5.1810196E7f

    invoke-virtual {v1, v3}, Lcom/google/android/gms/maps/model/CircleOptions;->a(I)Lcom/google/android/gms/maps/model/CircleOptions;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/CircleOptions;)Lcom/google/android/gms/maps/model/e;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/ib;->b:Lcom/google/android/gms/maps/model/e;

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-virtual {v1, v11, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/HashMap;

    invoke-virtual {v1, v2, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    move-object v1, v0

    :cond_1
    return-object v1
.end method

.method private a(Landroid/support/v4/app/Fragment;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Landroid/support/v4/app/Fragment;

    if-ne v0, p1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x7f04001a    # com.twitter.android.R.anim.slide_up

    const v2, 0x7f040017    # com.twitter.android.R.anim.slide_down

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->setCustomAnimations(II)Landroid/support/v4/app/FragmentTransaction;

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->g:Landroid/support/v4/app/Fragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0, p1}, Landroid/support/v4/app/FragmentTransaction;->show(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    iput-object p1, p0, Lcom/twitter/android/GeoDebugActivity;->g:Landroid/support/v4/app/Fragment;

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Landroid/support/v4/app/Fragment;

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->d:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/GeoDebugActivity;Landroid/support/v4/app/Fragment;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/GeoDebugActivity;->a(Landroid/support/v4/app/Fragment;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/GeoDebugActivity;)Lcom/google/android/gms/maps/SupportMapFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    return-object v0
.end method

.method private b(Lcom/google/android/gms/maps/model/LatLng;)Ljava/lang/String;
    .locals 4

    new-instance v0, Ljava/text/DecimalFormatSymbols;

    sget-object v1, Ljava/util/Locale;->ENGLISH:Ljava/util/Locale;

    invoke-direct {v0, v1}, Ljava/text/DecimalFormatSymbols;-><init>(Ljava/util/Locale;)V

    new-instance v1, Ljava/text/DecimalFormat;

    invoke-direct {v1}, Ljava/text/DecimalFormat;-><init>()V

    invoke-virtual {v1, v0}, Ljava/text/DecimalFormat;->setDecimalFormatSymbols(Ljava/text/DecimalFormatSymbols;)V

    const/4 v0, 0x7

    invoke-virtual {v1, v0}, Ljava/text/DecimalFormat;->setMaximumFractionDigits(I)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "("

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v2, ", "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v2, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v1, v2, v3}, Ljava/text/DecimalFormat;->format(D)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private b(Landroid/location/Location;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-le v0, v1, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v0

    invoke-virtual {p1, v0, v1}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    :cond_0
    return-void
.end method

.method private c(Lcom/google/android/gms/maps/model/LatLng;)Landroid/location/Location;
    .locals 3

    new-instance v0, Landroid/location/Location;

    const-string/jumbo v1, "gps"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLatitude(D)V

    iget-wide v1, p1, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setLongitude(D)V

    const/high16 v1, 0x41a00000    # 20.0f

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Landroid/location/Location;->setTime(J)V

    invoke-direct {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->b(Landroid/location/Location;)V

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/GeoDebugActivity;)Lcom/twitter/android/widget/GeoDeciderFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/GeoDebugActivity;->j:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->g()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f090192    # com.twitter.android.R.id.geo_decider_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/GeoDeciderFragment;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Lcom/twitter/library/platform/LocationProducer;)V

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/GeoDeciderFragment;->a(Lcom/twitter/android/widget/ag;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->hide(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Landroid/support/v4/app/Fragment;

    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->e:Lcom/twitter/android/widget/GeoDeciderFragment;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->g:Landroid/support/v4/app/Fragment;

    goto :goto_0
.end method

.method private g()V
    .locals 4

    const v3, 0x7f09018e    # com.twitter.android.R.id.map_container

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/SupportMapFragment;

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    if-nez v0, :cond_1

    invoke-static {}, Lcom/google/android/gms/maps/SupportMapFragment;->a()Lcom/google/android/gms/maps/SupportMapFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    const-string/jumbo v2, "map"

    invoke-virtual {v0, v3, v1, v2}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_1
    return-void
.end method

.method private h()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->f:Lcom/google/android/gms/maps/SupportMapFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/maps/SupportMapFragment;->c()Lcom/google/android/gms/maps/c;

    move-result-object v0

    if-eqz v0, :cond_0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/k;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/maps/c;->a(Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/u;->b(Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/u;->c(Z)V

    invoke-virtual {v0}, Lcom/google/android/gms/maps/c;->d()Lcom/google/android/gms/maps/u;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/maps/u;->f(Z)V

    new-instance v1, Lcom/twitter/android/hq;

    invoke-direct {v1, p0}, Lcom/twitter/android/hq;-><init>(Lcom/twitter/android/GeoDebugActivity;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/j;)V

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    invoke-virtual {v0}, Lcom/twitter/library/platform/m;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->a(Lcom/twitter/library/platform/i;)V

    :cond_0
    return-void
.end method

.method private k()V
    .locals 10

    const/4 v9, 0x0

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v1, Lcom/google/android/gms/maps/model/h;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/h;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    invoke-virtual {v0}, Lcom/twitter/library/platform/m;->f()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    new-instance v4, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v5

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v7

    invoke-direct {v4, v5, v6, v7, v8}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v1, v4}, Lcom/google/android/gms/maps/model/h;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/h;

    invoke-direct {p0, v4, v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/google/android/gms/maps/model/LatLng;Landroid/location/Location;)Lcom/twitter/android/ib;

    goto :goto_1

    :cond_2
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_3

    invoke-interface {v2, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/Location;

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    const/high16 v2, 0x41400000    # 12.0f

    invoke-static {v1, v2}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v0, v0, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v2

    iget v2, v2, Landroid/util/DisplayMetrics;->heightPixels:I

    div-int/lit8 v2, v2, 0x2

    iget-object v3, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/h;->a()Lcom/google/android/gms/maps/model/LatLngBounds;

    move-result-object v1

    invoke-static {v1, v0, v2, v9}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLngBounds;III)Lcom/google/android/gms/maps/a;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030082    # com.twitter.android.R.layout.geo_debug

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/location/Location;)V
    .locals 5

    new-instance v0, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v1

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    const/high16 v2, 0x41700000    # 15.0f

    invoke-static {v0, v2}, Lcom/google/android/gms/maps/b;->a(Lcom/google/android/gms/maps/model/LatLng;F)Lcom/google/android/gms/maps/a;

    move-result-object v2

    const/16 v3, 0x3e8

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/a;ILcom/google/android/gms/maps/h;)V

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->i:Lcom/google/android/gms/maps/model/k;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->i:Lcom/google/android/gms/maps/model/k;

    invoke-virtual {v1}, Lcom/google/android/gms/maps/model/k;->a()V

    :cond_0
    new-instance v1, Lcom/google/android/gms/maps/model/MarkerOptions;

    invoke-direct {v1}, Lcom/google/android/gms/maps/model/MarkerOptions;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/LatLng;)Lcom/google/android/gms/maps/model/MarkerOptions;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Z)Lcom/google/android/gms/maps/model/MarkerOptions;

    const/high16 v0, 0x43700000    # 240.0f

    invoke-static {v0}, Lcom/google/android/gms/maps/model/b;->a(F)Lcom/google/android/gms/maps/model/a;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/maps/model/MarkerOptions;->a(Lcom/google/android/gms/maps/model/a;)Lcom/google/android/gms/maps/model/MarkerOptions;

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/c;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->i:Lcom/google/android/gms/maps/model/k;

    invoke-virtual {p0}, Lcom/twitter/android/GeoDebugActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 5

    const/4 v4, 0x4

    const-string/jumbo v0, "Geo Debug Screen"

    invoke-virtual {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f090190    # com.twitter.android.R.id.decider_btn

    invoke-virtual {p0, v0}, Lcom/twitter/android/GeoDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    const-string/jumbo v1, "Decider"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v1, 0x7f090191    # com.twitter.android.R.id.map_btn

    invoke-virtual {p0, v1}, Lcom/twitter/android/GeoDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    const-string/jumbo v2, "Map"

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    const v2, 0x7f090193    # com.twitter.android.R.id.geo_marker_clear

    invoke-virtual {p0, v2}, Lcom/twitter/android/GeoDebugActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->d:Landroid/widget/Button;

    iget-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->d:Landroid/widget/Button;

    const-string/jumbo v3, "Clear"

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    invoke-static {}, Lcom/twitter/library/client/App;->n()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-static {p0}, Lcom/twitter/library/platform/p;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/twitter/android/GeoDebugActivity;->j:Z

    iget-boolean v2, p0, Lcom/twitter/android/GeoDebugActivity;->j:Z

    if-nez v2, :cond_1

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->d:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setVisibility(I)V

    :goto_1
    new-instance v1, Lcom/twitter/android/hp;

    invoke-direct {v1, p0}, Lcom/twitter/android/hp;-><init>(Lcom/twitter/android/GeoDebugActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->f()V

    return-void

    :cond_0
    const/4 v2, 0x0

    goto :goto_0

    :cond_1
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->a:Ljava/util/HashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->b:Ljava/util/HashMap;

    invoke-static {p0}, Lcom/twitter/library/platform/m;->a(Landroid/content/Context;)Lcom/twitter/library/platform/m;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    iget-object v2, p0, Lcom/twitter/android/GeoDebugActivity;->d:Landroid/widget/Button;

    new-instance v3, Lcom/twitter/android/hn;

    invoke-direct {v3, p0}, Lcom/twitter/android/hn;-><init>(Lcom/twitter/android/GeoDebugActivity;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v2, Lcom/twitter/android/ho;

    invoke-direct {v2, p0}, Lcom/twitter/android/ho;-><init>(Lcom/twitter/android/GeoDebugActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_1
.end method

.method public a(Lcom/google/android/gms/maps/model/LatLng;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->h:Lcom/google/android/gms/maps/c;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/GeoDebugActivity;->c(Lcom/google/android/gms/maps/model/LatLng;)Landroid/location/Location;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/GeoDebugActivity;->a(Lcom/google/android/gms/maps/model/LatLng;Landroid/location/Location;)Lcom/twitter/android/ib;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    iget-object v0, v0, Lcom/twitter/android/ib;->a:Landroid/location/Location;

    invoke-virtual {v1, v0}, Lcom/twitter/library/platform/m;->a(Landroid/location/Location;)V

    goto :goto_0
.end method

.method public b()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/GeoDebugActivity;->c:Lcom/twitter/library/platform/m;

    invoke-virtual {v0}, Lcom/twitter/library/platform/m;->g()V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStart()V

    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->h()V

    invoke-direct {p0}, Lcom/twitter/android/GeoDebugActivity;->k()V

    return-void
.end method
