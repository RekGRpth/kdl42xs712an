.class public Lcom/twitter/android/ActivityCursor;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/database/Cursor;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/database/Cursor;

.field private final c:J

.field private final d:I

.field private final e:I

.field private final f:Ljava/util/HashSet;

.field private final g:Ljava/util/HashSet;

.field private final h:Ljava/util/HashSet;

.field private final i:Ljava/util/HashSet;

.field private final j:Ljava/util/HashSet;

.field private final k:Ljava/util/HashSet;

.field private l:[Landroid/database/Cursor;

.field private m:[Ljava/util/HashMap;

.field private n:[Landroid/database/Cursor;

.field private o:[Ljava/util/HashMap;

.field private p:[Landroid/database/Cursor;

.field private q:[Ljava/util/HashMap;

.field private r:Ljava/util/HashSet;

.field private s:Ljava/util/HashSet;

.field private t:Ljava/util/HashSet;

.field private u:Ljava/util/ArrayList;

.field private v:I


# direct methods
.method constructor <init>(Landroid/database/Cursor;JLandroid/content/Context;II)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->f:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->g:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->h:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->i:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->j:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->k:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->r:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->s:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->t:Ljava/util/HashSet;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    iput-object p1, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    iput-wide p2, p0, Lcom/twitter/android/ActivityCursor;->c:J

    iput-object p4, p0, Lcom/twitter/android/ActivityCursor;->a:Landroid/content/Context;

    iput p5, p0, Lcom/twitter/android/ActivityCursor;->d:I

    iput p6, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-direct {p0}, Lcom/twitter/android/ActivityCursor;->c()V

    return-void
.end method

.method private static a(JLandroid/database/Cursor;Ljava/util/HashMap;)Landroid/database/Cursor;
    .locals 1

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-object p2

    :cond_0
    const/4 p2, 0x0

    goto :goto_0
.end method

.method private a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;)Landroid/database/Cursor;
    .locals 4

    iget-wide v0, p0, Lcom/twitter/android/ActivityCursor;->c:J

    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ActivityCursor;->c:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "owner_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ActivityCursor;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "user_id"

    invoke-direct {p0, v2, p3}, Lcom/twitter/android/ActivityCursor;->a(Ljava/lang/String;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/cm;->e:[Ljava/lang/String;

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {p3}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-eq v1, v2, :cond_2

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->r:Ljava/util/HashSet;

    invoke-virtual {v2, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0
.end method

.method private a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;Ljava/lang/String;ILjava/util/HashSet;)Landroid/database/Cursor;
    .locals 4

    iget-wide v0, p0, Lcom/twitter/android/ActivityCursor;->c:J

    invoke-static {p1, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/ActivityCursor;->c:J

    invoke-static {v0, v1, v2}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "owner_id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/ActivityCursor;->c:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-direct {p0, p4, p3}, Lcom/twitter/android/ActivityCursor;->a(Ljava/lang/String;Ljava/util/HashSet;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/Tweet;->b:[Ljava/lang/String;

    invoke-direct {p0, v0, v2, v1}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-interface {v0, p5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-interface {v0}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p2, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    :cond_1
    invoke-virtual {p3}, Ljava/util/HashSet;->size()I

    move-result v1

    invoke-virtual {p2}, Ljava/util/HashMap;->size()I

    move-result v2

    if-eq v1, v2, :cond_2

    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1, p3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {p2}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    invoke-virtual {p6, v1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    :cond_2
    return-object v0
.end method

.method private a(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/database/Cursor;)Lcom/twitter/library/api/TwitterUser;
    .locals 4

    new-instance v1, Lcom/twitter/library/api/TwitterUser;

    invoke-direct {v1}, Lcom/twitter/library/api/TwitterUser;-><init>()V

    const/4 v0, 0x2

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    const/4 v0, 0x4

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    const/4 v0, 0x3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    const/4 v0, 0x5

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    const/16 v0, 0x8

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->profileDescription:Ljava/lang/String;

    const/16 v0, 0x9

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TweetEntities;

    iput-object v0, v1, Lcom/twitter/library/api/TwitterUser;->descriptionEntities:Lcom/twitter/library/api/TweetEntities;

    const/4 v0, 0x7

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    iput v0, v1, Lcom/twitter/library/api/TwitterUser;->friendship:I

    return-object v1
.end method

.method private a(Ljava/lang/String;Ljava/util/HashSet;)Ljava/lang/String;
    .locals 5

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    move-result v0

    mul-int/lit8 v0, v0, 0x14

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " IN ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {p2}, Ljava/util/HashSet;->size()I

    move-result v3

    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v3, -0x1

    if-eq v1, v0, :cond_0

    const-string/jumbo v0, ","

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, ")"

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/database/Cursor;ILjava/util/HashSet;I)V
    .locals 5

    const/4 v0, 0x0

    invoke-interface {p1, p2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    array-length v2, v1

    div-int/lit8 v2, v2, 0x8

    invoke-static {p4, v2}, Ljava/lang/Math;->min(II)I

    move-result v2

    mul-int/lit8 v3, v2, 0x8

    invoke-static {v1, v0, v3}, Ljava/nio/ByteBuffer;->wrap([BII)Ljava/nio/ByteBuffer;

    move-result-object v1

    :goto_0
    if-ge v0, v2, :cond_0

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {p3, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method

.method private a(Landroid/database/Cursor;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V
    .locals 3

    const/16 v2, 0x8

    const/4 v1, 0x5

    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    const/16 v0, 0xb

    iget v1, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v0, p5, v1}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    iget v1, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-direct {p0, v0, v2, p3, v1}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    goto :goto_0

    :pswitch_3
    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v2, p4, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    goto :goto_0

    :pswitch_4
    iget v0, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-direct {p0, p1, v2, p3, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    goto :goto_0

    :pswitch_5
    iget v0, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v2, p4, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    goto :goto_0

    :pswitch_6
    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v1, p2, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-direct {p0, p1, v2, p6, v0}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;ILjava/util/HashSet;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_1
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_6
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method private b()V
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    invoke-virtual {v2}, Ljava/util/HashSet;->clear()V

    invoke-virtual {v4}, Ljava/util/HashSet;->clear()V

    invoke-virtual {v6}, Ljava/util/HashSet;->clear()V

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    move-object v0, p0

    move-object v3, v2

    move-object v5, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->r:Ljava/util/HashSet;

    invoke-virtual {v2, v0}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->s:Ljava/util/HashSet;

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->t:Ljava/util/HashSet;

    invoke-virtual {v6, v0}, Ljava/util/HashSet;->retainAll(Ljava/util/Collection;)Z

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v4}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v6}, Ljava/util/HashSet;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method private c()V
    .locals 12

    const/16 v11, 0x15

    const/4 v10, 0x1

    const/4 v7, 0x3

    invoke-direct {p0}, Lcom/twitter/android/ActivityCursor;->d()V

    invoke-direct {p0}, Lcom/twitter/android/ActivityCursor;->e()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->f:Ljava/util/HashSet;

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->g:Ljava/util/HashSet;

    iget-object v4, p0, Lcom/twitter/android/ActivityCursor;->h:Ljava/util/HashSet;

    iget-object v5, p0, Lcom/twitter/android/ActivityCursor;->i:Ljava/util/HashSet;

    iget-object v6, p0, Lcom/twitter/android/ActivityCursor;->j:Ljava/util/HashSet;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;Ljava/util/HashSet;)V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_1
    new-array v0, v7, [Landroid/database/Cursor;

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    new-array v0, v7, [Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    new-array v0, v7, [Landroid/database/Cursor;

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    new-array v0, v7, [Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    new-array v0, v7, [Landroid/database/Cursor;

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->n:[Landroid/database/Cursor;

    new-array v0, v7, [Ljava/util/HashMap;

    iput-object v0, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    sget-object v0, Lcom/twitter/android/ActivityCursor$ObjectField;->a:Lcom/twitter/android/ActivityCursor$ObjectField;

    invoke-virtual {v0}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v0

    sget-object v1, Lcom/twitter/android/ActivityCursor$ObjectField;->b:Lcom/twitter/android/ActivityCursor$ObjectField;

    invoke-virtual {v1}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v7

    sget-object v1, Lcom/twitter/android/ActivityCursor$ObjectField;->c:Lcom/twitter/android/ActivityCursor$ObjectField;

    invoke-virtual {v1}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v8

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    aput-object v2, v1, v0

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    sget-object v2, Lcom/twitter/library/provider/ax;->a:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    aget-object v3, v3, v0

    iget-object v4, p0, Lcom/twitter/android/ActivityCursor;->f:Ljava/util/HashSet;

    invoke-direct {p0, v2, v3, v4}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;)Landroid/database/Cursor;

    move-result-object v2

    aput-object v2, v1, v0

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, v7

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    sget-object v1, Lcom/twitter/library/provider/ax;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    aget-object v2, v2, v7

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->g:Ljava/util/HashSet;

    invoke-direct {p0, v1, v2, v3}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;)Landroid/database/Cursor;

    move-result-object v1

    aput-object v1, v0, v7

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, v7

    iget-object v9, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    sget-object v1, Lcom/twitter/library/provider/as;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    aget-object v2, v0, v7

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->h:Ljava/util/HashSet;

    const-string/jumbo v4, "g_status_id"

    iget-object v6, p0, Lcom/twitter/android/ActivityCursor;->s:Ljava/util/HashSet;

    move-object v0, p0

    move v5, v10

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;Ljava/lang/String;ILjava/util/HashSet;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v9, v7

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, v7

    iget-object v9, p0, Lcom/twitter/android/ActivityCursor;->n:[Landroid/database/Cursor;

    sget-object v1, Lcom/twitter/library/provider/as;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    aget-object v2, v0, v7

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->j:Ljava/util/HashSet;

    const-string/jumbo v4, "ref_id"

    iget-object v6, p0, Lcom/twitter/android/ActivityCursor;->t:Ljava/util/HashSet;

    move-object v0, p0

    move v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;Ljava/lang/String;ILjava/util/HashSet;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v9, v7

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, v8

    iget-object v7, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    sget-object v1, Lcom/twitter/library/provider/as;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    aget-object v2, v0, v8

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->i:Ljava/util/HashSet;

    const-string/jumbo v4, "g_status_id"

    iget-object v6, p0, Lcom/twitter/android/ActivityCursor;->s:Ljava/util/HashSet;

    move-object v0, p0

    move v5, v10

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;Ljava/lang/String;ILjava/util/HashSet;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v7, v8

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    aput-object v1, v0, v8

    iget-object v7, p0, Lcom/twitter/android/ActivityCursor;->n:[Landroid/database/Cursor;

    sget-object v1, Lcom/twitter/library/provider/as;->c:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    aget-object v2, v0, v8

    iget-object v3, p0, Lcom/twitter/android/ActivityCursor;->k:Ljava/util/HashSet;

    const-string/jumbo v4, "ref_id"

    iget-object v6, p0, Lcom/twitter/android/ActivityCursor;->t:Ljava/util/HashSet;

    move-object v0, p0

    move v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ActivityCursor;->a(Landroid/net/Uri;Ljava/util/HashMap;Ljava/util/HashSet;Ljava/lang/String;ILjava/util/HashSet;)Landroid/database/Cursor;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-direct {p0}, Lcom/twitter/android/ActivityCursor;->b()V

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityCursor;->moveToPosition(I)Z

    return-void
.end method

.method private d()V
    .locals 6

    const/4 v0, 0x0

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    if-eqz v1, :cond_2

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    if-eqz v4, :cond_0

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    iput-object v5, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    if-eqz v1, :cond_5

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v2, v1

    if-eqz v4, :cond_3

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_4
    iput-object v5, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    :cond_5
    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->n:[Landroid/database/Cursor;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->n:[Landroid/database/Cursor;

    array-length v2, v1

    :goto_2
    if-ge v0, v2, :cond_7

    aget-object v3, v1, v0

    if-eqz v3, :cond_6

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_7
    iput-object v5, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    iput-object v5, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    iput-object v5, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    return-void
.end method

.method private e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->f:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->h:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->j:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->s:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->t:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->r:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/ActivityCursor$ObjectField;[B)Ljava/util/ArrayList;
    .locals 9

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->e:I

    invoke-static {p2, v0}, Lcom/twitter/library/provider/az;->a([BI)[J

    move-result-object v1

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-wide v4, v1, v0

    iget-object v6, p0, Lcom/twitter/android/ActivityCursor;->p:[Landroid/database/Cursor;

    invoke-virtual {p1}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    iget-object v7, p0, Lcom/twitter/android/ActivityCursor;->q:[Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v8

    aget-object v7, v7, v8

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/android/ActivityCursor;->a(JLandroid/database/Cursor;Ljava/util/HashMap;)Landroid/database/Cursor;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-static {v4}, Lcom/twitter/android/ActivityCursor;->a(Landroid/database/Cursor;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public a(Lcom/twitter/android/ActivityCursor$ObjectField;[BLcom/twitter/android/ActivityCursor$IdType;)Ljava/util/ArrayList;
    .locals 8

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->d:I

    invoke-static {p2, v0}, Lcom/twitter/library/provider/az;->a([BI)[J

    move-result-object v3

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/twitter/android/ActivityCursor$ObjectField;->ordinal()I

    move-result v0

    sget-object v1, Lcom/twitter/android/s;->a:[I

    invoke-virtual {p3}, Lcom/twitter/android/ActivityCursor$IdType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->l:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->m:[Ljava/util/HashMap;

    aget-object v0, v2, v0

    :goto_0
    array-length v5, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v5, :cond_1

    aget-wide v6, v3, v2

    invoke-static {v6, v7, v1, v0}, Lcom/twitter/android/ActivityCursor;->a(JLandroid/database/Cursor;Ljava/util/HashMap;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_0

    new-instance v7, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v7, v6}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :pswitch_0
    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->n:[Landroid/database/Cursor;

    aget-object v1, v1, v0

    iget-object v2, p0, Lcom/twitter/android/ActivityCursor;->o:[Ljava/util/HashMap;

    aget-object v0, v2, v0

    goto :goto_0

    :cond_1
    return-object v4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public a()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->r:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->s:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->t:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->size()I

    move-result v0

    if-lez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/ActivityCursor;->d()V

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    return-void
.end method

.method public copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->copyStringToBuffer(ILandroid/database/CharArrayBuffer;)V

    return-void
.end method

.method public deactivate()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->deactivate()V

    return-void
.end method

.method public getBlob(I)[B
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    return-object v0
.end method

.method public getColumnCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnCount()I

    move-result v0

    return v0
.end method

.method public getColumnIndex(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getColumnIndexOrThrow(Ljava/lang/String;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public getColumnName(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnName(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getColumnNames()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getDouble(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public getExtras()Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public getFloat(I)F
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getFloat(I)F

    move-result v0

    return v0
.end method

.method public getInt(I)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public getLong(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getNotificationUri()Landroid/net/Uri;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getNotificationUri()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public getPosition()I
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    return v0
.end method

.method public getShort(I)S
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getShort(I)S

    move-result v0

    return v0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getType(I)I
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getType(I)I

    move-result v0

    return v0
.end method

.method public getWantsAllOnMoveCalls()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->getWantsAllOnMoveCalls()Z

    move-result v0

    return v0
.end method

.method public final isAfterLast()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isBeforeFirst()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isClosed()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v0

    return v0
.end method

.method public final isFirst()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final isLast()Z
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/ActivityCursor;->v:I

    add-int/lit8 v2, v0, -0x1

    if-ne v1, v2, :cond_0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    return v0
.end method

.method public move(I)Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 2

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ActivityCursor;->getCount()I

    move-result v1

    if-lt p1, v1, :cond_1

    iput v1, p0, Lcom/twitter/android/ActivityCursor;->v:I

    :cond_0
    :goto_0
    return v0

    :cond_1
    if-gez p1, :cond_2

    const/4 v1, -0x1

    iput v1, p0, Lcom/twitter/android/ActivityCursor;->v:I

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->u:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_1
    iget-object v1, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v1, v0}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/twitter/android/ActivityCursor;->v:I

    goto :goto_0

    :cond_3
    move v0, p1

    goto :goto_1
.end method

.method public moveToPrevious()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ActivityCursor;->v:I

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/ActivityCursor;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public registerContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public registerDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method public requery()Z
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ActivityCursor;->c()V

    :cond_0
    return v0
.end method

.method public respond(Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->respond(Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1, p2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    return-void
.end method

.method public unregisterContentObserver(Landroid/database/ContentObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public unregisterDataSetObserver(Landroid/database/DataSetObserver;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ActivityCursor;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method
