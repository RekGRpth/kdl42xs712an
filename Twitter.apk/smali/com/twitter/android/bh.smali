.class Lcom/twitter/android/bh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private final a:Lcom/twitter/library/client/aa;

.field private final b:Lcom/twitter/library/api/TwitterUser;

.field private final c:Lcom/twitter/android/client/c;


# direct methods
.method public constructor <init>(Lcom/twitter/android/BaseMessagesActivity;Lcom/twitter/library/api/TwitterUser;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/bh;->a:Lcom/twitter/library/client/aa;

    iput-object p2, p0, Lcom/twitter/android/bh;->b:Lcom/twitter/library/api/TwitterUser;

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 9

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/4 v6, 0x0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    const-string/jumbo v0, "follow"

    iget-object v1, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/bh;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v1, v2, v3, v6, v7}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/bh;->c:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v3, p0, Lcom/twitter/android/bh;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v3, v8, [Ljava/lang/String;

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "message_conversation::follow_dialog::"

    aput-object v5, v4, v6

    aput-object v0, v4, v8

    invoke-static {v4}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/bh;->b:Lcom/twitter/library/api/TwitterUser;

    iget-wide v2, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-virtual {v0, v2, v3, v7, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-string/jumbo v0, "dismiss"

    goto :goto_0
.end method
