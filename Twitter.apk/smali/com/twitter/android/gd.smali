.class Lcom/twitter/android/gd;
.super Landroid/widget/BaseAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/FilterActivity;

.field private final b:Landroid/content/Context;

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/FilterActivity;Landroid/content/Context;)V
    .locals 3

    iput-object p1, p0, Lcom/twitter/android/gd;->a:Lcom/twitter/android/FilterActivity;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/gd;->b:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    div-int/lit8 v1, v1, 0x3

    const v2, 0x7f0c0065    # com.twitter.android.R.dimen.filter_grid_view_divider

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    sub-int/2addr v1, v2

    iput v1, p0, Lcom/twitter/android/gd;->c:I

    int-to-float v1, v1

    const v2, 0x7f0c0064    # com.twitter.android.R.dimen.filter_grid_name_height

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    add-float/2addr v0, v1

    float-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/gd;->d:I

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/gd;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->c()[Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const v4, 0x7f090077    # com.twitter.android.R.id.image

    iget-object v0, p0, Lcom/twitter/android/gd;->a:Lcom/twitter/android/FilterActivity;

    iget-object v0, v0, Lcom/twitter/android/FilterActivity;->b:Lcom/twitter/android/FilterManager;

    invoke-virtual {v0}, Lcom/twitter/android/FilterManager;->c()[Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v0, v1

    if-lt p1, v0, :cond_1

    :cond_0
    const/4 p2, 0x0

    :goto_0
    return-object p2

    :cond_1
    if-nez p2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/gd;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f030072    # com.twitter.android.R.layout.filter_grid_thumb

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    new-instance v2, Landroid/widget/AbsListView$LayoutParams;

    iget v3, p0, Lcom/twitter/android/gd;->c:I

    iget v4, p0, Lcom/twitter/android/gd;->d:I

    invoke-direct {v2, v3, v4}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {p2, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    aget-object v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    const v0, 0x7f090176    # com.twitter.android.R.id.filter_name

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/FilterActivity;->b()[I

    move-result-object v1

    aget v1, v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    goto :goto_1
.end method
