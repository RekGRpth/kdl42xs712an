.class public Lcom/twitter/android/MediaTagFragment$MediaTagEditText;
.super Lcom/twitter/android/widget/SuggestionEditText;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    const v0, 0x7f010019    # com.twitter.android.R.attr.mediaTagEditTextStyle

    invoke-direct {p0, p1, p2, v0}, Lcom/twitter/android/widget/SuggestionEditText;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->c:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->requestFocus()Z

    return-void
.end method

.method protected a(IILjava/lang/CharSequence;)V
    .locals 0

    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public setSuggestionList(Landroid/widget/ListView;)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setListView(Landroid/widget/ListView;)V

    return-void
.end method
