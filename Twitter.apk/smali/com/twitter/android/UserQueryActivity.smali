.class public abstract Lcom/twitter/android/UserQueryActivity;
.super Lcom/twitter/android/ListFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/zf;


# instance fields
.field private a:Lcom/twitter/android/ze;

.field protected b:J

.field protected c:Ljava/lang/String;

.field d:Z

.field private e:Lcom/twitter/library/client/j;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/ListFragmentActivity;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/UserQueryActivity;Ljava/lang/String;)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/UserQueryActivity;->e(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 5

    const-wide/16 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->h()Lcom/twitter/library/client/j;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/UserQueryActivity;->e:Lcom/twitter/library/client/j;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/UserQueryActivity;->e:Lcom/twitter/library/client/j;

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    if-eqz p1, :cond_1

    const-string/jumbo v0, "user_id"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    const-string/jumbo v0, "username"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ListFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3, v0, v1}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    const-string/jumbo v3, "screen_name"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-virtual {v2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "twitter"

    invoke-virtual {v2}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    const-string/jumbo v3, "user_id"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    :try_start_0
    invoke-static {v3}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    :cond_2
    :goto_1
    const-string/jumbo v3, "screen_name"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-wide v0, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    iput-object v2, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method protected a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-wide v0, p1, Lcom/twitter/library/api/TwitterUser;->userId:J

    iput-wide v0, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    iget-object v0, p1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/library/api/TwitterUser;)V
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/UserQueryActivity;->d:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/api/TwitterUser;->d()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/UserQueryActivity;->d:Z

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    iget-wide v2, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/UserQueryActivity;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected g()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/ze;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/ze;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    const/4 v2, 0x1

    invoke-direct {v0, p0, v1, p0, v2}, Lcom/twitter/android/ze;-><init>(Landroid/content/Context;Landroid/support/v4/app/LoaderManager;Lcom/twitter/android/zf;I)V

    iput-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/ze;

    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/ze;

    iget-wide v1, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    iget-object v3, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ze;->a(JLjava/lang/String;J)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/UserQueryActivity;->a:Lcom/twitter/android/ze;

    iget-wide v1, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    iget-object v3, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/ze;->b(JLjava/lang/String;J)V

    goto :goto_0
.end method

.method protected h()Lcom/twitter/library/client/j;
    .locals 2

    new-instance v0, Lcom/twitter/android/zl;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/zl;-><init>(Lcom/twitter/android/UserQueryActivity;Lcom/twitter/android/zk;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/ListFragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/UserQueryActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/UserQueryActivity;->e:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/ListFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "user_id"

    iget-wide v1, p0, Lcom/twitter/android/UserQueryActivity;->b:J

    invoke-virtual {p1, v0, v1, v2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string/jumbo v0, "username"

    iget-object v1, p0, Lcom/twitter/android/UserQueryActivity;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
