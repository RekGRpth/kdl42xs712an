.class Lcom/twitter/android/me;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/widget/SlidingPaneLayout$PanelSlideListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onPanelClosed(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/MessagesActivity;->a(Lcom/twitter/android/MessagesActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/MessagesActivity;->a(Lcom/twitter/android/MessagesActivity;)V

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesActivity;->e()V

    return-void
.end method

.method public onPanelOpened(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/MessagesActivity;->a(Lcom/twitter/android/MessagesActivity;Ljava/lang/Integer;)Ljava/lang/Integer;

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/MessagesActivity;->a(Lcom/twitter/android/MessagesActivity;)V

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesActivity;->e()V

    iget-object v0, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/me;->a:Lcom/twitter/android/MessagesActivity;

    invoke-static {v1}, Lcom/twitter/android/MessagesActivity;->b(Lcom/twitter/android/MessagesActivity;)Landroid/support/v4/widget/SlidingPaneLayout;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    return-void
.end method

.method public onPanelSlide(Landroid/view/View;F)V
    .locals 0

    return-void
.end method
