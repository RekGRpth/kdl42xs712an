.class Lcom/twitter/android/ap;
.super Landroid/widget/Filter;
.source "Twttr"


# instance fields
.field final synthetic a:Landroid/app/Activity;

.field final synthetic b:Lcom/twitter/android/ao;


# direct methods
.method constructor <init>(Lcom/twitter/android/ao;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    iput-object p2, p0, Lcom/twitter/android/ap;->a:Landroid/app/Activity;

    invoke-direct {p0}, Landroid/widget/Filter;-><init>()V

    return-void
.end method


# virtual methods
.method public convertResultToString(Ljava/lang/Object;)Ljava/lang/CharSequence;
    .locals 1

    instance-of v0, p1, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    check-cast p1, Landroid/database/Cursor;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ao;->convertToString(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0
.end method

.method protected performFiltering(Ljava/lang/CharSequence;)Landroid/widget/Filter$FilterResults;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ao;->a(Ljava/lang/CharSequence;)Landroid/database/Cursor;

    move-result-object v0

    new-instance v1, Landroid/widget/Filter$FilterResults;

    invoke-direct {v1}, Landroid/widget/Filter$FilterResults;-><init>()V

    iput-object v0, v1, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->getCount()I

    move-result v0

    :goto_0
    iput v0, v1, Landroid/widget/Filter$FilterResults;->count:I

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected publishResults(Ljava/lang/CharSequence;Landroid/widget/Filter$FilterResults;)V
    .locals 2

    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    invoke-virtual {v0}, Lcom/twitter/android/ao;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ap;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->stopManagingCursor(Landroid/database/Cursor;)V

    :cond_0
    iget-object v0, p2, Landroid/widget/Filter$FilterResults;->values:Ljava/lang/Object;

    check-cast v0, Landroid/database/Cursor;

    iget-object v1, p0, Lcom/twitter/android/ap;->a:Landroid/app/Activity;

    invoke-virtual {v1, v0}, Landroid/app/Activity;->startManagingCursor(Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    invoke-virtual {v1, v0}, Lcom/twitter/android/ao;->changeCursor(Landroid/database/Cursor;)V

    iget-object v1, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    invoke-static {v1}, Lcom/twitter/android/ao;->a(Lcom/twitter/android/ao;)Lcom/twitter/android/aq;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ap;->b:Lcom/twitter/android/ao;

    invoke-static {v1}, Lcom/twitter/android/ao;->a(Lcom/twitter/android/ao;)Lcom/twitter/android/aq;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Lcom/twitter/android/aq;->a(Ljava/lang/CharSequence;Landroid/database/Cursor;)V

    :cond_1
    return-void
.end method
