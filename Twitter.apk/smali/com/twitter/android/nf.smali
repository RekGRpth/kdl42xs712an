.class public Lcom/twitter/android/nf;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Landroid/util/SparseArray;

.field private static b:Landroid/util/SparseArray;

.field private static c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/twitter/android/nf;->a:Landroid/util/SparseArray;

    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/twitter/android/nf;->b:Landroid/util/SparseArray;

    const/4 v0, 0x0

    sput v0, Lcom/twitter/android/nf;->c:I

    return-void
.end method

.method private static a(Landroid/content/res/Resources;)I
    .locals 1

    sget v0, Lcom/twitter/android/nf;->c:I

    if-nez v0, :cond_0

    const v0, 0x7f0201d9    # com.twitter.android.R.drawable.ic_nearby_map_account

    invoke-static {p0, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    sput v0, Lcom/twitter/android/nf;->c:I

    :cond_0
    sget v0, Lcom/twitter/android/nf;->c:I

    return v0
.end method

.method private static a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;
    .locals 2

    sget-object v0, Lcom/twitter/android/nf;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/nf;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v1, 0x0

    add-int v3, p1, p2

    sget-object v0, Lcom/twitter/android/nf;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :pswitch_1
    const v0, 0x7f0201d9    # com.twitter.android.R.drawable.ic_nearby_map_account

    move v2, v0

    :goto_1
    packed-switch p2, :pswitch_data_1

    move-object v0, v1

    goto :goto_0

    :pswitch_2
    const v0, 0x7f0201da    # com.twitter.android.R.drawable.ic_nearby_map_event

    move v2, v0

    goto :goto_1

    :pswitch_3
    const v0, 0x7f0201db    # com.twitter.android.R.drawable.ic_nearby_map_photos

    move v2, v0

    goto :goto_1

    :pswitch_4
    const v0, 0x7f0201dd    # com.twitter.android.R.drawable.ic_nearby_pin_single

    :goto_2
    invoke-static {p0, v2, v0}, Lcom/twitter/android/nf;->b(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/nf;->a:Landroid/util/SparseArray;

    invoke-virtual {v1, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    :pswitch_5
    const v0, 0x7f0201de    # com.twitter.android.R.drawable.ic_nearby_pin_stacked

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public static a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 2

    invoke-static {p0}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;)I

    move-result v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return-object p1

    :cond_0
    const/4 v1, 0x0

    invoke-static {p1, v0, v0, v1}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object p1

    goto :goto_0
.end method

.method public static a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;I)Landroid/graphics/Bitmap;
    .locals 1

    packed-switch p2, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    const v0, 0x7f0201dd    # com.twitter.android.R.drawable.ic_nearby_pin_single

    :goto_1
    invoke-static {p0, v0}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, p1, v0}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    const v0, 0x7f0201de    # com.twitter.android.R.drawable.ic_nearby_pin_stacked

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8

    const/4 v4, 0x0

    const/4 v7, 0x0

    invoke-static {p0}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;)I

    move-result v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getConfig()Landroid/graphics/Bitmap$Config;

    move-result-object v3

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, p2, v4, v4, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    new-instance v3, Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v5

    sub-int/2addr v5, v0

    invoke-virtual {p2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    invoke-direct {v3, v4, v5, v0, v6}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2, p1, v7, v3, v7}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    return-object v1
.end method

.method private static b(Landroid/content/res/Resources;II)Landroid/graphics/Bitmap;
    .locals 2

    invoke-static {p0, p2}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-static {p0, p1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {p0, v1, v0}, Lcom/twitter/android/nf;->a(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
