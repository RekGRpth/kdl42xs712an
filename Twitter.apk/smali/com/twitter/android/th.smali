.class public Lcom/twitter/android/th;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:I

.field private c:Landroid/view/View;

.field private d:I


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/th;->a:Landroid/content/Context;

    iput p2, p0, Lcom/twitter/android/th;->b:I

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/ListView;)V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/th;->a:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/AbsListView$LayoutParams;

    const/4 v2, -0x1

    iget v3, p0, Lcom/twitter/android/th;->b:I

    invoke-direct {v1, v2, v3}, Landroid/widget/AbsListView$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p1}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getPaddingTop()I

    move-result v2

    iput v2, p0, Lcom/twitter/android/th;->d:I

    invoke-virtual {v1}, Landroid/view/View;->getPaddingLeft()I

    move-result v2

    iget v3, p0, Lcom/twitter/android/th;->d:I

    iget v4, p0, Lcom/twitter/android/th;->b:I

    add-int/2addr v3, v4

    invoke-virtual {v1}, Landroid/view/View;->getPaddingRight()I

    move-result v4

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    instance-of v1, p1, Lcom/twitter/refresh/widget/RefreshableListView;

    if-eqz v1, :cond_1

    check-cast p1, Lcom/twitter/refresh/widget/RefreshableListView;

    const/4 v1, 0x0

    invoke-virtual {p1, v6, v0, v1, v6}, Lcom/twitter/refresh/widget/RefreshableListView;->a(ILandroid/view/View;Ljava/lang/Object;Z)V

    const/4 v1, 0x1

    invoke-virtual {p1, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshHeaderPosition(I)V

    :cond_1
    iput-object v0, p0, Lcom/twitter/android/th;->c:Landroid/view/View;

    return-void
.end method

.method public a(Landroid/widget/ListView;I)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/th;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v0, p0, Lcom/twitter/android/th;->c:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestLayout()V

    invoke-virtual {p1}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->getPaddingLeft()I

    move-result v1

    iget v2, p0, Lcom/twitter/android/th;->d:I

    add-int/2addr v2, p2

    invoke-virtual {v0}, Landroid/view/View;->getPaddingRight()I

    move-result v3

    invoke-virtual {v0}, Landroid/view/View;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/view/View;->setPadding(IIII)V

    :cond_0
    return-void
.end method

.method public b(Landroid/widget/ListView;)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/th;->b:I

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/th;->a(Landroid/widget/ListView;I)V

    return-void
.end method
