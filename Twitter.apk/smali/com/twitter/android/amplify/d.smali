.class public Lcom/twitter/android/amplify/d;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method static a(Lcom/twitter/library/client/Session;Lcom/twitter/library/provider/Tweet;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->v()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->m()Lcom/twitter/library/api/PromotedContent;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->F()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1, p0}, Lcom/twitter/android/amplify/d;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/Session;)Z
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-virtual {p0}, Lcom/twitter/library/provider/Tweet;->g()J

    move-result-wide v2

    cmp-long v2, v2, v0

    if-eqz v2, :cond_0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/library/provider/Tweet;->a(J)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
