.class public Lcom/twitter/android/zw;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/widget/ListView;J)Z
    .locals 7

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/widget/ListView;->getChildCount()I

    move-result v0

    add-int/lit8 v4, v0, -0x1

    move v3, v1

    :goto_0
    if-gt v3, v4, :cond_2

    invoke-virtual {p0, v3}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v2, 0x0

    instance-of v5, v0, Lcom/twitter/library/widget/UserView;

    if-eqz v5, :cond_0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    :goto_1
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iget-wide v5, v0, Lcom/twitter/android/zx;->c:J

    cmp-long v0, v5, p1

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_2
    return v0

    :cond_0
    instance-of v5, v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v5, :cond_3

    check-cast v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v5, v0, Lcom/twitter/library/widget/UserView;

    if-eqz v5, :cond_3

    check-cast v0, Lcom/twitter/library/widget/UserView;

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_2

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method
