.class public Lcom/twitter/android/dx;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# static fields
.field private static a:Ljava/util/Map;

.field private static b:Ljava/util/Map;


# instance fields
.field private final c:Lcom/twitter/android/client/c;

.field private final d:Ljava/util/List;

.field private final e:Lcom/twitter/library/client/aa;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/dx;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/dx;->b:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/client/c;)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x2

    invoke-direct {p0, p1, v0, v1}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/dx;->d:Ljava/util/List;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/dx;->e:Lcom/twitter/library/client/aa;

    iput-object p2, p0, Lcom/twitter/android/dx;->c:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/dx;->c:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/android/dy;

    invoke-direct {v1, p0}, Lcom/twitter/android/dy;-><init>(Lcom/twitter/android/dx;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/dx;)Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/dx;->e:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method static synthetic a()Ljava/util/Map;
    .locals 1

    sget-object v0, Lcom/twitter/android/dx;->b:Ljava/util/Map;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/dx;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/dx;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/dx;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/dx;->c:Lcom/twitter/android/client/c;

    return-object v0
.end method


# virtual methods
.method public a(Ljava/util/HashMap;Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/dx;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/dx;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/eb;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/dx;->d:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/eb;->h:Ljava/lang/String;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/android/eb;->a(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 8

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/eb;

    new-instance v2, Lcom/twitter/android/ea;

    invoke-direct {v2, p0, p3}, Lcom/twitter/android/ea;-><init>(Lcom/twitter/android/dx;Landroid/database/Cursor;)V

    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-wide v3, v2, Lcom/twitter/android/ea;->b:J

    iget-object v5, v2, Lcom/twitter/android/ea;->c:Ljava/lang/String;

    invoke-virtual {v0, v3, v4, v5}, Lcom/twitter/android/eb;->a(JLjava/lang/String;)V

    iget-object v3, v0, Lcom/twitter/android/eb;->b:Landroid/widget/ImageView;

    new-instance v4, Lcom/twitter/android/dz;

    invoke-direct {v4, p0, p2, v2}, Lcom/twitter/android/dz;-><init>(Lcom/twitter/android/dx;Landroid/content/Context;Lcom/twitter/android/ea;)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, v0, Lcom/twitter/android/eb;->d:Landroid/widget/TextView;

    iget-wide v4, v2, Lcom/twitter/android/ea;->g:J

    invoke-static {v1, v4, v5}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, v2, Lcom/twitter/android/ea;->e:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/android/eb;->a(Ljava/lang/String;)V

    iget-object v3, v2, Lcom/twitter/android/ea;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Lcom/twitter/android/eb;->b(Ljava/lang/String;)V

    iget-wide v3, v2, Lcom/twitter/android/ea;->b:J

    invoke-virtual {v0, v3, v4}, Lcom/twitter/android/eb;->a(J)V

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v7, v1}, Lcom/twitter/android/eb;->a(ZLandroid/content/res/Resources;)V

    sget-object v1, Lcom/twitter/android/dx;->b:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/android/eb;->j:Lcom/twitter/library/util/m;

    invoke-interface {v1, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v6, v0, Lcom/twitter/android/eb;->j:Lcom/twitter/library/util/m;

    iget-object v1, v2, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    if-eqz v1, :cond_1

    sget-object v1, Lcom/twitter/android/dx;->a:Ljava/util/Map;

    iget-object v3, v2, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v3, v3, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/dx;->a:Ljava/util/Map;

    iget-object v3, v2, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v3, v3, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    invoke-interface {v1, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/m;

    iput-object v1, v0, Lcom/twitter/android/eb;->j:Lcom/twitter/library/util/m;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/dx;->c:Lcom/twitter/android/client/c;

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v3, v0, Lcom/twitter/android/eb;->j:Lcom/twitter/library/util/m;

    invoke-interface {v1, v3}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v3, v0, Lcom/twitter/android/eb;->i:Landroid/widget/ImageView;

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, v0, Lcom/twitter/android/eb;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    sget-object v1, Lcom/twitter/android/dx;->b:Ljava/util/Map;

    iget-object v3, v0, Lcom/twitter/android/eb;->j:Lcom/twitter/library/util/m;

    iget-object v4, v0, Lcom/twitter/android/eb;->i:Landroid/widget/ImageView;

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_1
    iget-boolean v1, v2, Lcom/twitter/android/ea;->k:Z

    if-eqz v1, :cond_2

    iget-object v1, v0, Lcom/twitter/android/eb;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/dx;->mContext:Landroid/content/Context;

    const v4, 0x7f1000f2    # com.twitter.android.R.style.TextItalic

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    :goto_2
    iget-object v1, v2, Lcom/twitter/android/ea;->j:Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/dx;->mContext:Landroid/content/Context;

    iget-boolean v2, v2, Lcom/twitter/android/ea;->h:Z

    invoke-virtual {v0, v1, v3, v2}, Lcom/twitter/android/eb;->a(Ljava/lang/String;Landroid/content/Context;Z)V

    return-void

    :cond_0
    new-instance v1, Lcom/twitter/library/util/m;

    iget-object v3, v2, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    iget-object v3, v3, Lcom/twitter/library/api/conversations/DMPhoto;->mediaUrl:Ljava/lang/String;

    iget-object v4, v2, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    iget v4, v4, Lcom/twitter/library/api/conversations/DMPhoto;->width:I

    iget-object v5, v2, Lcom/twitter/android/ea;->i:Lcom/twitter/library/api/conversations/DMPhoto;

    iget v5, v5, Lcom/twitter/library/api/conversations/DMPhoto;->height:I

    const/4 v6, 0x1

    invoke-direct {v1, v3, v4, v5, v6}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;IIZ)V

    iput-object v1, v0, Lcom/twitter/android/eb;->j:Lcom/twitter/library/util/m;

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/eb;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, v0, Lcom/twitter/android/eb;->i:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/eb;->c:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/twitter/android/dx;->mContext:Landroid/content/Context;

    const v4, 0x7f1000f3    # com.twitter.android.R.style.TextNormal

    invoke-virtual {v1, v3, v4}, Landroid/widget/TextView;->setTextAppearance(Landroid/content/Context;I)V

    goto :goto_2
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    new-instance v1, Lcom/twitter/android/ea;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/ea;-><init>(Lcom/twitter/android/dx;Landroid/database/Cursor;)V

    new-instance v0, Landroid/content/Intent;

    iget-object v2, p0, Lcom/twitter/android/dx;->mContext:Landroid/content/Context;

    const-class v3, Lcom/twitter/android/DMConversationActivity;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "conversation_id"

    iget-object v1, v1, Lcom/twitter/android/ea;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c2    # com.twitter.android.R.layout.message_row_view

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/eb;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/eb;-><init>(Lcom/twitter/android/dx;Lcom/twitter/android/dy;)V

    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/eb;->b:Landroid/widget/ImageView;

    const v0, 0x7f0901e6    # com.twitter.android.R.id.message_image

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/eb;->i:Landroid/widget/ImageView;

    const v0, 0x7f0901e5    # com.twitter.android.R.id.preview

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/eb;->c:Landroid/widget/TextView;

    const v0, 0x7f0900b6    # com.twitter.android.R.id.timestamp

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/eb;->d:Landroid/widget/TextView;

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/eb;->e:Landroid/widget/TextView;

    const v0, 0x7f090098    # com.twitter.android.R.id.username

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/eb;->f:Landroid/widget/TextView;

    move-object v0, v1

    check-cast v0, Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    iput-object v0, v2, Lcom/twitter/android/eb;->a:Lcom/twitter/internal/android/widget/HighlightedRelativeLayout;

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/twitter/android/dx;->d:Ljava/util/List;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object v1
.end method
