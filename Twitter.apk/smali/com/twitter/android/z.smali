.class public Lcom/twitter/android/z;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lhe;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/Context;Landroid/net/Uri;)Lhb;
    .locals 3

    invoke-static {p1}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Context;)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "activity_type"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v1, Lhd;

    const-class v2, Lcom/twitter/android/ActivityFragment;

    invoke-direct {v1, p2, v2}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v1, 0x7f0f000d    # com.twitter.android.R.string.activity

    invoke-virtual {p1, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const v1, 0x7f0201e6    # com.twitter.android.R.drawable.ic_perch_activity_default

    invoke-virtual {v0, v1}, Lhd;->a(I)Lhd;

    move-result-object v0

    const-string/jumbo v1, "network_activity"

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhd;->b(Z)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    return-object v0
.end method
