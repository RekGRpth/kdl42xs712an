.class public Lcom/twitter/android/LoginChallengeActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field a:Ljava/lang/String;

.field private final b:Lcom/twitter/android/jj;

.field private c:I

.field private d:F

.field private e:I

.field private f:Landroid/os/Handler;

.field private g:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

.field private h:Ljava/lang/String;

.field private i:Ljava/lang/String;

.field private j:Lcom/twitter/library/client/Session;

.field private k:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/jj;

    invoke-direct {v0, p0}, Lcom/twitter/android/jj;-><init>(Lcom/twitter/android/LoginChallengeActivity;)V

    iput-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->b:Lcom/twitter/android/jj;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/LoginChallengeActivity;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/LoginChallengeActivity;->e:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/LoginChallengeActivity;Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/LoginChallengeActivity;->k:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/network/LoginVerificationRequiredResponse;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->g:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    return-object v0
.end method

.method static synthetic f(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/jj;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->b:Lcom/twitter/android/jj;

    return-object v0
.end method

.method private f()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login_challenge::::cancel"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    sget-object v1, Lcom/twitter/library/client/Session$LoginStatus;->a:Lcom/twitter/library/client/Session$LoginStatus;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/client/Session$LoginStatus;)V

    :cond_0
    return-void
.end method

.method static synthetic g(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private g()V
    .locals 4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->B()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/LoginChallengeActivity;->c:I

    iget v0, p0, Lcom/twitter/android/LoginChallengeActivity;->c:I

    if-nez v0, :cond_0

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/LoginChallengeActivity;->c:I

    :cond_0
    iget v0, p0, Lcom/twitter/android/LoginChallengeActivity;->c:I

    int-to-long v0, v0

    const-wide/16 v2, 0x3e8

    mul-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/twitter/android/LoginChallengeActivity;->c:I

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->C()F

    move-result v0

    iput v0, p0, Lcom/twitter/android/LoginChallengeActivity;->d:F

    iget v0, p0, Lcom/twitter/android/LoginChallengeActivity;->d:F

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_1

    const v0, 0x3f99999a    # 1.2f

    iput v0, p0, Lcom/twitter/android/LoginChallengeActivity;->d:F

    :cond_1
    return-void
.end method

.method static synthetic h(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic l(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic m(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic n(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic o(Lcom/twitter/android/LoginChallengeActivity;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/LoginChallengeActivity;->e:I

    return v0
.end method

.method static synthetic p(Lcom/twitter/android/LoginChallengeActivity;)F
    .locals 1

    iget v0, p0, Lcom/twitter/android/LoginChallengeActivity;->d:F

    return v0
.end method

.method static synthetic q(Lcom/twitter/android/LoginChallengeActivity;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->f:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 3

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f030185    # com.twitter.android.R.layout.webview_layout

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->a(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->c(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/z;->b(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-direct {p0}, Lcom/twitter/android/LoginChallengeActivity;->g()V

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "login_challenge::::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v0, "login_challenge_required_response"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iput-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->g:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    const-string/jumbo v0, "username"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->h:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->g:Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    iget-object v2, v0, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->d:Ljava/lang/String;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->f:Landroid/os/Handler;

    const v0, 0x7f0902d7    # com.twitter.android.R.id.webview

    invoke-virtual {p0, v0}, Lcom/twitter/android/LoginChallengeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/webkit/WebView;

    invoke-virtual {v0}, Landroid/webkit/WebView;->getSettings()Landroid/webkit/WebSettings;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/webkit/WebSettings;->setSaveFormData(Z)V

    invoke-virtual {v3, v6}, Landroid/webkit/WebSettings;->setJavaScriptEnabled(Z)V

    invoke-virtual {v3, v5}, Landroid/webkit/WebSettings;->setAllowFileAccess(Z)V

    new-instance v3, Lcom/twitter/android/jh;

    invoke-direct {v3, p0}, Lcom/twitter/android/jh;-><init>(Lcom/twitter/android/LoginChallengeActivity;)V

    invoke-virtual {v0, v3}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    if-eqz p1, :cond_2

    const-string/jumbo v1, "url"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->k:Ljava/lang/String;

    const-string/jumbo v1, "reqId"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/LoginChallengeActivity;->a:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/LoginChallengeActivity;->b:Lcom/twitter/android/jj;

    invoke-virtual {v1, v3, v4}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;Lcom/twitter/library/client/ah;)V

    const-string/jumbo v1, "session_id"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->i:Ljava/lang/String;

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->k:Ljava/lang/String;

    if-nez v1, :cond_1

    iput-object v2, p0, Lcom/twitter/android/LoginChallengeActivity;->k:Ljava/lang/String;

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->k:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    return-void

    :cond_2
    const-string/jumbo v3, "session_id"

    invoke-virtual {v1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->i:Ljava/lang/String;

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/LoginChallengeActivity;->f()V

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    return v0
.end method

.method public onBackPressed()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/LoginChallengeActivity;->f()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->e(Ljava/lang/String;)V

    return-void
.end method

.method public onResume()V
    .locals 4

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/LoginChallengeActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->i:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->h:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->j:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->i:Ljava/lang/String;

    :cond_0
    iget v0, p0, Lcom/twitter/android/LoginChallengeActivity;->c:I

    iput v0, p0, Lcom/twitter/android/LoginChallengeActivity;->e:I

    iget-object v0, p0, Lcom/twitter/android/LoginChallengeActivity;->f:Landroid/os/Handler;

    new-instance v1, Lcom/twitter/android/ji;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/ji;-><init>(Lcom/twitter/android/LoginChallengeActivity;Lcom/twitter/android/jh;)V

    iget v2, p0, Lcom/twitter/android/LoginChallengeActivity;->e:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "url"

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "reqId"

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "session_id"

    iget-object v1, p0, Lcom/twitter/android/LoginChallengeActivity;->i:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    return-void
.end method
