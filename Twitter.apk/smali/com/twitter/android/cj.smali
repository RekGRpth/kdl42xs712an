.class Lcom/twitter/android/cj;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ChangeEmailActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ChangeEmailActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/cj;->a:Lcom/twitter/android/ChangeEmailActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public d(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    const v0, 0x7f0f03c9    # com.twitter.android.R.string.settings_change_email_success

    iget-object v1, p0, Lcom/twitter/android/cj;->a:Lcom/twitter/android/ChangeEmailActivity;

    invoke-static {v1}, Lcom/twitter/android/ChangeEmailActivity;->a(Lcom/twitter/android/ChangeEmailActivity;)Landroid/widget/EditText;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/twitter/android/cj;->a:Lcom/twitter/android/ChangeEmailActivity;

    invoke-static {v1}, Lcom/twitter/android/ChangeEmailActivity;->b(Lcom/twitter/android/ChangeEmailActivity;)Landroid/widget/EditText;

    move-result-object v1

    const-string/jumbo v2, ""

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/cj;->a:Lcom/twitter/android/ChangeEmailActivity;

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void

    :cond_0
    const v0, 0x7f0f03c8    # com.twitter.android.R.string.settings_change_email_error

    goto :goto_0
.end method
