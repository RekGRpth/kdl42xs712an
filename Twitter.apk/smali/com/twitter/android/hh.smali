.class Lcom/twitter/android/hh;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/hd;


# direct methods
.method private constructor <init>(Lcom/twitter/android/hd;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hh;->a:Lcom/twitter/android/hd;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/hd;Lcom/twitter/android/gt;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/hh;-><init>(Lcom/twitter/android/hd;)V

    return-void
.end method


# virtual methods
.method public onPreDraw()Z
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/hh;->a:Lcom/twitter/android/hd;

    invoke-static {v0}, Lcom/twitter/android/hd;->b(Lcom/twitter/android/hd;)Lcom/twitter/library/widget/SlidingPanel;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/hh;->a:Lcom/twitter/android/hd;

    invoke-static {v1}, Lcom/twitter/android/hd;->c(Lcom/twitter/android/hd;)Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/hh;->a:Lcom/twitter/android/hd;

    iget-object v2, v2, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-virtual {v2}, Lcom/twitter/android/GalleryActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getHeight()I

    move-result v2

    int-to-double v2, v2

    const-wide v4, 0x3fe6666666666666L    # 0.7

    mul-double/2addr v2, v4

    double-to-int v2, v2

    const v3, 0x7f0901e1    # com.twitter.android.R.id.media_tags_list_header

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/SlidingPanel;->findViewById(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v3}, Landroid/view/View;->getHeight()I

    move-result v3

    invoke-virtual {v1}, Landroid/widget/ListView;->getChildCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-virtual {v1, v4}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->getBottom()I

    move-result v1

    add-int/2addr v1, v3

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SlidingPanel;->setPanelPreviewHeight(I)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->e()Z

    iget-object v1, p0, Lcom/twitter/android/hh;->a:Lcom/twitter/android/hd;

    iget-object v1, v1, Lcom/twitter/android/hd;->b:Lcom/twitter/android/GalleryActivity;

    invoke-static {v1, v6}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;Z)Z

    invoke-virtual {v0}, Lcom/twitter/library/widget/SlidingPanel;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return v6
.end method
