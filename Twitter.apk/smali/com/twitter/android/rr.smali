.class Lcom/twitter/android/rr;
.super Landroid/text/style/ClickableSpan;
.source "Twttr"


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/android/ReportTweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ReportTweetActivity;II)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/rr;->c:Lcom/twitter/android/ReportTweetActivity;

    iput p2, p0, Lcom/twitter/android/rr;->a:I

    iput p3, p0, Lcom/twitter/android/rr;->b:I

    invoke-direct {p0}, Landroid/text/style/ClickableSpan;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/rr;->c:Lcom/twitter/android/ReportTweetActivity;

    iget v1, p0, Lcom/twitter/android/rr;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/ReportTweetActivity;->a(I)V

    return-void
.end method

.method public updateDrawState(Landroid/text/TextPaint;)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/rr;->a:I

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setColor(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/TextPaint;->setFakeBoldText(Z)V

    const v0, 0x106000d    # android.R.color.transparent

    iput v0, p1, Landroid/text/TextPaint;->bgColor:I

    return-void
.end method
