.class public Lcom/twitter/android/ab;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Ljava/util/HashSet;

.field private final b:Lcom/twitter/library/scribe/ScribeAssociation;

.field private final c:Ljava/util/ArrayList;

.field private final d:Lcom/twitter/android/client/c;

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ab;->a:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ab;->c:Ljava/util/ArrayList;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ab;->d:Lcom/twitter/android/client/c;

    iput-object p1, p0, Lcom/twitter/android/ab;->e:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/ab;->b:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method


# virtual methods
.method public a(JLjava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ab;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ab;->d:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ab;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/ab;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 8

    const-wide/16 v6, -0x1

    const/4 v5, 0x0

    const-string/jumbo v0, "activity_row_id"

    invoke-virtual {p1, v0, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    const-string/jumbo v0, "tweet"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/ab;->a:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v1, :cond_0

    iget-boolean v0, v1, Lcom/twitter/library/provider/Tweet;->M:Z

    if-nez v0, :cond_0

    iget-object v0, v1, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ab;->d:Lcom/twitter/android/client/c;

    sget-object v3, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v2, v3, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    const-string/jumbo v0, "user"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterUser;

    const-string/jumbo v0, "list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/provider/ActivityDataList;

    const-string/jumbo v0, "event_type"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v4

    const-string/jumbo v0, "position"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/lit8 v5, v0, 0x1

    const-string/jumbo v0, "magic_rec_id"

    invoke-virtual {p1, v0, v6, v7}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v6

    move-object v0, p0

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/android/ab;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;IIJ)V

    :cond_1
    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/TwitterUser;Lcom/twitter/library/provider/ActivityDataList;IIJ)V
    .locals 7

    if-eqz p1, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "focal"

    :goto_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->b()J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/ab;->e:Landroid/content/Context;

    iget-object v4, p0, Lcom/twitter/android/ab;->b:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-static {v3, p1, v4, v0}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    iget-object v3, p1, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    iput-object v3, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    move-wide v5, v1

    move-object v2, v0

    move-wide v0, v5

    :goto_1
    packed-switch p4, :pswitch_data_0

    :goto_2
    iput-wide v0, v2, Lcom/twitter/library/scribe/ScribeItem;->a:J

    sget-object v0, Lcom/twitter/library/provider/x;->b:Ljava/util/HashMap;

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v2, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    iput p5, v2, Lcom/twitter/library/scribe/ScribeItem;->g:I

    iget-object v0, p0, Lcom/twitter/android/ab;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "ancestor"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    new-instance v2, Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {v2}, Lcom/twitter/library/scribe/ScribeItem;-><init>()V

    if-eqz p2, :cond_3

    invoke-virtual {p2}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v0

    goto :goto_1

    :cond_3
    if-eqz p3, :cond_4

    iget-wide v0, p3, Lcom/twitter/library/provider/ActivityDataList;->id:J

    goto :goto_1

    :cond_4
    const-wide/16 v0, -0x1

    goto :goto_1

    :pswitch_0
    const-string/jumbo v3, "magic_rec_tweet"

    iput-object v3, v2, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v3, 0x0

    iput v3, v2, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    goto :goto_2

    :pswitch_1
    const-string/jumbo v3, "magic_rec_user"

    iput-object v3, v2, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const/4 v3, 0x3

    iput v3, v2, Lcom/twitter/library/scribe/ScribeItem;->c:I

    invoke-static {p6, p7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/twitter/library/scribe/ScribeItem;->z:Ljava/lang/String;

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x12
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
