.class public Lcom/twitter/android/DMInboxFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field a:Lcom/twitter/android/eh;

.field private final b:Ljava/util/List;

.field private c:Ljava/lang/String;

.field private d:Landroid/widget/ListView;

.field private e:Ljava/lang/Boolean;

.field private f:Lcom/twitter/android/el;

.field private g:Landroid/view/View;

.field private h:Landroid/widget/LinearLayout;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->b:Ljava/util/List;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->e:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/DMInboxFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;
    .locals 3

    new-instance v0, Lcom/twitter/android/zx;

    invoke-direct {v0, p1}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/DMInboxFragment;->b:Ljava/util/List;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-object p1
.end method

.method private a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->b:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/zx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic b(Lcom/twitter/android/DMInboxFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->r()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/DMInboxFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->s()V

    return-void
.end method

.method static synthetic d(Lcom/twitter/android/DMInboxFragment;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->h:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private e()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->q()V

    new-instance v0, Lcom/twitter/android/el;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/el;-><init>(Landroid/app/Activity;Lcom/twitter/library/client/Session;)V

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->f:Lcom/twitter/android/el;

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->f:Lcom/twitter/android/el;

    invoke-virtual {v0}, Lcom/twitter/android/el;->start()V

    return-void
.end method

.method private e(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->ap()V

    return-void
.end method

.method static synthetic f(Lcom/twitter/android/DMInboxFragment;)I
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->u()I

    move-result v0

    return v0
.end method

.method static synthetic g(Lcom/twitter/android/DMInboxFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private q()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->f:Lcom/twitter/android/el;

    invoke-static {v0}, Lcom/twitter/internal/util/h;->a(Ljava/io/Closeable;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->f:Lcom/twitter/android/el;

    return-void
.end method

.method private r()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    const v1, 0x7f090126    # com.twitter.android.R.id.request_toggle_container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method private s()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/api/conversations/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    const v1, 0x7f090126    # com.twitter.android.R.id.request_toggle_container

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->r()V

    goto :goto_0
.end method

.method private u()I
    .locals 6

    const/high16 v5, 0x40000000    # 2.0f

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->h:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getWidth()I

    move-result v0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0080    # com.twitter.android.R.dimen.list_row_padding

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    mul-float/2addr v1, v5

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c009c    # com.twitter.android.R.dimen.mini_user_image_size

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f0c00b9    # com.twitter.android.R.dimen.request_avatar_padding

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    mul-float/2addr v3, v5

    add-float/2addr v2, v3

    int-to-float v0, v0

    sub-float/2addr v0, v1

    div-float/2addr v0, v2

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->floor(D)D

    move-result-wide v0

    double-to-int v0, v0

    return v0
.end method


# virtual methods
.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 3

    const v1, 0x7f0f0288    # com.twitter.android.R.string.messages_fetch_error

    const/4 v2, 0x1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    invoke-virtual {p4}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    packed-switch p2, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    const/16 v1, 0x194

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0, p3}, Lcom/twitter/android/DMInboxFragment;->b(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f0287    # com.twitter.android.R.string.messages_delete_error

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, p3}, Lcom/twitter/android/DMInboxFragment;->b(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p3}, Lcom/twitter/android/DMInboxFragment;->b(I)V

    check-cast p4, Lcom/twitter/library/api/conversations/s;

    invoke-interface {p4}, Lcom/twitter/library/api/conversations/s;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/DMInboxFragment;->i:Z

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 7

    const/4 v6, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->c:Ljava/lang/String;

    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox:thread::delete"

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/api/conversations/af;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/DMInboxFragment;->c:Ljava/lang/String;

    invoke-direct {v1, v2, v0, v3}, Lcom/twitter/library/api/conversations/af;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0, v6}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->as()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/DMInboxFragment;->i:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxFragment;->a(I)Z

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    const/4 v1, 0x3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->a(I)Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/DMInboxFragment;->e(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->b(I)V

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/DMInboxFragment;->e(Z)V

    goto :goto_0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    const v0, 0x7f090323    # com.twitter.android.R.id.dm_mark_as_read

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DMInboxFragment;->e:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/dx;

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/dx;->a(Ljava/util/HashMap;Z)V

    :cond_0
    invoke-direct {p0, p2}, Lcom/twitter/android/DMInboxFragment;->a(Ljava/util/HashMap;)V

    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method protected a(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->P()Z

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lcom/twitter/android/eg;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/eg;-><init>(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/android/ec;)V

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->p()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->a(I)Z

    goto :goto_0

    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxFragment;->b(I)V

    goto :goto_0
.end method

.method protected a(I)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    new-array v1, v5, [Ljava/lang/Integer;

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/twitter/library/util/CollectionsUtil;->a([Ljava/lang/Object;)Ljava/util/Set;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Lcom/twitter/library/api/conversations/ac;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/conversations/ac;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v2, 0x7

    invoke-virtual {p0, v1, v2, p1}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/library/service/b;II)Z

    :goto_0
    invoke-static {}, Lcom/twitter/library/api/conversations/ae;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/library/api/conversations/z;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/conversations/z;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v0, 0x6

    invoke-virtual {p0, v1, v0, v3}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/library/service/b;II)Z

    :cond_0
    invoke-virtual {p0, p1}, Lcom/twitter/android/DMInboxFragment;->a_(I)V

    return v4

    :cond_1
    new-instance v1, Lcom/twitter/library/api/conversations/n;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/library/api/conversations/n;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    const/4 v2, 0x4

    invoke-virtual {p0, v1, v2, p1}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/library/service/b;II)Z

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxFragment;->a(Z)V

    return-void
.end method

.method public c()V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_2

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/DMInboxFragment;->Q:J

    const-string/jumbo v4, "messages::::impression"

    const-string/jumbo v5, "ref_event"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    invoke-virtual {v0}, Lcom/twitter/android/eh;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/eg;

    invoke-direct {v1, p0, v6}, Lcom/twitter/android/eg;-><init>(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/android/ec;)V

    invoke-virtual {v0, v7, v6, v1}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    invoke-static {}, Lcom/twitter/library/api/conversations/ae;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x5

    new-instance v2, Lcom/twitter/android/ee;

    invoke-direct {v2, p0, v6}, Lcom/twitter/android/ee;-><init>(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/android/ec;)V

    invoke-virtual {v0, v1, v6, v2}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/DMInboxFragment;->Q:J

    new-array v3, v7, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages::::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected g()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->g()V

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/DMInboxFragment;->e(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxFragment;->a(I)Z

    return-void
.end method

.method protected j()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method protected k()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->k()V

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    if-nez p1, :cond_0

    const/4 v1, 0x3

    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->a(I)Z

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v1, :cond_1

    new-instance v1, Lcom/twitter/android/dx;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/twitter/android/dx;-><init>(Landroid/content/Context;Lcom/twitter/android/client/c;)V

    iput-object v1, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/twitter/android/DMInboxFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    if-nez v0, :cond_2

    new-instance v0, Lcom/twitter/android/eh;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/eh;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->d:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v0, Lcom/twitter/library/api/conversations/n;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/library/api/conversations/n;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0}, Lcom/twitter/library/api/conversations/n;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/DMInboxFragment;->i:Z

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    const v5, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v5, :cond_0

    const v1, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/DialogActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "ff"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    if-ne v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox::import:click"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/DMInboxFragment;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox:user_list:import:click"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    const/4 v0, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/DMInboxFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/DMInboxFragment;->l(Z)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/ac;->a:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    invoke-static {v2, v5, v6}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/api/conversations/o;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const/4 v6, 0x0

    const/4 v5, 0x0

    const v0, 0x7f030051    # com.twitter.android.R.layout.dm_inbox_list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/DMInboxFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v2, 0x7f030053    # com.twitter.android.R.layout.dm_request_toggle

    invoke-static {v0, v2, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    new-instance v2, Lcom/twitter/android/ec;

    invoke-direct {v2, p0}, Lcom/twitter/android/ec;-><init>(Lcom/twitter/android/DMInboxFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    const v2, 0x7f090129    # com.twitter.android.R.id.request_avatars

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->h:Landroid/widget/LinearLayout;

    const v0, 0x7f090046    # com.twitter.android.R.id.list_empty_text

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v2, Lcom/twitter/android/ed;

    invoke-direct {v2, p0}, Lcom/twitter/android/ed;-><init>(Lcom/twitter/android/DMInboxFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f0300c7    # com.twitter.android.R.layout.messages_inbox_header

    invoke-static {v2, v3, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2, v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f03004f    # com.twitter.android.R.layout.dm_inbox_footer

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3, v5, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->r()V

    iget-object v2, p0, Lcom/twitter/android/DMInboxFragment;->g:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->d:Landroid/widget/ListView;

    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/DMInboxFragment;->a:Lcom/twitter/android/eh;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/eh;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/DMInboxFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0285    # com.twitter.android.R.string.messages_delete_conversation

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0286    # com.twitter.android.R.string.messages_delete_conversation_question

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    const-string/jumbo v2, "conversation_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/DMInboxFragment;->c:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-virtual {v1, p0, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    const/4 v0, 0x1

    return v0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/DMInboxFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onStart()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStart()V

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->e()V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/DMInboxFragment;->q()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    return-void
.end method
