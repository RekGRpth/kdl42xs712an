.class public Lcom/twitter/android/oi;
.super Landroid/widget/BaseAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/av;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/twitter/library/util/aa;

.field private final c:F

.field private final d:I

.field private final e:I

.field private final f:Landroid/view/View$OnClickListener;

.field private final g:Lcom/twitter/android/ob;

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/ArrayList;

.field private final j:Ljava/util/ArrayList;

.field private final k:Ljava/util/HashMap;

.field private final l:Lcom/twitter/library/client/aa;

.field private m:Z

.field private n:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/util/aa;FIILandroid/view/View$OnClickListener;Lcom/twitter/android/ob;)V
    .locals 1

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/oi;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/oi;->j:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/oi;->k:Ljava/util/HashMap;

    iput-object p1, p0, Lcom/twitter/android/oi;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/oi;->l:Lcom/twitter/library/client/aa;

    iput-object p2, p0, Lcom/twitter/android/oi;->b:Lcom/twitter/library/util/aa;

    iput p3, p0, Lcom/twitter/android/oi;->c:F

    iput p4, p0, Lcom/twitter/android/oi;->d:I

    iput p5, p0, Lcom/twitter/android/oi;->e:I

    iput-object p6, p0, Lcom/twitter/android/oi;->f:Landroid/view/View$OnClickListener;

    iput-object p7, p0, Lcom/twitter/android/oi;->g:Lcom/twitter/android/ob;

    return-void
.end method

.method private a(I)V
    .locals 12

    const/4 v11, 0x0

    const/high16 v10, 0x3f000000    # 0.5f

    const/4 v3, 0x0

    const/4 v9, 0x0

    iget-object v5, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    iget-object v6, p0, Lcom/twitter/android/oi;->h:Ljava/util/ArrayList;

    iget v7, p0, Lcom/twitter/android/oi;->c:F

    new-instance v1, Lcom/twitter/android/ol;

    invoke-direct {v1, v9}, Lcom/twitter/android/ol;-><init>(Lcom/twitter/android/oj;)V

    move v4, p1

    move v2, v3

    :goto_0
    if-ltz v4, :cond_1

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget v8, v0, Lcom/twitter/android/ok;->e:F

    add-float/2addr v2, v8

    iget-object v8, v1, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_0

    add-float v8, v2, v10

    cmpl-float v8, v8, v7

    if-ltz v8, :cond_0

    iput v2, v1, Lcom/twitter/android/ol;->b:F

    invoke-virtual {v5, v11, v1}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    new-instance v1, Lcom/twitter/android/ol;

    invoke-direct {v1, v9}, Lcom/twitter/android/ol;-><init>(Lcom/twitter/android/oj;)V

    iget v2, v0, Lcom/twitter/android/ok;->e:F

    :cond_0
    iget-object v8, v1, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v8, v11, v0}, Ljava/util/ArrayList;->add(ILjava/lang/Object;)V

    add-int/lit8 v0, v4, -0x1

    move v4, v0

    goto :goto_0

    :cond_1
    new-instance v1, Lcom/twitter/android/ol;

    invoke-direct {v1, v9}, Lcom/twitter/android/ol;-><init>(Lcom/twitter/android/oj;)V

    add-int/lit8 v0, p1, 0x1

    move-object v2, v1

    move v1, v0

    :goto_1
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    invoke-virtual {v6, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget v4, v0, Lcom/twitter/android/ok;->e:F

    add-float/2addr v3, v4

    iget-object v4, v2, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_2

    add-float v4, v3, v10

    cmpl-float v4, v4, v7

    if-ltz v4, :cond_2

    iput v3, v2, Lcom/twitter/android/ol;->b:F

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/twitter/android/ol;

    invoke-direct {v2, v9}, Lcom/twitter/android/ol;-><init>(Lcom/twitter/android/oj;)V

    iget v3, v0, Lcom/twitter/android/ok;->e:F

    :cond_2
    iget-object v4, v2, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, v2, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    iput v3, v2, Lcom/twitter/android/ol;->b:F

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_4
    return-void
.end method

.method private d()J
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/twitter/android/oi;->e:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ol;

    iget-object v0, v0, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-wide v0, v0, Lcom/twitter/android/ok;->a:J

    goto :goto_0
.end method

.method private e()V
    .locals 12

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/twitter/android/oi;->n:Landroid/database/Cursor;

    iget-object v3, p0, Lcom/twitter/android/oi;->h:Ljava/util/ArrayList;

    invoke-direct {p0}, Lcom/twitter/android/oi;->d()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    if-eqz v1, :cond_3

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    iget v6, p0, Lcom/twitter/android/oi;->d:I

    :cond_0
    invoke-interface {v1, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    new-instance v9, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v9, v1}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {v9}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v9, v2, v2}, Lcom/twitter/library/util/s;->a(Lcom/twitter/library/provider/Tweet;II)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    new-instance v11, Lcom/twitter/android/ok;

    invoke-direct {v11, v7, v8, v9, v0}, Lcom/twitter/android/ok;-><init>(JLcom/twitter/library/provider/Tweet;Lcom/twitter/library/api/MediaEntity;)V

    invoke-virtual {v3, v11}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v1, -0x1

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-wide v6, v0, Lcom/twitter/android/ok;->a:J

    cmp-long v0, v6, v4

    if-nez v0, :cond_4

    add-int/lit8 v0, v2, -0x1

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_1

    :cond_2
    invoke-direct {p0, v1}, Lcom/twitter/android/oi;->a(I)V

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/oi;->notifyDataSetChanged()V

    return-void

    :cond_4
    move v0, v1

    goto :goto_2
.end method

.method private f()V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/oi;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/oi;->l:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/oi;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v3, v0

    :goto_1
    if-ltz v3, :cond_4

    iget-object v0, p0, Lcom/twitter/android/oi;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/oi;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_1
    :goto_2
    add-int/lit8 v0, v3, -0x1

    move v3, v0

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ok;

    iget-object v2, p0, Lcom/twitter/android/oi;->k:Ljava/util/HashMap;

    iget-object v6, v1, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {v2, v6}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/oi;->b:Lcom/twitter/library/util/aa;

    iget-object v6, v1, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {v2, v4, v5, v6}, Lcom/twitter/library/util/aa;->b(JLcom/twitter/library/util/m;)Lcom/twitter/library/util/ae;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/twitter/library/util/ae;->b()Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v2, v2, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v2, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/oi;->k:Ljava/util/HashMap;

    iget-object v1, v1, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/oi;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    goto :goto_0
.end method


# virtual methods
.method public a(J)I
    .locals 7

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ol;

    iget-object v0, v0, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-wide v5, v0, Lcom/twitter/android/ok;->a:J

    cmp-long v0, v5, p1

    if-nez v0, :cond_0

    :goto_1
    return v1

    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public a()Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/oi;->n:Landroid/database/Cursor;

    return-object v0
.end method

.method public a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/oi;->n:Landroid/database/Cursor;

    iput-object p1, p0, Lcom/twitter/android/oi;->n:Landroid/database/Cursor;

    invoke-direct {p0}, Lcom/twitter/android/oi;->e()V

    return-object v0
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/oi;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-nez v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ok;

    iget-object v1, v1, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/oi;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/twitter/android/oi;->e:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-wide v0, v0, Lcom/twitter/android/ok;->a:J

    goto :goto_0
.end method

.method public c()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/oi;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, p0, Lcom/twitter/android/oi;->e:I

    int-to-long v0, v0

    :goto_0
    return-wide v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-wide v0, v0, Lcom/twitter/android/ok;->a:J

    goto :goto_0
.end method

.method public c(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/oi;->m:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/oi;->m:Z

    iget-boolean v0, p0, Lcom/twitter/android/oi;->m:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/oi;->f()V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, -0x8000000000000000L

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ol;

    iget-object v0, v0, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    iget-wide v0, v0, Lcom/twitter/android/ok;->a:J

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 11

    iget-object v0, p0, Lcom/twitter/android/oi;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v4

    if-nez p2, :cond_1

    const v0, 0x7f0300e2    # com.twitter.android.R.layout.photo_page_row

    const/4 v1, 0x0

    invoke-virtual {v4, v0, p3, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    move-object p2, v0

    :goto_0
    iget-object v5, p0, Lcom/twitter/android/oi;->b:Lcom/twitter/library/util/aa;

    iget-object v6, p0, Lcom/twitter/android/oi;->f:Landroid/view/View$OnClickListener;

    iget-object v7, p0, Lcom/twitter/android/oi;->g:Lcom/twitter/android/ob;

    iget-object v0, p0, Lcom/twitter/android/oi;->i:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ol;

    const/4 v1, 0x0

    iget-object v0, v0, Lcom/twitter/android/ol;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ok;

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_2

    check-cast v1, Landroid/widget/ImageView;

    move-object v3, v1

    :goto_2
    invoke-virtual {v3}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/android/ok;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ok;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-wide v9, v0, Lcom/twitter/android/ok;->a:J

    invoke-static {v9, v10}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v1, p0, Lcom/twitter/android/oi;->m:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/oi;->l:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v9

    iget-object v1, v0, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    invoke-virtual {v5, v9, v10, v1}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_3
    const/4 v1, 0x0

    sget-object v9, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-interface {v7, v3, v1, v9}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_0
    invoke-virtual {v3}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout$LayoutParams;

    iget v0, v0, Lcom/twitter/android/ok;->e:F

    iput v0, v1, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    check-cast p2, Landroid/widget/LinearLayout;

    goto :goto_0

    :cond_2
    const v1, 0x7f0300e8    # com.twitter.android.R.layout.photo_row_item

    const/4 v3, 0x0

    invoke-virtual {v4, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v6}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/twitter/android/oi;->j:Ljava/util/ArrayList;

    new-instance v9, Ljava/lang/ref/WeakReference;

    invoke-direct {v9, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p2, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    move-object v3, v1

    goto :goto_2

    :cond_3
    const/4 v1, 0x0

    invoke-virtual {v3, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/twitter/android/oi;->k:Ljava/util/HashMap;

    iget-object v9, v0, Lcom/twitter/android/ok;->d:Lcom/twitter/library/util/m;

    const/4 v10, 0x0

    invoke-static {v10}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v10

    invoke-virtual {v1, v9, v10}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    :cond_4
    invoke-virtual {p2}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v1

    :goto_4
    if-ge v2, v1, :cond_5

    invoke-virtual {p2, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_5
    return-object p2
.end method
