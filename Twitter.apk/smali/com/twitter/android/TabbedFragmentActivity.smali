.class public abstract Lcom/twitter/android/TabbedFragmentActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/widget/TabHost$OnTabChangeListener;


# instance fields
.field protected a:I

.field b:Lcom/twitter/internal/android/widget/IconTabHost;

.field c:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

.field d:Landroid/support/v4/view/ViewPager;

.field e:Lcom/twitter/android/TabsAdapter;

.field f:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(I)Landroid/support/v4/app/Fragment;
    .locals 2

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->e:Lcom/twitter/android/TabsAdapter;

    invoke-virtual {v0}, Lcom/twitter/android/TabsAdapter;->getCount()I

    move-result v0

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->e:Lcom/twitter/android/TabsAdapter;

    invoke-virtual {v0, p1}, Lcom/twitter/android/TabsAdapter;->a(I)Lcom/twitter/android/ub;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ub;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03014f    # com.twitter.android.R.layout.tabbed_fragment_activity

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->c(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 3

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->h()V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->o()V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->k()V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->p()V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0079    # com.twitter.android.R.dimen.home_pager_margin

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f0b0063    # com.twitter.android.R.color.list_margin_bg

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFragmentActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->f:Landroid/content/SharedPreferences;

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/IconTabHost;->setOnTabChangedListener(Landroid/widget/TabHost$OnTabChangeListener;)V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->m()V

    return-void
.end method

.method a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0}, Landroid/widget/TabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0, p1}, Landroid/widget/TabHost;->setCurrentTabByTag(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->c:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    iget-object v1, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ViewPagerScrollBar;->setPosition(I)V

    :cond_0
    return-void
.end method

.method protected h()V
    .locals 2

    const v0, 0x7f090055    # com.twitter.android.R.id.scrollbar

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    iput-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->c:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->c:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "You must define a ViewPagerScrollBar with id R.id.scrollbar"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected k()V
    .locals 2

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "You must define a ViewPager with id R.id.pager"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected l()V
    .locals 2

    const v0, 0x1020012    # android.R.id.tabhost

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFragmentActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/IconTabHost;

    iput-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "You must define a tabhost with id android.R.id.tabhost"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected m()V
    .locals 4

    new-instance v0, Lcom/twitter/android/TabsAdapter;

    iget-object v1, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    iget-object v2, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v3, p0, Lcom/twitter/android/TabbedFragmentActivity;->c:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/TabsAdapter;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;Landroid/widget/TabHost;Landroid/support/v4/view/ViewPager;Lcom/twitter/internal/android/widget/ViewPagerScrollBar;)V

    iput-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->e:Lcom/twitter/android/TabsAdapter;

    return-void
.end method

.method protected n()Landroid/support/v4/app/Fragment;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->e:Lcom/twitter/android/TabsAdapter;

    iget-object v1, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/TabsAdapter;->a(I)Lcom/twitter/android/ub;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/ub;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method protected o()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->c:Lcom/twitter/internal/android/widget/ViewPagerScrollBar;

    iget v1, p0, Lcom/twitter/android/TabbedFragmentActivity;->a:I

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ViewPagerScrollBar;->setRange(I)V

    return-void
.end method

.method public onContentChanged()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onContentChanged()V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->l()V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/IconTabHost;->setup()V

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFragmentActivity;->k()V

    return-void
.end method

.method protected onPause()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "tag"

    iget-object v2, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v2}, Lcom/twitter/internal/android/widget/IconTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/IconTabHost;->getCurrentTab()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/IconTabHost;->setCurrentTab(I)V

    :cond_0
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "currentTab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFragmentActivity;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/IconTabHost;->getCurrentTabTag()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "currentTab"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    iget-object v1, p0, Lcom/twitter/android/TabbedFragmentActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v1}, Lcom/twitter/internal/android/widget/IconTabHost;->getCurrentTab()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    return-void
.end method

.method protected p()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TabbedFragmentActivity;->d:Landroid/support/v4/view/ViewPager;

    iget v1, p0, Lcom/twitter/android/TabbedFragmentActivity;->a:I

    add-int/lit8 v1, v1, -0x1

    div-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    return-void
.end method
