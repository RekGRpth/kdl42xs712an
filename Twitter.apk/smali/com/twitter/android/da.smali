.class public Lcom/twitter/android/da;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;


# instance fields
.field private final a:Lcom/twitter/android/db;


# direct methods
.method public constructor <init>(Lcom/twitter/android/db;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/da;->a:Lcom/twitter/android/db;

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/da;->a:Lcom/twitter/android/db;

    invoke-interface {v0}, Lcom/twitter/android/db;->b()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/da;->a:Lcom/twitter/android/db;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/twitter/android/db;->a(I)V

    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/da;->a:Lcom/twitter/android/db;

    invoke-interface {v0}, Lcom/twitter/android/db;->a()Landroid/view/animation/Animation;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/da;->a:Lcom/twitter/android/db;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/twitter/android/db;->a(I)V

    :cond_0
    return-void
.end method
