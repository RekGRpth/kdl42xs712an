.class public Lcom/twitter/android/WidgetSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private a:I

.field private b:Ljava/lang/String;

.field private c:I

.field private d:Lcom/twitter/android/widget/RadioButtonPreference;

.field private e:Lcom/twitter/android/widget/RadioButtonPreference;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    iput v1, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    iput v1, p0, Lcom/twitter/android/WidgetSettingsActivity;->c:I

    return-void
.end method

.method private a()V
    .locals 9

    const/4 v2, 0x0

    invoke-static {p0}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v3

    iget v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    invoke-virtual {v3, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetInfo(I)Landroid/appwidget/AppWidgetProviderInfo;

    move-result-object v0

    iget v4, v0, Landroid/appwidget/AppWidgetProviderInfo;->initialLayout:I

    const v0, 0x7f03018a    # com.twitter.android.R.layout.widget_clear_small_view

    if-ne v4, v0, :cond_1

    const v0, 0x7f060005    # com.twitter.android.R.xml.appwidget_small_provider

    move v1, v2

    :goto_0
    iget-object v5, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    :cond_0
    iget-object v5, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    invoke-static {p0}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v6

    iget v7, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    iget v8, p0, Lcom/twitter/android/WidgetSettingsActivity;->c:I

    invoke-virtual {v6, v7, v1, v5, v8}, Lcom/twitter/library/provider/f;->a(IILjava/lang/String;I)I

    new-instance v1, Landroid/widget/RemoteViews;

    invoke-virtual {p0}, Lcom/twitter/android/WidgetSettingsActivity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v1, v6, v4}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    iget v4, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    invoke-virtual {v3, v4, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v3, "appWidgetId"

    iget v4, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const/4 v3, -0x1

    invoke-virtual {p0, v3, v1}, Lcom/twitter/android/WidgetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1, v5}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Z)V

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/WidgetSettingsActivity;->finish()V

    return-void

    :cond_1
    const/4 v1, 0x1

    const v0, 0x7f060004    # com.twitter.android.R.xml.appwidget_large_provider

    goto :goto_0

    :cond_2
    invoke-static {p0, v0}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;I)V

    goto :goto_1
.end method

.method private static a(Landroid/preference/ListPreference;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p0}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p0, p1}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 10

    const v9, 0x7f0f0430    # com.twitter.android.R.string.settings_widget_account_title

    const/4 v3, -0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    array-length v0, p1

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/WidgetSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    array-length v5, p1

    move v1, v2

    move v0, v3

    :goto_1
    if-ge v1, v5, :cond_3

    aget-object v6, p1, v1

    sget-object v7, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    iget-object v8, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    iget-object v7, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v4, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-ne v0, v3, :cond_2

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v3, 0x1

    if-le v1, v3, :cond_0

    if-ltz v0, :cond_0

    new-instance v3, Landroid/preference/ListPreference;

    invoke-direct {v3, p0}, Landroid/preference/ListPreference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v5, "widget_account"

    invoke-virtual {v3, v5}, Landroid/preference/ListPreference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Landroid/preference/ListPreference;->setDialogTitle(I)V

    invoke-virtual {v3, v2}, Landroid/preference/ListPreference;->setPersistent(Z)V

    new-array v1, v1, [Ljava/lang/CharSequence;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {v3, v1}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v1}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setValueIndex(I)V

    iget-object v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setTitle(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    invoke-virtual {v3, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    invoke-virtual {v3, p0}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v9}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setOrder(I)V

    invoke-virtual {p0}, Lcom/twitter/android/WidgetSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    invoke-virtual {v0, v3}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f0901e2    # com.twitter.android.R.id.done

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/WidgetSettingsActivity;->a()V

    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030190    # com.twitter.android.R.layout.widget_settings

    invoke-virtual {p0, v0}, Lcom/twitter/android/WidgetSettingsActivity;->setContentView(I)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/WidgetSettingsActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/WidgetSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    :cond_0
    iget v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->a:I

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/WidgetSettingsActivity;->finish()V

    :cond_1
    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/WidgetSettingsActivity;->a()V

    :goto_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    return-void

    :cond_2
    const v0, 0x7f060019    # com.twitter.android.R.xml.widget_preferences

    invoke-virtual {p0, v0}, Lcom/twitter/android/WidgetSettingsActivity;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "widget_content_tweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/WidgetSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/RadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/RadioButtonPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iput-object v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->d:Lcom/twitter/android/widget/RadioButtonPreference;

    const-string/jumbo v0, "widget_content_mentions"

    invoke-virtual {p0, v0}, Lcom/twitter/android/WidgetSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/RadioButtonPreference;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/RadioButtonPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iput-object v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->e:Lcom/twitter/android/widget/RadioButtonPreference;

    const v0, 0x7f0901e2    # com.twitter.android.R.id.done

    invoke-virtual {p0, v0}, Lcom/twitter/android/WidgetSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onDestroy()V

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "widget_account"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    check-cast p2, Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    check-cast p1, Landroid/preference/ListPreference;

    iget-object v1, p0, Lcom/twitter/android/WidgetSettingsActivity;->b:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/twitter/android/WidgetSettingsActivity;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v3, "widget_content_tweets"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    iput v1, p0, Lcom/twitter/android/WidgetSettingsActivity;->c:I

    iget-object v2, p0, Lcom/twitter/android/WidgetSettingsActivity;->e:Lcom/twitter/android/widget/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "widget_content_mentions"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iput v0, p0, Lcom/twitter/android/WidgetSettingsActivity;->c:I

    iget-object v2, p0, Lcom/twitter/android/WidgetSettingsActivity;->d:Lcom/twitter/android/widget/RadioButtonPreference;

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/RadioButtonPreference;->setChecked(Z)V

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public onSearchRequested()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
