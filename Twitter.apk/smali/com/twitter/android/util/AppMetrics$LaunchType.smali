.class final enum Lcom/twitter/android/util/AppMetrics$LaunchType;
.super Ljava/lang/Enum;
.source "Twttr"


# static fields
.field public static final enum a:Lcom/twitter/android/util/AppMetrics$LaunchType;

.field public static final enum b:Lcom/twitter/android/util/AppMetrics$LaunchType;

.field public static final enum c:Lcom/twitter/android/util/AppMetrics$LaunchType;

.field private static final synthetic d:[Lcom/twitter/android/util/AppMetrics$LaunchType;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/twitter/android/util/AppMetrics$LaunchType;

    const-string/jumbo v1, "NOT_LAUNCHED_OR_LOGGED_IN"

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/util/AppMetrics$LaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    new-instance v0, Lcom/twitter/android/util/AppMetrics$LaunchType;

    const-string/jumbo v1, "FIRST_LAUNCH_TO_HOME_TIMELINE"

    invoke-direct {v0, v1, v3}, Lcom/twitter/android/util/AppMetrics$LaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    new-instance v0, Lcom/twitter/android/util/AppMetrics$LaunchType;

    const-string/jumbo v1, "LAUNCHED_TO_HOME_TIMELINE"

    invoke-direct {v0, v1, v4}, Lcom/twitter/android/util/AppMetrics$LaunchType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->c:Lcom/twitter/android/util/AppMetrics$LaunchType;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/twitter/android/util/AppMetrics$LaunchType;

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->a:Lcom/twitter/android/util/AppMetrics$LaunchType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->b:Lcom/twitter/android/util/AppMetrics$LaunchType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/twitter/android/util/AppMetrics$LaunchType;->c:Lcom/twitter/android/util/AppMetrics$LaunchType;

    aput-object v1, v0, v4

    sput-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->d:[Lcom/twitter/android/util/AppMetrics$LaunchType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/twitter/android/util/AppMetrics$LaunchType;
    .locals 1

    const-class v0, Lcom/twitter/android/util/AppMetrics$LaunchType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/util/AppMetrics$LaunchType;

    return-object v0
.end method

.method public static values()[Lcom/twitter/android/util/AppMetrics$LaunchType;
    .locals 1

    sget-object v0, Lcom/twitter/android/util/AppMetrics$LaunchType;->d:[Lcom/twitter/android/util/AppMetrics$LaunchType;

    invoke-virtual {v0}, [Lcom/twitter/android/util/AppMetrics$LaunchType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/util/AppMetrics$LaunchType;

    return-object v0
.end method
