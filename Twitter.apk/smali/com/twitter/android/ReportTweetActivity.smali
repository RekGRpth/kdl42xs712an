.class public Lcom/twitter/android/ReportTweetActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:[I

.field private static final b:[I


# instance fields
.field private c:Landroid/widget/CheckBox;

.field private d:Landroid/widget/Button;

.field private e:Landroid/widget/TextView;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Ljava/lang/Long;

.field private i:Ljava/lang/Long;

.field private j:Ljava/lang/String;

.field private k:Z

.field private l:I

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;

.field private o:Lcom/twitter/library/scribe/ScribeAssociation;

.field private p:Z

.field private q:Z

.field private r:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/ReportTweetActivity;->a:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/android/ReportTweetActivity;->b:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f09023f    # com.twitter.android.R.id.annoying_button
        0x7f090242    # com.twitter.android.R.id.spam_button
        0x7f090249    # com.twitter.android.R.id.abuse_button
        0x7f090245    # com.twitter.android.R.id.compromised_button
    .end array-data

    :array_1
    .array-data 4
        0x7f09023d    # com.twitter.android.R.id.annoying_section
        0x7f090240    # com.twitter.android.R.id.spam_section
        0x7f090243    # com.twitter.android.R.id.compromised_section
        0x7f090246    # com.twitter.android.R.id.abuse_section
        0x7f090239    # com.twitter.android.R.id.block_section
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    invoke-static {}, Lcom/twitter/android/util/af;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;
    .locals 4

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->q:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "profile::report_user"

    :goto_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    const/4 v0, 0x1

    aput-object p1, v2, v0

    const/4 v0, 0x2

    aput-object p2, v2, v0

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->o:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, "tweet::report_tweet"

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->q:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->h:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/twitter/android/ReportTweetActivity;->k:Z

    move-object v5, p1

    move v6, p2

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->i:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    iget-object v3, p0, Lcom/twitter/android/ReportTweetActivity;->h:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    iget-object v5, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/twitter/android/ReportTweetActivity;->k:Z

    move-object v7, p1

    move v8, p2

    invoke-virtual/range {v0 .. v8}, Lcom/twitter/android/client/c;->a(JJLjava/lang/String;ZLjava/lang/String;Z)Ljava/lang/String;

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    new-instance v0, Lcom/twitter/library/api/PromotedContent;

    invoke-direct {v0}, Lcom/twitter/library/api/PromotedContent;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/api/PromotedContent;->impressionId:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/twitter/android/ReportTweetActivity;->k:Z

    if-eqz v1, :cond_3

    const-string/jumbo v1, "earned"

    iput-object v1, v0, Lcom/twitter/library/api/PromotedContent;->disclosureType:Ljava/lang/String;

    :cond_3
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->h:Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->c(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private a(Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "should_block"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/ReportTweetActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->finish()V

    return-void
.end method

.method private b(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/widget/RadioButton;->setChecked(Z)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->g()V

    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/ReportTweetActivity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method private c(I)Landroid/text/style/ClickableSpan;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0060    # com.twitter.android.R.color.link

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    new-instance v1, Lcom/twitter/android/rr;

    invoke-direct {v1, p0, v0, p1}, Lcom/twitter/android/rr;-><init>(Lcom/twitter/android/ReportTweetActivity;II)V

    return-object v1
.end method

.method private f()I
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    invoke-virtual {v0}, Landroid/widget/RadioButton;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private g()V
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->f()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_0
    const v1, 0x7f090249    # com.twitter.android.R.id.abuse_button

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    const v1, 0x7f0f029c    # com.twitter.android.R.string.next

    invoke-virtual {p0, v1}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    const v1, 0x7f0f0482    # com.twitter.android.R.string.submit

    invoke-virtual {p0, v1}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method private h()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->f:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    const-string/jumbo v1, "abuse"

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/ReportTweetActivity;->a(Ljava/lang/String;Z)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->n:Ljava/lang/String;

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/ReportTweetActivity;->startActivity(Landroid/content/Intent;)V

    invoke-direct {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->a(Z)V

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AbuseActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "spammer_id"

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->h:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "spammer_username"

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "status_id"

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->i:Ljava/lang/Long;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pc_impression_id"

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "pc_earned"

    iget-boolean v2, p0, Lcom/twitter/android/ReportTweetActivity;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "mute_enabled"

    iget-boolean v2, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "should_block"

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "association"

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->o:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ReportTweetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method private k()V
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const v1, 0x7f0f0008    # com.twitter.android.R.string.abusive_title

    invoke-virtual {p0, v1}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ReportTweetActivity;->m:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/text/SpannableString;

    invoke-direct {v2, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v3, Landroid/text/style/ForegroundColorSpan;

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b0072    # com.twitter.android.R.color.red

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    invoke-direct {v3, v4}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v0, v1, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->e:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0300ff    # com.twitter.android.R.layout.report_tweet_fragment

    :goto_0
    new-instance v1, Lcom/twitter/android/client/z;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->a(Z)V

    const/16 v0, 0xc

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->c(I)V

    return-object v1

    :cond_0
    const v0, 0x7f030100    # com.twitter.android.R.layout.report_tweet_fragment_legacy

    goto :goto_0
.end method

.method a(I)V
    .locals 2

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const-string/jumbo v0, "twitter_rules_link"

    const-string/jumbo v1, "open_link"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ReportTweetActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0f0503    # com.twitter.android.R.string.twitter_rules_url

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "learn_more_link"

    const-string/jumbo v1, "open_link"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ReportTweetActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    const v0, 0x7f0f0203    # com.twitter.android.R.string.learn_more_about_blocking

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Landroid/net/Uri;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 11

    const v10, 0x7f0f035c    # com.twitter.android.R.string.report_tweet_as_abusive_summary

    const/16 v9, 0x22

    const-wide/16 v6, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->getIntent()Landroid/content/Intent;

    move-result-object v5

    const-string/jumbo v0, "status_url"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->f:Ljava/lang/String;

    const-string/jumbo v0, "spammer_id"

    invoke-virtual {v5, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->h:Ljava/lang/Long;

    const-string/jumbo v0, "status_id"

    invoke-virtual {v5, v0, v6, v7}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->i:Ljava/lang/Long;

    const-string/jumbo v0, "pc_impression_id"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    const-string/jumbo v0, "pc_earned"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->k:Z

    const-string/jumbo v0, "association"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeAssociation;

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->o:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v0, "friendship"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/ReportTweetActivity;->l:I

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->i:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    cmp-long v0, v3, v6

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->q:Z

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    sget-object v4, Lcom/twitter/android/ReportTweetActivity;->a:[I

    array-length v6, v4

    move v3, v2

    :goto_1
    if-ge v3, v6, :cond_2

    aget v7, v4, v3

    invoke-virtual {p0, v7}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    if-eqz v0, :cond_0

    iget-object v8, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v8, v7, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v0, p0}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    sget-object v3, Lcom/twitter/android/ReportTweetActivity;->b:[I

    array-length v4, v3

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_4

    aget v6, v3, v0

    invoke-virtual {p0, v6}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v6, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_4
    const v0, 0x7f090247    # com.twitter.android.R.id.abuse_label

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->e:Landroid/widget/TextView;

    const-string/jumbo v0, "spammer_username"

    invoke-virtual {v5, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    const-string/jumbo v0, ""

    :cond_5
    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->g:Ljava/lang/String;

    const v0, 0x7f0f035e    # com.twitter.android.R.string.report_tweet_pick_issue

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v0, 0x7f090238    # com.twitter.android.R.id.report_tweet_header

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const v3, 0x7f0f035d    # com.twitter.android.R.string.report_tweet_header

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/android/ReportTweetActivity;->g:Ljava/lang/String;

    aput-object v6, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/ReportTweetActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_8

    const v0, 0x7f0f004e    # com.twitter.android.R.string.block_and_unfollow_summary

    new-array v3, v1, [Ljava/lang/Object;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "@"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v6, p0, Lcom/twitter/android/ReportTweetActivity;->g:Ljava/lang/String;

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/ReportTweetActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v3, p0, Lcom/twitter/android/ReportTweetActivity;->q:Z

    if-eqz v3, :cond_7

    const v3, 0x7f0f0353    # com.twitter.android.R.string.report_account_as_abusive_summary

    invoke-virtual {p0, v3}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move-object v3, v0

    :goto_3
    const v0, 0x7f090248    # com.twitter.android.R.id.abuse_summary

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-array v6, v1, [Ljava/lang/Object;

    invoke-direct {p0, v2}, Lcom/twitter/android/ReportTweetActivity;->c(I)Landroid/text/style/ClickableSpan;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v7

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-static {v6, v4, v9}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09023b    # com.twitter.android.R.id.block_summary

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    new-array v4, v1, [Ljava/lang/Object;

    invoke-direct {p0, v1}, Lcom/twitter/android/ReportTweetActivity;->c(I)Landroid/text/style/ClickableSpan;

    move-result-object v6

    aput-object v6, v4, v2

    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v6

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    invoke-static {v4, v3, v9}, Lcom/twitter/library/util/Util;->a([Ljava/lang/Object;Ljava/lang/String;C)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f09023c    # com.twitter.android.R.id.block_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, p0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_a

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->c()V

    if-eqz p1, :cond_9

    const-string/jumbo v0, "s_abuse_type"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->m:Ljava/lang/String;

    const-string/jumbo v0, "s_abuse_url"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->n:Ljava/lang/String;

    :goto_4
    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    :goto_5
    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_6

    const v0, 0x7f09023a    # com.twitter.android.R.id.block_label

    const v3, 0x7f0f0051    # com.twitter.android.R.string.block_user_name

    new-array v4, v1, [Ljava/lang/Object;

    iget-object v6, p0, Lcom/twitter/android/ReportTweetActivity;->g:Ljava/lang/String;

    aput-object v6, v4, v2

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/ReportTweetActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/ReportTweetActivity;->a(ILjava/lang/String;)V

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->q:Z

    if-eqz v0, :cond_6

    const v0, 0x7f09023e    # com.twitter.android.R.id.annoying_label

    const v3, 0x7f0f002e    # com.twitter.android.R.string.annoying_account_title

    invoke-virtual {p0, v3}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/ReportTweetActivity;->a(ILjava/lang/String;)V

    const v0, 0x7f090241    # com.twitter.android.R.id.spam_label

    const v3, 0x7f0f0475    # com.twitter.android.R.string.spam_account_title

    invoke-virtual {p0, v3}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/ReportTweetActivity;->a(ILjava/lang/String;)V

    :cond_6
    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v3, 0x0

    const-string/jumbo v4, "impression"

    invoke-direct {p0, v3, v4}, Lcom/twitter/android/ReportTweetActivity;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v4, "ref_event"

    invoke-virtual {v5, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->c([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_7
    invoke-virtual {p0, v10}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move-object v3, v0

    goto/16 :goto_3

    :cond_8
    const v0, 0x7f0f004c    # com.twitter.android.R.string.block_account_summary

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v10}, Lcom/twitter/android/ReportTweetActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    move-object v4, v3

    move-object v3, v0

    goto/16 :goto_3

    :cond_9
    const v0, 0x7f09023f    # com.twitter.android.R.id.annoying_button

    invoke-direct {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    goto :goto_4

    :cond_a
    const v0, 0x7f0900ca    # com.twitter.android.R.id.submit

    invoke-virtual {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_5
.end method

.method protected c()V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300fe    # com.twitter.android.R.layout.report_title_bar

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/ReportTweetActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    new-instance v2, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;

    const/4 v3, 0x5

    invoke-direct {v2, v3}, Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;-><init>(I)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(Landroid/view/View;Lcom/twitter/internal/android/widget/ToolBar$LayoutParams;)V

    const v1, 0x7f0900ca    # com.twitter.android.R.id.submit

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->d:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    const/4 v0, -0x1

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_1

    const-string/jumbo v0, "out_abuse_type"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->m:Ljava/lang/String;

    const-string/jumbo v0, "out_abuse_url"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->n:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->n:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    const v0, 0x7f090249    # com.twitter.android.R.id.abuse_button

    invoke-direct {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->b(I)V

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->k()V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 2

    const/4 v0, 0x0

    const-string/jumbo v1, "cancel"

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/ReportTweetActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 8

    const v7, 0x7f090245    # com.twitter.android.R.id.compromised_button

    const v6, 0x7f090242    # com.twitter.android.R.id.spam_button

    const v5, 0x7f09023f    # com.twitter.android.R.id.annoying_button

    const/4 v1, 0x1

    const v4, 0x7f090249    # com.twitter.android.R.id.abuse_button

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const/4 v2, 0x0

    if-eq v0, v5, :cond_0

    const v3, 0x7f09023d    # com.twitter.android.R.id.annoying_section

    if-ne v0, v3, :cond_2

    :cond_0
    invoke-direct {p0, v5}, Lcom/twitter/android/ReportTweetActivity;->b(I)V

    move v0, v1

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v1, :cond_1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->e:Landroid/widget/TextView;

    const v1, 0x7f0f0008    # com.twitter.android.R.string.abusive_title

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void

    :cond_2
    if-eq v0, v6, :cond_3

    const v3, 0x7f090240    # com.twitter.android.R.id.spam_section

    if-ne v0, v3, :cond_4

    :cond_3
    invoke-direct {p0, v6}, Lcom/twitter/android/ReportTweetActivity;->b(I)V

    move v0, v1

    goto :goto_0

    :cond_4
    if-eq v0, v7, :cond_5

    const v3, 0x7f090243    # com.twitter.android.R.id.compromised_section

    if-ne v0, v3, :cond_6

    :cond_5
    invoke-direct {p0, v7}, Lcom/twitter/android/ReportTweetActivity;->b(I)V

    move v0, v1

    goto :goto_0

    :cond_6
    if-eq v0, v4, :cond_7

    const v3, 0x7f090246    # com.twitter.android.R.id.abuse_section

    if-eq v0, v3, :cond_7

    const v3, 0x7f090248    # com.twitter.android.R.id.abuse_summary

    if-ne v0, v3, :cond_a

    :cond_7
    iget-boolean v3, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v3, :cond_9

    if-ne v0, v4, :cond_8

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->r:Ljava/util/HashMap;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/RadioButton;

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/widget/RadioButton;->setChecked(Z)V

    :cond_8
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/AbuseActivity;

    invoke-direct {v0, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "spammer_id"

    iget-object v4, p0, Lcom/twitter/android/ReportTweetActivity;->h:Ljava/lang/Long;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "spammer_username"

    iget-object v4, p0, Lcom/twitter/android/ReportTweetActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "status_id"

    iget-object v4, p0, Lcom/twitter/android/ReportTweetActivity;->i:Ljava/lang/Long;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "pc_impression_id"

    iget-object v4, p0, Lcom/twitter/android/ReportTweetActivity;->j:Ljava/lang/String;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "pc_earned"

    iget-boolean v4, p0, Lcom/twitter/android/ReportTweetActivity;->k:Z

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "mute_enabled"

    iget-boolean v4, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "should_block"

    iget-object v4, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v4}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v4

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v3, "association"

    iget-object v4, p0, Lcom/twitter/android/ReportTweetActivity;->o:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ReportTweetActivity;->startActivityForResult(Landroid/content/Intent;I)V

    move v0, v2

    goto/16 :goto_0

    :cond_9
    invoke-direct {p0, v4}, Lcom/twitter/android/ReportTweetActivity;->b(I)V

    move v0, v2

    goto/16 :goto_0

    :cond_a
    const v1, 0x7f09023c    # com.twitter.android.R.id.block_button

    if-ne v0, v1, :cond_b

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->g()V

    move v0, v2

    goto/16 :goto_0

    :cond_b
    const v1, 0x7f090239    # com.twitter.android.R.id.block_section

    if-eq v0, v1, :cond_c

    const v1, 0x7f09023b    # com.twitter.android.R.id.block_summary

    if-ne v0, v1, :cond_d

    :cond_c
    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->toggle()V

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->g()V

    move v0, v2

    goto/16 :goto_0

    :cond_d
    const v1, 0x7f0900ca    # com.twitter.android.R.id.submit

    if-ne v0, v1, :cond_14

    const-string/jumbo v1, ""

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->f()I

    move-result v3

    if-ne v3, v4, :cond_f

    const-string/jumbo v0, "abuse"

    move-object v1, v0

    :cond_e
    :goto_1
    iget-object v3, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v3}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v3

    if-eqz v3, :cond_12

    const-string/jumbo v3, "block"

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/ReportTweetActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const-string/jumbo v0, "abuse"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->h()V

    move v0, v2

    goto/16 :goto_0

    :cond_f
    if-ne v3, v7, :cond_10

    const-string/jumbo v0, "compromised"

    move-object v1, v0

    goto :goto_1

    :cond_10
    if-ne v3, v6, :cond_11

    const-string/jumbo v0, "spam"

    move-object v1, v0

    goto :goto_1

    :cond_11
    if-ne v3, v5, :cond_e

    const-string/jumbo v0, "annoying"

    goto :goto_1

    :cond_12
    const-string/jumbo v3, "report_as_spam"

    invoke-direct {p0, v0, v3}, Lcom/twitter/android/ReportTweetActivity;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_13
    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->c:Landroid/widget/CheckBox;

    invoke-virtual {v0}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/ReportTweetActivity;->a(Ljava/lang/String;Z)V

    invoke-direct {p0, v0}, Lcom/twitter/android/ReportTweetActivity;->a(Z)V

    :cond_14
    move v0, v2

    goto/16 :goto_0
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->g()V

    iget-boolean v0, p0, Lcom/twitter/android/ReportTweetActivity;->p:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ReportTweetActivity;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->f()I

    move-result v0

    const v1, 0x7f090249    # com.twitter.android.R.id.abuse_button

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/ReportTweetActivity;->k()V

    :cond_0
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "s_abuse_type"

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->m:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "s_abuse_url"

    iget-object v1, p0, Lcom/twitter/android/ReportTweetActivity;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
