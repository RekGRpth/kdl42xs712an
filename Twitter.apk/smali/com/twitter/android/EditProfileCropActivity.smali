.class public Lcom/twitter/android/EditProfileCropActivity;
.super Lcom/twitter/android/CropActivity;
.source "Twttr"


# instance fields
.field private d:Landroid/widget/Button;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CropActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;

    move-result-object v0

    const v1, 0x7f030048    # com.twitter.android.R.layout.crop_photo

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/CropActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f090119    # com.twitter.android.R.id.save_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/EditProfileCropActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/twitter/android/EditProfileCropActivity;->d:Landroid/widget/Button;

    iget-object v0, p0, Lcom/twitter/android/EditProfileCropActivity;->d:Landroid/widget/Button;

    iget-object v1, p0, Lcom/twitter/android/EditProfileCropActivity;->b:Lcom/twitter/android/CropManager;

    invoke-virtual {v1}, Lcom/twitter/android/CropManager;->a()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    return-void
.end method

.method public a(Z)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/CropActivity;->a(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/EditProfileCropActivity;->d:Landroid/widget/Button;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    iget-object v0, p0, Lcom/twitter/android/EditProfileCropActivity;->c:Lcom/twitter/library/widget/CroppableImageView;

    const/high16 v1, 0x3f000000    # 0.5f

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/CroppableImageView;->setCropAspectRatio(F)V

    :cond_0
    return-void
.end method
