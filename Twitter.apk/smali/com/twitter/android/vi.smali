.class Lcom/twitter/android/vi;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TrendLocationsFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TrendLocationsFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/TrendLocationsFragment;->a(Lcom/twitter/android/TrendLocationsFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-static {v1, v0}, Lcom/twitter/android/TrendLocationsFragment;->a(Lcom/twitter/android/TrendLocationsFragment;I)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/twitter/android/TrendLocationsFragment;->b(Lcom/twitter/android/TrendLocationsFragment;I)V

    iget-object v0, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TrendLocationsFragment;->p()V

    new-instance v0, Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "trend_loc_prefs"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "lang"

    invoke-virtual {v0, v1, p5}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "country"

    invoke-virtual {v0, v1, p6}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "last_refresh"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/vi;->a:Lcom/twitter/android/TrendLocationsFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TrendLocationsFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f04be    # com.twitter.android.R.string.trends_loc_fetch_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
