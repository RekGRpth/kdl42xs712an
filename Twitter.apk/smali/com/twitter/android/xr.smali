.class public Lcom/twitter/android/xr;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ah;


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/ArrayList;

.field private final c:Ljava/util/ArrayList;

.field private final d:Ljava/util/HashMap;

.field private final e:Lcom/twitter/android/client/c;

.field private final f:Lcom/twitter/library/client/aa;

.field private final g:Landroid/content/Context;

.field private h:Ljava/lang/String;

.field private i:Lcom/twitter/library/scribe/ScribeAssociation;

.field private j:I

.field private k:J

.field private l:Z

.field private m:Z

.field private n:Lcom/twitter/android/widget/aj;

.field private o:Z

.field private p:Lcom/twitter/library/scribe/ScribeItem;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;ILcom/twitter/library/scribe/ScribeItem;Z)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xr;->a:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xr;->b:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xr;->c:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xr;->d:Ljava/util/HashMap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/xr;->j:I

    iput-boolean v1, p0, Lcom/twitter/android/xr;->l:Z

    iput-boolean v1, p0, Lcom/twitter/android/xr;->m:Z

    iput-boolean v1, p0, Lcom/twitter/android/xr;->o:Z

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/xr;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/xr;->g:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/xr;->i:Lcom/twitter/library/scribe/ScribeAssociation;

    iput p3, p0, Lcom/twitter/android/xr;->j:I

    iput-object p4, p0, Lcom/twitter/android/xr;->p:Lcom/twitter/library/scribe/ScribeItem;

    if-eqz p5, :cond_0

    new-instance v0, Lcom/twitter/android/widget/aj;

    invoke-direct {v0}, Lcom/twitter/android/widget/aj;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/xr;)Lcom/twitter/android/widget/aj;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    return-object v0
.end method

.method private a(Ljava/util/ArrayList;JLjava/lang/String;)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->h()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->g()Lcom/twitter/library/api/b;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/xr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/scribe/ScribeItem;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object p4, v5, v6

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/xr;->p:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v4, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    iget-object v5, p0, Lcom/twitter/android/xr;->h:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v5, "app_download_client_event"

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->g(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    iget-object v0, v0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->d(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v5, "3"

    invoke-virtual {v4, v5, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    const-string/jumbo v0, "4"

    invoke-virtual {v4, v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    :cond_0
    if-eqz v2, :cond_1

    const-string/jumbo v0, "6"

    invoke-virtual {v2}, Lcom/twitter/library/api/b;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v2}, Lcom/twitter/library/api/b;->b()Z

    move-result v0

    invoke-virtual {v4, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Z)Lcom/twitter/library/scribe/ScribeLog;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-virtual {v0, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/xr;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/xr;->o:Z

    return p1
.end method


# virtual methods
.method public a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->K()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "focal"

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->J()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "ancestor"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/aj;->a(Landroid/widget/ListView;)V

    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 6

    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/xr;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/xr;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/xr;->g:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v2

    new-instance v3, Ljp;

    iget-object v4, p0, Lcom/twitter/android/xr;->g:Landroid/content/Context;

    iget v5, p0, Lcom/twitter/android/xr;->j:I

    invoke-direct {v3, v4, v0, v5, v1}, Ljp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/util/Collection;)V

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    goto :goto_0
.end method

.method public a(JLjava/lang/String;)V
    .locals 5

    const/4 v3, 0x1

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/xr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v1, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v3, [Ljava/lang/String;

    aput-object p3, v2, v4

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xr;->p:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xr;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/xr;->h:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/xr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->c:Ljava/util/ArrayList;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/twitter/android/xr;->a(Ljava/util/ArrayList;JLjava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/xr;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/xr;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iget-boolean v0, p0, Lcom/twitter/android/xr;->o:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    new-array v1, v3, [Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/twitter/android/xr;->i:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v3}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, "::tweet:inline_action_labels:impression"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    return-void
.end method

.method public a(JLjava/lang/String;J)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-virtual {v0, p4, p5}, Lcom/twitter/android/widget/aj;->a(J)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v2, p1, p2}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p3, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/xr;->p:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeItem;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/xr;->h:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method

.method public a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 4

    iget-boolean v0, p2, Lcom/twitter/library/provider/Tweet;->M:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-wide v2, p2, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/twitter/android/xr;->a(JJ)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->a:Ljava/util/ArrayList;

    iget-wide v1, p2, Lcom/twitter/library/provider/Tweet;->L:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, p2, p3}, Lcom/twitter/android/xr;->a(Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V

    iget-object v0, p2, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->a:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->O()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/twitter/android/xr;->m:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/widget/aj;->a(Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V

    iget-boolean v0, p0, Lcom/twitter/android/xr;->l:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/aj;->a(J)Ljava/util/ArrayList;

    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/xs;

    invoke-direct {v1, p0, p1, p2}, Lcom/twitter/android/xs;-><init>(Lcom/twitter/android/xr;Landroid/view/View;Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :cond_2
    invoke-virtual {p1}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/xt;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/xt;-><init>(Lcom/twitter/android/xr;Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    return-void
.end method

.method public a(Landroid/widget/ListView;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->Q()F

    move-result v2

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->R()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/widget/aj;->a(FF)V

    iget-object v1, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-virtual {v1, p1}, Lcom/twitter/android/widget/aj;->a(Landroid/widget/ListView;)V

    iget-object v1, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    iget-object v2, p0, Lcom/twitter/android/xr;->i:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/aj;->a(Lcom/twitter/library/scribe/ScribeAssociation;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/aj;->a(J)Ljava/util/ArrayList;

    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/provider/Tweet;Landroid/os/Bundle;)V
    .locals 4

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/xr;->i:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {p0, p1}, Lcom/twitter/android/xr;->a(Lcom/twitter/library/provider/Tweet;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, p1, v1, v2}, Lcom/twitter/library/scribe/ScribeItem;->a(Landroid/content/Context;Lcom/twitter/library/provider/ParcelableTweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v0

    const-string/jumbo v1, "cursor"

    const/4 v2, -0x1

    invoke-virtual {p2, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->h:I

    const-string/jumbo v1, "entity_type"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->x:Ljava/lang/String;

    const-string/jumbo v1, "query"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->w:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/provider/Tweet;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->b:Ljava/lang/String;

    const-string/jumbo v1, "position"

    invoke-virtual {p2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->g:I

    const-string/jumbo v1, "tweet_count"

    const-wide/16 v2, -0x1

    invoke-virtual {p2, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v1

    long-to-int v1, v1

    iput v1, v0, Lcom/twitter/library/scribe/ScribeItem;->y:I

    iget-object v1, p0, Lcom/twitter/android/xr;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget v1, v0, Lcom/twitter/library/scribe/ScribeItem;->i:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    iget-object v1, v0, Lcom/twitter/library/scribe/ScribeItem;->n:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xr;->c:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xr;->h:Ljava/lang/String;

    return-void
.end method

.method public a(JJ)Z
    .locals 3

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/xr;->d:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashSet;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/xr;->d:Ljava/util/HashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public a(Landroid/widget/AbsListView;I)Z
    .locals 4

    const/4 v3, 0x0

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    if-eqz v0, :cond_0

    if-ne p2, v1, :cond_1

    iput-boolean v1, p0, Lcom/twitter/android/xr;->m:Z

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/xr;->k:J

    :cond_0
    :goto_0
    return v3

    :cond_1
    const/4 v0, 0x2

    if-ne p2, v0, :cond_2

    iput-boolean v1, p0, Lcom/twitter/android/xr;->l:Z

    goto :goto_0

    :cond_2
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xr;->e:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->O()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/xr;->l:Z

    if-eqz v0, :cond_3

    iget-wide v0, p0, Lcom/twitter/android/xr;->k:J

    :goto_1
    iget-object v2, p0, Lcom/twitter/android/xr;->n:Lcom/twitter/android/widget/aj;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/widget/aj;->a(J)Ljava/util/ArrayList;

    iput-boolean v3, p0, Lcom/twitter/android/xr;->l:Z

    goto :goto_0

    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    goto :goto_1
.end method

.method public a(Landroid/widget/AbsListView;III)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public b()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/xr;->d:Ljava/util/HashMap;

    iget-object v1, p0, Lcom/twitter/android/xr;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
