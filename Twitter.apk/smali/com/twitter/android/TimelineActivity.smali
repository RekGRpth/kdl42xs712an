.class public Lcom/twitter/android/TimelineActivity;
.super Lcom/twitter/android/UserQueryActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/UserQueryActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 1

    new-instance v0, Lcom/twitter/android/ug;

    invoke-direct {v0, p0}, Lcom/twitter/android/ug;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    return-object v0
.end method

.method protected a(Landroid/content/Intent;Lcom/twitter/library/client/e;)Lcom/twitter/android/iu;
    .locals 4

    check-cast p2, Lcom/twitter/android/ug;

    new-instance v0, Lcom/twitter/android/TimelineFragment;

    invoke-direct {v0}, Lcom/twitter/android/TimelineFragment;-><init>()V

    iget-boolean v1, p2, Lcom/twitter/android/ug;->b:Z

    if-eqz v1, :cond_0

    const-string/jumbo v1, "owner_id"

    iget-wide v2, p0, Lcom/twitter/android/TimelineActivity;->b:J

    invoke-virtual {p1, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    const-string/jumbo v1, "type"

    iget v2, p2, Lcom/twitter/android/ug;->c:I

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/android/TimelineFragment;->a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v1

    iget-boolean v2, p2, Lcom/twitter/android/ug;->a:Z

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/BaseListFragment;->j(Z)Lcom/twitter/android/client/BaseListFragment;

    new-instance v1, Lcom/twitter/android/iu;

    invoke-direct {v1, v0}, Lcom/twitter/android/iu;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    return-object v1
.end method

.method protected a(Landroid/content/Intent;)Ljava/lang/CharSequence;
    .locals 2

    const-string/jumbo v0, "title"

    const v1, 0x7f0f01d7    # com.twitter.android.R.string.home_timeline

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/TimelineActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 4

    invoke-super {p0, p1, p2}, Lcom/twitter/android/UserQueryActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    check-cast p2, Lcom/twitter/android/ug;

    iget-boolean v0, p2, Lcom/twitter/android/ug;->b:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TimelineActivity;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/twitter/android/TimelineActivity;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const v0, 0x7f0f04e3    # com.twitter.android.R.string.tweets_fetch_error

    const/4 v1, 0x1

    invoke-static {p0, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->g()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/library/api/TwitterUser;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/UserQueryActivity;->a(Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TimelineFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/android/TimelineFragment;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/UserQueryActivity;->onStart()V

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const v1, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/TimelineFragment;

    invoke-virtual {p0}, Lcom/twitter/android/TimelineActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/TimelineFragment;->D()Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/bn;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/android/client/bn;

    return-void
.end method
