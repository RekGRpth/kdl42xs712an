.class public Lcom/twitter/android/cx;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Z

.field private static b:Ljava/lang/String;


# direct methods
.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)I
    .locals 3

    if-eqz p1, :cond_1

    if-ltz p2, :cond_1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    if-lez p3, :cond_1

    add-int v0, p2, p3

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v1

    move v0, p2

    :goto_0
    if-ge v0, v1, :cond_0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    sub-int v0, v1, p2

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Landroid/support/v4/content/Loader;
    .locals 8

    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    new-instance v0, Lcom/twitter/android/bl;

    sget-object v2, Landroid/provider/ContactsContract$Data;->CONTENT_URI:Landroid/net/Uri;

    const/4 v1, 0x3

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v1, "data1"

    aput-object v1, v3, v6

    const-string/jumbo v1, "mimetype"

    aput-object v1, v3, v7

    const-string/jumbo v1, "display_name"

    aput-object v1, v3, v5

    const-string/jumbo v4, "data1 NOT NULL AND (mimetype=? OR mimetype=?)"

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v1, "vnd.android.cursor.item/phone_v2"

    aput-object v1, v5, v6

    const-string/jumbo v1, "vnd.android.cursor.item/email_v2"

    aput-object v1, v5, v7

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/util/List;Ljava/util/Map;Ljava/util/regex/Pattern;Z)Ljava/util/ArrayList;
    .locals 5

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    if-eqz p0, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    if-eqz p1, :cond_1

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->ensureCapacity(I)V

    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {p1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-virtual {p2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-nez v4, :cond_0

    new-instance v4, Lcom/twitter/library/api/TwitterContact;

    invoke-direct {v4, v1, v0, p3}, Lcom/twitter/library/api/TwitterContact;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v2
.end method

.method public static a(Landroid/database/Cursor;Ljava/util/Locale;Ljava/util/HashMap;Landroid/content/Context;Lcom/twitter/android/cy;)V
    .locals 11

    const/16 v1, 0x64

    const/4 v0, 0x0

    invoke-interface {p0}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-lez v2, :cond_5

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->getFormatTypeForLocale(Ljava/util/Locale;)I

    move-result v4

    :cond_0
    :goto_0
    invoke-interface {p0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_4

    const-string/jumbo v2, "vnd.android.cursor.item/phone_v2"

    const/4 v7, 0x1

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    packed-switch v4, :pswitch_data_0

    :goto_1
    invoke-virtual {v7}, Ljava/lang/String;->length()I

    move-result v8

    move v2, v0

    :goto_2
    if-ge v2, v8, :cond_2

    invoke-virtual {v7, v2}, Ljava/lang/String;->charAt(I)C

    move-result v9

    invoke-static {v9}, Landroid/telephony/PhoneNumberUtils;->isISODigit(C)Z

    move-result v10

    if-eqz v10, :cond_1

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :pswitch_0
    const-string/jumbo v2, "+1"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    :pswitch_1
    const/16 v2, 0x2b

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    goto :goto_1

    :cond_2
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    invoke-virtual {v3, v0, v2}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    goto :goto_0

    :cond_3
    invoke-interface {p0, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v7, Lcom/twitter/library/util/x;->h:Ljava/util/regex/Pattern;

    invoke-virtual {v7, v2}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/regex/Matcher;->matches()Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz p2, :cond_0

    const/4 v7, 0x2

    invoke-interface {p0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p2, v2, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int v4, v2, v3

    invoke-static {v4, v1}, Lcom/twitter/library/network/aa;->a(II)I

    move-result v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    move v4, v0

    move v0, v2

    :goto_3
    if-ge v4, v7, :cond_5

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    if-lez v0, :cond_8

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v2

    sub-int/2addr v2, v0

    invoke-static {v8, v6, v2, v1}, Lcom/twitter/android/cx;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)I

    move-result v10

    sub-int v2, v0, v10

    sub-int v0, v1, v10

    :goto_4
    invoke-virtual {v9}, Ljava/util/ArrayList;->clear()V

    if-lez v0, :cond_7

    if-lez v3, :cond_7

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v10

    sub-int/2addr v10, v3

    invoke-static {v9, v5, v10, v0}, Lcom/twitter/android/cx;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;II)I

    move-result v10

    sub-int/2addr v3, v10

    sub-int/2addr v0, v10

    move v0, v3

    :goto_5
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-virtual {v8, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-interface {p4, v3, v10, v4, v7}, Lcom/twitter/android/cy;->a([Ljava/lang/String;[Ljava/lang/String;II)V

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v0

    move v0, v2

    goto :goto_3

    :cond_5
    invoke-static {p3}, Lcom/twitter/android/cx;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-static {p3}, Lcom/twitter/android/cx;->c(Landroid/content/Context;)V

    :cond_6
    invoke-static {p3}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    const-string/jumbo v2, "hometab"

    invoke-direct {v1, p3, v0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "taut"

    const-wide/16 v2, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->e()Z

    return-void

    :cond_7
    move v0, v3

    goto :goto_5

    :cond_8
    move v2, v0

    move v0, v1

    goto :goto_4

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 4

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/cx;->b:Ljava/lang/String;

    if-eqz v1, :cond_0

    sget-object v1, Lcom/twitter/android/cx;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-boolean v0, Lcom/twitter/android/cx;->a:Z

    :goto_0
    return v0

    :cond_0
    new-instance v1, Lcom/twitter/library/client/f;

    const-string/jumbo v2, "ContactLoaderHelper"

    invoke-direct {v1, p0, v0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "contacts_uploaded"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v1

    sput-boolean v1, Lcom/twitter/android/cx;->a:Z

    sput-object v0, Lcom/twitter/android/cx;->b:Ljava/lang/String;

    sget-boolean v0, Lcom/twitter/android/cx;->a:Z

    goto :goto_0
.end method

.method public static c(Landroid/content/Context;)V
    .locals 4

    const/4 v3, 0x1

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/client/f;

    const-string/jumbo v2, "ContactLoaderHelper"

    invoke-direct {v1, p0, v0, v2}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v1

    const-string/jumbo v2, "contacts_uploaded"

    invoke-virtual {v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;Z)Lcom/twitter/library/client/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/f;->e()Z

    move-result v1

    if-eqz v1, :cond_0

    sput-boolean v3, Lcom/twitter/android/cx;->a:Z

    sput-object v0, Lcom/twitter/android/cx;->b:Ljava/lang/String;

    :cond_0
    return-void
.end method
