.class Lcom/twitter/android/vo;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ru;


# instance fields
.field final synthetic a:Landroid/support/v4/app/FragmentActivity;

.field final synthetic b:Lcom/twitter/library/provider/Tweet;

.field final synthetic c:Lcom/twitter/library/scribe/ScribeItem;

.field final synthetic d:Lcom/twitter/android/vn;


# direct methods
.method constructor <init>(Lcom/twitter/android/vn;Landroid/support/v4/app/FragmentActivity;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iput-object p2, p0, Lcom/twitter/android/vo;->a:Landroid/support/v4/app/FragmentActivity;

    iput-object p3, p0, Lcom/twitter/android/vo;->b:Lcom/twitter/library/provider/Tweet;

    iput-object p4, p0, Lcom/twitter/android/vo;->c:Lcom/twitter/library/scribe/ScribeItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v1, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v2, p0, Lcom/twitter/android/vo;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v2}, Lcom/twitter/android/vn;->a(Lcom/twitter/android/vn;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "quote"

    iget-object v3, p0, Lcom/twitter/android/vo;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v4, p0, Lcom/twitter/android/vo;->c:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    return-void
.end method

.method public a(IJLcom/twitter/library/provider/ParcelableTweet;ZLjava/lang/String;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v1, p0, Lcom/twitter/android/vo;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1, p6}, Lcom/twitter/android/vn;->a(Landroid/content/Context;Ljava/lang/String;)V

    if-eqz p5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v1, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v2, p0, Lcom/twitter/android/vo;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v2}, Lcom/twitter/android/vn;->a(Lcom/twitter/android/vn;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "unretweet"

    iget-object v3, p0, Lcom/twitter/android/vo;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v4, p0, Lcom/twitter/android/vo;->c:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v1, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v2, p0, Lcom/twitter/android/vo;->a:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v1, v2}, Lcom/twitter/android/vn;->a(Lcom/twitter/android/vn;Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v2, "retweet"

    iget-object v3, p0, Lcom/twitter/android/vo;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v4, p0, Lcom/twitter/android/vo;->c:Lcom/twitter/library/scribe/ScribeItem;

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/vn;->a(Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeItem;)V

    goto :goto_0
.end method

.method public b(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 9

    iget-object v0, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v1, v1, Lcom/twitter/android/vn;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v5, v5, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v6, "retweet_dialog"

    const-string/jumbo v7, ""

    const-string/jumbo v8, "dismiss"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public c(IJLcom/twitter/library/provider/ParcelableTweet;Z)V
    .locals 9

    iget-object v0, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v0, v0, Lcom/twitter/android/vn;->a:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v1, v1, Lcom/twitter/android/vn;->b:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/twitter/android/vo;->d:Lcom/twitter/android/vn;

    iget-object v5, v5, Lcom/twitter/android/vn;->d:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v6, "retweet_dialog"

    const-string/jumbo v7, ""

    const-string/jumbo v8, "impression"

    invoke-static {v5, v6, v7, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method
