.class public Lcom/twitter/android/PostStorage;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;

.field private static final b:Ljava/lang/ClassLoader;


# instance fields
.field public a:Ljava/util/ArrayList;

.field private final c:Ljava/util/LinkedHashMap;

.field private final d:Ljava/util/List;

.field private e:Lcom/twitter/android/PostStorage$LocationItem;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/pw;

    invoke-direct {v0}, Lcom/twitter/android/pw;-><init>()V

    sput-object v0, Lcom/twitter/android/PostStorage;->CREATOR:Landroid/os/Parcelable$Creator;

    const-class v0, Lcom/twitter/android/PostStorage;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/PostStorage;->b:Ljava/lang/ClassLoader;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/PostStorage;->d:Ljava/util/List;

    return-void
.end method

.method public constructor <init>(Landroid/os/Parcel;)V
    .locals 7

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedHashMap;

    const/4 v2, 0x4

    invoke-direct {v0, v2}, Ljava/util/LinkedHashMap;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    sget-object v0, Lcom/twitter/android/PostStorage$MediaItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArray(Landroid/os/Parcelable$Creator;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/PostStorage$MediaItem;

    array-length v3, v0

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    iget-object v5, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    iget-object v6, v4, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {v5, v6, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    :cond_0
    sget-object v0, Lcom/twitter/android/PostStorage$UrlItem;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/PostStorage;->d:Ljava/util/List;

    sget-object v0, Lcom/twitter/android/PostStorage;->b:Ljava/lang/ClassLoader;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$LocationItem;

    iput-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-lez v2, :cond_2

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    iput-object v3, p0, Lcom/twitter/android/PostStorage;->a:Ljava/util/ArrayList;

    :cond_2
    return-void
.end method


# virtual methods
.method public a()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$LocationItem;->a()Landroid/location/Location;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    return-object v0
.end method

.method public a(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    iget v2, v0, Lcom/twitter/android/PostStorage$MediaItem;->l:I

    if-ne v2, p1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->d()V

    iget-object v1, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    iget-object v0, v0, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/PostStorage;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$UrlItem;

    iget v2, v0, Lcom/twitter/android/PostStorage$UrlItem;->l:I

    if-ne v2, p1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/PostStorage;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    iget v0, v0, Lcom/twitter/android/PostStorage$LocationItem;->l:I

    if-ne v0, p1, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/PostStorage$LocationItem;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    return-void
.end method

.method public a(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 2

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage$MediaItem;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/PostStorage;->e()V

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$LocationItem;->c()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->d()V

    :cond_0
    return-void
.end method

.method public b(Lcom/twitter/android/PostStorage$MediaItem;)V
    .locals 2

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->a:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/android/PostStorage$MediaItem;->a(Lcom/twitter/android/PostStorage$MediaItem;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1, p1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public c()Lcom/twitter/android/PostStorage$LocationItem;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    return-object v0
.end method

.method public d()Lcom/twitter/android/PostStorage$MediaItem;
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    return-object v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    goto :goto_0
.end method

.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->d()V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    return-void
.end method

.method public f()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    return-object v0
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/PostStorage$MediaItem;

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage$MediaItem;->c()V

    goto :goto_0

    :cond_0
    return-void
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    new-array v1, v0, [Lcom/twitter/android/PostStorage$MediaItem;

    iget-object v3, p0, Lcom/twitter/android/PostStorage;->c:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3, v1}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    invoke-virtual {p1, v1, v0}, Landroid/os/Parcel;->writeTypedArray([Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->d:Ljava/util/List;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeTypedList(Ljava/util/List;)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->e:Lcom/twitter/android/PostStorage$LocationItem;

    invoke-virtual {p1, v0, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->a:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    move v1, v0

    :goto_0
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    if-lez v1, :cond_1

    :goto_1
    if-ge v2, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/PostStorage;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v1, v2

    goto :goto_0

    :cond_1
    return-void
.end method
