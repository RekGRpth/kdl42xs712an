.class Lcom/twitter/android/sl;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# instance fields
.field final synthetic a:Lcom/twitter/android/sh;

.field private b:Lcom/twitter/library/api/PromotedContent;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/sh;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/sl;->b:Lcom/twitter/library/api/PromotedContent;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/sl;->c:I

    return-void
.end method


# virtual methods
.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/sl;->c:I

    return-void
.end method

.method public a(Lcom/twitter/library/api/PromotedContent;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/sl;->b:Lcom/twitter/library/api/PromotedContent;

    return-void
.end method

.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/sl;->onClick(Lcom/twitter/library/widget/UserView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserView;JI)V
    .locals 7

    const/4 v2, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v0}, Lcom/twitter/android/sh;->b(Lcom/twitter/android/sh;)Lcom/twitter/android/client/c;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v0}, Lcom/twitter/android/sh;->c(Lcom/twitter/android/sh;)I

    move-result v0

    iget-object v1, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v1}, Lcom/twitter/library/widget/ActionButton;->isChecked()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/sl;->b:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v6, p2, p3, v1}, Lcom/twitter/android/client/c;->a(JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v1}, Lcom/twitter/android/sh;->d(Lcom/twitter/android/sh;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    if-ne v0, v2, :cond_0

    const-string/jumbo v0, "search:people:users:user:unfollow"

    :goto_0
    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v2}, Lcom/twitter/android/sh;->g(Lcom/twitter/android/sh;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    aput-object v0, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v1}, Lcom/twitter/android/sh;->f(Lcom/twitter/android/sh;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v4, v4, v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v1}, Lcom/twitter/android/sh;->f(Lcom/twitter/android/sh;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v1}, Lcom/twitter/android/sh;->e(Lcom/twitter/android/sh;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeLog;->f(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    iget v5, p0, Lcom/twitter/android/sl;->c:I

    move-wide v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/scribe/ScribeLog;->a(JLcom/twitter/library/api/PromotedContent;Ljava/lang/String;I)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void

    :cond_0
    const-string/jumbo v0, "search:universal::user:unfollow"

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/sl;->b:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v6, p2, p3, v5, v1}, Lcom/twitter/android/client/c;->a(JZLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/sl;->a:Lcom/twitter/android/sh;

    invoke-static {v1}, Lcom/twitter/android/sh;->d(Lcom/twitter/android/sh;)Lcom/twitter/library/util/FriendshipCache;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    if-ne v0, v2, :cond_2

    const-string/jumbo v0, "search:people:users:user:follow"

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "search:universal::user:follow"

    goto :goto_0
.end method
