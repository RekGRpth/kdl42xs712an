.class Lcom/twitter/android/my;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesThreadFragment;

.field private final b:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/MessagesThreadFragment;)V
    .locals 3

    iput-object p1, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-virtual {p1}, Lcom/twitter/android/MessagesThreadFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    invoke-virtual {p1}, Lcom/twitter/android/MessagesThreadFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c0092    # com.twitter.android.R.dimen.message_content_max_width

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/my;->b:I

    return-void
.end method

.method private a(Landroid/database/Cursor;)I
    .locals 4

    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v2}, Lcom/twitter/android/MessagesThreadFragment;->f(Lcom/twitter/android/MessagesThreadFragment;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private a(Landroid/widget/ImageView;Lcom/twitter/library/api/MediaEntity;)V
    .locals 4

    invoke-virtual {p1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    iget v1, p0, Lcom/twitter/android/my;->b:I

    iget v2, p2, Lcom/twitter/library/api/MediaEntity;->width:I

    int-to-float v2, v2

    iget v3, p2, Lcom/twitter/library/api/MediaEntity;->height:I

    int-to-float v3, v3

    div-float/2addr v2, v3

    const/high16 v3, 0x3f400000    # 0.75f

    invoke-static {v2, v3}, Ljava/lang/Math;->max(FF)F

    move-result v2

    const/high16 v3, 0x40400000    # 3.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    int-to-float v3, v1

    div-float v2, v3, v2

    float-to-int v2, v2

    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->height:I

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private a(Lcom/twitter/android/nc;Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;I)Z
    .locals 8

    const/4 v4, 0x0

    const/4 v2, 0x1

    const/16 v7, 0x8

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v0}, Lcom/twitter/android/MessagesThreadFragment;->i(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;

    move-result-object v0

    iget-object v1, p1, Lcom/twitter/android/nc;->e:Lcom/twitter/library/util/m;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iput-object v4, p1, Lcom/twitter/android/nc;->e:Lcom/twitter/library/util/m;

    if-eqz p3, :cond_2

    invoke-virtual {p3, v2}, Lcom/twitter/library/api/TweetEntities;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p3, Lcom/twitter/library/api/TweetEntities;->media:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaEntity;

    iget-object v1, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->j(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v4, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->j(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v4, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/m;

    iput-object v1, p1, Lcom/twitter/android/nc;->e:Lcom/twitter/library/util/m;

    :goto_0
    iget-object v1, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/my;->a(Landroid/widget/ImageView;Lcom/twitter/library/api/MediaEntity;)V

    iget-object v1, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->k(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v4, p1, Lcom/twitter/android/nc;->e:Lcom/twitter/library/util/m;

    invoke-interface {v1, v4}, Lcom/twitter/library/widget/ap;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget-object v4, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-virtual {v4, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->i(Lcom/twitter/android/MessagesThreadFragment;)Ljava/util/HashMap;

    move-result-object v1

    iget-object v4, p1, Lcom/twitter/android/nc;->e:Lcom/twitter/library/util/m;

    iget-object v5, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p2, p3}, Lcom/twitter/android/lw;->a(Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;)V

    invoke-virtual {p2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p1, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    invoke-virtual {v1, v7}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p1, Lcom/twitter/android/nc;->f:Landroid/view/View;

    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    iget-object v1, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    new-instance v3, Lcom/twitter/android/na;

    invoke-direct {v3, p0, v0}, Lcom/twitter/android/na;-><init>(Lcom/twitter/android/my;Lcom/twitter/library/api/MediaEntity;)V

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v0, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    new-instance v1, Lcom/twitter/android/nb;

    invoke-direct {v1, p0, p4}, Lcom/twitter/android/nb;-><init>(Lcom/twitter/android/my;I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    move v0, v2

    :goto_2
    return v0

    :cond_0
    new-instance v1, Lcom/twitter/library/util/m;

    iget-object v4, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    iget-object v5, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-virtual {v5}, Landroid/widget/ImageView;->getWidth()I

    move-result v5

    iget v6, v0, Lcom/twitter/library/api/MediaEntity;->height:I

    invoke-direct {v1, v4, v5, v6, v2}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;IIZ)V

    iput-object v1, p1, Lcom/twitter/android/nc;->e:Lcom/twitter/library/util/m;

    goto :goto_0

    :cond_1
    iget-object v1, p1, Lcom/twitter/android/nc;->f:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p1, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v0, p1, Lcom/twitter/android/nc;->f:Landroid/view/View;

    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    move v0, v3

    goto :goto_2
.end method


# virtual methods
.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 14

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Lcom/twitter/android/nc;

    const/4 v1, 0x2

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v1, 0x5

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-direct {p0, v0}, Lcom/twitter/android/my;->a(Landroid/database/Cursor;)I

    move-result v1

    if-nez v1, :cond_6

    const/4 v7, 0x1

    :goto_0
    const/16 v1, 0x8

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v3, 0x1

    if-ne v1, v3, :cond_7

    const/4 v1, 0x1

    move v10, v1

    :goto_1
    const/4 v3, 0x0

    iget-object v1, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v1}, Lcom/twitter/android/MessagesThreadFragment;->g(Lcom/twitter/android/MessagesThreadFragment;)Lcom/twitter/android/client/c;

    move-result-object v6

    iget-object v1, v9, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    if-eqz v1, :cond_1

    iget-object v1, v9, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x0

    if-eqz v2, :cond_0

    invoke-virtual {v6, v2}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v1

    :cond_0
    if-eqz v1, :cond_8

    iget-object v2, v9, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_2
    iget-object v1, v9, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    new-instance v2, Lcom/twitter/android/mz;

    invoke-direct {v2, p0, v4, v5}, Lcom/twitter/android/mz;-><init>(Lcom/twitter/android/my;J)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const/4 v1, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_9

    const/4 v2, 0x0

    :goto_3
    iget-object v1, v9, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    new-instance v4, Ljava/lang/StringBuilder;

    const/4 v1, 0x3

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v4, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6}, Lcom/twitter/android/client/c;->L()Z

    move-result v1

    if-eqz v1, :cond_14

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v1

    invoke-direct {p0, v9, v4, v2, v1}, Lcom/twitter/android/my;->a(Lcom/twitter/android/nc;Ljava/lang/StringBuilder;Lcom/twitter/library/api/TweetEntities;I)Z

    move-result v1

    move v11, v1

    :goto_4
    iget-object v1, v9, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v6}, Lcom/twitter/android/client/c;->V()F

    move-result v5

    invoke-virtual {v1, v3, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    iget-object v13, v9, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v4, 0x7f0b006b    # com.twitter.android.R.color.prefix

    invoke-virtual {v12, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    move v8, v7

    invoke-static/range {v1 .. v8}, Lcom/twitter/library/view/d;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;Lcom/twitter/library/view/c;IIIZZ)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v13, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v9, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    const/4 v2, 0x6

    invoke-static {v1, v2}, Landroid/text/util/Linkify;->addLinks(Landroid/widget/TextView;I)Z

    const/4 v1, 0x7

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v7, :cond_2

    iget-object v2, v9, Lcom/twitter/android/nc;->g:Landroid/widget/LinearLayout;

    const v3, 0x7f0202c1    # com.twitter.android.R.drawable.message_sent_background_bubble

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :cond_2
    iget-object v2, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v3, 0x7f0b0074    # com.twitter.android.R.color.secondary_text

    invoke-virtual {v12, v3}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTextColor(I)V

    if-eqz v10, :cond_c

    const/4 v2, 0x1

    if-ne v1, v2, :cond_a

    iget-object v1, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v2, 0x7f0f010e    # com.twitter.android.R.string.direct_message_sending

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :goto_5
    if-eqz v11, :cond_10

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v3, 0x7f0f011c    # com.twitter.android.R.string.dm_image_attached

    invoke-virtual {v2, v3}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ". "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_6
    iget-object v2, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v2

    if-nez v2, :cond_3

    const-string/jumbo v2, ""

    :cond_3
    iget-object v3, v9, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    if-nez v3, :cond_4

    const-string/jumbo v3, ""

    :cond_4
    iget-object v4, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v4}, Lcom/twitter/android/MessagesThreadFragment;->h(Lcom/twitter/android/MessagesThreadFragment;)Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_11

    iget-object v4, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v4}, Lcom/twitter/android/MessagesThreadFragment;->h(Lcom/twitter/android/MessagesThreadFragment;)Ljava/lang/String;

    move-result-object v4

    :goto_7
    if-eqz v7, :cond_12

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v5, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v6, 0x7f0f0120    # com.twitter.android.R.string.dm_received_message_format

    invoke-virtual {v5, v6}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v8, 0x0

    aput-object v4, v6, v8

    const/4 v4, 0x1

    aput-object v3, v6, v4

    const/4 v3, 0x2

    aput-object v2, v6, v3

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    :goto_8
    invoke-virtual {p1, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz v11, :cond_5

    if-eqz v7, :cond_13

    iget-object v1, v9, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v4, 0x7f0f011f    # com.twitter.android.R.string.dm_received_attached_image_preview

    invoke-virtual {v3, v4}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v6}, Lcom/twitter/android/MessagesThreadFragment;->h(Lcom/twitter/android/MessagesThreadFragment;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_5
    :goto_9
    iget-object v1, v9, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    iget-object v2, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v3, 0x7f0f01df    # com.twitter.android.R.string.image_profile

    invoke-virtual {v2, v3}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_0

    :cond_7
    const/4 v1, 0x0

    move v10, v1

    goto/16 :goto_1

    :cond_8
    iget-object v1, v9, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    const v2, 0x7f02003a    # com.twitter.android.R.drawable.bg_no_profile_photo_md

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageResource(I)V

    goto/16 :goto_2

    :cond_9
    const/4 v1, 0x6

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TweetEntities;

    move-object v2, v1

    goto/16 :goto_3

    :cond_a
    const/4 v2, 0x3

    if-ne v1, v2, :cond_b

    iget-object v1, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v2, 0x7f0f010e    # com.twitter.android.R.string.direct_message_sending

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v2, 0x7f0b0045    # com.twitter.android.R.color.deep_red

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v9, Lcom/twitter/android/nc;->g:Landroid/widget/LinearLayout;

    const v2, 0x7f0202c4    # com.twitter.android.R.drawable.message_sent_error_background_bubble

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_5

    :cond_b
    iget-object v1, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v2, 0x7f0f010d    # com.twitter.android.R.string.direct_message_not_sent

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v2, 0x7f0b0045    # com.twitter.android.R.color.deep_red

    invoke-virtual {v12, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v1, v9, Lcom/twitter/android/nc;->g:Landroid/widget/LinearLayout;

    const v2, 0x7f0202c4    # com.twitter.android.R.drawable.message_sent_error_background_bubble

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto/16 :goto_5

    :cond_c
    const/4 v1, 0x4

    move-object/from16 v0, p3

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_d

    iget-object v2, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/MessagesThreadFragment;->q()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_d
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    const-wide/32 v4, 0x5265c00

    add-long/2addr v2, v4

    invoke-static {v2, v3}, Landroid/text/format/DateUtils;->isToday(J)Z

    move-result v2

    if-eqz v2, :cond_e

    iget-object v2, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v3, 0x7f0f0572    # com.twitter.android.R.string.yesterday_with_time

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {}, Lcom/twitter/android/MessagesThreadFragment;->q()Ljava/text/SimpleDateFormat;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v12, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_e
    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/twitter/library/util/Util;->a(J)Z

    move-result v2

    if-eqz v2, :cond_f

    iget-object v2, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/MessagesThreadFragment;->r()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_f
    iget-object v2, v9, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    invoke-static {}, Lcom/twitter/android/MessagesThreadFragment;->s()Ljava/text/SimpleDateFormat;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_5

    :cond_10
    const-string/jumbo v1, ""

    goto/16 :goto_6

    :cond_11
    const-string/jumbo v4, ""

    goto/16 :goto_7

    :cond_12
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v5, 0x7f0f0122    # com.twitter.android.R.string.dm_sent_message_format

    invoke-virtual {v4, v5}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v3, v5, v6

    const/4 v3, 0x1

    aput-object v2, v5, v3

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto/16 :goto_8

    :cond_13
    iget-object v1, v9, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    iget-object v3, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    const v4, 0x7f0f0121    # com.twitter.android.R.string.dm_sent_attached_image_preview

    invoke-virtual {v3, v4}, Lcom/twitter/android/MessagesThreadFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object v2, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_9

    :cond_14
    move v11, v3

    goto/16 :goto_4
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/my;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-direct {p0, v0}, Lcom/twitter/android/my;->a(Landroid/database/Cursor;)I

    move-result v0

    return v0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x2

    return v0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x2

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/my;->a:Lcom/twitter/android/MessagesThreadFragment;

    invoke-static {v2}, Lcom/twitter/android/MessagesThreadFragment;->f(Lcom/twitter/android/MessagesThreadFragment;)J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c3    # com.twitter.android.R.layout.message_thread_row_received_view

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    move-object v1, v0

    :goto_0
    new-instance v2, Lcom/twitter/android/nc;

    invoke-direct {v2}, Lcom/twitter/android/nc;-><init>()V

    const v0, 0x7f09012d    # com.twitter.android.R.id.profile_image

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/nc;->a:Landroid/widget/ImageView;

    const v0, 0x7f090036    # com.twitter.android.R.id.content

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    iget-object v0, v2, Lcom/twitter/android/nc;->b:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/twitter/library/view/l;->a(Landroid/widget/TextView;)V

    const v0, 0x7f0900b6    # com.twitter.android.R.id.timestamp

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, v2, Lcom/twitter/android/nc;->c:Landroid/widget/TextView;

    const v0, 0x7f090077    # com.twitter.android.R.id.image

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, v2, Lcom/twitter/android/nc;->d:Landroid/widget/ImageView;

    const v0, 0x7f09012e    # com.twitter.android.R.id.image_border

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, v2, Lcom/twitter/android/nc;->f:Landroid/view/View;

    const v0, 0x7f09012a    # com.twitter.android.R.id.bubble

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, v2, Lcom/twitter/android/nc;->g:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    return-object v1

    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f0300c4    # com.twitter.android.R.layout.message_thread_row_sent_view

    invoke-virtual {v0, v1, p3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout;

    move-object v1, v0

    goto :goto_0
.end method
