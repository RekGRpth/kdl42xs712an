.class Lcom/twitter/android/aao;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/WelcomeActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/WelcomeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aao;->a:Lcom/twitter/android/WelcomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4

    const/4 v3, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    new-instance v0, Lcom/twitter/android/FollowFlowController;

    invoke-direct {v0}, Lcom/twitter/android/FollowFlowController;-><init>()V

    sget-object v1, Lcom/twitter/android/FollowFlowController$Initiator;->a:Lcom/twitter/android/FollowFlowController$Initiator;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a(Lcom/twitter/android/FollowFlowController$Initiator;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/android/FollowFlowController;->a(Z)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "follow_friends"

    aput-object v2, v1, v3

    const/4 v2, 0x1

    const-string/jumbo v3, "nux_tag_invite"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "follow_recommendations"

    aput-object v3, v1, v2

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->a([Ljava/lang/String;)Lcom/twitter/android/FollowFlowController;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/aao;->a:Lcom/twitter/android/WelcomeActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/FollowFlowController;->b(Landroid/app/Activity;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/aao;->a:Lcom/twitter/android/WelcomeActivity;

    invoke-static {v0}, Lcom/twitter/android/WelcomeActivity;->a(Lcom/twitter/android/WelcomeActivity;)V

    goto :goto_0
.end method
