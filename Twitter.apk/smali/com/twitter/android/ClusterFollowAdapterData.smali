.class public Lcom/twitter/android/ClusterFollowAdapterData;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Landroid/view/animation/Animation;

.field private final b:Landroid/view/animation/Animation;

.field private final c:Lcom/twitter/android/cu;

.field private final d:Lcom/twitter/android/client/c;

.field private final e:J

.field private final f:Lcom/twitter/library/util/FriendshipCache;

.field private final g:Ljava/util/ArrayList;

.field private h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

.field private i:Ljava/util/HashSet;

.field private j:Ljava/util/HashSet;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;Lcom/twitter/android/cu;Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->i:Ljava/util/HashSet;

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->j:Ljava/util/HashSet;

    iput-object p3, p0, Lcom/twitter/android/ClusterFollowAdapterData;->c:Lcom/twitter/android/cu;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->d:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->e:J

    iput-object p2, p0, Lcom/twitter/android/ClusterFollowAdapterData;->f:Lcom/twitter/library/util/FriendshipCache;

    if-nez p4, :cond_0

    new-instance v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    invoke-direct {v0}, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    :goto_0
    const v0, 0x7f040019    # com.twitter.android.R.anim.slide_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->a:Landroid/view/animation/Animation;

    const v0, 0x7f040007    # com.twitter.android.R.anim.fade_in

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->b:Landroid/view/animation/Animation;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->g:Ljava/util/ArrayList;

    return-void

    :cond_0
    iput-object p4, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/ClusterFollowAdapterData;)Lcom/twitter/android/cu;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->c:Lcom/twitter/android/cu;

    return-object v0
.end method

.method private a(JLcom/twitter/library/api/TwitterUser;Landroid/view/View;I)V
    .locals 7

    const/4 v0, 0x0

    check-cast p4, Lcom/twitter/library/widget/UserView;

    iget-wide v1, p3, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v3, p3, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {p4, v1, v2}, Lcom/twitter/library/widget/UserView;->setUserId(J)V

    if-eqz v3, :cond_1

    iget-object v4, p0, Lcom/twitter/android/ClusterFollowAdapterData;->d:Lcom/twitter/android/client/c;

    iget-object v4, v4, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/4 v5, 0x2

    invoke-interface {v4, v5, v3}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v4

    invoke-virtual {p4, v4}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    :goto_0
    iget-object v4, p3, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    iget-object v5, p3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {p4, v4, v5}, Lcom/twitter/library/widget/UserView;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-boolean v4, p3, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    invoke-virtual {p4, v4}, Lcom/twitter/library/widget/UserView;->setProtected(Z)V

    iget-boolean v4, p3, Lcom/twitter/library/api/TwitterUser;->verified:Z

    invoke-virtual {p4, v4}, Lcom/twitter/library/widget/UserView;->setVerified(Z)V

    iget-object v4, p3, Lcom/twitter/library/api/TwitterUser;->promotedContent:Lcom/twitter/library/api/PromotedContent;

    iget-object v5, p0, Lcom/twitter/android/ClusterFollowAdapterData;->d:Lcom/twitter/android/client/c;

    iget-boolean v5, v5, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {p4, v4, v5}, Lcom/twitter/library/widget/UserView;->a(Lcom/twitter/library/api/PromotedContent;Z)V

    iget-wide v5, p0, Lcom/twitter/android/ClusterFollowAdapterData;->e:J

    cmp-long v5, v5, v1

    if-nez v5, :cond_2

    iget-object v0, p4, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :cond_0
    :goto_1
    invoke-virtual {p4}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    invoke-virtual {p3}, Lcom/twitter/library/api/TwitterUser;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/zx;->f:Ljava/lang/String;

    iput-object v3, v0, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    iput-wide p1, v0, Lcom/twitter/android/zx;->c:J

    iget v1, p3, Lcom/twitter/library/api/TwitterUser;->friendship:I

    iput v1, v0, Lcom/twitter/android/zx;->e:I

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->c:Lcom/twitter/android/cu;

    invoke-interface {v0, p4, v4, p5}, Lcom/twitter/android/cu;->a(Lcom/twitter/library/widget/UserView;Lcom/twitter/library/api/PromotedContent;I)V

    return-void

    :cond_1
    const/4 v4, 0x0

    invoke-virtual {p4, v4}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_2
    iget-object v5, p4, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v5, v0}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v5, p0, Lcom/twitter/android/ClusterFollowAdapterData;->f:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v5, :cond_0

    invoke-virtual {v5, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v0, p4, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    invoke-virtual {v5, v1, v2}, Lcom/twitter/library/util/FriendshipCache;->i(J)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_1

    :cond_3
    iget-object v1, p4, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    iget v2, p3, Lcom/twitter/library/api/TwitterUser;->friendship:I

    and-int/lit8 v2, v2, 0x1

    if-eqz v2, :cond_4

    const/4 v0, 0x1

    :cond_4
    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    goto :goto_1
.end method

.method private a(Lcom/twitter/library/widget/UserView;)V
    .locals 3

    const v0, 0x7f020085    # com.twitter.android.R.drawable.btn_follow_action

    new-instance v1, Lcom/twitter/android/cp;

    invoke-direct {v1, p0}, Lcom/twitter/android/cp;-><init>(Lcom/twitter/android/ClusterFollowAdapterData;)V

    invoke-virtual {p1, v0, v1}, Lcom/twitter/library/widget/UserView;->a(ILcom/twitter/library/widget/a;)V

    iget-object v0, p1, Lcom/twitter/library/widget/UserView;->m:Lcom/twitter/library/widget/ActionButton;

    const v1, 0x7f020086    # com.twitter.android.R.drawable.btn_follow_action_bg

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setBackgroundResource(I)V

    new-instance v0, Lcom/twitter/android/zx;

    invoke-direct {v0, p1}, Lcom/twitter/android/zx;-><init>(Lcom/twitter/library/widget/BaseUserView;)V

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/UserView;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/ClusterFollowAdapterData;->g:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private b(Landroid/database/Cursor;)I
    .locals 1

    const-string/jumbo v0, "_id"

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    return v0
.end method


# virtual methods
.method public a(Landroid/view/View;)Landroid/view/View;
    .locals 2

    const v0, 0x7f0900d8    # com.twitter.android.R.id.wrapper_view

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 7

    const/4 v1, 0x0

    invoke-direct {p0, p3}, Lcom/twitter/android/ClusterFollowAdapterData;->b(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    invoke-static {p2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x7f03002c    # com.twitter.android.R.layout.cluster_follow_wrapper

    invoke-virtual {v0, v2, p4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    const v0, 0x7f0900d5    # com.twitter.android.R.id.cluster_follow_header

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/view/View;->setClickable(Z)V

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v0, 0x7f0900d8    # com.twitter.android.R.id.wrapper_view

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ClipRowView;

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/ClipRowView;->addView(Landroid/view/View;)V

    const v0, 0x7f0900d9    # com.twitter.android.R.id.cluster_recommendations

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    move v2, v1

    :goto_0
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v1

    if-ge v2, v1, :cond_0

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/widget/UserView;

    invoke-direct {p0, v1}, Lcom/twitter/android/ClusterFollowAdapterData;->a(Lcom/twitter/library/widget/UserView;)V

    new-instance v6, Lcom/twitter/android/co;

    invoke-direct {v6, p0, v1, v4, v5}, Lcom/twitter/android/co;-><init>(Lcom/twitter/android/ClusterFollowAdapterData;Lcom/twitter/library/widget/UserView;J)V

    invoke-virtual {v1, v6}, Lcom/twitter/library/widget/UserView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_0

    :cond_0
    move-object p1, v3

    :cond_1
    return-object p1
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->i:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->j:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method

.method public declared-synchronized a(JJLcom/twitter/library/api/TwitterUser;)V
    .locals 6

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterUser;

    const/4 v1, 0x0

    :goto_0
    array-length v2, v0

    if-ge v1, v2, :cond_0

    aget-object v2, v0, v1

    iget-wide v2, v2, Lcom/twitter/library/api/TwitterUser;->userId:J

    cmp-long v2, v2, p3

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/ClusterFollowAdapterData;->j:Ljava/util/HashSet;

    new-instance v3, Lcom/twitter/android/cq;

    iget-wide v4, p5, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-direct {v3, p1, p2, v4, v5}, Lcom/twitter/android/cq;-><init>(JJ)V

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    aput-object p5, v0, v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized a(J[Lcom/twitter/library/api/TwitterUser;)V
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->i:Ljava/util/HashSet;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/view/View;Landroid/database/Cursor;)V
    .locals 12

    const/4 v9, 0x0

    invoke-direct {p0, p2}, Lcom/twitter/android/ClusterFollowAdapterData;->b(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, [Lcom/twitter/library/api/TwitterUser;

    const v0, 0x7f0900d9    # com.twitter.android.R.id.cluster_recommendations

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/view/ViewGroup;

    move v8, v9

    :goto_0
    invoke-virtual {v7}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    if-ge v8, v0, :cond_2

    invoke-virtual {v7, v8}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    array-length v0, v6

    if-ge v8, v0, :cond_1

    aget-object v3, v6, v8

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/ClusterFollowAdapterData;->a(JLcom/twitter/library/api/TwitterUser;Landroid/view/View;I)V

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->j:Ljava/util/HashSet;

    new-instance v3, Lcom/twitter/android/cq;

    aget-object v10, v6, v8

    iget-wide v10, v10, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-direct {v3, v1, v2, v10, v11}, Lcom/twitter/android/cq;-><init>(JJ)V

    invoke-virtual {v0, v3}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v4}, Landroid/view/View;->clearAnimation()V

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->b:Landroid/view/animation/Animation;

    invoke-virtual {v4, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_0
    invoke-virtual {v4, v9}, Landroid/view/View;->setVisibility(I)V

    :goto_1
    add-int/lit8 v0, v8, 0x1

    move v8, v0

    goto :goto_0

    :cond_1
    const/16 v0, 0x8

    invoke-virtual {v4, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->i:Ljava/util/HashSet;

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const v0, 0x7f0900d7    # com.twitter.android.R.id.cluster_menu

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/ClusterFollowAdapterData;->a:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :cond_3
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_2

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/zx;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/zx;->b:Lcom/twitter/library/widget/BaseUserView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/BaseUserView;->setUserImage(Landroid/graphics/Bitmap;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method public declared-synchronized a(J)Z
    .locals 2

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public a(Landroid/database/Cursor;)Z
    .locals 2

    invoke-direct {p0, p1}, Lcom/twitter/android/ClusterFollowAdapterData;->b(Landroid/database/Cursor;)I

    move-result v0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/ClusterFollowAdapterData;->a(J)Z

    move-result v0

    return v0
.end method

.method public declared-synchronized b()Ljava/util/Set;
    .locals 7

    monitor-enter p0

    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    iget-object v0, v0, Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterUser;

    array-length v4, v0

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v4, :cond_0

    aget-object v5, v0, v1

    iget-wide v5, v5, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v2, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    monitor-exit p0

    return-object v2

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public c()Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ClusterFollowAdapterData;->h:Lcom/twitter/android/ClusterFollowAdapterData$Recommendations;

    return-object v0
.end method
