.class public Lcom/twitter/android/MessagesFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemLongClickListener;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private a:Landroid/support/v4/widget/CursorAdapter;

.field private b:Landroid/widget/ListView;

.field private c:Lcom/twitter/android/md;

.field private d:Landroid/view/View;

.field private e:Ljava/lang/Long;

.field private f:Ljava/lang/Boolean;

.field private g:Ljava/lang/Boolean;

.field private h:Ljava/lang/Boolean;

.field private i:Landroid/database/DataSetObserver;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->f:Ljava/lang/Boolean;

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->g:Ljava/lang/Boolean;

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->h:Ljava/lang/Boolean;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/MessagesFragment;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesFragment;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MessagesFragment;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/MessagesFragment;->b(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/MessagesFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/MessagesFragment;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesFragment;->b(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/MessagesFragment;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/MessagesFragment;->c(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private b(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10

    const/16 v3, 0x8

    const/4 v9, 0x0

    const/4 v8, 0x1

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/lx;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/twitter/android/MessagesFragment;->d:Landroid/view/View;

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->d:Landroid/view/View;

    const v2, 0x7f0901e3    # com.twitter.android.R.id.message_row_container

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v7

    invoke-virtual {v0, p1}, Lcom/twitter/android/lx;->a(Ljava/lang/Long;)I

    move-result v2

    if-gez v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v9}, Landroid/widget/ListView;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/ListView;->getEmptyView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p1

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/android/lx;->a(Landroid/view/View;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Long;)V

    iput-object p1, p0, Lcom/twitter/android/MessagesFragment;->e:Ljava/lang/Long;

    if-eqz v1, :cond_1

    invoke-virtual {v1, v9}, Landroid/view/View;->setVisibility(I)V

    add-int/lit8 v0, v7, -0x1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    new-instance v2, Lcom/twitter/android/mp;

    move-object v3, p0

    move-object v4, p1

    move-object v5, p3

    move-object v6, p2

    move-object v7, p4

    invoke-direct/range {v2 .. v7}, Lcom/twitter/android/mp;-><init>(Lcom/twitter/android/MessagesFragment;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->e:Ljava/lang/Long;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->e:Ljava/lang/Long;

    invoke-virtual {v0, p1}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v1, :cond_3

    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    const-wide/16 v0, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->e:Ljava/lang/Long;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v7, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->d:Landroid/view/View;

    if-eqz v0, :cond_1

    add-int v0, v2, v7

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0, v8}, Landroid/widget/ListView;->setItemChecked(IZ)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->smoothScrollToPosition(I)V

    goto :goto_0
.end method

.method static synthetic c(Lcom/twitter/android/MessagesFragment;)Lcom/twitter/android/md;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    return-object v0
.end method

.method private c(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/MessagesDetailActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "owner_id"

    iget-wide v2, p0, Lcom/twitter/android/MessagesFragment;->Q:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_name"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_fullname"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_profile_image"

    invoke-virtual {v0, v1, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    invoke-interface {v1, v0}, Lcom/twitter/android/md;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method static synthetic d(Lcom/twitter/android/MessagesFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/MessagesFragment;)Landroid/support/v4/widget/CursorAdapter;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    return-object v0
.end method

.method private e(Z)V
    .locals 1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->h:Ljava/lang/Boolean;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->ap()V

    return-void
.end method


# virtual methods
.method public a(Landroid/content/DialogInterface;II)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    packed-switch p2, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "destroy_thread"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v1, -0x1

    if-ne p3, v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v7, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox:thread::delete"

    aput-object v5, v4, v6

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->q()V

    new-array v2, v7, [Ljava/lang/String;

    aput-object v0, v2, v6

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesFragment;->d(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    invoke-interface {v1, v0}, Lcom/twitter/android/md;->a_(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 3

    const/4 v2, 0x3

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->f:Ljava/lang/Boolean;

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/android/MessagesFragment;->c(I)Z

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/MessagesFragment;->e(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v2}, Lcom/twitter/android/MessagesFragment;->b(I)V

    invoke-direct {p0, v1}, Lcom/twitter/android/MessagesFragment;->e(Z)V

    goto :goto_0
.end method

.method public a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    invoke-interface {v1, v0}, Lcom/twitter/android/md;->a(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    const v0, 0x7f090323    # com.twitter.android.R.id.dm_mark_as_read

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->h:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/lx;

    if-eqz v0, :cond_0

    if-nez p3, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/lx;->a(Ljava/util/HashMap;Z)V

    :cond_0
    return-void

    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->f:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/MessagesFragment;->b(Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->q()V

    new-instance v0, Lcom/twitter/android/mo;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/mo;-><init>(Lcom/twitter/android/MessagesFragment;Ljava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->i:Landroid/database/DataSetObserver;

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->registerDataSetObserver(Landroid/database/DataSetObserver;)V

    return-void
.end method

.method protected a(Z)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x2

    const/4 v1, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    if-eqz p1, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->P()Z

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x1

    new-instance v2, Lcom/twitter/android/mr;

    invoke-direct {v2, p0, v3}, Lcom/twitter/android/mr;-><init>(Lcom/twitter/android/MessagesFragment;Lcom/twitter/android/mn;)V

    invoke-virtual {v0, v1, v3, v2}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesFragment;->a_(I)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/MessagesFragment;->h(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesFragment;->c(I)Z

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v2}, Lcom/twitter/android/MessagesFragment;->b(I)V

    goto :goto_0
.end method

.method protected b()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesFragment;->a(Z)V

    return-void
.end method

.method public c()V
    .locals 8

    const/4 v7, 0x0

    const/4 v6, 0x1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    iget-wide v2, p0, Lcom/twitter/android/MessagesFragment;->Q:J

    const-string/jumbo v4, "messages::::impression"

    const-string/jumbo v5, "ref_event"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(JLjava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "ref_event"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/mr;

    invoke-direct {v1, p0, v7}, Lcom/twitter/android/mr;-><init>(Lcom/twitter/android/MessagesFragment;Lcom/twitter/android/mn;)V

    invoke-virtual {v0, v6, v7, v1}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/MessagesFragment;->Q:J

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "messages::::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected c(I)Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p1}, Lcom/twitter/android/MessagesFragment;->a(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/MessagesFragment;->a(Ljava/lang/String;I)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/MessagesFragment;->a_(I)V

    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->clearChoices()V

    return-void
.end method

.method protected g()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->g()V

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0}, Lcom/twitter/android/MessagesFragment;->e(Z)V

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/MessagesFragment;->c(I)Z

    return-void
.end method

.method protected j()I
    .locals 1

    const/16 v0, 0xd

    return v0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    if-nez p1, :cond_0

    invoke-virtual {v0, v3}, Lcom/twitter/android/client/c;->b(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, v4}, Lcom/twitter/android/MessagesFragment;->a(Ljava/lang/String;I)V

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->k()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v3}, Lcom/twitter/android/MessagesFragment;->a(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/lx;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/lx;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->g:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f0300c1    # com.twitter.android.R.layout.message_row_placeholder_view

    invoke-virtual {v1, v2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MessagesFragment;->d:Landroid/view/View;

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->d:Landroid/view/View;

    const v2, 0x7f0901e3    # com.twitter.android.R.id.message_row_container

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemLongClickListener(Landroid/widget/AdapterView$OnItemLongClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_3

    new-instance v0, Lcom/twitter/android/ve;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/twitter/android/ve;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->b:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onAttach(Landroid/app/Activity;)V

    instance-of v0, p1, Lcom/twitter/android/md;

    if-eqz v0, :cond_0

    check-cast p1, Lcom/twitter/android/md;

    iput-object p1, p0, Lcom/twitter/android/MessagesFragment;->c:Lcom/twitter/android/md;

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 7

    const v5, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const/4 v4, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eq v0, v5, :cond_0

    const v1, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    if-ne v0, v1, :cond_1

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const-class v3, Lcom/twitter/android/DialogActivity;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "ff"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    if-ne v0, v5, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox::import:click"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/MessagesFragment;->startActivity(Landroid/content/Intent;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, "messages:inbox:user_list:import:click"

    aput-object v5, v4, v6

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/mq;

    invoke-direct {v0, p0}, Lcom/twitter/android/mq;-><init>(Lcom/twitter/android/MessagesFragment;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->O:Lcom/twitter/library/client/j;

    invoke-virtual {p0, v2, p0}, Lcom/twitter/android/MessagesFragment;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "extra_use_placeholder"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->g:Ljava/lang/Boolean;

    :cond_0
    invoke-virtual {p0, v2}, Lcom/twitter/android/MessagesFragment;->l(Z)V

    return-void
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    iget-wide v0, p0, Lcom/twitter/android/MessagesFragment;->Q:J

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/twitter/android/bl;

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v3, Lcom/twitter/library/provider/al;->a:Landroid/net/Uri;

    invoke-virtual {v3}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string/jumbo v5, "ownerId"

    invoke-virtual {v3, v5, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/android/lx;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/bl;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 8

    const v7, 0x7f090124    # com.twitter.android.R.id.import_contacts_btn

    const/4 v6, 0x0

    const/4 v5, 0x0

    const v0, 0x7f0300c8    # com.twitter.android.R.layout.messages_list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/MessagesFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f090046    # com.twitter.android.R.id.list_empty_text

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v2, Lcom/twitter/android/mn;

    invoke-direct {v2, p0}, Lcom/twitter/android/mn;-><init>(Lcom/twitter/android/MessagesFragment;)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    invoke-virtual {v1, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    const v3, 0x7f030050    # com.twitter.android.R.layout.dm_inbox_header

    invoke-static {v2, v3, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2, v5, v6}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const v4, 0x7f03004f    # com.twitter.android.R.layout.dm_inbox_footer

    invoke-static {v3, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v0, v3, v5, v6}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v2, 0x7f090120    # com.twitter.android.R.id.scan_contacts_view

    invoke-virtual {v3, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->b:Landroid/widget/ListView;

    return-object v1
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->a:Landroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p0}, Lcom/twitter/android/MessagesFragment;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z
    .locals 3

    const/4 v1, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const-string/jumbo v2, "thread_id"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x2

    invoke-static {v2, v0}, Lcom/twitter/android/MessagesDestroyThreadDialogFragment;->a(ILjava/lang/String;)Lcom/twitter/android/MessagesDestroyThreadDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/MessagesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    const-string/jumbo v2, "destroy_thread"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/MessagesFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public q()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->i:Landroid/database/DataSetObserver;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MessagesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    iget-object v1, p0, Lcom/twitter/android/MessagesFragment;->i:Landroid/database/DataSetObserver;

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->unregisterDataSetObserver(Landroid/database/DataSetObserver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/MessagesFragment;->i:Landroid/database/DataSetObserver;

    :cond_0
    return-void
.end method
