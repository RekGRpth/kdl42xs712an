.class Lcom/twitter/android/aae;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/a;


# instance fields
.field final synthetic a:Lcom/twitter/android/UsersFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/UsersFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onClick(Lcom/twitter/library/widget/BaseUserView;JI)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/UserApprovalView;

    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/twitter/android/aae;->onClick(Lcom/twitter/library/widget/UserApprovalView;JI)V

    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/UserApprovalView;JI)V
    .locals 7

    const/4 v2, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1, v2}, Lcom/twitter/library/widget/UserApprovalView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/zx;

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    move-wide v1, p2

    invoke-static/range {v0 .. v5}, Lcom/twitter/android/UsersFragment;->a(Lcom/twitter/android/UsersFragment;JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    const/4 v2, 0x3

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1, v5}, Lcom/twitter/library/widget/UserApprovalView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->G(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/client/c;->j(J)Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->getTag()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/android/zx;

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    invoke-virtual {p1}, Lcom/twitter/library/widget/UserApprovalView;->getPromotedContent()Lcom/twitter/library/api/PromotedContent;

    move-result-object v3

    move-wide v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/UsersFragment;->a(JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zx;Z)V

    goto :goto_1

    :cond_2
    invoke-virtual {p1, v6}, Lcom/twitter/library/widget/UserApprovalView;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    invoke-static {v0}, Lcom/twitter/android/UsersFragment;->H(Lcom/twitter/android/UsersFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/twitter/android/client/c;->k(J)Ljava/lang/String;

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/aae;->a:Lcom/twitter/android/UsersFragment;

    iget-object v0, v0, Lcom/twitter/android/UsersFragment;->j:Ljava/util/HashMap;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
