.class Lcom/twitter/android/rw;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/se;


# instance fields
.field private final a:Landroid/content/res/Resources;

.field private final b:Lcom/twitter/internal/android/widget/DockLayout;

.field private final c:Landroid/view/View;

.field private final d:Landroid/widget/RadioButton;

.field private final e:Lcom/twitter/android/SearchFragment;

.field private final f:Z


# direct methods
.method public constructor <init>(Landroid/content/res/Resources;Lcom/twitter/internal/android/widget/DockLayout;Landroid/view/View;Landroid/widget/RadioButton;Lcom/twitter/android/SearchFragment;Z)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/rw;->a:Landroid/content/res/Resources;

    iput-object p2, p0, Lcom/twitter/android/rw;->b:Lcom/twitter/internal/android/widget/DockLayout;

    iput-object p3, p0, Lcom/twitter/android/rw;->c:Landroid/view/View;

    iput-object p4, p0, Lcom/twitter/android/rw;->d:Landroid/widget/RadioButton;

    iput-object p5, p0, Lcom/twitter/android/rw;->e:Lcom/twitter/android/SearchFragment;

    iput-boolean p6, p0, Lcom/twitter/android/rw;->f:Z

    iget-object v0, p0, Lcom/twitter/android/rw;->d:Landroid/widget/RadioButton;

    new-instance v1, Lcom/twitter/android/rx;

    invoke-direct {v1, p0, p5}, Lcom/twitter/android/rx;-><init>(Lcom/twitter/android/rw;Lcom/twitter/android/SearchFragment;)V

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/rw;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/rw;->d:Landroid/widget/RadioButton;

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/RadioButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/rw;->d:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/twitter/android/rw;->a:Landroid/content/res/Resources;

    const v2, 0x7f0e0009    # com.twitter.android.R.plurals.new_tweet_count

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v5, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/rw;->c:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/rw;->e:Lcom/twitter/android/SearchFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->J()V

    goto :goto_0
.end method

.method public a(I)V
    .locals 6

    const/4 v5, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/rw;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/rw;->d:Landroid/widget/RadioButton;

    const v1, 0x7f0201d7    # com.twitter.android.R.drawable.ic_nav_new_dot

    invoke-virtual {v0, v5, v5, v5, v1}, Landroid/widget/RadioButton;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/rw;->b:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/DockLayout;->a()V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/rw;->d:Landroid/widget/RadioButton;

    iget-object v1, p0, Lcom/twitter/android/rw;->a:Landroid/content/res/Resources;

    const v2, 0x7f0e0009    # com.twitter.android.R.plurals.new_tweet_count

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, p1, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/RadioButton;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/rw;->c:Landroid/view/View;

    invoke-virtual {v0, v5}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/rw;->e:Lcom/twitter/android/SearchFragment;

    invoke-virtual {v0}, Lcom/twitter/android/SearchFragment;->K()V

    goto :goto_0
.end method
