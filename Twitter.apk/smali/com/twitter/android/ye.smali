.class public Lcom/twitter/android/ye;
.super Landroid/support/v4/widget/CursorAdapter;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/av;


# instance fields
.field private A:Z

.field private B:I

.field private C:Z

.field protected final a:Landroid/app/Activity;

.field protected final b:Lcom/twitter/android/client/c;

.field protected final c:Lcom/twitter/library/client/aa;

.field protected final d:Lcom/twitter/library/widget/ap;

.field protected final e:Lcom/twitter/library/widget/aa;

.field protected final f:Ljava/util/ArrayList;

.field protected g:Z

.field private final h:Ljava/util/ArrayList;

.field private final i:Lcom/twitter/android/vs;

.field private final j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:I

.field private o:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;

.field private s:I

.field private t:Lcom/twitter/android/ob;

.field private u:I

.field private v:Z

.field private w:Z

.field private x:Z

.field private y:Lcom/twitter/library/util/FriendshipCache;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;)V
    .locals 15

    const v12, 0x7f030154    # com.twitter.android.R.layout.timeline_gap

    const v13, 0x7f030161    # com.twitter.android.R.layout.tweet_row_view

    const/4 v14, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v14}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;IIZ)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;II)V
    .locals 15

    const/4 v14, 0x1

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v12, p12

    move/from16 v13, p13

    invoke-direct/range {v0 .. v14}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;IIZ)V

    return-void
.end method

.method public constructor <init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;IIZ)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p2}, Landroid/support/v4/widget/CursorAdapter;-><init>(Landroid/content/Context;Landroid/database/Cursor;I)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/ye;->h:Ljava/util/ArrayList;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ye;->n:I

    const v0, 0x7fffffff

    iput v0, p0, Lcom/twitter/android/ye;->B:I

    iput-object p1, p0, Lcom/twitter/android/ye;->a:Landroid/app/Activity;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/ye;->c:Lcom/twitter/library/client/aa;

    iput-object p7, p0, Lcom/twitter/android/ye;->d:Lcom/twitter/library/widget/ap;

    iput-object p8, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    iput-object p9, p0, Lcom/twitter/android/ye;->e:Lcom/twitter/library/widget/aa;

    iput-boolean p6, p0, Lcom/twitter/android/ye;->x:Z

    invoke-virtual {p8}, Lcom/twitter/android/client/c;->af()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ye;->v:Z

    iget-boolean v0, p0, Lcom/twitter/android/ye;->v:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ye;->k:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ye;->l:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/ye;->m:Z

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ye;->c:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/library/api/UserSettings;->k:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/ye;->w:Z

    iput-object p10, p0, Lcom/twitter/android/ye;->i:Lcom/twitter/android/vs;

    iput p12, p0, Lcom/twitter/android/ye;->j:I

    iput p13, p0, Lcom/twitter/android/ye;->u:I

    iput-object p11, p0, Lcom/twitter/android/ye;->y:Lcom/twitter/library/util/FriendshipCache;

    invoke-static {}, Lgv;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/ye;->g:Z

    sget-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    iput-object v0, p0, Lcom/twitter/android/ye;->o:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    iput-boolean p14, p0, Lcom/twitter/android/ye;->C:Z

    return-void

    :cond_0
    iput-boolean p3, p0, Lcom/twitter/android/ye;->k:Z

    iput-boolean p4, p0, Lcom/twitter/android/ye;->l:Z

    iput-boolean p5, p0, Lcom/twitter/android/ye;->m:Z

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public constructor <init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;Z)V
    .locals 15

    const v12, 0x7f030154    # com.twitter.android.R.layout.timeline_gap

    const v13, 0x7f030161    # com.twitter.android.R.layout.tweet_row_view

    move-object v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p2

    move/from16 v3, p3

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v9, p9

    move-object/from16 v10, p10

    move-object/from16 v11, p11

    move/from16 v14, p12

    invoke-direct/range {v0 .. v14}, Lcom/twitter/android/ye;-><init>(Landroid/app/Activity;IZZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;IIZ)V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/widget/TweetView;Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "position"

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;)Lcom/twitter/library/provider/Tweet;
    .locals 7

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    new-instance v5, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v5, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    iget-boolean v0, p0, Lcom/twitter/android/ye;->v:Z

    if-eqz v0, :cond_0

    iget v0, v5, Lcom/twitter/library/provider/Tweet;->I:I

    and-int/lit8 v0, v0, -0x9

    iput v0, v5, Lcom/twitter/library/provider/Tweet;->I:I

    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    iget-object v6, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v1, p0, Lcom/twitter/android/ye;->y:Lcom/twitter/library/util/FriendshipCache;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/ye;->y:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v1, v5}, Lcom/twitter/library/util/FriendshipCache;->a(Lcom/twitter/library/provider/Tweet;)V

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->ab()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ye;->y:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setFriendshipCache(Lcom/twitter/library/util/FriendshipCache;)V

    :cond_2
    iget-boolean v1, p0, Lcom/twitter/android/ye;->C:Z

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setShouldSimulateInlineActions(Z)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->k:Z

    if-eqz v1, :cond_6

    iget-boolean v1, p0, Lcom/twitter/android/ye;->v:Z

    if-nez v1, :cond_6

    move v1, v2

    :goto_0
    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpand(Z)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->l:Z

    if-eqz v1, :cond_7

    iget-boolean v1, p0, Lcom/twitter/android/ye;->v:Z

    if-nez v1, :cond_7

    move v1, v2

    :goto_1
    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandMedia(Z)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->v:Z

    if-eqz v1, :cond_8

    move v1, v3

    :goto_2
    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setExpandCardMediaType(I)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->z:Z

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    iget-object v1, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->ad()Z

    move-result v1

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setSmartCrop(Z)V

    iget-object v1, p0, Lcom/twitter/android/ye;->o:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setFullPreview(Lcom/twitter/library/widget/TweetView$FullPreviewMode;)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->m:Z

    if-eqz v1, :cond_9

    iget-boolean v1, p0, Lcom/twitter/android/ye;->v:Z

    if-nez v1, :cond_9

    :goto_3
    invoke-virtual {v6, v2}, Lcom/twitter/library/widget/TweetView;->setAlwaysExpandPromotedCardsMedia(Z)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->g:Z

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setSimplifyUrls(Z)V

    iget-object v1, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->al()Z

    move-result v1

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setAmplifyEnabled(Z)V

    iget-boolean v1, p0, Lcom/twitter/android/ye;->A:Z

    if-eqz v1, :cond_3

    invoke-virtual {v6, v3}, Lcom/twitter/library/widget/TweetView;->setTweetCountMode(I)V

    :cond_3
    iget v1, p0, Lcom/twitter/android/ye;->B:I

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setViewCountThreshold(I)V

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->V()F

    move-result v1

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setContentSize(F)V

    iget-object v1, p0, Lcom/twitter/android/ye;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/ye;->c:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v5, v1, v2}, Lcom/twitter/library/provider/Tweet;->a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Z

    move-result v1

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setWillTranslate(Z)V

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->V()Lcom/twitter/library/card/instance/CardInstanceData;

    move-result-object v2

    if-eqz v2, :cond_a

    invoke-virtual {v6, v5, v2}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/card/instance/CardInstanceData;)Z

    move-result v1

    if-eqz v1, :cond_a

    new-instance v1, Lcom/twitter/android/card/u;

    iget-object v3, p0, Lcom/twitter/android/ye;->a:Landroid/app/Activity;

    invoke-direct {v1, v3}, Lcom/twitter/android/card/u;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v1, v5}, Lcom/twitter/android/card/u;->a(Lcom/twitter/library/provider/Tweet;)V

    invoke-virtual {v1, v6}, Lcom/twitter/android/card/u;->a(Lcom/twitter/library/widget/TweetView;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/card/u;->a(Lcom/twitter/library/card/instance/CardInstanceData;)V

    invoke-virtual {v1, v6}, Lcom/twitter/android/card/u;->a(Landroid/view/ViewGroup;)V

    const v2, 0x7f030024    # com.twitter.android.R.layout.card_view

    invoke-virtual {v1, v2}, Lcom/twitter/android/card/u;->a(I)V

    :goto_4
    iget-boolean v2, p0, Lcom/twitter/android/ye;->p:Z

    invoke-virtual {v6, v5, v2, v1}, Lcom/twitter/library/widget/TweetView;->a(Lcom/twitter/library/provider/Tweet;ZLcom/twitter/library/card/k;)V

    invoke-virtual {v6}, Lcom/twitter/library/widget/TweetView;->getContentDescription()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-boolean v1, v4, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v6, v1}, Lcom/twitter/library/widget/TweetView;->setRenderRTL(Z)V

    iget-object v1, p0, Lcom/twitter/android/ye;->t:Lcom/twitter/android/ob;

    if-eqz v1, :cond_4

    invoke-virtual {p0, v6, p2}, Lcom/twitter/android/ye;->a(Lcom/twitter/library/widget/TweetView;Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/ye;->t:Lcom/twitter/android/ob;

    invoke-interface {v2, p1, v5, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    :cond_4
    iget-object v1, p0, Lcom/twitter/android/ye;->i:Lcom/twitter/android/vs;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/twitter/android/ye;->i:Lcom/twitter/android/vs;

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/twitter/android/ye;->getItemId(I)J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/twitter/android/vs;->b(Lcom/twitter/android/yd;J)V

    :cond_5
    return-object v5

    :cond_6
    move v1, v3

    goto/16 :goto_0

    :cond_7
    move v1, v3

    goto/16 :goto_1

    :cond_8
    iget v1, p0, Lcom/twitter/android/ye;->n:I

    goto/16 :goto_2

    :cond_9
    move v2, v3

    goto/16 :goto_3

    :cond_a
    const/4 v1, 0x0

    goto :goto_4
.end method

.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->a()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected a(Landroid/view/View;)V
    .locals 3

    new-instance v0, Lcom/twitter/android/yd;

    invoke-direct {v0, p1}, Lcom/twitter/android/yd;-><init>(Landroid/view/View;)V

    iget-object v1, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/ye;->d:Lcom/twitter/library/widget/ap;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setProvider(Lcom/twitter/library/widget/ap;)V

    iget-object v2, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setRenderRTL(Z)V

    iget-boolean v2, p0, Lcom/twitter/android/ye;->w:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setDisplaySensitiveMedia(Z)V

    iget-object v2, p0, Lcom/twitter/android/ye;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->ad()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setSmartCrop(Z)V

    iget-object v2, p0, Lcom/twitter/android/ye;->o:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setFullPreview(Lcom/twitter/library/widget/TweetView$FullPreviewMode;)V

    iget-object v2, p0, Lcom/twitter/android/ye;->e:Lcom/twitter/library/widget/aa;

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setOnProfileImageClickListener(Lcom/twitter/library/widget/aa;)V

    iget-boolean v2, p0, Lcom/twitter/android/ye;->x:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setShowRetweetSocialProofToOwner(Z)V

    iget-boolean v2, p0, Lcom/twitter/android/ye;->C:Z

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setShouldSimulateInlineActions(Z)V

    invoke-virtual {p1, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;I)V
    .locals 6

    const/16 v5, 0x8

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yf;

    iget-object v1, v0, Lcom/twitter/android/yf;->b:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v1, p0, Lcom/twitter/android/ye;->h:Ljava/util/ArrayList;

    const/4 v2, 0x1

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/yf;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v4}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/yf;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v1, v0, Lcom/twitter/android/yf;->a:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v5}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, v0, Lcom/twitter/android/yf;->c:Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_3

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :cond_0
    :goto_1
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    :cond_1
    iget-object v1, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getUserImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v1, p3}, Lcom/twitter/library/widget/TweetView;->a(Landroid/graphics/Bitmap;Z)V

    :cond_2
    iget-object v1, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/TweetView;->getSummaryImageKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0, p3}, Lcom/twitter/library/widget/TweetView;->b(Landroid/graphics/Bitmap;Z)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ye;->q:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ye;->q:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/ye;->q:Ljava/lang/String;

    iput-object p2, p0, Lcom/twitter/android/ye;->r:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/library/widget/TweetView;->a(Ljava/util/HashMap;Z)Z

    goto :goto_1

    :cond_1
    return-void
.end method

.method protected a(Landroid/database/Cursor;)Z
    .locals 2

    const/16 v0, 0x12

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lcom/twitter/android/ob;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ye;->t:Lcom/twitter/android/ob;

    return-void
.end method

.method public b(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ye;->v:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ye;->v:Z

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 1

    invoke-virtual {p0, p3}, Lcom/twitter/android/ye;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    const v0, 0x7f0202ed    # com.twitter.android.R.drawable.timeline_gap_bg

    invoke-virtual {p0, p1, p3, v0}, Lcom/twitter/android/ye;->a(Landroid/view/View;Landroid/database/Cursor;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1, p3}, Lcom/twitter/android/ye;->a(Landroid/view/View;Landroid/database/Cursor;)Lcom/twitter/library/provider/Tweet;

    goto :goto_0
.end method

.method protected c()Lcom/twitter/library/util/FriendshipCache;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ye;->y:Lcom/twitter/library/util/FriendshipCache;

    return-object v0
.end method

.method public c(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/ye;->s:I

    and-int/2addr v0, p1

    if-eq v0, p1, :cond_0

    iget v0, p0, Lcom/twitter/android/ye;->s:I

    or-int/2addr v0, p1

    iput v0, p0, Lcom/twitter/android/ye;->s:I

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public c(J)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ye;->h:Ljava/util/ArrayList;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public c(Z)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/ye;->p:Z

    if-eq v0, p1, :cond_1

    iput-boolean p1, p0, Lcom/twitter/android/ye;->p:Z

    iget-boolean v0, p0, Lcom/twitter/android/ye;->p:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->r()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public d()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->b()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public d(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/ye;->n:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/android/ye;->n:I

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public d(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ye;->k:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ye;->k:Z

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ye;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->p()V

    goto :goto_1

    :cond_1
    return-void
.end method

.method public e(I)V
    .locals 1

    iget v0, p0, Lcom/twitter/android/ye;->B:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/android/ye;->B:I

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public e(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ye;->l:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ye;->l:Z

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public f()Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ye;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method public f(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ye;->m:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ye;->m:Z

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ye;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method public g(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ye;->g:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ye;->g:Z

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public getCount()I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ye;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    invoke-super {p0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 3

    const-wide/16 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->getCount()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-wide v0, v1

    :goto_0
    return-wide v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/ye;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_2

    const/16 v1, 0x15

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    goto :goto_0

    :cond_2
    move-wide v0, v1

    goto :goto_0
.end method

.method public getItemViewType(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ye;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0, p1}, Lcom/twitter/android/ye;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-virtual {p0, v0}, Lcom/twitter/android/ye;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 10

    const v9, 0x7f0f04ea    # com.twitter.android.R.string.tweets_protected_body

    const v8, 0x7f090036    # com.twitter.android.R.id.content

    const v3, 0x7f03015f    # com.twitter.android.R.layout.tweet_message_row_view

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/ye;->mContext:Landroid/content/Context;

    if-nez p2, :cond_0

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_0
    iget-object v2, p0, Lcom/twitter/android/ye;->r:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/ye;->q:Ljava/lang/String;

    new-instance v4, Landroid/text/SpannableStringBuilder;

    invoke-direct {v4}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const v0, 0x7f0f04eb    # com.twitter.android.R.string.tweets_protected_title

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {v5, v0, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    new-array v3, v7, [Ljava/lang/Object;

    aput-object v2, v3, v6

    invoke-virtual {v5, v9, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :goto_0
    new-instance v2, Lcom/twitter/internal/android/widget/TypefacesSpan;

    invoke-direct {v2, v1, v7}, Lcom/twitter/internal/android/widget/TypefacesSpan;-><init>(Landroid/content/Context;I)V

    const/16 v1, 0x21

    invoke-virtual {v4, v2, v6, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    return-object p2

    :cond_1
    const v0, 0x7f0f04eb    # com.twitter.android.R.string.tweets_protected_title

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v3, v2, v6

    invoke-virtual {v5, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v4}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    new-array v2, v7, [Ljava/lang/Object;

    aput-object v3, v2, v6

    invoke-virtual {v5, v9, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/ye;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/twitter/android/ye;->mContext:Landroid/content/Context;

    if-nez p2, :cond_3

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    :cond_3
    invoke-virtual {p2, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f04e9    # com.twitter.android.R.string.tweets_not_yet

    new-array v3, v7, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/ye;->q:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_4
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/widget/CursorAdapter;->getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    goto :goto_1
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public h(Z)V
    .locals 1

    if-nez p1, :cond_0

    sget-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->a:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    iput-object v0, p0, Lcom/twitter/android/ye;->o:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    :goto_0
    return-void

    :cond_0
    sget-object v0, Lcom/twitter/library/widget/TweetView$FullPreviewMode;->b:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    iput-object v0, p0, Lcom/twitter/android/ye;->o:Lcom/twitter/library/widget/TweetView$FullPreviewMode;

    goto :goto_0
.end method

.method public h()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ye;->s:I

    and-int/lit8 v0, v0, 0x1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public i(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/ye;->A:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/ye;->A:Z

    invoke-virtual {p0}, Lcom/twitter/android/ye;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public i()Z
    .locals 1

    iget v0, p0, Lcom/twitter/android/ye;->s:I

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isEnabled(I)Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/ye;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/ye;->i()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->isEnabled(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public j()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ye;->c:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_four_tweet_modules_1377"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_summary_cards_media_forward_1531"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_media_cards_media_forward_1567"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_promoted_website_card_1832"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p0, p2}, Lcom/twitter/android/ye;->a(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/ye;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/yf;

    invoke-direct {v1, v0}, Lcom/twitter/android/yf;-><init>(Landroid/view/View;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/ye;->u:I

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/ye;->a(Landroid/view/View;)V

    goto :goto_0
.end method

.method public swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/ye;->s:I

    invoke-virtual {p0}, Lcom/twitter/android/ye;->j()V

    invoke-super {p0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
