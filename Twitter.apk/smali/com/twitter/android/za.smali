.class Lcom/twitter/android/za;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Lcom/twitter/library/util/m;

.field public b:I

.field public final c:J

.field public final d:Lcom/twitter/library/widget/ActionButton;

.field public final e:Landroid/widget/ImageView;

.field public final f:Landroid/widget/ImageView;

.field public final g:Landroid/widget/TextView;

.field public final h:Landroid/widget/TextView;

.field public final i:Landroid/widget/TextView;

.field public final j:Landroid/widget/ImageView;

.field public final k:Lcom/twitter/library/widget/SocialBylineView;

.field public final l:Landroid/widget/TextView;

.field private final m:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;J)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/za;->m:Landroid/content/Context;

    iput-wide p3, p0, Lcom/twitter/android/za;->c:J

    const v0, 0x7f09002c    # com.twitter.android.R.id.action_button

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/ActionButton;

    iput-object v0, p0, Lcom/twitter/android/za;->d:Lcom/twitter/library/widget/ActionButton;

    const v0, 0x7f090054    # com.twitter.android.R.id.screenname_item

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/za;->h:Landroid/widget/TextView;

    const v0, 0x7f09004e    # com.twitter.android.R.id.protected_item

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/za;->e:Landroid/widget/ImageView;

    const v0, 0x7f090060    # com.twitter.android.R.id.verified_item

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/za;->f:Landroid/widget/ImageView;

    const v0, 0x7f090049    # com.twitter.android.R.id.name_item

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/za;->g:Landroid/widget/TextView;

    const v0, 0x7f09005e    # com.twitter.android.R.id.user_image

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/za;->j:Landroid/widget/ImageView;

    const v0, 0x7f09004d    # com.twitter.android.R.id.promoted

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/za;->i:Landroid/widget/TextView;

    const v0, 0x7f090059    # com.twitter.android.R.id.social_byline

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SocialBylineView;

    iput-object v0, p0, Lcom/twitter/android/za;->k:Lcom/twitter/library/widget/SocialBylineView;

    const v0, 0x7f09005c    # com.twitter.android.R.id.user_bio

    invoke-virtual {p2, v0}, Landroid/widget/RelativeLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/za;->l:Landroid/widget/TextView;

    return-void
.end method


# virtual methods
.method public a()V
    .locals 2

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/twitter/android/za;->k:Lcom/twitter/library/widget/SocialBylineView;

    invoke-virtual {v1}, Lcom/twitter/library/widget/SocialBylineView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/za;->i:Landroid/widget/TextView;

    invoke-virtual {v1}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_1

    add-int/lit8 v0, v0, 0x1

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/za;->l:Landroid/widget/TextView;

    rsub-int/lit8 v0, v0, 0x3

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setMaxLines(I)V

    return-void
.end method

.method public a(IILjava/lang/String;IZ)V
    .locals 7

    const/16 v3, 0x8

    const/4 v6, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/za;->m:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/za;->k:Lcom/twitter/library/widget/SocialBylineView;

    if-lez p2, :cond_1

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, p2}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    packed-switch p1, :pswitch_data_0

    invoke-virtual {v1, v3}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    :goto_0
    invoke-virtual {v1, p5}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    :goto_1
    return-void

    :pswitch_0
    if-lez p4, :cond_0

    const v2, 0x7f0e0002    # com.twitter.android.R.plurals.followed_by_count

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v2, p4, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    :goto_2
    invoke-virtual {v1, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_0

    :cond_0
    const v2, 0x7f0f01a4    # com.twitter.android.R.string.followed_by

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    goto :goto_2

    :pswitch_1
    const v2, 0x7f0f0452    # com.twitter.android.R.string.social_both_follow

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_0

    :pswitch_2
    const v2, 0x7f0f045d    # com.twitter.android.R.string.social_follow_and_follow

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_0

    :pswitch_3
    const v2, 0x7f0f0470    # com.twitter.android.R.string.social_similar_to

    new-array v3, v6, [Ljava/lang/Object;

    aput-object p3, v3, v5

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v3}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public a(ILandroid/content/res/Resources;)V
    .locals 5

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v3, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/za;->d:Lcom/twitter/library/widget/ActionButton;

    invoke-static {p1}, Lcom/twitter/library/provider/ay;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/ActionButton;->setEnabled(Z)V

    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    const v1, 0x7f0201a6    # com.twitter.android.R.drawable.ic_follow_action_checked

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setImageResource(I)V

    invoke-static {v0, v4}, Lcom/twitter/android/yu;->a(Landroid/view/View;F)V

    :goto_0
    iput p1, p0, Lcom/twitter/android/za;->b:I

    return-void

    :cond_0
    invoke-static {p1}, Lcom/twitter/library/provider/ay;->e(I)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setEnabled(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    const v1, 0x7f020126    # com.twitter.android.R.drawable.ic_blocked_default

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setImageResource(I)V

    const v1, 0x3ecccccd    # 0.4f

    invoke-static {v0, v1}, Lcom/twitter/android/yu;->a(Landroid/view/View;F)V

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3}, Lcom/twitter/library/widget/ActionButton;->setEnabled(Z)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    const v1, 0x7f0201a7    # com.twitter.android.R.drawable.ic_follow_action_default

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/ActionButton;->setImageResource(I)V

    invoke-static {v0, v4}, Lcom/twitter/android/yu;->a(Landroid/view/View;F)V

    goto :goto_0
.end method

.method public a(IZ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/za;->k:Lcom/twitter/library/widget/SocialBylineView;

    if-lez p1, :cond_0

    invoke-virtual {v0, p1}, Lcom/twitter/library/widget/SocialBylineView;->setIcon(I)V

    iget-object v1, p0, Lcom/twitter/android/za;->m:Landroid/content/Context;

    const v2, 0x7f0f046a    # com.twitter.android.R.string.social_follows_you

    invoke-virtual {v1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setLabel(Ljava/lang/CharSequence;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    invoke-virtual {v0, p2}, Lcom/twitter/library/widget/SocialBylineView;->setRenderRTL(Z)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SocialBylineView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/api/PromotedContent;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/za;->i:Landroid/widget/TextView;

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/za;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/za;->h:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v2, 0x40

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/za;->g:Landroid/widget/TextView;

    invoke-virtual {v0, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/za;->e:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/za;->e:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method public b(Z)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/za;->f:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/za;->f:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0
.end method
