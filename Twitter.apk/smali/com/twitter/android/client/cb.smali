.class Lcom/twitter/android/client/cb;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Lcom/twitter/library/provider/Tweet;

.field private final b:Landroid/widget/TextView;

.field private final c:Landroid/widget/TextView;

.field private final d:Landroid/widget/ImageView;

.field private e:Lcom/twitter/library/util/m;


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f090097    # com.twitter.android.R.id.name

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/client/cb;->b:Landroid/widget/TextView;

    const v0, 0x7f090036    # com.twitter.android.R.id.content

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/client/cb;->c:Landroid/widget/TextView;

    const v0, 0x7f090138    # com.twitter.android.R.id.background

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/client/cb;->d:Landroid/widget/ImageView;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/cb;)Landroid/widget/ImageView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/cb;->d:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/cb;Lcom/twitter/library/util/m;)Lcom/twitter/library/util/m;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/cb;->e:Lcom/twitter/library/util/m;

    return-object p1
.end method

.method static synthetic b(Lcom/twitter/android/client/cb;)Lcom/twitter/library/util/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/cb;->e:Lcom/twitter/library/util/m;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/provider/Tweet;JLcom/twitter/library/util/aa;)V
    .locals 8

    const/4 v6, 0x0

    const/4 v1, 0x1

    const/4 v3, 0x0

    iput-object p1, p0, Lcom/twitter/android/client/cb;->a:Lcom/twitter/library/provider/Tweet;

    iget-object v0, p0, Lcom/twitter/android/client/cb;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v7, p0, Lcom/twitter/android/client/cb;->c:Landroid/widget/TextView;

    move-object v0, p1

    move v2, v1

    move v4, v3

    move v5, v3

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/Tweet;->a(ZZZZZ)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->q()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->T()Lcom/twitter/library/api/MediaEntity;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/provider/Tweet;->l()Lcom/twitter/library/api/TweetClassicCard;

    move-result-object v2

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/api/MediaEntity;->mediaUrl:Ljava/lang/String;

    :goto_0
    move-object v2, v0

    :goto_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v6, p0, Lcom/twitter/android/client/cb;->d:Landroid/widget/ImageView;

    new-instance v0, Lcom/twitter/android/client/cc;

    move-object v1, p0

    move-object v3, p4

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/client/cc;-><init>(Lcom/twitter/android/client/cb;Ljava/lang/String;Lcom/twitter/library/util/aa;J)V

    invoke-virtual {v6, v0}, Landroid/widget/ImageView;->post(Ljava/lang/Runnable;)Z

    :goto_2
    return-void

    :cond_0
    if-eqz v2, :cond_1

    iget v0, v2, Lcom/twitter/library/api/TweetClassicCard;->type:I

    if-ne v0, v1, :cond_1

    iget-object v0, v2, Lcom/twitter/library/api/TweetClassicCard;->imageUrl:Ljava/lang/String;

    goto :goto_0

    :cond_1
    move-object v0, v6

    goto :goto_0

    :cond_2
    move-object v2, v6

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/client/cb;->d:Landroid/widget/ImageView;

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/cb;->e:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/cb;->d:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method
