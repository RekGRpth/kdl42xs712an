.class public Lcom/twitter/android/client/ao;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;Z)Ljava/lang/String;
    .locals 3

    if-nez p1, :cond_0

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object p1

    :cond_0
    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/geo/b;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/geo/b;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1, p7}, Lcom/twitter/library/api/geo/b;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    invoke-virtual {v1, p9}, Lcom/twitter/library/api/geo/a;->b(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    invoke-virtual {v1, p8}, Lcom/twitter/library/api/geo/a;->a(I)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    invoke-static {}, Lcom/twitter/android/client/ao;->b()Z

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/geo/a;->a(Z)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    invoke-virtual {v1, p10}, Lcom/twitter/library/api/geo/a;->b(Z)Lcom/twitter/library/api/geo/a;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/api/geo/a;->c(I)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "lat"

    invoke-virtual {v1, v2, p3, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;D)Lcom/twitter/library/service/b;

    move-result-object v1

    const-string/jumbo v2, "long"

    invoke-virtual {v1, v2, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;D)Lcom/twitter/library/service/b;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/android/client/ap;DDLjava/lang/String;ILjava/lang/String;ZLjava/lang/String;)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/api/geo/c;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/api/geo/c;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    invoke-virtual {v0, p11}, Lcom/twitter/library/api/geo/c;->c(Ljava/lang/String;)Lcom/twitter/library/api/geo/c;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/twitter/library/api/geo/c;->a(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v0

    invoke-virtual {v0, p9}, Lcom/twitter/library/api/geo/a;->b(Ljava/lang/String;)Lcom/twitter/library/api/geo/a;

    move-result-object v0

    invoke-virtual {v0, p8}, Lcom/twitter/library/api/geo/a;->a(I)Lcom/twitter/library/api/geo/a;

    move-result-object v0

    invoke-static {}, Lcom/twitter/android/client/ao;->b()Z

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/geo/a;->a(Z)Lcom/twitter/library/api/geo/a;

    move-result-object v0

    invoke-virtual {v0, p10}, Lcom/twitter/library/api/geo/a;->b(Z)Lcom/twitter/library/api/geo/a;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/twitter/library/api/geo/a;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "lat"

    invoke-virtual {v0, v2, p3, p4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;D)Lcom/twitter/library/service/b;

    move-result-object v0

    const-string/jumbo v2, "long"

    invoke-virtual {v0, v2, p5, p6}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;D)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {v1, v0, p2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a()Z
    .locals 1

    const-string/jumbo v0, "android_poi_compose_1922"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    const-string/jumbo v0, "android_poi_compose_1922"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private static b()Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/android/client/ao;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
