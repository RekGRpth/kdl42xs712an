.class public Lcom/twitter/android/client/BaseListFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/widget/AbsListView$OnScrollListener;
.implements Lcom/twitter/internal/android/widget/as;
.implements Lcom/twitter/library/client/o;
.implements Lcom/twitter/library/util/ar;
.implements Lcom/twitter/refresh/widget/b;


# instance fields
.field private A:Lcom/twitter/android/util/u;

.field private B:Lcom/twitter/library/client/m;

.field private C:Lcom/twitter/android/client/q;

.field private D:Z

.field private E:I

.field private F:I

.field private G:I

.field private H:Z

.field private I:Z

.field private final J:Lcom/twitter/library/view/i;

.field private final K:Landroid/os/Handler;

.field private final L:Ljava/lang/Runnable;

.field protected final M:Lcom/twitter/android/client/au;

.field protected N:Z

.field protected O:Lcom/twitter/library/client/j;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field protected P:Landroid/support/v4/widget/CursorAdapter;

.field protected Q:J

.field protected R:I

.field protected S:Lcom/twitter/library/scribe/ScribeAssociation;

.field protected T:Lcom/twitter/android/client/ag;

.field private final U:Ljava/lang/Runnable;

.field private V:Z

.field private W:Z

.field private X:Z

.field private Y:Ljava/util/LinkedHashSet;

.field private Z:Z

.field private a:Lcom/twitter/android/PromptView;

.field private aa:Lcom/twitter/refresh/widget/e;

.field private ab:I

.field private ac:Ljava/util/LinkedHashSet;

.field private ad:Ljava/util/LinkedHashSet;

.field private ae:Landroid/view/View$OnTouchListener;

.field private af:Lcom/twitter/android/th;

.field private ag:Lcom/twitter/library/client/z;

.field private ah:Landroid/view/View;

.field private b:Lcom/twitter/android/client/c;

.field private c:Landroid/graphics/Bitmap;

.field private d:Z

.field private e:Landroid/widget/ListView;

.field private f:Z

.field private g:Z

.field private h:I

.field private i:I

.field private j:Landroid/view/View;

.field private k:Landroid/view/View;

.field private l:Landroid/widget/ProgressBar;

.field private m:Ljava/util/ArrayList;

.field private n:Lcom/twitter/android/client/ae;

.field private o:I

.field private p:Z

.field private q:Z

.field private r:Z

.field private s:Z

.field private t:Z

.field private u:Lcom/twitter/library/api/Prompt;

.field private v:Lcom/twitter/android/client/ai;

.field private w:I

.field private x:Lcom/twitter/library/client/aa;

.field private y:Lcom/twitter/library/service/c;

.field private z:Lcom/twitter/library/client/w;


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/twitter/android/client/au;

    invoke-direct {v0}, Lcom/twitter/android/client/au;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->M:Lcom/twitter/android/client/au;

    const/4 v0, 0x0

    iput v0, p0, Lcom/twitter/android/client/BaseListFragment;->o:I

    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->H:Z

    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->I:Z

    new-instance v0, Lcom/twitter/android/client/aa;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/aa;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->J:Lcom/twitter/library/view/i;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    new-instance v0, Lcom/twitter/android/client/ab;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/ab;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->L:Ljava/lang/Runnable;

    new-instance v0, Lcom/twitter/android/client/ac;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/ac;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->U:Ljava/lang/Runnable;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->Y:Ljava/util/LinkedHashSet;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ac:Ljava/util/LinkedHashSet;

    new-instance v0, Ljava/util/LinkedHashSet;

    invoke-direct {v0}, Ljava/util/LinkedHashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ad:Ljava/util/LinkedHashSet;

    new-instance v0, Lcom/twitter/android/client/ad;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/ad;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ae:Landroid/view/View$OnTouchListener;

    return-void
.end method

.method public static a(Landroid/content/Intent;Z)Landroid/os/Bundle;
    .locals 3
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    :cond_0
    const-string/jumbo v1, "data"

    invoke-virtual {p0}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/client/BaseListFragment;)Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    const/4 v0, -0x1

    invoke-static {p0, p1, p2, v0}, Lcom/twitter/android/client/BaseListFragment;->a(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static a(Ljava/lang/String;Ljava/lang/String;II)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    if-ne p2, p3, :cond_0

    const-string/jumbo v1, "get_middle"

    :goto_0
    if-nez v1, :cond_1

    :goto_1
    return-object v0

    :cond_0
    packed-switch p2, :pswitch_data_0

    move-object v1, v0

    goto :goto_0

    :pswitch_0
    const-string/jumbo v1, "get_newer"

    goto :goto_0

    :pswitch_1
    const-string/jumbo v1, "get_older"

    goto :goto_0

    :pswitch_2
    const-string/jumbo v1, "get_initial"

    goto :goto_0

    :cond_1
    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    const/4 v3, 0x2

    aput-object v0, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    const/4 v0, 0x4

    aput-object v1, v2, v0

    invoke-static {v2}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private a(II)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/client/q;->a(Landroid/content/Context;)Lcom/twitter/library/client/q;

    move-result-object v1

    if-lez p1, :cond_0

    invoke-virtual {v1, p1}, Lcom/twitter/library/client/q;->a(I)Lcom/twitter/library/client/m;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Lcom/twitter/library/client/m;->c()Lcom/twitter/library/client/r;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/r;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/library/service/p;->a(Lcom/twitter/library/client/Session;)Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/q;->a(Lcom/twitter/library/client/m;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    if-nez v0, :cond_1

    if-lez p2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->x()Lcom/twitter/library/client/r;

    move-result-object v0

    invoke-virtual {v1, p2}, Lcom/twitter/library/client/q;->b(I)Lcom/twitter/library/client/m;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v1, v0}, Lcom/twitter/library/client/m;->a(Lcom/twitter/library/client/r;)Lcom/twitter/library/client/m;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v0, p0}, Lcom/twitter/library/client/m;->a(Lcom/twitter/library/client/o;)Lcom/twitter/library/client/m;

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->e()V

    :cond_2
    return-void

    :cond_3
    iput-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    goto :goto_0
.end method

.method private b(J)V
    .locals 4

    const/4 v1, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_3

    const-string/jumbo v2, "auto_ref"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->au()Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2, p1, p2}, Lcom/twitter/library/client/aa;->a(J)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    iget-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    if-eqz v3, :cond_4

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->am()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v1, v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->a()I

    move-result v0

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(II)V

    :cond_2
    :goto_1
    return-void

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    if-eqz v2, :cond_2

    if-lez v0, :cond_2

    invoke-direct {p0, v1, v0}, Lcom/twitter/android/client/BaseListFragment;->a(II)V

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/client/BaseListFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/client/BaseListFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->q:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/client/BaseListFragment;)Ljava/util/LinkedHashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ac:Ljava/util/LinkedHashSet;

    return-object v0
.end method

.method private e()Landroid/os/Bundle;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->setArguments(Landroid/os/Bundle;)V

    :cond_0
    return-object v0
.end method


# virtual methods
.method public A()Lcom/twitter/refresh/widget/a;
    .locals 8

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v3}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v1

    invoke-virtual {v3}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    if-ge v1, v0, :cond_0

    sub-int v1, v0, v1

    invoke-virtual {v3, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    :goto_0
    new-instance v4, Lcom/twitter/refresh/widget/a;

    invoke-virtual {v3, v0}, Landroid/widget/ListView;->getItemIdAtPosition(I)J

    move-result-wide v5

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    :goto_1
    invoke-direct {v4, v0, v5, v6, v1}, Lcom/twitter/refresh/widget/a;-><init>(IJI)V

    return-object v4

    :cond_0
    invoke-virtual {v3, v2}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    move v7, v1

    move-object v1, v0

    move v0, v7

    goto :goto_0

    :cond_1
    move v1, v2

    goto :goto_1
.end method

.method public B()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->ar()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected P()Z
    .locals 4

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->s:Z

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/client/BaseListFragment;->o:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method public final U()Z
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final V()Landroid/widget/ListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    return-object v0
.end method

.method protected final W()Lcom/twitter/android/PromptView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    return-object v0
.end method

.method protected final X()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v0}, Lcom/twitter/android/PromptView;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final Y()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->c()V

    return-void
.end method

.method public final Z()V
    .locals 4

    const/4 v1, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->Z:Z

    if-nez v0, :cond_2

    :cond_0
    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->p:Z

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->H:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->p:Z

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must call through to super class"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->i(Z)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->U:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->U:Ljava/lang/Runnable;

    const-wide/16 v2, 0x258

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public a(J)I
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->j()I

    move-result v0

    if-ltz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v1}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    :cond_0
    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v2, v2, p1

    if-nez v2, :cond_1

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-virtual {p1, p2, p3, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    const v0, 0x1020004    # android.R.id.empty

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v0, 0x102000a    # android.R.id.list

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const v1, 0x7f090047    # com.twitter.android.R.id.list_progress

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ProgressBar;

    iput-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->l:Landroid/widget/ProgressBar;

    if-eqz v3, :cond_2

    const v1, 0x7f090046    # com.twitter.android.R.id.list_empty_text

    invoke-virtual {v3, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/client/BaseListFragment;->k:Landroid/view/View;

    iput-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->j:Landroid/view/View;

    if-eqz v4, :cond_1

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->F:I

    if-lez v1, :cond_0

    const v1, 0x7f09003b    # com.twitter.android.R.id.empty_title

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v5, p0, Lcom/twitter/android/client/BaseListFragment;->F:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->G:I

    if-lez v1, :cond_1

    const v1, 0x7f09003a    # com.twitter.android.R.id.empty_desc

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v4, p0, Lcom/twitter/android/client/BaseListFragment;->G:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    :cond_2
    const v1, 0x7f090032    # com.twitter.android.R.id.banner

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/view/View;)Z

    move-result v4

    if-eqz v4, :cond_3

    new-instance v4, Lcom/twitter/android/client/q;

    invoke-direct {v4, v1}, Lcom/twitter/android/client/q;-><init>(Landroid/view/View;)V

    iput-object v4, p0, Lcom/twitter/android/client/BaseListFragment;->C:Lcom/twitter/android/client/q;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->ad()V

    :cond_3
    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->I:Z

    if-nez v1, :cond_4

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingTop()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v0, v6, v1, v6, v4}, Landroid/widget/ListView;->setPadding(IIII)V

    const v1, 0x7f0b0062    # com.twitter.android.R.color.list_bg

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundResource(I)V

    :cond_4
    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->N:Z

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->J:Lcom/twitter/library/view/i;

    invoke-static {v1}, Ljy;->a(Lcom/twitter/library/view/i;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    :goto_0
    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setScrollbarFadingEnabled(Z)V

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->R:I

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->ae:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->E:I

    if-eqz v1, :cond_6

    if-eqz v3, :cond_5

    invoke-virtual {v3, v1}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_5
    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setBackgroundColor(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setCacheColorHint(I)V

    :cond_6
    instance-of v1, v0, Lcom/twitter/refresh/widget/RefreshableListView;

    if-eqz v1, :cond_7

    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v1, :cond_9

    move-object v1, v0

    check-cast v1, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v1, p0}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Lcom/twitter/refresh/widget/b;)V

    invoke-virtual {v1, p0}, Lcom/twitter/refresh/widget/RefreshableListView;->setRefreshVisibilityListener(Lcom/twitter/refresh/widget/e;)V

    :cond_7
    :goto_1
    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    return-object v2

    :cond_8
    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->J:Lcom/twitter/library/view/i;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    goto :goto_0

    :cond_9
    invoke-virtual {v0, v7}, Landroid/widget/ListView;->setFooterDividersEnabled(Z)V

    goto :goto_1
.end method

.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300a5    # com.twitter.android.R.layout.list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/content/Intent;)Lcom/twitter/android/client/BaseListFragment;
    .locals 3

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;->e()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    :cond_0
    const-string/jumbo v1, "data"

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    return-object p0
.end method

.method protected a(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ac:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public final a(Lcom/twitter/android/client/ai;)Lcom/twitter/android/client/BaseListFragment;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    return-object p0
.end method

.method protected a(Lcom/twitter/android/client/av;)Lcom/twitter/android/client/BaseListFragment;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ad:Ljava/util/LinkedHashSet;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    return-object p0
.end method

.method public a(Lcom/twitter/refresh/widget/e;)Lcom/twitter/android/client/BaseListFragment;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->aa:Lcom/twitter/refresh/widget/e;

    return-object p0
.end method

.method protected final a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->z:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected a(IILcom/twitter/library/service/b;)V
    .locals 1

    iget-object v0, p3, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Ljava/lang/String;I)V

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->a_(I)V

    return-void
.end method

.method protected a(ILcom/twitter/library/util/ar;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method protected a(JJ)V
    .locals 0

    return-void
.end method

.method protected a(Landroid/content/Context;IILcom/twitter/library/service/b;)V
    .locals 1

    iget-object v0, p4, Lcom/twitter/library/service/b;->a:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    invoke-virtual {p0, p3}, Lcom/twitter/android/client/BaseListFragment;->b(I)V

    return-void
.end method

.method protected a(Landroid/database/Cursor;)V
    .locals 0

    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 0

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->d(Landroid/database/Cursor;)V

    return-void
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 0

    return-void
.end method

.method protected final a(Lcom/twitter/android/PromptView;)V
    .locals 2

    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeHeaderView(Landroid/view/View;)Z

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    return-void
.end method

.method public a(Lcom/twitter/android/client/ag;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->T:Lcom/twitter/android/client/ag;

    return-void
.end method

.method protected a(Lcom/twitter/android/client/ah;)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->X:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must be called before onCreateView"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->Y:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/twitter/library/api/Prompt;)V
    .locals 2

    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->u:Lcom/twitter/library/api/Prompt;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->c:Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p1, Lcom/twitter/library/api/Prompt;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/m;

    iget-object v1, p1, Lcom/twitter/library/api/Prompt;->i:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->c:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->c:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->d:Z

    :cond_0
    return-void
.end method

.method protected a(Lcom/twitter/library/client/z;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ag:Lcom/twitter/library/client/z;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->ag:Lcom/twitter/library/client/z;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->ag:Lcom/twitter/library/client/z;

    return-void
.end method

.method protected final a(Lcom/twitter/library/service/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->z:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/a;)V

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->a()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/au;->a(Z)V

    invoke-virtual {p0, p1, p2, v0}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/twitter/refresh/widget/a;)V
    .locals 0

    return-void
.end method

.method protected a(Lcom/twitter/refresh/widget/a;Z)V
    .locals 0

    return-void
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 0

    return-void
.end method

.method protected a(Ljava/lang/String;I)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p0, v0, p2}, Lcom/twitter/android/client/BaseListFragment;->a(ZI)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    new-instance v1, Lcom/twitter/android/client/PendingRequest;

    invoke-direct {v1, p1, p2}, Lcom/twitter/android/client/PendingRequest;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 0

    return-void
.end method

.method protected a(ZI)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    if-eq p2, v0, :cond_0

    const/4 v0, 0x4

    if-ne p2, v0, :cond_1

    :cond_0
    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->e()V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/m;->a(Z)V

    goto :goto_0
.end method

.method protected a(Landroid/view/View;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected a(Lcom/twitter/android/client/q;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected final a(Lcom/twitter/library/service/b;II)Z
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->U()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->y:Lcom/twitter/library/service/c;

    if-nez v1, :cond_2

    new-instance v1, Lcom/twitter/android/client/af;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/af;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->y:Lcom/twitter/library/service/c;

    :cond_2
    invoke-virtual {p0, p3}, Lcom/twitter/android/client/BaseListFragment;->c_(I)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->z:Lcom/twitter/library/client/w;

    iget-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->y:Lcom/twitter/library/service/c;

    invoke-virtual {v1, p1, p2, p3, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public aA()Lcom/twitter/library/service/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->y:Lcom/twitter/library/service/c;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/af;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/af;-><init>(Lcom/twitter/android/client/BaseListFragment;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->y:Lcom/twitter/library/service/c;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->y:Lcom/twitter/library/service/c;

    return-object v0
.end method

.method public aB()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/j;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Lcom/twitter/refresh/widget/j;->e()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->al()V

    :cond_0
    return-void
.end method

.method protected a_()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->M:Lcom/twitter/android/client/au;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/au;->a(Z)V

    return-void
.end method

.method protected a_(I)V
    .locals 2

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Z)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->b()V

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->j(I)I

    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->f:Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->a_(Z)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected a_(Z)V
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->l:Landroid/widget/ProgressBar;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    if-eqz p1, :cond_3

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->j:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v0, v2}, Landroid/widget/ProgressBar;->setVisibility(I)V

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->l:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v3}, Landroid/widget/ProgressBar;->setVisibility(I)V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->o()Z

    move-result v1

    if-nez v1, :cond_4

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setVisibility(I)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->j:Landroid/view/View;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->k:Landroid/view/View;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->k:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    :cond_5
    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->j:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method

.method public final aa()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->ae()V

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must call through to super class"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->q:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->d()V

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->q:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Must call through to super class"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->U:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    :cond_2
    return-void
.end method

.method public final ab()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->t:Z

    return v0
.end method

.method public ac()Lcom/twitter/android/client/q;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->C:Lcom/twitter/android/client/q;

    return-object v0
.end method

.method public final ad()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->C:Lcom/twitter/android/client/q;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->C:Lcom/twitter/android/client/q;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->C:Lcom/twitter/android/client/q;

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/android/client/q;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/q;->a(Z)V

    :cond_0
    return-void
.end method

.method protected ae()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/refresh/widget/a;)V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->ak()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    return-void
.end method

.method protected af()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->s:Z

    return v0
.end method

.method protected ag()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ah()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->j(I)I

    return-void
.end method

.method public ai()V
    .locals 1

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->j(I)I

    return-void
.end method

.method public aj()V
    .locals 0

    return-void
.end method

.method protected ak()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/client/BaseListFragment;->i:I

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    iput v0, p0, Lcom/twitter/android/client/BaseListFragment;->h:I

    :cond_0
    return-void
.end method

.method protected final al()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    invoke-interface {v0, p0}, Lcom/twitter/android/client/ai;->a(Lcom/twitter/android/client/BaseListFragment;)V

    :cond_0
    return-void
.end method

.method public final am()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->H:Z

    return v0
.end method

.method protected an()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->r_()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v0}, Lcom/twitter/android/PromptView;->a()Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->u:Lcom/twitter/library/api/Prompt;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v1}, Lcom/twitter/android/PromptView;->b()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    iget-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->u:Lcom/twitter/library/api/Prompt;

    iget-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->c:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/PromptView;->a(Lcom/twitter/library/api/Prompt;Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->u:Lcom/twitter/library/api/Prompt;

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v1}, Lcom/twitter/android/PromptView;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getFirstVisiblePosition()I

    move-result v0

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    goto :goto_0
.end method

.method protected ao()Lcom/twitter/internal/android/widget/ToolBar;
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ap()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->W:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->V()V

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    :cond_0
    return-void
.end method

.method public aq()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public ar()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public as()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    return v0
.end method

.method protected at()Lcom/twitter/library/metrics/h;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->a()Lcom/twitter/library/metrics/h;

    move-result-object v0

    return-object v0
.end method

.method protected au()Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method protected final av()Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method protected final aw()Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method protected ax()Lcom/twitter/library/client/m;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    return-object v0
.end method

.method protected ay()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v0}, Lcom/twitter/library/client/m;->g()V

    :cond_0
    return-void
.end method

.method public az()I
    .locals 2

    iget v0, p0, Lcom/twitter/android/client/BaseListFragment;->ab:I

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    sub-int/2addr v0, v1

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method protected b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f0300ce    # com.twitter.android.R.layout.msg_list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected b(Landroid/view/View$OnTouchListener;)Lcom/twitter/android/client/BaseListFragment;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ac:Ljava/util/LinkedHashSet;

    invoke-virtual {v0, p1}, Ljava/util/LinkedHashSet;->remove(Ljava/lang/Object;)Z

    return-object p0
.end method

.method protected b()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/PendingRequest;

    iget-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->z:Lcom/twitter/library/client/w;

    iget-object v3, v0, Lcom/twitter/android/client/PendingRequest;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/w;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->a_(I)V

    :goto_1
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    :cond_0
    iget v2, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {p0, v2}, Lcom/twitter/android/client/BaseListFragment;->b(I)V

    iget-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->k()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->r:Z

    return-void
.end method

.method protected b(I)V
    .locals 2

    const/4 v1, 0x0

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->a_(Z)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->T:Lcom/twitter/android/client/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->T:Lcom/twitter/android/client/ag;

    invoke-interface {v0}, Lcom/twitter/android/client/ag;->d()V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->c()V

    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->j(I)I

    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->f:Z

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->T:Lcom/twitter/android/client/ag;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->T:Lcom/twitter/android/client/ag;

    invoke-interface {v0}, Lcom/twitter/android/client/ag;->d()V

    goto :goto_0

    :pswitch_2
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0, v1}, Lcom/twitter/refresh/widget/RefreshableListView;->a(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected b(ILcom/twitter/library/util/ar;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method protected final b(Landroid/widget/AbsListView;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    invoke-interface {v0, p0, p1, p2}, Lcom/twitter/android/client/ai;->a(Lcom/twitter/android/client/BaseListFragment;Landroid/widget/AbsListView;I)V

    :cond_0
    return-void
.end method

.method protected b(Lcom/twitter/library/provider/Tweet;)V
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->b()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-virtual {v0}, Lcom/twitter/library/scribe/ScribeAssociation;->c()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v7

    const/4 v2, 0x2

    const-string/jumbo v3, ":tweet:double_click"

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/twitter/library/scribe/ScribeLog;->a([Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v4, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v4, v7, [Ljava/lang/String;

    aput-object v1, v4, v6

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v8, p1, v0, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method public final b(Lcom/twitter/library/scribe/ScribeAssociation;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/BaseListFragment;->S:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method protected final b(Lcom/twitter/library/service/a;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->z:Lcom/twitter/library/client/w;

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/w;->b(Lcom/twitter/library/service/a;)V

    return-void
.end method

.method protected b(Ljava/util/HashMap;)V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->d:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/twitter/library/util/m;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->u:Lcom/twitter/library/api/Prompt;

    iget-object v1, v1, Lcom/twitter/library/api/Prompt;->i:Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->c:Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->d:Z

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->an()V

    :cond_0
    return-void
.end method

.method public b(Z)V
    .locals 0

    return-void
.end method

.method protected b(Landroid/widget/ListView;Landroid/view/View;IJ)Z
    .locals 1

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Lhn;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected b_(I)Landroid/app/Dialog;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public b_()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->aa:Lcom/twitter/refresh/widget/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->aa:Lcom/twitter/refresh/widget/e;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/e;->b_()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    instance-of v0, v0, Lcom/twitter/internal/android/widget/GroupedRowView;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ah:Landroid/view/View;

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getView()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090231    # com.twitter.android.R.id.ptr_border

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ah:Landroid/view/View;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ah:Landroid/view/View;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ah:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    :cond_2
    return-void
.end method

.method protected c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;
    .locals 2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/android/yd;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected c()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->q:Z

    return-void
.end method

.method protected c(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setDt2fPressed(Z)V

    return-void
.end method

.method public c(Z)V
    .locals 0

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->i()V

    :cond_0
    return-void
.end method

.method protected c_(I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->i(I)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected d()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->q:Z

    return-void
.end method

.method public d(Landroid/database/Cursor;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    invoke-interface {p1, v0}, Landroid/database/Cursor;->registerContentObserver(Landroid/database/ContentObserver;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->s:Z

    return-void
.end method

.method protected d(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 3

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/TweetView;->setDt2fPressed(Z)V

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->n()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->b(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/library/widget/TweetView;->m()V

    return-void
.end method

.method protected d(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    new-instance v1, Lcom/twitter/android/client/PendingRequest;

    invoke-direct {v1, p1}, Lcom/twitter/android/client/PendingRequest;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method protected e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 5

    iget-object v2, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/PendingRequest;

    iget-object v4, v0, Lcom/twitter/android/client/PendingRequest;->a:Ljava/lang/String;

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const/4 v3, 0x1

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {p0, v3, v0}, Lcom/twitter/android/client/BaseListFragment;->a(ZI)V

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/PendingRequest;

    :goto_1
    return-object v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final e(J)V
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/client/BaseListFragment;->Q:J

    return-void
.end method

.method protected e(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1

    invoke-virtual {p0, p2}, Lcom/twitter/android/client/BaseListFragment;->c(Landroid/view/View;)Lcom/twitter/library/widget/TweetView;

    move-result-object v0

    invoke-static {v0}, Ljy;->a(Lcom/twitter/library/widget/TweetView;)V

    return-void
.end method

.method public f(I)J
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->isClosed()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-interface {v0}, Landroid/database/Cursor;->requery()Z

    :cond_0
    return-void
.end method

.method public final f(J)V
    .locals 3

    iget-wide v0, p0, Lcom/twitter/android/client/BaseListFragment;->Q:J

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->e(J)V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->k()V

    iget-boolean v2, p0, Lcom/twitter/android/client/BaseListFragment;->H:Z

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->b(J)V

    invoke-virtual {p0, v0, v1, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(JJ)V

    return-void
.end method

.method protected g()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->f()V

    return-void
.end method

.method protected h()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->A()Lcom/twitter/refresh/widget/a;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->g()V

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/refresh/widget/a;Z)V

    return-void
.end method

.method protected final h(I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/client/ae;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/client/ae;-><init>(Lcom/twitter/android/client/BaseListFragment;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    iput p1, p0, Lcom/twitter/android/client/BaseListFragment;->o:I

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->s:Z

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->o:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method protected i()V
    .locals 0

    return-void
.end method

.method protected final i(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->m()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->V:Z

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->V:Z

    if-nez v0, :cond_3

    if-eqz p1, :cond_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->n()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->V:Z

    goto :goto_0
.end method

.method protected i(I)Z
    .locals 2

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/PendingRequest;

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected j()I
    .locals 1

    const/4 v0, -0x1

    return v0
.end method

.method protected j(I)I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->A:Lcom/twitter/android/util/u;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/u;->a(Landroid/content/Context;)Lcom/twitter/android/util/u;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->A:Lcom/twitter/android/util/u;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->A:Lcom/twitter/android/util/u;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/u;->a(I)I

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final j(Z)Lcom/twitter/android/client/BaseListFragment;
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;->e()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-object p0
.end method

.method protected k()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->h:I

    iget v2, p0, Lcom/twitter/android/client/BaseListFragment;->i:I

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    :cond_0
    return-void
.end method

.method public final k(I)V
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->b_(I)Landroid/app/Dialog;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Dialog;->show()V

    :cond_0
    return-void
.end method

.method public final k(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/client/BaseListFragment;->H:Z

    return-void
.end method

.method protected l()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "unknown"

    return-object v0
.end method

.method public l(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/client/BaseListFragment;->W:Z

    return-void
.end method

.method public m(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->f:Z

    if-eq p1, v0, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/client/BaseListFragment;->f:Z

    :cond_0
    return-void
.end method

.method protected m()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected n()V
    .locals 0

    return-void
.end method

.method protected n(Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->ad:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/av;

    if-eqz v0, :cond_0

    invoke-interface {v0, p1}, Lcom/twitter/android/client/av;->c(Z)V

    goto :goto_0

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_1
    return-void
.end method

.method protected o()Z
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getCount()I

    move-result v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {v0}, Landroid/widget/ListView;->getFooterViewsCount()I

    move-result v0

    sub-int v0, v1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->W:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->S()Lhm;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v2

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/client/BaseListFragment;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/client/BaseListFragment;->a(Lcom/twitter/internal/android/widget/ToolBar;)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/as;)V

    :cond_0
    iget v0, p0, Lcom/twitter/android/client/BaseListFragment;->w:I

    if-lez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->af:Lcom/twitter/android/th;

    if-nez v0, :cond_1

    new-instance v0, Lcom/twitter/android/th;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget v2, p0, Lcom/twitter/android/client/BaseListFragment;->w:I

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/th;-><init>(Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->af:Lcom/twitter/android/th;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->af:Lcom/twitter/android/th;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/th;->a(Landroid/widget/ListView;)V

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->D:Z

    if-eqz v0, :cond_4

    new-instance v0, Lcom/twitter/android/PromptView;

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->l()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/twitter/android/PromptView;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    :goto_0
    if-eqz p1, :cond_3

    const-string/jumbo v0, "prompt"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    if-eqz v1, :cond_3

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/PromptView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->Z:Z

    return-void

    :cond_4
    iput-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->z:Lcom/twitter/library/client/w;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v3

    iput-object v3, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseFragmentActivity;->aa()Landroid/content/Intent;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->t:Z

    if-eqz p1, :cond_2

    const-string/jumbo v0, "scroll_pos"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/client/BaseListFragment;->h:I

    const-string/jumbo v0, "scroll_off"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/client/BaseListFragment;->i:I

    const-string/jumbo v0, "pending_reqs"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string/jumbo v3, "refresh"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    const-string/jumbo v3, "owner_id"

    iget-object v4, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-virtual {v0, v3, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    iput-wide v3, p0, Lcom/twitter/android/client/BaseListFragment;->Q:J

    const-string/jumbo v3, "chmode"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/client/BaseListFragment;->R:I

    const-string/jumbo v3, "bg_color"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/client/BaseListFragment;->E:I

    const-string/jumbo v3, "empty_title"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/client/BaseListFragment;->F:I

    const-string/jumbo v3, "empty_desc"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/client/BaseListFragment;->G:I

    const-string/jumbo v3, "prompt_host"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/client/BaseListFragment;->D:Z

    const-string/jumbo v3, "shim_size"

    invoke-virtual {v0, v3, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    iput v3, p0, Lcom/twitter/android/client/BaseListFragment;->w:I

    const-string/jumbo v3, "horizontal_padding_enabled"

    invoke-virtual {v0, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->I:Z

    const-string/jumbo v1, "auto_ref"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    if-lez v0, :cond_0

    if-nez p1, :cond_3

    :goto_2
    invoke-direct {p0, v2, v0}, Lcom/twitter/android/client/BaseListFragment;->a(II)V

    :cond_0
    :goto_3
    return-void

    :cond_1
    move v0, v2

    goto/16 :goto_0

    :cond_2
    iput v2, p0, Lcom/twitter/android/client/BaseListFragment;->h:I

    iput v2, p0, Lcom/twitter/android/client/BaseListFragment;->i:I

    new-instance v0, Ljava/util/ArrayList;

    const/4 v3, 0x5

    invoke-direct {v0, v3}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    goto :goto_1

    :cond_3
    const-string/jumbo v1, "auto_ref"

    invoke-virtual {p1, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    goto :goto_2

    :cond_4
    iput-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->g:Z

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/twitter/android/client/BaseListFragment;->Q:J

    iput v2, p0, Lcom/twitter/android/client/BaseListFragment;->R:I

    iput v2, p0, Lcom/twitter/android/client/BaseListFragment;->E:I

    iput v2, p0, Lcom/twitter/android/client/BaseListFragment;->F:I

    iput v2, p0, Lcom/twitter/android/client/BaseListFragment;->G:I

    goto :goto_3
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1

    iget v0, p0, Lcom/twitter/android/client/BaseListFragment;->F:I

    if-gtz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/client/BaseListFragment;->G:I

    if-lez v0, :cond_1

    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->b(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->W:Z

    if-eqz v1, :cond_0

    instance-of v1, v0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/BaseFragmentActivity;->b(Lcom/twitter/internal/android/widget/as;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->A:Lcom/twitter/android/util/u;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->A:Lcom/twitter/android/util/u;

    invoke-virtual {v0}, Lcom/twitter/android/util/u;->a()V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_2
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onDestroyView()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->L:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->n:Lcom/twitter/android/client/ae;

    invoke-interface {v0, v1}, Landroid/database/Cursor;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onPause()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->ag:Lcom/twitter/library/client/z;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->O:Lcom/twitter/library/client/j;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->O:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->x:Lcom/twitter/library/client/aa;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->ag:Lcom/twitter/library/client/z;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->O:Lcom/twitter/library/client/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->b:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->O:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->p:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "pending_reqs"

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->m:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string/jumbo v0, "scroll_pos"

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "scroll_off"

    iget v1, p0, Lcom/twitter/android/client/BaseListFragment;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "prompt"

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->a:Lcom/twitter/android/PromptView;

    invoke-virtual {v1}, Lcom/twitter/android/PromptView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    if-eqz v0, :cond_1

    const-string/jumbo v0, "auto_ref"

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->B:Lcom/twitter/library/client/m;

    invoke-virtual {v1}, Lcom/twitter/library/client/m;->a()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1
    return-void
.end method

.method public final onScroll(Landroid/widget/AbsListView;III)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->Y:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/ah;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/twitter/android/client/ah;->a(Landroid/widget/AbsListView;III)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    :goto_0
    return-void

    :cond_2
    if-eqz p3, :cond_1

    if-nez p2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->al()V

    :cond_3
    if-lez p2, :cond_5

    iget v0, p0, Lcom/twitter/android/client/BaseListFragment;->ab:I

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->v:Lcom/twitter/android/client/ai;

    invoke-interface {v0, p0}, Lcom/twitter/android/client/ai;->b(Lcom/twitter/android/client/BaseListFragment;)V

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-eqz v0, :cond_5

    add-int v0, p2, p3

    if-lt v0, p4, :cond_5

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCount()I

    move-result v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Landroid/database/Cursor;->moveToLast()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/database/Cursor;)V

    :cond_5
    iput p2, p0, Lcom/twitter/android/client/BaseListFragment;->ab:I

    goto :goto_0
.end method

.method public final onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->Y:Ljava/util/LinkedHashSet;

    invoke-virtual {v0}, Ljava/util/LinkedHashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/ah;

    invoke-interface {v0, p1, p2}, Lcom/twitter/android/client/ah;->a(Landroid/widget/AbsListView;I)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_1
    :goto_0
    return-void

    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/BaseListFragment;->b(Landroid/widget/AbsListView;I)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/au;->a(I)V

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->M:Lcom/twitter/android/client/au;

    invoke-virtual {v0}, Lcom/twitter/android/client/au;->c()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->a_()V

    :cond_3
    if-nez p2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/client/BaseListFragment;->ay()V

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/client/BaseListFragment;->X:Z

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->K:Landroid/os/Handler;

    iget-object v1, p0, Lcom/twitter/android/client/BaseListFragment;->L:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method protected p()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BaseListFragment;->h(I)V

    return-void
.end method

.method protected r_()Z
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->e:Landroid/widget/ListView;

    check-cast v0, Lcom/twitter/refresh/widget/RefreshableListView;

    invoke-virtual {v0}, Lcom/twitter/refresh/widget/RefreshableListView;->a()Z

    move-result v0

    iget-boolean v1, p0, Lcom/twitter/android/client/BaseListFragment;->d:Z

    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public t_()V
    .locals 0

    return-void
.end method

.method public w()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, ""

    return-object v0
.end method

.method protected x()Lcom/twitter/library/client/r;
    .locals 2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "AutoRefresh interval set without creating request."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public x_()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->aa:Lcom/twitter/refresh/widget/e;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BaseListFragment;->aa:Lcom/twitter/refresh/widget/e;

    invoke-interface {v0}, Lcom/twitter/refresh/widget/e;->x_()V

    :cond_0
    return-void
.end method
