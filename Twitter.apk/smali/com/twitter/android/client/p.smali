.class Lcom/twitter/android/client/p;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/ap;


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/p;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()J
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/p;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->d(Lcom/twitter/android/client/c;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    return-wide v0
.end method

.method public a(ILjava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3

    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/p;->a:Lcom/twitter/android/client/c;

    invoke-static {v0}, Lcom/twitter/android/client/c;->h(Lcom/twitter/android/client/c;)Landroid/util/SparseArray;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ao;

    iget-object v1, p0, Lcom/twitter/android/client/p;->a:Lcom/twitter/android/client/c;

    invoke-static {v1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/android/client/c;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p2, p2}, Lcom/twitter/library/util/ao;->a(JLjava/lang/String;Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/p;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->b:Lcom/twitter/library/util/aa;

    iget-object v1, p0, Lcom/twitter/android/client/p;->a:Lcom/twitter/android/client/c;

    invoke-static {v1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/android/client/c;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/twitter/library/util/aa;->a(JLcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
