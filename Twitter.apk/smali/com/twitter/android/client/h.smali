.class Lcom/twitter/android/client/h;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/client/c;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/h;->a:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    return-void
.end method

.method private a(Ljl;Lcom/twitter/library/client/Session;)V
    .locals 4

    invoke-virtual {p1}, Ljl;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljl;->e()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    invoke-virtual {p1}, Ljl;->f()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/h;->a:Lcom/twitter/android/client/c;

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/Session;Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/h;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/h;->a:Lcom/twitter/android/client/c;

    iget-object v0, v0, Lcom/twitter/android/client/c;->g:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-object v1, v1, Lcom/twitter/library/service/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->c(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    instance-of v1, p1, Ljl;

    if-eqz v1, :cond_0

    check-cast p1, Ljl;

    invoke-direct {p0, p1, v0}, Lcom/twitter/android/client/h;->a(Ljl;Lcom/twitter/library/client/Session;)V

    goto :goto_0
.end method
