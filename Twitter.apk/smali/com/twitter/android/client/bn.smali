.class public Lcom/twitter/android/client/bn;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/support/v4/app/LoaderManager$LoaderCallbacks;
.implements Landroid/text/TextWatcher;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/TextView$OnEditorActionListener;
.implements Lcom/twitter/internal/android/widget/af;
.implements Lcom/twitter/library/util/ar;
.implements Lcom/twitter/library/widget/l;


# instance fields
.field private final a:Lcom/twitter/android/client/c;

.field private final b:Landroid/support/v4/app/LoaderManager;

.field private final c:Landroid/support/v4/app/FragmentManager;

.field private final d:I

.field private final e:Lcom/twitter/library/client/j;

.field private final f:Lcom/twitter/android/client/bs;

.field private final g:Ljava/util/HashSet;

.field private final h:Ljava/util/ArrayList;

.field private final i:Landroid/support/v4/app/FragmentActivity;

.field private final j:Ljava/lang/Runnable;

.field private final k:Lcom/twitter/library/client/aa;

.field private l:Lcom/twitter/library/scribe/ScribeAssociation;

.field private m:Lcom/twitter/internal/android/widget/PopupEditText;

.field private n:Ljava/lang/String;

.field private o:Lcom/twitter/android/client/bq;

.field private p:Lhn;

.field private q:Z

.field private r:Lcom/twitter/android/client/bw;

.field private s:Ljava/lang/String;

.field private t:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;I)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/bn;->g:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/client/bn;->h:Ljava/util/ArrayList;

    new-instance v0, Lcom/twitter/android/client/bo;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/bo;-><init>(Lcom/twitter/android/client/bn;)V

    iput-object v0, p0, Lcom/twitter/android/client/bn;->j:Ljava/lang/Runnable;

    iput-object p1, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->b:Landroid/support/v4/app/LoaderManager;

    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->c:Landroid/support/v4/app/FragmentManager;

    iput p2, p0, Lcom/twitter/android/client/bn;->d:I

    new-instance v0, Lcom/twitter/android/client/bx;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/bx;-><init>(Lcom/twitter/android/client/bn;)V

    iput-object v0, p0, Lcom/twitter/android/client/bn;->e:Lcom/twitter/library/client/j;

    new-instance v0, Lcom/twitter/android/client/bs;

    iget-object v1, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/android/client/by;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/by;-><init>(Lcom/twitter/android/client/bn;)V

    invoke-direct {v0, p1, v1, p0, v2}, Lcom/twitter/android/client/bs;-><init>(Landroid/content/Context;Lcom/twitter/android/client/c;Landroid/view/View$OnClickListener;Lcom/twitter/android/ob;)V

    iput-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    new-instance v0, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v0}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const-string/jumbo v1, "app"

    invoke-virtual {v0, v1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/bn;)Lcom/twitter/internal/android/widget/PopupEditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/client/bn;)Landroid/support/v4/app/FragmentActivity;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/client/bn;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->g:Ljava/util/HashSet;

    return-object v0
.end method

.method private c(Ljava/lang/String;)V
    .locals 4

    const/4 v3, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p1}, Lcom/twitter/android/provider/SuggestionsProvider;->a(Ljava/lang/String;)Lcom/twitter/library/api/search/TwitterTypeAheadGroup;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->g:Ljava/util/HashSet;

    iget-object v1, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    const-string/jumbo v2, "search_box"

    invoke-virtual {v1, p1, v3, v3, v2}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method

.method static synthetic d(Lcom/twitter/android/client/bn;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->n:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/client/bn;)Lcom/twitter/android/client/bq;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->o:Lcom/twitter/android/client/bq;

    return-object v0
.end method

.method private e()Z
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v3, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    const-class v4, Lcom/twitter/android/SearchActivity;

    invoke-direct {v2, v3, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "com.twitter.android.action.SEARCH"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "user_query"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "query"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v3, "query_name"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "q_source"

    const-string/jumbo v3, "typed_query"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/client/bn;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private f()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    invoke-virtual {v0}, Lhn;->e()Landroid/view/View;

    move-result-object v0

    instance-of v1, v0, Lcom/twitter/library/widget/SearchQueryView;

    if-eqz v1, :cond_0

    check-cast v0, Lcom/twitter/library/widget/SearchQueryView;

    :goto_0
    invoke-virtual {v0, p0}, Lcom/twitter/library/widget/SearchQueryView;->setOnClearClickListener(Lcom/twitter/library/widget/l;)V

    iget-object v1, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0f0078    # com.twitter.android.R.string.button_search

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/SearchQueryView;->setContentDescription(Ljava/lang/CharSequence;)V

    return-void

    :cond_0
    const v1, 0x7f090212    # com.twitter.android.R.id.query

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/SearchQueryView;

    goto :goto_0
.end method

.method static synthetic g(Lcom/twitter/android/client/bn;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method private g()V
    .locals 4

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/client/bn;->t:Z

    if-eqz v0, :cond_0

    const v0, 0x7f0202a5    # com.twitter.android.R.drawable.icn_clearfield

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    const v3, 0x7f02022f    # com.twitter.android.R.drawable.ic_search_hint

    invoke-virtual {v2, v3, v1, v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private h()V
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/client/bn;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/client/bn;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v5, "search_box"

    const-string/jumbo v6, "typeahead"

    const-string/jumbo v7, "results"

    invoke-static {v4, v5, v6, v7}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/bn;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/client/bn;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    :cond_0
    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/android/client/bn;
    .locals 0

    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    :cond_0
    return-object p0
.end method

.method public a(Ljava/lang/String;)Lcom/twitter/android/client/bn;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    return-object p0
.end method

.method a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/bn;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/client/bn;->d:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/LoaderManager;->getLoader(I)Landroid/support/v4/content/Loader;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/content/Loader;->onContentChanged()V

    return-void
.end method

.method public a(I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    if-eqz v0, :cond_0

    packed-switch p1, :pswitch_data_0

    const v0, 0x7f030124    # com.twitter.android.R.layout.search_action_box

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    invoke-virtual {v1, v0}, Lhn;->c(I)Lhn;

    invoke-direct {p0}, Lcom/twitter/android/client/bn;->f()V

    :cond_0
    return-void

    :pswitch_0
    const v0, 0x7f030125    # com.twitter.android.R.layout.search_action_box_left

    goto :goto_0

    :pswitch_1
    const v0, 0x7f030126    # com.twitter.android.R.layout.search_action_box_right

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public a(II)V
    .locals 0

    return-void
.end method

.method public a(Landroid/os/Bundle;)V
    .locals 3

    if-eqz p1, :cond_0

    const-string/jumbo v0, "search_topic"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->s:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-string/jumbo v1, "TAG_CLEAR_RECENT_SEARCH_DIALOG"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/PromptDialogFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/bn;->s:Ljava/lang/String;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/android/client/br;

    iget-object v2, p0, Lcom/twitter/android/client/bn;->s:Ljava/lang/String;

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/client/br;-><init>(Lcom/twitter/android/client/bn;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    :cond_0
    return-void
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    iget-object v1, p0, Lcom/twitter/android/client/bn;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bs;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/bs;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public a(Lcom/twitter/android/client/bw;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/bn;->r:Lcom/twitter/android/client/bw;

    return-void
.end method

.method public a(Lcom/twitter/internal/android/widget/ToolBar;)V
    .locals 2

    const v0, 0x7f090309    # com.twitter.android.R.id.toolbar_search

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/client/bp;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/bp;-><init>(Lcom/twitter/android/client/bn;)V

    invoke-virtual {v0, v1}, Lhn;->a(Lho;)Lhn;

    iput-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    invoke-direct {p0}, Lcom/twitter/android/client/bn;->f()V

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    invoke-virtual {v0, p1, p2}, Lcom/twitter/android/client/bs;->a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/client/bn;->a(Ljava/lang/CharSequence;Z)V

    return-void
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 13

    const/16 v12, 0x8

    const/4 v11, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    invoke-virtual {v0, p2}, Lcom/twitter/android/client/bs;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/4 v1, 0x6

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_0
    const/4 v6, 0x3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v8, v6}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    new-instance v8, Landroid/content/Intent;

    iget-object v9, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    const-class v10, Lcom/twitter/android/SearchActivity;

    invoke-direct {v8, v9, v10}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v8, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v5

    invoke-virtual {v5, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v5, "user_query"

    iget-object v8, p0, Lcom/twitter/android/client/bn;->n:Ljava/lang/String;

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v5, "query"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v5, "source_association"

    iget-object v8, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v1, v5, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v5

    packed-switch v4, :pswitch_data_0

    :goto_1
    return-void

    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "query_name"

    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "search_type"

    invoke-virtual {v0, v1, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_1
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v1, 0x9

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v2, :cond_1

    move v1, v2

    :goto_2
    const/16 v8, 0xa

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v0, v2, :cond_2

    :goto_3
    const-string/jumbo v0, "query"

    invoke-virtual {v5, v0, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "query_name"

    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v0, "search_type"

    invoke-virtual {v5, v0, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v0, "near"

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    const-string/jumbo v0, "follows"

    invoke-virtual {v5, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_1
    move v1, v3

    goto :goto_2

    :cond_2
    move v2, v3

    goto :goto_3

    :pswitch_2
    invoke-interface {v0, v12}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    const-string/jumbo v1, "query_name"

    invoke-virtual {v5, v1, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "search_type"

    invoke-virtual {v5, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_3
    const-string/jumbo v0, "query_name"

    invoke-virtual {v5, v0, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-virtual {v0, v5}, Landroid/support/v4/app/FragmentActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method public a(Ljava/lang/CharSequence;Z)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/PopupEditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/bn;->c(Ljava/lang/CharSequence;)V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setSelection(I)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/widget/SearchQueryView;)Z
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/library/widget/SearchQueryView;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/bn;->c()Z

    :goto_0
    const/4 v0, 0x1

    return v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/twitter/library/widget/SearchQueryView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method public a(Lhn;)Z
    .locals 10

    const/4 v9, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/client/bn;->q:Z

    if-eqz v0, :cond_0

    :goto_0
    return v3

    :cond_0
    iget-object v4, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Lhn;->e()Landroid/view/View;

    move-result-object v0

    const v1, 0x7f090212    # com.twitter.android.R.id.query

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnEditorActionListener(Landroid/widget/TextView$OnEditorActionListener;)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setPopupEditTextListener(Lcom/twitter/internal/android/widget/af;)V

    iget-object v1, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    sget-object v1, Lcom/twitter/internal/android/widget/PopupEditText;->a:Landroid/widget/MultiAutoCompleteTextView$Tokenizer;

    sget-object v5, Lcom/twitter/internal/android/widget/PopupEditText;->b:Landroid/widget/Filterable;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->B()J

    move-result-wide v6

    invoke-virtual {v0, v1, v5, v6, v7}, Lcom/twitter/internal/android/widget/PopupEditText;->a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;Landroid/widget/Filterable;J)V

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/twitter/android/client/bn;->c(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    new-instance v1, Lcom/twitter/android/client/bq;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/bq;-><init>(Lcom/twitter/android/client/bn;)V

    iput-object v1, p0, Lcom/twitter/android/client/bn;->o:Lcom/twitter/android/client/bq;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->length()I

    move-result v1

    if-lez v1, :cond_1

    move v1, v2

    :goto_1
    iput-boolean v1, p0, Lcom/twitter/android/client/bn;->t:Z

    invoke-direct {p0}, Lcom/twitter/android/client/bn;->g()V

    iget-object v1, p0, Lcom/twitter/android/client/bn;->b:Landroid/support/v4/app/LoaderManager;

    iget v5, p0, Lcom/twitter/android/client/bn;->d:I

    invoke-virtual {v1, v5, v9, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    iget-object v1, p0, Lcom/twitter/android/client/bn;->e:Lcom/twitter/library/client/j;

    invoke-virtual {v4, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    const/4 v1, 0x2

    invoke-virtual {v4, v1, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    iget-object v1, p0, Lcom/twitter/android/client/bn;->g:Ljava/util/HashSet;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->m()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/android/client/bn;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/PopupEditText;->post(Ljava/lang/Runnable;)Z

    iget-object v0, p0, Lcom/twitter/android/client/bn;->k:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    new-array v5, v2, [Ljava/lang/String;

    iget-object v6, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    const-string/jumbo v7, "search_box"

    const-string/jumbo v8, "focus_field"

    invoke-static {v6, v7, v9, v8}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v3

    invoke-virtual {v4, v0, v1, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iput-boolean v2, p0, Lcom/twitter/android/client/bn;->q:Z

    move v3, v2

    goto/16 :goto_0

    :cond_1
    move v1, v3

    goto :goto_1
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/client/bn;->t:Z

    if-eq v0, v1, :cond_0

    iput-boolean v0, p0, Lcom/twitter/android/client/bn;->t:Z

    invoke-direct {p0}, Lcom/twitter/android/client/bn;->g()V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Ljava/lang/String;)Lcom/twitter/android/client/bn;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->l:Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-virtual {v0, p1}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    return-object p0
.end method

.method public b(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "search_topic"

    iget-object v1, p0, Lcom/twitter/android/client/bn;->s:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/bn;->q:Z

    return v0
.end method

.method public b(Lhn;)Z
    .locals 3

    const/4 v0, 0x0

    iget-boolean v1, p0, Lcom/twitter/android/client/bn;->q:Z

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/client/bn;->a:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/client/bn;->e:Lcom/twitter/library/client/j;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/4 v2, 0x2

    invoke-virtual {v1, v2, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    invoke-static {}, Lcom/twitter/android/provider/SuggestionsProvider;->a()V

    iget-object v1, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v1, p0}, Lcom/twitter/internal/android/widget/PopupEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {p1}, Lhn;->e()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/View;->clearFocus()V

    iget-object v1, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-static {v1, v2, v0}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/twitter/android/client/bn;->h()V

    iput-boolean v0, p0, Lcom/twitter/android/client/bn;->q:Z

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public c(Ljava/lang/CharSequence;)V
    .locals 3

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/client/bn;->c(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/client/bn;->b:Landroid/support/v4/app/LoaderManager;

    iget v1, p0, Lcom/twitter/android/client/bn;->d:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/bn;->q:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    invoke-virtual {v0}, Lhn;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->r:Lcom/twitter/android/client/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->r:Lcom/twitter/android/client/bw;

    invoke-interface {v0}, Lcom/twitter/android/client/bw;->a()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/client/bn;->q:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->p:Lhn;

    invoke-virtual {v0}, Lhn;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/bn;->r:Lcom/twitter/android/client/bw;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->r:Lcom/twitter/android/client/bw;

    invoke-interface {v0}, Lcom/twitter/android/client/bw;->b()V

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    const/4 v5, 0x1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f090212    # com.twitter.android.R.id.query

    if-ne v0, v1, :cond_1

    check-cast p1, Lcom/twitter/internal/android/widget/PopupEditText;

    iget-object v0, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0, p1, v5}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    invoke-virtual {p1}, Lcom/twitter/internal/android/widget/PopupEditText;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f090237    # com.twitter.android.R.id.clear

    if-ne v0, v1, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    const v2, 0x7f0f0342    # com.twitter.android.R.string.recent_search_one_clear

    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/support/v4/app/FragmentActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v0, p0, Lcom/twitter/android/client/bn;->s:Ljava/lang/String;

    invoke-static {v5}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    const v3, 0x7f0f0343    # com.twitter.android.R.string.recent_searches_clear

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->b(Ljava/lang/String;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/br;

    invoke-direct {v2, p0, v0}, Lcom/twitter/android/client/br;-><init>(Lcom/twitter/android/client/bn;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Lcom/twitter/android/widget/ce;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/bn;->c:Landroid/support/v4/app/FragmentManager;

    const-string/jumbo v2, "TAG_CLEAR_RECENT_SEARCH_DIALOG"

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v1, 0x7f09028d    # com.twitter.android.R.id.tapahead

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v5}, Lcom/twitter/android/client/bn;->a(Ljava/lang/CharSequence;Z)V

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->m:Lcom/twitter/internal/android/widget/PopupEditText;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/PopupEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/bn;->n:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    sget-object v1, Lcom/twitter/android/provider/SuggestionsProvider;->h:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    :goto_0
    new-instance v0, Landroid/support/v4/content/CursorLoader;

    iget-object v1, p0, Lcom/twitter/android/client/bn;->i:Landroid/support/v4/app/FragmentActivity;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0

    :cond_0
    sget-object v2, Lcom/twitter/android/provider/SuggestionsProvider;->h:Landroid/net/Uri;

    goto :goto_0
.end method

.method public onEditorAction(Landroid/widget/TextView;ILandroid/view/KeyEvent;)Z
    .locals 2

    invoke-virtual {p1}, Landroid/widget/TextView;->getId()I

    move-result v0

    const v1, 0x7f090212    # com.twitter.android.R.id.query

    if-ne v0, v1, :cond_1

    const/4 v0, 0x3

    if-eq p2, v0, :cond_0

    if-eqz p3, :cond_1

    const/16 v0, 0x42

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-direct {p0}, Lcom/twitter/android/client/bn;->e()Z

    move-result v0

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/client/bn;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bs;->a(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/client/bn;->f:Lcom/twitter/android/client/bs;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bs;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public r()V
    .locals 0

    return-void
.end method
