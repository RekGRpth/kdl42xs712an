.class public Lcom/twitter/android/client/BasePreferenceActivity;
.super Landroid/preference/PreferenceActivity;
.source "Twttr"


# instance fields
.field protected P:Lcom/twitter/library/client/j;

.field protected Q:Lcom/twitter/android/client/c;

.field private a:Lcom/twitter/library/client/aa;

.field private b:Lcom/twitter/internal/android/widget/ToolBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected a(Lhn;)Z
    .locals 2

    invoke-virtual {p1}, Lhn;->a()I

    move-result v0

    const v1, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->finish()V

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public c()Lcom/twitter/library/client/aa;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->a:Lcom/twitter/library/client/aa;

    return-object v0
.end method

.method protected final d()Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->a:Lcom/twitter/library/client/aa;

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/16 v2, 0x8

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f0300f0    # com.twitter.android.R.layout.preferences_list_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BasePreferenceActivity;->setContentView(I)V

    invoke-static {p0}, Lcom/twitter/library/util/Util;->a(Landroid/app/Activity;)V

    const v0, 0x7f09005a    # com.twitter.android.R.id.toolbar

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BasePreferenceActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    new-instance v1, Lhm;

    invoke-direct {v1, p0}, Lhm;-><init>(Landroid/app/Activity;)V

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/client/BasePreferenceActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v1

    if-eqz v1, :cond_2

    iput-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    new-instance v1, Lcom/twitter/android/client/aj;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/aj;-><init>(Lcom/twitter/android/client/BasePreferenceActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setOnToolBarItemSelectedListener(Lcom/twitter/internal/android/widget/as;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/client/BasePreferenceActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    :cond_0
    :goto_0
    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->Q:Lcom/twitter/android/client/c;

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->a:Lcom/twitter/library/client/aa;

    new-instance v0, Lcom/twitter/android/js;

    invoke-direct {v0, p0}, Lcom/twitter/android/js;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->P:Lcom/twitter/library/client/j;

    return-void

    :cond_1
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->setVisibility(I)V

    goto :goto_0
.end method

.method protected onPause()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->P:Lcom/twitter/library/client/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/BasePreferenceActivity;->P:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    :cond_0
    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onPostCreate(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 2

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onResume()V

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->P:Lcom/twitter/library/client/j;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/client/BasePreferenceActivity;->P:Lcom/twitter/library/client/j;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    :cond_0
    return-void
.end method

.method protected onStart()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStart()V

    invoke-static {}, Lcom/twitter/library/client/l;->b()V

    return-void
.end method

.method protected onStop()V
    .locals 0

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onStop()V

    invoke-static {}, Lcom/twitter/library/client/l;->c()V

    return-void
.end method

.method protected onTitleChanged(Ljava/lang/CharSequence;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/BasePreferenceActivity;->b:Lcom/twitter/internal/android/widget/ToolBar;

    invoke-virtual {v0, p1}, Lcom/twitter/internal/android/widget/ToolBar;->setTitle(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2}, Landroid/preference/PreferenceActivity;->onTitleChanged(Ljava/lang/CharSequence;I)V

    goto :goto_0
.end method
