.class Lcom/twitter/android/client/cg;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/twitter/library/api/TweetEntities;J)V
    .locals 7

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v1

    iget-object v0, p1, Lcom/twitter/library/api/TweetEntities;->contacts:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/ContactEntity;

    new-instance v4, Lcom/twitter/library/api/TwitterContact;

    iget-object v5, v0, Lcom/twitter/library/api/ContactEntity;->name:Ljava/lang/String;

    iget-object v0, v0, Lcom/twitter/library/api/ContactEntity;->email:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v0, v6}, Lcom/twitter/library/api/TwitterContact;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/api/TwitterContact;

    invoke-virtual {v1, v0, p2, p3}, Lcom/twitter/android/client/c;->a([Lcom/twitter/library/api/TwitterContact;J)Ljava/lang/String;

    :cond_1
    return-void
.end method

.method private a(Lcom/twitter/library/api/upload/w;J)V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/twitter/library/api/upload/w;->k()Lcom/twitter/library/api/TwitterStatus;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v0, v2, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    if-eqz v0, :cond_1

    iget-object v0, v2, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    iget-object v0, v0, Lcom/twitter/library/api/TweetEntities;->mentions:Ljava/util/ArrayList;

    iget-object v3, v2, Lcom/twitter/library/api/TwitterStatus;->j:Lcom/twitter/library/api/TweetEntities;

    iget-object v4, v3, Lcom/twitter/library/api/TweetEntities;->hashtags:Ljava/util/ArrayList;

    if-nez v0, :cond_2

    move v3, v1

    :goto_0
    if-nez v4, :cond_3

    move v0, v1

    :goto_1
    iget-wide v4, v2, Lcom/twitter/library/api/TwitterStatus;->e:J

    const-wide/16 v6, 0x0

    cmp-long v2, v4, v6

    if-lez v2, :cond_4

    const-string/jumbo v2, ":composition:send_reply:"

    :goto_2
    if-lez v3, :cond_0

    iget-object v4, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    new-instance v5, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v5, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v6, v9, [Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, "mentions:count"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v5, v6}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v5

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v6, ""

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_0
    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    new-instance v4, Lcom/twitter/library/scribe/ScribeLog;

    invoke-direct {v4, p2, p3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v5, v9, [Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v6, "hashtags:count"

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-virtual {v4, v5}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->e(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_1
    return-void

    :cond_2
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    move v3, v0

    goto/16 :goto_0

    :cond_3
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v0

    goto/16 :goto_1

    :cond_4
    const-string/jumbo v2, ":composition:send_tweet:"

    goto/16 :goto_2
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/e;)V
    .locals 15

    iget-object v1, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v12

    iget-object v1, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v1

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/twitter/library/api/upload/w;->k:Landroid/os/Bundle;

    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/service/e;->c()I

    move-result v2

    const/16 v4, 0xc8

    if-ne v2, v4, :cond_2

    const/4 v2, 0x1

    move v11, v2

    :goto_0
    invoke-static {v3}, Lcom/twitter/library/service/b;->b(Landroid/os/Bundle;)[I

    move-result-object v7

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    const-string/jumbo v2, "status_id"

    const-wide/16 v4, 0x0

    invoke-virtual {v3, v2, v4, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v3

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/upload/w;->i()Ljava/lang/String;

    move-result-object v5

    iget-object v2, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/android/xu;->a(Landroid/content/Context;)Lcom/twitter/android/xu;

    move-result-object v13

    invoke-virtual/range {p3 .. p3}, Lcom/twitter/library/service/e;->c()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string/jumbo v9, ":composition:send_tweet:save_draft:complete"

    aput-object v9, v2, v8

    invoke-virtual {v12, v6, v7, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v6, 0x7f0f031d    # com.twitter.android.R.string.post_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    :cond_0
    :goto_1
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/upload/w;->l()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v6, "welcome:compose:::"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    if-eqz v11, :cond_7

    const-string/jumbo v1, "success"

    :goto_2
    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v5

    invoke-virtual {v12, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    const/4 v2, 0x0

    move v11, v2

    goto :goto_0

    :sswitch_0
    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/upload/w;->j()J

    move-result-wide v6

    const-wide/16 v13, 0x0

    cmp-long v2, v6, v13

    if-lez v2, :cond_3

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/upload/w;->r()I

    move-result v2

    const/4 v10, 0x3

    if-ne v2, v10, :cond_3

    iget-object v2, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/android/xu;->a(Landroid/content/Context;)Lcom/twitter/android/xu;

    move-result-object v2

    invoke-virtual {v2, v8, v9, v6, v7}, Lcom/twitter/android/xu;->b(JJ)V

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v6, v7}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;)V

    :cond_3
    const v2, 0x7f0f02b2    # com.twitter.android.R.string.notif_sending_tweet_success

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v5, v2}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    invoke-virtual/range {p2 .. p2}, Lcom/twitter/library/api/upload/w;->m()Lcom/twitter/library/api/TweetEntities;

    move-result-object v2

    invoke-direct {p0, v2, v3, v4}, Lcom/twitter/android/client/cg;->a(Lcom/twitter/library/api/TweetEntities;J)V

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v8, v9}, Lcom/twitter/android/client/cg;->a(Lcom/twitter/library/api/upload/w;J)V

    goto :goto_1

    :sswitch_1
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v8

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v10, ":composition:send_tweet:save_draft:complete"

    aput-object v10, v2, v6

    invoke-virtual {v12, v8, v9, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/16 v2, 0xbb

    invoke-static {v7, v2}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v2

    if-eqz v2, :cond_4

    const v6, 0x7f0f012c    # com.twitter.android.R.string.duplicate_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    goto/16 :goto_1

    :cond_4
    const/16 v2, 0xe0

    invoke-static {v7, v2}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v2

    if-eqz v2, :cond_5

    const v6, 0x7f0f031d    # com.twitter.android.R.string.post_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    iget-object v2, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    const-class v5, Lcom/twitter/android/DialogActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "blocked_spammer_tweet"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_5
    const/16 v2, 0xdf

    invoke-static {v7, v2}, Lcom/twitter/library/util/Util;->a([II)Z

    move-result v2

    if-eqz v2, :cond_6

    const v6, 0x7f0f031d    # com.twitter.android.R.string.post_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    iget-object v2, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    const-class v5, Lcom/twitter/android/DialogActivity;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "blocked_automated_spammer"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    const/high16 v4, 0x10000000

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_1

    :cond_6
    const v6, 0x7f0f031d    # com.twitter.android.R.string.post_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    invoke-virtual {v12, v7}, Lcom/twitter/android/client/c;->b([I)V

    goto/16 :goto_1

    :sswitch_2
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, ":composition:send_tweet:save_draft:complete"

    aput-object v6, v4, v5

    invoke-virtual {v12, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_1

    :sswitch_3
    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v6

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v8, 0x0

    const-string/jumbo v9, ":composition:send_tweet:save_draft:complete"

    aput-object v9, v2, v8

    invoke-virtual {v12, v6, v7, v2}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v6, 0x7f0f031d    # com.twitter.android.R.string.post_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->b(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    goto/16 :goto_1

    :sswitch_4
    invoke-virtual {v13, v8, v9, v3, v4}, Lcom/twitter/android/xu;->a(JJ)V

    const v6, 0x7f0f0363    # com.twitter.android.R.string.retry_tweet_error

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;I)V

    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->g()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v13, v8, v9, v3, v4}, Lcom/twitter/android/xu;->c(JJ)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v10

    move-object v5, v13

    move-wide v6, v8

    move-wide v8, v3

    invoke-virtual/range {v5 .. v10}, Lcom/twitter/android/xu;->b(JJZ)V

    goto/16 :goto_1

    :sswitch_5
    invoke-static {}, Lcom/twitter/library/telephony/TelephonyUtil;->g()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v13, v8, v9, v3, v4}, Lcom/twitter/android/xu;->c(JJ)Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/library/client/Session;->d()Z

    move-result v10

    move-object v5, v13

    move-wide v6, v8

    move-wide v8, v3

    invoke-virtual/range {v5 .. v10}, Lcom/twitter/android/xu;->b(JJZ)V

    goto/16 :goto_1

    :cond_7
    const-string/jumbo v1, "failure"

    goto/16 :goto_2

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x130 -> :sswitch_2
        0x193 -> :sswitch_1
        0x3ea -> :sswitch_3
        0x3eb -> :sswitch_4
        0x3ec -> :sswitch_5
        0x3ed -> :sswitch_3
        0x3ef -> :sswitch_3
        0x3f0 -> :sswitch_3
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/cg;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 1

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/client/cg;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/e;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/e;)V
    .locals 6

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/cg;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v2, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    check-cast p1, Lcom/twitter/library/api/upload/w;

    invoke-direct {p0, v1, p1, p2}, Lcom/twitter/android/client/cg;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/w;Lcom/twitter/library/service/e;)V

    goto :goto_0
.end method
