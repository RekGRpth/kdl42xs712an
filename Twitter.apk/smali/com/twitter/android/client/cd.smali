.class public Lcom/twitter/android/client/cd;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Z

.field private static final b:Ljava/util/concurrent/atomic/AtomicInteger;

.field private static final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    invoke-static {}, Lcom/twitter/library/client/App;->m()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Uploader"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/twitter/android/client/cd;->a:Z

    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    sput-object v0, Lcom/twitter/android/client/cd;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    sput-object v0, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/twitter/library/client/Session;)Lcom/twitter/library/client/v;
    .locals 3

    sget-object v1, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/v;

    monitor-exit v1

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    const/4 v1, 0x0

    const v2, 0x7f0f02af    # com.twitter.android.R.string.notif_sending_drafts

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    new-instance v0, Lcom/twitter/library/api/upload/t;

    const-string/jumbo v1, "send_all_drafts"

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/upload/t;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/api/upload/r;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/upload/r;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/r;->a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/c;

    move-result-object v0

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/c;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/ce;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/ce;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;J)Ljava/lang/String;
    .locals 3

    new-instance v0, Lcom/twitter/library/api/upload/t;

    const-string/jumbo v1, "retry_tweet"

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/upload/t;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/api/upload/p;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/upload/p;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1, p2, p3}, Lcom/twitter/library/api/upload/p;->a(J)Lcom/twitter/library/api/upload/p;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/p;->a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/c;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/c;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/ce;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/ce;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/v;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    const v1, 0x7f0f02be    # com.twitter.android.R.string.notif_updating_profile

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/aw;->a(I)V

    new-instance v0, Lcom/twitter/library/api/upload/y;

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/api/upload/y;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;)V

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/y;->a(Lcom/twitter/library/client/v;)Lcom/twitter/library/api/upload/n;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/n;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/client/cd;->b:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/b;->d(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/service/b;->u()I

    move-result v1

    invoke-static {p1, p2, v1}, Lcom/twitter/android/client/cd;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/v;I)V

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/cf;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/cf;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;J)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/twitter/android/client/aw;->a(J)V

    const v1, 0x7f0f02b1    # com.twitter.android.R.string.notif_sending_tweet

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    new-instance v0, Lcom/twitter/library/api/upload/t;

    const-string/jumbo v1, "notification_retry"

    invoke-direct {v0, p0, v1}, Lcom/twitter/library/api/upload/t;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v1, Lcom/twitter/library/api/upload/s;

    invoke-direct {v1, p0, p1}, Lcom/twitter/library/api/upload/s;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v1, p3, p4}, Lcom/twitter/library/api/upload/s;->a(J)Lcom/twitter/library/api/upload/s;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/twitter/library/api/upload/s;->a(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/c;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/c;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/ce;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/ce;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;JLandroid/location/Location;Ljava/lang/String;Lcom/twitter/library/api/PromotedContent;Lcom/twitter/library/api/TweetEntities;ZLjava/util/ArrayList;)Ljava/lang/String;
    .locals 3

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    const v1, 0x7f0f02b1    # com.twitter.android.R.string.notif_sending_tweet

    invoke-virtual {v0, p1, p2, v1}, Lcom/twitter/android/client/aw;->a(Lcom/twitter/library/client/Session;Ljava/lang/String;I)V

    new-instance v0, Lcom/twitter/library/service/p;

    invoke-direct {v0, p1}, Lcom/twitter/library/service/p;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-static {p0, v0, p8}, Lcom/twitter/library/api/upload/x;->a(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/TweetEntities;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/api/upload/w;->a(Ljava/lang/String;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p7}, Lcom/twitter/library/api/upload/w;->a(Lcom/twitter/library/api/PromotedContent;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/twitter/library/api/upload/w;->b(J)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p5}, Lcom/twitter/library/api/upload/w;->a(Landroid/location/Location;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p6}, Lcom/twitter/library/api/upload/w;->b(Ljava/lang/String;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p9}, Lcom/twitter/library/api/upload/w;->b(Z)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    invoke-virtual {v0, p10}, Lcom/twitter/library/api/upload/w;->a(Ljava/util/ArrayList;)Lcom/twitter/library/api/upload/w;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/api/upload/t;

    const-string/jumbo v2, "send_tweet"

    invoke-direct {v1, p0, v2}, Lcom/twitter/library/api/upload/t;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/w;->b(Lcom/twitter/library/api/upload/o;)Lcom/twitter/library/api/upload/ad;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/twitter/library/api/upload/ad;->c(I)Lcom/twitter/library/service/b;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/upload/w;

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/client/cg;

    invoke-direct {v2, p0}, Lcom/twitter/android/client/cg;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0, v2}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;Lcom/twitter/library/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/library/client/Session;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/twitter/android/client/cd;->b(Lcom/twitter/library/client/Session;I)V

    return-void
.end method

.method private static a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/v;I)V
    .locals 3

    iput p2, p1, Lcom/twitter/library/client/v;->i:I

    sget-object v1, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method private static b(Lcom/twitter/library/client/Session;I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/library/client/Session;->c()Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    monitor-enter v2

    :try_start_0
    sget-object v0, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/client/v;

    if-eqz v0, :cond_0

    iget v0, v0, Lcom/twitter/library/client/v;->i:I

    if-ne v0, p1, :cond_0

    sget-object v0, Lcom/twitter/android/client/cd;->c:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    monitor-exit v2

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
