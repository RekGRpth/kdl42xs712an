.class public Lcom/twitter/android/client/cj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final a:Landroid/content/Context;

.field private b:Lcom/twitter/android/client/WidgetControl$WidgetList;

.field private c:[Lcom/twitter/android/client/ci;

.field private d:I

.field private e:Lcom/twitter/android/client/WidgetControl;

.field private f:J

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;IJLjava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x14

    new-array v0, v0, [Lcom/twitter/android/client/ci;

    iput-object v0, p0, Lcom/twitter/android/client/cj;->c:[Lcom/twitter/android/client/ci;

    iput-object p1, p0, Lcom/twitter/android/client/cj;->a:Landroid/content/Context;

    iput p2, p0, Lcom/twitter/android/client/cj;->d:I

    iput-wide p3, p0, Lcom/twitter/android/client/cj;->f:J

    iput-object p5, p0, Lcom/twitter/android/client/cj;->g:Ljava/lang/String;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Lcom/twitter/android/client/c;->q(J)Lcom/twitter/android/client/WidgetControl;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/cj;->e:Lcom/twitter/android/client/WidgetControl;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/client/cj;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/cj;->a:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/cj;->b:Lcom/twitter/android/client/WidgetControl$WidgetList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/cj;->b:Lcom/twitter/android/client/WidgetControl$WidgetList;

    invoke-virtual {v0}, Lcom/twitter/android/client/WidgetControl$WidgetList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/client/cj;->b:Lcom/twitter/android/client/WidgetControl$WidgetList;

    invoke-virtual {v0, p1}, Lcom/twitter/android/client/WidgetControl$WidgetList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/provider/Tweet;

    iget-object v1, p0, Lcom/twitter/android/client/cj;->c:[Lcom/twitter/android/client/ci;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/client/cj;->c:[Lcom/twitter/android/client/ci;

    aget-object v0, v0, p1

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/cj;->c:[Lcom/twitter/android/client/ci;

    new-instance v3, Lcom/twitter/android/client/ci;

    new-instance v4, Landroid/widget/RemoteViews;

    iget-object v5, p0, Lcom/twitter/android/client/cj;->a:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    const v6, 0x7f030123    # com.twitter.android.R.layout.scrollable_tweet_view

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    invoke-direct {v3, v4}, Lcom/twitter/android/client/ci;-><init>(Landroid/widget/RemoteViews;)V

    aput-object v3, v0, p1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/cj;->c:[Lcom/twitter/android/client/ci;

    aget-object v0, v0, p1

    iput-object v2, v0, Lcom/twitter/android/client/ci;->b:Lcom/twitter/library/provider/Tweet;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/twitter/android/client/cj;->c:[Lcom/twitter/android/client/ci;

    aget-object v0, v0, p1

    iget-object v1, v0, Lcom/twitter/android/client/ci;->a:Landroid/widget/RemoteViews;

    invoke-virtual {v2}, Lcom/twitter/library/provider/Tweet;->y()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/client/cj;->e:Lcom/twitter/android/client/WidgetControl;

    invoke-virtual {v3}, Lcom/twitter/android/client/WidgetControl;->d()Lcom/twitter/library/util/ao;

    move-result-object v3

    invoke-virtual {v3, v0}, Lcom/twitter/library/util/ao;->b(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    if-nez v3, :cond_1

    new-instance v3, Lcom/twitter/android/client/ck;

    invoke-direct {v3, p0, v7}, Lcom/twitter/android/client/ck;-><init>(Lcom/twitter/android/client/cj;Lcom/twitter/android/client/ch;)V

    new-instance v4, Lcom/twitter/library/network/d;

    iget-object v5, p0, Lcom/twitter/android/client/cj;->a:Landroid/content/Context;

    invoke-direct {v4, v5, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v4, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    invoke-virtual {v3}, Lcom/twitter/android/client/ck;->a()Landroid/graphics/Bitmap;

    move-result-object v3

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/cj;->a:Landroid/content/Context;

    iget-wide v4, p0, Lcom/twitter/android/client/cj;->f:J

    iget-object v6, p0, Lcom/twitter/android/client/cj;->g:Ljava/lang/String;

    invoke-static/range {v0 .. v6}, Lcom/twitter/android/client/WidgetControl;->a(Landroid/content/Context;Landroid/widget/RemoteViews;Lcom/twitter/library/provider/Tweet;Landroid/graphics/Bitmap;JLjava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->C:J

    iget-wide v4, p0, Lcom/twitter/android/client/cj;->f:J

    invoke-static {v2, v3, v4, v5}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "sb_account_name"

    iget-object v3, p0, Lcom/twitter/android/client/cj;->g:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "ref_event"

    const-string/jumbo v3, "widget::::click"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const v2, 0x7f090095    # com.twitter.android.R.id.row

    invoke-virtual {v1, v2, v0}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    return-object v1

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getViewTypeCount()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/cj;->e:Lcom/twitter/android/client/WidgetControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/cj;->e:Lcom/twitter/android/client/WidgetControl;

    iget v1, p0, Lcom/twitter/android/client/cj;->d:I

    invoke-static {v0, v1}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/WidgetControl;I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/cj;->b:Lcom/twitter/android/client/WidgetControl$WidgetList;

    :cond_0
    return-void
.end method

.method public onDataSetChanged()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/cj;->e:Lcom/twitter/android/client/WidgetControl;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/cj;->e:Lcom/twitter/android/client/WidgetControl;

    iget v1, p0, Lcom/twitter/android/client/cj;->d:I

    invoke-static {v0, v1}, Lcom/twitter/android/client/WidgetControl;->a(Lcom/twitter/android/client/WidgetControl;I)Lcom/twitter/android/client/WidgetControl$WidgetList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/cj;->b:Lcom/twitter/android/client/WidgetControl$WidgetList;

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 0

    return-void
.end method
