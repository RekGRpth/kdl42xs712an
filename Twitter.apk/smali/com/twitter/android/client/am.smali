.class Lcom/twitter/android/client/am;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Lcom/twitter/android/client/c;

.field final synthetic c:Lcom/twitter/android/client/al;


# direct methods
.method constructor <init>(Lcom/twitter/android/client/al;Ljava/lang/String;Lcom/twitter/android/client/c;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/client/am;->c:Lcom/twitter/android/client/al;

    iput-object p2, p0, Lcom/twitter/android/client/am;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/twitter/android/client/am;->b:Lcom/twitter/android/client/c;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/util/HashMap;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/am;->a:Ljava/lang/String;

    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/am;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v0, p0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_0

    if-eqz p4, :cond_0

    invoke-virtual {p4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/client/am;->c:Lcom/twitter/android/client/al;

    invoke-virtual {v1, v3, v4, v0}, Lcom/twitter/android/client/al;->a(JI)V

    goto :goto_0

    :cond_0
    return-void
.end method
