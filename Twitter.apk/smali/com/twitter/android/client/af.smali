.class Lcom/twitter/android/client/af;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/service/c;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Lcom/twitter/android/client/BaseListFragment;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/client/af;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Lcom/twitter/android/client/BaseListFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/af;->a:Landroid/content/Context;

    return-void
.end method


# virtual methods
.method public a(IILcom/twitter/library/service/b;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/af;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1, p2, p3}, Lcom/twitter/android/client/BaseListFragment;->a(IILcom/twitter/library/service/b;)V

    :cond_0
    return-void
.end method

.method public b(IILcom/twitter/library/service/b;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/af;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/af;->a:Landroid/content/Context;

    invoke-virtual {v0, v1, p1, p2, p3}, Lcom/twitter/android/client/BaseListFragment;->a(Landroid/content/Context;IILcom/twitter/library/service/b;)V

    :cond_0
    return-void
.end method
