.class public abstract Lcom/twitter/android/client/notifications/w;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected final a:Lcom/twitter/library/platform/e;

.field protected final b:Ljava/lang/String;

.field protected final c:J

.field protected d:Landroid/content/Context;

.field protected e:I


# direct methods
.method public constructor <init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/client/notifications/w;->a:Lcom/twitter/library/platform/e;

    iput-object p2, p0, Lcom/twitter/android/client/notifications/w;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/twitter/android/client/notifications/w;->c:J

    return-void
.end method


# virtual methods
.method public abstract a()I
.end method

.method protected a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;
    .locals 5

    new-instance v0, Landroid/text/SpannableString;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\u2007"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/SpannableString;-><init>(Ljava/lang/CharSequence;)V

    new-instance v1, Landroid/text/style/ForegroundColorSpan;

    iget v2, p0, Lcom/twitter/android/client/notifications/w;->e:I

    invoke-direct {v1, v2}, Landroid/text/style/ForegroundColorSpan;-><init>(I)V

    const/4 v2, 0x0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    const/16 v4, 0x21

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/text/SpannableString;->setSpan(Ljava/lang/Object;III)V

    return-object v0
.end method

.method a(Landroid/content/Context;)V
    .locals 2

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/notifications/w;->d:Landroid/content/Context;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x1060005    # android.R.color.secondary_text_dark

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/client/notifications/w;->e:I

    return-void
.end method

.method protected b(Lcom/twitter/library/platform/d;)Landroid/text/SpannableString;
    .locals 2

    iget-object v0, p1, Lcom/twitter/library/platform/d;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/twitter/library/platform/d;->c:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/client/notifications/w;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/text/SpannableString;

    move-result-object v0

    return-object v0
.end method

.method public abstract b()Ljava/lang/String;
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method protected d()[Lcom/twitter/library/platform/d;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/w;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    return-object v0
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract f()Landroid/content/Intent;
.end method

.method public g()Z
    .locals 2

    const/4 v0, 0x1

    iget-object v1, p0, Lcom/twitter/android/client/notifications/w;->a:Lcom/twitter/library/platform/e;

    iget-object v1, v1, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v1, v1

    if-gt v1, v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/w;->a:Lcom/twitter/library/platform/e;

    iget v1, v1, Lcom/twitter/library/platform/e;->b:I

    if-le v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/w;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->o:I

    return v0
.end method

.method public i()[I
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/w;->d()[Lcom/twitter/library/platform/d;

    move-result-object v1

    array-length v2, v1

    new-array v3, v2, [I

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v4, v1, v0

    iget v4, v4, Lcom/twitter/library/platform/d;->b:I

    aput v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-object v3
.end method

.method public j()Landroid/support/v4/app/NotificationCompat$InboxStyle;
    .locals 5

    new-instance v1, Landroid/support/v4/app/NotificationCompat$InboxStyle;

    invoke-direct {v1}, Landroid/support/v4/app/NotificationCompat$InboxStyle;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/w;->d()[Lcom/twitter/library/platform/d;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    invoke-virtual {p0, v4}, Lcom/twitter/android/client/notifications/w;->b(Lcom/twitter/library/platform/d;)Landroid/text/SpannableString;

    move-result-object v4

    if-eqz v4, :cond_0

    invoke-virtual {v1, v4}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->addLine(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/w;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setSummaryText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/w;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v4/app/NotificationCompat$InboxStyle;->setBigContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$InboxStyle;

    return-object v1
.end method

.method public k()I
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/w;->d()[Lcom/twitter/library/platform/d;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public l()I
    .locals 1

    const/high16 v0, 0x4000000

    return v0
.end method

.method public m()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/w;->c()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
