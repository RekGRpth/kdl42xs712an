.class public abstract Lcom/twitter/android/client/notifications/TweetNotif;
.super Lcom/twitter/android/client/notifications/StatusBarNotif;
.source "Twttr"


# direct methods
.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/StatusBarNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected abstract a()I
.end method

.method protected abstract b()I
.end method

.method public c()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->b:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "@"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/TweetNotif;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method public f()Ljava/lang/String;
    .locals 6

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->b:I

    if-le v0, v5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/TweetNotif;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/TweetNotif;->y_()I

    move-result v1

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    invoke-virtual {v4}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    iget-object v3, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v3, v3, Lcom/twitter/library/platform/e;->e:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected g()Landroid/content/Intent;
    .locals 8

    const/4 v7, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->b:I

    if-le v0, v7, :cond_1

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/TweetNotif;->d:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/MainActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "page"

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/TweetNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/client/notifications/TweetNotif;->d:Landroid/content/Context;

    const-class v2, Lcom/twitter/android/TweetActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v1, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v1, v1, Lcom/twitter/library/platform/e;->f:J

    iget-wide v3, p0, Lcom/twitter/android/client/notifications/TweetNotif;->b:J

    invoke-static {v1, v2, v3, v4}, Lcom/twitter/library/provider/w;->a(JJ)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "com.twitter.android.home.tw."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/client/notifications/TweetNotif;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v1, v1, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    if-eqz v1, :cond_0

    invoke-static {}, Lgr;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/client/notifications/TweetNotif;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/client/notifications/TweetNotif;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v3, v3, Lcom/twitter/library/platform/e;->h:Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    invoke-virtual {v4}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-wide v5, v5, Lcom/twitter/library/platform/e;->g:J

    invoke-static/range {v0 .. v7}, Lgr;->a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JI)V

    goto :goto_0
.end method

.method protected abstract y_()I
.end method

.method public z_()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget v0, v0, Lcom/twitter/library/platform/e;->b:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->d:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/TweetNotif;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    iget-object v0, v0, Lcom/twitter/library/platform/e;->d:Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/client/notifications/TweetNotif;->a:Lcom/twitter/library/platform/e;

    invoke-virtual {v0}, Lcom/twitter/library/platform/e;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
