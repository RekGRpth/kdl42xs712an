.class public abstract Lcom/twitter/android/client/notifications/h;
.super Lcom/twitter/android/client/notifications/i;
.source "Twttr"


# instance fields
.field private f:[Lcom/twitter/library/platform/d;


# direct methods
.method public constructor <init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/i;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/twitter/library/platform/d;)Z
.end method

.method public d()[Lcom/twitter/library/platform/d;
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->f:[Lcom/twitter/library/platform/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->f:[Lcom/twitter/library/platform/d;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->a:Lcom/twitter/library/platform/e;

    iget-object v1, v0, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v0, v1

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "InteractionNotifThresholdAggregator should not be used with an empty inbox"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    array-length v3, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, v1, v0

    invoke-virtual {p0, v4}, Lcom/twitter/android/client/notifications/h;->a(Lcom/twitter/library/platform/d;)Z

    move-result v5

    if-eqz v5, :cond_2

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    sget-object v0, Lcom/twitter/library/platform/e;->a:[Lcom/twitter/library/platform/d;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/library/platform/d;

    iput-object v0, p0, Lcom/twitter/android/client/notifications/h;->f:[Lcom/twitter/library/platform/d;

    iget-object v0, p0, Lcom/twitter/android/client/notifications/h;->f:[Lcom/twitter/library/platform/d;

    goto :goto_0
.end method
