.class public Lcom/twitter/android/client/notifications/LifelineTweetNotif;
.super Lcom/twitter/android/client/notifications/TweetNotif;
.source "Twttr"


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/twitter/android/client/notifications/k;

    invoke-direct {v0}, Lcom/twitter/android/client/notifications/k;-><init>()V

    sput-object v0, Lcom/twitter/android/client/notifications/LifelineTweetNotif;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(Landroid/os/Parcel;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/client/notifications/TweetNotif;-><init>(Landroid/os/Parcel;)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/TweetNotif;-><init>(Lcom/twitter/library/platform/e;JLjava/lang/String;)V

    return-void
.end method


# virtual methods
.method protected a()I
    .locals 1

    const v0, 0x7f0f02a8    # com.twitter.android.R.string.notif_new_lifeline_alerts

    return v0
.end method

.method protected b()I
    .locals 1

    const v0, 0x7f0f02a8    # com.twitter.android.R.string.notif_new_lifeline_alerts

    return v0
.end method

.method public d()I
    .locals 1

    const v0, 0x7f02023f    # com.twitter.android.R.drawable.ic_stat_alert

    return v0
.end method

.method protected e()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "lifeline_alert"

    return-object v0
.end method

.method protected y_()I
    .locals 1

    const v0, 0x7f0f02b6    # com.twitter.android.R.string.notif_single_lifeline_alert_format

    return v0
.end method
