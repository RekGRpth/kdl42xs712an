.class public abstract Lcom/twitter/android/client/notifications/j;
.super Lcom/twitter/android/client/notifications/h;
.source "Twttr"


# instance fields
.field private f:I


# direct methods
.method public constructor <init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V
    .locals 7

    const/4 v6, -0x1

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/twitter/android/client/notifications/h;-><init>(Lcom/twitter/library/platform/e;Ljava/lang/String;J)V

    iput v6, p0, Lcom/twitter/android/client/notifications/j;->f:I

    iget-object v1, p1, Lcom/twitter/library/platform/e;->q:[Lcom/twitter/library/platform/d;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    iget v4, v3, Lcom/twitter/library/platform/d;->b:I

    iget v5, p1, Lcom/twitter/library/platform/e;->n:I

    if-ne v4, v5, :cond_1

    iget v0, v3, Lcom/twitter/library/platform/d;->a:I

    iput v0, p0, Lcom/twitter/android/client/notifications/j;->f:I

    :cond_0
    iget v0, p0, Lcom/twitter/android/client/notifications/j;->f:I

    if-ne v0, v6, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Couldn\'t find notification id: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/twitter/library/platform/e;->n:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " inside inbox"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/platform/d;)Z
    .locals 2

    iget v0, p0, Lcom/twitter/android/client/notifications/j;->f:I

    iget v1, p1, Lcom/twitter/library/platform/d;->a:I

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public g()Z
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/client/notifications/j;->d()[Lcom/twitter/library/platform/d;

    move-result-object v0

    array-length v0, v0

    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
