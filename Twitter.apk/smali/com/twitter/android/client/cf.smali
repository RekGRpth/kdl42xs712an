.class Lcom/twitter/android/client/cf;
.super Lcom/twitter/library/service/a;
.source "Twttr"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/library/service/a;-><init>()V

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/n;Lcom/twitter/library/service/e;)V
    .locals 12

    const/4 v5, 0x1

    iget-object v0, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v7

    iget-object v0, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v8

    iget-object v9, p2, Lcom/twitter/library/api/upload/n;->k:Landroid/os/Bundle;

    invoke-virtual {p3}, Lcom/twitter/library/service/e;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    const-string/jumbo v0, "user"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    move-object v6, v0

    :goto_0
    if-eqz v1, :cond_6

    if-eqz v6, :cond_6

    move v4, v5

    :goto_1
    if-eqz v6, :cond_7

    iget-wide v0, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    move-wide v2, v0

    :goto_2
    const-string/jumbo v0, "avatar_uri"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    if-eqz v4, :cond_0

    iget-object v1, v6, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v7, v5, v1, v0}, Lcom/twitter/android/client/c;->a(ILjava/lang/String;Landroid/net/Uri;)V

    :cond_0
    invoke-virtual {p2}, Lcom/twitter/library/api/upload/n;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v4, :cond_1

    iget-object v1, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    iget-object v5, v6, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-static {v1, v5}, Lcom/twitter/android/ProfileFragment;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    iget-wide v10, v6, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v1, v10, v11}, Lcom/twitter/library/util/Util;->e(Landroid/content/Context;J)Z

    :cond_1
    const-string/jumbo v1, "header_uri"

    invoke-virtual {v9, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/net/Uri;

    if-eqz v1, :cond_2

    if-eqz v4, :cond_2

    iget-object v5, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    iget-object v7, v6, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-static {v5, v7}, Lcom/twitter/android/ProfileFragment;->c(Landroid/content/Context;Ljava/lang/String;)V

    iget-object v5, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    invoke-static {v5, v2, v3}, Lcom/twitter/library/util/Util;->e(Landroid/content/Context;J)Z

    new-instance v5, Ljava/io/File;

    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v5, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    invoke-static {v1, v2, v3}, Lcom/twitter/library/util/Util;->c(Landroid/content/Context;J)Ljava/io/File;

    move-result-object v1

    if-eqz v1, :cond_8

    invoke-virtual {v5, v1}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v5

    if-eqz v5, :cond_2

    const-string/jumbo v5, "header_uri"

    invoke-static {v1}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v9, v5, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_2
    :goto_3
    if-eqz v0, :cond_3

    if-eqz v4, :cond_3

    invoke-static {v0}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    :cond_3
    invoke-virtual {p2}, Lcom/twitter/library/api/upload/n;->u()I

    move-result v0

    invoke-static {p1, v0}, Lcom/twitter/android/client/cd;->a(Lcom/twitter/library/client/Session;I)V

    if-eqz v4, :cond_9

    const v0, 0x7f0f02bd    # com.twitter.android.R.string.notif_update_profile_success

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v10

    cmp-long v1, v2, v10

    if-nez v1, :cond_4

    invoke-virtual {p1, v6}, Lcom/twitter/library/client/Session;->a(Lcom/twitter/library/api/TwitterUser;)V

    :cond_4
    :goto_4
    invoke-virtual {v8, v4, v0, v9, p1}, Lcom/twitter/android/client/aw;->a(ZILandroid/os/Bundle;Lcom/twitter/library/client/Session;)V

    return-void

    :cond_5
    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    const-string/jumbo v2, "user"

    invoke-virtual {v9, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    move-object v6, v0

    goto/16 :goto_0

    :cond_6
    const/4 v0, 0x0

    move v4, v0

    goto/16 :goto_1

    :cond_7
    const-wide/16 v0, 0x0

    move-wide v2, v0

    goto/16 :goto_2

    :cond_8
    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    goto :goto_3

    :cond_9
    invoke-virtual {p3}, Lcom/twitter/library/service/e;->c()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    const v0, 0x7f0f02bc    # com.twitter.android.R.string.notif_update_profile_fail

    goto :goto_4

    :sswitch_0
    const v0, 0x7f0f02bb    # com.twitter.android.R.string.notif_update_header_fail_unavailable

    goto :goto_4

    :sswitch_1
    const v0, 0x7f0f02ba    # com.twitter.android.R.string.notif_update_header_fail_invalid_size

    goto :goto_4

    :sswitch_data_0
    .sparse-switch
        0x1a6 -> :sswitch_1
        0x1f7 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic a(Lcom/twitter/internal/android/service/a;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/service/b;

    invoke-virtual {p0, p1}, Lcom/twitter/android/client/cf;->a(Lcom/twitter/library/service/b;)V

    return-void
.end method

.method public a(Lcom/twitter/library/service/b;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/client/cf;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-wide v0, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    cmp-long v0, v0, v3

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    check-cast v0, Lcom/twitter/library/api/upload/n;

    invoke-virtual {p1}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/service/e;

    invoke-direct {p0, v2, v0, v1}, Lcom/twitter/android/client/cf;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/upload/n;Lcom/twitter/library/service/e;)V

    goto :goto_0
.end method
