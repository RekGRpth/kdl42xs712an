.class Lcom/twitter/android/fh;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/EditAccountActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/EditAccountActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 8

    const v3, 0x7f0f012f    # com.twitter.android.R.string.edit_account_email_username_update_failed

    const v7, 0x7f0f0131    # com.twitter.android.R.string.edit_account_update_failure

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "edit_account:::password:success"

    aput-object v3, v1, v2

    invoke-virtual {v0, v5, v6, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x193

    if-ne p3, v0, :cond_9

    invoke-static {p5}, Lcom/twitter/android/tg;->a(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v3}, Lcom/twitter/android/EditAccountActivity;->c(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/util/r;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v4}, Lcom/twitter/android/EditAccountActivity;->b(Lcom/twitter/android/EditAccountActivity;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Lcom/twitter/android/util/r;->a(Ljava/lang/String;)Lcom/twitter/android/util/r;

    move-result-object v3

    invoke-interface {v3}, Lcom/twitter/android/util/r;->q()Lcom/twitter/android/util/r;

    iget v3, v0, Lcom/twitter/library/api/al;->e:I

    const/16 v4, 0x72

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v3}, Lcom/twitter/android/EditAccountActivity;->d(Lcom/twitter/android/EditAccountActivity;)I

    move-result v3

    if-nez v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, v1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;I)I

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::password:wrong_old"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v3, "edit_account:::password:retry"

    aput-object v3, v1, v2

    invoke-virtual {v0, v5, v6, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    iget-object v1, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v1}, Lcom/twitter/android/EditAccountActivity;->e(Lcom/twitter/android/EditAccountActivity;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/EditAccountActivity;->b(Lcom/twitter/android/EditAccountActivity;Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v0, v0, Lcom/twitter/library/api/al;->e:I

    sparse-switch v0, :sswitch_data_0

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::password:failure"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    :sswitch_0
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::password:wrong_old"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::password:mismatch"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/EditAccountActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::password:weak"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0f02ed    # com.twitter.android.R.string.password_change_failure_too_week

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/EditAccountActivity;->removeDialog(I)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::password:minimum_length"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    const v2, 0x7f0f0443    # com.twitter.android.R.string.signup_error_password

    invoke-static {v0, v2, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->f(Lcom/twitter/android/EditAccountActivity;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {p5}, Lcom/twitter/android/tg;->c(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;

    move-result-object v0

    if-eqz v0, :cond_4

    move v0, v1

    :goto_1
    iget-object v4, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v4}, Lcom/twitter/android/EditAccountActivity;->g(Lcom/twitter/android/EditAccountActivity;)Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-static {p5}, Lcom/twitter/android/tg;->b(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;

    move-result-object v4

    if-eqz v4, :cond_5

    move v4, v1

    :goto_2
    if-eqz v0, :cond_6

    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v4, v1, [Ljava/lang/String;

    const-string/jumbo v7, "edit_account:::email:failure"

    aput-object v7, v4, v2

    invoke-virtual {v0, v5, v6, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v4, v1, [Ljava/lang/String;

    const-string/jumbo v7, "edit_account:::username:failure"

    aput-object v7, v4, v2

    invoke-virtual {v0, v5, v6, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    move v0, v3

    :goto_3
    iget-object v2, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v2}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    move v4, v2

    goto :goto_2

    :cond_6
    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::username:failure"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f0f0132    # com.twitter.android.R.string.edit_account_username_update_failed

    goto :goto_3

    :cond_7
    if-eqz v4, :cond_8

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::email:failure"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f0f012e    # com.twitter.android.R.string.edit_account_email_update_failed

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v4, v1, [Ljava/lang/String;

    const-string/jumbo v7, "edit_account:::settings:failure"

    aput-object v7, v4, v2

    invoke-virtual {v0, v5, v6, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    move v0, v3

    goto :goto_3

    :cond_9
    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-array v3, v1, [Ljava/lang/String;

    const-string/jumbo v4, "edit_account:::settings:failure"

    aput-object v4, v3, v2

    invoke-virtual {v0, v5, v6, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-virtual {v0}, Lcom/twitter/android/EditAccountActivity;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, v7, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/fh;->a:Lcom/twitter/android/EditAccountActivity;

    invoke-static {v0, p1}, Lcom/twitter/android/EditAccountActivity;->a(Lcom/twitter/android/EditAccountActivity;Lcom/twitter/library/client/Session;)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x3c -> :sswitch_1
        0x3e -> :sswitch_3
        0x72 -> :sswitch_0
        0xee -> :sswitch_2
    .end sparse-switch
.end method
