.class public Lcom/twitter/android/uh;
.super Lcom/twitter/android/ic;
.source "Twttr"


# instance fields
.field private A:Z

.field private final h:I

.field private final i:Landroid/view/View$OnClickListener;

.field private final j:Lcom/twitter/android/vf;

.field private final k:Ljava/util/List;

.field private final l:Ljava/util/ArrayList;

.field private final m:Lcom/twitter/android/zb;

.field private final n:Ljava/util/HashMap;

.field private o:I

.field private p:Lcom/twitter/android/ob;

.field private q:I

.field private final r:Lcom/twitter/android/zc;

.field private s:Ljava/util/Map;

.field private final t:Ljava/lang/String;

.field private final u:Landroid/view/LayoutInflater;

.field private final v:I

.field private final w:Ljava/lang/String;

.field private final x:Lcom/twitter/android/up;

.field private y:Lcom/twitter/library/api/Prompt;

.field private z:Z


# direct methods
.method public constructor <init>(Landroid/app/Activity;IIZZZILcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vf;Lcom/twitter/android/zb;Lcom/twitter/android/vs;Landroid/view/View$OnClickListener;Lcom/twitter/library/util/FriendshipCache;Ljava/lang/String;Z)V
    .locals 13

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move/from16 v4, p4

    move/from16 v5, p5

    move/from16 v6, p6

    move-object/from16 v7, p8

    move-object/from16 v8, p9

    move-object/from16 v9, p10

    move-object/from16 v10, p13

    move-object/from16 v11, p15

    move/from16 v12, p17

    invoke-direct/range {v1 .. v12}, Lcom/twitter/android/ic;-><init>(Landroid/app/Activity;IZZZLcom/twitter/library/widget/ap;Lcom/twitter/android/client/c;Lcom/twitter/library/widget/aa;Lcom/twitter/android/vs;Lcom/twitter/library/util/FriendshipCache;Z)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/uh;->k:Ljava/util/List;

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/uh;->l:Ljava/util/ArrayList;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/uh;->n:Ljava/util/HashMap;

    new-instance v1, Lcom/twitter/android/ut;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/android/ut;-><init>(Lcom/twitter/android/ui;)V

    iput-object v1, p0, Lcom/twitter/android/uh;->r:Lcom/twitter/android/zc;

    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/uh;->s:Ljava/util/Map;

    move/from16 v0, p3

    iput v0, p0, Lcom/twitter/android/uh;->h:I

    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/twitter/android/uh;->i:Landroid/view/View$OnClickListener;

    move-object/from16 v0, p11

    iput-object v0, p0, Lcom/twitter/android/uh;->j:Lcom/twitter/android/vf;

    move-object/from16 v0, p12

    iput-object v0, p0, Lcom/twitter/android/uh;->m:Lcom/twitter/android/zb;

    move/from16 v0, p7

    iput v0, p0, Lcom/twitter/android/uh;->q:I

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/twitter/android/uh;->t:Ljava/lang/String;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    invoke-static {p1}, Landroid/view/ViewConfiguration;->get(Landroid/content/Context;)Landroid/view/ViewConfiguration;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/ViewConfiguration;->getScaledTouchSlop()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/uh;->v:I

    const v1, 0x7f0f033a    # com.twitter.android.R.string.ptr_top_tweet_action

    invoke-virtual {p1, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/uh;->w:Ljava/lang/String;

    invoke-static {}, Lkm;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    :goto_0
    iput-object v1, p0, Lcom/twitter/android/uh;->x:Lcom/twitter/android/up;

    const/16 v1, 0x1a

    move/from16 v0, p3

    if-ne v0, v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {p0, v1}, Lcom/twitter/android/uh;->h(Z)V

    return-void

    :cond_0
    new-instance v1, Lcom/twitter/android/up;

    invoke-direct {v1, p1}, Lcom/twitter/android/up;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private a(ILandroid/view/ViewGroup;Landroid/view/View;)Landroid/view/View;
    .locals 8

    new-instance v4, Lcom/twitter/android/um;

    invoke-direct {v4, p3}, Lcom/twitter/android/um;-><init>(Landroid/view/View;)V

    new-instance v6, Lcom/twitter/android/ur;

    const/4 v0, 0x0

    invoke-direct {v6, v4, v0, p1}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/um;Lcom/twitter/library/api/TimelineScribeContent;I)V

    invoke-virtual {p3, v6}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v0, v4, Lcom/twitter/android/um;->b:Lcom/twitter/android/widget/PipView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    iget-object v7, v4, Lcom/twitter/android/um;->a:Landroid/support/v4/view/ViewPager;

    new-instance v0, Lcom/twitter/android/ui;

    iget v3, p0, Lcom/twitter/android/uh;->v:I

    move-object v1, p0

    move-object v2, p2

    move v5, p1

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/ui;-><init>(Lcom/twitter/android/uh;Landroid/view/ViewParent;ILcom/twitter/android/um;ILcom/twitter/android/ur;)V

    invoke-virtual {v7, v0}, Landroid/support/v4/view/ViewPager;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    return-object p3
.end method

.method private a(Landroid/content/Context;)Landroid/view/View;
    .locals 1

    new-instance v0, Landroid/view/View;

    invoke-direct {v0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    return-object v0
.end method

.method private a(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f030087    # com.twitter.android.R.layout.grouped_event_gallery_row

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/4 v1, 0x2

    invoke-direct {p0, v1, p1, v0}, Lcom/twitter/android/uh;->a(ILandroid/view/ViewGroup;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;
    .locals 7

    sget v0, Lcom/twitter/library/provider/cc;->G:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget v0, Lcom/twitter/library/provider/cc;->K:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    sget v0, Lcom/twitter/library/provider/cc;->M:I

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;

    if-lez v2, :cond_1

    const v1, 0x7f0f0164    # com.twitter.android.R.string.event_subtitle_popular_local

    :goto_0
    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-wide v5, v0, Lcom/twitter/library/api/TwitterTopic$LocalEvent;->distance:D

    invoke-static {v5, v6}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-static {v4, v2}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    const v1, 0x7f0f0162    # com.twitter.android.R.string.event_subtitle_local

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/uh;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/uh;->t:Ljava/lang/String;

    return-object v0
.end method

.method private static a(Lcom/twitter/internal/android/widget/GroupedRowView;IZZZ)V
    .locals 0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-static {p0, p2, p3, p4}, Lcom/twitter/android/uh;->a(Lcom/twitter/internal/android/widget/GroupedRowView;ZZZ)V

    goto :goto_0

    :pswitch_1
    invoke-static {p0, p2, p3}, Lcom/twitter/android/uh;->a(Lcom/twitter/internal/android/widget/GroupedRowView;ZZ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Lcom/twitter/internal/android/widget/GroupedRowView;IZZZZZ)V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x2

    if-nez p3, :cond_0

    if-eqz p4, :cond_3

    packed-switch p1, :pswitch_data_0

    if-nez p2, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-eqz p5, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :cond_1
    if-eqz p2, :cond_2

    if-nez p6, :cond_2

    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->a()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :cond_3
    if-nez p5, :cond_4

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method private static a(Lcom/twitter/internal/android/widget/GroupedRowView;ZZ)V
    .locals 1

    const/4 v0, 0x2

    if-nez p1, :cond_0

    if-eqz p2, :cond_1

    :cond_0
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    :cond_1
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    return-void
.end method

.method private static a(Lcom/twitter/internal/android/widget/GroupedRowView;ZZZ)V
    .locals 4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p1, :cond_2

    if-eqz p2, :cond_2

    if-eqz p3, :cond_1

    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    :cond_0
    :goto_0
    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setGroupStyle(I)V

    return-void

    :cond_1
    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    goto :goto_0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/twitter/internal/android/widget/GroupedRowView;->setSingle(Z)V

    if-eqz p3, :cond_3

    invoke-virtual {p0, v2}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :cond_3
    if-eqz p1, :cond_4

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0

    :cond_4
    if-eqz p2, :cond_0

    invoke-virtual {p0, v3}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0
.end method

.method public static a(I)Z
    .locals 2

    const/4 v0, 0x1

    if-eqz p0, :cond_0

    if-eq p0, v0, :cond_0

    const/4 v1, 0x4

    if-eq p0, v1, :cond_0

    const/4 v1, 0x5

    if-ne p0, v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f03008a    # com.twitter.android.R.layout.grouped_highlight_header_row_view

    invoke-virtual {v0, v1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/un;

    invoke-direct {v1, v0}, Lcom/twitter/android/un;-><init>(Landroid/view/View;)V

    iget-object v2, p0, Lcom/twitter/android/uh;->i:Landroid/view/View$OnClickListener;

    if-eqz v2, :cond_0

    iget-object v2, v1, Lcom/twitter/android/un;->c:Landroid/view/View;

    iget-object v3, p0, Lcom/twitter/android/uh;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v2, v3}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v1, v4}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/un;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method private b(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f030097    # com.twitter.android.R.layout.grouped_user_gallery_row

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-direct {p0, v1, p1, v0}, Lcom/twitter/android/uh;->a(ILandroid/view/ViewGroup;Landroid/view/View;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/twitter/internal/android/widget/GroupedRowView;IZZZZZ)V
    .locals 2

    const/4 v1, 0x0

    if-nez p3, :cond_0

    if-eqz p4, :cond_0

    const/4 v0, 0x6

    if-ne p1, v0, :cond_2

    if-eqz p6, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/internal/android/widget/GroupedRowView;->a()V

    goto :goto_0

    :cond_2
    const/16 v0, 0x10

    if-ne p1, v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/internal/android/widget/GroupedRowView;->setStyle(I)V

    goto :goto_0
.end method

.method private f(I)I
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/uh;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    if-ltz p1, :cond_0

    if-eqz v1, :cond_0

    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v2

    invoke-virtual {p0, p1}, Lcom/twitter/android/uh;->getItemViewType(I)I

    move-result v0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private g(I)Landroid/view/View;
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09015b    # com.twitter.android.R.id.event_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EventView;

    iget-object v2, p0, Lcom/twitter/android/uh;->l:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/ref/WeakReference;

    invoke-direct {v3, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v0, v4}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/widget/EventView;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v1
.end method

.method private h(I)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/uo;

    invoke-direct {v1, v0}, Lcom/twitter/android/uo;-><init>(Landroid/view/View;)V

    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v1, v3}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/uo;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method private i(I)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    invoke-virtual {v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/tp;

    invoke-direct {v1, v0}, Lcom/twitter/android/tp;-><init>(Landroid/view/View;)V

    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v1, v3}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/tp;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method private static j(I)Z
    .locals 1

    if-nez p0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f030158    # com.twitter.android.R.layout.trend_header

    invoke-virtual {v0, v1, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/us;

    invoke-direct {v1, v0}, Lcom/twitter/android/us;-><init>(Landroid/view/View;)V

    iget-object v2, v1, Lcom/twitter/android/us;->c:Landroid/widget/TextView;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/twitter/android/uh;->b:Lcom/twitter/android/client/c;

    invoke-virtual {v4}, Lcom/twitter/android/client/c;->V()F

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/widget/TextView;->setTextSize(IF)V

    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v1, v5}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/us;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    return-object v0
.end method

.method private l()Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f030086    # com.twitter.android.R.layout.grouped_convo_tweet_row_view

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f0901a1    # com.twitter.android.R.id.tweet_stub

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewStub;

    const v2, 0x7f030162    # com.twitter.android.R.layout.tweet_row_view_conv_tweet

    invoke-virtual {v0, v2}, Landroid/view/ViewStub;->setLayoutResource(I)V

    invoke-virtual {v0}, Landroid/view/ViewStub;->inflate()Landroid/view/View;

    invoke-virtual {p0, v1}, Lcom/twitter/android/uh;->a(Landroid/view/View;)V

    const/16 v0, 0x10

    iget v2, p0, Lcom/twitter/android/uh;->h:I

    if-ne v0, v2, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/yd;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :cond_0
    return-object v1
.end method


# virtual methods
.method protected a(Lcom/twitter/library/widget/TweetView;Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 5

    invoke-super {p0, p1, p2}, Lcom/twitter/android/ic;->a(Lcom/twitter/library/widget/TweetView;Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v1

    sget v0, Lcom/twitter/library/provider/cc;->j:I

    invoke-interface {p2, v0}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TimelineScribeContent;

    if-eqz v0, :cond_0

    const-string/jumbo v2, "cursor"

    iget v3, v0, Lcom/twitter/library/api/TimelineScribeContent;->tweetProofCursor:I

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v2, "entity_type"

    iget v3, v0, Lcom/twitter/library/api/TimelineScribeContent;->eventType:I

    invoke-static {v3}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "query"

    iget-object v3, v0, Lcom/twitter/library/api/TimelineScribeContent;->query:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v2, "tweet_count"

    iget-wide v3, v0, Lcom/twitter/library/api/TimelineScribeContent;->tweetCount:J

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    :cond_0
    return-object v1
.end method

.method protected a(Landroid/view/View;Landroid/database/Cursor;)Lcom/twitter/library/provider/Tweet;
    .locals 7

    const/4 v6, 0x1

    iget-boolean v0, p0, Lcom/twitter/android/uh;->A:Z

    if-eqz v0, :cond_0

    new-instance v1, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v1, p2}, Lcom/twitter/library/provider/Tweet;-><init>(Landroid/database/Cursor;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yd;

    iget-object v0, v0, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v2, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    iget-wide v2, v2, Lcom/twitter/library/api/Prompt;->n:J

    iget-wide v4, v1, Lcom/twitter/library/provider/Tweet;->u:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setActionPrompt(Lcom/twitter/library/api/Prompt;)V

    invoke-virtual {v0, v6}, Lcom/twitter/library/widget/TweetView;->setShowActionPrompt(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/uh;->z:Z

    if-nez v0, :cond_0

    iput-boolean v6, p0, Lcom/twitter/android/uh;->z:Z

    iget-object v0, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0}, Lcom/twitter/library/api/Prompt;->b()V

    iget-object v0, p0, Lcom/twitter/android/uh;->b:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    iget v1, v1, Lcom/twitter/library/api/Prompt;->b:I

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->c(I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/twitter/android/ic;->a(Landroid/view/View;Landroid/database/Cursor;)Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    return-object v0

    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/TweetView;->setShowActionPrompt(Z)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/ob;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    return-void
.end method

.method public a(Lcom/twitter/library/api/Prompt;)V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/uh;->z:Z

    iget-object v0, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    invoke-virtual {v0, p1}, Lcom/twitter/library/api/Prompt;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/uh;->y:Lcom/twitter/library/api/Prompt;

    invoke-virtual {p0}, Lcom/twitter/android/uh;->notifyDataSetChanged()V

    goto :goto_0
.end method

.method public a(Ljava/util/HashMap;Z)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/ic;->a(Ljava/util/HashMap;Z)V

    iget-object v0, p0, Lcom/twitter/android/uh;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/az;

    if-nez v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    :cond_0
    invoke-virtual {v0, p1}, Lcom/twitter/android/az;->a(Ljava/util/HashMap;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/uh;->l:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/EventView;

    if-nez v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->remove()V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/EventView;->setEventImages(Ljava/util/HashMap;)V

    goto :goto_1

    :cond_3
    return-void
.end method

.method public a(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/uh;->A:Z

    if-eq v0, p1, :cond_0

    iput-boolean p1, p0, Lcom/twitter/android/uh;->A:Z

    invoke-virtual {p0}, Lcom/twitter/android/uh;->notifyDataSetChanged()V

    :cond_0
    return-void
.end method

.method public b(I)J
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/uh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    if-eqz v0, :cond_0

    sget v1, Lcom/twitter/library/provider/cc;->c:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, -0x1

    goto :goto_0
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 34

    sget v3, Lcom/twitter/library/provider/cc;->g:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    sget v3, Lcom/twitter/library/provider/cc;->d:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v25

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/twitter/android/uh;->getItemViewType(I)I

    move-result v26

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v27

    const-string/jumbo v3, "entity_group_start"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v28

    const-string/jumbo v3, "entity_group_end"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v29

    const-string/jumbo v3, "entity_start"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v30

    const-string/jumbo v3, "entity_end"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v31

    const-string/jumbo v3, "position"

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v4

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    packed-switch v26, :pswitch_data_0

    invoke-super/range {p0 .. p3}, Lcom/twitter/android/ic;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    invoke-static {v11}, Lcom/twitter/library/provider/au;->g(I)Z

    move-result v3

    if-eqz v3, :cond_30

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    move-object v9, v3

    check-cast v9, Lcom/twitter/android/yd;

    invoke-static {}, Lkm;->c()Z

    move-result v3

    if-nez v3, :cond_0

    new-instance v3, Lcom/twitter/library/provider/Tweet;

    invoke-direct {v3}, Lcom/twitter/library/provider/Tweet;-><init>()V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/uh;->x:Lcom/twitter/android/up;

    iget-object v4, v4, Lcom/twitter/android/up;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/twitter/library/provider/Tweet;->b(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/uh;->x:Lcom/twitter/android/up;

    iget-object v4, v4, Lcom/twitter/android/up;->b:Ljava/lang/String;

    iput-object v4, v3, Lcom/twitter/library/provider/Tweet;->g:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/uh;->x:Lcom/twitter/android/up;

    iget-object v4, v4, Lcom/twitter/android/up;->c:Ljava/lang/String;

    iput-object v4, v3, Lcom/twitter/library/provider/Tweet;->k:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/uh;->x:Lcom/twitter/android/up;

    iget-object v4, v4, Lcom/twitter/android/up;->d:Ljava/lang/String;

    iput-object v4, v3, Lcom/twitter/library/provider/Tweet;->G:Ljava/lang/String;

    iput v11, v3, Lcom/twitter/library/provider/Tweet;->Z:I

    iget-object v4, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/TweetView;->setHideInlineActions(Z)V

    iget-object v4, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v4, v3}, Lcom/twitter/library/widget/TweetView;->setTweet(Lcom/twitter/library/provider/Tweet;)V

    :cond_0
    sget v3, Lcom/twitter/library/provider/cc;->c:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    sget v3, Lcom/twitter/library/provider/cc;->j:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TimelineScribeContent;

    if-eqz v3, :cond_2e

    iget-object v4, v3, Lcom/twitter/library/api/TimelineScribeContent;->id:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    :goto_0
    new-instance v12, Lcom/twitter/android/uk;

    const-string/jumbo v4, "discover_module"

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-static/range {v3 .. v8}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v4

    invoke-direct {v12, v3, v4, v10, v11}, Lcom/twitter/android/uk;-><init>(Lcom/twitter/library/api/TimelineScribeContent;Lcom/twitter/library/scribe/ScribeItem;J)V

    iget-object v3, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v3, v12}, Lcom/twitter/library/widget/TweetView;->setTag(Ljava/lang/Object;)V

    invoke-static {}, Lkm;->f()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    iget-object v3, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v3}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    invoke-virtual {v3, v12}, Lcom/twitter/library/provider/Tweet;->a(Ljava/lang/Object;)V

    :cond_1
    invoke-static {}, Lkm;->d()Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v4, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-static {}, Lkm;->f()I

    move-result v3

    const/4 v5, 0x1

    if-ne v3, v5, :cond_2f

    const/4 v3, 0x1

    :goto_1
    invoke-virtual {v4, v3}, Lcom/twitter/library/widget/TweetView;->a(Z)V

    iget-object v3, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-static {}, Lkm;->e()Z

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/uh;->w:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/widget/TweetView;->a(ZLjava/lang/String;)V

    iget-object v3, v9, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/twitter/android/DiscoverActivity;

    move-object/from16 v0, p2

    invoke-direct {v4, v0, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v5, "no_tweet_marker"

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/TweetView;->setPivotIntent(Landroid/content/Intent;)V

    :cond_2
    :goto_2
    :pswitch_0
    const/16 v3, 0xe

    move/from16 v0, v26

    if-eq v0, v3, :cond_3

    move-object/from16 v3, p1

    check-cast v3, Lcom/twitter/internal/android/widget/GroupedRowView;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->isFirst()Z

    move-result v9

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/uh;->q:I

    move/from16 v0, v28

    move/from16 v1, v29

    invoke-static {v3, v4, v0, v1, v9}, Lcom/twitter/android/uh;->a(Lcom/twitter/internal/android/widget/GroupedRowView;IZZZ)V

    move/from16 v4, v25

    move/from16 v5, v28

    move/from16 v6, v29

    move/from16 v7, v30

    move/from16 v8, v31

    invoke-static/range {v3 .. v9}, Lcom/twitter/android/uh;->a(Lcom/twitter/internal/android/widget/GroupedRowView;IZZZZZ)V

    move/from16 v4, v26

    move/from16 v5, v28

    move/from16 v6, v29

    move/from16 v7, v30

    move/from16 v8, v31

    invoke-static/range {v3 .. v9}, Lcom/twitter/android/uh;->b(Lcom/twitter/internal/android/widget/GroupedRowView;IZZZZZ)V

    :cond_3
    return-void

    :pswitch_1
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/ur;

    iget-object v7, v3, Lcom/twitter/android/ur;->a:Lcom/twitter/android/uj;

    sget v3, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    const-wide/16 v5, 0x0

    const/4 v4, 0x0

    if-eqz v3, :cond_37

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/Conversation$Metadata;

    if-eqz v3, :cond_37

    iget-wide v4, v3, Lcom/twitter/library/api/Conversation$Metadata;->targetTweetId:J

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v8, 0x7f0e0012    # com.twitter.android.R.plurals.replies_count

    iget v9, v3, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget v3, v3, Lcom/twitter/library/api/Conversation$Metadata;->targetCount:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v10, v11

    invoke-virtual {v6, v8, v9, v10}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    :goto_3
    iput-wide v4, v7, Lcom/twitter/android/uj;->b:J

    if-nez v3, :cond_4

    iget-object v3, v7, Lcom/twitter/android/uj;->a:Landroid/widget/TextView;

    const v4, 0x7f0f04cf    # com.twitter.android.R.string.tweet_conversation_view_more

    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    :cond_4
    iget-object v4, v7, Lcom/twitter/android/uj;->a:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_2
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    move-object v4, v3

    check-cast v4, Lcom/twitter/android/ur;

    iget-object v12, v4, Lcom/twitter/android/ur;->b:Lcom/twitter/android/un;

    sget v3, Lcom/twitter/library/provider/cc;->c:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    sget v3, Lcom/twitter/library/provider/cc;->j:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TimelineScribeContent;

    iput-object v3, v4, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    invoke-static {v11}, Lcom/twitter/library/provider/au;->f(I)Z

    move-result v5

    if-eqz v5, :cond_7

    const v10, 0x7f0f01c9    # com.twitter.android.R.string.highlight_top_tweet

    const/4 v8, 0x0

    if-eqz v3, :cond_6

    iget-object v5, v3, Lcom/twitter/library/api/TimelineScribeContent;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v5

    :goto_4
    const-string/jumbo v7, "discover_module"

    const/4 v9, 0x0

    iput-wide v5, v12, Lcom/twitter/android/un;->d:J

    const v15, 0x7f0f01c9    # com.twitter.android.R.string.highlight_top_tweet

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v15}, Lcom/twitter/android/un;->a(Landroid/content/Context;I)V

    iput-object v7, v4, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    const-string/jumbo v15, "tweet"

    iput-object v15, v4, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    iput v9, v4, Lcom/twitter/android/ur;->n:I

    invoke-static {v13, v14}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v15

    iput-object v15, v4, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    if-eqz v4, :cond_36

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    const-wide/16 v15, -0x1

    invoke-static/range {v15 .. v16}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-interface {v4, v0, v15, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    move-object v4, v7

    move v7, v9

    move v9, v10

    :goto_5
    iget-object v10, v12, Lcom/twitter/android/un;->a:Lcom/twitter/android/widget/TextSwitcherView;

    invoke-virtual {v10}, Lcom/twitter/android/widget/TextSwitcherView;->a()V

    const/4 v15, 0x0

    invoke-virtual {v10, v15}, Lcom/twitter/android/widget/TextSwitcherView;->setOutAnimation(Landroid/view/animation/Animation;)V

    const/4 v15, 0x0

    invoke-virtual {v10, v15}, Lcom/twitter/android/widget/TextSwitcherView;->setInAnimation(Landroid/view/animation/Animation;)V

    move-object/from16 v0, p2

    invoke-virtual {v12, v0, v9}, Lcom/twitter/android/un;->a(Landroid/content/Context;I)V

    sget v9, Lcom/twitter/library/provider/cc;->i:I

    move-object/from16 v0, p3

    invoke-interface {v0, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-nez v9, :cond_c

    const/4 v9, 0x1

    :goto_6
    if-eqz v9, :cond_5

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/uh;->s:Ljava/util/Map;

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-interface {v9, v15, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/twitter/android/uh;->s:Ljava/util/Map;

    invoke-static {v13, v14}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v15

    invoke-interface {v9, v15}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_d

    const/4 v9, 0x0

    invoke-virtual {v10, v9}, Lcom/twitter/android/widget/TextSwitcherView;->setDisplayedChild(I)V

    :goto_7
    iget-object v9, v12, Lcom/twitter/android/un;->c:Landroid/view/View;

    new-instance v10, Lcom/twitter/android/ul;

    invoke-static/range {v3 .. v8}, Lcom/twitter/library/scribe/ScribeItem;->a(Lcom/twitter/library/api/TimelineScribeContent;Ljava/lang/String;JILjava/lang/String;)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v3

    invoke-direct {v10, v3, v13, v14}, Lcom/twitter/android/ul;-><init>(Lcom/twitter/library/scribe/ScribeItem;J)V

    invoke-virtual {v9, v10}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    invoke-static {v11}, Lcom/twitter/library/provider/au;->f(I)Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-static {}, Lkm;->f()I

    move-result v3

    const/4 v4, 0x1

    if-eq v3, v4, :cond_e

    iget-object v3, v12, Lcom/twitter/android/un;->c:Landroid/view/View;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :cond_6
    const-wide/16 v5, -0x1

    goto/16 :goto_4

    :cond_7
    invoke-static {v11}, Lcom/twitter/library/provider/au;->j(I)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-static {v11}, Lcom/twitter/library/provider/au;->p(I)Z

    move-result v4

    if-nez v4, :cond_8

    invoke-static {v11}, Lcom/twitter/library/provider/au;->o(I)Z

    move-result v4

    if-eqz v4, :cond_9

    :cond_8
    sget v4, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget-object v8, v4, Lcom/twitter/library/api/TwitterTopic$Metadata;->id:Ljava/lang/String;

    const-wide/16 v6, -0x1

    iget v5, v4, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    packed-switch v5, :pswitch_data_1

    :pswitch_3
    const v5, 0x7f0f01c8    # com.twitter.android.R.string.highlight_generic

    :goto_8
    iget v4, v4, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    invoke-static {v4}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v4

    const/16 v9, 0x10

    move-wide/from16 v32, v6

    move v7, v9

    move v9, v5

    move-wide/from16 v5, v32

    goto/16 :goto_5

    :pswitch_4
    const v5, 0x7f0f01c6    # com.twitter.android.R.string.highlight_event_tv

    goto :goto_8

    :pswitch_5
    const v5, 0x7f0f01c3    # com.twitter.android.R.string.highlight_event_sports

    goto :goto_8

    :pswitch_6
    const v5, 0x7f0f01c2    # com.twitter.android.R.string.highlight_event_local

    goto :goto_8

    :pswitch_7
    const v5, 0x7f0f01c5    # com.twitter.android.R.string.highlight_event_trending

    goto :goto_8

    :cond_9
    invoke-static {v11}, Lcom/twitter/library/provider/au;->k(I)Z

    move-result v4

    if-eqz v4, :cond_b

    const v9, 0x7f0f01cd    # com.twitter.android.R.string.highlight_wtf

    const-string/jumbo v4, "user_module"

    const/4 v7, 0x3

    const/4 v8, 0x0

    if-eqz v3, :cond_a

    iget-object v5, v3, Lcom/twitter/library/api/TimelineScribeContent;->id:Ljava/lang/String;

    invoke-static {v5}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    goto/16 :goto_5

    :cond_a
    const-wide/16 v5, -0x1

    goto/16 :goto_5

    :cond_b
    const v9, 0x7f0f01c8    # com.twitter.android.R.string.highlight_generic

    const/4 v4, 0x0

    const/4 v7, -0x1

    const/4 v8, 0x0

    const-wide/16 v5, -0x1

    goto/16 :goto_5

    :cond_c
    const/4 v9, 0x0

    goto/16 :goto_6

    :cond_d
    const/4 v9, 0x1

    invoke-virtual {v10, v9}, Lcom/twitter/android/widget/TextSwitcherView;->setDisplayedChild(I)V

    const v9, 0x7f04000a    # com.twitter.android.R.anim.highlight_slide_up

    move-object/from16 v0, p2

    invoke-virtual {v10, v0, v9}, Lcom/twitter/android/widget/TextSwitcherView;->setInAnimation(Landroid/content/Context;I)V

    const v9, 0x7f040009    # com.twitter.android.R.anim.highlight_slide_out

    move-object/from16 v0, p2

    invoke-virtual {v10, v0, v9}, Lcom/twitter/android/widget/TextSwitcherView;->setOutAnimation(Landroid/content/Context;I)V

    new-instance v9, Lcom/twitter/android/uq;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/twitter/android/uh;->s:Ljava/util/Map;

    invoke-direct {v9, v10, v13, v14, v15}, Lcom/twitter/android/uq;-><init>(Lcom/twitter/android/widget/TextSwitcherView;JLjava/util/Map;)V

    const-wide/16 v15, 0xbb8

    move-wide v0, v15

    invoke-virtual {v10, v9, v0, v1}, Lcom/twitter/android/widget/TextSwitcherView;->a(Ljava/lang/Runnable;J)V

    goto/16 :goto_7

    :cond_e
    iget-object v3, v12, Lcom/twitter/android/un;->c:Landroid/view/View;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_2

    :pswitch_8
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/ur;

    iget-object v4, v3, Lcom/twitter/android/ur;->g:Lcom/twitter/android/tp;

    sget v3, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget v3, v3, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    packed-switch v3, :pswitch_data_2

    :pswitch_9
    const v3, 0x7f0f01c8    # com.twitter.android.R.string.highlight_generic

    :goto_9
    iget-object v4, v4, Lcom/twitter/android/tp;->c:Landroid/widget/TextView;

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_2

    :pswitch_a
    const v3, 0x7f0f016b    # com.twitter.android.R.string.explore_tv

    goto :goto_9

    :pswitch_b
    const v3, 0x7f0f016a    # com.twitter.android.R.string.explore_sports

    goto :goto_9

    :pswitch_c
    const v3, 0x7f0f0168    # com.twitter.android.R.string.explore_nearby

    goto :goto_9

    :pswitch_d
    const v3, 0x7f0f0169    # com.twitter.android.R.string.explore_news

    goto :goto_9

    :pswitch_e
    if-nez v30, :cond_10

    const/4 v3, 0x1

    move v5, v3

    :goto_a
    if-nez v31, :cond_11

    const/4 v3, 0x1

    move v4, v3

    :goto_b
    const/16 v3, 0x10

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/uh;->h:I

    if-ne v3, v6, :cond_12

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/ur;

    iget-object v3, v3, Lcom/twitter/android/ur;->h:Lcom/twitter/android/yd;

    :goto_c
    if-eqz v30, :cond_f

    const/4 v6, 0x5

    move/from16 v0, v26

    if-ne v0, v6, :cond_f

    iget-object v6, v3, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    sget-object v7, Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;->b:Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;

    invoke-virtual {v6, v7}, Lcom/twitter/library/widget/TweetView;->setRepliedUserSummaryMode(Lcom/twitter/library/util/ConversationUtils$RepliedUserSummaryMode;)V

    :cond_f
    iget-object v3, v3, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v3, v5, v4}, Lcom/twitter/library/widget/TweetView;->a(ZZ)V

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/uh;->a(Landroid/view/View;Landroid/database/Cursor;)Lcom/twitter/library/provider/Tweet;

    goto/16 :goto_2

    :cond_10
    const/4 v3, 0x0

    move v5, v3

    goto :goto_a

    :cond_11
    const/4 v3, 0x0

    move v4, v3

    goto :goto_b

    :cond_12
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/yd;

    goto :goto_c

    :pswitch_f
    sget v3, Lcom/twitter/library/provider/cc;->c:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v19

    sget v3, Lcom/twitter/library/provider/cc;->j:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TimelineScribeContent;

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v4

    move-object v10, v4

    check-cast v10, Lcom/twitter/android/ur;

    iput-object v3, v10, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    iget-object v0, v10, Lcom/twitter/android/ur;->c:Lcom/twitter/android/um;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget v4, v0, Lcom/twitter/android/um;->h:I

    const-string/jumbo v3, "data_type_source_start"

    move-object/from16 v0, v27

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    move-object/from16 v0, v21

    iget-wide v6, v0, Lcom/twitter/android/um;->c:J

    cmp-long v3, v19, v6

    if-eqz v3, :cond_14

    const/4 v3, 0x1

    move/from16 v18, v3

    :goto_d
    if-eq v4, v5, :cond_15

    const/4 v3, 0x1

    move/from16 v17, v3

    :goto_e
    if-nez v18, :cond_13

    if-eqz v17, :cond_2

    :cond_13
    move-wide/from16 v0, v19

    move-object/from16 v2, v21

    iput-wide v0, v2, Lcom/twitter/android/um;->c:J

    move-object/from16 v0, v21

    iput v5, v0, Lcom/twitter/android/um;->h:I

    const/16 v3, 0xa

    move/from16 v0, v26

    if-ne v3, v0, :cond_16

    sget v3, Lcom/twitter/library/provider/cc;->D:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {v3}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v8

    const-string/jumbo v7, "event"

    const-wide/16 v5, -0x1

    const/16 v4, 0x10

    sget v3, Lcom/twitter/library/provider/cc;->C:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v9, Lcom/twitter/android/fl;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/uh;->j:Lcom/twitter/android/vf;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/android/uh;->h:I

    invoke-static {v12}, Lcom/twitter/android/uh;->j(I)Z

    move-result v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/uh;->t:Ljava/lang/String;

    move-object/from16 v0, p2

    invoke-direct {v9, v0, v11, v12, v13}, Lcom/twitter/android/fl;-><init>(Landroid/content/Context;Lcom/twitter/android/vf;ZLjava/lang/String;)V

    :goto_f
    move-object/from16 v0, v21

    iput-object v8, v0, Lcom/twitter/android/um;->d:Ljava/lang/String;

    move-object/from16 v0, v21

    iput-object v3, v0, Lcom/twitter/android/um;->f:Ljava/lang/String;

    move-object/from16 v0, v21

    iput-object v7, v0, Lcom/twitter/android/um;->e:Ljava/lang/String;

    iput-object v8, v10, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    iput-object v7, v10, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    iput-wide v5, v10, Lcom/twitter/android/ur;->m:J

    iput v4, v10, Lcom/twitter/android/ur;->n:I

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v10, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    if-eqz v18, :cond_17

    const/4 v3, 0x0

    :goto_10
    invoke-static/range {p3 .. p3}, Lcom/twitter/library/provider/m;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    move-result-object v5

    if-eqz v5, :cond_19

    const-string/jumbo v4, "data_type_source_start"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    const-string/jumbo v4, "data_type_source_end"

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    new-instance v4, Lcom/twitter/library/provider/s;

    invoke-direct {v4, v5, v6, v7}, Lcom/twitter/library/provider/s;-><init>(Landroid/database/Cursor;II)V

    :goto_11
    invoke-virtual {v9, v4}, Lcom/twitter/android/az;->a(Landroid/database/Cursor;)Landroid/database/Cursor;

    invoke-virtual {v9}, Lcom/twitter/android/az;->getCount()I

    move-result v4

    move-object/from16 v0, v21

    iget-object v5, v0, Lcom/twitter/android/um;->b:Lcom/twitter/android/widget/PipView;

    move-object/from16 v0, v21

    iget-object v6, v0, Lcom/twitter/android/um;->b:Lcom/twitter/android/widget/PipView;

    invoke-virtual {v6, v4}, Lcom/twitter/android/widget/PipView;->setPipCount(I)V

    invoke-virtual {v5, v3}, Lcom/twitter/android/widget/PipView;->setPipOnPosition(I)V

    const/4 v6, 0x1

    if-le v4, v6, :cond_1a

    const/4 v4, 0x0

    invoke-virtual {v5, v4}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    :goto_12
    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/twitter/android/um;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4, v9}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    move-object/from16 v0, v21

    iget-object v4, v0, Lcom/twitter/android/um;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->k:Ljava/util/List;

    new-instance v4, Ljava/lang/ref/WeakReference;

    invoke-direct {v4, v9}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-interface {v3, v0, v4, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_2

    :cond_14
    const/4 v3, 0x0

    move/from16 v18, v3

    goto/16 :goto_d

    :cond_15
    const/4 v3, 0x0

    move/from16 v17, v3

    goto/16 :goto_e

    :cond_16
    const-string/jumbo v16, "user_module"

    const-string/jumbo v15, "user"

    const/4 v12, 0x3

    sget v3, Lcom/twitter/library/provider/cc;->q:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v13

    const/4 v11, 0x0

    new-instance v3, Lcom/twitter/android/yu;

    invoke-virtual/range {p0 .. p0}, Lcom/twitter/android/uh;->c()Lcom/twitter/library/util/FriendshipCache;

    move-result-object v5

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/uh;->r:Lcom/twitter/android/zc;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/twitter/android/uh;->m:Lcom/twitter/android/zb;

    const/4 v9, 0x0

    move-object/from16 v4, p2

    invoke-direct/range {v3 .. v9}, Lcom/twitter/android/yu;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;ILcom/twitter/android/zc;Lcom/twitter/android/zb;Z)V

    move v4, v12

    move-wide v5, v13

    move-object v7, v15

    move-object/from16 v8, v16

    move-object v9, v3

    move-object v3, v11

    goto/16 :goto_f

    :cond_17
    if-eqz v17, :cond_18

    move-object/from16 v0, v21

    iget-object v3, v0, Lcom/twitter/android/um;->a:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    goto/16 :goto_10

    :cond_18
    const/4 v3, 0x0

    goto/16 :goto_10

    :cond_19
    new-instance v4, Lcom/twitter/library/provider/s;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v6

    move-object/from16 v0, p3

    invoke-direct {v4, v0, v5, v6}, Lcom/twitter/library/provider/s;-><init>(Landroid/database/Cursor;II)V

    goto/16 :goto_11

    :cond_1a
    const/16 v4, 0x8

    invoke-virtual {v5, v4}, Lcom/twitter/android/widget/PipView;->setVisibility(I)V

    goto/16 :goto_12

    :pswitch_10
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v24, v3

    check-cast v24, Lcom/twitter/android/ur;

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/twitter/android/ur;->e:Lcom/twitter/android/widget/EventView;

    sget v4, Lcom/twitter/library/provider/cc;->C:I

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v5, Lcom/twitter/library/provider/cc;->D:I

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    sget v6, Lcom/twitter/library/provider/cc;->I:I

    move-object/from16 v0, p3

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v7, Lcom/twitter/library/provider/cc;->N:I

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget v7, Lcom/twitter/library/provider/cc;->F:I

    move-object/from16 v0, p3

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/twitter/library/provider/cc;->G:I

    move-object/from16 v0, p3

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v10, Lcom/twitter/library/provider/cc;->M:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v17

    sget v10, Lcom/twitter/library/provider/cc;->E:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v15

    sget v10, Lcom/twitter/library/provider/cc;->H:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    sget v10, Lcom/twitter/library/provider/cc;->j:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v10

    invoke-static {v10}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/twitter/library/api/TimelineScribeContent;

    move-object/from16 v0, v24

    iput-object v10, v0, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    invoke-interface/range {p3 .. p3}, Landroid/database/Cursor;->getPosition()I

    move-result v10

    add-int/lit8 v10, v10, -0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v10}, Lcom/twitter/android/uh;->f(I)I

    move-result v10

    const/16 v11, 0xb

    if-eq v10, v11, :cond_1c

    sget v10, Lcom/twitter/library/provider/cc;->J:I

    move-object/from16 v0, p3

    invoke-interface {v0, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget v11, Lcom/twitter/library/provider/cc;->K:I

    move-object/from16 v0, p3

    invoke-interface {v0, v11}, Landroid/database/Cursor;->getInt(I)I

    move-result v11

    :goto_13
    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/android/uh;->h:I

    const/16 v13, 0x10

    if-ne v12, v13, :cond_1b

    const-string/jumbo v12, "tweet_count"

    move-object/from16 v0, v27

    invoke-virtual {v0, v12, v11}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1b
    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/android/uh;->h:I

    invoke-static {v12}, Lcom/twitter/android/uh;->j(I)Z

    move-result v18

    const-wide/16 v12, -0x1

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/uh;->b:Lcom/twitter/android/client/c;

    iget-object v14, v14, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    const/16 v19, 0x1

    const/16 v20, 0x0

    const/16 v21, 0x0

    const/16 v22, 0x0

    const/16 v23, 0x0

    invoke-virtual/range {v3 .. v23}, Lcom/twitter/android/widget/EventView;->a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IJLcom/twitter/library/widget/ap;Ljava/lang/String;Ljava/lang/String;[BZZLjava/lang/Long;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x5

    if-ne v5, v3, :cond_1d

    const-string/jumbo v3, "stream"

    move-object/from16 v0, v24

    iput-object v3, v0, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    const/4 v3, 0x0

    move-object/from16 v0, v24

    iput-object v3, v0, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    const/16 v3, 0x11

    move-object/from16 v0, v24

    iput v3, v0, Lcom/twitter/android/ur;->n:I

    move-object/from16 v0, v24

    iput-object v4, v0, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    :goto_14
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-interface {v3, v0, v4, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_2

    :cond_1c
    const/4 v10, 0x0

    const/4 v11, 0x0

    goto :goto_13

    :cond_1d
    invoke-static {v5}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v24

    iput-object v3, v0, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    const-string/jumbo v3, "event"

    move-object/from16 v0, v24

    iput-object v3, v0, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    const/16 v3, 0x10

    move-object/from16 v0, v24

    iput v3, v0, Lcom/twitter/android/ur;->n:I

    move-object/from16 v0, v24

    iput-object v4, v0, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    goto :goto_14

    :pswitch_11
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/ur;

    iget-object v5, v3, Lcom/twitter/android/ur;->f:Lcom/twitter/android/uo;

    sget v4, Lcom/twitter/library/provider/cc;->C:I

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v4, Lcom/twitter/library/provider/cc;->j:I

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    invoke-static {v4}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/twitter/library/api/TimelineScribeContent;

    iput-object v4, v3, Lcom/twitter/android/ur;->i:Lcom/twitter/library/api/TimelineScribeContent;

    sget v4, Lcom/twitter/library/provider/cc;->F:I

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static/range {p2 .. p3}, Lcom/twitter/android/uh;->a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v4, v7}, Lcom/twitter/android/uo;->a(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)V

    sget v4, Lcom/twitter/library/provider/cc;->D:I

    move-object/from16 v0, p3

    invoke-interface {v0, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    invoke-static {v4}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/twitter/android/ur;->p:Ljava/lang/String;

    const-string/jumbo v4, "event"

    iput-object v4, v3, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    const/16 v4, 0x10

    iput v4, v3, Lcom/twitter/android/ur;->n:I

    iput-object v6, v3, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/uh;->h:I

    const/16 v4, 0x10

    if-ne v3, v4, :cond_1e

    const-string/jumbo v3, "tweet_count"

    const/4 v4, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_1e
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    const-wide/16 v4, -0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-interface {v3, v0, v4, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_2

    :pswitch_12
    sget v3, Lcom/twitter/library/provider/cc;->C:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    move-object v15, v3

    check-cast v15, Lcom/twitter/android/ur;

    iput-object v4, v15, Lcom/twitter/android/ur;->l:Ljava/lang/String;

    const-string/jumbo v3, "trend"

    iput-object v3, v15, Lcom/twitter/android/ur;->o:Ljava/lang/String;

    iget-object v0, v15, Lcom/twitter/android/ur;->d:Lcom/twitter/android/us;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v3, v0, Lcom/twitter/android/us;->c:Landroid/widget/TextView;

    sget v5, Lcom/twitter/library/provider/cc;->F:I

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    sget v3, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    move-object/from16 v16, v3

    check-cast v16, Lcom/twitter/library/api/TwitterTopic$Metadata;

    const-string/jumbo v3, "entity_type"

    move-object/from16 v0, v16

    iget v5, v0, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    invoke-static {v5}, Lcom/twitter/library/api/TwitterTopic;->b(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v27

    invoke-virtual {v0, v3, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget v3, Lcom/twitter/library/provider/cc;->D:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v3, 0x2

    if-eq v5, v3, :cond_1f

    const/4 v3, 0x4

    if-eq v5, v3, :cond_1f

    if-nez v5, :cond_20

    :cond_1f
    sget v3, Lcom/twitter/library/provider/cc;->I:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    sget v3, Lcom/twitter/library/provider/cc;->F:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    sget v3, Lcom/twitter/library/provider/cc;->G:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    sget v3, Lcom/twitter/library/provider/cc;->M:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v14

    sget v3, Lcom/twitter/library/provider/cc;->N:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    sget v3, Lcom/twitter/library/provider/cc;->E:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    sget v3, Lcom/twitter/library/provider/cc;->H:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    sget v3, Lcom/twitter/library/provider/cc;->J:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    sget v3, Lcom/twitter/library/provider/cc;->K:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    new-instance v3, Lcom/twitter/android/widget/TopicView$TopicData;

    invoke-direct/range {v3 .. v14}, Lcom/twitter/android/widget/TopicView$TopicData;-><init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I[B)V

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/twitter/android/us;->f:Lcom/twitter/android/widget/TopicView$TopicData;

    :cond_20
    sget v3, Lcom/twitter/library/provider/cc;->L:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/PromotedContent;

    if-eqz v3, :cond_24

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->b:Landroid/widget/ImageView;

    const v6, 0x7f0202b4    # com.twitter.android.R.drawable.icn_trending_default

    invoke-virtual {v5, v6}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v3}, Lcom/twitter/library/api/PromotedContent;->b()Z

    move-result v5

    if-eqz v5, :cond_21

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_15
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    if-eqz v5, :cond_2

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/uh;->n:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_28

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/uh;->o:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/uh;->n:Ljava/util/HashMap;

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/uh;->o:I

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v6, v4, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/uh;->o:I

    add-int/lit8 v4, v4, 0x1

    move-object/from16 v0, p0

    iput v4, v0, Lcom/twitter/android/uh;->o:I

    move v4, v5

    :goto_16
    iput v4, v15, Lcom/twitter/android/ur;->k:I

    const-string/jumbo v5, "trend_cursor"

    move-object/from16 v0, v27

    invoke-virtual {v0, v5, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v4, "tweet_count"

    sget v5, Lcom/twitter/library/provider/cc;->K:I

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    move-object/from16 v0, v27

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    if-eqz v3, :cond_29

    iget-wide v3, v3, Lcom/twitter/library/api/PromotedContent;->promotedTrendId:J

    :goto_17
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/uh;->p:Lcom/twitter/android/ob;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v0, p1

    move-object/from16 v1, v27

    invoke-interface {v5, v0, v3, v1}, Lcom/twitter/android/ob;->a(Landroid/view/View;Ljava/lang/Object;Landroid/os/Bundle;)V

    goto/16 :goto_2

    :cond_21
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v3}, Lcom/twitter/library/api/PromotedContent;->a()Z

    move-result v5

    if-eqz v5, :cond_22

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const v6, 0x7f020124    # com.twitter.android.R.drawable.ic_badge_gov_default

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto :goto_15

    :cond_22
    iget-object v5, v3, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_23

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const v6, 0x7f0f0333    # com.twitter.android.R.string.promoted_by

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v9, v3, Lcom/twitter/library/api/PromotedContent;->advertiserName:Ljava/lang/String;

    aput-object v9, v7, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v6, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_23
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const v6, 0x7f020125    # com.twitter.android.R.drawable.ic_badge_promoted_default

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual {v5, v6, v7, v8, v9}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    goto/16 :goto_15

    :cond_24
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->d:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    move-object/from16 v0, v16

    iget v5, v0, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    packed-switch v5, :pswitch_data_3

    move-object/from16 v0, v16

    iget-boolean v5, v0, Lcom/twitter/library/api/TwitterTopic$Metadata;->spiking:Z

    if-eqz v5, :cond_25

    const v5, 0x7f0202ac    # com.twitter.android.R.drawable.icn_global_default

    :goto_18
    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/twitter/android/us;->b:Landroid/widget/ImageView;

    invoke-virtual {v6, v5}, Landroid/widget/ImageView;->setImageResource(I)V

    move-object/from16 v0, v16

    iget v5, v0, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    const/4 v6, 0x3

    if-ne v5, v6, :cond_26

    invoke-static/range {p2 .. p3}, Lcom/twitter/android/uh;->a(Landroid/content/Context;Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    :pswitch_13
    const v5, 0x7f0202b5    # com.twitter.android.R.drawable.icn_tv_default

    goto :goto_18

    :pswitch_14
    const v5, 0x7f0202ad    # com.twitter.android.R.drawable.icn_local_default

    goto :goto_18

    :pswitch_15
    const v5, 0x7f0202b1    # com.twitter.android.R.drawable.icn_sports_default

    goto :goto_18

    :cond_25
    const v5, 0x7f0202b4    # com.twitter.android.R.drawable.icn_trending_default

    goto :goto_18

    :cond_26
    sget v5, Lcom/twitter/library/provider/cc;->K:I

    move-object/from16 v0, p3

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    if-lez v6, :cond_27

    move-object/from16 v0, v16

    iget v5, v0, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    packed-switch v5, :pswitch_data_4

    :pswitch_16
    const v5, 0x7f0f0163    # com.twitter.android.R.string.event_subtitle_popular_event

    :goto_19
    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-static {v9, v6}, Lcom/twitter/library/util/Util;->a(Landroid/content/res/Resources;I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v7, v8

    move-object/from16 v0, p2

    invoke-virtual {v0, v5, v7}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, v17

    iget-object v6, v0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    const/4 v6, 0x0

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    :pswitch_17
    const v5, 0x7f0f0166    # com.twitter.android.R.string.event_subtitle_popular_trend

    goto :goto_19

    :pswitch_18
    const v5, 0x7f0f0165    # com.twitter.android.R.string.event_subtitle_popular_show

    goto :goto_19

    :pswitch_19
    const v5, 0x7f0f0163    # com.twitter.android.R.string.event_subtitle_popular_event

    goto :goto_19

    :cond_27
    move-object/from16 v0, v17

    iget-object v5, v0, Lcom/twitter/android/us;->e:Landroid/widget/TextView;

    const/16 v6, 0x8

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_15

    :cond_28
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/twitter/android/uh;->n:Ljava/util/HashMap;

    invoke-virtual {v5, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    goto/16 :goto_16

    :cond_29
    const-wide/16 v3, -0x1

    goto/16 :goto_17

    :pswitch_1a
    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/android/ur;

    iget-object v4, v3, Lcom/twitter/android/ur;->g:Lcom/twitter/android/tp;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/uh;->h:I

    if-nez v3, :cond_2d

    invoke-static {v11}, Lcom/twitter/library/provider/au;->f(I)Z

    move-result v3

    if-eqz v3, :cond_2a

    const v3, 0x7f0f01ca    # com.twitter.android.R.string.highlight_top_tweet_view_more

    :goto_1a
    iget-object v4, v4, Lcom/twitter/android/tp;->c:Landroid/widget/TextView;

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(I)V

    goto/16 :goto_2

    :cond_2a
    invoke-static {v11}, Lcom/twitter/library/provider/au;->j(I)Z

    move-result v3

    if-eqz v3, :cond_2b

    sget v3, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget v3, v3, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    packed-switch v3, :pswitch_data_5

    :pswitch_1b
    const v3, 0x7f0f0099    # com.twitter.android.R.string.cluster_footer_more

    goto :goto_1a

    :pswitch_1c
    const v3, 0x7f0f01c7    # com.twitter.android.R.string.highlight_event_tv_view_more

    goto :goto_1a

    :pswitch_1d
    const v3, 0x7f0f01c4    # com.twitter.android.R.string.highlight_event_sports_view_more

    goto :goto_1a

    :cond_2b
    invoke-static {v11}, Lcom/twitter/library/provider/au;->k(I)Z

    move-result v3

    if-eqz v3, :cond_2c

    const v3, 0x7f0f01ce    # com.twitter.android.R.string.highlight_wtf_view_more

    goto :goto_1a

    :cond_2c
    const v3, 0x7f0f0099    # com.twitter.android.R.string.cluster_footer_more

    goto :goto_1a

    :cond_2d
    sget v3, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterTopic$Metadata;

    iget v3, v3, Lcom/twitter/library/api/TwitterTopic$Metadata;->type:I

    packed-switch v3, :pswitch_data_6

    :pswitch_1e
    const v3, 0x7f0f0099    # com.twitter.android.R.string.cluster_footer_more

    goto :goto_1a

    :pswitch_1f
    const v3, 0x7f0f04bd    # com.twitter.android.R.string.trends_event_view_more_tv

    goto :goto_1a

    :pswitch_20
    const v3, 0x7f0f04bc    # com.twitter.android.R.string.trends_event_view_more_sports

    goto :goto_1a

    :cond_2e
    const-wide/16 v5, -0x1

    goto/16 :goto_0

    :cond_2f
    const/4 v3, 0x0

    goto/16 :goto_1

    :cond_30
    invoke-static {v11}, Lcom/twitter/library/provider/au;->e(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual/range {p1 .. p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v3

    move-object v12, v3

    check-cast v12, Lcom/twitter/android/yd;

    invoke-static {v11}, Lcom/twitter/library/provider/au;->m(I)Z

    move-result v3

    if-eqz v3, :cond_35

    sget v3, Lcom/twitter/library/provider/cc;->f:I

    move-object/from16 v0, p3

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v3

    invoke-static {v3}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TweetPivotOptions;

    if-eqz v3, :cond_32

    invoke-virtual {v3}, Lcom/twitter/library/api/TweetPivotOptions;->b()Z

    move-result v4

    if-eqz v4, :cond_34

    iget-object v4, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v5, 0x1

    iget-object v6, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotTitle:Ljava/lang/String;

    invoke-virtual {v4, v5, v6}, Lcom/twitter/library/widget/TweetView;->a(ZLjava/lang/String;)V

    :cond_31
    :goto_1b
    iget-object v13, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v4, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotEventId:Ljava/lang/String;

    iget v5, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotEventType:I

    iget-object v6, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotTitle:Ljava/lang/String;

    iget-object v7, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotQuery:Ljava/lang/String;

    const/4 v8, 0x0

    iget-object v9, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotHashtag:Ljava/lang/String;

    const/4 v10, 0x0

    const/4 v11, 0x0

    move-object/from16 v3, p2

    invoke-static/range {v3 .. v11}, Lcom/twitter/android/vf;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;)Landroid/content/Intent;

    move-result-object v3

    invoke-virtual {v13, v3}, Lcom/twitter/library/widget/TweetView;->setPivotIntent(Landroid/content/Intent;)V

    :cond_32
    :goto_1c
    move-object/from16 v0, p0

    iget v3, v0, Lcom/twitter/android/uh;->h:I

    if-nez v3, :cond_33

    invoke-static {}, Lkm;->g()Z

    move-result v3

    if-eqz v3, :cond_33

    iget-object v3, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    invoke-virtual {v3}, Lcom/twitter/library/widget/TweetView;->getTweet()Lcom/twitter/library/provider/Tweet;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/twitter/library/provider/Tweet;->a(Ljava/lang/Object;)V

    :cond_33
    iget-object v3, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/TweetView;->a(Z)V

    goto/16 :goto_2

    :cond_34
    invoke-virtual {v3}, Lcom/twitter/library/api/TweetPivotOptions;->a()Z

    move-result v4

    if-eqz v4, :cond_31

    iget-object v4, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    iget-object v5, v3, Lcom/twitter/library/api/TweetPivotOptions;->pivotHashtag:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/twitter/library/widget/TweetView;->a(Ljava/lang/String;)V

    iget-object v4, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Lcom/twitter/library/widget/TweetView;->a(ZLjava/lang/String;)V

    goto :goto_1b

    :cond_35
    iget-object v3, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/widget/TweetView;->a(ZLjava/lang/String;)V

    iget-object v3, v12, Lcom/twitter/android/yd;->d:Lcom/twitter/library/widget/TweetView;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Lcom/twitter/library/widget/TweetView;->setPivotIntent(Landroid/content/Intent;)V

    goto :goto_1c

    :cond_36
    move-object v4, v7

    move v7, v9

    move v9, v10

    goto/16 :goto_5

    :cond_37
    move-object v3, v4

    move-wide/from16 v32, v5

    move-wide/from16 v4, v32

    goto/16 :goto_3

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_e
        :pswitch_e
        :pswitch_2
        :pswitch_10
        :pswitch_11
        :pswitch_10
        :pswitch_f
        :pswitch_12
        :pswitch_1a
        :pswitch_f
        :pswitch_0
        :pswitch_11
        :pswitch_8
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_5
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_d
        :pswitch_9
        :pswitch_a
        :pswitch_c
        :pswitch_b
    .end packed-switch

    :pswitch_data_3
    .packed-switch 0x2
        :pswitch_13
        :pswitch_14
        :pswitch_15
    .end packed-switch

    :pswitch_data_4
    .packed-switch 0x1
        :pswitch_17
        :pswitch_18
        :pswitch_16
        :pswitch_19
    .end packed-switch

    :pswitch_data_5
    .packed-switch 0x2
        :pswitch_1c
        :pswitch_1b
        :pswitch_1d
    .end packed-switch

    :pswitch_data_6
    .packed-switch 0x2
        :pswitch_1f
        :pswitch_1e
        :pswitch_20
    .end packed-switch
.end method

.method public getItemViewType(I)I
    .locals 4

    invoke-virtual {p0, p1}, Lcom/twitter/android/uh;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/Cursor;

    sget v1, Lcom/twitter/library/provider/cc;->g:I

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    sget v2, Lcom/twitter/library/provider/cc;->e:I

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-interface {v0}, Landroid/database/Cursor;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v3, "entity_group_end"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    packed-switch v2, :pswitch_data_0

    :cond_0
    invoke-super {p0, p1}, Lcom/twitter/android/ic;->getItemViewType(I)I

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    if-eqz v0, :cond_1

    const/16 v0, 0xc

    goto :goto_0

    :cond_1
    invoke-static {v1}, Lcom/twitter/library/provider/au;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/android/uh;->h:I

    const/16 v1, 0x1e

    if-ne v0, v1, :cond_3

    const/16 v0, 0x10

    goto :goto_0

    :cond_3
    const/4 v0, 0x6

    goto :goto_0

    :pswitch_1
    invoke-static {v1}, Lcom/twitter/library/provider/au;->c(I)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto :goto_0

    :cond_4
    invoke-static {v1}, Lcom/twitter/library/provider/au;->d(I)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_0

    :pswitch_2
    invoke-static {v1}, Lcom/twitter/library/provider/au;->i(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/16 v0, 0xa

    goto :goto_0

    :cond_5
    invoke-static {v1}, Lcom/twitter/library/provider/au;->h(I)Z

    move-result v0

    if-eqz v0, :cond_6

    const/16 v0, 0xb

    goto :goto_0

    :cond_6
    invoke-static {v1}, Lcom/twitter/library/provider/au;->q(I)Z

    move-result v0

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/twitter/android/uh;->h:I

    if-nez v0, :cond_7

    const/16 v0, 0x8

    goto :goto_0

    :cond_7
    const/16 v0, 0x1d

    iget v1, p0, Lcom/twitter/android/uh;->h:I

    if-ne v0, v1, :cond_8

    const/16 v0, 0xf

    goto :goto_0

    :cond_8
    const/16 v0, 0xe

    goto :goto_0

    :cond_9
    invoke-static {v1}, Lcom/twitter/library/provider/au;->r(I)Z

    move-result v0

    if-eqz v0, :cond_a

    const/16 v0, 0x9

    goto :goto_0

    :cond_a
    const/4 v0, 0x7

    goto :goto_0

    :pswitch_3
    const/16 v0, 0xd

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public getViewTypeCount()I
    .locals 1

    const/16 v0, 0x11

    return v0
.end method

.method public isEnabled(I)Z
    .locals 2

    invoke-virtual {p0, p1}, Lcom/twitter/android/uh;->getItemViewType(I)I

    move-result v0

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/16 v1, 0x10

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/ic;->isEnabled(I)Z

    move-result v0

    goto :goto_0
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    const/4 v3, 0x0

    invoke-interface {p2}, Landroid/database/Cursor;->getPosition()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/uh;->getItemViewType(I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/ic;->newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/uh;->u:Landroid/view/LayoutInflater;

    const v1, 0x7f030085    # com.twitter.android.R.layout.grouped_convo_header_row_view

    invoke-virtual {v0, v1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/uj;

    invoke-direct {v1, v0}, Lcom/twitter/android/uj;-><init>(Landroid/view/View;)V

    new-instance v2, Lcom/twitter/android/ur;

    invoke-direct {v2, v1, v3}, Lcom/twitter/android/ur;-><init>(Lcom/twitter/android/uj;Lcom/twitter/library/api/TimelineScribeContent;)V

    invoke-virtual {v0, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/twitter/android/uh;->b()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    const v0, 0x7f03006f    # com.twitter.android.R.layout.explore_header_row_view

    invoke-direct {p0, v0}, Lcom/twitter/android/uh;->i(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/twitter/android/uh;->l()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    invoke-direct {p0}, Lcom/twitter/android/uh;->l()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    invoke-direct {p0, p3}, Lcom/twitter/android/uh;->a(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    const v0, 0x7f030088    # com.twitter.android.R.layout.grouped_event_row_view

    invoke-direct {p0, v0}, Lcom/twitter/android/uh;->g(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    const v0, 0x7f030094    # com.twitter.android.R.layout.grouped_sports_event_row_view

    invoke-direct {p0, v0}, Lcom/twitter/android/uh;->g(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    const v0, 0x7f03008d    # com.twitter.android.R.layout.grouped_local_event_row_view

    invoke-direct {p0, v0}, Lcom/twitter/android/uh;->h(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    invoke-direct {p0, p3}, Lcom/twitter/android/uh;->b(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_a
    invoke-direct {p0}, Lcom/twitter/android/uh;->k()Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_b
    const v0, 0x7f03008f    # com.twitter.android.R.layout.grouped_more_row_view

    invoke-direct {p0, v0}, Lcom/twitter/android/uh;->i(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_c
    invoke-direct {p0, p1}, Lcom/twitter/android/uh;->a(Landroid/content/Context;)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    :pswitch_d
    const v0, 0x7f030091    # com.twitter.android.R.layout.grouped_nearby_event_row_view

    invoke-direct {p0, v0}, Lcom/twitter/android/uh;->h(I)Landroid/view/View;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_1
        :pswitch_6
        :pswitch_8
        :pswitch_7
        :pswitch_5
        :pswitch_a
        :pswitch_b
        :pswitch_9
        :pswitch_c
        :pswitch_d
        :pswitch_2
    .end packed-switch
.end method
