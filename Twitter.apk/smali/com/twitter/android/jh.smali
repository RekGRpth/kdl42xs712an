.class Lcom/twitter/android/jh;
.super Lcom/twitter/library/network/ab;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/LoginChallengeActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/LoginChallengeActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-direct {p0}, Lcom/twitter/library/network/ab;-><init>()V

    return-void
.end method


# virtual methods
.method protected a(Landroid/webkit/WebView;Ljava/lang/String;Landroid/net/Uri;)Z
    .locals 9

    const/4 v8, 0x0

    const/4 v7, 0x1

    invoke-virtual {p3}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "over_limit"

    invoke-virtual {p3, v1}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string/jumbo v2, "success"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->c(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v6, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    iget-object v1, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginChallengeActivity;->d(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v2}, Lcom/twitter/android/LoginChallengeActivity;->e(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->a:J

    iget-object v4, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v4}, Lcom/twitter/android/LoginChallengeActivity;->e(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/network/LoginVerificationRequiredResponse;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/library/network/LoginVerificationRequiredResponse;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v5}, Lcom/twitter/android/LoginChallengeActivity;->f(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/jj;

    move-result-object v5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;Lcom/twitter/library/client/ac;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v6, Lcom/twitter/android/LoginChallengeActivity;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/LoginChallengeActivity;->a(Lcom/twitter/android/LoginChallengeActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1, p2, v8}, Lcom/twitter/library/util/Util;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/util/Map;)V

    :goto_0
    return v7

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->h(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginChallengeActivity;->g(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v7, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login_challenge::::limit_exceeded"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/LoginChallengeActivity;->a(Lcom/twitter/android/LoginChallengeActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1, p2, v8}, Lcom/twitter/library/util/Util;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/LoginChallengeActivity;->a(Lcom/twitter/android/LoginChallengeActivity;Ljava/lang/String;)Ljava/lang/String;

    invoke-static {p1, p2, v8}, Lcom/twitter/library/util/Util;->a(Landroid/webkit/WebView;Ljava/lang/String;Ljava/util/Map;)V

    goto :goto_0
.end method

.method public onReceivedError(Landroid/webkit/WebView;ILjava/lang/String;Ljava/lang/String;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v0}, Lcom/twitter/android/LoginChallengeActivity;->b(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/jh;->a:Lcom/twitter/android/LoginChallengeActivity;

    invoke-static {v1}, Lcom/twitter/android/LoginChallengeActivity;->a(Lcom/twitter/android/LoginChallengeActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login_challenge:webview:::failure"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method
