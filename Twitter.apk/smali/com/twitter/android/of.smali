.class Lcom/twitter/android/of;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/OneFactorLoginActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/OneFactorLoginActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/od;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/of;-><init>(Lcom/twitter/android/OneFactorLoginActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 12

    iget-object v0, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->c(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->b:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    if-eq v0, v1, :cond_1

    :cond_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "pdus"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    move-object v8, v0

    check-cast v8, [Ljava/lang/Object;

    array-length v10, v8

    const/4 v0, 0x0

    move v9, v0

    :goto_0
    if-ge v9, v10, :cond_0

    aget-object v0, v8, v9

    check-cast v0, [B

    check-cast v0, [B

    invoke-static {v0}, Landroid/telephony/SmsMessage;->createFromPdu([B)Landroid/telephony/SmsMessage;

    move-result-object v0

    invoke-virtual {v0}, Landroid/telephony/SmsMessage;->getMessageBody()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/util/x;->y:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->groupCount()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    iget-object v0, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->d(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "login::1fa:intercept:success"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->f(Lcom/twitter/android/OneFactorLoginActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/OneFactorLoginActivity;->e(Lcom/twitter/android/OneFactorLoginActivity;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v11, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    iget-object v0, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v0}, Lcom/twitter/android/OneFactorLoginActivity;->j(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v2}, Lcom/twitter/android/OneFactorLoginActivity;->g(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/network/OneFactorLoginResponse;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/network/OneFactorLoginResponse;->b:J

    iget-object v4, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v4}, Lcom/twitter/android/OneFactorLoginActivity;->h(Lcom/twitter/android/OneFactorLoginActivity;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v5}, Lcom/twitter/android/OneFactorLoginActivity;->g(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/library/network/OneFactorLoginResponse;

    move-result-object v5

    iget-object v5, v5, Lcom/twitter/library/network/OneFactorLoginResponse;->a:Ljava/lang/String;

    iget-object v7, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    invoke-static {v7}, Lcom/twitter/android/OneFactorLoginActivity;->i(Lcom/twitter/android/OneFactorLoginActivity;)Lcom/twitter/android/oe;

    move-result-object v7

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/Session;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/client/ag;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v11, v0}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/of;->a:Lcom/twitter/android/OneFactorLoginActivity;

    sget-object v1, Lcom/twitter/android/OneFactorLoginActivity$LoginState;->e:Lcom/twitter/android/OneFactorLoginActivity$LoginState;

    invoke-static {v0, v1}, Lcom/twitter/android/OneFactorLoginActivity;->a(Lcom/twitter/android/OneFactorLoginActivity;Lcom/twitter/android/OneFactorLoginActivity$LoginState;)V

    :cond_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto/16 :goto_0
.end method
