.class public Lcom/twitter/android/tg;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;
    .locals 1

    const-string/jumbo v0, "password"

    invoke-static {p0, v0}, Lcom/twitter/android/tg;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/twitter/library/api/al;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/twitter/library/api/al;
    .locals 3

    if-eqz p0, :cond_1

    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget-object v2, v0, Lcom/twitter/library/api/al;->d:Ljava/lang/String;

    invoke-virtual {p1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static b(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;
    .locals 1

    const-string/jumbo v0, "email"

    invoke-static {p0, v0}, Lcom/twitter/android/tg;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/twitter/library/api/al;

    move-result-object v0

    return-object v0
.end method

.method public static c(Ljava/util/ArrayList;)Lcom/twitter/library/api/al;
    .locals 1

    const-string/jumbo v0, "screen_name"

    invoke-static {p0, v0}, Lcom/twitter/android/tg;->a(Ljava/util/ArrayList;Ljava/lang/String;)Lcom/twitter/library/api/al;

    move-result-object v0

    return-object v0
.end method
