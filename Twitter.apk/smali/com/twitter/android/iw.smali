.class Lcom/twitter/android/iw;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/ListTabActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/ListTabActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/ListTabActivity;->a(Lcom/twitter/android/ListTabActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    const/4 v1, 0x1

    iput v1, v0, Lcom/twitter/android/ListTabActivity;->f:I

    const v0, 0x7f0f021f    # com.twitter.android.R.string.lists_subscribing

    iget-object v1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v1}, Lcom/twitter/android/ListTabActivity;->c(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "list::::subscribe"

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v1}, Lcom/twitter/android/ListTabActivity;->V()V

    iget-object v1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0f0211    # com.twitter.android.R.string.lists_add_subscriber_error

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JJLjava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/ListTabActivity;->c(Lcom/twitter/android/ListTabActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/android/ListTabActivity;->a(Lcom/twitter/android/ListTabActivity;Z)V

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    iput-wide p5, v0, Lcom/twitter/android/ListTabActivity;->c:J

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    iput-wide p7, v0, Lcom/twitter/android/ListTabActivity;->b:J

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ListTabActivity;->c()V

    if-eqz p9, :cond_0

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0, p9}, Lcom/twitter/android/ListTabActivity;->setTitle(Ljava/lang/CharSequence;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v0, 0x194

    if-ne p3, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    const v1, 0x7f0f020c    # com.twitter.android.R.string.list_not_found

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ListTabActivity;->finish()V

    goto :goto_0
.end method

.method public b(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 7

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/ListTabActivity;->b(Lcom/twitter/android/ListTabActivity;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0xc8

    if-ne p3, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    const/4 v1, 0x2

    iput v1, v0, Lcom/twitter/android/ListTabActivity;->f:I

    const v0, 0x7f0f0221    # com.twitter.android.R.string.lists_unsubscribing

    iget-object v1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v1}, Lcom/twitter/android/ListTabActivity;->d(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "list::::unsubscribe"

    aput-object v6, v4, v5

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v1}, Lcom/twitter/android/ListTabActivity;->V()V

    iget-object v1, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void

    :cond_1
    const v0, 0x7f0f021e    # com.twitter.android.R.string.lists_remove_subscriber_error

    goto :goto_0
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;IJJ)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    iget-wide v0, v0, Lcom/twitter/android/ListTabActivity;->b:J

    cmp-long v0, v0, p6

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-static {v0}, Lcom/twitter/android/ListTabActivity;->e(Lcom/twitter/android/ListTabActivity;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    cmp-long v0, p8, v0

    if-nez v0, :cond_1

    const/16 v0, 0xc8

    if-eq p3, v0, :cond_0

    const/16 v0, 0x194

    if-ne p3, v0, :cond_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/iw;->a:Lcom/twitter/android/ListTabActivity;

    invoke-virtual {v0}, Lcom/twitter/android/ListTabActivity;->b()V

    :cond_1
    return-void
.end method
