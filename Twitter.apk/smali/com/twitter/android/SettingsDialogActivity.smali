.class public Lcom/twitter/android/SettingsDialogActivity;
.super Lcom/twitter/android/client/BaseListActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListActivity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f030138    # com.twitter.android.R.layout.settings

    invoke-virtual {p0, v0}, Lcom/twitter/android/SettingsDialogActivity;->setContentView(I)V

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const v2, 0x7f0f03c6    # com.twitter.android.R.string.settings

    invoke-virtual {p0, v2}, Lcom/twitter/android/SettingsDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const v2, 0x7f0f048e    # com.twitter.android.R.string.support_account_title

    invoke-virtual {p0, v2}, Lcom/twitter/android/SettingsDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0}, Lcom/twitter/android/SettingsDialogActivity;->getListView()Landroid/widget/ListView;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/tf;

    invoke-direct {v2, p0, p0, v0}, Lcom/twitter/android/tf;-><init>(Lcom/twitter/android/SettingsDialogActivity;Landroid/content/Context;[Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method protected onListItemClick(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 2

    packed-switch p3, :pswitch_data_0

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/SettingsDialogActivity;->finish()V

    return-void

    :pswitch_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SettingsDialogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_1
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/WebViewActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const v1, 0x7f0f002d    # com.twitter.android.R.string.android_support_url

    invoke-virtual {p0, v1}, Lcom/twitter/android/SettingsDialogActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/SettingsDialogActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
