.class Lcom/twitter/android/kq;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/client/ai;


# instance fields
.field final synthetic a:Lcom/twitter/android/MainActivity;

.field private final b:Landroid/net/Uri;


# direct methods
.method public constructor <init>(Lcom/twitter/android/MainActivity;Landroid/net/Uri;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/kq;->b:Landroid/net/Uri;

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/client/BaseListFragment;)V
    .locals 7

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, v1, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kr;->a(I)Lhb;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kq;->b:Landroid/net/Uri;

    iget-object v2, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, v0, Lhb;->k:I

    if-lez v1, :cond_0

    sget-object v1, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/kq;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    new-instance v2, Ljp;

    iget-object v3, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    iget-object v4, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v4}, Lcom/twitter/android/MainActivity;->G(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/aa;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v6, v5}, Ljp;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;ILjava/util/Collection;)V

    invoke-static {v1, v2}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    iget-object v2, p0, Lcom/twitter/android/kq;->b:Landroid/net/Uri;

    invoke-virtual {v1, v2, v6, v6}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;IZ)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lhb;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/BaseListFragment;->getTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1, v6}, Lcom/twitter/internal/android/widget/DockLayout;->a(I)V

    const/4 v0, 0x1

    invoke-virtual {v1, v0}, Lcom/twitter/internal/android/widget/DockLayout;->setTopLocked(Z)V

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/android/client/BaseListFragment;Landroid/widget/AbsListView;I)V
    .locals 0

    return-void
.end method

.method public b(Lcom/twitter/android/client/BaseListFragment;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    iget-object v1, v1, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kr;->a(I)Lhb;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v1}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lhb;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/twitter/android/client/BaseListFragment;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/kq;->a:Lcom/twitter/android/MainActivity;

    invoke-static {v0}, Lcom/twitter/android/MainActivity;->z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/DockLayout;->setTopLocked(Z)V

    :cond_0
    return-void
.end method
