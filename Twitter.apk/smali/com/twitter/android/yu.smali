.class public Lcom/twitter/android/yu;
.super Lcom/twitter/android/az;
.source "Twttr"


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Lcom/twitter/android/client/c;

.field private final d:Lcom/twitter/library/util/FriendshipCache;

.field private final e:I

.field private final f:Lcom/twitter/android/zb;

.field private final g:Landroid/view/LayoutInflater;

.field private final h:Ljava/util/ArrayList;

.field private final i:Ljava/util/ArrayList;

.field private final j:Ljava/util/ArrayList;

.field private final k:Lcom/twitter/android/zc;

.field private final l:Z

.field private final m:Lcom/twitter/library/client/aa;

.field private n:Landroid/database/Cursor;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;ILcom/twitter/android/zc;Lcom/twitter/android/zb;)V
    .locals 7

    const/4 v6, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/yu;-><init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;ILcom/twitter/android/zc;Lcom/twitter/android/zb;Z)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/util/FriendshipCache;ILcom/twitter/android/zc;Lcom/twitter/android/zb;Z)V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/az;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/yu;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/yu;->i:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/twitter/android/yu;->b:Landroid/content/Context;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/yu;->c:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/yu;->m:Lcom/twitter/library/client/aa;

    iput-object p2, p0, Lcom/twitter/android/yu;->d:Lcom/twitter/library/util/FriendshipCache;

    iput p3, p0, Lcom/twitter/android/yu;->e:I

    iput-object p4, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    iput-object p5, p0, Lcom/twitter/android/yu;->f:Lcom/twitter/android/zb;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/yu;->g:Landroid/view/LayoutInflater;

    iput-boolean p6, p0, Lcom/twitter/android/yu;->l:Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/yu;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/yu;->e:I

    return v0
.end method

.method static synthetic a(Landroid/view/View;F)V
    .locals 0

    invoke-static {p0, p1}, Lcom/twitter/android/yu;->b(Landroid/view/View;F)V

    return-void
.end method

.method private static b(Landroid/view/View;F)V
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-ge v0, v1, :cond_0

    new-instance v0, Landroid/view/animation/AlphaAnimation;

    invoke-direct {v0, p1, p1}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    const-wide/16 v1, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    invoke-virtual {p0, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p1}, Landroid/view/View;->setAlpha(F)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/database/Cursor;)Landroid/database/Cursor;
    .locals 8

    iget-object v0, p0, Lcom/twitter/android/yu;->n:Landroid/database/Cursor;

    if-eq v0, p1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/yu;->a:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    iget-object v1, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    iput-object p1, p0, Lcom/twitter/android/yu;->n:Landroid/database/Cursor;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    if-eqz p1, :cond_2

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    invoke-interface {p1}, Landroid/database/Cursor;->getPosition()I

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v4, p1}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/twitter/android/yu;->a:Ljava/util/List;

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v6, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-boolean v4, p0, Lcom/twitter/android/yu;->l:Z

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v4, p1}, Lcom/twitter/android/zc;->l(Landroid/database/Cursor;)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x4

    if-ge v4, v5, :cond_4

    new-instance v4, Lcom/twitter/android/yz;

    iget-object v5, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v5, p1}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;)J

    move-result-wide v5

    iget-object v7, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v7, p1}, Lcom/twitter/android/zc;->h(Landroid/database/Cursor;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v7

    invoke-direct {v4, v3, v5, v6, v7}, Lcom/twitter/android/yz;-><init>(IJLcom/twitter/library/api/PromotedContent;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    iget-boolean v3, p0, Lcom/twitter/android/yu;->l:Z

    if-eqz v3, :cond_2

    new-instance v3, Lcom/twitter/android/yz;

    invoke-direct {v3, v2}, Lcom/twitter/android/yz;-><init>(Ljava/util/ArrayList;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/yu;->notifyDataSetChanged()V

    :cond_3
    return-object v0

    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x5

    if-ge v4, v5, :cond_1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    new-instance v4, Lcom/twitter/android/yz;

    iget-object v5, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v5, p1}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;)J

    move-result-wide v5

    iget-object v7, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v7, p1}, Lcom/twitter/android/zc;->h(Landroid/database/Cursor;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v7

    invoke-direct {v4, v3, v5, v6, v7}, Lcom/twitter/android/yz;-><init>(IJLcom/twitter/library/api/PromotedContent;)V

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public a(Landroid/view/ViewGroup;Lcom/twitter/android/yz;)Ljava/lang/Object;
    .locals 15

    iget-object v2, p0, Lcom/twitter/android/yu;->c:Lcom/twitter/android/client/c;

    iget-object v6, p0, Lcom/twitter/android/yu;->b:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    iget-object v3, p0, Lcom/twitter/android/yu;->f:Lcom/twitter/android/zb;

    iget-object v1, p0, Lcom/twitter/android/yu;->g:Landroid/view/LayoutInflater;

    const v4, 0x7f030171    # com.twitter.android.R.layout.user_gallery_top_page

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v14, v1

    check-cast v14, Landroid/widget/RelativeLayout;

    iget-object v11, p0, Lcom/twitter/android/yu;->n:Landroid/database/Cursor;

    move-object/from16 v0, p2

    iget v1, v0, Lcom/twitter/android/yz;->b:I

    invoke-interface {v11, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    iget-object v1, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v1, v11}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;)J

    move-result-wide v4

    new-instance v8, Lcom/twitter/android/za;

    invoke-direct {v8, v6, v14, v4, v5}, Lcom/twitter/android/za;-><init>(Landroid/content/Context;Landroid/widget/RelativeLayout;J)V

    iget-object v1, p0, Lcom/twitter/android/yu;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v14, v8}, Landroid/widget/RelativeLayout;->setTag(Ljava/lang/Object;)V

    iget-object v1, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v1, v11}, Lcom/twitter/android/zc;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    const v6, 0x7f0c00af    # com.twitter.android.R.dimen.profile_avatar_size

    invoke-virtual {v9, v6}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    invoke-static {v1, v6}, Lcom/twitter/library/util/Util;->a(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    new-instance v7, Lcom/twitter/library/util/m;

    invoke-direct {v7, v1, v6, v6}, Lcom/twitter/library/util/m;-><init>(Ljava/lang/String;II)V

    iput-object v7, v8, Lcom/twitter/android/za;->a:Lcom/twitter/library/util/m;

    iget-object v1, v8, Lcom/twitter/android/za;->j:Landroid/widget/ImageView;

    iget-object v6, v8, Lcom/twitter/android/za;->a:Lcom/twitter/library/util/m;

    invoke-virtual {v2, v6}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/util/m;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v1, v11}, Lcom/twitter/android/zc;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v11}, Lcom/twitter/android/zc;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v1, v2}, Lcom/twitter/android/za;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, v8, Lcom/twitter/android/za;->l:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v6, v11}, Lcom/twitter/android/zc;->e(Landroid/database/Cursor;)Ljava/lang/CharSequence;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v11}, Lcom/twitter/android/zc;->g(Landroid/database/Cursor;)Z

    move-result v2

    invoke-virtual {v8, v2}, Lcom/twitter/android/za;->a(Z)V

    iget-object v2, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v11}, Lcom/twitter/android/zc;->f(Landroid/database/Cursor;)Z

    move-result v2

    invoke-virtual {v8, v2}, Lcom/twitter/android/za;->b(Z)V

    iget-object v2, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v11}, Lcom/twitter/android/zc;->h(Landroid/database/Cursor;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v6

    invoke-virtual {v8, v6}, Lcom/twitter/android/za;->a(Lcom/twitter/library/api/PromotedContent;)V

    iget-object v2, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    move-object/from16 v0, p2

    iget v7, v0, Lcom/twitter/android/yz;->b:I

    invoke-interface {v2, v11, v7}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v2

    new-instance v7, Lcom/twitter/android/zd;

    invoke-direct {v7, v1, v2}, Lcom/twitter/android/zd;-><init>(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItem;)V

    iget-object v1, p0, Lcom/twitter/android/yu;->m:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    cmp-long v1, v4, v1

    if-nez v1, :cond_2

    iget-object v1, v8, Lcom/twitter/android/za;->d:Lcom/twitter/library/widget/ActionButton;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    :goto_0
    invoke-virtual {v8}, Lcom/twitter/android/za;->a()V

    if-eqz v3, :cond_1

    new-instance v1, Lcom/twitter/android/yw;

    move-object v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/yw;-><init>(Lcom/twitter/android/yu;Lcom/twitter/android/zb;JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zd;)V

    invoke-virtual {v14, v1}, Landroid/widget/RelativeLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v14

    :cond_2
    iget-object v1, v8, Lcom/twitter/android/za;->d:Lcom/twitter/library/widget/ActionButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/twitter/library/widget/ActionButton;->setVisibility(I)V

    iget-object v1, p0, Lcom/twitter/android/yu;->d:Lcom/twitter/library/util/FriendshipCache;

    iget-object v2, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v11}, Lcom/twitter/android/zc;->i(Landroid/database/Cursor;)I

    move-result v2

    if-eqz v1, :cond_3

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->a(J)Z

    move-result v10

    if-eqz v10, :cond_5

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/util/FriendshipCache;->h(J)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v8, v1, v9}, Lcom/twitter/android/za;->a(ILandroid/content/res/Resources;)V

    :cond_3
    :goto_1
    iget v1, v8, Lcom/twitter/android/za;->b:I

    iput v1, v7, Lcom/twitter/android/zd;->b:I

    if-eqz v3, :cond_4

    iget-object v10, v8, Lcom/twitter/android/za;->d:Lcom/twitter/library/widget/ActionButton;

    new-instance v1, Lcom/twitter/android/yv;

    move-object v2, p0

    invoke-direct/range {v1 .. v9}, Lcom/twitter/android/yv;-><init>(Lcom/twitter/android/yu;Lcom/twitter/android/zb;JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zd;Lcom/twitter/android/za;Landroid/content/res/Resources;)V

    invoke-virtual {v10, v1}, Lcom/twitter/library/widget/ActionButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    iget v1, v8, Lcom/twitter/android/za;->b:I

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v1

    if-eqz v1, :cond_6

    const v1, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    iget-object v2, p0, Lcom/twitter/android/yu;->c:Lcom/twitter/android/client/c;

    iget-boolean v2, v2, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual {v8, v1, v2}, Lcom/twitter/android/za;->a(IZ)V

    goto :goto_0

    :cond_5
    invoke-virtual {v8, v2, v9}, Lcom/twitter/android/za;->a(ILandroid/content/res/Resources;)V

    goto :goto_1

    :cond_6
    iget-object v1, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v1, v11}, Lcom/twitter/android/zc;->k(Landroid/database/Cursor;)I

    move-result v9

    const v10, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    iget-object v1, p0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v1, v11}, Lcom/twitter/android/zc;->j(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    iget-object v1, p0, Lcom/twitter/android/yu;->c:Lcom/twitter/android/client/c;

    iget-boolean v13, v1, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual/range {v8 .. v13}, Lcom/twitter/android/za;->a(IILjava/lang/String;IZ)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 4

    iget v0, p1, Lcom/twitter/library/util/ao;->g:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/yu;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public a(Ljava/util/HashMap;)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/yu;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/za;

    iget-object v1, v0, Lcom/twitter/android/za;->a:Lcom/twitter/library/util/m;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/za;->a:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/twitter/android/za;->a:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/twitter/android/za;->j:Landroid/widget/ImageView;

    iget-object v0, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    :cond_1
    return-void
.end method

.method public b(I)Lcom/twitter/android/yz;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yz;

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(Landroid/view/ViewGroup;Lcom/twitter/android/yz;)Ljava/lang/Object;
    .locals 17

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/twitter/android/yu;->c:Lcom/twitter/android/client/c;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/twitter/android/yu;->b:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/twitter/android/yu;->g:Landroid/view/LayoutInflater;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/yu;->f:Lcom/twitter/android/zb;

    const v1, 0x7f030170    # com.twitter.android.R.layout.user_gallery_more_page

    const/4 v2, 0x0

    invoke-virtual {v13, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    move-object v8, v1

    check-cast v8, Landroid/widget/LinearLayout;

    if-eqz v3, :cond_0

    const v1, 0x7f0902b8    # com.twitter.android.R.id.view_more

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/yx;

    move-object/from16 v0, p0

    invoke-direct {v2, v0, v3}, Lcom/twitter/android/yx;-><init>(Lcom/twitter/android/yu;Lcom/twitter/android/zb;)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v1, 0x7f0902b6    # com.twitter.android.R.id.face_pile

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    move-object v9, v1

    check-cast v9, Landroid/widget/LinearLayout;

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setTag(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/twitter/android/yu;->n:Landroid/database/Cursor;

    new-instance v15, Ljava/util/ArrayList;

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/twitter/android/yz;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {v15, v1}, Ljava/util/ArrayList;-><init>(I)V

    move-object/from16 v0, p2

    iget-object v1, v0, Lcom/twitter/android/yz;->c:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :cond_1
    :goto_0
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-interface {v14, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v14}, Lcom/twitter/android/zc;->c(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v15, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v14}, Lcom/twitter/android/zc;->d(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v14}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;)J

    move-result-wide v4

    const v2, 0x7f03016f    # com.twitter.android.R.layout.user_gallery_face_pile_avatar

    const/4 v10, 0x0

    invoke-virtual {v13, v2, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    move-object v10, v2

    check-cast v10, Landroid/widget/ImageView;

    invoke-virtual {v10, v7}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    invoke-virtual {v11, v7}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    invoke-virtual {v10, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    invoke-virtual {v10, v6}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/yu;->h:Ljava/util/ArrayList;

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v2, v14}, Lcom/twitter/android/zc;->b(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-interface {v6, v14}, Lcom/twitter/android/zc;->h(Landroid/database/Cursor;)Lcom/twitter/library/api/PromotedContent;

    move-result-object v6

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/twitter/android/yu;->k:Lcom/twitter/android/zc;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v7, v14, v1}, Lcom/twitter/android/zc;->a(Landroid/database/Cursor;I)Lcom/twitter/library/scribe/ScribeItem;

    move-result-object v1

    new-instance v7, Lcom/twitter/android/zd;

    invoke-direct {v7, v2, v1}, Lcom/twitter/android/zd;-><init>(Ljava/lang/String;Lcom/twitter/library/scribe/ScribeItem;)V

    if-eqz v3, :cond_2

    new-instance v1, Lcom/twitter/android/yy;

    move-object/from16 v2, p0

    invoke-direct/range {v1 .. v7}, Lcom/twitter/android/yy;-><init>(Lcom/twitter/android/yu;Lcom/twitter/android/zb;JLcom/twitter/library/api/PromotedContent;Lcom/twitter/android/zd;)V

    invoke-virtual {v10, v1}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    invoke-virtual {v9, v10}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v3

    const/4 v1, 0x1

    if-le v3, v1, :cond_4

    const/4 v1, 0x1

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    move-object v2, v1

    :goto_1
    const v1, 0x7f0902b7    # com.twitter.android.R.id.name_pile

    invoke-virtual {v8, v1}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v4, 0x7f0e0013    # com.twitter.android.R.plurals.search_user_name_pile

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v15, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    invoke-virtual {v12, v4, v3, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move-object/from16 v0, p1

    invoke-virtual {v0, v8}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    return-object v8

    :cond_4
    const/4 v1, 0x0

    move-object v2, v1

    goto :goto_1
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 5

    check-cast p3, Landroid/view/View;

    iget-object v0, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yz;

    iget v0, v0, Lcom/twitter/android/yz;->a:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/yu;->i:Ljava/util/ArrayList;

    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    goto :goto_0

    :pswitch_1
    invoke-virtual {p3}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getChildCount()I

    move-result v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v2, :cond_0

    iget-object v3, p0, Lcom/twitter/android/yu;->h:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/yu;->j:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/yz;

    iget v1, v0, Lcom/twitter/android/yz;->a:I

    packed-switch v1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :pswitch_0
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/yu;->a(Landroid/view/ViewGroup;Lcom/twitter/android/yz;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0, p1, v0}, Lcom/twitter/android/yu;->b(Landroid/view/ViewGroup;Lcom/twitter/android/yz;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
