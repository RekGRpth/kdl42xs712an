.class Lcom/twitter/android/nj;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/platform/i;


# instance fields
.field final synthetic a:Lcom/twitter/android/NearbyFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/NearbyFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/nj;->a:Lcom/twitter/android/NearbyFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/location/Location;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/nj;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->c(Lcom/twitter/android/NearbyFragment;)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/nj;->a:Lcom/twitter/android/NearbyFragment;

    iget-object v1, p0, Lcom/twitter/android/nj;->a:Lcom/twitter/android/NearbyFragment;

    const v2, 0x7f0201dc    # com.twitter.android.R.drawable.ic_nearby_map_user_location

    invoke-static {v2}, Lcom/google/android/gms/maps/model/b;->a(I)Lcom/google/android/gms/maps/model/a;

    move-result-object v2

    const/4 v3, 0x1

    invoke-static {v1, p1, v2, v3}, Lcom/twitter/android/NearbyFragment;->a(Lcom/twitter/android/NearbyFragment;Landroid/location/Location;Lcom/google/android/gms/maps/model/a;Z)Lcom/google/android/gms/maps/model/k;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/NearbyFragment;->a(Lcom/twitter/android/NearbyFragment;Lcom/google/android/gms/maps/model/k;)Lcom/google/android/gms/maps/model/k;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/nj;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->d(Lcom/twitter/android/NearbyFragment;)Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/library/platform/LocationProducer;->b(Lcom/twitter/library/platform/i;)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/nj;->a:Lcom/twitter/android/NearbyFragment;

    invoke-static {v0}, Lcom/twitter/android/NearbyFragment;->c(Lcom/twitter/android/NearbyFragment;)Lcom/google/android/gms/maps/model/k;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/maps/model/LatLng;

    invoke-virtual {p1}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {p1}, Landroid/location/Location;->getLongitude()D

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/gms/maps/model/LatLng;-><init>(DD)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/maps/model/k;->a(Lcom/google/android/gms/maps/model/LatLng;)V

    goto :goto_0
.end method
