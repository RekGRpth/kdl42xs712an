.class Lcom/twitter/android/spen/b;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/spen/f;


# instance fields
.field final synthetic a:Lcom/twitter/android/spen/CanvasActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/spen/CanvasActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->b(Lcom/twitter/android/spen/CanvasActivity;)V

    return-void
.end method

.method public a(Z)V
    .locals 3

    const v2, 0x7f090304    # com.twitter.android.R.id.canvas_done

    iget-object v1, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/twitter/android/spen/CanvasActivity;->a(Lcom/twitter/android/spen/CanvasActivity;Z)Z

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->d(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->d(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    if-nez v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->d(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasActivity;->e(Lcom/twitter/android/spen/CanvasActivity;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhn;->c(Z)Lhn;

    goto :goto_1
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->c(Lcom/twitter/android/spen/CanvasActivity;)Landroid/view/View;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public c()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    const v1, 0x7f0f0222    # com.twitter.android.R.string.load_image_failure

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    const/16 v1, -0xb

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/CanvasActivity;->setResult(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/b;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasActivity;->finish()V

    return-void
.end method
