.class public Lcom/twitter/android/spen/d;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a(Landroid/content/Context;Lcom/twitter/android/PostStorage$MediaItem;)Landroid/content/Intent;
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/spen/CanvasActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    if-eqz p1, :cond_0

    iget-object v1, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    if-eqz v1, :cond_0

    const-string/jumbo v1, "imageUri"

    iget-object v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->b:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    const-string/jumbo v1, "imageFilterId"

    iget v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->h:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "imageIsEnhanced"

    iget-boolean v2, p1, Lcom/twitter/android/PostStorage$MediaItem;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :cond_0
    return-object v0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->H()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/twitter/library/util/v;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p0}, Lcom/twitter/android/spen/d;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static b(Landroid/content/Context;)Z
    .locals 10

    const/4 v1, 0x1

    const/4 v3, 0x0

    const-string/jumbo v0, "unknown"

    :try_start_0
    new-instance v2, Lcom/samsung/android/sdk/pen/Spen;

    invoke-direct {v2}, Lcom/samsung/android/sdk/pen/Spen;-><init>()V

    invoke-virtual {v2, p0}, Lcom/samsung/android/sdk/pen/Spen;->initialize(Landroid/content/Context;)V
    :try_end_0
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    const-string/jumbo v0, "ok"
    :try_end_1
    .catch Lcom/samsung/android/sdk/SsdkUnsupportedException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    move-object v2, v0

    move v0, v1

    :goto_0
    invoke-static {p0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v4

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-virtual {v5}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/String;

    const-string/jumbo v8, ":composition:spen_canvas::initialize"

    aput-object v8, v7, v3

    aput-object v2, v7, v1

    invoke-virtual {v4, v5, v6, v7}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return v0

    :catch_0
    move-exception v2

    move-object v4, v2

    move v2, v3

    :goto_1
    invoke-virtual {v4}, Lcom/samsung/android/sdk/SsdkUnsupportedException;->getType()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    :goto_2
    move v9, v2

    move-object v2, v0

    move v0, v9

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "vendor_not_supported"

    goto :goto_2

    :pswitch_1
    const-string/jumbo v0, "device_not_supported"

    goto :goto_2

    :pswitch_2
    const-string/jumbo v0, "library_not_installed"

    goto :goto_2

    :pswitch_3
    const-string/jumbo v0, "library_update_is_required"

    goto :goto_2

    :pswitch_4
    const-string/jumbo v0, "library_update_is_recommended"

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-static {v2}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    :cond_0
    move-object v2, v0

    move v0, v3

    goto :goto_0

    :catch_2
    move-exception v2

    move-object v4, v2

    move v2, v1

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
