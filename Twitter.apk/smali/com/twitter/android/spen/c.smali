.class Lcom/twitter/android/spen/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/SeekBar$OnSeekBarChangeListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/spen/CanvasActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/spen/CanvasActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/spen/c;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onProgressChanged(Landroid/widget/SeekBar;IZ)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/c;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0, p2}, Lcom/twitter/android/spen/CanvasActivity;->a(Lcom/twitter/android/spen/CanvasActivity;I)V

    return-void
.end method

.method public onStartTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/spen/c;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v0}, Lcom/twitter/android/spen/CanvasActivity;->h(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/spen/c;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v1}, Lcom/twitter/android/spen/CanvasActivity;->f(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, ":composition:spen_canvas:pen_size:click"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/spen/c;->a:Lcom/twitter/android/spen/CanvasActivity;

    invoke-static {v5}, Lcom/twitter/android/spen/CanvasActivity;->g(Lcom/twitter/android/spen/CanvasActivity;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public onStopTrackingTouch(Landroid/widget/SeekBar;)V
    .locals 0

    return-void
.end method
