.class public Lcom/twitter/android/spen/CanvasActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# instance fields
.field private a:Landroid/widget/SeekBar;

.field private b:Lcom/twitter/android/spen/StrokeSizeView;

.field private c:Landroid/view/View;

.field private d:Landroid/view/View;

.field private e:Landroid/view/View;

.field private f:Landroid/view/View;

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:Lcom/twitter/android/spen/CanvasView;

.field private l:Landroid/net/Uri;

.field private m:I

.field private n:I

.field private o:Z

.field private p:Landroid/view/View;

.field private q:Lcom/twitter/internal/android/widget/ToolBar;

.field private r:Ljava/lang/String;

.field private s:F

.field private t:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    const/16 v0, 0xa

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    const v0, 0x7f0900aa    # com.twitter.android.R.id.canvas_button_black

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->n:I

    const-string/jumbo v0, "empty_canvas"

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/spen/CanvasView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    return-object v0
.end method

.method private a(I)V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->h()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iput p1, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->b:Lcom/twitter/android/spen/StrokeSizeView;

    invoke-virtual {v0, p1}, Lcom/twitter/android/spen/StrokeSizeView;->setStrokeSizePercentage(I)V

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->g()V

    return-void

    :cond_1
    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->h()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iput p1, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    goto :goto_0
.end method

.method private a(II)V
    .locals 5

    const v4, 0x7f0900a6    # com.twitter.android.R.id.pen_size_slider

    const/4 v1, 0x4

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->e:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->d:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    :goto_0
    const v0, 0x7f0900a4    # com.twitter.android.R.id.canvas_pen_button

    if-eq p1, v0, :cond_0

    const v0, 0x7f0900a5    # com.twitter.android.R.id.canvas_eraser_button

    if-eq p1, v0, :cond_0

    const v0, 0x7f0900b0    # com.twitter.android.R.id.canvas_clear_button

    if-ne p1, v0, :cond_2

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->e:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->f:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->c:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->d:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->n:I

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/view/View;->setSelected(Z)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/view/View;->setSelected(Z)V

    iput p1, p0, Lcom/twitter/android/spen/CanvasActivity;->n:I

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/spen/CanvasActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/spen/CanvasActivity;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/spen/CanvasActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/spen/CanvasActivity;->t:Z

    return p1
.end method

.method private b(I)I
    .locals 4

    mul-int/lit8 v0, p1, 0x1e

    int-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    add-int/lit8 v0, v0, 0x5

    return v0
.end method

.method static synthetic b(Lcom/twitter/android/spen/CanvasActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->k()V

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/spen/CanvasActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->p:Landroid/view/View;

    return-object v0
.end method

.method private c(Landroid/os/Bundle;)V
    .locals 5

    const/16 v2, 0xa

    const/4 v4, 0x0

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->f()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    if-eqz p1, :cond_2

    const-string/jumbo v0, "imageUri"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    :cond_0
    const-string/jumbo v0, "imageFilterId"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->m:I

    const-string/jumbo v0, "imageIsEnhanced"

    invoke-virtual {p1, v0, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasActivity;->o:Z

    const-string/jumbo v0, "penStrokeSize"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    const-string/jumbo v0, "penStrokeStyle"

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    const-string/jumbo v0, "eraserStrokeSize"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    const-string/jumbo v0, "penColor"

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b001a    # com.twitter.android.R.color.canvas_color_black

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    const-string/jumbo v0, "EXTRA_CANVAS_DATA"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/spen/CanvasView;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    iget v2, p0, Lcom/twitter/android/spen/CanvasActivity;->m:I

    iget-boolean v3, p0, Lcom/twitter/android/spen/CanvasActivity;->o:Z

    invoke-virtual {v0, p0, v1, v2, v3}, Lcom/twitter/android/spen/CanvasView;->a(Landroid/content/Context;Landroid/net/Uri;IZ)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->p:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    const-string/jumbo v0, "photo"

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/spen/a;

    invoke-direct {v1, p0}, Lcom/twitter/android/spen/a;-><init>(Lcom/twitter/android/spen/CanvasActivity;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "imageUri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "imageFilterId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->m:I

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "imageIsEnhanced"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasActivity;->o:Z

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->p:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic d(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/internal/android/widget/ToolBar;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->q:Lcom/twitter/internal/android/widget/ToolBar;

    return-object v0
.end method

.method static synthetic e(Lcom/twitter/android/spen/CanvasActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/spen/CanvasActivity;->t:Z

    return v0
.end method

.method static synthetic f(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private f()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    new-instance v1, Lcom/twitter/android/spen/b;

    invoke-direct {v1, p0}, Lcom/twitter/android/spen/b;-><init>(Lcom/twitter/android/spen/CanvasActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/CanvasView;->setListener(Lcom/twitter/android/spen/f;)V

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/spen/CanvasActivity;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    return-object v0
.end method

.method private g()V
    .locals 5

    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001f    # com.twitter.android.R.color.canvas_color_transparent

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    :goto_0
    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    invoke-direct {p0, v1}, Lcom/twitter/android/spen/CanvasActivity;->b(I)I

    move-result v1

    int-to-float v1, v1

    iget v2, p0, Lcom/twitter/android/spen/CanvasActivity;->s:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    iget v2, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    invoke-direct {p0, v2}, Lcom/twitter/android/spen/CanvasActivity;->b(I)I

    move-result v2

    int-to-float v2, v2

    iget v3, p0, Lcom/twitter/android/spen/CanvasActivity;->s:F

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iget-object v3, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    iget v4, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    invoke-virtual {v3, v4, v1, v2, v0}, Lcom/twitter/android/spen/CanvasView;->a(IIII)V

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->b:Lcom/twitter/android/spen/StrokeSizeView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/spen/StrokeSizeView;->setStrokeColor(I)V

    return-void

    :cond_0
    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto :goto_0
.end method

.method private h()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->e:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->f:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x3

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic h(Lcom/twitter/android/spen/CanvasActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method private k()V
    .locals 3

    const v0, 0x7f0900a6    # com.twitter.android.R.id.pen_size_slider

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    const v0, 0x7f0900a7    # com.twitter.android.R.id.stroke_size_view

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/spen/StrokeSizeView;

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->b:Lcom/twitter/android/spen/StrokeSizeView;

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    new-instance v1, Lcom/twitter/android/spen/c;

    invoke-direct {v1, p0}, Lcom/twitter/android/spen/c;-><init>(Lcom/twitter/android/spen/CanvasActivity;)V

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->h()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->a:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setProgress(I)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->a(I)V

    :cond_1
    return-void

    :cond_2
    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->h()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private l()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-static {p0, v1, v3, v4}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;ZJ)Ljava/io/File;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v4, v3}, Lcom/twitter/android/spen/CanvasView;->a(Ljava/io/File;)Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    invoke-static {v1}, Lcom/twitter/library/util/n;->b(Landroid/net/Uri;)Z

    const-string/jumbo v1, "tempImageFile"

    invoke-virtual {v2, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    const/4 v1, -0x1

    invoke-virtual {p0, v1, v2}, Lcom/twitter/android/spen/CanvasActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->finish()V

    :goto_0
    return v0

    :cond_0
    const v3, 0x7f0f0386    # com.twitter.android.R.string.save_image_failure

    invoke-static {p0, v3, v0}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    const/16 v0, -0xa

    invoke-virtual {p0, v0, v2}, Lcom/twitter/android/spen/CanvasActivity;->setResult(ILandroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->finish()V

    move v0, v1

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const v1, 0x7f03001a    # com.twitter.android.R.layout.canvas_activity_layout

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->d(I)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 2

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f0f0055    # com.twitter.android.R.string.button_action_canvas

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->setTitle(I)V

    new-instance v0, Landroid/util/DisplayMetrics;

    invoke-direct {v0}, Landroid/util/DisplayMetrics;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    int-to-float v0, v0

    const/high16 v1, 0x44a00000    # 1280.0f

    div-float/2addr v0, v1

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->s:F

    const v0, 0x7f0900a8    # com.twitter.android.R.id.colors_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->c:Landroid/view/View;

    const v0, 0x7f0900a4    # com.twitter.android.R.id.canvas_pen_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->e:Landroid/view/View;

    const v0, 0x7f0900a5    # com.twitter.android.R.id.canvas_eraser_button

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->f:Landroid/view/View;

    const v0, 0x7f0900af    # com.twitter.android.R.id.clear_button_container

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->d:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001a    # com.twitter.android.R.color.canvas_color_black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    const v0, 0x7f0900a3    # com.twitter.android.R.id.canvas_spinner

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->p:Landroid/view/View;

    const v0, 0x7f0900a2    # com.twitter.android.R.id.canvas_view

    invoke-virtual {p0, v0}, Lcom/twitter/android/spen/CanvasActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/spen/CanvasView;

    iput-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-direct {p0, p1}, Lcom/twitter/android/spen/CanvasActivity;->c(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "saved_history"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/spen/CanvasActivity;->t:Z

    :cond_0
    iget v0, p0, Lcom/twitter/android/spen/CanvasActivity;->n:I

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/spen/CanvasActivity;->a(II)V

    return-void
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    const v0, 0x7f090304    # com.twitter.android.R.id.canvas_done

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/spen/CanvasActivity;->t:Z

    invoke-virtual {v0, v1}, Lhn;->c(Z)Lhn;

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 1

    const v0, 0x7f110004    # com.twitter.android.R.menu.canvas_menu

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    iput-object p2, p0, Lcom/twitter/android/spen/CanvasActivity;->q:Lcom/twitter/internal/android/widget/ToolBar;

    const/4 v0, 0x1

    return v0
.end method

.method public a(Lhn;)Z
    .locals 7

    const/4 v4, 0x2

    const/4 v6, 0x0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v2

    const v3, 0x7f090304    # com.twitter.android.R.id.canvas_done

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, ":composition:spen_canvas:done:click"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->l()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const v3, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v2, v3, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v4, v4, [Ljava/lang/String;

    const-string/jumbo v5, ":composition:spen_canvas:cancel:click"

    aput-object v5, v4, v6

    iget-object v5, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v5, v4, v0

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->finish()V

    goto :goto_0

    :cond_1
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto :goto_0
.end method

.method public onBackPressed()V
    .locals 6

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->d()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, ":composition:spen_canvas:cancel:click"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    return-void
.end method

.method public final onClickHandler(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x2

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iput v3, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0900a5    # com.twitter.android.R.id.canvas_eraser_button

    if-ne v1, v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:eraser:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/4 v0, 0x3

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    :cond_0
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/spen/CanvasActivity;->a(II)V

    invoke-direct {p0}, Lcom/twitter/android/spen/CanvasActivity;->g()V

    return-void

    :cond_1
    const v2, 0x7f0900a4    # com.twitter.android.R.id.canvas_pen_button

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:pen:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const v2, 0x7f0900a9    # com.twitter.android.R.id.canvas_button_cyan

    if-ne v1, v2, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:change_color:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001c    # com.twitter.android.R.color.canvas_color_cyan

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto :goto_0

    :cond_3
    const v2, 0x7f0900ad    # com.twitter.android.R.id.canvas_button_blue

    if-ne v1, v2, :cond_4

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:change_color:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001b    # com.twitter.android.R.color.canvas_color_blue

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto :goto_0

    :cond_4
    const v2, 0x7f0900ae    # com.twitter.android.R.id.canvas_button_green

    if-ne v1, v2, :cond_5

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:change_color:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001d    # com.twitter.android.R.color.canvas_color_green

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto/16 :goto_0

    :cond_5
    const v2, 0x7f0900ac    # com.twitter.android.R.id.canvas_button_red

    if-ne v1, v2, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:change_color:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001e    # com.twitter.android.R.color.canvas_color_red

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto/16 :goto_0

    :cond_6
    const v2, 0x7f0900ab    # com.twitter.android.R.id.canvas_button_yellow

    if-ne v1, v2, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:change_color:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b0020    # com.twitter.android.R.color.canvas_color_yellow

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto/16 :goto_0

    :cond_7
    const v2, 0x7f0900aa    # com.twitter.android.R.id.canvas_button_black

    if-ne v1, v2, :cond_8

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:change_color:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0b001a    # com.twitter.android.R.color.canvas_color_black

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    goto/16 :goto_0

    :cond_8
    const v2, 0x7f0900b0    # com.twitter.android.R.id.canvas_clear_button

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/spen/CanvasActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, ":composition:spen_canvas:clear_all:click"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/twitter/android/spen/CanvasActivity;->r:Ljava/lang/String;

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->c()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/spen/CanvasView;->setListener(Lcom/twitter/android/spen/f;)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->k:Lcom/twitter/android/spen/CanvasView;

    invoke-virtual {v0}, Lcom/twitter/android/spen/CanvasView;->b()V

    :cond_0
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "penStrokeSize"

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->g:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "penStrokeStyle"

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "eraserStrokeSize"

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->h:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "penColor"

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->j:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "saved_history"

    iget-boolean v1, p0, Lcom/twitter/android/spen/CanvasActivity;->t:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "imageUri"

    iget-object v1, p0, Lcom/twitter/android/spen/CanvasActivity;->l:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "imageFilterId"

    iget v1, p0, Lcom/twitter/android/spen/CanvasActivity;->m:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "imageIsEnhanced"

    iget-boolean v1, p0, Lcom/twitter/android/spen/CanvasActivity;->o:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_0
    return-void
.end method
