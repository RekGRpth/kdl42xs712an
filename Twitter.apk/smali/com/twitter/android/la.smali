.class Lcom/twitter/android/la;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/util/au;


# instance fields
.field final synthetic a:Lcom/twitter/library/util/at;

.field final synthetic b:Lcom/twitter/android/MediaPlayerActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MediaPlayerActivity;Lcom/twitter/library/util/at;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/la;->b:Lcom/twitter/android/MediaPlayerActivity;

    iput-object p2, p0, Lcom/twitter/android/la;->a:Lcom/twitter/library/util/at;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/as;Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/la;->a:Lcom/twitter/library/util/at;

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/an;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/an;->b()Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/la;->b:Lcom/twitter/android/MediaPlayerActivity;

    invoke-static {v1}, Lcom/twitter/android/MediaPlayerActivity;->a(Lcom/twitter/android/MediaPlayerActivity;)Lco/vine/android/player/SdkVideoView;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/lb;

    invoke-direct {v2, p0}, Lcom/twitter/android/lb;-><init>(Lcom/twitter/android/la;)V

    invoke-virtual {v1, v2}, Lco/vine/android/player/SdkVideoView;->post(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/twitter/android/la;->b:Lcom/twitter/android/MediaPlayerActivity;

    invoke-static {v1}, Lcom/twitter/android/MediaPlayerActivity;->a(Lcom/twitter/android/MediaPlayerActivity;)Lco/vine/android/player/SdkVideoView;

    move-result-object v1

    iget-object v0, v0, Lcom/twitter/library/util/an;->c:Ljava/lang/Object;

    check-cast v0, Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lco/vine/android/player/SdkVideoView;->setVideoPath(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/la;->b:Lcom/twitter/android/MediaPlayerActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MediaPlayerActivity;->f()V

    invoke-virtual {p1, p0}, Lcom/twitter/library/util/as;->b(Lcom/twitter/library/util/au;)V

    goto :goto_0
.end method
