.class Lcom/twitter/android/os;
.super Landroid/os/AsyncTask;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/ou;


# instance fields
.field final a:J

.field final b:Landroid/net/Uri;

.field final c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

.field final synthetic d:Lcom/twitter/android/PhotoSelectHelper;

.field private final e:Ljava/lang/ref/WeakReference;

.field private f:Ljava/lang/String;

.field private g:Landroid/net/Uri;

.field private h:Ljava/io/File;

.field private i:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/PhotoSelectHelper;JLandroid/net/Uri;Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;Lcom/twitter/android/AttachMediaListener;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p4, p0, Lcom/twitter/android/os;->b:Landroid/net/Uri;

    iput-wide p2, p0, Lcom/twitter/android/os;->a:J

    iput-object p5, p0, Lcom/twitter/android/os;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p6}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/os;->e:Ljava/lang/ref/WeakReference;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Integer;
    .locals 7

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/twitter/android/os;->b:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v4, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v4}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v4

    invoke-static {v4, v3}, Lcom/twitter/library/util/j;->b(Landroid/content/Context;Landroid/net/Uri;)Ljava/io/File;

    move-result-object v4

    iput-object v4, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    iget-object v4, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    if-eqz v4, :cond_3

    iget-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/os;->g:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/os;->f:Ljava/lang/String;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    if-nez v0, :cond_4

    invoke-static {v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "image/gif"

    iget-object v4, p0, Lcom/twitter/android/os;->f:Ljava/lang/String;

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v3}, Lcom/twitter/android/PhotoSelectHelper;->b(Landroid/net/Uri;)Z

    move-result v0

    if-nez v0, :cond_4

    :cond_0
    move v0, v1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v0

    iget-wide v4, p0, Lcom/twitter/android/os;->a:J

    invoke-static {v0, v3, v4, v5}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Landroid/net/Uri;J)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    iget-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    invoke-static {v0}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/os;->g:Landroid/net/Uri;

    iget-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/util/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/os;->f:Ljava/lang/String;

    :cond_1
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->as()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "image/gif"

    iget-object v3, p0, Lcom/twitter/android/os;->f:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/os;->h:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v3

    const-wide/32 v5, 0x300000

    cmp-long v0, v3, v5

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->c(Lcom/twitter/android/PhotoSelectHelper;)Ljava/util/EnumSet;

    move-result-object v0

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$MediaType;->b:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    invoke-virtual {v0, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/os;->g:Landroid/net/Uri;

    invoke-static {v0, v3}, Lcom/twitter/media/MediaUtils;->a(Landroid/content/Context;Landroid/net/Uri;)Lcom/twitter/media/ImageInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/media/ImageInfo;->isAnimated:Z

    iput-boolean v0, p0, Lcom/twitter/android/os;->i:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->c(Lcom/twitter/android/PhotoSelectHelper;)Ljava/util/EnumSet;

    move-result-object v0

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$MediaType;->a:Lcom/twitter/android/PhotoSelectHelper$MediaType;

    invoke-virtual {v0, v3}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/android/os;->i:Z

    if-nez v0, :cond_5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    :goto_3
    return-object v0

    :cond_3
    invoke-virtual {v0, v3}, Landroid/content/ContentResolver;->getType(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/os;->f:Ljava/lang/String;

    iput-object v3, p0, Lcom/twitter/android/os;->g:Landroid/net/Uri;

    goto/16 :goto_0

    :cond_4
    move v0, v2

    goto/16 :goto_1

    :cond_5
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3

    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method protected a(Ljava/lang/Integer;)V
    .locals 6

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/os;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AttachMediaListener;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v0

    const v1, 0x7f0f0222    # com.twitter.android.R.string.load_image_failure

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;I)V

    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->e()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/os;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    sget-object v3, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    if-eq v1, v3, :cond_3

    iget-boolean v1, p0, Lcom/twitter/android/os;->i:Z

    if-nez v1, :cond_3

    const-string/jumbo v1, "image/gif"

    iget-object v3, p0, Lcom/twitter/android/os;->f:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v1}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/media/filters/c;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x1

    :goto_1
    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/twitter/android/os;->b:Landroid/net/Uri;

    sget-object v2, Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;->b:Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V

    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v1, p0, Lcom/twitter/android/os;->b:Landroid/net/Uri;

    invoke-static {v0, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PhotoSelectHelper;Landroid/net/Uri;)Landroid/net/Uri;

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v1}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/twitter/android/FilterActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "uri"

    iget-object v2, p0, Lcom/twitter/android/os;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "scribe_section"

    iget-object v2, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v2}, Lcom/twitter/android/PhotoSelectHelper;->d(Lcom/twitter/android/PhotoSelectHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/os;->c:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    sget-object v2, Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;->a:Lcom/twitter/android/PhotoSelectHelper$AddMediaMode;

    if-ne v1, v2, :cond_2

    const-string/jumbo v1, "title_icon_res_id"

    const v2, 0x7f02015a    # com.twitter.android.R.drawable.ic_compose_album_multi_grid

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "done_menu_item_res_id"

    const v2, 0x7f11000d    # com.twitter.android.R.menu.filter_select

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_2
    iget-object v1, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v1}, Lcom/twitter/android/PhotoSelectHelper;->b(Lcom/twitter/android/PhotoSelectHelper;)Landroid/app/Activity;

    move-result-object v1

    const/16 v2, 0x103

    invoke-virtual {v1, v0, v2}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    goto/16 :goto_0

    :cond_3
    move v1, v2

    goto :goto_1

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v1, p0, Lcom/twitter/android/os;->b:Landroid/net/Uri;

    iget-object v3, p0, Lcom/twitter/android/os;->g:Landroid/net/Uri;

    invoke-virtual {v0, v1, v3}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;Landroid/net/Uri;)Lcom/twitter/android/PostStorage$MediaItem;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/os;->i:Z

    iput-boolean v1, v0, Lcom/twitter/android/PostStorage$MediaItem;->k:Z

    iget-object v1, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    const/4 v3, 0x0

    iget-wide v4, p0, Lcom/twitter/android/os;->a:J

    invoke-static {v1, v3, v4, v5}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/android/PhotoSelectHelper;Landroid/content/Intent;J)V

    iget-object v1, p0, Lcom/twitter/android/os;->d:Lcom/twitter/android/PhotoSelectHelper;

    invoke-static {v0}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Ljava/util/Collection;Z)V

    goto/16 :goto_0
.end method

.method public a()Z
    .locals 2

    const/4 v1, 0x0

    new-array v0, v1, [Ljava/lang/Void;

    invoke-virtual {p0, v0}, Lcom/twitter/android/os;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return v1
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/os;->cancel(Z)Z

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/os;->a([Ljava/lang/Void;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Integer;

    invoke-virtual {p0, p1}, Lcom/twitter/android/os;->a(Ljava/lang/Integer;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/os;->e:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/AttachMediaListener;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/os;->b:Landroid/net/Uri;

    sget-object v2, Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;->a:Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;

    invoke-interface {v0, v1, v2}, Lcom/twitter/android/AttachMediaListener;->a(Landroid/net/Uri;Lcom/twitter/android/AttachMediaListener$MediaAttachProgress;)V

    :cond_0
    return-void
.end method
