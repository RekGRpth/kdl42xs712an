.class public Lcom/twitter/android/BackupCodeFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Lcom/twitter/android/widget/ce;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/twitter/android/ar;

.field private c:Lcom/twitter/android/as;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->d:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/BackupCodeFragment;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/BackupCodeFragment;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/BackupCodeFragment;->a_(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/BackupCodeFragment;I)V
    .locals 0

    invoke-virtual {p0, p1}, Lcom/twitter/android/BackupCodeFragment;->b(I)V

    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->b:Lcom/twitter/android/ar;

    invoke-virtual {v0}, Lcom/twitter/android/ar;->clear()V

    iget-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->b:Lcom/twitter/android/ar;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ar;->add(Ljava/lang/Object;)V

    iput-object p1, p0, Lcom/twitter/android/BackupCodeFragment;->d:Ljava/lang/String;

    return-void
.end method

.method private static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/16 v3, 0x8

    const/4 v2, 0x4

    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xc

    if-eq v0, v1, :cond_1

    :cond_0
    :goto_0
    return-object p0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const/16 v1, 0xe

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(I)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method

.method private q()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/twitter/library/util/Util;->b(Landroid/content/Context;Ljava/lang/String;)V

    const v0, 0x7f0f00da    # com.twitter.android.R.string.copied_to_clipboard

    invoke-virtual {p0, v0}, Lcom/twitter/android/BackupCodeFragment;->a(I)V

    return-void
.end method

.method private r()V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v6, [Ljava/lang/String;

    const-string/jumbo v4, "backup_code::take_screenshot::impression"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-static {v6}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0247    # com.twitter.android.R.string.login_verification_generated_code

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0259    # com.twitter.android.R.string.login_verification_welcome_take_screenshot

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0571    # com.twitter.android.R.string.yes

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f029d    # com.twitter.android.R.string.no

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/content/DialogInterface$OnCancelListener;)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {v0, p0, v5}, Lcom/twitter/android/widget/PromptDialogFragment;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method private s()V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {v0}, Landroid/view/View;->getDrawingCache()Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v4}, Landroid/view/View;->setDrawingCacheEnabled(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v2, Lcom/twitter/android/au;

    const v3, 0x7f0f038a    # com.twitter.android.R.string.saving

    invoke-virtual {p0, v3}, Lcom/twitter/android/BackupCodeFragment;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, p0, v0, v3}, Lcom/twitter/android/au;-><init>(Lcom/twitter/android/BackupCodeFragment;Landroid/content/Context;Ljava/lang/String;)V

    new-array v0, v5, [Landroid/graphics/Bitmap;

    aput-object v1, v0, v4

    invoke-virtual {v2, v0}, Lcom/twitter/android/au;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->e()V

    goto :goto_0
.end method


# virtual methods
.method protected a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 1

    const v0, 0x7f030017    # com.twitter.android.R.layout.backup_code

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/BackupCodeFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method a(I)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/twitter/android/BackupCodeFragment;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 6

    const/4 v5, 0x0

    const/4 v3, 0x1

    if-ne p2, v3, :cond_0

    const/4 v0, -0x1

    if-ne p3, v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "backup_code::take_screenshot:ok:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->s()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "backup_code::take_screenshot:cancel:click"

    aput-object v4, v3, v5

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0
.end method

.method a(Landroid/net/Uri;)V
    .locals 6

    if-eqz p1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "backup_code::take_screenshot::success"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const v0, 0x7f0f0395    # com.twitter.android.R.string.screenshot_success

    invoke-virtual {p0, v0}, Lcom/twitter/android/BackupCodeFragment;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->e()V

    goto :goto_0
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 4

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v2

    invoke-interface {v2}, Landroid/widget/ListAdapter;->getCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    if-ne p3, v2, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/twitter/library/api/account/k;->d(Landroid/content/Context;Ljava/lang/String;)V

    new-instance v0, Lcom/twitter/android/at;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/BackupCodeFragment;->a:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/twitter/android/at;-><init>(Lcom/twitter/android/BackupCodeFragment;Landroid/content/Context;Ljava/lang/String;Z)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/at;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/ah;

    invoke-virtual {v0, p3}, Lcom/twitter/android/widget/ah;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->q()V

    goto :goto_0
.end method

.method a(Ljava/lang/String;Z)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f0f024d    # com.twitter.android.R.string.login_verification_please_reenroll

    invoke-virtual {p0, v1}, Lcom/twitter/android/BackupCodeFragment;->a(I)V

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/twitter/android/BackupCodeFragment;->b(Ljava/lang/String;)V

    if-eqz p2, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->r()V

    goto :goto_0
.end method

.method e()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "backup_code::take_screenshot::failure"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    const/4 v0, 0x2

    invoke-static {v0}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0506    # com.twitter.android.R.string.unable_to_screenshot

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->d(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f0507    # com.twitter.android.R.string.unable_to_screenshot_write_down_code

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    const v1, 0x7f0f02d5    # com.twitter.android.R.string.ok

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    return-void
.end method

.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "backup_code::take_screenshot:cancel:click"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v4}, Lcom/twitter/android/BackupCodeFragment;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "bc_account_name"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/BackupCodeFragment;->a:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/BackupCodeFragment;->a:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/BackupCodeFragment;->a:Ljava/lang/String;

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    new-array v0, v4, [Ljava/lang/String;

    const-string/jumbo v4, "backup_code::::impression"

    aput-object v4, v0, v5

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    if-nez p1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "show_welcome"

    invoke-virtual {v0, v1, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0}, Lcom/twitter/android/BackupCodeFragment;->r()V

    :cond_2
    return-void
.end method

.method public onResume()V
    .locals 6

    const/4 v5, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/BackupCodeFragment;->d:Ljava/lang/String;

    aput-object v3, v2, v5

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v2, p0, Lcom/twitter/android/BackupCodeFragment;->b:Lcom/twitter/android/ar;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/BackupCodeFragment;->c:Lcom/twitter/android/as;

    if-nez v2, :cond_1

    :cond_0
    new-instance v2, Lcom/twitter/android/ar;

    const v3, 0x7f030018    # com.twitter.android.R.layout.backup_code_row_view

    const v4, 0x7f090094    # com.twitter.android.R.id.backup_code

    invoke-direct {v2, v0, v3, v4, v1}, Lcom/twitter/android/ar;-><init>(Landroid/content/Context;IILjava/util/ArrayList;)V

    iput-object v2, p0, Lcom/twitter/android/BackupCodeFragment;->b:Lcom/twitter/android/ar;

    new-instance v1, Lcom/twitter/android/as;

    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/BackupCodeFragment;->b:Lcom/twitter/android/ar;

    invoke-direct {v1, v0, v2, v3}, Lcom/twitter/android/as;-><init>(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/android/ar;)V

    iput-object v1, p0, Lcom/twitter/android/BackupCodeFragment;->c:Lcom/twitter/android/as;

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/BackupCodeFragment;->V()Landroid/widget/ListView;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/BackupCodeFragment;->c:Lcom/twitter/android/as;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/twitter/android/BackupCodeFragment;->d:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/twitter/android/at;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/BackupCodeFragment;->a:Ljava/lang/String;

    invoke-direct {v1, p0, v0, v2, v5}, Lcom/twitter/android/at;-><init>(Lcom/twitter/android/BackupCodeFragment;Landroid/content/Context;Ljava/lang/String;Z)V

    new-array v0, v5, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/twitter/android/at;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_2
    return-void
.end method
