.class Lcom/twitter/android/xn;
.super Lcom/twitter/android/card/a;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetFragment;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TweetFragment;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-direct {p0, p2}, Lcom/twitter/android/card/a;-><init>(Landroid/app/Activity;)V

    return-void
.end method

.method private A()Z
    .locals 3
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    const/4 v0, 0x0

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0xb

    if-lt v1, v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->isChangingConfigurations()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/xn;->p()Lcom/twitter/library/card/Card;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/Card;)V

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public a()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/AmplifyPlayerFragment;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-eqz v0, :cond_0

    check-cast v0, Lcom/twitter/android/AmplifyPlayerFragment;

    invoke-virtual {v0}, Lcom/twitter/android/AmplifyPlayerFragment;->h()V

    :cond_0
    return-void
.end method

.method public a(J)V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/card/a;->a(J)V

    iget-object v0, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    iput v2, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getActionButton()Lcom/twitter/library/widget/ActionButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public a(Lcom/twitter/library/card/element/Player;Z)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    const-class v1, Lcom/twitter/android/AmplifyPlayerFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/AmplifyPlayerFragment;

    invoke-direct {v0}, Lcom/twitter/android/AmplifyPlayerFragment;-><init>()V

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const v2, 0x7f0900df    # com.twitter.android.R.id.root_layout

    sget-object v3, Lcom/twitter/android/AmplifyPlayerFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0, v3}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    sget-object v2, Lcom/twitter/android/AmplifyPlayerFragment;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->executePendingTransactions()Z

    check-cast v0, Lcom/twitter/android/AmplifyPlayerFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/AmplifyPlayerFragment;->a(Lcom/twitter/library/card/element/Player;)V

    invoke-virtual {v0, p2}, Lcom/twitter/android/AmplifyPlayerFragment;->a(Z)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/AmplifyPlayerFragment;->a:Ljava/lang/String;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/FragmentManager;->popBackStack(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/provider/Tweet;ZZ)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/xn;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v1}, Lcom/twitter/android/TweetFragment;->P(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v1

    invoke-static {v0, v1, p1, p2, p3}, Lcom/twitter/android/amplify/a;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeAssociation;Lcom/twitter/library/provider/Tweet;ZZ)V

    return-void
.end method

.method public a(Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V
    .locals 12

    iget-object v0, p0, Lcom/twitter/android/xn;->i:Landroid/content/Context;

    iget-object v1, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v2, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v2}, Lcom/twitter/android/TweetFragment;->O(Lcom/twitter/android/TweetFragment;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move-wide/from16 v6, p4

    move/from16 v8, p6

    move/from16 v9, p7

    move-object/from16 v10, p8

    move/from16 v11, p9

    invoke-static/range {v0 .. v11}, Lcom/twitter/android/amplify/a;->a(Landroid/content/Context;Lcom/twitter/library/provider/Tweet;Lcom/twitter/library/scribe/ScribeAssociation;Ljava/lang/String;ILcom/twitter/library/amplify/model/AmplifyVideo;DIZLjava/lang/String;Z)V

    return-void
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 10

    const-string/jumbo v0, "click"

    invoke-virtual {p0, v0}, Lcom/twitter/android/xn;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    const/4 v6, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/xn;->b()Z

    move-result v9

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move v7, p4

    move v8, p5

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZ)V

    return-void
.end method

.method public b(J)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1, p2}, Lcom/twitter/android/card/a;->b(J)V

    iget-object v0, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->q:J

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    iput v2, v0, Lcom/twitter/library/provider/Tweet;->ac:I

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v0}, Lcom/twitter/android/widget/TweetDetailView;->getActionButton()Lcom/twitter/library/widget/ActionButton;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/library/widget/ActionButton;->setChecked(Z)V

    :cond_0
    return-void
.end method

.method public b()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public c()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public d()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->J(Lcom/twitter/android/TweetFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->n(Lcom/twitter/android/TweetFragment;)V

    :cond_0
    return-void
.end method

.method public f()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->J(Lcom/twitter/android/TweetFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->M(Lcom/twitter/android/TweetFragment;)V

    :cond_0
    return-void
.end method

.method public g()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->J(Lcom/twitter/android/TweetFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->N(Lcom/twitter/android/TweetFragment;)V

    :cond_0
    return-void
.end method

.method public i()V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0}, Lcom/twitter/android/card/a;->i()V

    iget-object v0, p0, Lcom/twitter/android/xn;->e:Lcom/twitter/android/card/p;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xn;->e:Lcom/twitter/android/card/p;

    invoke-virtual {v0}, Lcom/twitter/android/card/p;->a()Z

    iput-object v2, p0, Lcom/twitter/android/xn;->e:Lcom/twitter/android/card/p;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/xn;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-static {}, Lcom/twitter/android/card/n;->a()Lcom/twitter/android/card/n;

    move-result-object v1

    invoke-virtual {v1, p0, v0}, Lcom/twitter/android/card/n;->a(Lcom/twitter/library/card/k;Lcom/twitter/library/card/Card;)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/card/Card;)V

    :cond_1
    return-void
.end method

.method public j()V
    .locals 5

    const-string/jumbo v0, "click"

    invoke-virtual {p0, v0}, Lcom/twitter/android/xn;->g(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    sget-object v1, Lcom/twitter/library/api/PromotedEvent;->l:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v0, v1}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/library/api/PromotedEvent;)V

    iget-object v0, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->j:Lcom/twitter/android/yb;

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v2, p0, Lcom/twitter/android/xn;->d:Lcom/twitter/library/provider/Tweet;

    const/4 v3, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/xn;->b()Z

    move-result v4

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/android/yb;->a(Landroid/support/v4/app/Fragment;Lcom/twitter/library/provider/Tweet;ZZ)V

    return-void
.end method

.method public k()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/card/a;->k()V

    invoke-virtual {p0}, Lcom/twitter/android/xn;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/TweetDetailView;->setCard(Lcom/twitter/library/card/Card;)V

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TweetDetailView;->g()V

    invoke-virtual {v0}, Lcom/twitter/library/card/Card;->u()V

    :cond_0
    return-void
.end method

.method public l()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/card/a;->l()V

    invoke-virtual {p0}, Lcom/twitter/android/xn;->p()Lcom/twitter/library/card/Card;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/xn;->a:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->f:Lcom/twitter/android/widget/TweetDetailView;

    invoke-virtual {v1}, Lcom/twitter/android/widget/TweetDetailView;->h()V

    invoke-direct {p0}, Lcom/twitter/android/xn;->A()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/twitter/android/xn;->a(Lcom/twitter/library/card/Card;)V

    :cond_0
    invoke-virtual {v0, v1}, Lcom/twitter/library/card/Card;->c(Z)V

    :cond_1
    return-void
.end method
