.class public Lcom/twitter/android/TabbedFollowingsActivity;
.super Lcom/twitter/android/TabbedFragmentActivity;
.source "Twttr"

# interfaces
.implements Lcom/twitter/internal/android/widget/z;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/TabbedFragmentActivity;-><init>()V

    return-void
.end method

.method private a(Ljava/lang/String;ILandroid/os/Bundle;)V
    .locals 6

    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/TabbedFollowingsActivity;->e:Lcom/twitter/android/TabsAdapter;

    iget-object v2, p0, Lcom/twitter/android/TabbedFollowingsActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v2, p1}, Lcom/twitter/internal/android/widget/IconTabHost;->newTabSpec(Ljava/lang/String;)Landroid/widget/TabHost$TabSpec;

    move-result-object v2

    const v3, 0x7f0300dd    # com.twitter.android.R.layout.people_tab_indicator

    iget-object v4, p0, Lcom/twitter/android/TabbedFollowingsActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    const/4 v5, 0x0

    invoke-static {v0, v3, v4, v5, p2}, Lcom/twitter/android/widget/TabIndicator;->a(Landroid/view/LayoutInflater;ILandroid/widget/TabHost;II)Lcom/twitter/android/widget/TabIndicator;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TabHost$TabSpec;->setIndicator(Landroid/view/View;)Landroid/widget/TabHost$TabSpec;

    move-result-object v0

    const-class v2, Lcom/twitter/android/UsersFragment;

    invoke-virtual {v1, v0, v2, p3}, Lcom/twitter/android/TabsAdapter;->a(Landroid/widget/TabHost$TabSpec;Ljava/lang/Class;Landroid/os/Bundle;)V

    return-void
.end method

.method private b(I)Landroid/os/Bundle;
    .locals 3

    const/4 v2, 0x1

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "type"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "follow"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "fetch_always"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "refresh"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "originating_activity"

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private q()V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/TabbedFollowingsActivity;->b(I)Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "all"

    const v2, 0x7f0f01af    # com.twitter.android.R.string.followings_tab_title_all

    invoke-direct {p0, v1, v2, v0}, Lcom/twitter/android/TabbedFollowingsActivity;->a(Ljava/lang/String;ILandroid/os/Bundle;)V

    return-void
.end method

.method private r()V
    .locals 3

    const-string/jumbo v0, "favorite_people"

    const v1, 0x7f0f01b0    # com.twitter.android.R.string.followings_tab_title_favorite_people

    const/16 v2, 0x10

    invoke-direct {p0, v2}, Lcom/twitter/android/TabbedFollowingsActivity;->b(I)Landroid/os/Bundle;

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/TabbedFollowingsActivity;->a(Ljava/lang/String;ILandroid/os/Bundle;)V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 3

    const/4 v0, 0x2

    iput v0, p0, Lcom/twitter/android/TabbedFollowingsActivity;->a:I

    invoke-super {p0, p1, p2}, Lcom/twitter/android/TabbedFragmentActivity;->a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V

    const v0, 0x7f0f0326    # com.twitter.android.R.string.profile_friends

    invoke-virtual {p0, v0}, Lcom/twitter/android/TabbedFollowingsActivity;->setTitle(I)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFollowingsActivity;->d:Landroid/support/v4/view/ViewPager;

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFollowingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c00a3    # com.twitter.android.R.dimen.people_tabbar_margin

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iget-object v0, p0, Lcom/twitter/android/TabbedFollowingsActivity;->b:Lcom/twitter/internal/android/widget/IconTabHost;

    invoke-virtual {v0, p0}, Lcom/twitter/internal/android/widget/IconTabHost;->setOnTabClickedListener(Lcom/twitter/internal/android/widget/z;)V

    invoke-direct {p0}, Lcom/twitter/android/TabbedFollowingsActivity;->q()V

    invoke-direct {p0}, Lcom/twitter/android/TabbedFollowingsActivity;->r()V

    return-void
.end method

.method public f()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFollowingsActivity;->g()V

    return-void
.end method

.method protected g()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFollowingsActivity;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->aB()V

    :cond_0
    return-void
.end method

.method public onTabChanged(Ljava/lang/String;)V
    .locals 5

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/TabbedFragmentActivity;->onTabChanged(Ljava/lang/String;)V

    move v1, v2

    move v3, v2

    :goto_0
    iget v0, p0, Lcom/twitter/android/TabbedFollowingsActivity;->a:I

    if-ge v1, v0, :cond_1

    invoke-virtual {p0, v1}, Lcom/twitter/android/TabbedFollowingsActivity;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    if-eqz v0, :cond_0

    iget-boolean v4, v0, Lcom/twitter/android/UsersFragment;->x:Z

    if-eqz v4, :cond_0

    iput-boolean v2, v0, Lcom/twitter/android/UsersFragment;->x:Z

    const/4 v3, 0x1

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/TabbedFollowingsActivity;->n()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UsersFragment;

    if-eqz v0, :cond_2

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/twitter/android/UsersFragment;->d(I)Z

    :cond_2
    return-void
.end method
