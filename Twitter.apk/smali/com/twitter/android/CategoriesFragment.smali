.class public Lcom/twitter/android/CategoriesFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/tz;


# instance fields
.field a:Lcom/twitter/library/util/ao;

.field b:Z

.field c:Z

.field d:Z

.field e:Z

.field private f:Landroid/content/SharedPreferences;

.field private g:Ljava/util/HashSet;

.field private h:Ljava/util/ArrayList;

.field private i:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/CategoriesFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/CategoriesFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/CategoriesFragment;)Ljava/util/HashSet;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->g:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/CategoriesFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/CategoriesFragment;->e(Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    return-object v0
.end method

.method static synthetic b(Lcom/twitter/android/CategoriesFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/CategoriesFragment;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic d(Lcom/twitter/android/CategoriesFragment;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->i:Ljava/util/ArrayList;

    return-object v0
.end method

.method private q()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->b:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "welcome"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "interests"

    goto :goto_0
.end method

.method private r()Ljava/lang/String;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->b:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "category"

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private s()V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string/jumbo v5, "interests::::impression"

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    return-void
.end method

.method private u()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->v()V

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->y()V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->g:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    return-void
.end method

.method private v()V
    .locals 6

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v2, p0, Lcom/twitter/android/CategoriesFragment;->Q:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->q()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->r()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    aput-object v5, v2, v3

    const/4 v3, 0x3

    aput-object v5, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "results"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/CategoriesFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method

.method private y()V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-wide v2, p0, Lcom/twitter/android/CategoriesFragment;->Q:J

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x5

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->q()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->r()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const/4 v4, 0x0

    aput-object v4, v2, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "user"

    aput-object v4, v2, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "results"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/CategoriesFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->a(Ljava/util/ArrayList;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->i:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    return-void
.end method


# virtual methods
.method protected a(I)V
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->d:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/CategoriesFragment;->Q:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->g(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/CategoriesFragment;->a(Ljava/lang/String;I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-wide v1, p0, Lcom/twitter/android/CategoriesFragment;->Q:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->h(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p1}, Lcom/twitter/android/CategoriesFragment;->a(Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 2

    const/4 v1, 0x3

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, p2}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->c:Z

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/CategoriesFragment;->a(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/CategoriesFragment;->b(I)V

    goto :goto_0
.end method

.method protected a(Landroid/widget/ListView;Landroid/view/View;IJ)V
    .locals 1

    invoke-virtual {p1, p3}, Landroid/widget/ListView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/CategoriesFragment;->startActivity(Landroid/content/Intent;)V

    return-void
.end method

.method protected a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/cg;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/cg;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method protected a(Z)V
    .locals 6

    const/4 v5, 0x0

    const/4 v4, 0x0

    const/4 v3, 0x3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->f:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "loc"

    const-string/jumbo v2, ""

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz p1, :cond_1

    invoke-virtual {p0, v3}, Lcom/twitter/android/CategoriesFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-lez v1, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget-object v1, v1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0, v3}, Lcom/twitter/android/CategoriesFragment;->a_(I)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/CategoriesFragment;->a(I)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v3}, Lcom/twitter/android/CategoriesFragment;->a_(I)V

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    invoke-virtual {v0, v4, v5, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    goto :goto_0
.end method

.method protected a_()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->a_()V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    check-cast v0, Lcom/twitter/android/cg;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/cg;->a()V

    :cond_0
    return-void
.end method

.method public e()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->s()V

    return-void
.end method

.method protected i()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->i()V

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/CategoriesFragment;->a(I)V

    return-void
.end method

.method public n_()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/cg;

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/twitter/android/cg;-><init>(Lcom/twitter/android/CategoriesFragment;Landroid/content/Context;I)V

    iput-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v2

    iput-object v2, p0, Lcom/twitter/android/CategoriesFragment;->f:Landroid/content/SharedPreferences;

    new-instance v2, Lcom/twitter/library/util/ao;

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    const/high16 v4, 0x200000

    invoke-direct {v2, v3, v0, v4, v7}, Lcom/twitter/library/util/ao;-><init>(Landroid/content/Context;IILjava/lang/String;)V

    iput-object v2, p0, Lcom/twitter/android/CategoriesFragment;->a:Lcom/twitter/library/util/ao;

    iget-object v2, p0, Lcom/twitter/android/CategoriesFragment;->a:Lcom/twitter/library/util/ao;

    invoke-virtual {v2, p0}, Lcom/twitter/library/util/ao;->a(Lcom/twitter/library/util/ar;)V

    new-instance v2, Lcom/twitter/android/ch;

    invoke-direct {v2, p0}, Lcom/twitter/android/ch;-><init>(Lcom/twitter/android/CategoriesFragment;)V

    iput-object v2, p0, Lcom/twitter/android/CategoriesFragment;->O:Lcom/twitter/library/client/j;

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v2

    if-eqz v2, :cond_0

    const-string/jumbo v3, "onboarding"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/CategoriesFragment;->b:Z

    const-string/jumbo v3, "personalized"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    iput-boolean v3, p0, Lcom/twitter/android/CategoriesFragment;->d:Z

    const-string/jumbo v3, "is_hidden"

    invoke-virtual {v2, v3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    :cond_0
    if-nez p1, :cond_2

    iget-boolean v2, p0, Lcom/twitter/android/CategoriesFragment;->b:Z

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->av()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v5, 0x5

    new-array v5, v5, [Ljava/lang/String;

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->q()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->r()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v0

    const/4 v0, 0x2

    aput-object v7, v5, v0

    const/4 v0, 0x3

    aput-object v7, v5, v0

    const/4 v0, 0x4

    const-string/jumbo v6, "impression"

    aput-object v6, v5, v0

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    iput-boolean v1, p0, Lcom/twitter/android/CategoriesFragment;->c:Z

    :goto_0
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/CategoriesFragment;->g:Ljava/util/HashSet;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/CategoriesFragment;->h:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/CategoriesFragment;->i:Ljava/util/ArrayList;

    return-void

    :cond_2
    const-string/jumbo v2, "was_personalized"

    iget-boolean v3, p0, Lcom/twitter/android/CategoriesFragment;->d:Z

    invoke-virtual {p1, v2, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iget-boolean v3, p0, Lcom/twitter/android/CategoriesFragment;->d:Z

    if-ne v2, v3, :cond_3

    const-string/jumbo v2, "state_fetched"

    invoke-virtual {p1, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_1
    iput-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->c:Z

    const-string/jumbo v0, "is_hidden"

    iget-boolean v1, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 7

    const/4 v4, 0x0

    new-instance v0, Landroid/support/v4/content/CursorLoader;

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/provider/y;->a:Landroid/net/Uri;

    iget-wide v5, p0, Lcom/twitter/android/CategoriesFragment;->Q:J

    invoke-static {v2, v5, v6}, Lcom/twitter/library/provider/w;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    sget-object v3, Lcom/twitter/library/provider/bd;->a:[Ljava/lang/String;

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Landroid/support/v4/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onDestroy()V

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->aw()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/CategoriesFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/CursorAdapter;->swapCursor(Landroid/database/Cursor;)Landroid/database/Cursor;

    return-void
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->f:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "loc"

    invoke-virtual {p0}, Lcom/twitter/android/CategoriesFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget-object v2, v2, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v2}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onResume()V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/CategoriesFragment;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/CategoriesFragment;->P:Landroid/support/v4/widget/CursorAdapter;

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/twitter/android/CategoriesFragment;->a(I)V

    :cond_0
    iget-boolean v0, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->s()V

    :cond_1
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "state_fetched"

    iget-boolean v1, p0, Lcom/twitter/android/CategoriesFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "was_personalized"

    iget-boolean v1, p0, Lcom/twitter/android/CategoriesFragment;->d:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "is_hidden"

    iget-boolean v1, p0, Lcom/twitter/android/CategoriesFragment;->e:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    return-void
.end method

.method public onStop()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/CategoriesFragment;->u()V

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->onStop()V

    return-void
.end method
