.class public Lcom/twitter/android/rh;
.super Lcom/twitter/android/aaa;
.source "Twttr"


# instance fields
.field protected final a:Z

.field protected b:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IZ)V
    .locals 11

    const/4 v7, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move/from16 v8, p7

    invoke-direct/range {v1 .. v10}, Lcom/twitter/android/aaa;-><init>(Landroid/content/Context;IILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IIZZ)V

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/twitter/android/rh;->b:Z

    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/twitter/android/rh;->a:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;Z)V
    .locals 9

    const/4 v7, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move-object v6, p6

    move/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/twitter/android/rh;-><init>(Landroid/content/Context;ILcom/twitter/android/client/c;ILcom/twitter/library/widget/a;Lcom/twitter/library/util/FriendshipCache;IZ)V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/rh;->a:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x2

    goto :goto_0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/rh;->b:Z

    return-void
.end method

.method public bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V
    .locals 9

    const/16 v8, 0xc

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/16 v6, 0xd

    const/4 v2, 0x0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/aaa;->bindView(Landroid/view/View;Landroid/content/Context;Landroid/database/Cursor;)V

    const/16 v0, 0xe

    invoke-interface {p3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    iget-boolean v0, p0, Lcom/twitter/android/rh;->a:Z

    if-eqz v0, :cond_2

    move-object v0, p1

    check-cast v0, Lcom/twitter/library/widget/UserSocialView;

    iget-object v1, p0, Lcom/twitter/android/rh;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v1}, Lcom/twitter/android/client/c;->V()F

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/UserSocialView;->setContentSize(F)V

    iget-boolean v1, p0, Lcom/twitter/android/rh;->e:Z

    if-eqz v1, :cond_0

    const/16 v1, 0x8

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const/16 v1, 0x9

    invoke-interface {p3, v1}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v1

    invoke-static {v1}, Lcom/twitter/library/util/Util;->a([B)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/api/TweetEntities;

    invoke-virtual {v0, v5, v1}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    :goto_0
    iget-boolean v1, p0, Lcom/twitter/android/rh;->b:Z

    if-eqz v1, :cond_1

    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    :goto_1
    packed-switch v1, :pswitch_data_0

    iget-object v4, p0, Lcom/twitter/android/rh;->c:Lcom/twitter/android/client/c;

    iget-boolean v5, v4, Lcom/twitter/android/client/c;->f:Z

    move v4, v2

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/UserSocialView;->a(IILjava/lang/String;IZ)V

    :goto_2
    return-void

    :cond_0
    invoke-virtual {v0, v3, v3}, Lcom/twitter/library/widget/UserSocialView;->a(Ljava/lang/String;Lcom/twitter/library/api/TweetEntities;)V

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1

    :pswitch_0
    const v2, 0x7f020117    # com.twitter.android.R.drawable.ic_activity_follow_tweet_default

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p0, Lcom/twitter/android/rh;->c:Lcom/twitter/android/client/c;

    iget-boolean v5, v5, Lcom/twitter/android/client/c;->f:Z

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/widget/UserSocialView;->a(IILjava/lang/String;IZ)V

    goto :goto_2

    :cond_2
    invoke-interface {p3, v8}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    packed-switch v0, :pswitch_data_1

    :goto_3
    check-cast p1, Lcom/twitter/library/widget/BaseUserView;

    invoke-virtual {p1, v3}, Lcom/twitter/library/widget/BaseUserView;->setExtraInfo(Ljava/lang/String;)V

    goto :goto_2

    :pswitch_1
    if-nez v4, :cond_3

    const v0, 0x7f0f01a4    # com.twitter.android.R.string.followed_by

    new-array v1, v7, [Ljava/lang/Object;

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :cond_3
    invoke-virtual {p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e0002    # com.twitter.android.R.plurals.followed_by_count

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v2

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v3, v7

    invoke-virtual {v0, v1, v4, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_2
    const v0, 0x7f0f0452    # com.twitter.android.R.string.social_both_follow

    new-array v1, v7, [Ljava/lang/Object;

    invoke-interface {p3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p2, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public newView(Landroid/content/Context;Landroid/database/Cursor;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/twitter/android/rh;->a:Z

    if-eqz v0, :cond_0

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03017e    # com.twitter.android.R.layout.user_social_row_view

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/rh;->a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f03017d    # com.twitter.android.R.layout.user_row_view

    invoke-virtual {v0, v1, p3, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/rh;->a(Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;

    move-result-object v0

    goto :goto_0
.end method
