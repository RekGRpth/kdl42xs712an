.class Lcom/twitter/android/mf;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/MessagesActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/MessagesActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/mf;->a:Lcom/twitter/android/MessagesActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/mf;->a:Lcom/twitter/android/MessagesActivity;

    invoke-virtual {v0}, Lcom/twitter/android/MessagesActivity;->e()V

    iget-object v0, p0, Lcom/twitter/android/mf;->a:Lcom/twitter/android/MessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/MessagesActivity;->a(Lcom/twitter/android/MessagesActivity;)V

    iget-object v0, p0, Lcom/twitter/android/mf;->a:Lcom/twitter/android/MessagesActivity;

    invoke-static {v0}, Lcom/twitter/android/MessagesActivity;->b(Lcom/twitter/android/MessagesActivity;)Landroid/support/v4/widget/SlidingPaneLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/SlidingPaneLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    return-void
.end method
