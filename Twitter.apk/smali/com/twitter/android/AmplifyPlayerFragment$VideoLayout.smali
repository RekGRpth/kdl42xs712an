.class public final Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;
.super Landroid/widget/FrameLayout;
.source "Twttr"


# instance fields
.field private final a:Lcom/twitter/library/amplify/n;

.field private final b:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/amplify/n;Lcom/twitter/library/amplify/control/AmplifyVideoControlView;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->setWillNotDraw(Z)V

    iput-object p2, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->a:Lcom/twitter/library/amplify/n;

    iput-object p3, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->b:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->a:Lcom/twitter/library/amplify/n;

    invoke-virtual {v0}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->b:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {p0, v0}, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->addView(Landroid/view/View;)V

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->a:Lcom/twitter/library/amplify/n;

    invoke-static {v0, p2, p3, p4, p5}, Lcom/twitter/library/amplify/n;->a(Lcom/twitter/library/amplify/n;IIII)Landroid/graphics/Rect;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->b:Lcom/twitter/library/amplify/control/AmplifyVideoControlView;

    invoke-virtual {v1, p2, p3, p4, p5}, Lcom/twitter/library/amplify/control/AmplifyVideoControlView;->layout(IIII)V

    iget-object v1, p0, Lcom/twitter/android/AmplifyPlayerFragment$VideoLayout;->a:Lcom/twitter/library/amplify/n;

    invoke-virtual {v1}, Lcom/twitter/library/amplify/n;->a()Landroid/view/View;

    move-result-object v1

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iget v3, v0, Landroid/graphics/Rect;->top:I

    iget v4, v0, Landroid/graphics/Rect;->right:I

    iget v0, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v1, v2, v3, v4, v0}, Landroid/view/View;->layout(IIII)V

    return-void
.end method
