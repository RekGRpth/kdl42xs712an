.class public Lcom/twitter/android/MentionContactFragment;
.super Lcom/twitter/android/client/BaseListFragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/twitter/library/widget/i;


# instance fields
.field private a:Lcom/twitter/android/cz;

.field private b:Ljava/util/ArrayList;

.field private c:Z

.field private d:Lcom/twitter/android/lv;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BaseListFragment;-><init>()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MentionContactFragment;->c:Z

    return-void
.end method

.method private a(Landroid/widget/ListView;)V
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0xb
    .end annotation

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0xb

    if-lt v0, v1, :cond_0

    const/high16 v0, 0x3000000

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setScrollBarStyle(I)V

    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setFastScrollAlwaysVisible(Z)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/ListView;->setVerticalScrollBarEnabled(Z)V

    goto :goto_0
.end method

.method private e()V
    .locals 2

    const/4 v1, 0x3

    iget-boolean v0, p0, Lcom/twitter/android/MentionContactFragment;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    invoke-virtual {v0}, Lcom/twitter/android/cz;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/MentionContactFragment;->a_(I)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, v1}, Lcom/twitter/android/MentionContactFragment;->b(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V
    .locals 6

    const/4 v5, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/support/v4/content/Loader;->getId()I

    move-result v0

    if-ne v0, v5, :cond_2

    if-eqz p2, :cond_2

    invoke-interface {p2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    :goto_0
    invoke-interface {p2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v1, "vnd.android.cursor.item/email_v2"

    invoke-interface {p2, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {p2, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p2, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_0

    if-eqz v1, :cond_0

    const-string/jumbo v3, "@"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-lez v3, :cond_0

    sget-object v3, Lcom/twitter/library/util/x;->h:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/regex/Matcher;->matches()Z

    move-result v3

    if-eqz v3, :cond_0

    new-instance v3, Lcom/twitter/library/api/TwitterContact;

    invoke-direct {v3, v2, v1, v4}, Lcom/twitter/library/api/TwitterContact;-><init>(Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    iget-object v1, p0, Lcom/twitter/android/MentionContactFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    invoke-virtual {v0}, Lcom/twitter/android/cz;->notifyDataSetChanged()V

    :cond_2
    iput-boolean v4, p0, Lcom/twitter/android/MentionContactFragment;->c:Z

    invoke-direct {p0}, Lcom/twitter/android/MentionContactFragment;->e()V

    return-void
.end method

.method public a(Lcom/twitter/android/lv;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/MentionContactFragment;->d:Lcom/twitter/android/lv;

    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/cz;->a(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected b()V
    .locals 0

    invoke-super {p0}, Lcom/twitter/android/client/BaseListFragment;->b()V

    invoke-direct {p0}, Lcom/twitter/android/MentionContactFragment;->e()V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onActivityCreated(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->b:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    new-instance v0, Lcom/twitter/android/cz;

    invoke-virtual {p0}, Lcom/twitter/android/MentionContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MentionContactFragment;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1, v2, p0}, Lcom/twitter/android/cz;-><init>(Landroid/content/Context;Ljava/util/ArrayList;Lcom/twitter/library/widget/i;)V

    iput-object v0, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MentionContactFragment;->V()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setFastScrollEnabled(Z)V

    invoke-direct {p0, v0}, Lcom/twitter/android/MentionContactFragment;->a(Landroid/widget/ListView;)V

    invoke-direct {p0}, Lcom/twitter/android/MentionContactFragment;->e()V

    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/twitter/android/MentionContactFragment;->c:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/MentionContactFragment;->getLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v3, v1, p0}, Landroid/support/v4/app/LoaderManager;->initLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    :cond_1
    return-void
.end method

.method public onClick(Lcom/twitter/library/widget/InviteView;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->d:Lcom/twitter/android/lv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MentionContactFragment;->d:Lcom/twitter/android/lv;

    iget-object v1, p0, Lcom/twitter/android/MentionContactFragment;->a:Lcom/twitter/android/cz;

    invoke-virtual {v1, p2}, Lcom/twitter/android/cz;->a(I)Lcom/twitter/library/api/TwitterContact;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/twitter/android/lv;->a(Lcom/twitter/library/api/TwitterContact;)V

    :cond_0
    return-void
.end method

.method public bridge synthetic onClick(Ljava/lang/Object;I)V
    .locals 0

    check-cast p1, Lcom/twitter/library/widget/InviteView;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/MentionContactFragment;->onClick(Lcom/twitter/library/widget/InviteView;I)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onCreate(Landroid/os/Bundle;)V

    if-eqz p1, :cond_0

    const-string/jumbo v0, "state_loading"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MentionContactFragment;->c:Z

    const-string/jumbo v0, "contact_list"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MentionContactFragment;->b:Ljava/util/ArrayList;

    :goto_0
    return-void

    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/MentionContactFragment;->b:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public onCreateLoader(ILandroid/os/Bundle;)Landroid/support/v4/content/Loader;
    .locals 1

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MentionContactFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/cx;->a(Landroid/content/Context;)Landroid/support/v4/content/Loader;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    const v0, 0x7f03003e    # com.twitter.android.R.layout.contact_list_fragment

    invoke-virtual {p0, p1, v0, p2}, Lcom/twitter/android/MentionContactFragment;->a(Landroid/view/LayoutInflater;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    const v0, 0x7f09010d    # com.twitter.android.R.id.contact_filter_query

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    invoke-virtual {v0, p0}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    return-object v1
.end method

.method public synthetic onLoadFinished(Landroid/support/v4/content/Loader;Ljava/lang/Object;)V
    .locals 0

    check-cast p2, Landroid/database/Cursor;

    invoke-virtual {p0, p1, p2}, Lcom/twitter/android/MentionContactFragment;->a(Landroid/support/v4/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onLoaderReset(Landroid/support/v4/content/Loader;)V
    .locals 0

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseListFragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "state_loading"

    iget-boolean v1, p0, Lcom/twitter/android/MentionContactFragment;->c:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "contact_list"

    iget-object v1, p0, Lcom/twitter/android/MentionContactFragment;->b:Ljava/util/ArrayList;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method
