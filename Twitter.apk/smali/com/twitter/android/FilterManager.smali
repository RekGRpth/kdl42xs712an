.class public Lcom/twitter/android/FilterManager;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"


# instance fields
.field protected a:Landroid/content/Context;

.field protected b:Ljava/util/LinkedHashMap;

.field protected c:Lcom/twitter/media/filters/Filters;

.field protected d:I

.field protected e:Lcom/twitter/android/gk;

.field protected f:[Landroid/graphics/Bitmap;

.field protected g:Landroid/net/Uri;

.field protected h:Z

.field protected i:Z

.field protected j:Lcom/twitter/android/gi;

.field private k:Lcom/twitter/android/gl;

.field private l:Ljava/util/LinkedHashMap;

.field private m:I

.field private n:I

.field private o:Lcom/twitter/android/gj;

.field private p:I


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method

.method public static a(Landroid/support/v4/app/FragmentManager;Landroid/content/Context;)Lcom/twitter/android/FilterManager;
    .locals 3

    const-string/jumbo v0, "FilterManager"

    invoke-virtual {p0, v0}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/FilterManager;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/FilterManager;

    invoke-direct {v0}, Lcom/twitter/android/FilterManager;-><init>()V

    invoke-virtual {p0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    const-string/jumbo v2, "FilterManager"

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, v0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    return-object v0
.end method

.method private e(I)Lcom/twitter/android/gh;
    .locals 2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/gl;->a(I)Lcom/twitter/android/gh;

    move-result-object v0

    goto :goto_0
.end method

.method private e()V
    .locals 9

    const/4 v2, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/twitter/android/FilterManager;->i:Z

    if-nez v0, :cond_5

    iget v0, p0, Lcom/twitter/android/FilterManager;->p:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gn;

    iget v1, p0, Lcom/twitter/android/FilterManager;->m:I

    if-eqz v0, :cond_0

    add-int/lit8 v1, v1, -0x1

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    if-le v3, v1, :cond_1

    iget-object v3, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v3}, Ljava/util/LinkedHashMap;->size()I

    move-result v3

    sub-int/2addr v3, v1

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->size()I

    move-result v1

    new-array v4, v1, [Ljava/lang/Integer;

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1, v4}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move v1, v7

    :goto_0
    if-ge v1, v3, :cond_1

    iget-object v5, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    aget-object v6, v4, v1

    invoke-virtual {v5, v6}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_1
    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gn;

    move-object v1, v0

    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/FilterManager;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v3

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->e:Lcom/twitter/android/gk;

    iget v4, v1, Lcom/twitter/android/gn;->b:I

    invoke-direct {p0, v4}, Lcom/twitter/android/FilterManager;->e(I)Lcom/twitter/android/gh;

    move-result-object v5

    if-eqz v5, :cond_8

    iget-object v4, v5, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    :goto_2
    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v4}, Lcom/twitter/android/gl;->a()Landroid/graphics/Bitmap;

    move-result-object v4

    :cond_2
    if-nez v4, :cond_3

    iget-object v4, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/twitter/android/gl;->a(Z)Landroid/graphics/Bitmap;

    move-result-object v4

    :cond_3
    if-nez v4, :cond_4

    if-eqz v0, :cond_4

    :try_start_0
    iget v6, v0, Lcom/twitter/android/gk;->a:I

    iget v8, v0, Lcom/twitter/android/gk;->b:I

    iget-object v0, v0, Lcom/twitter/android/gk;->c:Landroid/graphics/Bitmap$Config;

    invoke-static {v6, v8, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    :cond_4
    :goto_3
    if-nez v4, :cond_6

    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    new-instance v2, Lcom/twitter/library/util/BitmapNotCreatedException;

    const-string/jumbo v3, "Unable to create image for filtering"

    invoke-direct {v2, v3}, Lcom/twitter/library/util/BitmapNotCreatedException;-><init>(Ljava/lang/String;)V

    invoke-static {v0, v2}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Ljava/lang/Throwable;)V

    iget-object v0, v1, Lcom/twitter/android/gn;->a:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gf;

    iget-object v1, v0, Lcom/twitter/android/gf;->a:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/twitter/android/gf;->b:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    const v0, 0x7f020285    # com.twitter.android.R.drawable.ic_tweet_placeholder_photo_dark_error

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    sget-object v0, Landroid/widget/ImageView$ScaleType;->CENTER_INSIDE:Landroid/widget/ImageView$ScaleType;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setScaleType(Landroid/widget/ImageView$ScaleType;)V

    :cond_5
    :goto_4
    return-void

    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/crashlytics/android/d;->a(Ljava/lang/Throwable;)V

    goto :goto_3

    :cond_6
    if-nez v5, :cond_7

    new-instance v5, Lcom/twitter/android/gh;

    invoke-direct {v5}, Lcom/twitter/android/gh;-><init>()V

    iget v0, v1, Lcom/twitter/android/gn;->b:I

    iput v0, v5, Lcom/twitter/android/gh;->b:I

    iput-object v2, v5, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    :cond_7
    iget-boolean v0, p0, Lcom/twitter/android/FilterManager;->h:Z

    iput-boolean v0, v5, Lcom/twitter/android/gh;->a:Z

    iget v0, v5, Lcom/twitter/android/gh;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0, v5}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Lcom/twitter/android/gl;->b(I)Lcom/twitter/android/gh;

    new-instance v0, Lcom/twitter/android/gj;

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    iget v3, p0, Lcom/twitter/android/FilterManager;->d:I

    iget-object v6, v1, Lcom/twitter/android/gn;->a:Landroid/view/View;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/gj;-><init>(Lcom/twitter/android/FilterManager;Lcom/twitter/media/filters/Filters;ILandroid/graphics/Bitmap;Lcom/twitter/android/gh;Landroid/view/View;)V

    iput-object v0, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gj;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_4

    :cond_8
    move-object v4, v2

    goto/16 :goto_2

    :cond_9
    move-object v1, v0

    goto/16 :goto_1
.end method


# virtual methods
.method public a()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/FilterManager;->i:Z

    invoke-direct {p0}, Lcom/twitter/android/FilterManager;->e()V

    return-void
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/twitter/android/FilterManager;->p:I

    return-void
.end method

.method public a(II)V
    .locals 1

    iput p2, p0, Lcom/twitter/android/FilterManager;->n:I

    const/4 v0, 0x2

    invoke-static {p1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/FilterManager;->m:I

    return-void
.end method

.method public a(Landroid/view/View;I)V
    .locals 3

    if-ltz p2, :cond_0

    iget v0, p0, Lcom/twitter/android/FilterManager;->n:I

    if-gt p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    new-instance v1, Lcom/twitter/android/gn;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/twitter/android/gn;-><init>(Lcom/twitter/android/gg;)V

    iput-object p1, v1, Lcom/twitter/android/gn;->a:Landroid/view/View;

    iput p2, v1, Lcom/twitter/android/gn;->b:I

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v0, v1}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/twitter/android/FilterManager;->e()V

    goto :goto_0
.end method

.method public a(Z)V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/gj;->cancel(Z)Z

    iput-object v2, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v0}, Lcom/twitter/android/gl;->b()V

    :cond_1
    if-eqz p1, :cond_2

    iput-object v2, p0, Lcom/twitter/android/FilterManager;->e:Lcom/twitter/android/gk;

    :cond_2
    return-void
.end method

.method protected a(ZLcom/twitter/android/gh;Landroid/graphics/Bitmap;)V
    .locals 2

    iget v0, p2, Lcom/twitter/android/gh;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    if-eqz p1, :cond_0

    if-nez p3, :cond_4

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    if-eqz p3, :cond_3

    iput-object p3, p2, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p2}, Lcom/twitter/android/gl;->a(ILcom/twitter/android/gh;)V

    :cond_2
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/FilterManager;->o:Lcom/twitter/android/gj;

    invoke-direct {p0}, Lcom/twitter/android/FilterManager;->e()V

    return-void

    :cond_3
    iget-object v1, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/gl;->b(I)Lcom/twitter/android/gh;

    goto :goto_0

    :cond_4
    iput-object p3, p2, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v0, p2}, Lcom/twitter/android/gl;->a(ILcom/twitter/android/gh;)V

    goto :goto_0
.end method

.method public a(Landroid/net/Uri;Lcom/twitter/android/gk;Lcom/twitter/android/gk;ZLcom/twitter/android/gm;)Z
    .locals 8

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    if-eqz v0, :cond_0

    move v0, v7

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/twitter/android/gi;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/gi;-><init>(Lcom/twitter/android/FilterManager;Landroid/net/Uri;Lcom/twitter/android/gk;Lcom/twitter/android/gk;ZLcom/twitter/android/gm;)V

    iput-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/gi;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/twitter/android/gm;)Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    invoke-virtual {v0, p1}, Lcom/twitter/android/gi;->a(Lcom/twitter/android/gm;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(ZLandroid/net/Uri;)Z
    .locals 2

    const/4 v0, 0x1

    iget v1, p0, Lcom/twitter/android/FilterManager;->d:I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->f:[Landroid/graphics/Bitmap;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-boolean v1, p0, Lcom/twitter/android/FilterManager;->h:Z

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/FilterManager;->g:Landroid/net/Uri;

    if-ne v1, p2, :cond_0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0, p1}, Lcom/twitter/media/filters/Filters;->b(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/FilterManager;->i:Z

    return-void
.end method

.method public c(I)Lcom/twitter/android/gh;
    .locals 3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/twitter/android/gl;->b(I)Lcom/twitter/android/gh;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v2, v1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    goto :goto_0
.end method

.method public c()[Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->f:[Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public d()Lcom/twitter/android/gk;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->e:Lcom/twitter/android/gk;

    return-object v0
.end method

.method public d(I)V
    .locals 3

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    invoke-virtual {v0, v1}, Ljava/util/LinkedHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/gh;

    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/twitter/android/gh;->c:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v2, v1, v0}, Lcom/twitter/android/gl;->a(ILcom/twitter/android/gh;)V

    :cond_0
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    const/4 v3, 0x1

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0, v3}, Lcom/twitter/android/FilterManager;->setRetainInstance(Z)V

    invoke-virtual {p0}, Lcom/twitter/android/FilterManager;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->a:Landroid/content/Context;

    const-string/jumbo v1, "activity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    invoke-virtual {v0}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v0

    :goto_0
    new-instance v1, Ljava/util/LinkedHashMap;

    invoke-direct {v1}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v1, p0, Lcom/twitter/android/FilterManager;->l:Ljava/util/LinkedHashMap;

    new-instance v1, Lcom/twitter/android/gl;

    const/high16 v2, 0x100000

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x18

    invoke-direct {v1, v0}, Lcom/twitter/android/gl;-><init>(I)V

    iput-object v1, p0, Lcom/twitter/android/FilterManager;->k:Lcom/twitter/android/gl;

    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    iput v3, p0, Lcom/twitter/android/FilterManager;->m:I

    iput-boolean v3, p0, Lcom/twitter/android/FilterManager;->i:Z

    return-void

    :cond_1
    const/16 v0, 0x10

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/twitter/android/FilterManager;->a(Z)V

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/gi;->cancel(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->j:Lcom/twitter/android/gi;

    invoke-virtual {v0}, Lcom/twitter/android/gi;->getStatus()Landroid/os/AsyncTask$Status;

    move-result-object v0

    sget-object v1, Landroid/os/AsyncTask$Status;->RUNNING:Landroid/os/AsyncTask$Status;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->c:Lcom/twitter/media/filters/Filters;

    invoke-virtual {v0}, Lcom/twitter/media/filters/Filters;->a()V

    :cond_1
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    return-void
.end method

.method public onStop()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/FilterManager;->b:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    return-void
.end method
