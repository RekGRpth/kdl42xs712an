.class public Lcom/twitter/android/MainActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"

# interfaces
.implements Landroid/accounts/OnAccountsUpdateListener;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/twitter/android/f;
.implements Lcom/twitter/android/util/ab;
.implements Lcom/twitter/android/widget/ce;
.implements Lcom/twitter/library/client/am;
.implements Lcom/twitter/library/util/ar;


# static fields
.field public static final a:Landroid/net/Uri;

.field public static final b:Landroid/net/Uri;

.field public static final c:Landroid/net/Uri;

.field public static final d:Landroid/net/Uri;

.field public static final e:Landroid/net/Uri;

.field public static final f:Landroid/net/Uri;

.field public static final g:Landroid/net/Uri;

.field private static n:Ljava/util/Map;

.field private static o:Ljava/util/ArrayList;

.field private static final p:Z

.field private static final q:I

.field private static final r:Ljava/util/List;

.field private static final s:Ljava/util/List;

.field private static t:I

.field private static u:I


# instance fields
.field private A:Lcom/twitter/library/client/f;

.field private B:Z

.field private C:Lcom/twitter/android/kr;

.field private E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private F:Lcom/twitter/library/client/j;

.field private G:Z

.field private H:Z

.field private I:I

.field private J:Lcom/mobeta/android/dslv/DragSortListView;

.field private K:Lcom/twitter/android/ki;

.field private L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

.field private M:Lcom/twitter/android/kf;

.field private N:Landroid/view/View;

.field private O:Lcom/twitter/internal/android/widget/HorizontalListView;

.field private P:Lcom/twitter/android/km;

.field private Q:Landroid/widget/ImageView;

.field private R:Landroid/widget/TextView;

.field private S:Landroid/widget/TextView;

.field private T:Landroid/view/View;

.field private U:Ljava/util/ArrayList;

.field private V:J

.field private W:Ljava/util/Collection;

.field private X:Lcom/twitter/library/util/ar;

.field private Y:Lcom/twitter/android/util/aa;

.field private Z:Lcom/twitter/library/client/al;

.field private aa:Lhn;

.field private ab:Lhn;

.field private ac:Landroid/graphics/drawable/BitmapDrawable;

.field private ad:Lcom/twitter/internal/android/widget/DockLayout;

.field private ae:Z

.field private af:Ljava/lang/Runnable;

.field private ag:Lcom/twitter/library/service/a;

.field private ah:Lcom/twitter/android/kl;

.field private ai:Lcom/twitter/android/client/aw;

.field private aj:Z

.field h:Lcom/twitter/android/fe;

.field i:Lcom/twitter/android/kd;

.field j:Ljava/lang/String;

.field k:I

.field l:I

.field m:Landroid/support/v4/view/ViewPager;

.field private final v:Lcom/twitter/android/kc;

.field private w:J

.field private x:Lcom/twitter/android/e;

.field private y:Landroid/content/SharedPreferences;

.field private z:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "twitter://timeline/home"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://timeline/act"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->b:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://timeline/disco"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->c:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://timeline/con"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->d:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://timeline/me"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->e:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://timeline/experiment"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->f:Landroid/net/Uri;

    const-string/jumbo v0, "twitter://timeline/favorite_people"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->g:Landroid/net/Uri;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/twitter/android/MainActivity;->n:Ljava/util/Map;

    sget-object v0, Lcom/twitter/android/MainActivity;->n:Ljava/util/Map;

    const-string/jumbo v3, "discover"

    new-instance v4, Lcom/twitter/android/ey;

    invoke-direct {v4}, Lcom/twitter/android/ey;-><init>()V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/MainActivity;->n:Ljava/util/Map;

    const-string/jumbo v3, "activity"

    new-instance v4, Lcom/twitter/android/z;

    invoke-direct {v4}, Lcom/twitter/android/z;-><init>()V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/android/MainActivity;->n:Ljava/util/Map;

    const-string/jumbo v3, "favorite_people"

    new-instance v4, Lcom/twitter/android/fy;

    invoke-direct {v4}, Lcom/twitter/android/fy;-><init>()V

    invoke-interface {v0, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    sput-object v0, Lcom/twitter/android/MainActivity;->o:Ljava/util/ArrayList;

    sget-object v0, Lcom/twitter/android/MainActivity;->o:Ljava/util/ArrayList;

    const-string/jumbo v3, "twitter://experiment"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/MainActivity;->o:Ljava/util/ArrayList;

    const-string/jumbo v3, "twitter://favorite_people"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/MainActivity;->o:Ljava/util/ArrayList;

    const-string/jumbo v3, "twitter://discover"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    sget-object v0, Lcom/twitter/android/MainActivity;->o:Ljava/util/ArrayList;

    const-string/jumbo v3, "twitter://activity"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/client/App;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    sput-boolean v0, Lcom/twitter/android/MainActivity;->p:Z

    sget-boolean v0, Lcom/twitter/android/MainActivity;->p:Z

    if-eqz v0, :cond_2

    const/16 v0, 0xf0

    :goto_1
    sput v0, Lcom/twitter/android/MainActivity;->q:I

    sput v1, Lcom/twitter/android/MainActivity;->t:I

    sput v1, Lcom/twitter/android/MainActivity;->u:I

    new-array v0, v7, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v3, 0xc

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v2

    const/4 v3, 0x7

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->r:Ljava/util/List;

    new-array v0, v7, [Ljava/lang/Integer;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v5

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v6

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/MainActivity;->s:Ljava/util/List;

    return-void

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    new-instance v0, Lcom/twitter/android/kc;

    invoke-direct {v0, p0}, Lcom/twitter/android/kc;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->v:Lcom/twitter/android/kc;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/twitter/android/MainActivity;->V:J

    return-void
.end method

.method static synthetic A(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HorizontalListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->O:Lcom/twitter/internal/android/widget/HorizontalListView;

    return-object v0
.end method

.method static synthetic B(Lcom/twitter/android/MainActivity;)Lcom/mobeta/android/dslv/DragSortListView;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->J:Lcom/mobeta/android/dslv/DragSortListView;

    return-object v0
.end method

.method static synthetic C(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/HiddenDrawerLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    return-object v0
.end method

.method static synthetic D(Lcom/twitter/android/MainActivity;)J
    .locals 2

    iget-wide v0, p0, Lcom/twitter/android/MainActivity;->V:J

    return-wide v0
.end method

.method static synthetic E(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic F(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic G(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic H(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic I(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;J)J
    .locals 0

    iput-wide p1, p0, Lcom/twitter/android/MainActivity;->V:J

    return-wide p1
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Landroid/graphics/drawable/BitmapDrawable;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    return-object p1
.end method

.method public static a(Landroid/content/Context;)Landroid/os/Bundle;
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "shim_size"

    invoke-static {p0}, Lcom/twitter/android/MainActivity;->b(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/TaskStackBuilder;
    .locals 3

    invoke-static {p0}, Landroid/support/v4/app/TaskStackBuilder;->create(Landroid/content/Context;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/MainActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "sb_account_name"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kr;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Landroid/os/Bundle;)Lhb;
    .locals 1

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->c(Landroid/os/Bundle;)Lhb;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Lcom/twitter/internal/android/service/a;)Ljava/lang/String;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/internal/android/service/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(I)V
    .locals 8

    const/4 v2, 0x0

    const/4 v1, 0x1

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "shim_size"

    iget v4, p0, Lcom/twitter/android/MainActivity;->I:I

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :cond_0
    packed-switch p1, :pswitch_data_0

    :cond_1
    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    const-string/jumbo v0, "type"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "empty_title"

    const v2, 0x7f0f0153    # com.twitter.android.R.string.empty_timeline

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "empty_desc"

    const v2, 0x7f0f0154    # com.twitter.android.R.string.empty_timeline_desc

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v0, "prompt_host"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "auto_ref"

    sget v1, Lcom/twitter/android/MainActivity;->q:I

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v1, Lhd;

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/HomeTimelineFragment;

    invoke-direct {v1, v2, v4}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v1, v3}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v1

    const v2, 0x7f0f049a    # com.twitter.android.R.string.tab_title_home

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v1

    const v2, 0x7f0201e9    # com.twitter.android.R.drawable.ic_perch_home_default

    invoke-virtual {v1, v2}, Lhd;->a(I)Lhd;

    move-result-object v1

    const v2, 0x7f020254    # com.twitter.android.R.drawable.ic_tab_home

    invoke-virtual {v1, v2}, Lhd;->b(I)Lhd;

    move-result-object v1

    const-string/jumbo v2, "home"

    invoke-virtual {v1, v2}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v1

    invoke-virtual {v1}, Lhd;->a()Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kf;->c(Lhb;)I

    goto :goto_0

    :pswitch_2
    const-string/jumbo v0, "activity_type"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v2, Lhd;

    sget-object v4, Lcom/twitter/android/MainActivity;->b:Landroid/net/Uri;

    const-class v5, Lcom/twitter/android/ActivityFragment;

    invoke-direct {v2, v4, v5}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v2, v3}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v2

    const v3, 0x7f0f000d    # com.twitter.android.R.string.activity

    invoke-virtual {p0, v3}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v2

    const v3, 0x7f0201e6    # com.twitter.android.R.drawable.ic_perch_activity_default

    invoke-virtual {v2, v3}, Lhd;->a(I)Lhd;

    move-result-object v2

    const-string/jumbo v3, "network_activity"

    invoke-virtual {v2, v3}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v2

    invoke-virtual {v2}, Lhd;->a()Lhb;

    move-result-object v2

    new-instance v3, Landroid/content/Intent;

    const-class v4, Lcom/twitter/android/ActivitiesActivity;

    invoke-direct {v3, p0, v4}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v4, "activity_type"

    invoke-virtual {v3, v4, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Lcom/twitter/android/kf;->a(Lhb;Landroid/content/Intent;)I

    goto/16 :goto_0

    :pswitch_3
    const-string/jumbo v0, "legacy"

    iget-boolean v1, p0, Lcom/twitter/android/MainActivity;->ae:Z

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v1, Lhd;

    sget-object v2, Lcom/twitter/android/MainActivity;->c:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/DiscoverFragment;

    invoke-direct {v1, v2, v4}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v1, v3}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v1

    const v2, 0x7f0f0112    # com.twitter.android.R.string.discover

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v1

    const v2, 0x7f0201ed    # com.twitter.android.R.drawable.ic_perch_toptweets_default

    invoke-virtual {v1, v2}, Lhd;->a(I)Lhd;

    move-result-object v1

    const v2, 0x7f020252    # com.twitter.android.R.drawable.ic_tab_discover

    invoke-virtual {v1, v2}, Lhd;->b(I)Lhd;

    move-result-object v1

    const-string/jumbo v2, "discover"

    invoke-virtual {v1, v2}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v1

    invoke-virtual {v1}, Lhd;->a()Lhb;

    move-result-object v1

    new-instance v2, Landroid/content/Intent;

    const-class v3, Lcom/twitter/android/DiscoverActivity;

    invoke-direct {v2, p0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/kf;->a(Lhb;Landroid/content/Intent;)I

    goto/16 :goto_0

    :pswitch_4
    invoke-static {}, Lgl;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "type"

    const/16 v1, 0x16

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    new-instance v0, Lhd;

    sget-object v1, Lcom/twitter/android/MainActivity;->g:Landroid/net/Uri;

    const-class v2, Lcom/twitter/android/FavoritePeopleTimelineFragment;

    invoke-direct {v0, v1, v2}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v0, v3}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v1, 0x7f0f016c    # com.twitter.android.R.string.favorite_people

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const-string/jumbo v1, "favorite_people"

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v1, v0}, Lcom/twitter/android/kf;->a(Lhb;)I

    goto/16 :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v1, Lcom/twitter/android/ke;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/twitter/android/ke;-><init>(I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/kf;->b(Lcom/twitter/android/kg;)V

    goto/16 :goto_0

    :pswitch_6
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->H:Z

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-static {v3, p0, v0}, Lcom/twitter/android/MentionsTimelineActivity;->a(Landroid/os/Bundle;Landroid/content/Context;Lcom/twitter/library/client/aa;)Ljava/lang/Class;

    move-result-object v0

    :goto_1
    iget-object v5, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v6, Lhd;

    sget-object v7, Lcom/twitter/android/MainActivity;->d:Landroid/net/Uri;

    invoke-direct {v6, v7, v0}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v6, v3}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v3, 0x7f0f0498    # com.twitter.android.R.string.tab_title_connect

    invoke-virtual {p0, v3}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const-string/jumbo v3, "connect"

    invoke-virtual {v0, v3}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    const v3, 0x7f02024f    # com.twitter.android.R.drawable.ic_tab_connect

    invoke-virtual {v0, v3}, Lhd;->b(I)Lhd;

    move-result-object v3

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v0, :cond_4

    if-eqz v4, :cond_2

    iget-boolean v0, v4, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-nez v0, :cond_4

    :cond_2
    move v0, v1

    :goto_2
    invoke-virtual {v3, v0}, Lhd;->a(Z)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ActivitiesActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v5, v0, v1}, Lcom/twitter/android/kf;->a(Lhb;Landroid/content/Intent;)I

    goto/16 :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-boolean v5, p0, Lcom/twitter/android/MainActivity;->H:Z

    invoke-static {v3, p0, v0, v5}, Lcom/twitter/android/NotificationActivity;->a(Landroid/os/Bundle;Landroid/content/Context;Lcom/twitter/library/client/aa;Z)Ljava/lang/Class;

    move-result-object v0

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_2

    :pswitch_7
    const-string/jumbo v0, "is_me"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "refresh"

    invoke-virtual {v3, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v1, Lhd;

    sget-object v2, Lcom/twitter/android/MainActivity;->e:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/ProfileFragment;

    invoke-direct {v1, v2, v4}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v1, v3}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v1

    const v2, 0x7f0f0497    # com.twitter.android.R.string.tab_title_account

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v1

    const v2, 0x7f020256    # com.twitter.android.R.drawable.ic_tab_me

    invoke-virtual {v1, v2}, Lhd;->b(I)Lhd;

    move-result-object v1

    const-string/jumbo v2, "me"

    invoke-virtual {v1, v2}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/MainActivity;->ae:Z

    invoke-virtual {v1, v2}, Lhd;->a(Z)Lhd;

    move-result-object v1

    invoke-virtual {v1}, Lhd;->a()Lhb;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kf;->a(Lhb;)I

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public static a(Landroid/app/Activity;Landroid/net/Uri;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/MainActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    if-eqz p1, :cond_0

    const-string/jumbo v0, "page"

    invoke-virtual {v1, v0, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    :cond_0
    instance-of v0, p0, Lcom/twitter/android/client/BaseFragmentActivity;

    if-eqz v0, :cond_1

    move-object v0, p0

    check-cast v0, Lcom/twitter/android/client/BaseFragmentActivity;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/BaseFragmentActivity;->d(Landroid/content/Intent;)V

    :goto_0
    invoke-virtual {p0}, Landroid/app/Activity;->finish()V

    return-void

    :cond_1
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;)V
    .locals 4

    const-string/jumbo v0, "sb_account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "sb_account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    const-string/jumbo v1, "sb_account_name"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->d(Ljava/lang/String;)V

    :cond_0
    const-string/jumbo v0, "sb_account_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->removeExtra(Ljava/lang/String;)V

    :cond_1
    const-string/jumbo v0, "twitter"

    invoke-virtual {p1}, Landroid/content/Intent;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "timeline"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    sget-object v0, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;)V

    :goto_0
    return-void

    :cond_2
    const-string/jumbo v1, "mentions"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/twitter/android/MainActivity;->d:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_4
    const-string/jumbo v0, "page"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->y:Landroid/content/SharedPreferences;

    const-string/jumbo v1, "tag"

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;)V

    goto :goto_0

    :cond_5
    const-string/jumbo v1, "ref_event"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v2}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/kr;->a(I)Lhb;

    move-result-object v1

    iget-object v1, v1, Lhb;->b:Landroid/os/Bundle;

    const-string/jumbo v2, "ref_event"

    const-string/jumbo v3, "ref_event"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_6
    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    invoke-static {p1, p2}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/support/v4/app/TaskStackBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/support/v4/app/TaskStackBuilder;->addNextIntent(Landroid/content/Intent;)Landroid/support/v4/app/TaskStackBuilder;

    invoke-virtual {v0}, Landroid/support/v4/app/TaskStackBuilder;->startActivities()V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->b(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->g(Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V
    .locals 0

    invoke-direct/range {p0 .. p6}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->g(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Ljava/lang/String;J)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/twitter/android/MainActivity;->a(Ljava/lang/String;J)V

    return-void
.end method

.method private a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V
    .locals 6

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->i:Lcom/twitter/android/kd;

    const-string/jumbo v2, "taut"

    const-wide/16 v3, 0x0

    invoke-virtual {p2, v2, v3, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->u()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v2, p3, v2

    if-lez v2, :cond_1

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/twitter/android/kd;->hasMessages(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/twitter/android/kd;->removeMessages(I)V

    :cond_0
    const/4 v2, 0x2

    invoke-virtual {v1, v2, p1}, Lcom/twitter/android/kd;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2, p5, p6}, Lcom/twitter/android/kd;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    const-string/jumbo v2, "tatt"

    const-wide/16 v3, 0x0

    invoke-virtual {p2, v2, v3, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->w()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v0, p3, v2

    if-lez v0, :cond_4

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/twitter/android/kd;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/twitter/android/kd;->removeMessages(I)V

    :cond_2
    const/4 v0, 0x3

    invoke-virtual {v1, v0, p1}, Lcom/twitter/android/kd;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0, p5, p6}, Lcom/twitter/android/kd;->sendMessageDelayed(Landroid/os/Message;J)Z

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/twitter/android/kd;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    invoke-virtual {v1, v0}, Lcom/twitter/android/kd;->removeMessages(I)V

    :cond_3
    const/4 v0, 0x4

    invoke-virtual {v1, v0, p1}, Lcom/twitter/android/kd;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0, p5, p6}, Lcom/twitter/android/kd;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_4
    const-string/jumbo v0, "taot"

    const-wide/16 v2, 0x0

    invoke-virtual {p2, v0, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v2

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->w()J

    move-result-wide v4

    add-long/2addr v2, v4

    cmp-long v0, p3, v2

    if-lez v0, :cond_6

    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Lcom/twitter/android/kd;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x5

    invoke-virtual {v1, v0}, Lcom/twitter/android/kd;->removeMessages(I)V

    :cond_5
    const/4 v0, 0x5

    invoke-virtual {v1, v0, p1}, Lcom/twitter/android/kd;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0, p5, p6}, Lcom/twitter/android/kd;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_6
    return-void
.end method

.method private a(Lhn;I)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    invoke-virtual {p1, p2}, Lhn;->f(I)V

    invoke-virtual {p1}, Lhn;->k()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-gtz p2, :cond_1

    const-string/jumbo v0, ""

    :goto_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    :goto_1
    invoke-virtual {p1, v0}, Lhn;->b(Ljava/lang/CharSequence;)Lhn;

    return-void

    :cond_1
    const/high16 v0, 0x7f0e0000    # com.twitter.android.R.plurals.badge_item_count

    new-array v3, v6, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v2, v0, p2, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    const v3, 0x7f0f0009    # com.twitter.android.R.string.accessibility_concatenation_format

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    aput-object v1, v3, v5

    aput-object v0, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private a(Ljava/lang/String;J)V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v3

    const-wide/16 v4, 0x0

    cmp-long v4, p2, v4

    if-lez v4, :cond_2

    invoke-static {v0, p2, p3}, Lcom/twitter/library/service/TimelineHelper;->a(Landroid/content/Context;J)Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-static {v0, p1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-nez v3, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    new-instance v4, Landroid/os/Bundle;

    const/4 v5, 0x5

    invoke-direct {v4, v5}, Landroid/os/Bundle;-><init>(I)V

    const-string/jumbo v5, "activity"

    invoke-virtual {v4, v5, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v3, "discover"

    invoke-virtual {v4, v3, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v3, "home"

    invoke-virtual {v4, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "messages"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v0, "show_notif"

    invoke-virtual {v4, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {p0, p1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    invoke-static {p0, v4, v1, v0}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Landroid/os/Bundle;ZLandroid/accounts/Account;)Z

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/MainActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/MainActivity;->H:Z

    return p1
.end method

.method private static b(Landroid/content/Context;)I
    .locals 2

    invoke-static {p0}, Lgk;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c009d    # com.twitter.android.R.dimen.nav_bar_height

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    goto :goto_0
.end method

.method private b(I)V
    .locals 5

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/twitter/android/MainActivity;->k:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/android/MainActivity;->k:I

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->V()V

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    sget-object v3, Lcom/twitter/android/MainActivity;->d:Landroid/net/Uri;

    iget v4, p0, Lcom/twitter/android/MainActivity;->k:I

    if-nez v0, :cond_2

    :goto_1
    invoke-virtual {p0, v3, v4, v1}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;IZ)V

    :cond_0
    return-void

    :cond_1
    move v0, v2

    goto :goto_0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method static synthetic b(Lcom/twitter/android/MainActivity;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->c(I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/MainActivity;Landroid/os/Bundle;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->d(Landroid/os/Bundle;)V

    return-void
.end method

.method private b(Z)V
    .locals 5

    const/4 v4, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->R:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->S:Landroid/widget/TextView;

    iget-object v3, v1, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v2, 0x1

    invoke-virtual {v0, v2, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    iget-object v2, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->Q:Landroid/widget/ImageView;

    iget-object v1, v1, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_0
    if-eqz p1, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->o()V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    const-string/jumbo v1, ""

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/bn;->a(Ljava/lang/CharSequence;)V

    :cond_1
    iput-object v4, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Q:Landroid/widget/ImageView;

    invoke-virtual {v0, v4}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/MainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->H:Z

    return v0
.end method

.method private c(Landroid/os/Bundle;)Lhb;
    .locals 3

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->H:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-static {p1, p0, v0}, Lcom/twitter/android/RootMentionsTimelineActivity;->a(Landroid/os/Bundle;Landroid/content/Context;Lcom/twitter/library/client/aa;)Ljava/lang/Class;

    move-result-object v0

    :goto_0
    new-instance v1, Lhd;

    sget-object v2, Lcom/twitter/android/MainActivity;->d:Landroid/net/Uri;

    invoke-direct {v1, v2, v0}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v1, p1}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v1, 0x7f0f0498    # com.twitter.android.R.string.tab_title_connect

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const-string/jumbo v1, "connect"

    invoke-virtual {v0, v1}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    const v1, 0x7f02024f    # com.twitter.android.R.drawable.ic_tab_connect

    invoke-virtual {v0, v1}, Lhd;->b(I)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-boolean v1, p0, Lcom/twitter/android/MainActivity;->H:Z

    invoke-static {p1, p0, v0, v1}, Lcom/twitter/android/RootNotificationActivity;->a(Landroid/os/Bundle;Landroid/content/Context;Lcom/twitter/library/client/aa;Z)Ljava/lang/Class;

    move-result-object v0

    goto :goto_0
.end method

.method private c(I)V
    .locals 3

    iget v0, p0, Lcom/twitter/android/MainActivity;->l:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/twitter/android/MainActivity;->l:I

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->V()V

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v0, :cond_0

    sget-object v0, Lcom/twitter/android/MainActivity;->e:Landroid/net/Uri;

    iget v1, p0, Lcom/twitter/android/MainActivity;->l:I

    iget-boolean v2, p0, Lcom/twitter/android/MainActivity;->ae:Z

    invoke-virtual {p0, v0, v1, v2}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;IZ)V

    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/twitter/android/MainActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/kf;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    return-object v0
.end method

.method private d(Landroid/os/Bundle;)V
    .locals 2

    invoke-static {p0}, Lcom/twitter/library/platform/TwitterDataSyncService;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    invoke-static {p0, p1, v0, v1}, Lcom/twitter/library/platform/TwitterDataSyncService;->a(Landroid/content/Context;Landroid/os/Bundle;ZLandroid/accounts/Account;)Z

    :cond_0
    return-void
.end method

.method static synthetic e(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/km;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->P:Lcom/twitter/android/km;

    return-object v0
.end method

.method private e(Landroid/net/Uri;)Z
    .locals 2

    sget-object v0, Lcom/twitter/android/MainActivity;->n:Ljava/util/Map;

    invoke-static {p1}, Lcom/twitter/library/client/al;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhe;

    if-eqz v0, :cond_0

    invoke-interface {v0, p0, p1}, Lhe;->a(Landroid/content/Context;Landroid/net/Uri;)Lhb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v1, v0}, Lcom/twitter/android/kf;->a(Lhb;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/al;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    return-object v0
.end method

.method private f(Ljava/lang/String;)Ljava/util/Collection;
    .locals 4

    new-instance v0, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "hometab"

    invoke-direct {v0, p0, p1, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v1, "page_order"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/twitter/android/MainActivity;->r:Ljava/util/List;

    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/util/StringTokenizer;

    const-string/jumbo v1, ","

    invoke-direct {v2, v0, v1}, Ljava/util/StringTokenizer;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Ljava/util/LinkedHashSet;

    invoke-direct {v1}, Ljava/util/LinkedHashSet;-><init>()V

    :cond_1
    :goto_1
    invoke-virtual {v2}, Ljava/util/StringTokenizer;->hasMoreTokens()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Ljava/util/StringTokenizer;->nextToken()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    sget-object v3, Lcom/twitter/android/MainActivity;->r:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_2
    sget-object v0, Lcom/twitter/android/MainActivity;->r:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashSet;->add(Ljava/lang/Object;)Z

    goto :goto_2

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private f(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->af:Ljava/lang/Runnable;

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/jz;

    invoke-direct {v0, p0}, Lcom/twitter/android/jz;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->af:Ljava/lang/Runnable;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->i:Lcom/twitter/android/kd;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->af:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/android/kd;->removeCallbacks(Ljava/lang/Runnable;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->i:Lcom/twitter/android/kd;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->af:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Lcom/twitter/android/kd;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic g(Lcom/twitter/android/MainActivity;)Lhn;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->aa:Lhn;

    return-object v0
.end method

.method private g(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ab:Lhn;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/android/util/ah;->a(Lcom/twitter/library/api/TwitterUser;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ab:Lhn;

    sget-object v1, Lcom/twitter/android/MainActivity;->d:Landroid/net/Uri;

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ab:Lhn;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    goto :goto_0
.end method

.method private g(Ljava/lang/String;)V
    .locals 5

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v1}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    const/4 v2, 0x6

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v3}, Lcom/twitter/android/kr;->d()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    aput-object p1, v3, v4

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/twitter/library/scribe/ScribeLog;->a(Lcom/twitter/library/scribe/ScribeAssociation;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v3, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v1, v3}, Lcom/twitter/library/scribe/ScribeLog;->e(I)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    return-void
.end method

.method static synthetic h(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/aa;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    return-object v0
.end method

.method static synthetic i(Lcom/twitter/android/MainActivity;)Landroid/graphics/drawable/BitmapDrawable;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    return-object v0
.end method

.method static synthetic j(Lcom/twitter/android/MainActivity;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->U:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic k(Lcom/twitter/android/MainActivity;)Landroid/view/View;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->T:Landroid/view/View;

    return-object v0
.end method

.method private k()Ljava/util/Collection;
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v0, :cond_1

    sget-object v0, Lcom/twitter/android/MainActivity;->s:Ljava/util/List;

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->W:Ljava/util/Collection;

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->W:Ljava/util/Collection;

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->W:Ljava/util/Collection;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->f(Ljava/lang/String;)Ljava/util/Collection;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->W:Ljava/util/Collection;

    goto :goto_0
.end method

.method static synthetic l(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private l()Z
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic m(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/platform/LocationProducer;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v0

    return-object v0
.end method

.method private m()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->b(Z)V

    return-void
.end method

.method static synthetic n(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private n()V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v0}, Lcom/twitter/android/kf;->a()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "type"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "empty_title"

    const v2, 0x7f0f0153    # com.twitter.android.R.string.empty_timeline

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "empty_desc"

    const v2, 0x7f0f0154    # com.twitter.android.R.string.empty_timeline_desc

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "shim_size"

    iget v2, p0, Lcom/twitter/android/MainActivity;->I:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string/jumbo v1, "prompt_host"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string/jumbo v1, "auto_ref"

    sget v2, Lcom/twitter/android/MainActivity;->q:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v2, Lhd;

    sget-object v3, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    const-class v4, Lcom/twitter/android/HomeTimelineFragment;

    invoke-direct {v2, v3, v4}, Lhd;-><init>(Landroid/net/Uri;Ljava/lang/Class;)V

    invoke-virtual {v2, v0}, Lhd;->a(Landroid/os/Bundle;)Lhd;

    move-result-object v0

    const v2, 0x7f0f049a    # com.twitter.android.R.string.tab_title_home

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lhd;->a(Ljava/lang/CharSequence;)Lhd;

    move-result-object v0

    const v2, 0x7f0201e9    # com.twitter.android.R.drawable.ic_perch_home_default

    invoke-virtual {v0, v2}, Lhd;->a(I)Lhd;

    move-result-object v0

    const v2, 0x7f020254    # com.twitter.android.R.drawable.ic_tab_home

    invoke-virtual {v0, v2}, Lhd;->b(I)Lhd;

    move-result-object v0

    const-string/jumbo v2, "home"

    invoke-virtual {v0, v2}, Lhd;->a(Ljava/lang/String;)Lhd;

    move-result-object v0

    invoke-virtual {v0}, Lhd;->a()Lhb;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/kf;->c(Lhb;)I

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    sget-object v1, Lcom/twitter/android/MainActivity;->o:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/al;->a(Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->e(Landroid/net/Uri;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    invoke-virtual {v2, v0}, Lcom/twitter/library/client/al;->a(Landroid/net/Uri;)V

    goto :goto_1

    :cond_2
    return-void
.end method

.method static synthetic o(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method private o()V
    .locals 2

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->n()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v0}, Lcom/twitter/android/kf;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v0}, Lcom/twitter/android/kr;->e()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->P:Lcom/twitter/android/km;

    invoke-virtual {v0}, Lcom/twitter/android/km;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_0
    return-void
.end method

.method private p()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v0}, Lcom/twitter/android/kf;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v0}, Lcom/twitter/android/kr;->e()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->P:Lcom/twitter/android/km;

    invoke-virtual {v0}, Lcom/twitter/android/km;->notifyDataSetChanged()V

    return-void
.end method

.method static synthetic p(Lcom/twitter/android/MainActivity;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->m()V

    return-void
.end method

.method static synthetic q(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic r(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic s(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic t(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic u(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic v(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/c;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    return-object v0
.end method

.method static synthetic w(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/z;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->M()Lcom/twitter/android/client/z;

    move-result-object v0

    return-object v0
.end method

.method static synthetic x(Lcom/twitter/android/MainActivity;)Lcom/twitter/android/client/bn;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v0

    return-object v0
.end method

.method static synthetic y(Lcom/twitter/android/MainActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    return-object v0
.end method

.method static synthetic z(Lcom/twitter/android/MainActivity;)Lcom/twitter/internal/android/widget/DockLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ad:Lcom/twitter/internal/android/widget/DockLayout;

    return-object v0
.end method


# virtual methods
.method public a(Lhb;)Lcom/twitter/android/client/BaseListFragment;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {p1, v0}, Lhb;->a(Landroid/support/v4/app/FragmentManager;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/client/BaseListFragment;

    return-object v0
.end method

.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 4

    const/4 v3, 0x1

    new-instance v1, Lcom/twitter/android/client/z;

    invoke-direct {v1, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v0, 0x2

    invoke-static {}, Lgu;->a()Z

    move-result v2

    if-nez v2, :cond_0

    const/16 v0, 0x12

    :cond_0
    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->c(I)V

    invoke-virtual {v1, v3}, Lcom/twitter/android/client/z;->d(Z)V

    invoke-static {}, Lgm;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0300a0    # com.twitter.android.R.layout.legacy_main_activity

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->d(I)V

    :goto_0
    invoke-static {p0}, Lgk;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v0

    const v2, 0x7f100036    # com.twitter.android.R.style.CombinedNavigationBar

    invoke-virtual {v0, v2, v3}, Landroid/content/res/Resources$Theme;->applyStyle(IZ)V

    :cond_1
    return-object v1

    :cond_2
    const v0, 0x7f0300b4    # com.twitter.android.R.layout.main_activity

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/z;->d(I)V

    goto :goto_0
.end method

.method public a(Landroid/content/DialogInterface;II)V
    .locals 2

    const/4 v0, -0x1

    if-ne v0, p3, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/SettingsActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    goto :goto_0
.end method

.method public a(Landroid/graphics/Bitmap;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setFilterBitmap(Z)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->N:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->h()V

    goto :goto_0
.end method

.method a(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v0, p1}, Lcom/twitter/android/kr;->a(Landroid/net/Uri;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    invoke-static {p1}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/net/Uri;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/MainActivity;->b(Landroid/net/Uri;)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    :cond_0
    return-void
.end method

.method a(Landroid/net/Uri;I)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v1, v0, Lhb;->k:I

    add-int/2addr v1, p2

    iput v1, v0, Lhb;->k:I

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->f(Landroid/net/Uri;)V

    :cond_1
    return-void
.end method

.method a(Landroid/net/Uri;IZ)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iget-object v2, v0, Lhb;->c:Landroid/net/Uri;

    invoke-virtual {p1, v2}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, v0, Lhb;->k:I

    if-eq v2, p2, :cond_0

    iput p2, v0, Lhb;->k:I

    iput-boolean p3, v0, Lhb;->j:Z

    iget-object v0, v0, Lhb;->c:Landroid/net/Uri;

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->f(Landroid/net/Uri;)V

    :cond_1
    return-void
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v3

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/android/MainActivity;->b(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/MainActivity;->I:I

    invoke-virtual {p0, v6}, Lcom/twitter/android/MainActivity;->c(Landroid/content/Intent;)V

    invoke-static {}, Lgm;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    invoke-static {p0}, Lcom/twitter/library/client/al;->a(Landroid/content/Context;)Lcom/twitter/library/client/al;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    invoke-virtual {v0, p0}, Lcom/twitter/library/client/al;->a(Lcom/twitter/library/client/am;)V

    invoke-static {p0}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->ai:Lcom/twitter/android/client/aw;

    new-instance v0, Lcom/twitter/android/kd;

    invoke-direct {v0, p0}, Lcom/twitter/android/kd;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->i:Lcom/twitter/android/kd;

    invoke-virtual {v3, p0}, Lcom/twitter/android/client/c;->e(Landroid/content/Context;)V

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    const v2, 0x7f0c0079    # com.twitter.android.R.dimen.home_pager_margin

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMargin(I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    const v1, 0x7f0b0063    # com.twitter.android.R.color.list_margin_bg

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPager;->setPageMarginDrawable(I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v0, v8}, Landroid/support/v4/view/ViewPager;->setOffscreenPageLimit(I)V

    const v0, 0x7f0901ad    # com.twitter.android.R.id.drawer_layout

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const v1, 0x7f09008d    # com.twitter.android.R.id.account_row

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x7f09006b    # com.twitter.android.R.id.settings

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const v1, 0x7f0900e1    # com.twitter.android.R.id.dock

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/DockLayout;

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->ad:Lcom/twitter/internal/android/widget/DockLayout;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ad:Lcom/twitter/internal/android/widget/DockLayout;

    if-eqz v1, :cond_0

    new-instance v1, Lcom/twitter/android/fe;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->ad:Lcom/twitter/internal/android/widget/DockLayout;

    invoke-direct {v1, v2}, Lcom/twitter/android/fe;-><init>(Lcom/twitter/internal/android/widget/DockLayout;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->h:Lcom/twitter/android/fe;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ad:Lcom/twitter/internal/android/widget/DockLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ad:Lcom/twitter/internal/android/widget/DockLayout;

    new-instance v2, Lcom/twitter/android/jt;

    invoke-direct {v2, p0}, Lcom/twitter/android/jt;-><init>(Lcom/twitter/android/MainActivity;)V

    invoke-virtual {v1, v2}, Lcom/twitter/internal/android/widget/DockLayout;->setDockListener(Lcom/twitter/internal/android/widget/h;)V

    :cond_0
    new-instance v1, Lcom/twitter/android/e;

    const v2, 0x7f0901b0    # com.twitter.android.R.id.main_layout

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, v2, p0}, Lcom/twitter/android/e;-><init>(Landroid/content/Context;Landroid/view/View;Lcom/twitter/android/f;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->x:Lcom/twitter/android/e;

    invoke-virtual {p0, v5}, Lcom/twitter/android/MainActivity;->getPreferences(I)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->y:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->y:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "version_code"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    sput v1, Lcom/twitter/android/MainActivity;->u:I

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1, p0, v6, v7}, Landroid/accounts/AccountManager;->addOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;Landroid/os/Handler;Z)V

    iput-boolean v7, p0, Lcom/twitter/android/MainActivity;->G:Z

    new-instance v1, Lcom/twitter/android/kj;

    invoke-direct {v1, p0}, Lcom/twitter/android/kj;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->F:Lcom/twitter/library/client/j;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->F:Lcom/twitter/library/client/j;

    invoke-virtual {v3, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    new-instance v1, Lcom/twitter/android/kl;

    invoke-direct {v1, p0}, Lcom/twitter/android/kl;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->ah:Lcom/twitter/android/kl;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->ah:Lcom/twitter/android/kl;

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->a(Lcom/twitter/library/client/z;)V

    new-instance v1, Lcom/twitter/android/kk;

    invoke-direct {v1, p0}, Lcom/twitter/android/kk;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->ag:Lcom/twitter/library/service/a;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ag:Lcom/twitter/library/service/a;

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/library/service/a;)V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iput-object v4, p0, Lcom/twitter/android/MainActivity;->U:Ljava/util/ArrayList;

    new-instance v1, Lcom/twitter/android/kf;

    invoke-direct {v1, p0, v4}, Lcom/twitter/android/kf;-><init>(Landroid/app/Activity;Ljava/util/ArrayList;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    new-instance v1, Lcom/twitter/android/ju;

    invoke-direct {v1, p0}, Lcom/twitter/android/ju;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->b(Ljava/lang/String;)V

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->z:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->z:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "show_int_dlg"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v8}, Lcom/twitter/android/widget/PromptDialogFragment;->b(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0106    # com.twitter.android.R.string.dialog_notifications_tab

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->e(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f03c6    # com.twitter.android.R.string.settings

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->g(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    const v2, 0x7f0f0057    # com.twitter.android.R.string.button_action_dismiss

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->i(I)Lcom/twitter/android/widget/PromptDialogFragment;

    move-result-object v1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/android/widget/PromptDialogFragment;->a(Landroid/support/v4/app/FragmentManager;)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->z:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string/jumbo v2, "show_int_dlg"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_1
    const v1, 0x7f0901ae    # com.twitter.android.R.id.drawer_background

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->N:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->h()V

    if-nez p1, :cond_2

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->j()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->d(Ljava/lang/String;)V

    :cond_2
    iput-boolean v5, p0, Lcom/twitter/android/MainActivity;->aj:Z

    iget-boolean v1, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->k()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/twitter/android/MainActivity;->a(I)V

    goto :goto_0

    :cond_3
    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->n()V

    :cond_4
    new-instance v1, Lcom/twitter/android/kr;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-direct {v1, p0, p0, v4, v2}, Lcom/twitter/android/kr;-><init>(Lcom/twitter/android/MainActivity;Lcom/twitter/android/MainActivity;Ljava/util/ArrayList;Landroid/support/v4/view/ViewPager;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v1, v2}, Landroid/support/v4/view/ViewPager;->setAdapter(Landroid/support/v4/view/PagerAdapter;)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v1, v7}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->setDrawerOpenable(Z)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v1}, Lcom/twitter/android/kf;->b()Lcom/twitter/android/kg;

    move-result-object v1

    invoke-virtual {v1, p0, v6, v6}, Lcom/twitter/android/kg;->a(Landroid/content/Context;Landroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->T:Landroid/view/View;

    const v1, 0x7f0901af    # com.twitter.android.R.id.drawer_list

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/mobeta/android/dslv/DragSortListView;

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->J:Lcom/mobeta/android/dslv/DragSortListView;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->J:Lcom/mobeta/android/dslv/DragSortListView;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->T:Landroid/view/View;

    invoke-virtual {v1, v2}, Lcom/mobeta/android/dslv/DragSortListView;->addHeaderView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->J:Lcom/mobeta/android/dslv/DragSortListView;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v1, v2}, Lcom/mobeta/android/dslv/DragSortListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->J:Lcom/mobeta/android/dslv/DragSortListView;

    new-instance v2, Lcom/twitter/android/kh;

    invoke-direct {v2, p0, v6}, Lcom/twitter/android/kh;-><init>(Lcom/twitter/android/MainActivity;Lcom/twitter/android/jt;)V

    invoke-virtual {v1, v2}, Lcom/mobeta/android/dslv/DragSortListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->J:Lcom/mobeta/android/dslv/DragSortListView;

    new-instance v2, Lcom/twitter/android/jv;

    invoke-direct {v2, p0}, Lcom/twitter/android/jv;-><init>(Lcom/twitter/android/MainActivity;)V

    invoke-virtual {v1, v2}, Lcom/mobeta/android/dslv/DragSortListView;->setDropListener(Lcom/mobeta/android/dslv/m;)V

    new-instance v1, Lcom/twitter/android/jw;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/jw;-><init>(Lcom/twitter/android/MainActivity;Lcom/twitter/internal/android/widget/HiddenDrawerLayout;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->K:Lcom/twitter/android/ki;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->K:Lcom/twitter/android/ki;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->setDrawerListener(Lcom/twitter/internal/android/widget/n;)V

    const v0, 0x7f0900f0    # com.twitter.android.R.id.account_image

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->Q:Landroid/widget/ImageView;

    const v0, 0x7f0900f1    # com.twitter.android.R.id.user_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->R:Landroid/widget/TextView;

    const v0, 0x7f0900f2    # com.twitter.android.R.id.account_name

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->S:Landroid/widget/TextView;

    invoke-direct {p0, v5}, Lcom/twitter/android/MainActivity;->b(Z)V

    const v0, 0x7f090156    # com.twitter.android.R.id.tabs

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/internal/android/widget/HorizontalListView;

    const v1, 0x7f090290    # com.twitter.android.R.id.toolbar_tabs

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/twitter/internal/android/widget/HorizontalListView;

    if-eqz v1, :cond_5

    invoke-static {p0}, Lgk;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_a

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HorizontalListView;->getParent()Landroid/view/ViewParent;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    invoke-virtual {v2, v0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    move-object v0, v1

    :cond_5
    :goto_1
    new-instance v1, Lcom/twitter/android/km;

    iget-boolean v2, p0, Lcom/twitter/android/MainActivity;->ae:Z

    invoke-direct {v1, v2, v4}, Lcom/twitter/android/km;-><init>(ZLjava/util/ArrayList;)V

    iput-object v1, p0, Lcom/twitter/android/MainActivity;->P:Lcom/twitter/android/km;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->P:Lcom/twitter/android/km;

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setAdapter(Landroid/widget/ListAdapter;)V

    new-instance v1, Lcom/twitter/android/jx;

    invoke-direct {v1, p0}, Lcom/twitter/android/jx;-><init>(Lcom/twitter/android/MainActivity;)V

    invoke-virtual {v0, v1}, Lcom/twitter/internal/android/widget/HorizontalListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->O:Lcom/twitter/internal/android/widget/HorizontalListView;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Intent;)V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->f()V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->d()V

    if-nez p1, :cond_6

    invoke-virtual {v3, v6}, Lcom/twitter/android/client/c;->b([I)V

    invoke-static {}, Lcom/twitter/library/client/App;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->n()Ljava/lang/String;

    :cond_6
    invoke-static {p0}, Lcom/google/android/gcm/a;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_b

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->e(Landroid/content/Context;)V

    :cond_7
    :goto_2
    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-nez v0, :cond_8

    new-instance v0, Lcom/twitter/android/jy;

    invoke-direct {v0, p0}, Lcom/twitter/android/jy;-><init>(Lcom/twitter/android/MainActivity;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->X:Lcom/twitter/library/util/ar;

    :cond_8
    invoke-static {}, Lcom/twitter/library/util/Util;->f()Z

    move-result v0

    if-eqz v0, :cond_9

    if-nez p1, :cond_9

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v3, "app::::explorebytouch_enabled"

    aput-object v3, v2, v5

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/library/scribe/ScribeService;->a(Landroid/content/Context;Lcom/twitter/library/scribe/ScribeLog;)V

    :cond_9
    return-void

    :cond_a
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->R()Lcom/twitter/internal/android/widget/ToolBar;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/twitter/internal/android/widget/ToolBar;->setCustomView(Landroid/view/View;)V

    goto :goto_1

    :cond_b
    invoke-static {p0}, Lcom/google/android/gcm/a;->j(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v3, v0}, Lcom/twitter/android/client/c;->i(Ljava/lang/String;)Ljava/lang/String;

    goto :goto_2
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/util/ae;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->Q:Landroid/widget/ImageView;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->f()V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/twitter/library/client/aa;->d(Ljava/lang/String;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Z)V

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->m()V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    invoke-direct {p0, p1, v0, v1}, Lcom/twitter/android/MainActivity;->a(Ljava/lang/String;J)V

    return-void
.end method

.method protected a(Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Y:Lcom/twitter/android/util/aa;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Y:Lcom/twitter/android/util/aa;

    invoke-virtual {v0}, Lcom/twitter/android/util/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Y:Lcom/twitter/android/util/aa;

    invoke-virtual {v0, p1}, Lcom/twitter/android/util/aa;->a(Z)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->h()V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 6

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v3

    const v0, 0x7f090315    # com.twitter.android.R.id.timelines

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-static {}, Lgj;->a()Z

    move-result v4

    invoke-virtual {v0, v4}, Lhn;->b(Z)Lhn;

    const v0, 0x7f090311    # com.twitter.android.R.id.toolbar_dms

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iget v4, p0, Lcom/twitter/android/MainActivity;->l:I

    invoke-direct {p0, v0, v4}, Lcom/twitter/android/MainActivity;->a(Lhn;I)V

    const v0, 0x7f090312    # com.twitter.android.R.id.toolbar_notif

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v4

    if-eqz v3, :cond_3

    iget-boolean v0, v3, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Lhn;->e(I)V

    iget v0, p0, Lcom/twitter/android/MainActivity;->k:I

    invoke-direct {p0, v4, v0}, Lcom/twitter/android/MainActivity;->a(Lhn;I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->aa:Lhn;

    if-eqz v3, :cond_0

    iget-object v4, v3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_4

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lhn;->a(Ljava/lang/CharSequence;)Lhn;

    :goto_1
    iget-object v4, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    if-nez v4, :cond_6

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v4

    iget-object v4, v4, Lcom/twitter/android/client/c;->a:Lcom/twitter/library/widget/ap;

    iget-object v3, v3, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-interface {v4, v2, v3}, Lcom/twitter/library/widget/ap;->a(ILjava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v2

    if-eqz v2, :cond_5

    new-instance v3, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    invoke-direct {v3, v4, v2}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    iput-object v3, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v2}, Lhn;->a(Landroid/graphics/drawable/Drawable;)Lhn;

    :cond_0
    :goto_2
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->U()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->z:Landroid/content/SharedPreferences;

    const-string/jumbo v2, "debug_show_explore_timeline"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    const v0, 0x7f09030f    # com.twitter.android.R.id.toolbar_explore

    invoke-virtual {p1, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lhn;->b(Z)Lhn;

    :cond_2
    return v1

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    iget-object v4, v3, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lhn;->a(Ljava/lang/CharSequence;)Lhn;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "@"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v3, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lhn;->c(Ljava/lang/CharSequence;)Lhn;

    goto :goto_1

    :cond_5
    const v2, 0x7f02003b    # com.twitter.android.R.drawable.bg_no_profile_photo_sm

    invoke-virtual {v0, v2}, Lhn;->g(I)Lhn;

    goto :goto_2

    :cond_6
    iget-object v2, p0, Lcom/twitter/android/MainActivity;->ac:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, v2}, Lhn;->a(Landroid/graphics/drawable/Drawable;)Lhn;

    goto :goto_2
.end method

.method protected a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z
    .locals 3

    const/4 v1, 0x1

    invoke-super {p0, p1, p2}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhm;Lcom/twitter/internal/android/widget/ToolBar;)Z

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->ae:Z

    if-eqz v0, :cond_1

    const v0, 0x7f110012    # com.twitter.android.R.menu.legacy_home_toolbar

    invoke-virtual {p1, v0, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const v0, 0x7f090310    # com.twitter.android.R.id.find_people

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lhn;->d()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    const v2, 0x7f0f006f    # com.twitter.android.R.string.button_find_people

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :cond_0
    const v0, 0x7f090319    # com.twitter.android.R.id.filter_notifications

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->ab:Lhn;

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    const v2, 0x7f090045    # com.twitter.android.R.id.home

    invoke-virtual {p0, v2}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->setDragHandle(Landroid/view/View;)V

    const v0, 0x7f090313    # com.twitter.android.R.id.my_profile

    invoke-virtual {p2, v0}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->aa:Lhn;

    return v1

    :cond_1
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-boolean v0, v0, Lcom/twitter/library/api/TwitterUser;->verified:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    const v2, 0x7f110010    # com.twitter.android.R.menu.home_toolbar

    invoke-virtual {p1, v2, p2}, Lhm;->a(ILcom/twitter/internal/android/widget/ToolBar;)V

    const v2, 0x7f090312    # com.twitter.android.R.id.toolbar_notif

    invoke-virtual {p2, v2}, Lcom/twitter/internal/android/widget/ToolBar;->a(I)Lhn;

    move-result-object v2

    if-eqz v0, :cond_3

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Lhn;->e(I)V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    const/4 v0, 0x2

    goto :goto_2
.end method

.method public a(Lhn;)Z
    .locals 9

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v1

    const v2, 0x7f090319    # com.twitter.android.R.id.filter_notifications

    if-ne v1, v2, :cond_0

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/NotificationsTimelineSettingsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "account_name"

    iget-object v3, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    return v0

    :cond_0
    const v2, 0x7f090317    # com.twitter.android.R.id.accounts

    if-ne v1, v2, :cond_1

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "page"

    sget-object v3, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "account_name"

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    const v2, 0x7f090310    # com.twitter.android.R.id.find_people

    if-ne v1, v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v4}, Lcom/twitter/android/kr;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string/jumbo v4, ""

    aput-object v4, v3, v0

    const-string/jumbo v4, "perch"

    aput-object v4, v3, v7

    const-string/jumbo v4, "find_people"

    aput-object v4, v3, v5

    const-string/jumbo v4, "click"

    aput-object v4, v3, v8

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->e(I)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/RootTabbedFindPeopleActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1, v5}, Lcom/twitter/android/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_2
    const v2, 0x7f090313    # com.twitter.android.R.id.my_profile

    if-ne v1, v2, :cond_3

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_3
    const v2, 0x7f090312    # com.twitter.android.R.id.toolbar_notif

    if-ne v1, v2, :cond_5

    iget-boolean v1, p0, Lcom/twitter/android/MainActivity;->H:Z

    if-eqz v1, :cond_4

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/RootMentionsTimelineActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/RootNotificationActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "activity_mention_only"

    iget-boolean v3, p0, Lcom/twitter/android/MainActivity;->H:Z

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_5
    const v2, 0x7f090311    # com.twitter.android.R.id.toolbar_dms

    if-ne v1, v2, :cond_6

    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/RootMessagesActivity;->b(Landroid/content/Context;)Ljava/lang/Class;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_6
    const v2, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v1, v2, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-direct {v2, v3, v4}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    iget-object v4, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v4}, Lcom/twitter/android/kr;->d()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    const-string/jumbo v4, ""

    aput-object v4, v3, v0

    const-string/jumbo v0, "perch"

    aput-object v0, v3, v7

    const-string/jumbo v0, ""

    aput-object v0, v3, v5

    const-string/jumbo v0, "click"

    aput-object v0, v3, v8

    invoke-virtual {v2, v3}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v0, v3}, Lcom/twitter/library/scribe/ScribeLog;->e(I)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v1, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->K:Lcom/twitter/android/ki;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ki;->a(Lhn;)Z

    move-result v0

    goto/16 :goto_0

    :cond_7
    const v2, 0x7f090316    # com.twitter.android.R.id.drafts

    if-ne v1, v2, :cond_8

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/DraftsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "return_to_drafts"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_8
    const v2, 0x7f090314    # com.twitter.android.R.id.lists

    if-ne v1, v2, :cond_9

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ListsActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "owner_id"

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "profile"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_9
    const v2, 0x7f090315    # com.twitter.android.R.id.timelines

    if-ne v1, v2, :cond_a

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ProfileCollectionsListActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "user_id"

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_a
    const v2, 0x7f09030f    # com.twitter.android.R.id.toolbar_explore

    if-ne v1, v2, :cond_b

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/ExploreActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_b
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->a(Lhn;)Z

    move-result v0

    goto/16 :goto_0
.end method

.method public b(Landroid/net/Uri;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->I()Z

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ai:Lcom/twitter/android/client/aw;

    const/16 v2, 0x3f

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;I)V

    sget-object v1, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v1, p1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ai:Lcom/twitter/android/client/aw;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/client/aw;->b(Ljava/lang/String;I)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v0, p1}, Lcom/twitter/android/kr;->a(Landroid/net/Uri;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kr;->getPageTitle(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->L()Lcom/twitter/android/client/bn;

    move-result-object v1

    invoke-virtual {v0}, Lcom/twitter/android/kr;->d()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/bn;->a(Ljava/lang/String;)Lcom/twitter/android/client/bn;

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->g(Landroid/net/Uri;)V

    return-void
.end method

.method protected b(Ljava/lang/String;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->A:Lcom/twitter/library/client/f;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->A:Lcom/twitter/library/client/f;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/f;->b(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    iput-object p1, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-direct {v0, p0, p1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    const-string/jumbo v1, "connect_tab"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/twitter/android/MainActivity;->H:Z

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/f;->a(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->A:Lcom/twitter/library/client/f;

    return-void
.end method

.method public b(Lhn;)Z
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-virtual {p1}, Lhn;->a()I

    move-result v2

    const v3, 0x7f090045    # com.twitter.android.R.id.home

    if-ne v2, v3, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v2

    new-instance v3, Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v4, 0x5

    new-array v4, v4, [Ljava/lang/String;

    iget-object v5, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v5}, Lcom/twitter/android/kr;->d()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v1

    const-string/jumbo v1, ""

    aput-object v1, v4, v0

    const/4 v1, 0x2

    const-string/jumbo v5, "perch"

    aput-object v5, v4, v1

    const/4 v1, 0x3

    const-string/jumbo v5, ""

    aput-object v5, v4, v1

    const/4 v1, 0x4

    const-string/jumbo v5, "long_press"

    aput-object v5, v4, v1

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-object v4, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v4

    invoke-virtual {v1, v4}, Lcom/twitter/library/scribe/ScribeLog;->e(I)Lcom/twitter/library/scribe/ScribeLog;

    invoke-virtual {v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "page"

    sget-object v3, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v1

    const-string/jumbo v2, "account_name"

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public c()Lcom/twitter/android/client/BaseListFragment;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/kr;->a(I)Lhb;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Lhb;)Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    return-object v0
.end method

.method public c(Landroid/net/Uri;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v0, p1}, Lcom/twitter/android/kf;->a(Landroid/net/Uri;)Lhb;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, v0, Lhb;->l:Z

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/MainActivity;->M:Lcom/twitter/android/kf;

    invoke-virtual {v1, v0}, Lcom/twitter/android/kf;->b(Lhb;)V

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->p()V

    goto :goto_0
.end method

.method d()V
    .locals 10

    const-wide/16 v8, 0x0

    const/4 v7, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "hometab"

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->i:Lcom/twitter/android/kd;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    const-string/jumbo v5, "ft"

    invoke-virtual {v2, v5, v8, v9}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v5

    cmp-long v5, v5, v8

    if-nez v5, :cond_1

    invoke-virtual {v0, v7}, Lcom/twitter/android/kd;->hasMessages(I)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {v0, v7}, Lcom/twitter/android/kd;->removeMessages(I)V

    :cond_0
    invoke-virtual {v0, v7, v1}, Lcom/twitter/android/kd;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v5

    const-wide/32 v6, 0xea60

    invoke-virtual {v0, v5, v6, v7}, Lcom/twitter/android/kd;->sendMessageDelayed(Landroid/os/Message;J)Z

    :cond_1
    const-wide/16 v5, 0x7530

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V

    return-void
.end method

.method f()V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/twitter/android/MainActivity;->c(I)V

    invoke-direct {p0, v2}, Lcom/twitter/android/MainActivity;->b(I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->U:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lhb;

    iput v2, v0, Lhb;->k:I

    goto :goto_0

    :cond_0
    invoke-direct {p0, v3}, Lcom/twitter/android/MainActivity;->f(Landroid/net/Uri;)V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getSupportLoaderManager()Landroid/support/v4/app/LoaderManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->v:Lcom/twitter/android/kc;

    invoke-virtual {v0, v2, v3, v1}, Landroid/support/v4/app/LoaderManager;->restartLoader(ILandroid/os/Bundle;Landroid/support/v4/app/LoaderManager$LoaderCallbacks;)Landroid/support/v4/content/Loader;

    return-void
.end method

.method public g()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->c()Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->aB()V

    :cond_0
    return-void
.end method

.method protected h()V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->N:Landroid/view/View;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b0077    # com.twitter.android.R.color.text

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method protected h_()V
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->c()Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->aa()V

    :cond_0
    return-void
.end method

.method protected i()Ljava/lang/String;
    .locals 1

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->c()Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->w()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected m_()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->c()Lcom/twitter/android/client/BaseListFragment;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-boolean v1, p0, Lcom/twitter/android/MainActivity;->aj:Z

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Z()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/MainActivity;->aj:Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0}, Lcom/twitter/android/client/BaseListFragment;->Y()V

    goto :goto_0
.end method

.method public onAccountsUpdated([Landroid/accounts/Account;)V
    .locals 10

    const/4 v5, 0x0

    const/4 v4, 0x0

    array-length v7, p1

    move v3, v4

    move-object v0, v5

    move v1, v4

    move v2, v4

    :goto_0
    if-ge v3, v7, :cond_2

    aget-object v6, p1, v3

    sget-object v8, Lcom/twitter/library/util/a;->a:Ljava/lang/String;

    iget-object v9, v6, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_1

    add-int/lit8 v2, v2, 0x1

    if-nez v0, :cond_0

    move-object v0, v6

    :cond_0
    if-nez v1, :cond_1

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v8, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    invoke-virtual {v6, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    const/4 v1, 0x1

    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    if-nez v1, :cond_3

    if-lez v2, :cond_3

    iput-object v5, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    invoke-virtual {p0, v4}, Lcom/twitter/android/MainActivity;->a(Z)V

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->m()V

    :cond_3
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 7

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    invoke-super {p0, p1, p2, p3}, Lcom/twitter/android/client/BaseFragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    if-ne p2, v1, :cond_0

    const-string/jumbo v0, "account"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/UserAccount;

    iget-object v0, v0, Lcom/twitter/android/UserAccount;->a:Landroid/accounts/Account;

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    iget-object v2, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->x:Lcom/twitter/android/e;

    invoke-virtual {v1, v0}, Lcom/twitter/android/e;->a(Landroid/accounts/Account;)V

    goto :goto_0

    :pswitch_1
    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->B:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0, v1}, Lcom/twitter/android/MainActivity;->showDialog(I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/MainActivity;->B:Z

    goto :goto_0

    :pswitch_2
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/client/f;

    const-string/jumbo v1, "hometab"

    invoke-direct {v2, p0, v0, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "taut"

    const-wide/16 v3, 0x0

    invoke-virtual {v2, v0, v3, v4}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v3, -0x1

    cmp-long v0, v0, v3

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public onAttachedToWindow()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onAttachedToWindow()V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/Window;->setFormat(I)V

    return-void
.end method

.method public onBackPressed()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->l()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onBackPressed()V

    :cond_0
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    const v1, 0x7f09008d    # com.twitter.android.R.id.account_row

    if-ne v0, v1, :cond_1

    const-string/jumbo v0, "perch:::accounts:click"

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->g(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/AccountsDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v1, "page"

    sget-object v2, Lcom/twitter/android/MainActivity;->a:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "account_name"

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const v1, 0x7f09006b    # com.twitter.android.R.id.settings

    if-ne v0, v1, :cond_0

    const-string/jumbo v0, "perch:::settings:click"

    invoke-direct {p0, v0}, Lcom/twitter/android/MainActivity;->g(Ljava/lang/String;)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/SettingsDialogActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/MainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->K:Lcom/twitter/android/ki;

    invoke-virtual {v0, p1}, Lcom/twitter/android/ki;->a(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onContentChanged()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onContentChanged()V

    const v0, 0x7f0900bb    # com.twitter.android.R.id.pager

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v4/view/ViewPager;

    iput-object v0, p0, Lcom/twitter/android/MainActivity;->m:Landroid/support/v4/view/ViewPager;

    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onCreateDialog(I)Landroid/app/Dialog;

    move-result-object v0

    :goto_0
    return-object v0

    :pswitch_1
    new-instance v1, Lcom/twitter/android/ka;

    invoke-direct {v1, p0, v0}, Lcom/twitter/android/ka;-><init>(Lcom/twitter/android/MainActivity;Lcom/twitter/android/client/c;)V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string/jumbo v6, "location_prompt::::impression"

    aput-object v6, v4, v5

    invoke-virtual {v0, v2, v3, v4}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/client/c;->a(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    new-instance v1, Lcom/twitter/android/kb;

    invoke-direct {v1, p0}, Lcom/twitter/android/kb;-><init>(Lcom/twitter/android/MainActivity;)V

    invoke-virtual {v0, p0, v1}, Lcom/twitter/android/client/c;->b(Landroid/app/Activity;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-static {p0, v0}, Lgx;->a(Landroid/content/Context;Lcom/twitter/android/client/c;)Landroid/app/Dialog;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onDestroy()V

    invoke-static {p0}, Lcom/twitter/library/platform/PushService;->g(Landroid/content/Context;)V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MainActivity;->ah:Lcom/twitter/android/kl;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Lcom/twitter/library/client/z;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->F:Lcom/twitter/library/client/j;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v1

    if-eqz v0, :cond_0

    invoke-virtual {v1, v0}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ag:Lcom/twitter/library/service/a;

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->b(Lcom/twitter/library/service/a;)V

    iget-boolean v0, p0, Lcom/twitter/android/MainActivity;->G:Z

    if-eqz v0, :cond_1

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/accounts/AccountManager;->removeOnAccountsUpdatedListener(Landroid/accounts/OnAccountsUpdateListener;)V

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MainActivity;->E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->A:Lcom/twitter/library/client/f;

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->E:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-virtual {v0, v2}, Lcom/twitter/library/client/f;->b(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_2
    const/4 v0, 0x1

    invoke-virtual {v1, v0, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->Z:Lcom/twitter/library/client/al;

    invoke-virtual {v0, p0}, Lcom/twitter/library/client/al;->b(Lcom/twitter/library/client/am;)V

    :cond_3
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onNewIntent(Landroid/content/Intent;)V

    invoke-virtual {p0, p1}, Lcom/twitter/android/MainActivity;->setIntent(Landroid/content/Intent;)V

    invoke-direct {p0, p1}, Lcom/twitter/android/MainActivity;->a(Landroid/content/Intent;)V

    return-void
.end method

.method protected onPause()V
    .locals 4

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onPause()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->ai:Lcom/twitter/android/client/aw;

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x3f

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->y:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string/jumbo v1, "ver"

    const/4 v2, 0x4

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "version_code"

    sget v2, Lcom/twitter/android/MainActivity;->u:I

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "tag"

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v2}, Lcom/twitter/android/kr;->c()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string/jumbo v1, "st"

    iget-wide v2, p0, Lcom/twitter/android/MainActivity;->w:J

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/MainActivity;->aj:Z

    return-void
.end method

.method protected onPostCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onPostCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->K:Lcom/twitter/android/ki;

    invoke-virtual {v0}, Lcom/twitter/android/ki;->e()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "currentTab"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    if-eqz v0, :cond_0

    invoke-virtual {p0, v0}, Lcom/twitter/android/MainActivity;->a(Landroid/net/Uri;)V

    :cond_0
    return-void
.end method

.method protected onResume()V
    .locals 13

    const-wide/16 v11, 0x0

    const/4 v10, 0x0

    const/4 v9, 0x1

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onResume()V

    invoke-static {}, Lgm;->c()V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->X()Lcom/twitter/library/client/aa;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->j:Ljava/lang/String;

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/twitter/library/client/aa;->d(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    :try_start_0
    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    sget v3, Lcom/twitter/android/MainActivity;->u:I

    if-nez v3, :cond_5

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->E()J

    move-result-wide v3

    cmp-long v3, v3, v11

    if-nez v3, :cond_5

    iget-object v3, p0, Lcom/twitter/android/MainActivity;->ai:Lcom/twitter/android/client/aw;

    invoke-virtual {v3}, Lcom/twitter/android/client/aw;->a()V

    :cond_1
    :goto_0
    sput v2, Lcom/twitter/android/MainActivity;->u:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget v2, Lcom/twitter/android/MainActivity;->t:I

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->y:Landroid/content/SharedPreferences;

    const-string/jumbo v3, "ver"

    invoke-interface {v2, v3, v10}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    if-nez v2, :cond_7

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/platform/LocationProducer;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v2

    if-eqz v2, :cond_6

    const-string/jumbo v2, "debug_prefs"

    invoke-virtual {p0, v2, v10}, Lcom/twitter/android/MainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "show_loc_dlg"

    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v9}, Lcom/twitter/android/MainActivity;->showDialog(I)V

    :cond_2
    :goto_2
    const/4 v2, 0x4

    sput v2, Lcom/twitter/android/MainActivity;->t:I

    :cond_3
    iget-object v2, p0, Lcom/twitter/android/MainActivity;->y:Landroid/content/SharedPreferences;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-string/jumbo v5, "st"

    invoke-interface {v2, v5, v11, v12}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v5

    iput-wide v5, p0, Lcom/twitter/android/MainActivity;->w:J

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    iget-wide v5, p0, Lcom/twitter/android/MainActivity;->w:J

    const-wide/32 v7, 0x36ee80

    add-long/2addr v5, v7

    cmp-long v2, v5, v3

    if-gez v2, :cond_4

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->o()V

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->d(Lcom/twitter/library/client/Session;)Ljava/lang/String;

    iput-wide v3, p0, Lcom/twitter/android/MainActivity;->w:J

    :cond_4
    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    invoke-direct {p0, v0, v1, v2}, Lcom/twitter/android/MainActivity;->a(Ljava/lang/String;J)V

    return-void

    :cond_5
    :try_start_1
    sget v3, Lcom/twitter/android/MainActivity;->u:I

    if-le v2, v3, :cond_1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->E()J

    move-result-wide v5

    invoke-virtual {v0}, Lcom/twitter/android/client/c;->D()J

    move-result-wide v7

    add-long/2addr v5, v7

    cmp-long v3, v3, v5

    if-lez v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/MainActivity;->ai:Lcom/twitter/android/client/aw;

    invoke-virtual {v3}, Lcom/twitter/android/client/aw;->a()V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    goto/16 :goto_1

    :cond_6
    invoke-virtual {p0, v9}, Lcom/twitter/android/MainActivity;->showDialog(I)V

    goto :goto_2

    :cond_7
    if-eq v2, v9, :cond_8

    const/4 v3, 0x2

    if-ne v2, v3, :cond_9

    :cond_8
    const-string/jumbo v2, "discover_prefs"

    invoke-virtual {p0, v2, v10}, Lcom/twitter/android/MainActivity;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    const-string/jumbo v3, "show_loc_dlg"

    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->N()Lcom/twitter/library/platform/LocationProducer;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/platform/LocationProducer;->c()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {p0, v9}, Lcom/twitter/android/MainActivity;->showDialog(I)V

    goto :goto_2

    :cond_9
    const/4 v3, 0x3

    if-ne v2, v3, :cond_2

    invoke-static {p0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string/jumbo v3, "show_int_dlg"

    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_2
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Lcom/twitter/android/client/BaseFragmentActivity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->C:Lcom/twitter/android/kr;

    invoke-virtual {v0}, Lcom/twitter/android/kr;->c()Landroid/net/Uri;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "currentTab"

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 7

    invoke-direct {p0}, Lcom/twitter/android/MainActivity;->l()Z

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/client/f;

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v3, "hometab"

    invoke-direct {v2, p0, v0, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/MainActivity;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/client/f;JJ)V

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onSearchRequested()Z

    move-result v0

    return v0
.end method

.method protected onStart()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStart()V

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->Y()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    const-string/jumbo v2, "app:ready"

    invoke-static {p0, v0, v1, v2}, Lcom/twitter/android/util/AppMetrics;->a(Landroid/content/Context;JLjava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->X:Lcom/twitter/library/util/ar;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->X:Lcom/twitter/library/util/ar;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    :cond_0
    return-void
.end method

.method protected onStop()V
    .locals 3

    invoke-super {p0}, Lcom/twitter/android/client/BaseFragmentActivity;->onStop()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->L:Lcom/twitter/internal/android/widget/HiddenDrawerLayout;

    invoke-virtual {v0}, Lcom/twitter/internal/android/widget/HiddenDrawerLayout;->g()V

    iget-object v0, p0, Lcom/twitter/android/MainActivity;->X:Lcom/twitter/library/util/ar;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MainActivity;->K()Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/twitter/android/MainActivity;->X:Lcom/twitter/library/util/ar;

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    :cond_0
    return-void
.end method
