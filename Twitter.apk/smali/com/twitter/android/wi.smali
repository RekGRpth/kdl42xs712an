.class Lcom/twitter/android/wi;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/TweetActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/wi;->a:Lcom/twitter/android/TweetActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    iget-object v0, p0, Lcom/twitter/android/wi;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->m(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/scribe/ScribeLog;

    iget-object v2, p0, Lcom/twitter/android/wi;->a:Lcom/twitter/android/TweetActivity;

    iget-object v2, v2, Lcom/twitter/android/TweetActivity;->a:Lcom/twitter/library/client/Session;

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Lcom/twitter/library/scribe/ScribeLog;-><init>(J)V

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string/jumbo v4, ":composition::remove_photo:click"

    aput-object v4, v2, v3

    invoke-virtual {v1, v2}, Lcom/twitter/library/scribe/ScribeLog;->b([Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/scribe/ScribeLog;)V

    iget-object v0, p0, Lcom/twitter/android/wi;->a:Lcom/twitter/android/TweetActivity;

    invoke-static {v0}, Lcom/twitter/android/TweetActivity;->n(Lcom/twitter/android/TweetActivity;)Lcom/twitter/android/PostStorage;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/PostStorage;->e()V

    return-void
.end method
