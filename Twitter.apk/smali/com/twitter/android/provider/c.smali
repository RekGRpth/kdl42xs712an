.class Lcom/twitter/android/provider/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/android/internal/util/Predicate;


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/provider/a;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/provider/c;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/api/search/TwitterTypeAhead;)Z
    .locals 1

    invoke-static {}, Lcom/twitter/library/api/conversations/ae;->a()Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p1, :cond_1

    iget-object v0, p1, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/library/api/TwitterUser;

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/twitter/library/api/search/TwitterTypeAhead;->e:Lcom/twitter/library/api/TwitterUser;

    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public synthetic apply(Ljava/lang/Object;)Z
    .locals 1

    check-cast p1, Lcom/twitter/library/api/search/TwitterTypeAhead;

    invoke-virtual {p0, p1}, Lcom/twitter/android/provider/c;->a(Lcom/twitter/library/api/search/TwitterTypeAhead;)Z

    move-result v0

    return v0
.end method
