.class final Lcom/twitter/android/un;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public final a:Lcom/twitter/android/widget/TextSwitcherView;

.field public final b:Landroid/widget/TextView;

.field public final c:Landroid/view/View;

.field public d:J


# direct methods
.method constructor <init>(Landroid/view/View;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const v0, 0x7f0901a2    # com.twitter.android.R.id.textSwitcher

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/widget/TextSwitcherView;

    iput-object v0, p0, Lcom/twitter/android/un;->a:Lcom/twitter/android/widget/TextSwitcherView;

    const v0, 0x7f090090    # com.twitter.android.R.id.title

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/un;->b:Landroid/widget/TextView;

    const v0, 0x7f0901a3    # com.twitter.android.R.id.dismiss

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/un;->c:Landroid/view/View;

    return-void
.end method


# virtual methods
.method a(Landroid/content/Context;I)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/un;->b:Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method
