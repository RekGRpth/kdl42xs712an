.class Lcom/twitter/android/xd;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/widget/k;


# instance fields
.field final synthetic a:Lcom/twitter/library/widget/PageableListView;

.field final synthetic b:Lcom/twitter/android/TweetFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/TweetFragment;Lcom/twitter/library/widget/PageableListView;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iput-object p2, p0, Lcom/twitter/android/xd;->a:Lcom/twitter/library/widget/PageableListView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/widget/AbsListView;)V
    .locals 12

    const/4 v11, 0x1

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v0

    iget-wide v6, v0, Lcom/twitter/library/provider/Tweet;->o:J

    iget-object v1, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v1}, Lcom/twitter/android/TweetFragment;->q()Lcom/twitter/android/xo;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v4, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v4, v4, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/provider/Tweet;->D()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v4, v1, Lcom/twitter/android/xo;->c:J

    const-wide/32 v8, 0x493e0

    add-long/2addr v4, v8

    cmp-long v0, v2, v4

    if-ltz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0, v11}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-wide v2, v1, Lcom/twitter/android/xo;->c:J

    iget-object v10, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    const-wide/16 v4, 0x0

    iget-object v8, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v8, v8, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v8, v8, Lcom/twitter/library/provider/Tweet;->j:J

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJJJ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0, v11}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/xd;->a:Lcom/twitter/library/widget/PageableListView;

    invoke-virtual {v0, v11}, Lcom/twitter/library/widget/PageableListView;->a(Z)V

    goto :goto_0
.end method

.method public b(Landroid/widget/AbsListView;)V
    .locals 12

    const/4 v11, 0x2

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lkn;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/UserPresenceFragment;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/UserPresenceFragment;->q()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->d(Lcom/twitter/android/TweetFragment;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->c(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/UserPresenceFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v1, v1, Lcom/twitter/library/provider/Tweet;->o:J

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/UserPresenceFragment;->b(J)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    iget-object v1, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->i:Lcom/twitter/android/xq;

    invoke-virtual {v1}, Lcom/twitter/android/xq;->getCount()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v0, v1}, Lcom/twitter/android/xq;->a(I)Lcom/twitter/library/provider/Tweet;

    move-result-object v4

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, v0, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4, v0}, Lcom/twitter/library/provider/Tweet;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-virtual {v0}, Lcom/twitter/android/TweetFragment;->q()Lcom/twitter/android/xo;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    iget-wide v5, v0, Lcom/twitter/android/xo;->b:J

    const-wide/32 v7, 0x493e0

    add-long/2addr v5, v7

    cmp-long v3, v1, v5

    if-ltz v3, :cond_0

    iget-object v3, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v3, v11}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;I)Z

    move-result v3

    if-nez v3, :cond_0

    iput-wide v1, v0, Lcom/twitter/android/xo;->b:J

    iget-object v10, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->e(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v1, v1, Lcom/twitter/android/TweetFragment;->a:Lcom/twitter/library/client/Session;

    iget-object v2, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v2, v2, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v2, v2, Lcom/twitter/library/provider/Tweet;->o:J

    iget-wide v4, v4, Lcom/twitter/library/provider/Tweet;->o:J

    const-wide/16 v6, 0x0

    iget-object v8, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    iget-object v8, v8, Lcom/twitter/android/TweetFragment;->b:Lcom/twitter/library/provider/Tweet;

    iget-wide v8, v8, Lcom/twitter/library/provider/Tweet;->j:J

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JJJJ)Ljava/lang/String;

    move-result-object v0

    invoke-static {v10, v0, v11}, Lcom/twitter/android/TweetFragment;->b(Lcom/twitter/android/TweetFragment;Ljava/lang/String;I)V

    iget-object v0, p0, Lcom/twitter/android/xd;->a:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->f(Lcom/twitter/android/TweetFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xd;->a:Lcom/twitter/library/widget/PageableListView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/library/widget/PageableListView;->b(Z)V

    goto/16 :goto_0
.end method

.method public onScroll(Landroid/widget/AbsListView;III)V
    .locals 0

    return-void
.end method

.method public onScrollStateChanged(Landroid/widget/AbsListView;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/xl;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/xd;->b:Lcom/twitter/android/TweetFragment;

    invoke-static {v0}, Lcom/twitter/android/TweetFragment;->a(Lcom/twitter/android/TweetFragment;)Lcom/twitter/android/xl;

    move-result-object v0

    invoke-interface {v0}, Lcom/twitter/android/xl;->g()V

    :cond_0
    return-void
.end method
