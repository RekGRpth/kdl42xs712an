.class Lcom/twitter/android/samsung/single/h;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:Lcom/twitter/android/client/c;

.field final synthetic c:Z

.field final synthetic d:Lcom/twitter/library/provider/Tweet;

.field final synthetic e:Lcom/twitter/library/client/aa;

.field final synthetic f:Ljava/lang/String;

.field final synthetic g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;ILcom/twitter/android/client/c;ZLcom/twitter/library/provider/Tweet;Lcom/twitter/library/client/aa;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    iput p2, p0, Lcom/twitter/android/samsung/single/h;->a:I

    iput-object p3, p0, Lcom/twitter/android/samsung/single/h;->b:Lcom/twitter/android/client/c;

    iput-boolean p4, p0, Lcom/twitter/android/samsung/single/h;->c:Z

    iput-object p5, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    iput-object p6, p0, Lcom/twitter/android/samsung/single/h;->e:Lcom/twitter/library/client/aa;

    iput-object p7, p0, Lcom/twitter/android/samsung/single/h;->f:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6

    const/4 v5, 0x0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-static {v0}, Lcom/twitter/android/samsung/single/p;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f000c    # com.twitter.android.R.string.action_error

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-static {v1, v0, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    new-instance v1, Lcom/twitter/android/samsung/single/i;

    invoke-direct {v1, p0}, Lcom/twitter/android/samsung/single/i;-><init>(Lcom/twitter/android/samsung/single/h;)V

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;Lcom/twitter/library/client/j;)Lcom/twitter/library/client/j;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->b:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-static {v1}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->b(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;)Lcom/twitter/library/client/j;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    iget-boolean v0, p0, Lcom/twitter/android/samsung/single/h;->c:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v0, v0, Lcom/twitter/library/provider/Tweet;->F:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->b:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/samsung/single/h;->e:Lcom/twitter/library/client/aa;

    iget-object v3, p0, Lcom/twitter/android/samsung/single/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->F:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->b:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/samsung/single/h;->e:Lcom/twitter/library/client/aa;

    iget-object v3, p0, Lcom/twitter/android/samsung/single/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->C:J

    invoke-virtual {v1, v2, v3, v4}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;J)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->b:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/samsung/single/h;->e:Lcom/twitter/library/client/aa;

    iget-object v3, p0, Lcom/twitter/android/samsung/single/h;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    iget-wide v3, v3, Lcom/twitter/library/provider/Tweet;->C:J

    iget-object v5, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    iget-object v5, v5, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JLcom/twitter/library/api/PromotedContent;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->a(Lcom/twitter/android/samsung/single/RetweetOptionsActivity;Ljava/lang/String;)Ljava/lang/String;

    goto :goto_0

    :cond_3
    const/4 v0, -0x3

    if-ne p2, v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    sget-object v1, Lcom/twitter/android/composer/ComposerIntentWrapper$Action;->d:Lcom/twitter/android/composer/ComposerIntentWrapper$Action;

    invoke-static {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Landroid/content/Context;Lcom/twitter/android/composer/ComposerIntentWrapper$Action;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    const v2, 0x7f0f033f    # com.twitter.android.R.string.quote_format

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    iget-object v4, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v4}, Lcom/twitter/library/provider/Tweet;->i()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v5

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/twitter/android/samsung/single/h;->d:Lcom/twitter/library/provider/Tweet;

    invoke-virtual {v5}, Lcom/twitter/library/provider/Tweet;->a()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;[I)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a(Ljava/lang/String;)Lcom/twitter/android/composer/ComposerIntentWrapper;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/android/composer/ComposerIntentWrapper;->a()Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v1, v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->startActivity(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/samsung/single/h;->g:Lcom/twitter/android/samsung/single/RetweetOptionsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/samsung/single/RetweetOptionsActivity;->finish()V

    goto/16 :goto_0
.end method
