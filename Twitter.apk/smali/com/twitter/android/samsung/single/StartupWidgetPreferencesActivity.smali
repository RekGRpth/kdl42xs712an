.class public Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;
.super Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->d:Ljava/lang/String;

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    const-string/jumbo v0, "widget::::add"

    iget v1, p0, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->c:I

    invoke-static {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->c:I

    invoke-static {p0, p1, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Ljava/lang/String;I)V

    :cond_0
    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    const/4 v2, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->onCreate(Landroid/os/Bundle;)V

    iget v0, p0, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->c:I

    if-nez v0, :cond_0

    invoke-virtual {p0, v2}, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->setResult(I)V

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->finish()V

    :cond_0
    invoke-static {p0}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->a(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->finish()V

    :cond_1
    invoke-static {p0}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;)I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_2

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    sget-object v1, Lcom/twitter/android/samsung/model/g;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    aget-object v0, v0, v2

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->d:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->finish()V

    :cond_2
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->d:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/single/StartupWidgetPreferencesActivity;->a(Ljava/lang/String;)V

    invoke-super {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->onDestroy()V

    return-void
.end method
