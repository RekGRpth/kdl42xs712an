.class public Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;
.super Landroid/preference/PreferenceActivity;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field protected a:Landroid/preference/PreferenceGroup;

.field protected b:Landroid/preference/Preference;

.field protected c:I

.field private d:Ljava/lang/String;

.field private e:Landroid/preference/ListPreference;

.field private f:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->f:Z

    return-void
.end method

.method private a(Landroid/preference/ListPreference;Ljava/lang/String;)V
    .locals 3

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {p1, p2}, Landroid/preference/ListPreference;->findIndexOfValue(Ljava/lang/String;)I

    move-result v1

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    aget-object v0, v0, v1

    invoke-virtual {p1, v0}, Landroid/preference/ListPreference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;Landroid/preference/ListPreference;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->f:Z

    return p1
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    return-void
.end method

.method private f()V
    .locals 3

    const-string/jumbo v0, "widget::accounts:switch:click"

    iget v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0, v1}, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->a(Landroid/content/Context;Ljava/lang/String;I)V

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    iget v2, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method private g()I
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    return v0
.end method


# virtual methods
.method protected a()V
    .locals 8

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->b:Landroid/preference/Preference;

    new-instance v2, Lcom/twitter/android/samsung/single/m;

    invoke-direct {v2, p0}, Lcom/twitter/android/samsung/single/m;-><init>(Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    new-instance v2, Lcom/twitter/android/samsung/single/n;

    invoke-direct {v2, p0}, Lcom/twitter/android/samsung/single/n;-><init>(Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;)V

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntries()[Ljava/lang/CharSequence;

    move-result-object v2

    array-length v0, v2

    add-int/lit8 v0, v0, -0x1

    new-array v3, v0, [Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-virtual {v0}, Landroid/preference/ListPreference;->getEntryValues()[Ljava/lang/CharSequence;

    move-result-object v4

    array-length v0, v4

    add-int/lit8 v0, v0, -0x1

    new-array v5, v0, [Ljava/lang/CharSequence;

    move v0, v1

    :goto_0
    array-length v6, v3

    add-int/lit8 v6, v6, -0x1

    if-ge v0, v6, :cond_0

    add-int/lit8 v6, v0, 0x2

    aget-object v6, v2, v6

    aput-object v6, v3, v0

    add-int/lit8 v6, v0, 0x2

    aget-object v6, v4, v6

    aput-object v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    const v2, 0x7f0f040e    # com.twitter.android.R.string.settings_notif_tweets_summary_off

    invoke-virtual {p0, v2}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v3, v0

    const-string/jumbo v2, "0"

    aput-object v2, v5, v0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-virtual {v0, v3}, Landroid/preference/ListPreference;->setEntries([Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-virtual {v0, v5}, Landroid/preference/ListPreference;->setEntryValues([Ljava/lang/CharSequence;)V

    iget v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->c(Landroid/content/Context;I)J

    move-result-wide v2

    const-wide/16 v6, 0x0

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->f:Z

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    aget-object v2, v5, v1

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    aget-object v2, v5, v1

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    aget-object v0, v5, v1

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0, v1}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;Ljava/lang/String;I)V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected b()V
    .locals 1

    const v0, 0x7f06001b    # com.twitter.android.R.xml.widget_single_preferences

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->addPreferencesFromResource(I)V

    return-void
.end method

.method protected c()V
    .locals 13

    const/4 v12, 0x1

    const/4 v3, 0x0

    iget v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v4

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v5

    sget-object v0, Lcom/twitter/android/samsung/model/g;->a:Ljava/lang/String;

    invoke-virtual {v5, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v6

    array-length v7, v6

    move v2, v3

    move v1, v3

    :goto_0
    if-ge v2, v7, :cond_2

    aget-object v8, v6, v2

    sget-object v0, Lcom/twitter/android/samsung/model/g;->a:Ljava/lang/String;

    iget-object v9, v8, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v9, Landroid/preference/CheckBoxPreference;

    invoke-direct {v9, p0}, Landroid/preference/CheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-static {v5, v8}, Lcom/twitter/android/samsung/model/g;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v10

    iget-object v0, v10, Lcom/twitter/library/api/TwitterUser;->name:Ljava/lang/String;

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setTitle(Ljava/lang/CharSequence;)V

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v11, "@"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v11, v10, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    add-int/lit8 v0, v1, 0x1

    invoke-virtual {v9, v1}, Landroid/preference/CheckBoxPreference;->setOrder(I)V

    if-eqz v4, :cond_1

    iget-object v1, v10, Lcom/twitter/library/api/TwitterUser;->username:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    :cond_0
    :goto_1
    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->d()Landroid/preference/Preference$OnPreferenceClickListener;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    iget-object v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    invoke-virtual {v1, v9}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_1
    if-nez v4, :cond_0

    iget-object v1, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    aget-object v10, v6, v3

    iget-object v10, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v1, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v12}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v9, v1}, Landroid/preference/CheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    iget-object v1, v8, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget v8, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v1, v8}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Ljava/lang/String;I)V

    goto :goto_1

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->b:Landroid/preference/Preference;

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->g()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOrder(I)V

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    iget-object v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->b:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    return-void

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method protected d()Landroid/preference/Preference$OnPreferenceClickListener;
    .locals 1

    new-instance v0, Lcom/twitter/android/samsung/single/o;

    invoke-direct {v0, p0}, Lcom/twitter/android/samsung/single/o;-><init>(Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;)V

    return-object v0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    invoke-virtual {v0}, Landroid/preference/PreferenceGroup;->removeAll()V

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c()V

    :cond_0
    return-void
.end method

.method public onBackPressed()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->finish()V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->setResult(I)V

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e()V

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->b()V

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f03c6    # com.twitter.android.R.string.settings

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->setTitle(Ljava/lang/CharSequence;)V

    const-string/jumbo v0, "accounts"

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceGroup;

    iput-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    iget-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a:Landroid/preference/PreferenceGroup;

    const-string/jumbo v1, "add_account"

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceGroup;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->b:Landroid/preference/Preference;

    const-string/jumbo v0, "refresh_rate"

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iput-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->e:Landroid/preference/ListPreference;

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->a()V

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c()V

    iget v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->d:Ljava/lang/String;

    return-void
.end method

.method protected onDestroy()V
    .locals 3

    iget v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->e(Landroid/content/Context;I)V

    iget v0, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->d:Ljava/lang/String;

    if-nez v1, :cond_3

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->d:Ljava/lang/String;

    if-eq v0, v1, :cond_3

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->f()V

    :cond_1
    :goto_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    iget v2, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-boolean v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->f:Z

    if-eqz v1, :cond_2

    iget v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->c:I

    invoke-static {p0, v1}, Lcom/twitter/android/samsung/data/PollingService;->a(Landroid/content/Context;I)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->sendBroadcast(Landroid/content/Intent;)V

    :cond_2
    const/4 v1, -0x1

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->setResult(ILandroid/content/Intent;)V

    invoke-super {p0}, Landroid/preference/PreferenceActivity;->onDestroy()V

    return-void

    :cond_3
    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->f()V

    goto :goto_0
.end method

.method protected onUserLeaveHint()V
    .locals 0

    invoke-virtual {p0}, Lcom/twitter/android/samsung/single/WidgetPreferencesActivity;->finish()V

    return-void
.end method
