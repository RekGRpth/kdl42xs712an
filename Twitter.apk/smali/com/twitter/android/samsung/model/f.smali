.class public Lcom/twitter/android/samsung/model/f;
.super Lcom/twitter/android/samsung/model/WidgetViewModel;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/samsung/model/WidgetViewModel;-><init>()V

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    const v0, 0x7f030194    # com.twitter.android.R.layout.widget_single_item

    return v0
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;)V
    .locals 5

    const v4, 0x7f0f0481    # com.twitter.android.R.string.stub_username

    const v1, 0x7f0902e6    # com.twitter.android.R.id.flipper_item_image

    const/16 v3, 0x8

    const v0, 0x7f020305    # com.twitter.android.R.drawable.widget_stub

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const/4 v0, 0x0

    invoke-virtual {p2, v1, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902e8    # com.twitter.android.R.id.twitter_name

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0902e9    # com.twitter.android.R.id.twitter_handle

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "@"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f09001e    # com.twitter.android.R.id.text

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f047f    # com.twitter.android.R.string.stub_content

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    const v0, 0x7f0902e7    # com.twitter.android.R.id.thumbnail

    const v1, 0x7f020306    # com.twitter.android.R.drawable.widget_stub_profile

    invoke-virtual {p2, v0, v1}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    const v0, 0x7f0902f3    # com.twitter.android.R.id.logged_out_share

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902f4    # com.twitter.android.R.id.button_frame_divider

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902f5    # com.twitter.android.R.id.logged_in_button_frame

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    const v0, 0x7f0902fa    # com.twitter.android.R.id.image_loading_progress_bar

    invoke-virtual {p2, v0, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    return-void
.end method

.method public a(Landroid/content/Context;Landroid/widget/RemoteViews;I)V
    .locals 0

    return-void
.end method

.method public d()Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;
    .locals 1

    sget-object v0, Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;->c:Lcom/twitter/android/samsung/model/WidgetViewModel$ModelType;

    return-object v0
.end method

.method public e()J
    .locals 2

    const-wide/16 v0, 0x0

    return-wide v0
.end method
