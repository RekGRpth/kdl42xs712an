.class public Lcom/twitter/android/samsung/data/WidgetDataUpdateService;
.super Landroid/app/Service;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field private static a:[Ljava/lang/String;

.field private static c:Ljava/lang/String;

.field private static d:J


# instance fields
.field private final b:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/16 v0, 0x26

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "th"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string/jumbo v2, "jp"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string/jumbo v2, "fi"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string/jumbo v2, "my"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string/jumbo v2, "kr"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string/jumbo v2, "se"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string/jumbo v2, "mx"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string/jumbo v2, "sg"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string/jumbo v2, "sa"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string/jumbo v2, "kw"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string/jumbo v2, "qa"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string/jumbo v2, "us"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string/jumbo v2, "br"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string/jumbo v2, "it"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string/jumbo v2, "de"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string/jumbo v2, "fr"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string/jumbo v2, "pl"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string/jumbo v2, "eg"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string/jumbo v2, "ph"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string/jumbo v2, "pe"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string/jumbo v2, "tr"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string/jumbo v2, "pt"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string/jumbo v2, "nl"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string/jumbo v2, "ru"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string/jumbo v2, "cn"

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    const-string/jumbo v2, "es"

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    const-string/jumbo v2, "ae"

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    const-string/jumbo v2, "in"

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    const-string/jumbo v2, "gb"

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    const-string/jumbo v2, "au"

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    const-string/jumbo v2, "ar"

    aput-object v2, v0, v1

    const/16 v1, 0x20

    const-string/jumbo v2, "za"

    aput-object v2, v0, v1

    const/16 v1, 0x21

    const-string/jumbo v2, "ca"

    aput-object v2, v0, v1

    const/16 v1, 0x22

    const-string/jumbo v2, "cl"

    aput-object v2, v0, v1

    const/16 v1, 0x23

    const-string/jumbo v2, "ve"

    aput-object v2, v0, v1

    const/16 v1, 0x24

    const-string/jumbo v2, "co"

    aput-object v2, v0, v1

    const/16 v1, 0x25

    const-string/jumbo v2, "ch"

    aput-object v2, v0, v1

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a:[Ljava/lang/String;

    const-string/jumbo v0, "list:%s/%s filter:images"

    sput-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->c:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    const/4 v1, 0x0

    const-string/jumbo v3, "SUL_List1"

    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    move v0, v1

    :goto_0
    sget-object v4, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    sget-object v4, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {p1, v5}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    :goto_1
    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->c:Ljava/lang/String;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v3, v2, v1

    const/4 v1, 0x1

    aput-object p1, v2, v1

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move-object p1, v2

    goto :goto_1
.end method

.method static synthetic a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;)Ljava/util/Map;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    return-object v0
.end method

.method private a(I)V
    .locals 1

    invoke-static {p0}, Lcom/twitter/android/samsung/single/k;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b(I)V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->d(I)V

    goto :goto_0
.end method

.method private a(J)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "com.twitter.android.widget.extra_logged_out_search_id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.twitter.android.widget.single.update_logged_out"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public static a(Landroid/content/Context;I)V
    .locals 4

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/k;->c(Landroid/content/Context;I)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    sget-wide v2, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->d:J

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    const-wide/32 v2, 0xdbba0

    add-long/2addr v0, v2

    sput-wide v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->d:J

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    invoke-static {p0, v0, p1, v1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;Ljava/lang/String;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {p0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;)V
    .locals 1

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, p1, p2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Context;Ljava/lang/String;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;)V

    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;)V
    .locals 3

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.twitter.android.widget.single.update_discover"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "fetch_type"

    invoke-virtual {p3}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->ordinal()I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method private a(Landroid/content/Intent;I)V
    .locals 4

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "user"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "appWidgetId"

    const/4 v2, 0x0

    invoke-virtual {p1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string/jumbo v0, "fetch_type"

    const/4 v3, -0x1

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    if-ltz v0, :cond_0

    invoke-static {}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->values()[Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    move-result-object v3

    aget-object v0, v3, v0

    :goto_0
    invoke-direct {p0, v1, v2, v0, p2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Ljava/lang/String;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;I)V

    return-void

    :cond_0
    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(I)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;J)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(J)V

    return-void
.end method

.method private a(Lcom/twitter/android/samsung/data/f;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;Lcom/twitter/library/network/OAuthToken;I)V
    .locals 6

    new-instance v0, Lcom/twitter/android/samsung/data/b;

    new-instance v5, Lcom/twitter/android/samsung/data/q;

    invoke-direct {v5, p0, p1, p4}, Lcom/twitter/android/samsung/data/q;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;Lcom/twitter/android/samsung/data/f;I)V

    move-object v1, p0

    move-object v2, p1

    move-object v3, p3

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/twitter/android/samsung/data/b;-><init>(Landroid/content/Context;Lcom/twitter/android/samsung/data/f;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;Lcom/twitter/android/samsung/data/g;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/samsung/data/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method private declared-synchronized a(Ljava/lang/String;ILcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;I)V
    .locals 4

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v0, Lcom/twitter/android/samsung/data/f;

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->b:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-direct {v0, p1, v1, p2}, Lcom/twitter/android/samsung/data/f;-><init>(Ljava/lang/String;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;I)V

    new-instance v1, Lcom/twitter/android/samsung/data/p;

    invoke-direct {v1, p0, p2, p4}, Lcom/twitter/android/samsung/data/p;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;II)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/f;)Lcom/twitter/android/samsung/data/e;

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v2, v1}, Lcom/twitter/android/samsung/data/e;->a(Lcom/twitter/android/samsung/data/k;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    :try_start_2
    new-instance v2, Lcom/twitter/android/samsung/data/e;

    invoke-direct {v2}, Lcom/twitter/android/samsung/data/e;-><init>()V

    invoke-virtual {v2, v1}, Lcom/twitter/android/samsung/data/e;->a(Lcom/twitter/android/samsung/data/k;)V

    iget-object v1, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v1, v0, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/twitter/android/samsung/model/g;->b(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/network/OAuthToken;

    move-result-object v1

    if-nez v1, :cond_3

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/model/g;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/AccountManagerFuture;

    invoke-virtual {p0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0f003d    # com.twitter.android.R.string.authenticator_activity_loginfail_text_pwonly

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {p0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    iget-object v1, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    :cond_3
    invoke-direct {p0, v0, p3, v1, p4}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/f;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;Lcom/twitter/library/network/OAuthToken;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private b(I)V
    .locals 5

    const/4 v4, 0x0

    invoke-static {p0}, Lcom/twitter/android/samsung/single/k;->e(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->c(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->d:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Lcom/twitter/android/samsung/data/f;

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->d:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-direct {v0, v1, v2, v4}, Lcom/twitter/android/samsung/data/f;-><init>(Ljava/lang/String;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;I)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/f;)Lcom/twitter/android/samsung/data/e;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/twitter/android/samsung/data/e;

    invoke-direct {v1}, Lcom/twitter/android/samsung/data/e;-><init>()V

    new-instance v2, Lcom/twitter/android/samsung/data/l;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/samsung/data/l;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;I)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/samsung/data/e;->a(Lcom/twitter/android/samsung/data/k;)V

    iget-object v2, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/twitter/android/samsung/data/d;

    new-instance v2, Lcom/twitter/android/samsung/data/m;

    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/android/samsung/data/m;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;Lcom/twitter/android/samsung/data/f;I)V

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/samsung/data/d;-><init>(Landroid/content/Context;Lcom/twitter/android/samsung/data/g;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/twitter/android/samsung/data/d;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic b(Landroid/content/Context;I)V
    .locals 0

    invoke-static {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->c(Landroid/content/Context;I)V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->e(I)V

    return-void
.end method

.method private c(I)V
    .locals 5

    const/4 v4, 0x0

    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->a(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;)J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/twitter/android/samsung/data/f;

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->c:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-direct {v0, v1, v2, v4}, Lcom/twitter/android/samsung/data/f;-><init>(Ljava/lang/String;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;I)V

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/f;)Lcom/twitter/android/samsung/data/e;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Lcom/twitter/android/samsung/data/e;

    invoke-direct {v1}, Lcom/twitter/android/samsung/data/e;-><init>()V

    new-instance v2, Lcom/twitter/android/samsung/data/n;

    invoke-direct {v2, p0, p1}, Lcom/twitter/android/samsung/data/n;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;I)V

    invoke-virtual {v1, v2}, Lcom/twitter/android/samsung/data/e;->a(Lcom/twitter/android/samsung/data/k;)V

    iget-object v2, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v1, Lcom/twitter/android/samsung/data/a;

    new-instance v2, Lcom/twitter/android/samsung/data/o;

    invoke-direct {v2, p0, v0, p1}, Lcom/twitter/android/samsung/data/o;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;Lcom/twitter/android/samsung/data/f;I)V

    invoke-direct {v1, p0, v2}, Lcom/twitter/android/samsung/data/a;-><init>(Landroid/content/Context;Lcom/twitter/android/samsung/data/g;)V

    new-array v0, v4, [Ljava/lang/Void;

    invoke-virtual {v1, v0}, Lcom/twitter/android/samsung/data/a;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method private static c(Landroid/content/Context;I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-string/jumbo v1, "com.twitter.android.widget.single.stop_widget"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    const-string/jumbo v1, "com.twitter.android.widget.single.stop_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method static synthetic c(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b(I)V

    return-void
.end method

.method private declared-synchronized d(I)V
    .locals 7

    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    invoke-static {p0, v0}, Lcom/twitter/android/samsung/single/k;->b(Landroid/content/Context;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    :cond_1
    :try_start_1
    new-instance v2, Lcom/twitter/android/samsung/data/f;

    const/4 v0, 0x0

    sget-object v1, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;->a:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;

    const/4 v3, 0x0

    invoke-direct {v2, v0, v1, v3}, Lcom/twitter/android/samsung/data/f;-><init>(Ljava/lang/String;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$RequestType;I)V

    invoke-virtual {p0, v2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Lcom/twitter/android/samsung/data/f;)Lcom/twitter/android/samsung/data/e;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/twitter/android/samsung/data/e;

    invoke-direct {v0}, Lcom/twitter/android/samsung/data/e;-><init>()V

    new-instance v1, Lcom/twitter/android/samsung/data/r;

    invoke-direct {v1, p0, p1}, Lcom/twitter/android/samsung/data/r;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;I)V

    invoke-virtual {v0, v1}, Lcom/twitter/android/samsung/data/e;->a(Lcom/twitter/android/samsung/data/k;)V

    iget-object v1, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/twitter/library/util/Util;->g:Ljava/util/Random;

    invoke-virtual {v0}, Ljava/util/Random;->nextLong()J

    move-result-wide v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    new-instance v0, Lcom/twitter/android/samsung/data/h;

    new-instance v6, Lcom/twitter/android/samsung/data/s;

    invoke-direct {v6, p0, v2, p1}, Lcom/twitter/android/samsung/data/s;-><init>(Lcom/twitter/android/samsung/data/WidgetDataUpdateService;Lcom/twitter/android/samsung/data/f;I)V

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/twitter/android/samsung/data/h;-><init>(Landroid/content/Context;Lcom/twitter/android/samsung/data/f;JLjava/lang/String;Lcom/twitter/android/samsung/data/g;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/samsung/data/h;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private e(I)V
    .locals 2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sget-object v1, Lcom/twitter/android/samsung/single/TwitterWidgetProvider;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string/jumbo v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/android/samsung/data/f;)Lcom/twitter/android/samsung/data/e;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/f;

    invoke-virtual {v0, p1}, Lcom/twitter/android/samsung/data/f;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->b:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/samsung/data/e;

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "com.twitter.android.widget.single.update_logged_out"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-direct {p0, p3}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(I)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0

    :cond_1
    const-string/jumbo v1, "com.twitter.android.widget.single.stop_widget"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string/jumbo v0, "com.twitter.android.widget.single.stop_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->stopSelf(I)V

    goto :goto_0

    :cond_2
    const-string/jumbo v1, "com.twitter.android.widget.single.update_discover"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1, p3}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService;->a(Landroid/content/Intent;I)V

    goto :goto_0
.end method
