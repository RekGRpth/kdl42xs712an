.class Lcom/twitter/android/samsung/data/b;
.super Landroid/os/AsyncTask;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private a:Lcom/twitter/android/samsung/data/f;

.field private b:Lcom/twitter/library/network/OAuthToken;

.field private c:Lcom/twitter/android/samsung/data/g;

.field private d:Landroid/content/Context;

.field private e:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/android/samsung/data/f;Lcom/twitter/library/network/OAuthToken;Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;Lcom/twitter/android/samsung/data/g;)V
    .locals 0

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/samsung/data/b;->d:Landroid/content/Context;

    iput-object p2, p0, Lcom/twitter/android/samsung/data/b;->a:Lcom/twitter/android/samsung/data/f;

    iput-object p5, p0, Lcom/twitter/android/samsung/data/b;->c:Lcom/twitter/android/samsung/data/g;

    iput-object p3, p0, Lcom/twitter/android/samsung/data/b;->b:Lcom/twitter/library/network/OAuthToken;

    iput-object p4, p0, Lcom/twitter/android/samsung/data/b;->e:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Lcom/twitter/library/api/search/d;
    .locals 14

    iget-object v0, p0, Lcom/twitter/android/samsung/data/b;->d:Landroid/content/Context;

    invoke-static {v0}, Lcom/twitter/library/network/aa;->a(Landroid/content/Context;)Lcom/twitter/library/network/aa;

    move-result-object v0

    new-instance v7, Lcom/twitter/library/network/n;

    iget-object v1, p0, Lcom/twitter/android/samsung/data/b;->b:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v7, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    new-instance v1, Ljava/lang/StringBuilder;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "discover"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "universal"

    aput-object v4, v2, v3

    invoke-static {v0, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/CharSequence;)V

    const-string/jumbo v0, ".json"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v0, "include_user_entities"

    const/4 v1, 0x1

    invoke-static {v8, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const/16 v0, 0x1c

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v9

    iget-object v0, p0, Lcom/twitter/android/samsung/data/b;->d:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/samsung/data/b;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/twitter/android/samsung/data/b;->a:Lcom/twitter/android/samsung/data/f;

    iget-object v2, v2, Lcom/twitter/android/samsung/data/f;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/android/samsung/model/g;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/twitter/android/samsung/model/g;->a(Landroid/accounts/AccountManager;Landroid/accounts/Account;)Lcom/twitter/library/api/TwitterUser;

    move-result-object v0

    iget-wide v3, v0, Lcom/twitter/library/api/TwitterUser;->userId:J

    iget-object v0, p0, Lcom/twitter/android/samsung/data/b;->d:Landroid/content/Context;

    invoke-static {v0, v3, v4}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    const/16 v1, 0x13

    invoke-virtual {v0, v3, v4, v1}, Lcom/twitter/library/provider/az;->h(JI)J

    move-result-wide v5

    sget-object v1, Lcom/twitter/android/samsung/data/c;->a:[I

    iget-object v2, p0, Lcom/twitter/android/samsung/data/b;->e:Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;

    invoke-virtual {v2}, Lcom/twitter/android/samsung/data/WidgetDataUpdateService$FetchType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    new-instance v1, Lcom/twitter/library/network/d;

    iget-object v2, p0, Lcom/twitter/android/samsung/data/b;->d:Landroid/content/Context;

    invoke-direct {v1, v2, v8}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v9}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/search/d;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x1

    const-wide/16 v8, 0x0

    const-wide/16 v10, 0x0

    const/4 v12, 0x1

    const/4 v13, 0x0

    move-object v1, v0

    invoke-virtual/range {v1 .. v13}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/search/d;JZZZJJZZ)I

    :goto_1
    return-object v2

    :pswitch_0
    const/4 v1, 0x6

    const/16 v2, 0x13

    invoke-virtual/range {v0 .. v6}, Lcom/twitter/library/provider/az;->a(IIJJ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "next_cursor"

    invoke-static {v8, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    const/4 v1, 0x5

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/twitter/library/provider/az;->a(IIJ)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    const-string/jumbo v2, "prev_cursor"

    invoke-static {v8, v2, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    invoke-virtual {v9}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    if-eqz v0, :cond_4

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/al;

    iget v3, v0, Lcom/twitter/library/api/al;->a:I

    const/16 v4, 0x58

    if-ne v3, v4, :cond_3

    invoke-static {v1}, Lcom/twitter/library/network/aa;->a(Lcom/twitter/internal/network/HttpOperation;)Lcom/twitter/library/api/RateLimit;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-wide v3, v0, Lcom/twitter/library/api/RateLimit;->b:J

    iget-object v0, p0, Lcom/twitter/android/samsung/data/b;->c:Lcom/twitter/android/samsung/data/g;

    invoke-interface {v0, v3, v4}, Lcom/twitter/android/samsung/data/g;->a(J)V

    goto :goto_2

    :cond_3
    iget-object v3, p0, Lcom/twitter/android/samsung/data/b;->c:Lcom/twitter/android/samsung/data/g;

    iget v4, v0, Lcom/twitter/library/api/al;->a:I

    iget-object v0, v0, Lcom/twitter/library/api/al;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-interface {v3, v4, v0, v5}, Lcom/twitter/android/samsung/data/g;->a(ILjava/lang/String;Ljava/lang/Exception;)V

    goto :goto_2

    :cond_4
    const/4 v2, 0x0

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method

.method protected declared-synchronized a(Lcom/twitter/library/api/search/d;)V
    .locals 1

    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/twitter/android/samsung/data/b;->c:Lcom/twitter/android/samsung/data/g;

    invoke-interface {v0, p1}, Lcom/twitter/android/samsung/data/g;->a(Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/samsung/data/b;->a([Ljava/lang/Void;)Lcom/twitter/library/api/search/d;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Lcom/twitter/library/api/search/d;

    invoke-virtual {p0, p1}, Lcom/twitter/android/samsung/data/b;->a(Lcom/twitter/library/api/search/d;)V

    return-void
.end method
