.class public Lcom/twitter/android/samsung/data/i;
.super Ljava/lang/Object;
.source "Twttr"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xe
.end annotation

.annotation runtime Ljava/lang/Deprecated;
.end annotation


# static fields
.field public static final a:[B

.field public static final b:[B

.field public static final c:Ljava/lang/String;

.field public static final d:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/16 v1, 0x11

    const/16 v0, 0x16

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/twitter/android/samsung/data/i;->a:[B

    const/16 v0, 0x29

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/twitter/android/samsung/data/i;->b:[B

    sget-object v0, Lcom/twitter/android/samsung/data/i;->a:[B

    invoke-static {v1, v0}, Lcom/twitter/android/samsung/data/i;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/data/i;->c:Ljava/lang/String;

    sget-object v0, Lcom/twitter/android/samsung/data/i;->b:[B

    invoke-static {v1, v0}, Lcom/twitter/android/samsung/data/i;->a(B[B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/twitter/android/samsung/data/i;->d:Ljava/lang/String;

    return-void

    nop

    :array_0
    .array-data 1
        -0x64t
        -0x64t
        -0x51t
        -0x3ct
        -0x27t
        -0x41t
        -0x25t
        -0x33t
        -0x3at
        -0x43t
        -0x1ft
        -0x69t
        -0x68t
        -0x3bt
        -0x23t
        -0x20t
        -0x5ft
        -0x57t
        -0x5at
        -0x51t
        -0x56t
        -0x30t
    .end array-data

    nop

    :array_1
    .array-data 1
        -0x25t
        -0x52t
        -0x5et
        -0x60t
        -0x3bt
        -0x31t
        -0x5dt
        -0x3et
        -0x62t
        -0x24t
        -0x5at
        -0x60t
        -0x57t
        -0x25t
        -0x20t
        -0x35t
        -0x52t
        -0x59t
        -0x3ct
        -0x63t
        -0x62t
        -0x68t
        -0x59t
        -0x3ft
        -0x35t
        -0x31t
        -0x66t
        -0x53t
        -0x3ct
        -0x30t
        -0x5dt
        -0x43t
        -0x54t
        -0x69t
        -0x52t
        -0x36t
        -0x48t
        -0x66t
        -0x39t
        -0x52t
        -0x44t
    .end array-data
.end method

.method public static a(B[B)Ljava/lang/String;
    .locals 4

    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p1

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    array-length v2, p1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-byte v3, p1, v0

    sub-int v3, p0, v3

    int-to-char v3, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
