.class public Lcom/twitter/android/cq;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:J

.field public b:J


# direct methods
.method constructor <init>(JJ)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide p1, p0, Lcom/twitter/android/cq;->a:J

    iput-wide p3, p0, Lcom/twitter/android/cq;->b:J

    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v0, 0x0

    instance-of v1, p1, Lcom/twitter/android/cq;

    if-eqz v1, :cond_0

    check-cast p1, Lcom/twitter/android/cq;

    iget-wide v1, p0, Lcom/twitter/android/cq;->a:J

    iget-wide v3, p1, Lcom/twitter/android/cq;->a:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-wide v1, p0, Lcom/twitter/android/cq;->b:J

    iget-wide v3, p1, Lcom/twitter/android/cq;->b:J

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hashCode()I
    .locals 3

    iget-wide v0, p0, Lcom/twitter/android/cq;->a:J

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->hashCode()I

    move-result v0

    iget-wide v1, p0, Lcom/twitter/android/cq;->b:J

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method
