.class Lcom/twitter/android/c;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/AccountSettingsActivity;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>(Lcom/twitter/android/AccountSettingsActivity;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/c;->b:Ljava/lang/String;

    invoke-static {}, Landroid/content/ContentResolver;->getMasterSyncAutomatically()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/c;->c:Z

    return-void
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 8

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v7, 0x0

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/AccountSettingsActivity;->d:Z

    if-eqz v0, :cond_0

    new-array v0, v4, [Ljava/lang/Boolean;

    iget-object v1, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-object v2, p0, Lcom/twitter/android/c;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v0, v7

    invoke-virtual {p0, v0}, Lcom/twitter/android/c;->publishProgress([Ljava/lang/Object;)V

    :cond_0
    iget-object v6, p0, Lcom/twitter/android/c;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/AccountSettingsActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/i;->a:Landroid/net/Uri;

    invoke-static {v1, v6}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v4, [Ljava/lang/String;

    const-string/jumbo v4, "interval"

    aput-object v4, v2, v7

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    const/16 v0, 0x3c

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1, v7}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_2
    iput v0, p0, Lcom/twitter/android/c;->e:I

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    sget-object v1, Lcom/twitter/library/provider/w;->c:Ljava/lang/String;

    invoke-static {v0, v6, v1}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/c;->d:Z

    return-object v3
.end method

.method protected a(Ljava/lang/Void;)V
    .locals 6

    const/4 v2, 0x1

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/AccountSettingsActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/twitter/android/c;->d:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-boolean v0, p0, Lcom/twitter/android/c;->c:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    const v1, 0x7f0f0427    # com.twitter.android.R.string.settings_sync_data_summary_master_off

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(I)V

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v1, "polling_interval"

    invoke-virtual {v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    iget v1, p0, Lcom/twitter/android/c;->e:I

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-virtual {v0}, Lcom/twitter/android/AccountSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-object v1, v1, Lcom/twitter/android/AccountSettingsActivity;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->j()Lcom/twitter/library/api/UserSettings;

    move-result-object v4

    if-eqz v4, :cond_5

    iget-boolean v0, v4, Lcom/twitter/library/api/UserSettings;->j:Z

    if-eqz v0, :cond_5

    move v1, v2

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v5, "protected"

    invoke-virtual {v0, v5}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    if-eqz v4, :cond_6

    iget-boolean v0, v4, Lcom/twitter/library/api/UserSettings;->i:Z

    if-eqz v0, :cond_6

    move v1, v2

    :goto_3
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v5, "discoverable_by_email"

    invoke-virtual {v0, v5}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/AccountSettingsActivity;->a(Lcom/twitter/android/AccountSettingsActivity;)Lcom/twitter/library/util/z;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/util/z;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v4, :cond_7

    iget-boolean v0, v4, Lcom/twitter/library/api/UserSettings;->l:Z

    if-eqz v0, :cond_7

    move v1, v2

    :goto_4
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v5, "discoverable_by_mobile_phone"

    invoke-virtual {v0, v5}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_1
    if-eqz v4, :cond_8

    iget-boolean v0, v4, Lcom/twitter/library/api/UserSettings;->k:Z

    if-eqz v0, :cond_8

    move v1, v2

    :goto_5
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v5, "display_sensitive_media"

    invoke-virtual {v0, v5}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-static {}, Lcom/twitter/library/client/App;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v4, :cond_9

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/AccountSettingsActivity;->b(Lcom/twitter/android/AccountSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "dm_event_api"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_9

    move v1, v2

    :goto_6
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v5, "use_dm_event_api"

    invoke-virtual {v0, v5}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    if-eqz v4, :cond_a

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/AccountSettingsActivity;->b(Lcom/twitter/android/AccountSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v0

    const-string/jumbo v1, "group_dms"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_a

    :goto_7
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v1, "enable_group_dms"

    invoke-virtual {v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v1, "allow_media_tagging"

    invoke-virtual {v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/ListPreference;

    if-eqz v4, :cond_3

    iget-object v1, v4, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    iget-object v1, v4, Lcom/twitter/library/api/UserSettings;->n:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->a(Landroid/preference/ListPreference;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-boolean v1, p0, Lcom/twitter/android/c;->d:Z

    iput-boolean v1, v0, Lcom/twitter/android/AccountSettingsActivity;->a:Z

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget v1, p0, Lcom/twitter/android/c;->e:I

    iput v1, v0, Lcom/twitter/android/AccountSettingsActivity;->c:I

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-boolean v1, p0, Lcom/twitter/android/c;->d:Z

    invoke-virtual {v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->a(Z)V

    goto/16 :goto_1

    :cond_5
    move v1, v3

    goto/16 :goto_2

    :cond_6
    move v1, v3

    goto/16 :goto_3

    :cond_7
    move v1, v3

    goto/16 :goto_4

    :cond_8
    move v1, v3

    goto/16 :goto_5

    :cond_9
    move v1, v3

    goto :goto_6

    :cond_a
    move v2, v3

    goto :goto_7
.end method

.method protected varargs a([Ljava/lang/Boolean;)V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    const/4 v1, 0x0

    aget-object v1, p1, v1

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->b(Z)V

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/c;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/c;->a(Ljava/lang/Void;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/c;->a:Lcom/twitter/android/AccountSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/AccountSettingsActivity;->e:Landroid/preference/CheckBoxPreference;

    iget-boolean v1, p0, Lcom/twitter/android/c;->c:Z

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    return-void
.end method

.method protected synthetic onProgressUpdate([Ljava/lang/Object;)V
    .locals 0

    check-cast p1, [Ljava/lang/Boolean;

    invoke-virtual {p0, p1}, Lcom/twitter/android/c;->a([Ljava/lang/Boolean;)V

    return-void
.end method
