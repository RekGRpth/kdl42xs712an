.class Lcom/twitter/android/a;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/android/util/e;


# instance fields
.field final synthetic a:Lcom/twitter/android/AccountSettingsActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/AccountSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/a;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Z)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/a;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/AccountSettingsActivity;->a(Lcom/twitter/android/AccountSettingsActivity;)Lcom/twitter/library/util/z;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, p1, v1, v2}, Lcom/twitter/library/util/z;->a(ZJ)V

    iget-object v0, p0, Lcom/twitter/android/a;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v1, "discoverable_by_mobile_phone"

    invoke-virtual {v0, v1}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/a;->a:Lcom/twitter/android/AccountSettingsActivity;

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/a;->a:Lcom/twitter/android/AccountSettingsActivity;

    const-string/jumbo v2, "settings_other"

    invoke-virtual {v0, v2}, Lcom/twitter/android/AccountSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0
.end method
