.class Lcom/twitter/android/ef;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;


# instance fields
.field final synthetic a:Landroid/database/Cursor;

.field final synthetic b:Lcom/twitter/android/ee;


# direct methods
.method constructor <init>(Lcom/twitter/android/ee;Landroid/database/Cursor;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iput-object p2, p0, Lcom/twitter/android/ef;->a:Landroid/database/Cursor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGlobalLayout()V
    .locals 6

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeGlobalOnLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->removeAllViews()V

    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->f(Lcom/twitter/android/DMInboxFragment;)I

    move-result v0

    move v1, v0

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/ef;->a:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_1

    if-lez v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/ef;->a:Landroid/database/Cursor;

    const/4 v2, 0x7

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v3, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DMInboxFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v4, 0x7f030052    # com.twitter.android.R.layout.dm_request_avatar_preview

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/widget/UserView;

    invoke-static {v3, v0}, Lcom/twitter/android/DMInboxFragment;->a(Lcom/twitter/android/DMInboxFragment;Lcom/twitter/library/widget/UserView;)Lcom/twitter/library/widget/UserView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/widget/UserView;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/zx;

    iput-object v2, v0, Lcom/twitter/android/zx;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->g(Lcom/twitter/android/DMInboxFragment;)Lcom/twitter/android/client/c;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/twitter/library/widget/UserView;->setUserImage(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_1

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/ef;->b:Lcom/twitter/android/ee;

    iget-object v0, v0, Lcom/twitter/android/ee;->a:Lcom/twitter/android/DMInboxFragment;

    invoke-static {v0}, Lcom/twitter/android/DMInboxFragment;->e(Lcom/twitter/android/DMInboxFragment;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/ef;->a:Landroid/database/Cursor;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    return-void
.end method
