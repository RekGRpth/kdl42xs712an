.class Lcom/twitter/android/hx;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;


# instance fields
.field final synthetic a:Lcom/twitter/android/hu;


# direct methods
.method constructor <init>(Lcom/twitter/android/hu;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 7

    const/16 v2, 0x8

    const/4 v5, 0x0

    const/4 v1, 0x0

    invoke-virtual {p1, p3}, Landroid/widget/AdapterView;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setEnabled(Z)V

    const-string/jumbo v3, "Source"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    move v0, v1

    :goto_1
    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->e(Lcom/twitter/android/hu;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Spinner;->getAdapter()Landroid/widget/SpinnerAdapter;

    move-result-object v3

    invoke-interface {v3}, Landroid/widget/SpinnerAdapter;->getCount()I

    move-result v3

    if-ge v0, v3, :cond_1

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getProvider()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v4}, Lcom/twitter/android/hu;->e(Lcom/twitter/android/hu;)Landroid/widget/Spinner;

    move-result-object v4

    invoke-virtual {v4, v0}, Landroid/widget/Spinner;->getItemAtPosition(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->e(Lcom/twitter/android/hu;)Landroid/widget/Spinner;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/widget/Spinner;->setSelection(I)V

    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setVisibility(I)V

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->e(Lcom/twitter/android/hu;)Landroid/widget/Spinner;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setVisibility(I)V

    goto :goto_0

    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_3
    const-string/jumbo v3, "Latitude"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getLatitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    move v6, v2

    move v2, v1

    move v1, v6

    goto :goto_2

    :cond_4
    const-string/jumbo v3, "Longitude"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getLongitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setEnabled(Z)V

    move v6, v2

    move v2, v1

    move v1, v6

    goto :goto_2

    :cond_5
    const-string/jumbo v3, "H-Accuracy"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getAccuracy()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move v6, v2

    move v2, v1

    move v1, v6

    goto/16 :goto_2

    :cond_6
    const-string/jumbo v3, "Altitude"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->hasAltitude()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getAltitude()D

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(D)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_3
    move v6, v2

    move v2, v1

    move v1, v6

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_8
    const-string/jumbo v3, "Speed"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_9

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getSpeed()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_4
    move v6, v2

    move v2, v1

    move v1, v6

    goto/16 :goto_2

    :cond_9
    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_4

    :cond_a
    const-string/jumbo v3, "Bearing"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_c

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getBearing()F

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(F)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_5
    move v6, v2

    move v2, v1

    move v1, v6

    goto/16 :goto_2

    :cond_b
    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto :goto_5

    :cond_c
    const-string/jumbo v3, "Timestamp"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v0}, Lcom/twitter/android/hu;->d(Lcom/twitter/android/hu;)Landroid/widget/EditText;

    move-result-object v0

    iget-object v3, p0, Lcom/twitter/android/hx;->a:Lcom/twitter/android/hu;

    invoke-static {v3}, Lcom/twitter/android/hu;->b(Lcom/twitter/android/hu;)Landroid/location/Location;

    move-result-object v3

    invoke-virtual {v3}, Landroid/location/Location;->getTime()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    move v6, v2

    move v2, v1

    move v1, v6

    goto/16 :goto_2

    :cond_d
    move v1, v2

    goto/16 :goto_2
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    return-void
.end method
