.class Lcom/twitter/android/bu;
.super Landroid/support/v4/app/FragmentPagerAdapter;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/CardPreviewerActivity;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/twitter/android/CardPreviewerActivity;Landroid/support/v4/app/FragmentManager;)V
    .locals 6

    iput-object p1, p0, Lcom/twitter/android/bu;->a:Lcom/twitter/android/CardPreviewerActivity;

    invoke-direct {p0, p2}, Landroid/support/v4/app/FragmentPagerAdapter;-><init>(Landroid/support/v4/app/FragmentManager;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/bu;->b:Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/twitter/android/CardPreviewerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v2, "host"

    invoke-static {p1}, Lcom/twitter/android/CardPreviewerActivity;->a(Lcom/twitter/android/CardPreviewerActivity;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/twitter/android/bu;->b:Ljava/util/ArrayList;

    new-instance v3, Lcom/twitter/android/bv;

    const-class v4, Lcom/twitter/android/CardPreviewerFragment;

    const v5, 0x7f0f008f    # com.twitter.android.R.string.cards_previewer_title

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v3, p0, v4, v1, v5}, Lcom/twitter/android/bv;-><init>(Lcom/twitter/android/bu;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/twitter/android/bu;->b:Ljava/util/ArrayList;

    new-instance v2, Lcom/twitter/android/bv;

    const-class v3, Lcom/twitter/android/CardDebugFragment;

    const/4 v4, 0x0

    const v5, 0x7f0f008c    # com.twitter.android.R.string.cards_debug_log_title

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, p0, v3, v4, v0}, Lcom/twitter/android/bv;-><init>(Lcom/twitter/android/bu;Ljava/lang/Class;Landroid/os/Bundle;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    return-void
.end method


# virtual methods
.method public a(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/bu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bv;

    iget-object v0, v0, Lcom/twitter/android/bv;->b:Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/bu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getItem(I)Landroid/support/v4/app/Fragment;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/bu;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/bv;

    iget-object v1, p0, Lcom/twitter/android/bu;->a:Lcom/twitter/android/CardPreviewerActivity;

    iget-object v2, v0, Lcom/twitter/android/bv;->c:Ljava/lang/Class;

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    iget-object v0, v0, Lcom/twitter/android/bv;->a:Landroid/os/Bundle;

    invoke-static {v1, v2, v0}, Landroid/support/v4/app/Fragment;->instantiate(Landroid/content/Context;Ljava/lang/String;Landroid/os/Bundle;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    return-object v0
.end method

.method public synthetic getPageTitle(I)Ljava/lang/CharSequence;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/twitter/android/bu;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
