.class public Lcom/twitter/android/PhotoGridActivity;
.super Lcom/twitter/android/client/BaseFragmentActivity;
.source "Twttr"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/client/BaseFragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Landroid/os/Bundle;)Lcom/twitter/android/client/z;
    .locals 2

    new-instance v0, Lcom/twitter/android/client/z;

    invoke-direct {v0, p0}, Lcom/twitter/android/client/z;-><init>(Lcom/twitter/android/client/BaseFragmentActivity;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/z;->a(Z)V

    return-object v0
.end method

.method public a(Landroid/os/Bundle;Lcom/twitter/android/client/z;)V
    .locals 3

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "user_name"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/twitter/android/PhotoGridActivity;->setTitle(Ljava/lang/CharSequence;)V

    const v1, 0x7f0f0488    # com.twitter.android.R.string.subtitle_photos

    invoke-virtual {p0, v1}, Lcom/twitter/android/PhotoGridActivity;->e(I)V

    if-nez p1, :cond_0

    new-instance v1, Lcom/twitter/android/PhotoGridFragment;

    invoke-direct {v1}, Lcom/twitter/android/PhotoGridFragment;-><init>()V

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/twitter/android/PhotoGridFragment;->a(Landroid/content/Intent;Z)Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/PhotoGridFragment;->setArguments(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/PhotoGridActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    const v2, 0x7f0900e3    # com.twitter.android.R.id.fragment_container

    invoke-virtual {v0, v2, v1}, Landroid/support/v4/app/FragmentTransaction;->add(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    :cond_0
    return-void
.end method
