.class final Lcom/twitter/android/aw;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/BaseEditProfileActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/BaseEditProfileActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/BaseEditProfileActivity;Lcom/twitter/android/av;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/aw;-><init>(Lcom/twitter/android/BaseEditProfileActivity;)V

    return-void
.end method


# virtual methods
.method public a(Ljava/util/HashMap;)V
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->l:Lcom/twitter/library/util/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->l:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_2

    iget-object v1, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    iget-object v1, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseEditProfileActivity;->n:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->m:Lcom/twitter/library/util/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/android/BaseEditProfileActivity;->m:Lcom/twitter/library/util/m;

    invoke-virtual {p1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/util/ae;

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v0, v0, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v0, Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    iget-object v2, v2, Lcom/twitter/android/BaseEditProfileActivity;->o:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getId()I

    move-result v2

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/BaseEditProfileActivity;->a(Landroid/graphics/Bitmap;I)V

    :cond_1
    return-void

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/aw;->a:Lcom/twitter/android/BaseEditProfileActivity;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/twitter/android/BaseEditProfileActivity;->h:Z

    goto :goto_0
.end method
