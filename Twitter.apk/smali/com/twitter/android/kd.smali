.class final Lcom/twitter/android/kd;
.super Landroid/os/Handler;
.source "Twttr"


# instance fields
.field private final a:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/kd;->a:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method private a(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;IILjava/lang/String;Lcom/twitter/library/client/aa;)V
    .locals 7

    const-wide/16 v2, 0x0

    new-instance v6, Lcom/twitter/library/client/f;

    invoke-virtual {p7}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "hometab"

    invoke-direct {v6, p1, v0, v1}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v6, p6, v2, v3}, Lcom/twitter/library/client/f;->a(Ljava/lang/String;J)J

    move-result-wide v0

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    const-wide/16 v4, -0x1

    :goto_0
    move-object v0, p2

    move-object v1, p3

    move v2, p4

    move v3, p5

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;IIJ)Ljava/lang/String;

    invoke-virtual {v6}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    invoke-virtual {v0, p6, v1, v2}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    return-void

    :cond_0
    invoke-static {v0, v1}, Lcom/twitter/library/util/Util;->b(J)J

    move-result-wide v4

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/twitter/android/kd;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-static {v1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v2

    invoke-static {v1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v7

    iget-object v3, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v3, Lcom/twitter/library/client/Session;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const/16 v0, 0x190

    invoke-virtual {v2, v3, v4, v4, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;III)Ljava/lang/String;

    new-instance v0, Lcom/twitter/library/client/f;

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "hometab"

    invoke-direct {v0, v1, v2, v3}, Lcom/twitter/library/client/f;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->b()Lcom/twitter/library/client/f;

    move-result-object v0

    const-string/jumbo v1, "ft"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/library/client/f;->b(Ljava/lang/String;J)Lcom/twitter/library/client/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/client/f;->d()V

    goto :goto_0

    :pswitch_1
    const/4 v4, 0x1

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->r()I

    move-result v5

    const-string/jumbo v6, "taut"

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/kd;->a(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;IILjava/lang/String;Lcom/twitter/library/client/aa;)V

    goto :goto_0

    :pswitch_2
    const/4 v4, 0x3

    invoke-virtual {v2}, Lcom/twitter/android/client/c;->v()I

    move-result v5

    const-string/jumbo v6, "tatt"

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/kd;->a(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;IILjava/lang/String;Lcom/twitter/library/client/aa;)V

    goto :goto_0

    :pswitch_3
    const/4 v4, 0x4

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->v()I

    move-result v5

    const-string/jumbo v6, "taot"

    move-object v0, p0

    invoke-direct/range {v0 .. v7}, Lcom/twitter/android/kd;->a(Landroid/content/Context;Lcom/twitter/android/client/c;Lcom/twitter/library/client/Session;IILjava/lang/String;Lcom/twitter/library/client/aa;)V

    goto :goto_0

    :pswitch_4
    invoke-virtual {v2}, Lcom/twitter/android/client/c;->m()Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
