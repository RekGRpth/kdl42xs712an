.class Lcom/twitter/android/xy;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/twitter/library/service/c;


# instance fields
.field private final a:Lcom/twitter/android/TweetSettingsActivity;


# direct methods
.method public constructor <init>(Lcom/twitter/android/TweetSettingsActivity;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/twitter/android/xy;->a:Lcom/twitter/android/TweetSettingsActivity;

    return-void
.end method


# virtual methods
.method public a(IILcom/twitter/library/service/b;)V
    .locals 0

    return-void
.end method

.method public b(IILcom/twitter/library/service/b;)V
    .locals 7

    const/4 v1, 0x1

    invoke-virtual {p3}, Lcom/twitter/library/service/b;->c()Lcom/twitter/internal/android/service/k;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/internal/android/service/k;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/service/e;

    iget-object v2, p0, Lcom/twitter/android/xy;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-virtual {v2}, Lcom/twitter/android/TweetSettingsActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/twitter/android/xy;->a:Lcom/twitter/android/TweetSettingsActivity;

    invoke-virtual {v3}, Lcom/twitter/android/TweetSettingsActivity;->c()Lcom/twitter/library/client/aa;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/twitter/library/platform/PushService;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v4

    iget-object v5, p3, Lcom/twitter/library/service/b;->k:Landroid/os/Bundle;

    const/4 v6, 0x3

    if-ne p1, v6, :cond_0

    invoke-virtual {v0}, Lcom/twitter/library/service/e;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v4, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, "notification_not_enabled"

    invoke-virtual {v5, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    if-nez v4, :cond_4

    :cond_2
    invoke-static {v2}, Lcom/twitter/android/client/aw;->a(Landroid/content/Context;)Lcom/twitter/android/client/aw;

    move-result-object v2

    if-nez v4, :cond_3

    move v0, v1

    :goto_1
    invoke-virtual {v2, v3, v0}, Lcom/twitter/android/client/aw;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    goto :goto_1

    :cond_4
    const v0, 0x7f0f00ee    # com.twitter.android.R.string.default_error_message

    invoke-static {v2, v0, v1}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
