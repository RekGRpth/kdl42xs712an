.class Lcom/twitter/android/nw;
.super Landroid/content/BroadcastReceiver;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/NotificationSettingsActivity;


# direct methods
.method private constructor <init>(Lcom/twitter/android/NotificationSettingsActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/nw;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/twitter/android/NotificationSettingsActivity;Lcom/twitter/android/nu;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/nw;-><init>(Lcom/twitter/android/NotificationSettingsActivity;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    sget-object v0, Lcom/twitter/library/platform/PushService;->d:Ljava/lang/String;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    iget-object v1, p0, Lcom/twitter/android/nw;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->a(Lcom/twitter/android/NotificationSettingsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    iget-object v1, p0, Lcom/twitter/android/nw;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->a(Lcom/twitter/android/NotificationSettingsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v1, p0, Lcom/twitter/android/nw;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iput-boolean v0, v1, Lcom/twitter/android/NotificationSettingsActivity;->B:Z

    return-void
.end method
