.class public Lcom/twitter/android/MediaTagFragment;
.super Landroid/support/v4/app/Fragment;
.source "Twttr"

# interfaces
.implements Landroid/text/TextWatcher;
.implements Lcom/twitter/android/widget/cr;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field private final a:Lcom/twitter/android/lp;

.field private final b:Lcom/twitter/android/lo;

.field private c:Lcom/twitter/android/client/c;

.field private d:Lcom/twitter/library/client/aa;

.field private e:Z

.field private f:Lcom/twitter/android/widget/DraggableHeaderLayout;

.field private g:Landroid/widget/ImageView;

.field private h:Landroid/view/View;

.field private i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

.field private j:Landroid/widget/TextView;

.field private k:Landroid/widget/ListView;

.field private l:I

.field private m:Lcom/twitter/android/lq;

.field private n:Lcom/twitter/android/PhotoSelectHelper;

.field private o:Z

.field private p:Ljava/util/List;

.field private q:I


# direct methods
.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    new-instance v0, Lcom/twitter/android/lp;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/lp;-><init>(Lcom/twitter/android/MediaTagFragment;Lcom/twitter/android/le;)V

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->a:Lcom/twitter/android/lp;

    new-instance v0, Lcom/twitter/android/lo;

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/lo;-><init>(Lcom/twitter/android/MediaTagFragment;Lcom/twitter/android/le;)V

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->b:Lcom/twitter/android/lo;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/MediaTagFragment;->q:I

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MediaTagFragment;I)I
    .locals 0

    iput p1, p0, Lcom/twitter/android/MediaTagFragment;->q:I

    return p1
.end method

.method static synthetic a(Lcom/twitter/android/MediaTagFragment;)Lcom/twitter/android/widget/DraggableHeaderLayout;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->f:Lcom/twitter/android/widget/DraggableHeaderLayout;

    return-object v0
.end method

.method private a(Landroid/graphics/Bitmap;)V
    .locals 4

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v0

    int-to-float v0, v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->g:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->k:Landroid/widget/ListView;

    invoke-virtual {v2}, Landroid/widget/ListView;->getWidth()I

    move-result v2

    int-to-float v3, v2

    div-float v0, v3, v0

    float-to-int v0, v0

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->g:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, p1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->f:Lcom/twitter/android/widget/DraggableHeaderLayout;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0c0087    # com.twitter.android.R.dimen.media_tag_compose_visible_image_height

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    sub-int/2addr v0, v2

    invoke-virtual {v1, v0}, Lcom/twitter/android/widget/DraggableHeaderLayout;->setAnchorOffset(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->g:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/twitter/android/MediaTagFragment;Landroid/graphics/Bitmap;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/twitter/android/MediaTagFragment;->a(Landroid/graphics/Bitmap;)V

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MediaTagFragment;Ljava/lang/CharSequence;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/twitter/android/MediaTagFragment;->b(Ljava/lang/CharSequence;I)V

    return-void
.end method

.method private a(Ljava/util/ArrayList;Ljava/lang/String;)V
    .locals 9

    new-instance v1, Landroid/text/SpannableStringBuilder;

    invoke-direct {v1}, Landroid/text/SpannableStringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v5

    new-instance v6, Lcom/twitter/android/ln;

    invoke-direct {v6, v0, v2}, Lcom/twitter/android/ln;-><init>(Lcom/twitter/library/api/MediaTag;Landroid/content/res/Resources;)V

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v8, v0, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v7

    const/16 v8, 0x21

    invoke-virtual {v1, v6, v5, v7, v8}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    iget-wide v5, v0, Lcom/twitter/library/api/MediaTag;->userId:J

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {v0, v3}, Lcom/twitter/android/lq;->a(Ljava/util/Set;)V

    if-eqz p2, :cond_1

    invoke-virtual {v1, p2}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    new-instance v2, Lcom/twitter/android/ll;

    invoke-direct {v2, p0, v1}, Lcom/twitter/android/ll;-><init>(Lcom/twitter/android/MediaTagFragment;Landroid/text/SpannableStringBuilder;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method static synthetic a(Lcom/twitter/android/MediaTagFragment;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/twitter/android/MediaTagFragment;->o:Z

    return p1
.end method

.method private b(Ljava/lang/CharSequence;I)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SuggestionEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v0, p1}, Lcom/twitter/android/widget/SuggestionEditText;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0, p2}, Lcom/twitter/android/widget/SuggestionEditText;->setSelection(I)V

    invoke-virtual {v0, p0}, Lcom/twitter/android/widget/SuggestionEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->h()V

    return-void
.end method

.method static synthetic b(Lcom/twitter/android/MediaTagFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/MediaTagFragment;->e:Z

    return v0
.end method

.method static synthetic c(Lcom/twitter/android/MediaTagFragment;)I
    .locals 1

    iget v0, p0, Lcom/twitter/android/MediaTagFragment;->q:I

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/MediaTagFragment;)Lcom/twitter/android/MediaTagFragment$MediaTagEditText;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    return-object v0
.end method

.method private e()V
    .locals 2

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const-string/jumbo v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0, v1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    return-void
.end method

.method static synthetic e(Lcom/twitter/android/MediaTagFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->g()V

    return-void
.end method

.method private f()V
    .locals 6

    const/4 v3, 0x1

    const/4 v5, 0x0

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->b()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x6

    if-lt v1, v2, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    rsub-int/lit8 v0, v0, 0xa

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0f0271    # com.twitter.android.R.string.media_tag_remaining_max

    new-array v2, v3, [Ljava/lang/Object;

    const/16 v3, 0xa

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v2, v5

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->j:Landroid/widget/TextView;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setVisibility(I)V

    :goto_1
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e0005    # com.twitter.android.R.plurals.media_tag_remaining

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->j:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1
.end method

.method static synthetic f(Lcom/twitter/android/MediaTagFragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->f()V

    return-void
.end method

.method private g()V
    .locals 4

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v2

    const-class v3, Lcom/twitter/android/ln;

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/ln;

    array-length v0, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->h:Landroid/view/View;

    if-eqz v0, :cond_1

    :goto_1
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    return-void

    :cond_0
    move v0, v1

    goto :goto_0

    :cond_1
    const/16 v1, 0x8

    goto :goto_1
.end method

.method static synthetic g(Lcom/twitter/android/MediaTagFragment;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/MediaTagFragment;->o:Z

    return v0
.end method

.method private h()V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0c0084    # com.twitter.android.R.dimen.media_tag_compose_extra_line_spacing

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    new-instance v3, Lcom/twitter/android/lm;

    invoke-direct {v3, p0, v1, v0}, Lcom/twitter/android/lm;-><init>(Lcom/twitter/android/MediaTagFragment;ILandroid/view/ViewGroup$MarginLayoutParams;)V

    invoke-virtual {v2, v3}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->post(Ljava/lang/Runnable;)Z

    return-void
.end method


# virtual methods
.method public a(II)V
    .locals 0

    return-void
.end method

.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {v0, p2}, Lcom/twitter/android/lq;->a(Ljava/util/HashMap;)V

    :cond_0
    return-void
.end method

.method public a(Ljava/lang/CharSequence;)V
    .locals 5

    const/4 v1, 0x1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/twitter/android/provider/SuggestionsProvider;->b(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    iget v3, p0, Lcom/twitter/android/MediaTagFragment;->l:I

    const-string/jumbo v4, "compose_media_tagging"

    invoke-virtual {v0, v2, v1, v3, v4}, Lcom/twitter/android/client/c;->a(Ljava/lang/String;IILjava/lang/String;)Ljava/lang/String;

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(Ljava/lang/CharSequence;I)V
    .locals 13

    const/4 v12, 0x1

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->k:Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getHeaderViewsCount()I

    move-result v0

    sub-int v1, p2, v0

    if-ltz v1, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {v0}, Lcom/twitter/android/lq;->getCount()I

    move-result v0

    if-lt v1, v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v6, Landroid/text/SpannableStringBuilder;

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    const-class v2, Lcom/twitter/android/ln;

    invoke-virtual {v6, v5, v0, v2}, Landroid/text/SpannableStringBuilder;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/ln;

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {v2, v1}, Lcom/twitter/android/lq;->a(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;

    const/4 v2, 0x6

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v2}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v7

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->n(I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string/jumbo v2, "composition::media_tag:taggable_user:click"

    :goto_1
    iget-object v4, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    new-array v9, v12, [Ljava/lang/String;

    aput-object v2, v9, v5

    invoke-virtual {v4, v7, v8, v9}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    invoke-static {v3}, Lcom/twitter/library/provider/ay;->n(I)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v7

    const/4 v3, 0x0

    array-length v9, v0

    move v4, v5

    :goto_2
    if-ge v4, v9, :cond_3

    aget-object v2, v0, v4

    invoke-virtual {v2}, Lcom/twitter/android/ln;->a()Lcom/twitter/library/api/MediaTag;

    move-result-object v10

    iget-wide v10, v10, Lcom/twitter/library/api/MediaTag;->userId:J

    cmp-long v10, v10, v7

    if-nez v10, :cond_6

    :goto_3
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move-object v3, v2

    goto :goto_2

    :cond_2
    const-string/jumbo v2, "composition::media_tag:nontaggable_user:click"

    goto :goto_1

    :cond_3
    if-eqz v3, :cond_4

    const-string/jumbo v0, ""

    invoke-static {v6, v3, v0, v5}, Lcom/twitter/library/util/Util;->a(Landroid/text/Editable;Ljava/lang/Object;Ljava/lang/String;Z)V

    invoke-virtual {v6}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v0

    invoke-direct {p0, v6, v0}, Lcom/twitter/android/MediaTagFragment;->b(Ljava/lang/CharSequence;I)V

    :goto_4
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->c()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/lq;->a(Ljava/util/Set;)V

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->f()V

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->g()V

    goto/16 :goto_0

    :cond_4
    array-length v0, v0

    const/16 v2, 0xa

    if-ge v0, v2, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getSelectionEnd()I

    move-result v0

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->b:Lcom/twitter/android/lo;

    invoke-virtual {v2, v6, v0}, Lcom/twitter/android/lo;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    const/4 v3, 0x3

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/api/MediaTag;

    invoke-direct {v4, v7, v8, v1, v3}, Lcom/twitter/library/api/MediaTag;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/twitter/android/ln;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-direct {v3, v4, v5}, Lcom/twitter/android/ln;-><init>(Lcom/twitter/library/api/MediaTag;Landroid/content/res/Resources;)V

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v2, v0, v4}, Landroid/text/SpannableStringBuilder;->replace(IILjava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    add-int/2addr v0, v2

    add-int/lit8 v0, v0, 0x1

    const/16 v1, 0x21

    invoke-virtual {v6, v3, v2, v0, v1}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    invoke-direct {p0, v6, v0}, Lcom/twitter/android/MediaTagFragment;->b(Ljava/lang/CharSequence;I)V

    iget-boolean v0, p0, Lcom/twitter/android/MediaTagFragment;->o:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0, v12}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->a(Z)V

    :cond_5
    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->e()V

    goto :goto_4

    :cond_6
    move-object v2, v3

    goto/16 :goto_3
.end method

.method public a(Ljava/util/List;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/MediaTagFragment;->p:Ljava/util/List;

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {v0, p1}, Lcom/twitter/android/lq;->a(Ljava/util/List;)V

    :cond_0
    return-void
.end method

.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 10

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-interface {p1, v1}, Landroid/text/Editable;->charAt(I)C

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/util/Util;->a(C)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    iget-boolean v0, v0, Lcom/twitter/android/client/c;->f:Z

    if-eqz v0, :cond_3

    :cond_1
    move v0, v2

    :goto_0
    iget-object v3, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    :goto_1
    invoke-virtual {v3, v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setGravity(I)V

    invoke-interface {p1}, Landroid/text/Editable;->length()I

    move-result v0

    const-class v3, Lcom/twitter/android/ln;

    invoke-interface {p1, v1, v0, v3}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/ln;

    array-length v3, v0

    if-lez v3, :cond_7

    iget-object v3, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v3, p0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->removeTextChangedListener(Landroid/text/TextWatcher;)V

    array-length v5, v0

    move v4, v1

    move v3, v1

    :goto_2
    if-ge v4, v5, :cond_5

    aget-object v6, v0, v4

    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanStart(Ljava/lang/Object;)I

    move-result v7

    invoke-interface {p1, v6}, Landroid/text/Editable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v8

    const/4 v9, -0x1

    if-le v7, v9, :cond_2

    if-lt v8, v7, :cond_2

    invoke-interface {p1, v7, v8}, Landroid/text/Editable;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6}, Lcom/twitter/android/ln;->a()Lcom/twitter/library/api/MediaTag;

    move-result-object v9

    iget-object v9, v9, Lcom/twitter/library/api/MediaTag;->name:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string/jumbo v9, " "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8, v7}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string/jumbo v3, ""

    invoke-static {p1, v6, v3, v1}, Lcom/twitter/library/util/Util;->a(Landroid/text/Editable;Ljava/lang/Object;Ljava/lang/String;Z)V

    move v3, v2

    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    const/4 v0, 0x3

    goto :goto_1

    :cond_5
    if-eqz v3, :cond_6

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->f()V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->c()Ljava/util/Set;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/lq;->a(Ljava/util/Set;)V

    :cond_6
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0, p0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    :cond_7
    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->g()V

    invoke-direct {p0}, Lcom/twitter/android/MediaTagFragment;->h()V

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->d()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    iget-boolean v0, p0, Lcom/twitter/android/MediaTagFragment;->o:Z

    if-nez v0, :cond_9

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->a(Z)V

    :cond_9
    return-void
.end method

.method public b()Ljava/util/ArrayList;
    .locals 5

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Lcom/twitter/android/ln;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/ln;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/twitter/android/ln;->a()Lcom/twitter/library/api/MediaTag;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public c()Ljava/util/Set;
    .locals 6

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v0}, Landroid/text/Editable;->length()I

    move-result v3

    const-class v4, Lcom/twitter/android/ln;

    invoke-interface {v0, v1, v3, v4}, Landroid/text/Editable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/twitter/android/ln;

    array-length v3, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v0, v1

    invoke-virtual {v4}, Lcom/twitter/android/ln;->a()Lcom/twitter/library/api/MediaTag;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/api/MediaTag;->userId:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    return-object v2
.end method

.method public d()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getText()Landroid/text/Editable;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v1}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->getSelectionEnd()I

    move-result v1

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->b:Lcom/twitter/android/lo;

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/lo;->findTokenStart(Ljava/lang/CharSequence;I)I

    move-result v2

    if-ltz v2, :cond_0

    if-le v1, v2, :cond_0

    invoke-interface {v0, v2, v1}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public o_()V
    .locals 0

    return-void
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    if-eqz p1, :cond_2

    const-string/jumbo v0, "tags"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const-string/jumbo v1, "partial_tag"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    :goto_0
    if-nez v1, :cond_0

    if-eqz v0, :cond_1

    :cond_0
    invoke-direct {p0, v1, v0}, Lcom/twitter/android/MediaTagFragment;->a(Ljava/util/ArrayList;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "photo_tags"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    const/4 v1, 0x0

    move-object v2, v1

    move-object v1, v0

    move-object v0, v2

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/MediaTagFragment;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->f()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/lq;

    invoke-direct {v2, v0, v1}, Lcom/twitter/android/lq;-><init>(Landroid/app/Activity;Lcom/twitter/library/api/TwitterUser;)V

    iput-object v2, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    iget-object v2, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    iget-object v3, p0, Lcom/twitter/android/MediaTagFragment;->p:Ljava/util/List;

    invoke-virtual {v2, v3}, Lcom/twitter/android/lq;->a(Ljava/util/List;)V

    iget-boolean v1, v1, Lcom/twitter/library/api/TwitterUser;->isProtected:Z

    iput-boolean v1, p0, Lcom/twitter/android/MediaTagFragment;->e:Z

    invoke-static {v0}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    iget-boolean v1, p0, Lcom/twitter/android/MediaTagFragment;->e:Z

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->a:Lcom/twitter/android/lp;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    :cond_0
    invoke-virtual {v0}, Lcom/twitter/android/client/c;->A()I

    move-result v1

    iput v1, p0, Lcom/twitter/android/MediaTagFragment;->l:I

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    const/4 v6, 0x0

    const v0, 0x7f0300bb    # com.twitter.android.R.layout.media_tag_fragment

    invoke-virtual {p1, v0, p2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/twitter/android/widget/DraggableHeaderLayout;

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->f:Lcom/twitter/android/widget/DraggableHeaderLayout;

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->f:Lcom/twitter/android/widget/DraggableHeaderLayout;

    invoke-virtual {v0}, Lcom/twitter/android/widget/DraggableHeaderLayout;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    new-instance v2, Lcom/twitter/android/le;

    invoke-direct {v2, p0}, Lcom/twitter/android/le;-><init>(Lcom/twitter/android/MediaTagFragment;)V

    invoke-virtual {v0, v2}, Landroid/view/ViewTreeObserver;->addOnGlobalLayoutListener(Landroid/view/ViewTreeObserver$OnGlobalLayoutListener;)V

    const v0, 0x7f0901da    # com.twitter.android.R.id.tag_photo_preview

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->g:Landroid/widget/ImageView;

    const v0, 0x7f0901db    # com.twitter.android.R.id.search_icon

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->h:Landroid/view/View;

    const v0, 0x7f0901dd    # com.twitter.android.R.id.tags_remaining

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->j:Landroid/widget/TextView;

    const v0, 0x7f09011f    # com.twitter.android.R.id.suggestion_list_view

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    const v2, 0x7f03014a    # com.twitter.android.R.layout.suggested_users_label

    invoke-virtual {p1, v2, v0, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;)V

    const v3, 0x7f09028c    # com.twitter.android.R.id.suggested_users_label

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-boolean v3, p0, Lcom/twitter/android/MediaTagFragment;->e:Z

    if-eqz v3, :cond_1

    const v3, 0x7f0f0273    # com.twitter.android.R.string.media_tag_suggestions_protected

    :goto_0
    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    const v3, 0x7f090037    # com.twitter.android.R.id.divider

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    new-instance v5, Lcom/twitter/android/lf;

    invoke-direct {v5, p0, v2, v3, v0}, Lcom/twitter/android/lf;-><init>(Lcom/twitter/android/MediaTagFragment;Landroid/widget/TextView;Landroid/view/View;Landroid/widget/ListView;)V

    invoke-virtual {v4, v5}, Lcom/twitter/android/lq;->a(Lcom/twitter/android/aq;)V

    const v2, 0x7f0901de    # com.twitter.android.R.id.drop_shadow

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    new-instance v3, Lcom/twitter/android/lh;

    invoke-direct {v3, p0, v0, v2}, Lcom/twitter/android/lh;-><init>(Lcom/twitter/android/MediaTagFragment;Landroid/widget/ListView;Landroid/view/View;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnScrollListener(Landroid/widget/AbsListView$OnScrollListener;)V

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->k:Landroid/widget/ListView;

    const v2, 0x7f0901dc    # com.twitter.android.R.id.photo_tag_text

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-static {}, Lcom/twitter/android/widget/ar;->a()Landroid/text/Editable$Factory;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setEditableFactory(Landroid/text/Editable$Factory;)V

    invoke-virtual {v2, v6}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setFilteringEnabled(Z)V

    invoke-virtual {v2, p0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->addTextChangedListener(Landroid/text/TextWatcher;)V

    invoke-virtual {v2, p0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setSuggestionListener(Lcom/twitter/android/widget/cr;)V

    new-instance v3, Lcom/twitter/android/li;

    invoke-direct {v3, p0}, Lcom/twitter/android/li;-><init>(Lcom/twitter/android/MediaTagFragment;)V

    invoke-virtual {v2, v3}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setOnLongClickListener(Landroid/view/View$OnLongClickListener;)V

    new-instance v3, Lcom/twitter/android/lj;

    invoke-direct {v3, p0}, Lcom/twitter/android/lj;-><init>(Lcom/twitter/android/MediaTagFragment;)V

    invoke-virtual {v2, v3}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    invoke-static {}, Lcom/twitter/android/widget/at;->a()Landroid/text/method/MovementMethod;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    iget-object v3, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    iget-boolean v3, v3, Lcom/twitter/android/client/c;->f:Z

    if-eqz v3, :cond_0

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setGravity(I)V

    :cond_0
    invoke-virtual {v2, v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setSuggestionList(Landroid/widget/ListView;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->m:Lcom/twitter/android/lq;

    invoke-virtual {v2, v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->b:Lcom/twitter/android/lo;

    iget-object v3, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    invoke-virtual {v3}, Lcom/twitter/android/client/c;->B()J

    move-result-wide v3

    invoke-virtual {v2, v0, v3, v4}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->a(Landroid/widget/MultiAutoCompleteTextView$Tokenizer;J)V

    iput-object v2, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    return-object v1

    :cond_1
    const v3, 0x7f0f0272    # com.twitter.android.R.string.media_tag_suggestions_default

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/twitter/android/MediaTagFragment;->a(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->n:Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {v0}, Lcom/twitter/android/PhotoSelectHelper;->a()V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->a:Lcom/twitter/android/lp;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->c:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    const-string/jumbo v0, "tags"

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->b()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    const-string/jumbo v0, "partial_tag"

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onStart()V
    .locals 3

    const/4 v2, 0x1

    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->requestFocus()Z

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-static {v0, v1, v2}, Lcom/twitter/library/util/Util;->a(Landroid/content/Context;Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->i:Lcom/twitter/android/MediaTagFragment$MediaTagEditText;

    invoke-virtual {v0, v2}, Lcom/twitter/android/MediaTagFragment$MediaTagEditText;->a(Z)V

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0

    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 5

    invoke-super {p0, p1, p2}, Landroid/support/v4/app/Fragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    new-instance v0, Lcom/twitter/android/PhotoSelectHelper;

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    new-instance v2, Lcom/twitter/android/lk;

    invoke-direct {v2, p0}, Lcom/twitter/android/lk;-><init>(Lcom/twitter/android/MediaTagFragment;)V

    const-string/jumbo v3, ""

    sget-object v4, Lcom/twitter/android/PhotoSelectHelper$MediaType;->c:Ljava/util/EnumSet;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/twitter/android/PhotoSelectHelper;-><init>(Landroid/app/Activity;Lcom/twitter/android/AttachMediaListener;Ljava/lang/String;Ljava/util/EnumSet;)V

    iput-object v0, p0, Lcom/twitter/android/MediaTagFragment;->n:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v0, p0, Lcom/twitter/android/MediaTagFragment;->n:Lcom/twitter/android/PhotoSelectHelper;

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->d:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/twitter/android/PhotoSelectHelper;->a(Lcom/twitter/library/client/Session;)V

    invoke-virtual {p0}, Lcom/twitter/android/MediaTagFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string/jumbo v1, "uri"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    iget-object v1, p0, Lcom/twitter/android/MediaTagFragment;->n:Lcom/twitter/android/PhotoSelectHelper;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Lcom/twitter/android/PhotoSelectHelper;->a(Landroid/net/Uri;Z)V

    return-void
.end method
