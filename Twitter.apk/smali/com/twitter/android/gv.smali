.class Lcom/twitter/android/gv;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic a:Ljava/util/List;

.field final synthetic b:Lcom/twitter/library/provider/Tweet;

.field final synthetic c:Lcom/twitter/android/GalleryActivity;


# direct methods
.method constructor <init>(Lcom/twitter/android/GalleryActivity;Ljava/util/List;Lcom/twitter/library/provider/Tweet;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    iput-object p2, p0, Lcom/twitter/android/gv;->a:Ljava/util/List;

    iput-object p3, p0, Lcom/twitter/android/gv;->b:Lcom/twitter/library/provider/Tweet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7

    const/4 v6, 0x5

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/twitter/android/gv;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/twitter/android/gv;->b:Lcom/twitter/library/provider/Tweet;

    iget-object v0, v0, Lcom/twitter/library/provider/Tweet;->J:Lcom/twitter/library/api/PromotedContent;

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    invoke-static {v1}, Lcom/twitter/android/GalleryActivity;->c(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/client/c;

    move-result-object v1

    sget-object v2, Lcom/twitter/library/api/PromotedEvent;->f:Lcom/twitter/library/api/PromotedEvent;

    invoke-virtual {v1, v2, v0}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/api/PromotedEvent;Lcom/twitter/library/api/PromotedContent;)V

    :cond_0
    iget-object v1, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    new-instance v2, Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    const-class v3, Lcom/twitter/android/ProfileActivity;

    invoke-direct {v2, v0, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v3, "screen_name"

    iget-object v0, p0, Lcom/twitter/android/gv;->a:Ljava/util/List;

    const/4 v4, 0x0

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/MediaTag;

    iget-object v0, v0, Lcom/twitter/library/api/MediaTag;->screenName:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "association"

    new-instance v3, Lcom/twitter/library/scribe/ScribeAssociation;

    invoke-direct {v3}, Lcom/twitter/library/scribe/ScribeAssociation;-><init>()V

    invoke-virtual {v3, v6}, Lcom/twitter/library/scribe/ScribeAssociation;->a(I)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    iget-object v4, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    invoke-static {v4}, Lcom/twitter/android/GalleryActivity;->d(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/library/client/Session;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->a(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    const-string/jumbo v4, "gallery"

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->b(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    const-string/jumbo v4, "media_tag_summary"

    invoke-virtual {v3, v4}, Lcom/twitter/library/scribe/ScribeAssociation;->c(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeAssociation;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/twitter/android/GalleryActivity;->startActivity(Landroid/content/Intent;)V

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    iget-object v1, p0, Lcom/twitter/android/gv;->b:Lcom/twitter/library/provider/Tweet;

    invoke-static {v0, v6, v1}, Lcom/twitter/android/GalleryActivity;->a(Lcom/twitter/android/GalleryActivity;ILcom/twitter/library/provider/Tweet;)V

    return-void

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/gv;->c:Lcom/twitter/android/GalleryActivity;

    invoke-static {v0}, Lcom/twitter/android/GalleryActivity;->e(Lcom/twitter/android/GalleryActivity;)Lcom/twitter/android/hd;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/android/hd;->a(Z)V

    goto :goto_0
.end method
