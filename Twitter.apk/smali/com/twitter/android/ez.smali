.class Lcom/twitter/android/ez;
.super Lcom/twitter/library/client/j;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/DiscoverFragment;


# direct methods
.method constructor <init>(Lcom/twitter/android/DiscoverFragment;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-direct {p0}, Lcom/twitter/library/client/j;-><init>()V

    return-void
.end method


# virtual methods
.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;JIJJLcom/twitter/library/api/TwitterUser;)V
    .locals 7

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2, p2}, Lcom/twitter/android/DiscoverFragment;->e(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->k(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ev;

    move-wide/from16 v0, p10

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/ev;->a(J)Ljava/lang/Long;

    move-result-object v3

    if-eqz v3, :cond_1

    if-eqz p12, :cond_0

    iget-object v4, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-virtual {v4}, Lcom/twitter/android/DiscoverFragment;->V()Landroid/widget/ListView;

    move-result-object v4

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    invoke-static {v4, v5, v6}, Lcom/twitter/android/zw;->a(Landroid/widget/ListView;J)Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    move-wide/from16 v0, p10

    invoke-virtual {v2, v0, v1}, Lcom/twitter/android/ev;->b(J)Ljava/lang/Long;

    :cond_1
    return-void
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 5

    const v4, 0x7f0f04e3    # com.twitter.android.R.string.tweets_fetch_error

    const/4 v3, 0x1

    if-nez p5, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/DiscoverFragment;->a(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    iget v2, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    invoke-virtual {v1, v2}, Lcom/twitter/android/DiscoverFragment;->b(I)V

    const/16 v1, 0xc8

    if-ne p3, v1, :cond_1

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    iget-object v1, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-virtual {v1}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->e()Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "last_refresh"

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/DiscoverFragment;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_1
    iget v1, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    const/4 v2, 0x3

    if-eq v1, v2, :cond_2

    iget v0, v0, Lcom/twitter/android/client/PendingRequest;->b:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    :cond_2
    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->c(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->d(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0, v4, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public a(Lcom/twitter/library/client/Session;Ljava/lang/String;I[ILjava/lang/String;J)V
    .locals 13

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2, p2}, Lcom/twitter/android/DiscoverFragment;->b(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v4

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    move-wide/from16 v0, p6

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/twitter/android/DiscoverFragment;->b(JJ)Landroid/util/Pair;

    move-result-object v9

    const/16 v2, 0xc8

    move/from16 v0, p3

    if-eq v0, v2, :cond_1

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->e(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v2, v4, v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    iget-object v2, v2, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1}, Lcom/twitter/library/util/FriendshipCache;->e(J)V

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->f(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz v9, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->g(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v2

    check-cast v2, Lcom/twitter/android/ev;

    iget-object v3, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    move-wide/from16 v0, p6

    invoke-virtual {v2, v0, v1, v6, v7}, Lcom/twitter/android/ev;->a(JJ)V

    iget-object v12, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->h(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/android/client/c;

    move-result-object v2

    const/16 v6, 0x9

    const-wide/16 v7, -0x1

    iget-object v11, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v11, Ljava/lang/Integer;

    move-object v3, p1

    move-wide/from16 v9, p6

    invoke-virtual/range {v2 .. v11}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;JIJJLjava/lang/Integer;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v12, v2}, Lcom/twitter/android/DiscoverFragment;->c(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public a(Lcom/twitter/library/client/Session;Z)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->l(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    check-cast v0, Lcom/twitter/android/ev;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p2}, Lcom/twitter/android/ev;->b(Z)V

    :cond_0
    return-void
.end method

.method public c(Lcom/twitter/library/client/Session;Ljava/lang/String;ILjava/lang/String;J)V
    .locals 4

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0, p2}, Lcom/twitter/android/DiscoverFragment;->d(Lcom/twitter/android/DiscoverFragment;Ljava/lang/String;)Lcom/twitter/android/client/PendingRequest;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v0

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-virtual {v2, v0, v1, p5, p6}, Lcom/twitter/android/DiscoverFragment;->b(JJ)Landroid/util/Pair;

    const/16 v2, 0xc8

    if-eq p3, v2, :cond_0

    iget-object v2, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v2}, Lcom/twitter/android/DiscoverFragment;->i(Lcom/twitter/android/DiscoverFragment;)Lcom/twitter/library/client/Session;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    iget-object v0, v0, Lcom/twitter/android/DiscoverFragment;->a:Lcom/twitter/library/util/FriendshipCache;

    invoke-virtual {v0, p5, p6}, Lcom/twitter/library/util/FriendshipCache;->b(J)V

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-static {v0}, Lcom/twitter/android/DiscoverFragment;->j(Lcom/twitter/android/DiscoverFragment;)Landroid/support/v4/widget/CursorAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/widget/CursorAdapter;->notifyDataSetChanged()V

    iget-object v0, p0, Lcom/twitter/android/ez;->a:Lcom/twitter/android/DiscoverFragment;

    invoke-virtual {v0}, Lcom/twitter/android/DiscoverFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const v1, 0x7f0f052b    # com.twitter.android.R.string.users_destroy_friendship_error

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_0
    return-void
.end method
