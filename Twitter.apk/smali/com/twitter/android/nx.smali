.class Lcom/twitter/android/nx;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/NotificationSettingsActivity;

.field private b:Z

.field private c:Z

.field private final d:Ljava/lang/String;

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private l:I

.field private m:I

.field private n:I

.field private o:I

.field private p:I

.field private q:I

.field private r:I

.field private s:Z

.field private t:Z

.field private u:Z


# direct methods
.method public constructor <init>(Lcom/twitter/android/NotificationSettingsActivity;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    iput-object p2, p0, Lcom/twitter/android/nx;->d:Ljava/lang/String;

    return-void
.end method

.method private a(Landroid/preference/Preference;)I
    .locals 1

    if-eqz p1, :cond_0

    check-cast p1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {p1}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(ILjava/lang/String;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->j(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->i(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "settings:notifications:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "::deselect"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->l(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->k(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "settings:notifications:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "::select"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Landroid/content/ContentValues;IIIIIIIIIIIIII)V
    .locals 3

    const/4 v1, 0x0

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p4}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p5}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p6}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p7}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p8}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p14

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p15

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    const-string/jumbo v2, "notif_mention"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_message"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p3}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_tweet"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p2}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_experimental"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p9}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_lifeline_alerts"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p10}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_recommendations"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p11}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_news"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, p12}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string/jumbo v1, "notif_vit_notable_event"

    sget-object v2, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    move/from16 v0, p13

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    return-void
.end method

.method private b(Landroid/preference/Preference;)I
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_0

    check-cast p1, Landroid/preference/ListPreference;

    invoke-virtual {p1}, Landroid/preference/ListPreference;->getValue()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    goto :goto_0
.end method

.method private b(ILjava/lang/String;)V
    .locals 7

    const/4 v3, 0x1

    const/4 v6, 0x0

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->n(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->m(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "settings:notifications:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "::deselect"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->p(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->o(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "settings:notifications:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":from_people_you_follow:select"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->r(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v0

    iget-object v1, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v1}, Lcom/twitter/android/NotificationSettingsActivity;->q(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v1

    new-array v3, v3, [Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "settings:notifications:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, ":from_anyone:select"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v0, v1, v2, v3}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected varargs a([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 22

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/nx;->b:Z

    if-nez v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/nx;->c:Z

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    return-object v2

    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nx;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/library/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v3, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/twitter/android/nx;->e:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/twitter/android/nx;->j:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/twitter/android/nx;->f:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/twitter/android/nx;->g:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/twitter/android/nx;->h:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/twitter/android/nx;->i:I

    move-object/from16 v0, p0

    iget v10, v0, Lcom/twitter/android/nx;->k:I

    move-object/from16 v0, p0

    iget v11, v0, Lcom/twitter/android/nx;->l:I

    move-object/from16 v0, p0

    iget v12, v0, Lcom/twitter/android/nx;->m:I

    move-object/from16 v0, p0

    iget v13, v0, Lcom/twitter/android/nx;->n:I

    move-object/from16 v0, p0

    iget v14, v0, Lcom/twitter/android/nx;->o:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/twitter/android/nx;->p:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/nx;->q:I

    move/from16 v16, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/twitter/android/nx;->r:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v3, v3, Lcom/twitter/android/NotificationSettingsActivity;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/twitter/library/api/account/k;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    new-instance v3, Landroid/content/ContentValues;

    const/16 v20, 0xa

    move/from16 v0, v20

    invoke-direct {v3, v0}, Landroid/content/ContentValues;-><init>(I)V

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/nx;->c:Z

    move/from16 v20, v0

    if-eqz v20, :cond_1

    const-string/jumbo v20, "vibrate"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/nx;->t:Z

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string/jumbo v20, "ringtone"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    move-object/from16 v21, v0

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->d:Ljava/lang/String;

    move-object/from16 v21, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v20, "light"

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/nx;->u:Z

    move/from16 v21, v0

    invoke-static/range {v21 .. v21}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v21

    move-object/from16 v0, v20

    move-object/from16 v1, v21

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/nx;->b:Z

    move/from16 v20, v0

    if-eqz v20, :cond_2

    if-nez v19, :cond_11

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v17}, Lcom/twitter/android/nx;->a(Landroid/content/ContentValues;IIIIIIIIIIIIII)V

    :cond_2
    :goto_1
    invoke-virtual {v3}, Landroid/content/ContentValues;->size()I

    move-result v2

    if-lez v2, :cond_3

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v2}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nx;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    move/from16 v1, v19

    invoke-virtual {v2, v0, v3, v1}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;Landroid/content/ContentValues;Z)I

    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->l:I

    if-eq v2, v6, :cond_4

    const-string/jumbo v2, "mention"

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2}, Lcom/twitter/android/nx;->b(ILjava/lang/String;)V

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->p:I

    if-eq v2, v5, :cond_5

    const-string/jumbo v2, "message"

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->k:I

    if-eq v2, v4, :cond_6

    const-string/jumbo v2, "tweet"

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->m:I

    if-eq v2, v7, :cond_7

    const-string/jumbo v2, "retweet"

    move-object/from16 v0, p0

    invoke-direct {v0, v7, v2}, Lcom/twitter/android/nx;->b(ILjava/lang/String;)V

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->n:I

    if-eq v2, v8, :cond_8

    const-string/jumbo v2, "favorite"

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, Lcom/twitter/android/nx;->b(ILjava/lang/String;)V

    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->o:I

    if-eq v2, v9, :cond_9

    const-string/jumbo v2, "follow"

    move-object/from16 v0, p0

    invoke-direct {v0, v9, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->q:I

    if-eq v2, v10, :cond_a

    const-string/jumbo v2, "address_book"

    move-object/from16 v0, p0

    invoke-direct {v0, v10, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->r:I

    if-eq v2, v11, :cond_b

    const-string/jumbo v2, "experimental"

    move-object/from16 v0, p0

    invoke-direct {v0, v11, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->s:I

    if-eq v2, v12, :cond_c

    const-string/jumbo v2, "lifeline_alert"

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_c
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->t:I

    if-eq v2, v13, :cond_d

    const-string/jumbo v2, "recommendation"

    move-object/from16 v0, p0

    invoke-direct {v0, v13, v2}, Lcom/twitter/android/nx;->b(ILjava/lang/String;)V

    :cond_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->u:I

    if-eq v2, v14, :cond_e

    const-string/jumbo v2, "news"

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_e
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->v:I

    if-eq v2, v15, :cond_f

    const-string/jumbo v2, "vit_notable_event"

    move-object/from16 v0, p0

    invoke-direct {v0, v15, v2}, Lcom/twitter/android/nx;->a(ILjava/lang/String;)V

    :cond_f
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v2, v2, Lcom/twitter/android/NotificationSettingsActivity;->B:Z

    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/twitter/android/nx;->s:Z

    if-eq v2, v3, :cond_10

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/twitter/android/nx;->s:Z

    if-eqz v2, :cond_15

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/NotificationSettingsActivity;->f(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v3}, Lcom/twitter/android/NotificationSettingsActivity;->e(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "settings:notifications:::enable_notifications"

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_10
    :goto_2
    const/4 v2, 0x0

    goto/16 :goto_0

    :cond_11
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/nx;->s:Z

    move/from16 v20, v0

    if-nez v20, :cond_12

    if-nez v2, :cond_12

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v17}, Lcom/twitter/android/nx;->a(Landroid/content/ContentValues;IIIIIIIIIIIIII)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/NotificationSettingsActivity;->b(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v2

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;)Ljava/lang/String;

    goto/16 :goto_1

    :cond_12
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/twitter/android/nx;->s:Z

    move/from16 v20, v0

    if-nez v20, :cond_13

    const/16 v2, 0x400

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/twitter/android/NotificationSettingsActivity;->c(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    goto/16 :goto_1

    :cond_13
    const/16 v20, 0x0

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->g:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v4}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->a:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v6}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->b:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v7}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->c:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v8}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->d:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v9}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->e:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v5}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->h:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v10}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->i:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->f:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v12}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->j:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v13}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->k:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v14}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->n:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    invoke-virtual {v0, v15}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v21

    or-int v20, v20, v21

    sget-object v21, Lcom/twitter/library/provider/NotificationSetting;->l:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v21

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v16

    or-int v16, v16, v20

    sget-object v20, Lcom/twitter/library/provider/NotificationSetting;->m:Lcom/twitter/library/provider/NotificationSetting;

    move-object/from16 v0, v20

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v17

    or-int v16, v16, v17

    if-eqz v2, :cond_14

    const/16 v2, 0x400

    :goto_3
    or-int v2, v2, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Lcom/twitter/android/NotificationSettingsActivity;->d(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v16

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v2}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    goto/16 :goto_1

    :cond_14
    const/4 v2, 0x0

    goto :goto_3

    :cond_15
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v2}, Lcom/twitter/android/NotificationSettingsActivity;->h(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/android/client/c;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v3}, Lcom/twitter/android/NotificationSettingsActivity;->g(Lcom/twitter/android/NotificationSettingsActivity;)Lcom/twitter/library/client/Session;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const-string/jumbo v7, "settings:notifications:::disable_notifications"

    aput-object v7, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lcom/twitter/android/nx;->a([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected onPreExecute()V
    .locals 4

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-super {p0}, Landroid/os/AsyncTask;->onPreExecute()V

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    invoke-static {v0}, Lcom/twitter/android/NotificationSettingsActivity;->a(Lcom/twitter/android/NotificationSettingsActivity;)Landroid/preference/CheckBoxPreference;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/nx;->s:Z

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->e:Z

    if-eqz v0, :cond_4

    iput v2, p0, Lcom/twitter/android/nx;->e:I

    :goto_0
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->J:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->l:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->I:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->k:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->M:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->n:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->N:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->o:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->g:Z

    if-eqz v0, :cond_5

    iput v2, p0, Lcom/twitter/android/nx;->q:I

    :goto_1
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->i:Z

    if-eqz v0, :cond_6

    iput v2, p0, Lcom/twitter/android/nx;->r:I

    :goto_2
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->K:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->j:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->E:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->b(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->f:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->H:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->i:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->L:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->m:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->M:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->n:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->N:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->o:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->O:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->p:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->j:Z

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->f:I

    iput v0, p0, Lcom/twitter/android/nx;->g:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->h:I

    iput v0, p0, Lcom/twitter/android/nx;->h:I

    :cond_0
    :goto_3
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->l:I

    iget v3, p0, Lcom/twitter/android/nx;->f:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->p:I

    iget v3, p0, Lcom/twitter/android/nx;->j:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->m:I

    iget v3, p0, Lcom/twitter/android/nx;->g:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->n:I

    iget v3, p0, Lcom/twitter/android/nx;->h:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->o:I

    iget v3, p0, Lcom/twitter/android/nx;->i:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->k:I

    iget v3, p0, Lcom/twitter/android/nx;->e:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->B:Z

    iget-boolean v3, p0, Lcom/twitter/android/nx;->s:Z

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->q:I

    iget v3, p0, Lcom/twitter/android/nx;->k:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->r:I

    iget v3, p0, Lcom/twitter/android/nx;->l:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->s:I

    iget v3, p0, Lcom/twitter/android/nx;->m:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->t:I

    iget v3, p0, Lcom/twitter/android/nx;->n:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->u:I

    iget v3, p0, Lcom/twitter/android/nx;->o:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->v:I

    iget v3, p0, Lcom/twitter/android/nx;->p:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->w:I

    iget v3, p0, Lcom/twitter/android/nx;->q:I

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->x:I

    iget v3, p0, Lcom/twitter/android/nx;->r:I

    if-eq v0, v3, :cond_b

    :cond_1
    move v0, v2

    :goto_4
    iput-boolean v0, p0, Lcom/twitter/android/nx;->b:Z

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    const-string/jumbo v3, "use_led"

    invoke-virtual {v0, v3}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/nx;->u:Z

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    const-string/jumbo v3, "vibrate"

    invoke-virtual {v0, v3}, Lcom/twitter/android/NotificationSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/nx;->t:Z

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->z:Z

    iget-boolean v3, p0, Lcom/twitter/android/nx;->u:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->y:Z

    iget-boolean v3, p0, Lcom/twitter/android/nx;->t:Z

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->A:Ljava/lang/String;

    iget-object v3, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v3, v3, Lcom/twitter/android/NotificationSettingsActivity;->d:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    iput-boolean v1, p0, Lcom/twitter/android/nx;->c:Z

    return-void

    :cond_4
    iput v1, p0, Lcom/twitter/android/nx;->e:I

    goto/16 :goto_0

    :cond_5
    iput v1, p0, Lcom/twitter/android/nx;->q:I

    goto/16 :goto_1

    :cond_6
    iput v1, p0, Lcom/twitter/android/nx;->r:I

    goto/16 :goto_2

    :cond_7
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->D:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->a(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->e:I

    goto/16 :goto_2

    :cond_8
    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->F:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->b(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->g:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-object v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->G:Landroid/preference/Preference;

    invoke-direct {p0, v0}, Lcom/twitter/android/nx;->b(Landroid/preference/Preference;)I

    move-result v0

    iput v0, p0, Lcom/twitter/android/nx;->h:I

    iget-object v0, p0, Lcom/twitter/android/nx;->a:Lcom/twitter/android/NotificationSettingsActivity;

    iget-boolean v0, v0, Lcom/twitter/android/NotificationSettingsActivity;->c:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/twitter/android/nx;->g:I

    if-eqz v0, :cond_9

    iget v0, p0, Lcom/twitter/android/nx;->l:I

    iput v0, p0, Lcom/twitter/android/nx;->q:I

    :goto_5
    iget v0, p0, Lcom/twitter/android/nx;->h:I

    if-eqz v0, :cond_a

    iget v0, p0, Lcom/twitter/android/nx;->l:I

    iput v0, p0, Lcom/twitter/android/nx;->r:I

    goto/16 :goto_3

    :cond_9
    iput v1, p0, Lcom/twitter/android/nx;->q:I

    goto :goto_5

    :cond_a
    iput v1, p0, Lcom/twitter/android/nx;->r:I

    goto/16 :goto_3

    :cond_b
    move v0, v1

    goto/16 :goto_4
.end method
