.class Lcom/twitter/android/au;
.super Landroid/os/AsyncTask;
.source "Twttr"


# instance fields
.field final synthetic a:Lcom/twitter/android/BackupCodeFragment;

.field private b:Ljava/lang/ref/WeakReference;

.field private c:Ljava/lang/String;

.field private d:Landroid/app/ProgressDialog;


# direct methods
.method constructor <init>(Lcom/twitter/android/BackupCodeFragment;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/twitter/android/au;->a:Lcom/twitter/android/BackupCodeFragment;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/au;->b:Ljava/lang/ref/WeakReference;

    iput-object p3, p0, Lcom/twitter/android/au;->c:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected varargs a([Landroid/graphics/Bitmap;)Landroid/net/Uri;
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    aget-object v2, p1, v0

    iget-object v0, p0, Lcom/twitter/android/au;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v2, :cond_0

    if-eqz v0, :cond_0

    invoke-static {v0, v1, v2, v1, v1}, Lcom/twitter/library/util/n;->a(Landroid/content/Context;Lcom/twitter/library/util/l;Landroid/graphics/Bitmap;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method

.method protected a(Landroid/net/Uri;)V
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/au;->a:Lcom/twitter/android/BackupCodeFragment;

    invoke-virtual {v0, p1}, Lcom/twitter/android/BackupCodeFragment;->a(Landroid/net/Uri;)V

    return-void
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    check-cast p1, [Landroid/graphics/Bitmap;

    invoke-virtual {p0, p1}, Lcom/twitter/android/au;->a([Landroid/graphics/Bitmap;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0

    check-cast p1, Landroid/net/Uri;

    invoke-virtual {p0, p1}, Lcom/twitter/android/au;->a(Landroid/net/Uri;)V

    return-void
.end method

.method protected onPreExecute()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/au;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    if-eqz v0, :cond_0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-direct {v1, v0}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setProgressStyle(I)V

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    iget-object v1, p0, Lcom/twitter/android/au;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/ProgressDialog;->setIndeterminate(Z)V

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0, v2}, Landroid/app/ProgressDialog;->setCancelable(Z)V

    iget-object v0, p0, Lcom/twitter/android/au;->d:Landroid/app/ProgressDialog;

    invoke-virtual {v0}, Landroid/app/ProgressDialog;->show()V

    :cond_0
    return-void
.end method
