.class public Lcom/twitter/android/vf;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Landroid/util/SparseArray;


# instance fields
.field private final b:Ljava/lang/ref/WeakReference;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/twitter/android/vf;->a:Landroid/util/SparseArray;

    sget-object v0, Lcom/twitter/android/vf;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string/jumbo v2, "tv"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/vf;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string/jumbo v2, "sports"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    sget-object v0, Lcom/twitter/android/vf;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string/jumbo v2, "unplanned"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    return-void
.end method

.method public constructor <init>(Landroid/support/v4/app/Fragment;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/twitter/android/vf;->b:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;)Landroid/content/Intent;
    .locals 6

    const/4 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x1

    packed-switch p2, :pswitch_data_0

    move v0, v2

    move-object v3, v4

    :goto_0
    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aO()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aP()Ljava/util/ArrayList;

    move-result-object v2

    sget-object v5, Lcom/twitter/android/vf;->a:Landroid/util/SparseArray;

    invoke-virtual {v5, p2, v4}, Landroid/util/SparseArray;->get(ILjava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    :cond_0
    if-eqz v2, :cond_4

    const-class v2, Lcom/twitter/android/EventSearchActivity;

    :goto_1
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string/jumbo v2, "query_name"

    invoke-virtual {v4, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "query"

    invoke-virtual {v2, v4, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "seed_hashtag"

    invoke-virtual {v2, v4, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "event_type"

    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "scribe_context"

    invoke-virtual {v2, v4, p5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "terminal"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v2

    const-string/jumbo v4, "search_button"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "q_source"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v2, "topic_data"

    invoke-virtual {v0, v2, p8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    if-ne p2, v1, :cond_1

    if-eqz p7, :cond_6

    const-string/jumbo v1, "q_type"

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :cond_1
    :goto_2
    const/4 v1, 0x7

    if-eq p2, v1, :cond_2

    const/4 v1, 0x6

    if-eq p2, v1, :cond_2

    invoke-static {p1}, Lcom/twitter/library/api/TwitterTopic;->b(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string/jumbo v1, "event_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_2
    return-object v0

    :pswitch_0
    const-string/jumbo v0, "tvev"

    move-object v3, v0

    move v0, v1

    goto :goto_0

    :pswitch_1
    const-string/jumbo v0, "loev"

    move-object v3, v0

    move v0, v2

    goto/16 :goto_0

    :pswitch_2
    const-string/jumbo v0, "upev"

    move-object v3, v0

    move v0, v1

    goto/16 :goto_0

    :pswitch_3
    if-eqz p7, :cond_3

    const-string/jumbo v0, "promoted_trend_click"

    :goto_3
    move-object v3, v0

    move v0, v1

    goto/16 :goto_0

    :cond_3
    const-string/jumbo v0, "trend_click"

    goto :goto_3

    :pswitch_4
    const-string/jumbo v0, "spev"

    move-object v3, v0

    move v0, v1

    goto/16 :goto_0

    :cond_4
    if-eqz v0, :cond_5

    const-class v2, Lcom/twitter/android/SearchTerminalActivity;

    goto/16 :goto_1

    :cond_5
    const-class v2, Lcom/twitter/android/SearchActivity;

    goto/16 :goto_1

    :cond_6
    const-string/jumbo v2, "q_type"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public a(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/android/widget/TopicView$TopicData;)V
    .locals 10

    iget-object v0, p0, Lcom/twitter/android/vf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Landroid/support/v4/app/Fragment;

    if-eqz v9, :cond_0

    invoke-virtual {v9}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v5, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object/from16 v8, p6

    invoke-static/range {v0 .. v8}, Lcom/twitter/android/vf;->a(Landroid/content/Context;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLcom/twitter/android/widget/TopicView$TopicData;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v9, v0}, Landroid/support/v4/app/Fragment;->startActivity(Landroid/content/Intent;)V

    :cond_0
    return-void
.end method
