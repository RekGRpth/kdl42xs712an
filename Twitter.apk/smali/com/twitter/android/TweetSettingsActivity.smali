.class public Lcom/twitter/android/TweetSettingsActivity;
.super Lcom/twitter/android/client/BasePreferenceActivity;
.source "Twttr"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;
.implements Lcom/twitter/library/util/ar;


# instance fields
.field a:Landroid/preference/CheckBoxPreference;

.field b:Landroid/preference/PreferenceCategory;

.field c:Ljava/lang/String;

.field private d:Lcom/twitter/android/xz;

.field private e:Lcom/twitter/library/client/Session;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/preference/Preference;

.field private h:Landroid/content/Intent;

.field private final i:Ljava/util/HashMap;

.field private final j:Ljava/util/HashMap;

.field private final k:Ljava/util/HashMap;

.field private l:I

.field private m:I

.field private n:Z

.field private o:Z

.field private p:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/twitter/android/client/BasePreferenceActivity;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->i:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->j:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->k:Ljava/util/HashMap;

    const/4 v0, -0x1

    iput v0, p0, Lcom/twitter/android/TweetSettingsActivity;->l:I

    return-void
.end method

.method private a(ZI)Landroid/content/Intent;
    .locals 2

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->h:Landroid/content/Intent;

    if-nez v0, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->h:Landroid/content/Intent;

    :cond_0
    iput-boolean p1, p0, Lcom/twitter/android/TweetSettingsActivity;->o:Z

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->h:Landroid/content/Intent;

    const-string/jumbo v1, "enabled"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "count"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->h:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic a(Lcom/twitter/android/TweetSettingsActivity;)Lcom/twitter/library/client/Session;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->e:Lcom/twitter/library/client/Session;

    return-object v0
.end method

.method private a()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->f:Landroid/preference/Preference;

    if-nez v0, :cond_0

    new-instance v0, Landroid/preference/Preference;

    invoke-direct {v0, p0}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setOrder(I)V

    iget-boolean v1, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    if-eqz v1, :cond_1

    const v1, 0x7f0f0406    # com.twitter.android.R.string.settings_notif_tweets_none_selected_title_favorite_people

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    :goto_0
    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setShouldDisableView(Z)V

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setSelectable(Z)V

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->f:Landroid/preference/Preference;

    :cond_0
    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    iget-object v1, p0, Lcom/twitter/android/TweetSettingsActivity;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    return-void

    :cond_1
    const v1, 0x7f0f0405    # com.twitter.android.R.string.settings_notif_tweets_none_selected_title

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setTitle(I)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/twitter/android/TweetSettingsActivity;)Lcom/twitter/android/client/c;
    .locals 1

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    return-object v0
.end method

.method static synthetic c(Lcom/twitter/android/TweetSettingsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetSettingsActivity;->o:Z

    return v0
.end method

.method static synthetic d(Lcom/twitter/android/TweetSettingsActivity;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    return v0
.end method


# virtual methods
.method public a(Lcom/twitter/library/util/ao;Ljava/util/HashMap;)V
    .locals 6

    const/4 v0, 0x1

    iget v1, p1, Lcom/twitter/library/util/ao;->g:I

    if-ne v0, v1, :cond_1

    iget v0, p0, Lcom/twitter/android/TweetSettingsActivity;->l:I

    if-lez v0, :cond_1

    iget-object v3, p0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    invoke-virtual {v3}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v4

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    invoke-virtual {v3, v2}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    const-string/jumbo v1, "msg"

    invoke-virtual {v0}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    check-cast v0, Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-virtual {v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/api/TwitterUser;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/twitter/library/util/ae;

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/twitter/library/util/ae;->b()Z

    move-result v5

    if-eqz v5, :cond_0

    iget-object v1, v1, Lcom/twitter/library/util/ae;->c:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Landroid/graphics/Bitmap;)V

    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_1
    return-void
.end method

.method a(Ljava/util/ArrayList;)V
    .locals 10

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v3

    iget-object v4, p0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    invoke-virtual {p1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz v3, :cond_4

    invoke-direct {p0}, Lcom/twitter/android/TweetSettingsActivity;->a()V

    move v1, v2

    :goto_0
    invoke-virtual {p0}, Lcom/twitter/android/TweetSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v5, 0x7f0e0014    # com.twitter.android.R.plurals.settings_notif_tweets_count

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v0, v5, v1, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    iput-object v4, p0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    iput v1, p0, Lcom/twitter/android/TweetSettingsActivity;->l:I

    iput v1, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    const/4 v0, -0x1

    iget v1, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    invoke-direct {p0, v3, v1}, Lcom/twitter/android/TweetSettingsActivity;->a(ZI)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/twitter/android/TweetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    return-void

    :cond_0
    iget-object v5, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    invoke-virtual {p1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v1, v2

    :goto_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    iget-boolean v7, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    if-eqz v7, :cond_1

    iget-object v7, p0, Lcom/twitter/android/TweetSettingsActivity;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    iget v9, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v7, v8, v9}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    new-instance v7, Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-direct {v7, p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v7, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Lcom/twitter/library/api/TwitterUser;)V

    iget-object v8, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    if-eqz v8, :cond_2

    iget-object v8, v0, Lcom/twitter/library/api/TwitterUser;->profileImageUrl:Ljava/lang/String;

    invoke-virtual {v5, v8}, Lcom/twitter/android/client/c;->g(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a(Landroid/graphics/Bitmap;)V

    :cond_2
    iget v0, v0, Lcom/twitter/library/api/TwitterUser;->friendship:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v7, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->setDefaultValue(Ljava/lang/Object;)V

    invoke-virtual {v7, p0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    invoke-virtual {v4, v7}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    const-string/jumbo v0, "notif_tweets"

    invoke-virtual {v7, v0}, Lcom/twitter/android/widget/UserCheckBoxPreference;->setDependency(Ljava/lang/String;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->g:Landroid/preference/Preference;

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {v0, v5}, Landroid/preference/Preference;->setOrder(I)V

    goto/16 :goto_0

    :cond_4
    move v1, v2

    goto/16 :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x0

    invoke-super {p0, p1}, Lcom/twitter/android/client/BasePreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    const v0, 0x7f060018    # com.twitter.android.R.xml.tweet_prefs

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetSettingsActivity;->addPreferencesFromResource(I)V

    const-string/jumbo v0, "notif_tweets"

    invoke-virtual {p0, v0}, Lcom/twitter/android/TweetSettingsActivity;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iget-boolean v2, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    if-eqz v2, :cond_0

    const v2, 0x7f0f0403    # com.twitter.android.R.string.settings_notif_timeline_title_favorite_people

    invoke-virtual {p0, v2}, Lcom/twitter/android/TweetSettingsActivity;->setTitle(I)V

    const v2, 0x7f0f0404    # com.twitter.android.R.string.settings_notif_tweets_all_notifications_title

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setTitle(I)V

    :goto_0
    const-string/jumbo v2, "enabled"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/twitter/android/TweetSettingsActivity;->o:Z

    iget-boolean v2, p0, Lcom/twitter/android/TweetSettingsActivity;->o:Z

    iput-boolean v2, p0, Lcom/twitter/android/TweetSettingsActivity;->p:Z

    iget-boolean v2, p0, Lcom/twitter/android/TweetSettingsActivity;->o:Z

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    invoke-virtual {v0, v6}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    invoke-virtual {v0, p0}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->a:Landroid/preference/CheckBoxPreference;

    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0f0224    # com.twitter.android.R.string.loading

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->setTitle(I)V

    invoke-virtual {p0}, Lcom/twitter/android/TweetSettingsActivity;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    new-instance v2, Lcom/twitter/android/widget/MessagePreference;

    invoke-direct {v2, p0}, Lcom/twitter/android/widget/MessagePreference;-><init>(Landroid/content/Context;)V

    const-string/jumbo v3, "msg"

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/MessagePreference;->setKey(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    if-eqz v3, :cond_1

    invoke-static {}, Lgl;->h()I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/MessagePreference;->setSummary(I)V

    :goto_1
    invoke-virtual {v2, v6}, Lcom/twitter/android/widget/MessagePreference;->setShouldDisableView(Z)V

    invoke-virtual {v2, v6}, Lcom/twitter/android/widget/MessagePreference;->setSelectable(Z)V

    invoke-virtual {v2, v6}, Lcom/twitter/android/widget/MessagePreference;->setPersistent(Z)V

    iput-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->g:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    new-instance v2, Lcom/twitter/android/xz;

    invoke-direct {v2, p0}, Lcom/twitter/android/xz;-><init>(Lcom/twitter/android/TweetSettingsActivity;)V

    invoke-virtual {v0, v7, p0}, Lcom/twitter/android/client/c;->a(ILcom/twitter/library/util/ar;)V

    invoke-virtual {v0, v2}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/j;)V

    iput-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->d:Lcom/twitter/android/xz;

    const-string/jumbo v2, "account_name"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p0}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/twitter/library/client/aa;->b(Ljava/lang/String;)Lcom/twitter/library/client/Session;

    move-result-object v1

    iput-object v1, p0, Lcom/twitter/android/TweetSettingsActivity;->e:Lcom/twitter/library/client/Session;

    const/16 v2, 0x10

    const/16 v3, 0x190

    invoke-virtual {v0, v1, v6, v2, v3}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;III)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->c:Ljava/lang/String;

    return-void

    :cond_0
    const v2, 0x7f0f0402    # com.twitter.android.R.string.settings_notif_timeline_title

    invoke-virtual {p0, v2}, Lcom/twitter/android/TweetSettingsActivity;->setTitle(I)V

    goto/16 :goto_0

    :cond_1
    const v3, 0x7f0f0407    # com.twitter.android.R.string.settings_notif_tweets_summary

    new-array v4, v7, [Ljava/lang/Object;

    const v5, 0x7f0f0530    # com.twitter.android.R.string.users_enable_notifications

    invoke-virtual {p0, v5}, Lcom/twitter/android/TweetSettingsActivity;->getString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {p0, v3, v4}, Lcom/twitter/android/TweetSettingsActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/twitter/android/widget/MessagePreference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method protected onDestroy()V
    .locals 2

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onDestroy()V

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    const/4 v1, 0x1

    invoke-virtual {v0, v1, p0}, Lcom/twitter/android/client/c;->b(ILcom/twitter/library/util/ar;)V

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lcom/twitter/android/TweetSettingsActivity;->d:Lcom/twitter/android/xz;

    invoke-virtual {v0, v1}, Lcom/twitter/android/client/c;->b(Lcom/twitter/library/client/j;)V

    return-void
.end method

.method public onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 8

    const/4 v7, 0x1

    const-string/jumbo v0, "notif_tweets"

    invoke-virtual {p1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    iget v0, p0, Lcom/twitter/android/TweetSettingsActivity;->l:I

    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    invoke-direct {p0}, Lcom/twitter/android/TweetSettingsActivity;->a()V

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->e:Lcom/twitter/library/client/Session;

    invoke-virtual {v0}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v3

    new-array v5, v7, [Ljava/lang/String;

    const/4 v6, 0x0

    if-eqz v1, :cond_2

    const-string/jumbo v0, "settings:notifications:favorite_people::enable_notifications"

    :goto_1
    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    move v0, v1

    :goto_2
    const/4 v1, -0x1

    iget v2, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    invoke-direct {p0, v0, v2}, Lcom/twitter/android/TweetSettingsActivity;->a(ZI)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/twitter/android/TweetSettingsActivity;->setResult(ILandroid/content/Intent;)V

    return v7

    :cond_1
    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->f:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->b:Landroid/preference/PreferenceCategory;

    iget-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "settings:notifications:favorite_people::disable_notifications"

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->a:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0}, Landroid/preference/CheckBoxPreference;->isChecked()Z

    move-result v0

    check-cast p1, Lcom/twitter/android/widget/UserCheckBoxPreference;

    invoke-virtual {p1}, Lcom/twitter/android/widget/UserCheckBoxPreference;->a()Lcom/twitter/library/api/TwitterUser;

    move-result-object v1

    iget-boolean v2, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->k:Ljava/util/HashMap;

    invoke-virtual {v1}, Lcom/twitter/library/api/TwitterUser;->a()J

    move-result-wide v3

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {v2, v1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_4
    check-cast p2, Ljava/lang/Integer;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v2

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->i:Ljava/util/HashMap;

    iget-wide v3, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    goto :goto_2

    :cond_5
    iget-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->i:Ljava/util/HashMap;

    iget-wide v3, v1, Lcom/twitter/library/api/TwitterUser;->userId:J

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v3, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget v1, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/twitter/android/TweetSettingsActivity;->m:I

    goto :goto_2
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onResume()V

    invoke-static {}, Lgl;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    return-void
.end method

.method protected onStop()V
    .locals 9

    const/4 v8, 0x3

    const/4 v7, 0x0

    invoke-super {p0}, Lcom/twitter/android/client/BasePreferenceActivity;->onStop()V

    iget-boolean v0, p0, Lcom/twitter/android/TweetSettingsActivity;->n:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->k:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v1, v0, 0x1010

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->j:Ljava/util/HashMap;

    invoke-static {v3, v4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    and-int/lit16 v0, v0, 0x1010

    if-eq v1, v0, :cond_0

    new-instance v0, Lin;

    invoke-virtual {p0}, Lcom/twitter/android/TweetSettingsActivity;->d()Lcom/twitter/library/client/Session;

    move-result-object v5

    invoke-direct {v0, p0, v5}, Lin;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V

    invoke-virtual {v0, v8}, Lin;->c(I)Lcom/twitter/library/service/b;

    move-result-object v5

    const-string/jumbo v6, "user_id"

    invoke-virtual {v5, v6, v3, v4}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;J)Lcom/twitter/library/service/b;

    move-result-object v3

    const-string/jumbo v4, "device_follow"

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v5

    invoke-virtual {v3, v4, v5}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    move-result-object v3

    const-string/jumbo v4, "email_follow"

    invoke-static {v1}, Lcom/twitter/library/provider/ay;->m(I)Z

    move-result v1

    invoke-virtual {v3, v4, v1}, Lcom/twitter/library/service/b;->a(Ljava/lang/String;Z)Lcom/twitter/library/service/b;

    invoke-static {p0}, Lcom/twitter/library/client/w;->a(Landroid/content/Context;)Lcom/twitter/library/client/w;

    move-result-object v1

    new-instance v3, Lcom/twitter/android/xy;

    invoke-direct {v3, p0}, Lcom/twitter/android/xy;-><init>(Lcom/twitter/android/TweetSettingsActivity;)V

    invoke-virtual {v1, v0, v8, v7, v3}, Lcom/twitter/library/client/w;->a(Lcom/twitter/library/service/b;IILcom/twitter/library/service/c;)Z

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/twitter/android/TweetSettingsActivity;->Q:Lcom/twitter/android/client/c;

    iget-object v2, p0, Lcom/twitter/android/TweetSettingsActivity;->e:Lcom/twitter/library/client/Session;

    iget-object v0, p0, Lcom/twitter/android/TweetSettingsActivity;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/TwitterUser;

    invoke-virtual {v1, v2, v0, v7}, Lcom/twitter/android/client/c;->a(Lcom/twitter/library/client/Session;Lcom/twitter/library/api/TwitterUser;Z)Ljava/lang/String;

    goto :goto_1

    :cond_2
    iget-boolean v0, p0, Lcom/twitter/android/TweetSettingsActivity;->p:Z

    iget-boolean v1, p0, Lcom/twitter/android/TweetSettingsActivity;->o:Z

    if-eq v0, v1, :cond_3

    invoke-virtual {p0}, Lcom/twitter/android/TweetSettingsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string/jumbo v1, "from_notification_landing"

    invoke-virtual {v0, v1, v7}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    new-instance v0, Lcom/twitter/android/ya;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/twitter/android/ya;-><init>(Lcom/twitter/android/TweetSettingsActivity;Lcom/twitter/android/xx;)V

    new-array v1, v7, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/twitter/android/ya;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_3
    return-void
.end method
