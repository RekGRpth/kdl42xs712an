.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface;


# static fields
.field private static IsRunning:Ljava/lang/Boolean;

.field private static final mGetExtraDataArgsSignature:[Ljava/lang/Class;

.field private static mListener:Ljava/lang/Object;

.field private static mOnActionComplete:Ljava/lang/reflect/Method;

.field private static final mOnActionCompleteArgsSignature:[Ljava/lang/Class;

.field private static mSetImage:Ljava/lang/reflect/Method;

.field private static mSetImageArgs:[Ljava/lang/Object;

.field private static final mSetImageArgsSignature:[Ljava/lang/Class;


# instance fields
.field private m3DBitmap:Landroid/graphics/Bitmap;

.field private mContext:Landroid/content/Context;

.field private mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

.field private mFilePath:Ljava/lang/String;

.field private final mGetExtraDataArgs:[Ljava/lang/Object;

.field private mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

.field private mObjectBase:Ljava/lang/Object;

.field private mObjectImage:Ljava/lang/reflect/Method;

.field private final mOnActionCompleteArgs:[Ljava/lang/Object;

.field private mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

.field private mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

.field private final mRect:Landroid/graphics/RectF;

.field private final mSetExtraDataArgs:[Ljava/lang/Object;

.field private mViewGroup:Landroid/view/ViewGroup;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, Ljava/lang/String;

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetExtraDataArgsSignature:[Ljava/lang/Class;

    new-array v0, v3, [Ljava/lang/Object;

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImageArgs:[Ljava/lang/Object;

    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, Landroid/graphics/Bitmap;

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImageArgsSignature:[Ljava/lang/Class;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->IsRunning:Ljava/lang/Boolean;

    new-array v0, v3, [Ljava/lang/Class;

    const-class v1, Ljava/lang/Object;

    aput-object v1, v0, v2

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionCompleteArgsSignature:[Ljava/lang/Class;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetExtraDataArgs:[Ljava/lang/Object;

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    new-array v0, v1, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionCompleteArgs:[Ljava/lang/Object;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->invokeListener()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;Landroid/graphics/Bitmap;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->m3DBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method private invokeListener()V
    .locals 3

    const/4 v2, 0x0

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionComplete:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionCompleteArgs:[Ljava/lang/Object;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectBase:Ljava/lang/Object;

    aput-object v1, v0, v2

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->stopSetBitmap(Z)V

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionComplete:Ljava/lang/reflect/Method;

    sget-object v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mListener:Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionCompleteArgs:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->stopRemoveView()V

    :cond_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method private mGetField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/reflect/Field;->get(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/NoSuchFieldException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchFieldException;->printStackTrace()V

    goto :goto_1
.end method

.method private varargs mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p1, p4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1
.end method

.method private varargs mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Class;

    invoke-virtual {v0, p2, v1}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    invoke-virtual {v0, p1, p3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_3

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_1
.end method

.method private setImage(Landroid/graphics/Bitmap;)V
    .locals 3

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImage:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImageArgs:[Ljava/lang/Object;

    const/4 v1, 0x0

    aput-object p1, v0, v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImage:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectBase:Ljava/lang/Object;

    sget-object v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImageArgs:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getNativeHandle()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public getType()I
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mContext:Landroid/content/Context;

    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onUnload()V
    .locals 0

    return-void
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenObjectRuntimeInterface$UpdateListener;)Z
    .locals 3

    sput-object p1, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mListener:Ljava/lang/Object;

    :try_start_0
    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mListener:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "onCompleted"

    sget-object v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionCompleteArgsSignature:[Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mOnActionComplete:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x1

    return v0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 0

    return-void
.end method

.method public setRect(Landroid/graphics/RectF;)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->left:F

    iput v1, v0, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->top:F

    iput v1, v0, Landroid/graphics/RectF;->top:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->right:F

    iput v1, v0, Landroid/graphics/RectF;->right:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, p1, Landroid/graphics/RectF;->bottom:F

    iput v1, v0, Landroid/graphics/RectF;->bottom:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v0, v0, Landroid/graphics/RectF;->right:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->bottom:F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v1, v1, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    iget v2, v2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_0
    return-void
.end method

.method public start(Ljava/lang/Object;Landroid/graphics/RectF;Landroid/graphics/PointF;FLandroid/graphics/PointF;Landroid/view/ViewGroup;)V
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectBase:Ljava/lang/Object;

    iput-object p6, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "setImage"

    sget-object v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImageArgsSignature:[Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetImage:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    :try_start_1
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSuperclass()Ljava/lang/Class;

    move-result-object v0

    const-string/jumbo v1, "setExtraDataString"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectImage:Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const-string/jumbo v0, "getRect"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-direct {p0, p1, v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    const-string/jumbo v0, "left"

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v2, Landroid/graphics/RectF;->left:F

    const-string/jumbo v0, "top"

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v2, Landroid/graphics/RectF;->top:F

    const-string/jumbo v0, "right"

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v2, Landroid/graphics/RectF;->right:F

    const-string/jumbo v0, "bottom"

    invoke-direct {p0, v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetField(Ljava/lang/Object;Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mRect:Landroid/graphics/RectF;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, v1, Landroid/graphics/RectF;->bottom:F

    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v0, v1

    float-to-int v0, v0

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    iget v2, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v1, v2

    float-to-int v1, v1

    new-instance v2, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v2, v0, v1}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    iget v1, p2, Landroid/graphics/RectF;->left:F

    float-to-int v1, v1

    iget v2, p2, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetExtraDataArgs:[Ljava/lang/Object;

    const-string/jumbo v1, "currentPage"

    aput-object v1, v0, v5

    const-string/jumbo v0, "getExtraDataString"

    sget-object v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetExtraDataArgsSignature:[Ljava/lang/Class;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mGetExtraDataArgs:[Ljava/lang/Object;

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mInvoke(Ljava/lang/Object;Ljava/lang/String;[Ljava/lang/Class;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    if-nez v0, :cond_0

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->IsRunning:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2}, Landroid/view/ViewGroup;->getHeight()I

    move-result v2

    invoke-direct {v0, v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v5, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    new-instance v1, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    new-instance v2, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$1;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;)V

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;->bringToFront()V

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mLayoutParams:Landroid/widget/RelativeLayout$LayoutParams;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setZOrderMediaOverlay(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-direct {v0, v1, v2, v5}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;-><init>(Landroid/content/Context;Landroid/view/View;I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->setPhysicsRenderer(Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;)V

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->IsRunning:Ljava/lang/Boolean;

    new-instance v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$2;-><init>(Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->setOnCompleteSandFrameListener(Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer$OnCompleteSandFrameListener;)V

    :cond_0
    :goto_2
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/NoSuchMethodException;->printStackTrace()V

    goto/16 :goto_1

    :cond_1
    iput-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mFilePath:Ljava/lang/String;

    invoke-static {v0}, Landroid/graphics/BitmapFactory;->decodeFile(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->setImage(Landroid/graphics/Bitmap;)V

    goto :goto_2
.end method

.method public stop(Z)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    sget-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->IsRunning:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->ScreenCapture()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->m3DBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_2

    const-string/jumbo v0, "mytag"

    const-string/jumbo v1, "[SandPaperActiveObject] onUnselected() - Capture image is nuLLLLLLLLLLLLLLLLLLLLLLLL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->m3DBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->setImage(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectImage:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    const-string/jumbo v1, "currentPage"

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mFilePath:Ljava/lang/String;

    aput-object v2, v0, v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectImage:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectBase:Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->onStop()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    :cond_5
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->IsRunning:Ljava/lang/Boolean;

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_1
.end method

.method public stopRemoveView()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;->onStop()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsView:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsGLView;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mViewGroup:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mDummyView:Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper$DummyView;

    :cond_1
    return-void
.end method

.method public stopSetBitmap(Z)V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mPhysicsRenderer:Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsRenderer;->ScreenCapture()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->m3DBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const-string/jumbo v0, "mytag"

    const-string/jumbo v1, "[SnowPaperActiveObject] onUnselected() - Capture image is nuLLLLLLLLLLLLLLLLLLLLLLLL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->m3DBitmap:Landroid/graphics/Bitmap;

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->setImage(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectImage:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    const-string/jumbo v1, "currentPage"

    aput-object v1, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mFilePath:Ljava/lang/String;

    aput-object v2, v0, v1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectImage:Ljava/lang/reflect/Method;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mObjectBase:Ljava/lang/Object;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->mSetExtraDataArgs:[Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    :cond_1
    :goto_0
    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/objectruntime/preload/SandPaper;->IsRunning:Ljava/lang/Boolean;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/reflect/InvocationTargetException;->printStackTrace()V

    goto :goto_0
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
