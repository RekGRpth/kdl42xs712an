.class public Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/objectruntime/preload/IPhysicsEngineJNI;


# instance fields
.field public mNativePhysicsEngine:I


# direct methods
.method public constructor <init>()V
    .locals 2
    .annotation build Landroid/annotation/SuppressLint;
        value = {
            "SdCardPath"
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    const-string/jumbo v0, "/data/data/com.samsung.android.sdk.spen30/lib/libSPenPaperEffect.so"

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :try_start_1
    const-string/jumbo v0, "SPenPaperEffect"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "PaperEffect library is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_2
    const-string/jumbo v0, "SPenPaperEffect"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "PaperEffect library is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static native native_DeInit_JNI(I)V
.end method

.method private static native native_Draw_PhysicsEngine(I)V
.end method

.method private static native native_Init_JNI()I
.end method

.method private static native native_Init_PhysicsEngine(IIII)V
.end method

.method private static native native_SetTexture(ILjava/lang/String;Landroid/graphics/Bitmap;Z)V
.end method

.method private static native native_onKeyEvent(II)V
.end method

.method private static native native_onSensorEvent(IIFFF)V
.end method

.method private static native native_onSettingEvent(IIF)V
.end method

.method private static native native_onTouchEvent(IIII[I[I)V
.end method


# virtual methods
.method public DeInit_PhysicsEngineJNI()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_DeInit_JNI(I)V

    return-void
.end method

.method public Draw_PhysicsEngine()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_Draw_PhysicsEngine(I)V

    return-void
.end method

.method public Init_PhysicsEngine(III)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_Init_PhysicsEngine(IIII)V

    return-void
.end method

.method public Init_PhysicsEngineJNI()V
    .locals 1

    invoke-static {}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_Init_JNI()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    return-void
.end method

.method public SetTexture(Ljava/lang/String;Landroid/graphics/Bitmap;Z)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_SetTexture(ILjava/lang/String;Landroid/graphics/Bitmap;Z)V

    return-void
.end method

.method public onKeyEvent(I)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_onKeyEvent(II)V

    return-void
.end method

.method public onSensorEvent(IFFF)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_onSensorEvent(IIFFF)V

    return-void
.end method

.method public onSettingEvent(IF)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    invoke-static {v0, p1, p2}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_onSettingEvent(IIF)V

    return-void
.end method

.method public onTouchEvent(III[I[I)V
    .locals 6

    iget v0, p0, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->mNativePhysicsEngine:I

    move v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/objectruntime/preload/PhysicsEngineJNI;->native_onTouchEvent(IIII[I[I)V

    return-void
.end method
