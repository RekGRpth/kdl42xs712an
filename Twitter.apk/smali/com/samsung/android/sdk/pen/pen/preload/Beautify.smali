.class public Lcom/samsung/android/sdk/pen/pen/preload/Beautify;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "SdCardPath"
    }
.end annotation


# static fields
.field private static final BEAUTIFY_PARAMETER_CURSIVE:I = 0x2

.field private static final BEAUTIFY_PARAMETER_DUMMY:I = 0x4

.field private static final BEAUTIFY_PARAMETER_LINETYPE:I = 0x1

.field private static final BEAUTIFY_PARAMETER_MODULATION:I = 0x6

.field private static final BEAUTIFY_PARAMETER_SCRATCH:I = 0x7

.field private static final BEAUTIFY_PARAMETER_SLANT:I = 0x9

.field private static final BEAUTIFY_PARAMETER_SLANTINDEX:I = 0x8

.field private static final BEAUTIFY_PARAMETER_STYLEID:I = 0x0

.field private static final BEAUTIFY_PARAMETER_SUSTENANCE:I = 0x3

.field private static final BEAUTIFY_PARAMETER_THICKNESS:I = 0x5

.field private static final BEAUTIFY_STYLEID_CURSIVE_LM:I = 0xb

.field private static final BEAUTIFY_STYLEID_HUAI:I = 0xc

.field private static final BEAUTIFY_STYLEID_HUANG:I = 0x5

.field private static final BEAUTIFY_STYLEID_HUI:I = 0x6

.field private static final BEAUTIFY_STYLEID_RUNNING_HAND_S:I = 0x1

.field private static final BEAUTIFY_STYLEID_WANG:I = 0x3

.field private static final DEFAULT_SETTING_VALUES:[[I

.field private static final MAX_PARAMETER_INDEX:I = 0xa

.field private static final MAX_STYLEID_COUNT:I = 0x6

.field public static final TAG:Ljava/lang/String; = "Beautify"

.field private static mDensity:F = 0.0f

.field private static final mImagePath_eraser_bar:Ljava/lang/String; = "eraser_bar"

.field private static final mImagePath_eraser_handel:Ljava/lang/String; = "eraser_handel"

.field private static final mImagePath_eraser_handel_press:Ljava/lang/String; = "eraser_handel_press"

.field private static final mImagePath_snote_option_in_bg:Ljava/lang/String; = "snote_option_in_bg"


# instance fields
.field private mAdvancedSettingLayout:Landroid/widget/LinearLayout;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mButtonList:Ljava/util/ArrayList;

.field private mContext:Landroid/content/Context;

.field private mCurrentButtonIndex:I

.field private mCursiveSeekBar:Landroid/widget/SeekBar;

.field private mDummySeekBar:Landroid/widget/SeekBar;

.field private mModulationSeekBar:Landroid/widget/SeekBar;

.field private mResetButton:Landroid/view/View;

.field private final mResetButtonListener:Landroid/view/View$OnClickListener;

.field private final mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

.field private mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

.field private final mSettingValues:[[I

.field private mSustenanceSeekBar:Landroid/widget/SeekBar;

.field private final nativeBeautify:I


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x6

    const/4 v8, 0x5

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    new-array v0, v9, [[I

    const/4 v1, 0x0

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v3, 0x0

    const/16 v4, 0xb

    aput v4, v2, v3

    aput v6, v2, v5

    aput v8, v2, v6

    const/16 v3, 0x8

    aput v3, v2, v7

    const/4 v3, 0x4

    aput v5, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v8

    const/16 v3, 0x46

    aput v3, v2, v9

    const/4 v3, 0x7

    const/16 v4, 0xb

    aput v4, v2, v3

    const/16 v3, 0x9

    aput v5, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    const/16 v3, 0xc

    aput v3, v1, v2

    aput v6, v1, v5

    aput v8, v1, v6

    aput v5, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    const/16 v3, 0xd

    aput v3, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v5

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v8, v1, v2

    aput v6, v1, v5

    aput v6, v1, v6

    aput v6, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0x12

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    aput v8, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v6

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v9, v1, v2

    aput v6, v1, v5

    aput v7, v1, v6

    aput v7, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    aput v9, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v7

    const/4 v1, 0x4

    const/16 v2, 0xa

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput v5, v2, v3

    aput v6, v2, v5

    aput v6, v2, v6

    const/16 v3, 0x8

    aput v3, v2, v7

    const/4 v3, 0x4

    aput v5, v2, v3

    const/16 v3, 0xf

    aput v3, v2, v8

    const/16 v3, 0x46

    aput v3, v2, v9

    const/4 v3, 0x7

    aput v7, v2, v3

    const/16 v3, 0x9

    aput v5, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v1, v1, [I

    const/4 v2, 0x0

    aput v7, v1, v2

    aput v5, v1, v5

    aput v7, v1, v6

    aput v8, v1, v7

    const/4 v2, 0x4

    aput v5, v1, v2

    const/16 v2, 0xc

    aput v2, v1, v8

    const/16 v2, 0x46

    aput v2, v1, v9

    const/4 v2, 0x7

    aput v5, v1, v2

    const/16 v2, 0x9

    aput v5, v1, v2

    aput-object v1, v0, v8

    sput-object v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    sget-object v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I

    invoke-virtual {v0}, [[I->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I

    new-instance v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$1;-><init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButtonListener:Landroid/view/View$OnClickListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$2;-><init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    const-string/jumbo v0, "/data/data/com.samsung.android.sdk.spen30/lib/libSPenBeautify.so"

    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    invoke-static {v0}, Ljava/lang/System;->load(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-static {}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    :try_start_1
    const-string/jumbo v0, "SPenBeautify"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Beautify library is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_2
    const-string/jumbo v0, "SPenBeautify"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Beautify library is not initialized."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    return-object v0
.end method

.method static synthetic access$1()[[I
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->DEFAULT_SETTING_VALUES:[[I

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)[[I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$7(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$8(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$9(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setBeautifyType(I)V

    return-void
.end method

.method private beautifyTypeLayout()Landroid/widget/LinearLayout;
    .locals 8

    const/4 v1, 0x0

    const/4 v3, -0x2

    const/high16 v7, 0x41f00000    # 30.0f

    new-instance v2, Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    move v0, v1

    :goto_0
    const/4 v3, 0x6

    if-lt v0, v3, :cond_0

    return-object v2

    :cond_0
    new-instance v3, Landroid/widget/ImageButton;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v3, v4}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    add-int/lit8 v4, v0, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    const-string/jumbo v6, "chinabrush_mode_0"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    new-instance v4, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v5

    invoke-direct {p0, v7}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v6

    invoke-direct {v4, v5, v6}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string/jumbo v4, "chinabrush_effect_btn_normal"

    const-string/jumbo v5, "chinabrush_effect_btn_press"

    invoke-direct {p0, v4, v5, v5}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/ImageButton;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v3, v1}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private calculatePixel(F)I
    .locals 1

    sget v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDensity:F

    mul-float/2addr v0, p1

    float-to-int v0, v0

    return v0
.end method

.method private cursiveLayout()Landroid/view/View;
    .locals 6

    const/high16 v5, 0x41400000    # 12.0f

    const/4 v4, -0x2

    const/4 v3, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->cursiveSeekbar()Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    const-string/jumbo v2, "cursive"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    const/high16 v2, 0x40900000    # 4.5f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    invoke-virtual {v1, v3, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private cursiveSeekbar()Landroid/widget/SeekBar;
    .locals 9

    const/16 v5, 0xa0

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v6, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0xc

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setMax(I)V

    const-string/jumbo v0, "eraser_handel"

    const-string/jumbo v1, "eraser_handel_press"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v1, 0x3

    invoke-direct {v7, v0, v1, v8}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    const-string/jumbo v0, "eraser_bar"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_3

    const/16 v3, 0xa

    if-gt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_0

    const/high16 v0, 0x40d00000    # 6.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v1, v2

    aput-object v7, v1, v8

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    return-object v6

    :cond_0
    const/high16 v0, 0x40b00000    # 5.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_2

    const/high16 v0, 0x40600000    # 3.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_2
    const/high16 v0, 0x40200000    # 2.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_3
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0
.end method

.method private dummyLayout()Landroid/view/View;
    .locals 6

    const/high16 v5, 0x41400000    # 12.0f

    const/4 v4, -0x2

    const/4 v3, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->dummySeekbar()Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    const-string/jumbo v2, "dummy"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    const/high16 v2, 0x40900000    # 4.5f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    invoke-virtual {v1, v3, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private dummySeekbar()Landroid/widget/SeekBar;
    .locals 9

    const/16 v5, 0xa0

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v6, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0x14

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setMax(I)V

    const-string/jumbo v0, "eraser_handel"

    const-string/jumbo v1, "eraser_handel_press"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v1, 0x3

    invoke-direct {v7, v0, v1, v8}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    const-string/jumbo v0, "eraser_bar"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_3

    const/16 v3, 0xa

    if-gt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_0

    const/high16 v0, 0x40d00000    # 6.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v1, v2

    aput-object v7, v1, v8

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    return-object v6

    :cond_0
    const/high16 v0, 0x40b00000    # 5.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_2

    const/high16 v0, 0x40600000    # 3.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_2
    const/high16 v0, 0x40200000    # 2.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_3
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0
.end method

.method private initView()Landroid/widget/LinearLayout;
    .locals 4

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const v3, 0x43ac8000    # 345.0f

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const v1, -0x969697

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    const/high16 v1, 0x42200000    # 40.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setX(F)V

    const/high16 v1, 0x43070000    # 135.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setY(F)V

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->beautifyTypeLayout()Landroid/widget/LinearLayout;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->cursiveLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->sustenanceLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->dummyLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->modulationLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->resetButtonLayout()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private modulationLayout()Landroid/view/View;
    .locals 6

    const/high16 v5, 0x41400000    # 12.0f

    const/4 v4, -0x2

    const/4 v3, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->modulationSeekbar()Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    const-string/jumbo v2, "modulation"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    const/high16 v2, 0x40900000    # 4.5f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    invoke-virtual {v1, v3, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private modulationSeekbar()Landroid/widget/SeekBar;
    .locals 9

    const/16 v5, 0xa0

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v6, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0x64

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setMax(I)V

    const-string/jumbo v0, "eraser_handel"

    const-string/jumbo v1, "eraser_handel_press"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v1, 0x3

    invoke-direct {v7, v0, v1, v8}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    const-string/jumbo v0, "eraser_bar"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_3

    const/16 v3, 0xa

    if-gt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_0

    const/high16 v0, 0x40d00000    # 6.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v1, v2

    aput-object v7, v1, v8

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    return-object v6

    :cond_0
    const/high16 v0, 0x40b00000    # 5.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_2

    const/high16 v0, 0x40600000    # 3.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_2
    const/high16 v0, 0x40200000    # 2.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_3
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0
.end method

.method private static native native_command(IILjava/util/ArrayList;)Ljava/util/ArrayList;
.end method

.method private static native native_construct(I)Z
.end method

.method private static native native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_end(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_getAdvancedSetting(I)Ljava/lang/String;
.end method

.method private static native native_getColor(I)I
.end method

.method private static native native_getMaxSettingValue(I)F
.end method

.method private static native native_getMinSettingValue(I)F
.end method

.method private static native native_getPenAttribute(II)Z
.end method

.method private static native native_getProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_getSize(I)F
.end method

.method private static native native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
.end method

.method private static native native_init()I
.end method

.method private static native native_isCurveEnabled(I)Z
.end method

.method private static native native_move(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_onLoad(I)V
.end method

.method private static native native_onUnload(I)V
.end method

.method private static native native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private static native native_setAdvancedSetting(ILjava/lang/String;)Z
.end method

.method private static native native_setBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setColor(II)Z
.end method

.method private static native native_setCurveEnabled(IZ)Z
.end method

.method private static native native_setProperty(ILandroid/os/Bundle;)Z
.end method

.method private static native native_setReferenceBitmap(ILandroid/graphics/Bitmap;)Z
.end method

.method private static native native_setSize(IF)Z
.end method

.method private static native native_start(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z
.end method

.method private resetButtonLayout()Landroid/view/View;
    .locals 5

    const/high16 v4, 0x40a00000    # 5.0f

    new-instance v0, Landroid/widget/Button;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/Button;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v2, -0x2

    const/high16 v3, 0x42200000    # 40.0f

    invoke-direct {p0, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/16 v2, 0x11

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->gravity:I

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-direct {p0, v4}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const-string/jumbo v1, "reset"

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButton:Landroid/view/View;

    return-object v0
.end method

.method private setBeautifyType(I)V
    .locals 5

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    if-eqz v0, :cond_0

    iput p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    new-instance v1, Ljava/lang/StringBuffer;

    invoke-direct {v1}, Ljava/lang/StringBuffer;-><init>()V

    move v0, v2

    :goto_0
    const/16 v3, 0xa

    if-lt v0, v3, :cond_1

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setAdvancedSetting(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    invoke-virtual {v1}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;->onChanged(Ljava/lang/String;)V

    move v1, v2

    :goto_1
    const/4 v0, 0x6

    if-lt v1, v0, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingValues:[[I

    aget-object v3, v3, p1

    aget v3, v3, v0

    packed-switch v0, :pswitch_data_0

    :goto_2
    :pswitch_0
    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(I)Ljava/lang/StringBuffer;

    const/16 v3, 0x3b

    invoke-virtual {v1, v3}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :pswitch_1
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    :pswitch_2
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    :pswitch_3
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    :pswitch_4
    iget-object v4, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v4, v3}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    :cond_2
    if-ne p1, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    const/4 v3, 0x1

    invoke-virtual {v0, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    :goto_3
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Landroid/graphics/drawable/StateListDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/StateListDrawable;-><init>()V

    if-eqz p1, :cond_0

    const/4 v1, 0x2

    new-array v1, v1, [I

    fill-array-data v1, :array_0

    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_0
    if-eqz p2, :cond_1

    new-array v1, v4, [I

    const v2, 0x10100a7    # android.R.attr.state_pressed

    aput v2, v1, v3

    invoke-virtual {p0, p2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_1
    if-eqz p3, :cond_2

    new-array v1, v4, [I

    const v2, 0x10100a1    # android.R.attr.state_selected

    aput v2, v1, v3

    invoke-virtual {p0, p3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/StateListDrawable;->addState([ILandroid/graphics/drawable/Drawable;)V

    :cond_2
    return-object v0

    nop

    :array_0
    .array-data 4
        -0x10100a7
        -0x10100a1
    .end array-data
.end method

.method private setListener()V
    .locals 3

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    const/4 v0, 0x6

    if-lt v1, v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCursiveSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDummySeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mModulationSeekBar:Landroid/widget/SeekBar;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSeekBarListner:Landroid/widget/SeekBar$OnSeekBarChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButton:Landroid/view/View;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButton:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mResetButtonListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_4
    return-void

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mButtonList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    new-instance v2, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;

    invoke-direct {v2, p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify$3;-><init>(Lcom/samsung/android/sdk/pen/pen/preload/Beautify;)V

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private sustenanceLayout()Landroid/view/View;
    .locals 6

    const/high16 v5, 0x41400000    # 12.0f

    const/4 v4, -0x2

    const/4 v3, 0x0

    new-instance v0, Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    const/high16 v2, 0x40a00000    # 5.0f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    iput v2, v1, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setOrientation(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->sustenanceSeekbar()Landroid/widget/SeekBar;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    new-instance v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    new-instance v2, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextSize(F)V

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    const/4 v2, 0x1

    invoke-virtual {v1, v2, v5}, Landroid/widget/TextView;->setTextSize(IF)V

    const-string/jumbo v2, "sustenance"

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    const/high16 v2, 0x40900000    # 4.5f

    invoke-direct {p0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v2

    invoke-virtual {v1, v3, v2, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private sustenanceSeekbar()Landroid/widget/SeekBar;
    .locals 9

    const/16 v5, 0xa0

    const/4 v8, 0x1

    const/4 v2, 0x0

    new-instance v6, Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-direct {v6, v0}, Landroid/widget/SeekBar;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/high16 v1, 0x42dc0000    # 110.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    const/4 v3, -0x2

    invoke-direct {v0, v1, v3}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/16 v0, 0x10

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setMax(I)V

    const-string/jumbo v0, "eraser_handel"

    const-string/jumbo v1, "eraser_handel_press"

    const/4 v3, 0x0

    invoke-direct {p0, v0, v1, v3}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableSelectImg(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/graphics/drawable/StateListDrawable;

    move-result-object v0

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setThumb(Landroid/graphics/drawable/Drawable;)V

    new-instance v0, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v0}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/high16 v1, 0x40a00000    # 5.0f

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    new-instance v7, Landroid/graphics/drawable/ClipDrawable;

    const/4 v1, 0x3

    invoke-direct {v7, v0, v1, v8}, Landroid/graphics/drawable/ClipDrawable;-><init>(Landroid/graphics/drawable/Drawable;II)V

    const-string/jumbo v0, "eraser_bar"

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->targetSdkVersion:I

    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0xe

    if-lt v3, v4, :cond_3

    const/16 v3, 0xa

    if-gt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_0

    const/high16 v0, 0x40d00000    # 6.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    :goto_0
    new-instance v0, Landroid/graphics/drawable/InsetDrawable;

    move v4, v2

    move v5, v3

    invoke-direct/range {v0 .. v5}, Landroid/graphics/drawable/InsetDrawable;-><init>(Landroid/graphics/drawable/Drawable;IIII)V

    const/4 v1, 0x2

    new-array v1, v1, [Landroid/graphics/drawable/Drawable;

    aput-object v0, v1, v2

    aput-object v7, v1, v8

    new-instance v0, Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/graphics/drawable/LayerDrawable;-><init>([Landroid/graphics/drawable/Drawable;)V

    invoke-virtual {v6, v0}, Landroid/widget/SeekBar;->setProgressDrawable(Landroid/graphics/drawable/Drawable;)V

    iput-object v6, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSustenanceSeekBar:Landroid/widget/SeekBar;

    return-object v6

    :cond_0
    const/high16 v0, 0x40b00000    # 5.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    if-ne v0, v5, :cond_2

    const/high16 v0, 0x40600000    # 3.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_2
    const/high16 v0, 0x40200000    # 2.5f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0

    :cond_3
    const/high16 v0, 0x40000000    # 2.0f

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->calculatePixel(F)I

    move-result v3

    goto :goto_0
.end method


# virtual methods
.method public construct()V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_construct(I)Z

    return-void
.end method

.method public draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_draw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getAdvancedSetting(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getColor(I)I

    move-result v0

    return v0
.end method

.method public getMaxSettingValue()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getMaxSettingValue(I)F

    move-result v0

    return v0
.end method

.method public getMinSettingValue()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getMinSettingValue(I)F

    move-result v0

    return v0
.end method

.method public getNativeHandle()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    return v0
.end method

.method public getPenAttribute(I)Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getPenAttribute(II)Z

    move-result v0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getProperty(ILandroid/os/Bundle;)Z

    return-void
.end method

.method public getSize()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getSize(I)F

    move-result v0

    return v0
.end method

.method public getStrokeRect([Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;
    .locals 7

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_getStrokeRect(I[Landroid/graphics/PointF;[F[IFZLjava/lang/String;)Landroid/graphics/RectF;

    move-result-object v0

    return-object v0
.end method

.method public hideAdvancedSetting()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->isShown()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public isCurveEnabled()Z
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_isCurveEnabled(I)Z

    move-result v0

    return v0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_onLoad(I)V

    return-void
.end method

.method public onUnload()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_onUnload(I)V

    return-void
.end method

.method public redrawPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1, v1, v1}, Landroid/graphics/Bitmap;->setPixel(III)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-static {v0, p1, p2, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_redraw(ILandroid/view/MotionEvent;Landroid/graphics/RectF;I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    const-string/jumbo v0, ";"

    invoke-virtual {p1, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v0, v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setAdvancedSetting(ILjava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 2

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setBitmap(ILandroid/graphics/Bitmap;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setColor(I)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setColor(II)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setCurveEnabled(IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method protected setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    :try_start_0
    const-string/jumbo v1, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    const-string/jumbo v1, "drawable"

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v0, p1, v1, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "TAG"

    const-string/jumbo v2, "Resource loading is Failed"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setProperty(ILandroid/os/Bundle;)Z

    return-void
.end method

.method public setReferenceBitmap(Landroid/graphics/Bitmap;)V
    .locals 0

    return-void
.end method

.method public setSize(F)V
    .locals 2

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->nativeBeautify:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->native_setSize(IF)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    :cond_0
    return-void
.end method

.method public showAdvancedSetting(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;Landroid/view/ViewGroup;)V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    if-nez v0, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->density:F

    sput v0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mDensity:F

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->initView()Landroid/widget/LinearLayout;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setListener()V

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mSettingListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface$ChangeListener;

    iput v1, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mCurrentButtonIndex:I

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->setBeautifyType(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {p3, v0}, Landroid/view/ViewGroup;->bringChildToFront(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/pen/preload/Beautify;->mAdvancedSettingLayout:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method
