.class public Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final TAG:Ljava/lang/String; = "EquationRecognition"


# instance fields
.field private mEqRecLib:Lcom/samsung/vip/engine/a;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public addStroke([F[F)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/vip/engine/a;->a([F[F)V

    goto :goto_0
.end method

.method public dispose()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/a;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    :cond_0
    return-void
.end method

.method public init(Landroid/content/Context;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/recognition/preload/VIRecogUtils;->copyDatabase(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string/jumbo v1, "EquationRecognition"

    const-string/jumbo v2, "Fail to copy database."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v2, "/vidata/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    new-instance v2, Lcom/samsung/vip/engine/a;

    invoke-direct {v2}, Lcom/samsung/vip/engine/a;-><init>()V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    invoke-virtual {v2, v1}, Lcom/samsung/vip/engine/a;->a(Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_1

    const-string/jumbo v2, "EquationRecognition"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "equation engine init : ret = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public process()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/EquationRecognition;->mEqRecLib:Lcom/samsung/vip/engine/a;

    invoke-virtual {v0}, Lcom/samsung/vip/engine/a;->b()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
