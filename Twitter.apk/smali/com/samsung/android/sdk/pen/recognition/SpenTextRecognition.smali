.class public final Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;
.super Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;
.source "Twttr"


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V

    return-void
.end method


# virtual methods
.method public getLanguage()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;->getLanguage()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedLanguage()Ljava/util/List;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;->getSupportedLanguage()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public setLanguage(Ljava/lang/String;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Recognition is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenTextRecognition;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenLanguageRecognitionInterface;->setLanguage(Ljava/lang/String;)V

    return-void
.end method
