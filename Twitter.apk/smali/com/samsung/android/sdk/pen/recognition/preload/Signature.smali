.class public Lcom/samsung/android/sdk/pen/recognition/preload/Signature;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface;


# static fields
.field public static final SIGNATURE_DEFAULT_MIN_SIZE:I = 0xc8

.field public static final SIGNATURE_DEFAULT_USER_ID:I = 0xa

.field public static final SIGNATURE_MAX_POINT_COUNT:I = 0x400

.field public static final SIGNATURE_MAX_REGISTRATION_NUM:I = 0x3

.field public static final SIGNATURE_VERIFICATION_LEVEL_HIGH:I = 0x2

.field public static final SIGNATURE_VERIFICATION_LEVEL_LOW:I = 0x0

.field public static final SIGNATURE_VERIFICATION_LEVEL_MEDIUM:I = 0x1

.field private static final TAG:Ljava/lang/String; = "Signature"

.field public static final VERIFICATION_LEVEL_KEY:Ljava/lang/String; = "VerificationLevel"


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

.field private final mHandler:Landroid/os/Handler;

.field private mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

.field private mVerificationLevel:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    const/4 v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->verifySignature(I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private closeSignatureEngine()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/4 v0, 0x1

    goto :goto_0
.end method

.method private getSignatureTempPath()Ljava/lang/String;
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->getParent()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v0, "/_tmp/"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private openSignatureEngine()Z
    .locals 6

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->getSignatureTempPath()Ljava/lang/String;

    move-result-object v1

    const-string/jumbo v0, "Signature"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "openSignatureEngine() : saveDirPath = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v2, 0x400

    const/4 v3, 0x3

    const/16 v4, 0xa

    const/16 v5, 0xc8

    invoke-virtual/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->initHSV(Ljava/lang/String;IIII)Z

    move-result v0

    return v0
.end method

.method private registerSignature()I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->signatureTraining(I)I

    move-result v0

    goto :goto_0
.end method

.method private unregisterSignature()Z
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->deleteRegistration(I)Z

    move-result v0

    return v0
.end method

.method private verifySignature(I)Z
    .locals 3

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v1, :cond_0

    const-string/jumbo v1, "Signature"

    const-string/jumbo v2, "Signature Engine is not Opened!"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v0

    :cond_0
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    :goto_1
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v2, 0xa

    invoke-virtual {v1, v2, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->verification(II)Z

    move-result v0

    goto :goto_0

    :pswitch_1
    const/4 v0, 0x1

    goto :goto_1

    :pswitch_2
    const/4 v0, 0x2

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getMinimumRequiredCount()I
    .locals 1

    const/4 v0, 0x3

    return v0
.end method

.method public getNativeHandle()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getPrivateKeyHint()Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getProperty(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "VerificationLevel"

    iget v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    return-void
.end method

.method public getRegisteredCount()I
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->getRegisteredDataCount(I)I

    move-result v0

    goto :goto_0
.end method

.method public onLoad(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->openSignatureEngine()Z

    move-result v0

    if-nez v0, :cond_0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Fail to load signature engine"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public onUnload()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->closeSignatureEngine()Z

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mContext:Landroid/content/Context;

    return-void
.end method

.method public register(Ljava/util/List;)V
    .locals 5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Signature Engine is not Opened!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "stroke is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->registerSignature()I

    move-result v0

    if-gez v0, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string/jumbo v1, "Fail to register the signature!"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPoints()[Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getTimeStamps()[I

    move-result-object v3

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->getPressures()[F

    move-result-object v0

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    invoke-virtual {v4, v2, v3, v0}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->setDrawData([Landroid/graphics/PointF;[I[F)V

    goto :goto_0

    :cond_3
    return-void
.end method

.method public request(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    if-nez v0, :cond_0

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "The result listener isn\'t set yet!!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    if-nez v0, :cond_1

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Signature Engine is not Opened!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_1
    if-nez p1, :cond_2

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Input parameter \'stroke\' is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mHSVLib:Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/recognition/preload/HSVLib;->checkRegistration(I)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "Signature"

    const-string/jumbo v1, "Registered signature is not enough"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :cond_3
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;

    invoke-direct {v1, p0, p1}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature$SignatureVerificationRunnable;-><init>(Lcom/samsung/android/sdk/pen/recognition/preload/Signature;Ljava/util/List;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public setProperty(Landroid/os/Bundle;)V
    .locals 2

    const-string/jumbo v0, "VerificationLevel"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "VerificationLevel"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    const/4 v1, 0x2

    if-le v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :cond_1
    iput v0, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mVerificationLevel:I

    :cond_2
    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->mListener:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenSignatureVerificationInterface$ResultListener;

    return-void
.end method

.method public unlock(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public unregisterAll()V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/recognition/preload/Signature;->unregisterSignature()Z

    return-void
.end method
