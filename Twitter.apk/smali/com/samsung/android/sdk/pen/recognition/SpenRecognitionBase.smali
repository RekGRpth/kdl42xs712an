.class public abstract Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

.field private mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p2, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    return-object v0
.end method


# virtual methods
.method getPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    return-object v0
.end method

.method public request(Ljava/util/List;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenRecognitionBase is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;->request(Ljava/util/List;)V

    return-void
.end method

.method public setResultListener(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : SpenRecognitionBase is not loaded"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mResultListener:Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$ResultListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    new-instance v1, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase$1;-><init>(Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;)V

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;->setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V

    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/recognition/SpenRecognitionBase;->mPluginObject:Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface;->setResultListener(Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenRecognitionInterface$ResultListener;)V

    goto :goto_0
.end method
