.class public Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "Twttr"


# instance fields
.field private final mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Landroid/view/View;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v0, 0x1

    invoke-direct {p0, p2, v0}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    check-cast p2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-void
.end method

.method private checkSelection(Ljava/lang/CharSequence;)V
    .locals 3

    const/16 v2, 0x8

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mHandleButton:[Landroid/widget/ImageButton;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)[Landroid/widget/ImageButton;

    move-result-object v0

    const/4 v1, 0x2

    aget-object v0, v0, v1

    invoke-virtual {v0, v2}, Landroid/widget/ImageButton;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    return-void
.end method


# virtual methods
.method public clearMetaKeyStates(I)Z
    .locals 3

    const-string/jumbo v0, "EditableInputConnection"

    const-string/jumbo v1, "clearMetaKeyStates()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;

    move-result-object v1

    if-eqz v1, :cond_1

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mKeyListener:Landroid/text/method/KeyListener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$15(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/method/KeyListener;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-interface {v1, v2, v0, p1}, Landroid/text/method/KeyListener;->clearMetaKeyState(Landroid/view/View;Landroid/text/Editable;I)V
    :try_end_0
    .catch Ljava/lang/AbstractMethodError; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_1
    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/AbstractMethodError;->printStackTrace()V

    goto :goto_1
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "commitText());"

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->checkSelection(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateSelection()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$17(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    return v2
.end method

.method public deleteSurroundingText(II)Z
    .locals 9

    const/4 v7, -0x1

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "deleteSurroundingText() beforeLength = "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ", afterLength = "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    sub-int v0, p1, p2

    if-le v0, v3, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v6

    if-nez v6, :cond_1

    :goto_0
    return v2

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->beginBatchEdit()Z

    invoke-static {v6}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-static {v6}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    if-le v0, v1, :cond_9

    :goto_1
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v4

    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v5

    if-ge v5, v4, :cond_8

    :goto_2
    if-eq v5, v7, :cond_7

    if-eq v4, v7, :cond_7

    if-ge v5, v1, :cond_2

    move v1, v5

    :cond_2
    if-le v4, v0, :cond_7

    move v8, v4

    move v4, v1

    move v1, v8

    :goto_3
    if-lez p1, :cond_4

    sub-int v0, v4, p1

    if-gez v0, :cond_3

    move v0, v2

    :cond_3
    invoke-interface {v6, v0, v4}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    sub-int v2, v4, v0

    :cond_4
    if-lez p2, :cond_6

    sub-int/2addr v1, v2

    add-int v0, v1, p2

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v2

    if-le v0, v2, :cond_5

    invoke-interface {v6}, Landroid/text/Editable;->length()I

    move-result v0

    :cond_5
    invoke-interface {v6, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->endBatchEdit()Z

    move v2, v3

    goto :goto_0

    :cond_7
    move v4, v1

    move v1, v0

    goto :goto_3

    :cond_8
    move v8, v5

    move v5, v4

    move v4, v8

    goto :goto_2

    :cond_9
    move v8, v1

    move v1, v0

    move v0, v8

    goto :goto_1
.end method

.method public finishComposingText()Z
    .locals 2

    const/4 v0, 0x0

    const-string/jumbo v1, "finishComposingText());"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->finishComposingText()Z

    move-result v0

    return v0
.end method

.method public getEditable()Landroid/text/Editable;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v0

    return-object v0
.end method

.method public getExtractedText(Landroid/view/inputmethod/ExtractedTextRequest;I)Landroid/view/inputmethod/ExtractedText;
    .locals 4

    const/4 v3, -0x1

    const/4 v2, 0x0

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->mTextView:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v1, :cond_0

    new-instance v0, Landroid/view/inputmethod/ExtractedText;

    invoke-direct {v0}, Landroid/view/inputmethod/ExtractedText;-><init>()V

    iput v2, v0, Landroid/view/inputmethod/ExtractedText;->flags:I

    iput v3, v0, Landroid/view/inputmethod/ExtractedText;->partialStartOffset:I

    iput v3, v0, Landroid/view/inputmethod/ExtractedText;->partialEndOffset:I

    iput v2, v0, Landroid/view/inputmethod/ExtractedText;->startOffset:I

    const-string/jumbo v1, ""

    iput-object v1, v0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    iput v2, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    iput v2, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->getEditable()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v3

    if-eqz v3, :cond_0

    iput-object v2, v0, Landroid/view/inputmethod/ExtractedText;->text:Ljava/lang/CharSequence;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, v0, Landroid/view/inputmethod/ExtractedText;->selectionStart:I

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v1

    iput v1, v0, Landroid/view/inputmethod/ExtractedText;->selectionEnd:I

    :cond_0
    return-object v0
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 4

    const/4 v0, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "sendKeyEvent() code = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", action = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v0

    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/view/inputmethod/BaseInputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0

    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v3, 0x1

    invoke-static {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    if-ne v1, v0, :cond_1

    if-lez v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v0

    add-int/lit8 v2, v1, -0x1

    invoke-interface {v0, v2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mEditable:Landroid/text/Editable;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;)Landroid/text/Editable;

    move-result-object v3

    if-ge v1, v0, :cond_2

    move v2, v1

    :goto_1
    if-ge v1, v0, :cond_3

    :goto_2
    invoke-interface {v3, v2, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x43
        :pswitch_0
    .end packed-switch
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    const-string/jumbo v0, "setComposingText());"

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->checkSelection(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$16(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextBox$EditableInputConnection;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenTextBox;Z)V

    return v2
.end method
