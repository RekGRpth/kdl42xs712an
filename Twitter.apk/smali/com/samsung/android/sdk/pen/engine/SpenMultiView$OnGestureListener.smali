.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 4

    const/4 v0, 0x1

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {p1, v1}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v3

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->getToolTypeAction(I)I

    move-result v2

    if-ne v2, v0, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mSmartScaleGestureDetector:Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenSmartScaleGestureDetector;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
