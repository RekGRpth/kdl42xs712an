.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;
.super Ljava/lang/Object;
.source "Twttr"


# virtual methods
.method public abstract onExceedLimit()V
.end method

.method public abstract onFocusChanged(Z)V
.end method

.method public abstract onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
.end method

.method public abstract onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
.end method

.method public abstract onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
.end method

.method public abstract onRequestScroll(FF)V
.end method

.method public abstract onSelectionChanged(II)Z
.end method

.method public abstract onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
.end method

.method public abstract onUndo()V
.end method

.method public abstract onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
.end method
