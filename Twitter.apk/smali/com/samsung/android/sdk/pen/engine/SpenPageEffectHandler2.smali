.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;
.super Landroid/os/Handler;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler;


# static fields
.field private static final PAGE_EFFECT_IN:I = 0x1

.field private static final PAGE_EFFECT_OUT:I = 0x2

.field private static final PAGE_EFFECT_TIMER_INTERVAL:I = 0x0

.field private static final TAG:Ljava/lang/String; = "PageEffectHandler"


# instance fields
.field private mBlackPaint:Landroid/graphics/Paint;

.field private mCount:I

.field private mCurrentX:F

.field private mDirection:I

.field private mDistanceTable:[F

.field private mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

.field private mScreenHeight:I

.field private mScreenWidth:I

.field private mScreenshot:Landroid/graphics/Bitmap;

.field private mSrcPaint:Landroid/graphics/Paint;

.field private mWorking:Z


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;)V
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    const/16 v0, 0x14

    new-array v0, v0, [F

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->SRC:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    return-void

    nop

    :array_0
    .array-data 4
        0x3e19999a    # 0.15f
        0x3e4f0414
        0x3e8b8100
        0x3ebc048d
        0x3efd6720
        0x3f2ac35b
        0x3f6625ce
        0x3f9b1788
        0x3fd106d5
        0x400cdbe1
        0x403dd810
        0x407fdd38
        0x40ac6bf6
        0x40e86213
        0x411c992c
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
        0x41480000    # 12.5f
    .end array-data
.end method

.method private endAnimation()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onFinish()V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public drawAnimation(Landroid/graphics/Canvas;)V
    .locals 7

    const/4 v2, 0x0

    const-string/jumbo v0, "PageEffectHandler"

    const-string/jumbo v1, "Start draw animation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mSrcPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    cmpg-float v0, v0, v2

    if-gez v0, :cond_0

    const-string/jumbo v0, "PageEffectHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v3, v4

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ") ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v0, v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v1, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v3, v0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    :goto_0
    const-string/jumbo v0, "PageEffectHandler"

    const-string/jumbo v1, "Start draw animation"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_0
    const-string/jumbo v0, "PageEffectHandler"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "(0, 0) ("

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ", "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ")"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    int-to-float v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    move-object v1, p1

    move v3, v2

    invoke-virtual/range {v1 .. v6}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)V
    .locals 7

    const/4 v2, 0x2

    const v6, 0x3c23d70a    # 0.01f

    const-wide/16 v4, 0x0

    const/4 v3, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v2, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v0, v0

    const/high16 v1, 0x41000000    # 8.0f

    div-float/2addr v0, v1

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-nez v1, :cond_2

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-ltz v0, :cond_3

    const/16 v0, 0x13

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer(Landroid/graphics/Canvas;)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    neg-float v0, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    :cond_1
    :goto_1
    return-void

    :cond_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-ne v1, v3, :cond_0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    goto :goto_0

    :cond_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    invoke-virtual {p0, v2, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1

    :cond_4
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    array-length v1, v1

    if-lt v0, v1, :cond_6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    array-length v2, v2

    add-int/lit8 v2, v2, -0x1

    aget v1, v1, v2

    mul-float/2addr v0, v1

    mul-float/2addr v0, v6

    :goto_2
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-nez v1, :cond_7

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    add-float/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    :cond_5
    :goto_3
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    if-nez v0, :cond_8

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->endAnimation()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    goto :goto_1

    :cond_6
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    int-to-float v0, v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDistanceTable:[F

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    aget v1, v1, v2

    mul-float/2addr v0, v1

    mul-float/2addr v0, v6

    goto :goto_2

    :cond_7
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    if-ne v1, v3, :cond_5

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    sub-float v0, v1, v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    goto :goto_3

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdate()V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    invoke-virtual {p0, v3, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_1
.end method

.method public isWorking()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    return v0
.end method

.method public saveScreenshot()Z
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer(Landroid/graphics/Canvas;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method public setCanvasInformation(IIII)V
    .locals 0

    return-void
.end method

.method public setPaint(Landroid/graphics/Paint;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mBlackPaint:Landroid/graphics/Paint;

    return-void
.end method

.method public setScreenResolution(II)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenWidth:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenHeight:I

    return-void
.end method

.method public startAnimation(I)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mScreenshot:Landroid/graphics/Bitmap;

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCount:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mCurrentX:F

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mDirection:I

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->mWorking:Z

    const/4 v0, 0x2

    const-wide/16 v2, 0x0

    invoke-virtual {p0, v0, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler2;->sendEmptyMessageDelayed(IJ)Z

    move v0, v1

    goto :goto_0
.end method
