.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenTextChangeListener;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final CONTROL_STATE_NOT_CHANGED:I = 0x2

.field public static final CONTROL_STATE_SELECTED:I = 0x0

.field public static final CONTROL_STATE_UNSELECTED:I = 0x1


# virtual methods
.method public abstract onChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;I)V
.end method

.method public abstract onFocusChanged(Z)V
.end method

.method public abstract onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
.end method

.method public abstract onSelectionChanged(II)Z
.end method
