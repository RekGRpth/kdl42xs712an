.class public Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final TAG:Ljava/lang/String; = "SpenCapturePage"


# instance fields
.field private mBitmapHeight:I

.field private mBitmapWidth:I

.field private mCanvasLayer:Landroid/graphics/Bitmap;

.field private mIsHyperText:Z

.field private mNativeCapture:I

.field private mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_init()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_construct(ILandroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    const/16 v0, 0x8

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_0
    return-void
.end method

.method private createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 5

    const/4 v3, 0x6

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    if-eqz v0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v2

    iput v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    if-nez v2, :cond_2

    const-string/jumbo v0, "The width of pageDoc is 0"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_2
    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    if-nez v2, :cond_3

    const-string/jumbo v0, "The height of pageDoc is 0"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0

    :cond_3
    const-string/jumbo v2, "SpenCapturePage"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "createBitmap Width="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, " Height="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    if-ne v0, v2, :cond_4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    if-eq v1, v0, :cond_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    const/4 v1, 0x0

    invoke-virtual {p1, v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getLayerIdByIndex(I)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V

    goto :goto_0
.end method

.method private static native native_captureRect(ILandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z
.end method

.method private static native native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;
.end method

.method private static native native_construct(ILandroid/content/Context;)Z
.end method

.method private static native native_finalize(I)V
.end method

.method private static native native_init()I
.end method

.method private static native native_setCanvasBitmap(IILandroid/graphics/Bitmap;)V
.end method

.method private static native native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z
.end method

.method private static native native_setScreenSize(III)Z
.end method


# virtual methods
.method public capturePage(F)Landroid/graphics/Bitmap;
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p1

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p1

    float-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0
.end method

.method public captureRect(Landroid/graphics/RectF;)Landroid/graphics/Bitmap;
    .locals 4

    const/4 v0, 0x0

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    :try_start_0
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v2

    float-to-int v2, v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    invoke-static {v2, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_captureRect(ILandroid/graphics/Bitmap;Landroid/graphics/RectF;)Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    if-eqz v2, :cond_0

    move-object v0, v1

    goto :goto_0

    :catch_0
    move-exception v1

    const-string/jumbo v1, "SpenCapturePage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Failed to create bitmap w = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string/jumbo v3, " h = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x2

    const-string/jumbo v2, " : fail createBitmap."

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_finalize(I)V

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    goto :goto_0
.end method

.method public compressPage(Ljava/lang/String;F)V
    .locals 8

    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-nez p1, :cond_2

    const/16 v0, 0x9

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_2
    float-to-double v0, p2

    cmpl-double v0, v0, v6

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    int-to-float v1, v1

    mul-float/2addr v1, p2

    float-to-int v1, v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    int-to-float v2, v2

    mul-float/2addr v2, p2

    float-to-int v2, v2

    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_1
    const/4 v3, 0x0

    :try_start_0
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, p1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Ljava/io/File;->createNewFile()Z

    new-instance v2, Ljava/io/FileOutputStream;

    invoke-direct {v2, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    sget-object v1, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v3, v2}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_7
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_6
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :goto_2
    if-eqz v2, :cond_3

    :try_start_2
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5

    :cond_3
    :goto_3
    float-to-double v1, p2

    cmpl-double v1, v1, v6

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mCanvasLayer:Landroid/graphics/Bitmap;

    goto :goto_1

    :catch_0
    move-exception v1

    move-object v2, v3

    :goto_4
    :try_start_3
    const-string/jumbo v3, "SpenCapturePage"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "filename = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " width = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " height = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ratio = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    if-eqz v2, :cond_3

    :try_start_4
    invoke-virtual {v2}, Ljava/io/OutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_3

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_3

    :catch_2
    move-exception v1

    :goto_5
    :try_start_5
    const-string/jumbo v2, "SpenCapturePage"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "filename = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " width = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " height = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, " ratio = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    if-eqz v3, :cond_3

    :try_start_6
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3

    goto/16 :goto_3

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :catchall_0
    move-exception v0

    :goto_6
    if-eqz v3, :cond_5

    :try_start_7
    invoke-virtual {v3}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :cond_5
    :goto_7
    throw v0

    :catch_4
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :catch_5
    move-exception v1

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto/16 :goto_3

    :catchall_1
    move-exception v0

    move-object v3, v2

    goto :goto_6

    :catch_6
    move-exception v1

    move-object v3, v2

    goto :goto_5

    :catch_7
    move-exception v1

    goto/16 :goto_4

    :cond_6
    move-object v2, v3

    goto/16 :goto_2
.end method

.method public isHyperTextViewEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    return v0
.end method

.method public setHyperTextViewEnabled(Z)V
    .locals 4

    const/4 v1, 0x1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    const/4 v3, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mIsHyperText:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v2, v1, v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_command(IILjava/util/ArrayList;I)Ljava/util/ArrayList;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setPageDoc(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 3

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    const-string/jumbo v0, "SpenCapturePage"

    const-string/jumbo v1, "setPageDoc is closed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->createBitmap(Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mBitmapHeight:I

    invoke-static {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setScreenSize(III)Z

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->mNativeCapture:I

    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenCapturePage;->native_setPageDoc(ILcom/samsung/android/sdk/pen/document/SpenPageDoc;)Z

    goto :goto_0
.end method
