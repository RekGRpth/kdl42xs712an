.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
.super Landroid/view/SurfaceView;
.source "Twttr"

# interfaces
.implements Landroid/hardware/Camera$PreviewCallback;
.implements Landroid/view/SurfaceHolder$Callback;


# static fields
.field private static final TAG:Ljava/lang/String; = "CameraFrame"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mCamera:Landroid/hardware/Camera;

.field private mCameraDegree:I

.field private mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

.field private mIsAutofocus:Z

.field private mIsPreviewCreated:Z

.field private mPictureHeight:I

.field private mPictureWidth:I

.field private mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

.field private mPreviewHeight:I

.field private mPreviewWidth:I

.field private final mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mZoomMax:I

.field private final pictureListener:Landroid/hardware/Camera$PictureCallback;

.field private final shutterListener:Landroid/hardware/Camera$ShutterCallback;


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0, p1}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->shutterListener:Landroid/hardware/Camera$ShutterCallback;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->pictureListener:Landroid/hardware/Camera$PictureCallback;

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mActivity:Landroid/app/Activity;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setKeepScreenOn(Z)V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    return-object v0
.end method


# virtual methods
.method public cancelAutoFocus()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->cancelAutoFocus()V

    return-void
.end method

.method public getCamera()Landroid/hardware/Camera;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    return-object v0
.end method

.method public getCameraDegree()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    return v0
.end method

.method public initCamera()V
    .locals 8

    const/4 v7, 0x0

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v7}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    :cond_0
    :try_start_0
    invoke-static {}, Landroid/hardware/Camera;->open()Landroid/hardware/Camera;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    const-string/jumbo v0, "TAG"

    const-string/jumbo v1, "Camera open failed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "Camera open exception"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setPreviewDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, p0}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :goto_2
    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v2

    if-eqz v2, :cond_8

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPictureSizes()Ljava/util/List;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_9

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    if-nez v0, :cond_3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v4, v0, Landroid/hardware/Camera$Size;->width:I

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    :cond_3
    const-string/jumbo v0, "CameraFrame"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "PictureSize Width: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string/jumbo v5, "Height :"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedPreviewSizes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_5

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_4
    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_a

    :cond_5
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    if-nez v0, :cond_6

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v3, v0, Landroid/hardware/Camera$Size;->width:I

    iput v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    :cond_6
    const-string/jumbo v0, "CameraFrame"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string/jumbo v4, "PreviewSize Width: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, "Height :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    invoke-virtual {v2, v0, v3}, Landroid/hardware/Camera$Parameters;->setPreviewSize(II)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    invoke-virtual {v2, v0, v3}, Landroid/hardware/Camera$Parameters;->setPictureSize(II)V

    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getSupportedFocusModes()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_7

    const-string/jumbo v3, "continuous-picture"

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    const-string/jumbo v0, "continuous-picture"

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    :cond_7
    :goto_5
    invoke-virtual {v2}, Landroid/hardware/Camera$Parameters;->getMaxZoom()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mZoomMax:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v2}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    :cond_8
    :try_start_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->startPreview()V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_3

    :goto_6
    new-instance v2, Landroid/hardware/Camera$CameraInfo;

    invoke-direct {v2}, Landroid/hardware/Camera$CameraInfo;-><init>()V

    invoke-static {v1, v2}, Landroid/hardware/Camera;->getCameraInfo(ILandroid/hardware/Camera$CameraInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v0

    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v1

    :goto_7
    iget v1, v2, Landroid/hardware/Camera$CameraInfo;->orientation:I

    sub-int v0, v1, v0

    add-int/lit16 v0, v0, 0x168

    rem-int/lit16 v0, v0, 0x168

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCameraDegree:I

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setDisplayOrientation(I)V

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_1

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_2

    :cond_9
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    mul-int/lit8 v5, v5, 0x3

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/lit8 v6, v6, 0x4

    if-ne v5, v6, :cond_2

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    const/16 v6, 0x4b0

    if-ge v5, v6, :cond_2

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iget v6, v0, Landroid/hardware/Camera$Size;->width:I

    if-ge v5, v6, :cond_2

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureWidth:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPictureHeight:I

    goto/16 :goto_3

    :cond_a
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/Camera$Size;

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    mul-int/lit8 v5, v5, 0x3

    iget v6, v0, Landroid/hardware/Camera$Size;->height:I

    mul-int/lit8 v6, v6, 0x4

    if-ne v5, v6, :cond_4

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget v6, v0, Landroid/hardware/Camera$Size;->width:I

    if-ge v5, v6, :cond_4

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    const/16 v6, 0x320

    if-ge v5, v6, :cond_4

    iget v5, v0, Landroid/hardware/Camera$Size;->width:I

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewWidth:I

    iget v0, v0, Landroid/hardware/Camera$Size;->height:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewHeight:I

    goto/16 :goto_4

    :cond_b
    const-string/jumbo v3, "auto"

    invoke-interface {v0, v3}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const-string/jumbo v0, "auto"

    invoke-virtual {v2, v0}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_c
    const-string/jumbo v0, "CameraFrame"

    const-string/jumbo v3, "No focus mode"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_5

    :catch_3
    move-exception v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    invoke-interface {v0, v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;->onComplete([B)V

    goto/16 :goto_6

    :pswitch_0
    move v0, v1

    goto/16 :goto_7

    :pswitch_1
    const/16 v0, 0x5a

    goto/16 :goto_7

    :pswitch_2
    const/16 v0, 0xb4

    goto/16 :goto_7

    :pswitch_3
    const/16 v0, 0x10e

    goto/16 :goto_7

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    return-void
.end method

.method public onPreviewFrame([BLandroid/hardware/Camera;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;->OnPreview()V

    return-void
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 1

    invoke-super {p0}, Landroid/view/SurfaceView;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v0

    return-object v0
.end method

.method public setFocus(Landroid/graphics/Rect;)V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_0

    const-string/jumbo v0, "CameraFrame"

    const-string/jumbo v1, "Camera is not created yet."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    if-nez v0, :cond_1

    const-string/jumbo v0, "CameraFrame"

    const-string/jumbo v1, "Preview of Camera is not  created yet."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    new-instance v0, Landroid/hardware/Camera$Area;

    const/16 v1, 0x3e8

    invoke-direct {v0, p1, v1}, Landroid/hardware/Camera$Area;-><init>(Landroid/graphics/Rect;I)V

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusAreas(Ljava/util/List;)V

    const-string/jumbo v1, "auto"

    invoke-virtual {v0, v1}, Landroid/hardware/Camera$Parameters;->setFocusMode(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1, v0}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    const-string/jumbo v0, "CameraFrame"

    const-string/jumbo v1, "setFocus."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->autoFocus(Landroid/hardware/Camera$AutoFocusCallback;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "CameraFrame"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "Preview of Camera is not  created yet. state = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public setOnCompleteCameraFrameListener(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCompleteListener:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnCompleteCameraFrameListener;

    return-void
.end method

.method public setOnPreviewCallback(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mPreviewCallback:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView$OnPreviewCallback;

    return-void
.end method

.method public setZoom(I)V
    .locals 3

    const/16 v0, 0x64

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v1, :cond_0

    const-string/jumbo v0, "CameraFrame"

    const-string/jumbo v1, "camera open error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    if-gez p1, :cond_2

    const/4 v1, 0x0

    :goto_1
    if-le v1, v0, :cond_1

    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v1}, Landroid/hardware/Camera;->getParameters()Landroid/hardware/Camera$Parameters;

    move-result-object v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mZoomMax:I

    mul-int/2addr v0, v2

    div-int/lit8 v0, v0, 0x64

    invoke-virtual {v1, v0}, Landroid/hardware/Camera$Parameters;->setZoom(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setParameters(Landroid/hardware/Camera$Parameters;)V

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_2

    :cond_2
    move v1, p1

    goto :goto_1
.end method

.method public stop()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0, v1}, Landroid/hardware/Camera;->setPreviewCallback(Landroid/hardware/Camera$PreviewCallback;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->stopPreview()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    invoke-virtual {v0}, Landroid/hardware/Camera;->release()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->initCamera()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsPreviewCreated:Z

    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->stop()V

    return-void
.end method

.method public takePicture()V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    if-nez v0, :cond_1

    const-string/jumbo v0, "KYKY"

    const-string/jumbo v1, "KYKY 2 camera open error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    if-nez v0, :cond_0

    const-string/jumbo v0, "KYKY"

    const-string/jumbo v1, "KYKY Start autofocus."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mIsAutofocus:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->mCamera:Landroid/hardware/Camera;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->shutterListener:Landroid/hardware/Camera$ShutterCallback;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->pictureListener:Landroid/hardware/Camera$PictureCallback;

    invoke-virtual {v0, v1, v2, v3}, Landroid/hardware/Camera;->takePicture(Landroid/hardware/Camera$ShutterCallback;Landroid/hardware/Camera$PictureCallback;Landroid/hardware/Camera$PictureCallback;)V

    goto :goto_0
.end method
