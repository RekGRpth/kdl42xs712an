.class Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;
.super Landroid/view/GestureDetector$SimpleOnGestureListener;
.source "Twttr"


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-direct {p0}, Landroid/view/GestureDetector$SimpleOnGestureListener;-><init>()V

    return-void
.end method

.method private isEventWithinView(Landroid/view/MotionEvent;Landroid/view/View;)Z
    .locals 5

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    const/4 v1, 0x2

    new-array v1, v1, [I

    invoke-virtual {p2, v1}, Landroid/view/View;->getLocationOnScreen([I)V

    const/4 v2, 0x0

    aget v2, v1, v2

    invoke-virtual {p2}, Landroid/view/View;->getWidth()I

    move-result v3

    add-int/2addr v3, v2

    const/4 v4, 0x1

    aget v1, v1, v4

    invoke-virtual {p2}, Landroid/view/View;->getHeight()I

    move-result v4

    add-int/2addr v4, v1

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/graphics/Rect;->set(IIII)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawX()F

    move-result v1

    float-to-int v1, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Rect;->contains(II)Z

    move-result v0

    return v0
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->onDown(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z

    move-result v0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v1

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-lt v4, v1, :cond_1

    :cond_0
    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->isEventWithinView(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnItemLongClicked:Landroid/widget/AdapterView$OnItemLongClickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$12(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/AdapterView$OnItemLongClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v6

    add-int/lit8 v6, v6, 0x1

    add-int/2addr v4, v6

    invoke-interface {v5, v4}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemLongClickListener;->onItemLongClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    goto :goto_1

    :cond_2
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 6

    const/4 v5, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v2

    :goto_0
    if-lt v0, v2, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    float-to-int v3, p3

    add-int/2addr v2, v3

    iput v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    if-gtz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v2, 0x1

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v0

    const v2, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v2}, Landroid/widget/EdgeEffect;->onPull(F)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mNextX:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mMaxX:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$6(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v2

    if-lt v0, v2, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mRightEdgeEffect:Landroid/widget/EdgeEffect;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$7(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/EdgeEffect;

    move-result-object v0

    const v2, 0x3e99999a    # 0.3f

    invoke-virtual {v0, v2}, Landroid/widget/EdgeEffect;->onPull(F)V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->requestLayout()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$8(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;

    move-result-object v0

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;->onScrolll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V

    :cond_2
    return v5

    :cond_3
    :try_start_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/View;->setPressed(Z)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 7

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildCount()I

    move-result v0

    if-lt v6, v0, :cond_1

    :cond_0
    :goto_1
    const/4 v0, 0x1

    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->isEventWithinView(Landroid/view/MotionEvent;Landroid/view/View;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnItemClicked:Landroid/widget/AdapterView$OnItemClickListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/AdapterView$OnItemClickListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v6

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v6

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemClickListener;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mOnItemSelected:Landroid/widget/AdapterView$OnItemSelectedListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$11(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)Landroid/widget/AdapterView$OnItemSelectedListener;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    add-int/2addr v3, v6

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mAdapter:Landroid/widget/ListAdapter;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$2;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->mLeftViewIndex:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->access$10(Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;)I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    add-int/2addr v5, v6

    invoke-interface {v4, v5}, Landroid/widget/ListAdapter;->getItemId(I)J

    move-result-wide v4

    invoke-interface/range {v0 .. v5}, Landroid/widget/AdapterView$OnItemSelectedListener;->onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    goto :goto_1

    :cond_3
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0
.end method
