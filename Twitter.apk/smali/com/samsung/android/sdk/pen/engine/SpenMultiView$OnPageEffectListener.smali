.class Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)V

    return-void
.end method


# virtual methods
.method public onFinish()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->mPageEffectListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectListener;->onFinish()V

    :cond_0
    return-void
.end method

.method public onUpdate()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->invalidate()V

    return-void
.end method

.method public onUpdateCanvasLayer(Landroid/graphics/Canvas;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    const/4 v1, 0x0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas(Landroid/graphics/Canvas;Z)V
    invoke-static {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;Z)V

    return-void
.end method

.method public onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenMultiView$OnPageEffectListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenMultiView;

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->updateCanvas2(Landroid/graphics/Canvas;)V
    invoke-static {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenMultiView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenMultiView;Landroid/graphics/Canvas;)V

    return-void
.end method
