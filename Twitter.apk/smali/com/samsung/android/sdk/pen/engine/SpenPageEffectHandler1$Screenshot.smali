.class Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public bmp:Landroid/graphics/Bitmap;

.field public dst:Landroid/graphics/Rect;

.field public src:Landroid/graphics/Rect;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;


# direct methods
.method public constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    return-void
.end method


# virtual methods
.method public clean()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->src:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->dst:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    return-void
.end method

.method public saveScreenshot()Z
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mCanvasHeight:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)I

    move-result v2

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_1

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1;)Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler$Listener;->onUpdateCanvasLayer2(Landroid/graphics/Canvas;)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :catch_0
    move-exception v1

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    goto :goto_0

    :catch_1
    move-exception v1

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenPageEffectHandler1$Screenshot;->bmp:Landroid/graphics/Bitmap;

    goto :goto_0
.end method
