.class Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/ScaleGestureDetector$OnScaleGestureListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)V

    return-void
.end method


# virtual methods
.method public onScale(Landroid/view/ScaleGestureDetector;)Z
    .locals 5

    const/high16 v4, 0x40a00000    # 5.0f

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusX()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->x:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getFocusY()F

    move-result v1

    iput v1, v0, Landroid/graphics/PointF;->y:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F

    move-result v1

    invoke-virtual {p1}, Landroid/view/ScaleGestureDetector;->getScaleFactor()F

    move-result v2

    sub-float v2, v3, v2

    sub-float/2addr v1, v2

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F

    move-result v0

    cmpg-float v0, v0, v3

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;F)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F

    move-result v0

    cmpl-float v0, v0, v4

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;F)V

    :cond_1
    const-string/jumbo v0, "SpenSimpleView"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "onScale=["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->x:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;

    move-result-object v2

    iget v2, v2, Landroid/graphics/PointF;->y:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/Matrix;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;

    move-result-object v3

    iget v3, v3, Landroid/graphics/PointF;->x:F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;

    move-result-object v4

    iget v4, v4, Landroid/graphics/PointF;->y:F

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Matrix;->setScale(FFFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate()V

    const/4 v0, 0x1

    return v0
.end method

.method public onScaleBegin(Landroid/view/ScaleGestureDetector;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onScaleEnd(Landroid/view/ScaleGestureDetector;)V
    .locals 0

    return-void
.end method
