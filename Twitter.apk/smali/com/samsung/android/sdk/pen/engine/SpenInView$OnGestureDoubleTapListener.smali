.class Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnDoubleTapListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V

    return-void
.end method


# virtual methods
.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1, v5}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->getToolTypeAction(I)I

    move-result v0

    const/4 v1, 0x5

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "SpenInView"

    const-string/jumbo v1, "one finger double tab"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mRatioBitmapWidth:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v1

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3fc00000    # 1.5f

    mul-float/2addr v3, v4

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->updateScreenFrameBuffer()V

    :cond_0
    return v5

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mScreenWidth:I
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v3

    int-to-float v3, v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$OnGestureDoubleTapListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mBitmapWidth:I
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenInView;)I

    move-result v4

    int-to-float v4, v4

    div-float/2addr v3, v4

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->setZoom(FFF)V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method
