.class public Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;
.super Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
.source "Twttr"


# static fields
.field private static final DBG:Z = true

.field private static final DEFAULT_BORDER_LEFT_MARGIN:I = 0x3

.field private static final TEXT_INPUT_UNLIMITED:I = -0x1


# instance fields
.field private BOTTOM_MARGIN:I

.field private LEFT_MARGIN:I

.field private RIGHT_MARGIN:I

.field private TOP_MARGIN:I

.field private mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

.field private mEditable:Z

.field private mFirstDraw:Z

.field private final mGestureDetector:Landroid/view/GestureDetector;

.field private mReceiveActionDown:Z

.field private final mRequestObjectChange:Ljava/lang/Runnable;

.field private mSizeChanged:Z

.field private mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

.field private final mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

.field private mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

.field private mTransactionFitTextBox:Z

.field private final mUpdateHandler:Landroid/os/Handler;

.field private mUseTextEraser:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V
    .locals 3

    const/4 v2, 0x0

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;-><init>(Landroid/content/Context;Lcom/samsung/android/sdk/pen/document/SpenPageDoc;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$1;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    new-instance v0, Landroid/view/GestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;

    invoke-direct {v1, p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUpdateHandler:Landroid/os/Handler;

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$2;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    return-void
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    return-object v0
.end method

.method private checkSettingTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 4

    const/4 v3, 0x0

    const/4 v2, 0x7

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-ltz v0, :cond_0

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_1

    :cond_0
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_1
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    if-ltz v0, :cond_2

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    const/4 v1, 0x1

    if-le v0, v1, :cond_3

    :cond_2
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_3
    iget-object v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    if-eqz v0, :cond_4

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    cmpg-float v0, v0, v3

    if-ltz v0, :cond_4

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    cmpg-float v0, v0, v3

    if-gez v0, :cond_5

    :cond_4
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_5
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-ltz v0, :cond_6

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-le v0, v2, :cond_7

    :cond_6
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_7
    return-void
.end method

.method private checkTextBoxValidation()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private verifyRect(Landroid/graphics/RectF;)Z
    .locals 3

    const/4 v0, 0x0

    const/4 v2, 0x0

    if-nez p1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Landroid/graphics/RectF;->width()F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    invoke-virtual {p1}, Landroid/graphics/RectF;->height()F

    move-result v1

    cmpl-float v1, v1, v2

    if-lez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public appendText(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->appendText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public close()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    :cond_0
    :goto_0
    return-void

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getObjectText()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    :try_end_0
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    if-eqz v0, :cond_4

    :try_start_1
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->parseHyperText()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getText()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getTemplateProperty()Z

    move-result v1

    if-eqz v1, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isViewModeEnabled()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setVereticalPan(F)V

    :cond_3
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_6

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onVisibleUpdated(Z)V
    :try_end_1
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_4
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->close()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    :try_end_2
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_2 .. :try_end_2} :catch_1

    :cond_5
    :goto_2
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->close()V

    goto :goto_0

    :cond_6
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V
    :try_end_3
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    :try_start_4
    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V
    :try_end_4
    .catch Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException; {:try_start_4 .. :try_end_4} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;->printStackTrace()V

    goto :goto_2
.end method

.method public fit(Z)V
    .locals 1

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionFitTextBox:Z

    goto :goto_0
.end method

.method public getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;
    .locals 2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    return-object v0
.end method

.method public getPixel(II)I
    .locals 4

    const/4 v0, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getX()F

    move-result v1

    add-float/2addr v1, v3

    float-to-int v1, v1

    sub-int v1, p1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getY()F

    move-result v2

    add-float/2addr v2, v3

    float-to-int v2, v2

    sub-int v2, p2, v2

    if-lez v1, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getWidth()I

    move-result v3

    if-ge v1, v3, :cond_0

    if-lez v2, :cond_0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getHeight()I

    move-result v3

    if-lt v2, v3, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getPixel(II)I

    move-result v0

    goto :goto_0
.end method

.method public getText(Z)Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTextCursorPosition()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getCursorPos()I

    move-result v0

    goto :goto_0
.end method

.method public getTextLimit()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getTextLimit()I

    move-result v0

    goto :goto_0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 2

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;-><init>()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineIndent:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    return-object v0
.end method

.method protected handleTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 12

    const/16 v11, 0x8

    const/4 v10, 0x6

    const/4 v9, 0x3

    const/4 v3, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    :goto_0
    return v2

    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    goto :goto_0

    :pswitch_0
    const-string/jumbo v0, "handleTouchEvent() MotionEvent.ACTION_UP"

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(ZLjava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isResizeZonePressed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isRotateZonePressed()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isMoveZonePressed()Z

    move-result v0

    if-eqz v0, :cond_5

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isCornerZonePressed()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isHorizontalResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v0

    and-int/lit8 v0, v0, -0x2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->isVerticalResizeZonePressed()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getAutoFitOption()I

    move-result v0

    and-int/lit8 v0, v0, -0x3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setAutoFitOption(I)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultWidth()I

    move-result v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getDefaultHeight()I

    move-result v5

    move v1, v2

    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v1, v0, :cond_6

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    :cond_5
    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->invalidate()V

    move v2, v3

    goto/16 :goto_0

    :cond_6
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRectList()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/RectF;

    invoke-virtual {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getBoundBox(I)Landroid/graphics/RectF;

    move-result-object v6

    if-nez v6, :cond_7

    const-string/jumbo v0, "handleTouchEvent() boundBox is null."

    invoke-static {v3, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(ZLjava/lang/String;)V

    goto/16 :goto_0

    :cond_7
    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v7

    int-to-float v8, v4

    cmpg-float v7, v7, v8

    if-gez v7, :cond_9

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v11, :cond_8

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v9, :cond_8

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v8, 0x5

    if-ne v7, v8, :cond_c

    :cond_8
    iget v7, v0, Landroid/graphics/RectF;->left:F

    int-to-float v8, v4

    add-float/2addr v7, v8

    iput v7, v0, Landroid/graphics/RectF;->right:F

    iget v7, v6, Landroid/graphics/RectF;->left:F

    int-to-float v8, v4

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->right:F

    :cond_9
    :goto_2
    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v7

    int-to-float v8, v5

    cmpg-float v7, v7, v8

    if-gez v7, :cond_b

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v11, :cond_a

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v10, :cond_a

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v8, 0x7

    if-ne v7, v8, :cond_e

    :cond_a
    iget v7, v0, Landroid/graphics/RectF;->top:F

    int-to-float v8, v5

    add-float/2addr v7, v8

    iput v7, v0, Landroid/graphics/RectF;->bottom:F

    iget v7, v6, Landroid/graphics/RectF;->top:F

    int-to-float v8, v5

    add-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->bottom:F

    :cond_b
    :goto_3
    invoke-virtual {v6}, Landroid/graphics/RectF;->centerX()F

    move-result v7

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerX()F

    move-result v8

    sub-float/2addr v7, v8

    float-to-int v7, v7

    invoke-virtual {v6}, Landroid/graphics/RectF;->centerY()F

    move-result v6

    invoke-virtual {v0}, Landroid/graphics/RectF;->centerY()F

    move-result v8

    sub-float/2addr v6, v8

    float-to-int v6, v6

    int-to-float v7, v7

    int-to-float v6, v6

    invoke-virtual {v0, v7, v6}, Landroid/graphics/RectF;->offset(FF)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_1

    :cond_c
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v10, :cond_d

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v3, :cond_d

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v8, 0x4

    if-ne v7, v8, :cond_9

    :cond_d
    iget v7, v0, Landroid/graphics/RectF;->right:F

    int-to-float v8, v4

    sub-float/2addr v7, v8

    iput v7, v0, Landroid/graphics/RectF;->left:F

    iget v7, v6, Landroid/graphics/RectF;->right:F

    int-to-float v8, v4

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->left:F

    goto :goto_2

    :cond_e
    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-eq v7, v3, :cond_f

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    const/4 v8, 0x2

    if-eq v7, v8, :cond_f

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    iget v7, v7, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->mState:I

    if-ne v7, v9, :cond_b

    :cond_f
    iget v7, v0, Landroid/graphics/RectF;->bottom:F

    int-to-float v8, v5

    sub-float/2addr v7, v8

    iput v7, v0, Landroid/graphics/RectF;->top:F

    iget v7, v6, Landroid/graphics/RectF;->bottom:F

    int-to-float v8, v5

    sub-float/2addr v7, v8

    iput v7, v6, Landroid/graphics/RectF;->top:F

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public hideSoftInput()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    goto :goto_0
.end method

.method public isContextMenuVisible()Z
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuVisible()Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->isContextMenuVisible()Z

    move-result v0

    goto :goto_0
.end method

.method public isEditable()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    return v0
.end method

.method public isTextEraserEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 7

    const/4 v6, 0x1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    invoke-direct {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->verifyRect(Landroid/graphics/RectF;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isValid()Z

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getWidth()I

    move-result v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->getHeight()I

    move-result v5

    invoke-direct {v2, v3, v1, v4, v5}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;-><init>(Landroid/content/Context;Landroid/view/ViewGroup;II)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setObjectText(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setEraserMode(Z)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBoxActionListener:Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextBoxListener(Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getCursorPos()I

    move-result v0

    invoke-virtual {v1, v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setMargin(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    :cond_1
    :goto_1
    invoke-super {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onAttachedToWindow()V

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->bringToFront()V

    goto :goto_1
.end method

.method public onDraw(Landroid/graphics/Canvas;)V
    .locals 3

    const/4 v2, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mFirstDraw:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUpdateHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mRequestObjectChange:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    if-eqz v0, :cond_2

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    :cond_2
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method protected onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getStyle()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->left:F

    const/high16 v1, 0x40400000    # 3.0f

    sub-float/2addr v0, v1

    iput v0, p2, Landroid/graphics/RectF;->left:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTempPaint:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CPaint;->getPaint(I)Landroid/graphics/Paint;

    move-result-object v0

    invoke-virtual {p1, p2, v0}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onDrawBorder(Landroid/graphics/Canvas;Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V

    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    invoke-super {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onSizeChanged(IIII)V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;->reset()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mSizeChanged:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isContextMenuShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextMenuLocation()V

    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isTouchEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_3

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z

    :cond_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTransactionTouchEvent:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;

    invoke-virtual {v2, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTransactionTouchEvent;->check(Landroid/view/MotionEvent;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->mTextEraserEnable:Z

    if-eqz v2, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    if-ne v1, v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->close()V

    goto :goto_0

    :cond_4
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    and-int/lit16 v2, v2, 0xff

    packed-switch v2, :pswitch_data_0

    :cond_5
    :goto_1
    invoke-virtual {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->handleTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v2

    if-eqz v2, :cond_9

    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTouchState:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CTouchState;->reset()V

    :cond_6
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    and-int/lit16 v1, v1, 0xff

    if-ne v1, v0, :cond_7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->isFocused()Z

    :cond_7
    :goto_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v1, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    goto :goto_0

    :pswitch_0
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    :cond_8
    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideTextBox()V

    goto :goto_1

    :cond_9
    move v0, v1

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method protected onVisibleUpdated(Z)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onVisibleUpdated(Ljava/util/ArrayList;Z)V

    :cond_0
    return-void
.end method

.method public removeText()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->removeText()V

    goto :goto_0
.end method

.method public selectAllText()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelectionAll()V

    goto :goto_0
.end method

.method protected setActionListener(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mActionListener:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$ActionListener;

    return-void
.end method

.method public setContextMenuVisible(Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->isEditable()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContextMenuVisible(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->updateContextMenuLocation()V

    :goto_0
    return-void

    :cond_0
    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    goto :goto_0
.end method

.method public setEditable(Z)V
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    iget-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v2, :cond_3

    invoke-super {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    :cond_2
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    :goto_1
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObject()Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    move-result-object v2

    iget-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-eqz v3, :cond_5

    :goto_2
    invoke-virtual {v2, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRotatable(Z)V

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->bringToFront()V

    goto :goto_0

    :cond_3
    invoke-super {p0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setContextMenuVisible(Z)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v2, :cond_4

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mIsClosed:Z

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->hideSoftInput()V

    :cond_4
    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    goto :goto_1

    :cond_5
    move v0, v1

    goto :goto_2
.end method

.method public setMargin(IIII)V
    .locals 1

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->LEFT_MARGIN:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->RIGHT_MARGIN:I

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->TOP_MARGIN:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->BOTTOM_MARGIN:I

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, p3, p2, p4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setMargin(IIII)V

    goto :goto_0
.end method

.method public setObject(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 4

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    iget v2, v1, Landroid/graphics/RectF;->left:F

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->left:F

    iget v2, v1, Landroid/graphics/RectF;->right:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->right:F

    iget v2, v1, Landroid/graphics/RectF;->top:F

    float-to-int v2, v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->top:F

    iget v2, v1, Landroid/graphics/RectF;->bottom:F

    float-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v2

    double-to-int v2, v2

    int-to-float v2, v2

    iput v2, v1, Landroid/graphics/RectF;->bottom:F

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->setRect(Landroid/graphics/RectF;Z)V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setObjectList(Ljava/util/ArrayList;)V

    return-void
.end method

.method public setSelection(II)V
    .locals 4

    const/4 v3, 0x7

    const/4 v2, 0x1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    if-le p1, p2, :cond_2

    :cond_1
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_2
    if-ltz p1, :cond_3

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le p2, v0, :cond_4

    :cond_3
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_4
    if-ne p1, p2, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IZ)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1, p2, v2}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setSelection(IIZ)V

    goto :goto_0
.end method

.method public setText(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    if-nez p1, :cond_1

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setText(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public setTextCursorPosition(I)V
    .locals 3

    const/4 v2, 0x7

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->getText(Z)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_1

    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_1
    if-ltz p1, :cond_2

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-le p1, v0, :cond_3

    :cond_2
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    goto :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setCursorPos(I)V

    goto :goto_0
.end method

.method public setTextEraserEnabled(Z)V
    .locals 1

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mUseTextEraser:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setEraserMode(Z)V

    :cond_0
    return-void
.end method

.method public setTextLimit(I)V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    const/4 v0, -0x1

    if-ge p1, v0, :cond_1

    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextLimit(I)V

    goto :goto_0
.end method

.method public setTextSelectionContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setContextMenu(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V

    :cond_0
    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkSettingTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-eq v0, v2, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    if-le v0, v2, :cond_c

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget v2, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    sub-int v2, v0, v2

    const/4 v0, 0x0

    :goto_1
    and-int/lit8 v3, v2, 0x1

    if-ne v3, v1, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v3, v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    :cond_2
    and-int/lit8 v1, v2, 0x2

    if-ne v1, v4, :cond_3

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v4, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    :cond_3
    and-int/lit8 v1, v2, 0x4

    if-ne v1, v5, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v5, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextStyle(IZ)V

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    if-eq v0, v1, :cond_5

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;-><init>()V

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$ForegroundColorSpanInfo;->foregroundColor:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    :cond_5
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_6

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;-><init>()V

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontSizeSpanInfo;->fontSize:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_7

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;-><init>()V

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$FontNameSpanInfo;->fontName:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    :cond_7
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    if-eq v0, v1, :cond_8

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;-><init>()V

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    int-to-char v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextDirectionSpanInfo;->textDirection:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setTextFont(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextSpanInfo;)V

    :cond_8
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    if-eq v0, v1, :cond_9

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;-><init>()V

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    int-to-char v1, v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$AlignParagraphInfo;->align:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V

    :cond_9
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    cmpl-float v0, v0, v1

    if-nez v0, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    if-eq v0, v1, :cond_b

    :cond_a
    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;-><init>()V

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacingType:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->type:I

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$LineSpacingParagraphInfo;->lineSpacing:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->setParagraph(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox$TextParagraphInfo;)V

    :cond_b
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->color:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->align:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->lineSpacing:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->direction:I

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mEditable:Z

    if-nez v0, :cond_0

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->fit()V
    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->onObjectChanged()V

    goto/16 :goto_0

    :cond_c
    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v2, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->style:I

    sub-int/2addr v0, v2

    move v2, v0

    move v0, v1

    goto/16 :goto_1
.end method

.method public setTouchEnabled(Z)V
    .locals 1

    invoke-super {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->setTouchEnabled(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTouch(Z)V

    :cond_0
    return-void
.end method

.method public showSoftInput()V
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->checkTextBoxValidation()Z

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->showSoftInput()V

    goto :goto_0
.end method
