.class public Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;
.super Landroid/view/View;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "ViewConstructor"
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "SpenSimpleView"


# instance fields
.field private cancelTouch:Z

.field private canvasAction:[I

.field private currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

.field private dstRect:Landroid/graphics/RectF;

.field private eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFocus:Landroid/graphics/PointF;

.field private final mHeight:I

.field private mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

.field private mMatrix:Landroid/graphics/Matrix;

.field private mPenList:Ljava/util/ArrayList;

.field private mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

.field private mRatio:F

.field private mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

.field private mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

.field private mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

.field private final mWidth:I

.field private updateRect:Landroid/graphics/Rect;


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->construct(Landroid/content/Context;)V

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    invoke-direct {p0, p2, p3}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->createBitmap(II)V

    return-void
.end method

.method private JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V
    .locals 2

    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p2, Landroid/graphics/RectF;->right:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p2, Landroid/graphics/RectF;->bottom:F

    cmpl-float v0, v0, v1

    if-gez v0, :cond_0

    iget v0, p1, Landroid/graphics/Rect;->left:I

    iget v1, p1, Landroid/graphics/Rect;->right:I

    if-ge v0, v1, :cond_2

    iget v0, p1, Landroid/graphics/Rect;->top:I

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    if-lt v0, v1, :cond_3

    :cond_2
    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0

    :cond_3
    iget v0, p2, Landroid/graphics/RectF;->left:F

    iget v1, p1, Landroid/graphics/Rect;->left:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_4

    iget v0, p2, Landroid/graphics/RectF;->left:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->left:I

    :cond_4
    iget v0, p2, Landroid/graphics/RectF;->top:F

    iget v1, p1, Landroid/graphics/Rect;->top:I

    int-to-float v1, v1

    cmpg-float v0, v0, v1

    if-gez v0, :cond_5

    iget v0, p2, Landroid/graphics/RectF;->top:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->top:I

    :cond_5
    iget v0, p2, Landroid/graphics/RectF;->right:F

    iget v1, p1, Landroid/graphics/Rect;->right:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_6

    iget v0, p2, Landroid/graphics/RectF;->right:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->right:I

    :cond_6
    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    iget v1, p1, Landroid/graphics/Rect;->bottom:I

    int-to-float v1, v1

    cmpl-float v0, v0, v1

    if-lez v0, :cond_0

    iget v0, p2, Landroid/graphics/RectF;->bottom:F

    float-to-int v0, v0

    iput v0, p1, Landroid/graphics/Rect;->bottom:I

    goto :goto_0
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mRatio:F

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;)Landroid/graphics/Matrix;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;

    return-object v0
.end method

.method private construct(Landroid/content/Context;)V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x3

    const/4 v4, 0x0

    const/4 v3, 0x2

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->getPenInfoList()Ljava/util/List;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    const-string/jumbo v1, "com.samsung.android.sdk.pen.pen.preload.InkPen"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :goto_2
    const/4 v0, 0x5

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v4, v0, v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    const/4 v1, 0x1

    aput v3, v0, v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v3, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput v3, v0, v5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    const/4 v1, 0x4

    aput v5, v0, v1

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    new-instance v0, Landroid/graphics/PointF;

    invoke-direct {v0}, Landroid/graphics/PointF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;

    new-instance v0, Landroid/graphics/Matrix;

    invoke-direct {v0}, Landroid/graphics/Matrix;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/view/ScaleGestureDetector;

    new-instance v1, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;

    invoke-direct {v1, p0, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$onScaleGestureListener;)V

    invoke-direct {v0, p1, v1}, Landroid/view/ScaleGestureDetector;-><init>(Landroid/content/Context;Landroid/view/ScaleGestureDetector$OnScaleGestureListener;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    goto :goto_0

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    new-instance v2, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    invoke-direct {v2, v6}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;)V

    iput-object v0, v2, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_2

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_2

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_2

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_2
.end method

.method private createBitmap(II)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    :try_start_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    :cond_2
    :goto_1
    return-void

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v2, :cond_1

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x2

    const-string/jumbo v1, "Bitmap failed to create."

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_1
.end method

.method private onTouchGesture(Landroid/view/MotionEvent;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/ScaleGestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    :cond_0
    return-void
.end method

.method private onTouchInputEraser(Landroid/view/MotionEvent;)V
    .locals 4

    const/4 v3, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_0

    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    if-eqz v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    if-eqz v1, :cond_3

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3, v3, v3}, Landroid/graphics/Bitmap;->setPixel(III)V

    :cond_2
    packed-switch v0, :pswitch_data_0

    :cond_3
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0

    :pswitch_1
    iput-boolean v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->startPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->movePen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->endPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    :pswitch_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method private onTouchInputPen(Landroid/view/MotionEvent;)V
    .locals 3

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-nez v0, :cond_0

    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    :cond_0
    iget-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    if-eqz v1, :cond_1

    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v1, :cond_2

    new-instance v1, Landroid/graphics/RectF;

    invoke-direct {v1}, Landroid/graphics/RectF;-><init>()V

    packed-switch v0, :pswitch_data_0

    :cond_2
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate(Landroid/graphics/Rect;)V

    goto :goto_0

    :pswitch_1
    iput-boolean v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->setEmpty()V

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, p1, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    invoke-direct {p0, v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->JoinRect(Landroid/graphics/Rect;Landroid/graphics/RectF;)V

    goto :goto_1

    :pswitch_4
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->cancelTouch:Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method


# virtual methods
.method public captureCurrentView()Landroid/graphics/Bitmap;
    .locals 5

    const/4 v4, 0x0

    const/4 v1, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    :goto_0
    return-object v0

    :cond_1
    :try_start_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    sget-object v3, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v0, v2, v3}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_1
    new-instance v2, Landroid/graphics/Canvas;

    invoke-direct {v2, v0}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v3, v4, v4, v1}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v0, "SpenSimpleView"

    const-string/jumbo v2, "Failed to create bitmap"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x2

    const-string/jumbo v2, " : fail createBitmap."

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    move-object v0, v1

    goto :goto_1
.end method

.method public clearScreen()V
    .locals 3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    new-instance v0, Landroid/graphics/Canvas;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v0, v1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    const/4 v1, 0x0

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawColor(ILandroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->invalidate()V

    :cond_0
    return-void
.end method

.method public close()V
    .locals 5

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->defaultPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->close()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->close()V

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    :cond_3
    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->updateRect:Landroid/graphics/Rect;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mFocus:Landroid/graphics/PointF;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mMatrix:Landroid/graphics/Matrix;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    iput-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mScaleGestureDetector:Landroid/view/ScaleGestureDetector;

    return-void

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v2, :cond_5

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->destroyPen(Lcom/samsung/android/sdk/pen/pen/SpenPen;)V

    iput-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    :cond_5
    iput-object v4, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    goto :goto_0
.end method

.method public closeControl()V
    .locals 0

    return-void
.end method

.method public getCanvasHeight()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHeight:I

    return v0
.end method

.method public getCanvasWidth()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mWidth:I

    return v0
.end method

.method public getEraserSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    return-object v0
.end method

.method public getPenSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    return-object v0
.end method

.method public getRemoverSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getSelectionSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public getToolTypeAction(I)I
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x4

    if-gt p1, v1, :cond_0

    if-gez p1, :cond_1

    :cond_0
    move p1, v0

    :cond_1
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    if-eqz v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aget v0, v0, p1

    :cond_2
    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5

    const/4 v4, 0x0

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    int-to-float v1, v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/RectF;->set(FFFF)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->save()I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->dstRect:Landroid/graphics/RectF;

    invoke-virtual {p1, v0, v4, v1, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/RectF;Landroid/graphics/Paint;)V

    invoke-virtual {p1}, Landroid/graphics/Canvas;->restore()V

    invoke-super {p0, p1}, Landroid/view/View;->onDraw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 2

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;->onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_0
    return v1
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 3

    const/4 v2, 0x1

    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aget v0, v1, v0

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    invoke-interface {v0, p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;->onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_1
    return v2

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->onTouchInputPen(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->onTouchInputEraser(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->onTouchGesture(Landroid/view/MotionEvent;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setBackgroundColorChangeListener(Ljava/lang/Object;Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;)V
    .locals 2

    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "SpenSimpleView"

    const-string/jumbo v1, "setBackgroundColorListener"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-interface {p2, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface$SpenBackgroundColorChangeListener;->onChanged(Z)V

    :cond_0
    return-void
.end method

.method public setEraserSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;)V
    .locals 2

    if-eqz p1, :cond_1

    iget v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    const/4 v1, 0x0

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->eraser:Lcom/samsung/android/sdk/pen/engine/SpenEraser;

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->setSize(F)V

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingEraserInfo:Lcom/samsung/android/sdk/pen/SpenSettingEraserInfo;

    return-void
.end method

.method public setHoverListener(Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;)V
    .locals 2

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    const-string/jumbo v1, "4."

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mHoverListener:Lcom/samsung/android/sdk/pen/engine/SpenHoverListener;

    :goto_0
    return-void

    :cond_0
    const/16 v0, 0xc

    const-string/jumbo v1, "S Pen Hover Listener cannot be supported under android ICS"

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public setPenSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    if-eqz p1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    :cond_1
    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mSettingPenInfo:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    return-void

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;

    iget-object v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;->className:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v1

    if-nez v1, :cond_0

    iget v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    const/4 v3, 0x0

    cmpg-float v1, v1, v3

    if-gez v1, :cond_3

    const/high16 v1, 0x41200000    # 10.0f

    iput v1, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    :cond_3
    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-nez v1, :cond_4

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mPenManager:Lcom/samsung/android/sdk/pen/pen/SpenPenManager;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->penInfo:Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPenManager;->createPen(Lcom/samsung/android/sdk/pen/pen/SpenPenInfo;)Lcom/samsung/android/sdk/pen/pen/SpenPen;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setBitmap(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :cond_4
    :goto_1
    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setColor(I)V

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setSize(F)V

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v3, 0x3

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-boolean v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->isCurvable:Z

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setCurveEnabled(Z)V

    :cond_5
    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    const/4 v3, 0x4

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iget-object v3, p1, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/samsung/android/sdk/pen/pen/SpenPen;->setAdvancedSetting(Ljava/lang/String;)V

    :cond_6
    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView$PluginData;->handle:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->currentPen:Lcom/samsung/android/sdk/pen/pen/SpenPen;

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/ClassNotFoundException;->printStackTrace()V

    goto :goto_1

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InstantiationException;->printStackTrace()V

    goto :goto_1

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/IllegalAccessException;->printStackTrace()V

    goto :goto_1

    :catch_3
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method public setRemoverSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 0

    return-void
.end method

.method public setSelectionSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 0

    return-void
.end method

.method public setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 0

    return-void
.end method

.method public setToolTypeAction(II)V
    .locals 2

    const/4 v0, 0x0

    const/4 v1, 0x7

    if-gt p2, v1, :cond_0

    if-gez p2, :cond_1

    :cond_0
    move p2, v0

    :cond_1
    const/4 v1, 0x4

    if-gt p1, v1, :cond_2

    if-gez p1, :cond_3

    :cond_2
    move p1, v0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->canvasAction:[I

    aput p2, v0, p1

    :cond_4
    return-void
.end method

.method public setTouchListener(Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSimpleView;->mTouchListener:Lcom/samsung/android/sdk/pen/engine/SpenTouchListener;

    return-void
.end method
