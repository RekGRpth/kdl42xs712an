.class Lcom/samsung/android/sdk/pen/engine/SpenSLog;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field static final synthetic $assertionsDisabled:Z

.field private static CALL_DEPTH:I = 0x0

.field private static final CONFIG_LOGCAT_DBG:Z = true

.field private static final CONFIG_LOGFILE_DBG:Z = false

.field private static final DEFAULT_LOG_FILE:Ljava/lang/String;

.field private static DEFAULT_MAX_SIZE:I = 0x0

.field private static final TAG:Ljava/lang/String; = "SLog:"

.field private static mCall_Depth:I

.field private static sInstance:Lcom/samsung/android/sdk/pen/engine/SpenSLog;


# instance fields
.field private mBefore:J

.field private mFile:Ljava/io/File;

.field private mFout:Ljava/io/BufferedWriter;

.field private mSuccessFileCreation:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v2, 0x5

    const-class v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->$assertionsDisabled:Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {}, Landroid/os/Environment;->getExternalStorageDirectory()Ljava/io/File;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "/.SPenSDK30/control_log.txt"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->DEFAULT_LOG_FILE:Ljava/lang/String;

    const/16 v0, 0x2800

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->DEFAULT_MAX_SIZE:I

    sput v2, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->CALL_DEPTH:I

    sput v2, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->sInstance:Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>()V
    .locals 2

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mFout:Ljava/io/BufferedWriter;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mFile:Ljava/io/File;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mSuccessFileCreation:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mBefore:J

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->startLog()V

    return-void
.end method

.method public static d(Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static d(ZLjava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static e(Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static e(ZLjava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->e(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private static getCallingClassName()Ljava/lang/String;
    .locals 3

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v1

    sget v2, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    aget-object v1, v1, v2

    invoke-virtual {v1}, Ljava/lang/StackTraceElement;->getClassName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x2e

    invoke-virtual {v1, v2}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCallingLine()I
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v0

    return v0
.end method

.method private static getCallingMethodName()Ljava/lang/String;
    .locals 2

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v0

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getInstance()Lcom/samsung/android/sdk/pen/engine/SpenSLog;
    .locals 1

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->sInstance:Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    if-nez v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->sInstance:Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->sInstance:Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->startLog()V

    :cond_0
    sget-object v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->sInstance:Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    return-object v0
.end method

.method private static getMsg(Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getInstance()Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    move-result-object v2

    iget-wide v2, v2, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mBefore:J

    sub-long v2, v0, v2

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getInstance()Lcom/samsung/android/sdk/pen/engine/SpenSLog;

    move-result-object v4

    iput-wide v0, v4, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mBefore:J

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "("

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ": "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getTag()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string/jumbo v1, "SLog:["

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getCallingClassName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getCallingMethodName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getCallingLine()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sget v1, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->CALL_DEPTH:I

    sput v1, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    return-object v0
.end method

.method public static i(Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static i(ZLjava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->i(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static varargs printStackInfo([I)V
    .locals 5

    const/4 v1, 0x2

    sget-boolean v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    array-length v0, p0

    const/4 v2, 0x1

    if-gt v0, v2, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    array-length v0, p0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v2}, Ljava/lang/Throwable;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v2

    :goto_1
    add-int/lit8 v3, v0, 0x1

    if-lt v1, v3, :cond_2

    return-void

    :cond_1
    const/4 v0, 0x0

    aget v0, p0, v0

    goto :goto_0

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v4, "() line :"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    aget-object v4, v2, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getLineNumber()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->d(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private startLog()V
    .locals 0

    return-void
.end method

.method public static v(Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static v(ZLjava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->v(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static w(Ljava/lang/String;)V
    .locals 2

    invoke-static {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getMsg(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->getTag()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public static w(ZLjava/lang/String;)V
    .locals 1

    if-eqz p0, :cond_0

    sget v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mCall_Depth:I

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->w(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method private writeFile(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mSuccessFileCreation:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v0

    sget v2, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->DEFAULT_MAX_SIZE:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mFile:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_1
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->startLog()V

    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mFout:Ljava/io/BufferedWriter;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\r\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/BufferedWriter;->append(Ljava/lang/CharSequence;)Ljava/io/Writer;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mFout:Ljava/io/BufferedWriter;

    invoke-virtual {v0}, Ljava/io/BufferedWriter;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenSLog;->mSuccessFileCreation:Z

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method
