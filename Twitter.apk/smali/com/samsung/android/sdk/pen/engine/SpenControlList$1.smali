.class Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenTextBox$TextBoxActionListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExceedLimit()V
    .locals 0

    return-void
.end method

.method public onFocusChanged(Z)V
    .locals 0

    return-void
.end method

.method public onMoreButtonDown(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 0

    return-void
.end method

.method public onObjectChanged(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onObjectChanged(Ljava/util/ArrayList;)V

    :cond_0
    return-void
.end method

.method public onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->mListener:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ActionListener;->onRequestCoordinateInfo(Lcom/samsung/android/sdk/pen/engine/SpenControlBase$CoordinateInfo;)V

    :cond_0
    return-void
.end method

.method public onRequestScroll(FF)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->onRequestScroll(FF)V

    return-void
.end method

.method public onSelectionChanged(II)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onSettingTextInfoChanged(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 0

    return-void
.end method

.method public onUndo()V
    .locals 0

    return-void
.end method

.method public onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-virtual {v0, p1, p2}, Lcom/samsung/android/sdk/pen/engine/SpenControlList;->onVisibleUpdated(Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;Z)V

    return-void
.end method
