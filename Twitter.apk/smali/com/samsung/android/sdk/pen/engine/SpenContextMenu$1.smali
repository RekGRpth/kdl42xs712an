.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView$Listener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onScrolll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->isEdgeLeft()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageLeftItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->invalidate()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->isEdgeRight()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mBgImageRightItem:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$CustomImageView;->invalidate()V

    :cond_1
    return-void
.end method
