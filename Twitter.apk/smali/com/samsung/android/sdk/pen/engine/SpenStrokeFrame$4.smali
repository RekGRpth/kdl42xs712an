.class Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 8

    const/high16 v7, 0x40000000    # 2.0f

    const/4 v4, 0x0

    const/16 v1, 0x3ca

    const/16 v2, -0x3ca

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    move v0, v4

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Path;->reset()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/PointF;

    move-result-object v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/PointF;->set(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v3}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    move v0, v3

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->cancelAutoFocus()V

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPrevTouchPoint:Landroid/graphics/PointF;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$25(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/PointF;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Landroid/graphics/PointF;->set(FF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomY:F
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$31(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v0

    sub-float/2addr v0, v1

    float-to-int v0, v0

    div-int/lit8 v0, v0, 0x5

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v2

    rsub-int/lit8 v2, v2, 0x64

    if-ge v2, v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    rsub-int/lit8 v0, v0, 0x64

    :cond_0
    const/16 v2, 0xa

    if-le v0, v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/4 v5, 0x2

    invoke-static {v2, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    :cond_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomLast:I
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$32(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v5

    add-int/2addr v0, v5

    invoke-static {v2, v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    if-gez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    move v0, v3

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    const/16 v2, 0x64

    if-le v0, v2, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v2, 0x64

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$33(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    const/16 v2, 0x64

    invoke-static {v0, v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$29(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$27(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;F)V

    move v0, v3

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomPrev:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$34(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v1

    if-ne v0, v1, :cond_4

    move v0, v3

    goto/16 :goto_0

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v1

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$30(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomCur:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$28(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setZoom(I)V

    move v0, v3

    goto/16 :goto_0

    :pswitch_2
    new-instance v5, Landroid/graphics/PathMeasure;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mTouchCheckPath:Landroid/graphics/Path;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$24(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/Path;

    move-result-object v0

    invoke-direct {v5, v0, v4}, Landroid/graphics/PathMeasure;-><init>(Landroid/graphics/Path;Z)V

    invoke-virtual {p2, v4}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    const/4 v6, 0x2

    if-ne v0, v6, :cond_5

    const/high16 v0, 0x42480000    # 50.0f

    :goto_1
    invoke-virtual {v5}, Landroid/graphics/PathMeasure;->getLength()F

    move-result v5

    cmpg-float v0, v5, v0

    if-gez v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->takePicture()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    move v0, v3

    goto/16 :goto_0

    :cond_5
    const/high16 v0, 0x41200000    # 10.0f

    goto :goto_1

    :cond_6
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mZoomMode:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$35(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)I

    move-result v0

    if-ne v0, v3, :cond_a

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/RectF;->width()F

    move-result v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    div-float/2addr v0, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v5

    cmpl-float v0, v0, v5

    if-lez v0, :cond_b

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v7

    sub-float/2addr v0, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v0, v5

    const/high16 v5, -0x3b060000    # -2000.0f

    mul-float/2addr v0, v5

    float-to-int v0, v0

    if-ge v0, v2, :cond_7

    move v0, v2

    :cond_7
    if-le v0, v1, :cond_8

    move v0, v1

    :cond_8
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->width()F

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v7}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v7

    mul-float/2addr v6, v7

    div-float/2addr v5, v6

    const/high16 v6, 0x44fa0000    # 2000.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    if-ge v5, v2, :cond_10

    :goto_2
    if-le v2, v1, :cond_e

    :cond_9
    :goto_3
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mCameraView:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;

    move-result-object v2

    new-instance v5, Landroid/graphics/Rect;

    add-int/lit8 v6, v1, -0x1e

    add-int/lit8 v7, v0, -0x1e

    add-int/lit8 v1, v1, 0x1e

    add-int/lit8 v0, v0, 0x1e

    invoke-direct {v5, v6, v7, v1, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-virtual {v2, v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrameView;->setFocus(Landroid/graphics/Rect;)V

    :cond_a
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    invoke-static {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$26(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;I)V

    move v0, v3

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->width()F

    move-result v5

    div-float/2addr v5, v7

    sub-float/2addr v0, v5

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v5}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/RectF;->height()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mPreviewRatio:F
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)F

    move-result v6

    div-float/2addr v5, v6

    div-float/2addr v0, v5

    const/high16 v5, -0x3b060000    # -2000.0f

    mul-float/2addr v0, v5

    float-to-int v0, v0

    if-ge v0, v2, :cond_c

    move v0, v2

    :cond_c
    if-le v0, v1, :cond_d

    move v0, v1

    :cond_d
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v6, v7

    sub-float/2addr v5, v6

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame$4;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->mRect:Landroid/graphics/RectF;
    invoke-static {v6}, Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;->access$9(Lcom/samsung/android/sdk/pen/engine/SpenStrokeFrame;)Landroid/graphics/RectF;

    move-result-object v6

    invoke-virtual {v6}, Landroid/graphics/RectF;->height()F

    move-result v6

    div-float/2addr v5, v6

    const/high16 v6, 0x44fa0000    # 2000.0f

    mul-float/2addr v5, v6

    float-to-int v5, v5

    if-ge v5, v2, :cond_f

    :goto_4
    if-gt v2, v1, :cond_9

    :cond_e
    move v1, v2

    goto :goto_3

    :cond_f
    move v2, v5

    goto :goto_4

    :cond_10
    move v2, v5

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
