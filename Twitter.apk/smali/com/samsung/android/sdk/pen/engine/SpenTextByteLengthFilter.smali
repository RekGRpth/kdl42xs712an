.class Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/text/InputFilter;


# instance fields
.field private mByte:I

.field private mContext:Landroid/content/Context;

.field private mToastMessage:Landroid/widget/Toast;


# direct methods
.method public constructor <init>(Landroid/content/Context;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    return-void
.end method

.method private getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string/jumbo v2, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;

    move-result-object v1

    const-string/jumbo v2, "string"

    const-string/jumbo v3, "com.samsung.android.sdk.spen30"

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    return-void
.end method

.method public filter(Ljava/lang/CharSequence;IILandroid/text/Spanned;II)Ljava/lang/CharSequence;
    .locals 4

    const/4 v3, 0x0

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    invoke-interface {p1, p2, p3}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    invoke-interface {p4}, Landroid/text/Spanned;->length()I

    move-result v1

    sub-int v2, p6, p5

    sub-int/2addr v1, v2

    sub-int/2addr v0, v1

    if-gtz v0, :cond_3

    const-string/jumbo v0, "string_reached_maximum_input"

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->getMultiLanguage(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mContext:Landroid/content/Context;

    invoke-static {v1, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    const/16 v1, 0x50

    const/16 v2, 0x96

    invoke-virtual {v0, v1, v3, v2}, Landroid/widget/Toast;->setGravity(III)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    :cond_1
    const-string/jumbo v0, ""

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    invoke-virtual {v1, v0}, Landroid/widget/Toast;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mToastMessage:Landroid/widget/Toast;

    invoke-virtual {v0, v3}, Landroid/widget/Toast;->setDuration(I)V

    goto :goto_1

    :cond_3
    sub-int v1, p3, p2

    if-lt v0, v1, :cond_4

    const/4 v0, 0x0

    goto :goto_0

    :cond_4
    add-int/2addr v0, p2

    invoke-interface {p1, p2, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    goto :goto_0
.end method

.method public getLimitLength()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    return v0
.end method

.method public setLimitLength(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenTextByteLengthFilter;->mByte:I

    return-void
.end method
