.class Lcom/samsung/android/sdk/pen/engine/SpenInView$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnHoverListener;


# instance fields
.field public mHoverIconID:I

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenInView;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->mHoverIconID:I

    return-void
.end method


# virtual methods
.method public onHover(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0x9

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mHoverDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->setCustomHoveringIcon(Landroid/graphics/drawable/Drawable;)I
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$1(Lcom/samsung/android/sdk/pen/engine/SpenInView;Landroid/graphics/drawable/Drawable;)I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->mHoverIconID:I

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mPageDoc:Lcom/samsung/android/sdk/pen/document/SpenPageDoc;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$3(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/document/SpenPageDoc;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/document/SpenPageDoc;->isObjectContained(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenInView;->mControl:Lcom/samsung/android/sdk/pen/engine/SpenControlBase;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenInView;)Lcom/samsung/android/sdk/pen/engine/SpenControlBase;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase;->onCanvasHoverEnter()V

    :cond_1
    :goto_0
    invoke-virtual {p1, p2}, Landroid/view/View;->onHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0

    :cond_2
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenInView;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenInView$1;->mHoverIconID:I

    # invokes: Lcom/samsung/android/sdk/pen/engine/SpenInView;->removeHoveringIcon(I)Z
    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenInView;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenInView;I)Z

    goto :goto_0
.end method
