.class Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/GestureDetector$OnGestureListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;


# direct methods
.method private constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)V

    return-void
.end method


# virtual methods
.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0

    return-void
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 6

    const/4 v5, 0x2

    const/4 v4, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mReceiveActionDown:Z
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$4(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getObjectList()Ljava/util/ArrayList;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getRect()Landroid/graphics/RectF;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/RectF;->contains(FF)Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isRotatable()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectTextBox;->isReadOnlyEnabled()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v0

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->bringToFront()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->enableTextInput(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mTextBox:Lcom/samsung/android/sdk/pen/engine/SpenTextBox;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->access$5(Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;)Lcom/samsung/android/sdk/pen/engine/SpenTextBox;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenTextBox;->fit(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, v4}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setEditable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0, v5}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->setStyle(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->hide()V

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->getStyle()I

    move-result v0

    if-eq v0, v5, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->mInstance:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->updateContextMenu()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox$GestureListener;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/engine/SpenControlTextBox;->mContextMenu:Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/engine/SpenControlBase$ContextMenuMgr;->show()V

    goto :goto_0
.end method
