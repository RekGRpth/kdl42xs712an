.class Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected mDirtyFlag:Z

.field protected mFlag:Z

.field private final mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

.field final synthetic this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenControlList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;-><init>(Lcom/samsung/android/sdk/pen/engine/SpenControlList;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->reset()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;)Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mObject:Lcom/samsung/android/sdk/pen/engine/SpenControlList$ObjectGroup;

    return-object v0
.end method


# virtual methods
.method reset()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mDirtyFlag:Z

    return-void
.end method

.method set(Z)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    if-ne p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mFlag:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenControlList$Group;->mDirtyFlag:Z

    goto :goto_0
.end method
