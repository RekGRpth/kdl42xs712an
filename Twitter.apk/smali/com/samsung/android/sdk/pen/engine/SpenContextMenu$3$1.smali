.class Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

.field private final synthetic val$lvWidthAct:F

.field private final synthetic val$step:F


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;FF)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    iput p2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$lvWidthAct:F

    iput p3, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$step:F

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$lvWidthAct:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F

    move-result v1

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->val$step:F

    mul-float/2addr v1, v2

    sub-float/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mCount:F
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$18(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)F

    move-result v2

    const/high16 v3, 0x3f800000    # 1.0f

    add-float/2addr v2, v3

    invoke-static {v1, v2}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$19(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;F)V

    const/4 v1, 0x0

    cmpl-float v1, v0, v1

    if-lez v1, :cond_1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v1

    float-to-int v0, v0

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->scrollTo(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mHorizontalListView:Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$2(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenHorizontalListView;->scrollTo(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$20(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3$1;->this$1:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;

    # getter for: Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->this$0:Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;->access$0(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu$3;)Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;->access$21(Lcom/samsung/android/sdk/pen/engine/SpenContextMenu;Ljava/util/Timer;)V

    goto :goto_0
.end method
