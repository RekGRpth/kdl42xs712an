.class public interface abstract Lcom/samsung/android/sdk/pen/engine/SpenControlListener;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final PRESS_TYPE_LONG_PRESS:I = 0x2

.field public static final PRESS_TYPE_NONE:I = 0x0

.field public static final PRESS_TYPE_TAP:I = 0x1


# virtual methods
.method public abstract onClosed(Ljava/util/ArrayList;)Z
.end method

.method public abstract onCreated(Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;Ljava/util/ArrayList;ILandroid/graphics/PointF;)Z
.end method

.method public abstract onMenuSelected(Ljava/util/ArrayList;I)Z
.end method

.method public abstract onObjectChanged(Ljava/util/ArrayList;)V
.end method

.method public abstract onRectChanged(Landroid/graphics/RectF;Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
.end method

.method public abstract onRotationChanged(FLcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
.end method
