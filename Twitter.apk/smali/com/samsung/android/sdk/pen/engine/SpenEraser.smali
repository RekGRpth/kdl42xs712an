.class Lcom/samsung/android/sdk/pen/engine/SpenEraser;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final TOUCH_TOLERANCE_ERASER:F = 1.0f


# instance fields
.field private mCanvas:Landroid/graphics/Canvas;

.field private mPaint:Landroid/graphics/Paint;

.field private mQuadLastX:F

.field private mQuadLastY:F

.field private mQuadStartX:F

.field private mQuadStartY:F

.field private mSize:F

.field private mUnitPath:Landroid/graphics/Path;

.field private mX:F

.field private mY:F


# direct methods
.method public constructor <init>()V
    .locals 3

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Join;->ROUND:Landroid/graphics/Paint$Join;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeJoin(Landroid/graphics/Paint$Join;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Cap;->ROUND:Landroid/graphics/Paint$Cap;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeCap(Landroid/graphics/Paint$Cap;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    new-instance v1, Landroid/graphics/PorterDuffXfermode;

    sget-object v2, Landroid/graphics/PorterDuff$Mode;->CLEAR:Landroid/graphics/PorterDuff$Mode;

    invoke-direct {v1, v2}, Landroid/graphics/PorterDuffXfermode;-><init>(Landroid/graphics/PorterDuff$Mode;)V

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setXfermode(Landroid/graphics/Xfermode;)Landroid/graphics/Xfermode;

    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/graphics/Path;->incReserve(I)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    return-void
.end method

.method public endPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 5

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->getSize()F

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {v1}, Landroid/graphics/Path;->rewind()V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->moveTo(FF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Path;->lineTo(FF)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    const/4 v2, 0x0

    invoke-virtual {v1, p2, v2}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    const/high16 v1, 0x40000000    # 2.0f

    div-float/2addr v0, v1

    const/high16 v1, 0x3f800000    # 1.0f

    add-float/2addr v0, v1

    iget v1, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    iget v2, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v0

    iget v3, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v0

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v4

    invoke-virtual {p2, v1, v2, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    return-void
.end method

.method public getSize()F
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    return v0
.end method

.method public movePen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 13

    const/4 v1, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v11, 0x40000000    # 2.0f

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    sub-float/2addr v0, v2

    invoke-static {v0}, Ljava/lang/Math;->abs(F)F

    move-result v0

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    iget v3, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    sub-float/2addr v2, v3

    invoke-static {v2}, Ljava/lang/Math;->abs(F)F

    move-result v2

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->getSize()F

    move-result v3

    cmpg-float v0, v0, v12

    if-gez v0, :cond_0

    cmpg-float v0, v2, v12

    if-gez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {v0}, Landroid/graphics/Path;->rewind()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Path;->moveTo(FF)V

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getHistorySize()I

    move-result v2

    move v0, v1

    :goto_1
    if-lt v0, v2, :cond_1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    add-float/2addr v0, v2

    div-float/2addr v0, v11

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    add-float/2addr v0, v2

    div-float/2addr v0, v11

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iget v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iget v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    invoke-virtual {v0, v2, v4, v5, v6}, Landroid/graphics/Path;->quadTo(FFFF)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, v2, v4}, Landroid/graphics/Canvas;->drawPath(Landroid/graphics/Path;Landroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    invoke-virtual {v0, p2, v1}, Landroid/graphics/Path;->computeBounds(Landroid/graphics/RectF;Z)V

    div-float v0, v3, v11

    add-float/2addr v0, v12

    iget v1, p2, Landroid/graphics/RectF;->left:F

    sub-float/2addr v1, v0

    iget v2, p2, Landroid/graphics/RectF;->top:F

    sub-float/2addr v2, v0

    iget v3, p2, Landroid/graphics/RectF;->right:F

    add-float/2addr v3, v0

    iget v4, p2, Landroid/graphics/RectF;->bottom:F

    add-float/2addr v0, v4

    invoke-virtual {p2, v1, v2, v3, v0}, Landroid/graphics/RectF;->set(FFFF)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    goto :goto_0

    :cond_1
    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalX(I)F

    move-result v4

    invoke-virtual {p1, v0}, Landroid/view/MotionEvent;->getHistoricalY(I)F

    move-result v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    add-float/2addr v6, v4

    div-float/2addr v6, v11

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iget v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    add-float/2addr v6, v5

    div-float/2addr v6, v11

    iput v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mUnitPath:Landroid/graphics/Path;

    iget v7, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iget v8, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iget v9, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iget v10, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    invoke-virtual {v6, v7, v8, v9, v10}, Landroid/graphics/Path;->quadTo(FFFF)V

    iput v4, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iput v5, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 1

    new-instance v0, Landroid/graphics/Canvas;

    invoke-direct {v0, p1}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mCanvas:Landroid/graphics/Canvas;

    return-void
.end method

.method public setSize(F)V
    .locals 2

    iput p1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mPaint:Landroid/graphics/Paint;

    iget v1, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mSize:F

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    :cond_0
    return-void
.end method

.method public startPen(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V
    .locals 1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadStartY:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mX:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastX:F

    iget v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mY:F

    iput v0, p0, Lcom/samsung/android/sdk/pen/engine/SpenEraser;->mQuadLastY:F

    return-void
.end method
