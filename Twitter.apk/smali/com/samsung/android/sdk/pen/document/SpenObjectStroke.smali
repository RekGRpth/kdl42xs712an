.class public final Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;
.super Lcom/samsung/android/sdk/pen/document/SpenObjectBase;
.source "Twttr"


# static fields
.field public static final TOOL_TYPE_ERASER:I = 0x4

.field public static final TOOL_TYPE_FINGER:I = 0x1

.field public static final TOOL_TYPE_MOUSE:I = 0x3

.field public static final TOOL_TYPE_SPEN:I = 0x2

.field public static final TOOL_TYPE_UNKNOWN:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init1(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Z)V
    .locals 6

    const/4 v2, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    move-object v0, p0

    move-object v1, p1

    move-object v3, v2

    move-object v4, v2

    move v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init3(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    invoke-direct/range {p0 .. p5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 6

    const/4 v1, 0x0

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;-><init>(I)V

    move-object v0, p0

    move-object v2, v1

    move-object v3, v1

    move-object v4, v1

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method private native ObjectStroke_addPoint4(Landroid/graphics/PointF;FI)Z
.end method

.method private native ObjectStroke_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z
.end method

.method private native ObjectStroke_enableCurve(Z)Z
.end method

.method private native ObjectStroke_getAdvancedPenSetting()Ljava/lang/String;
.end method

.method private native ObjectStroke_getColor()I
.end method

.method private native ObjectStroke_getInputType()I
.end method

.method private native ObjectStroke_getPenName()Ljava/lang/String;
.end method

.method private native ObjectStroke_getPenSize()F
.end method

.method private native ObjectStroke_getPoints()[Landroid/graphics/PointF;
.end method

.method private native ObjectStroke_getPressures()[F
.end method

.method private native ObjectStroke_getTimeStamps()[I
.end method

.method private native ObjectStroke_getToolType()I
.end method

.method private native ObjectStroke_init1(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_init3(Ljava/lang/String;[Landroid/graphics/PointF;[F[I)Z
.end method

.method private native ObjectStroke_init4(Ljava/lang/String;[Landroid/graphics/PointF;[F[IZ)Z
.end method

.method private native ObjectStroke_isCurvable()Z
.end method

.method private native ObjectStroke_move(FF)Z
.end method

.method private native ObjectStroke_resize(FF)Z
.end method

.method private native ObjectStroke_setAdvancedPenSetting(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_setColor(I)Z
.end method

.method private native ObjectStroke_setInputType(I)Z
.end method

.method private native ObjectStroke_setPenName(Ljava/lang/String;)Z
.end method

.method private native ObjectStroke_setPenSize(F)Z
.end method

.method private native ObjectStroke_setPoints([Landroid/graphics/PointF;[F[I)Z
.end method

.method private native ObjectStroke_setRotation(F)Z
.end method

.method private native ObjectStroke_setToolType(I)Z
.end method

.method private throwUncheckedException(I)V
    .locals 3

    const/16 v0, 0x13

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string/jumbo v2, "SpenObjectStroke("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ") is already closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/document/SpenAlreadyClosedException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    return-void
.end method


# virtual methods
.method public addPoint(Landroid/graphics/PointF;FI)V
    .locals 1

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_addPoint4(Landroid/graphics/PointF;FI)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_copy(Lcom/samsung/android/sdk/pen/document/SpenObjectBase;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public getAdvancedPenSetting()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getAdvancedPenSetting()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getColor()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getColor()I

    move-result v0

    return v0
.end method

.method public getPenName()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPenName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPenSize()F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPenSize()F

    move-result v0

    return v0
.end method

.method public getPoints()[Landroid/graphics/PointF;
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPoints()[Landroid/graphics/PointF;

    move-result-object v0

    return-object v0
.end method

.method public getPressures()[F
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getPressures()[F

    move-result-object v0

    return-object v0
.end method

.method public getTimeStamps()[I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getTimeStamps()[I

    move-result-object v0

    return-object v0
.end method

.method public getToolType()I
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_getToolType()I

    move-result v0

    return v0
.end method

.method public isCurveEnabled()Z
    .locals 1

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_isCurvable()Z

    move-result v0

    return v0
.end method

.method public setAdvancedPenSetting(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setAdvancedPenSetting(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setColor(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setColor(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setCurveEnabled(Z)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_enableCurve(Z)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setPenName(Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPenName(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setPenSize(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPenSize(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setPoints([Landroid/graphics/PointF;[F[I)V
    .locals 2

    if-nez p1, :cond_1

    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPoints([Landroid/graphics/PointF;[F[I)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void

    :cond_1
    array-length v0, p1

    array-length v1, p2

    if-ne v0, v1, :cond_2

    array-length v0, p1

    array-length v1, p3

    if-eq v0, v1, :cond_3

    :cond_2
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/samsung/android/sdk/pen/util/SpenError;->ThrowUncheckedException(I)V

    :cond_3
    invoke-direct {p0, p1, p2, p3}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setPoints([Landroid/graphics/PointF;[F[I)Z

    move-result v0

    goto :goto_0
.end method

.method public setRect(Landroid/graphics/RectF;Z)V
    .locals 0

    invoke-super {p0, p1, p2}, Lcom/samsung/android/sdk/pen/document/SpenObjectBase;->setRect(Landroid/graphics/RectF;Z)V

    return-void
.end method

.method public setRotation(F)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setRotation(F)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method

.method public setToolType(I)V
    .locals 1

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->ObjectStroke_setToolType(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenError;->getError()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/samsung/android/sdk/pen/document/SpenObjectStroke;->throwUncheckedException(I)V

    :cond_0
    return-void
.end method
