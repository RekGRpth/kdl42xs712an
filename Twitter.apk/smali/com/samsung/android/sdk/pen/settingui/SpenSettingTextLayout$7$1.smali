.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown$NameDropdownSelectListner;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 6

    const/high16 v5, 0x42040000    # 33.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$10(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->setPreviewTypeface(Landroid/graphics/Typeface;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    invoke-static {}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontList()Ljava/util/List;

    move-result-object v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v2

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v1

    invoke-static {v1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getFontName(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->font:Ljava/lang/String;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_0
    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v1

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    sub-int/2addr v3, v4

    if-lt v0, v3, :cond_2

    :goto_0
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v3

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v3

    invoke-virtual {v3}, Landroid/widget/Button;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v4

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    sub-int/2addr v3, v4

    if-gt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v0

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v2, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "..."

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    :goto_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v0

    invoke-static {p1}, Lcom/samsung/android/sdk/pen/util/SpenFont;->getTypeFace(I)Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    return-void

    :cond_1
    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v1, v0, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v1}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v3

    const-string/jumbo v4, "..."

    invoke-virtual {v2, v4}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    add-float/2addr v3, v4

    invoke-virtual {v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mFontTypeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$12(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout$7;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->mCurrentFontName:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;->access$11(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
