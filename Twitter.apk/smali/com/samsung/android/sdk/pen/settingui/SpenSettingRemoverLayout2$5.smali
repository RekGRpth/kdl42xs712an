.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0, v2}, Landroid/widget/SeekBar;->incrementProgressBy(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v0}, Landroid/widget/SeekBar;->getProgress()I

    move-result v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserSizeSeekBar:Landroid/widget/SeekBar;

    invoke-virtual {v1}, Landroid/widget/SeekBar;->getMax()I

    move-result v1

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/high16 v1, 0x41200000    # 10.0f

    cmpg-float v0, v0, v1

    if-gez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v0, v0, v1

    iget v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    const/high16 v2, 0x3f800000    # 1.0f

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCanvasSize:I

    int-to-float v3, v3

    const/high16 v4, 0x44870000    # 1080.0f

    div-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCutterDataList:[Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mCurrentCutterType:I

    aget-object v1, v1, v2

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;->size:F

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$5;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->mEraserPlusButton:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setEnabled(Z)V

    goto :goto_0
.end method
