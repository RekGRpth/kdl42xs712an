.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnTouchListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 9

    const/4 v8, 0x1

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureDetector:Landroid/view/GestureDetector;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p2}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestDisallowInterceptTouchEvent(Z)V

    :goto_0
    return v8

    :cond_0
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawX()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getRawY()F

    move-result v2

    invoke-static {v2}, Ljava/lang/Math;->round(F)I

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    :cond_1
    :goto_1
    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->requestDisallowInterceptTouchEvent(Z)V

    goto :goto_0

    :pswitch_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMoveSettingLayout:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mXDelta:I

    sub-int v2, v0, v2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mYDelta:I

    sub-int v0, v3, v0

    if-gez v2, :cond_2

    move v2, v1

    :cond_2
    if-gez v0, :cond_3

    move v0, v1

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v3}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getWidth()I

    move-result v4

    sub-int/2addr v3, v4

    add-int/lit8 v3, v3, -0x2

    if-le v2, v3, :cond_4

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCanvasLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v2}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getWidth()I

    move-result v3

    sub-int/2addr v2, v3

    add-int/lit8 v2, v2, -0x2

    :cond_4
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v3, v3, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x43230000    # 163.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v4}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getHeight()I

    move-result v4

    const-string/jumbo v5, "settingui-settingPen"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "height = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v5, "settingui-settingPen"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string/jumbo v7, "before mMoveY = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-gt v4, v3, :cond_7

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v5, v3

    if-le v0, v5, :cond_7

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sub-int/2addr v0, v3

    :cond_5
    :goto_2
    const-string/jumbo v3, "settingui-settingPen"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string/jumbo v5, "after mMoveY = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    if-gez v2, :cond_6

    move v2, v1

    :cond_6
    if-gez v0, :cond_8

    :goto_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput v1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget v2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mCurrentTopMargin:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_1

    :cond_7
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int v4, v5, v4

    if-le v0, v4, :cond_5

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v5, v0

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v6, v6, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42640000    # 57.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    sub-int/2addr v5, v6

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->setExpandBarPosition(I)V
    invoke-static {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->access$6(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;I)V

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v4, v4, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v4}, Landroid/graphics/Rect;->height()I

    move-result v4

    sub-int/2addr v4, v3

    if-le v0, v4, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMovableRect:Landroid/graphics/Rect;

    invoke-virtual {v0}, Landroid/graphics/Rect;->height()I

    move-result v0

    sub-int/2addr v0, v3

    goto :goto_2

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mMoveSettingLayout:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$33;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout;->mActionListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout$ActionListener;->onMoved()V

    goto/16 :goto_1

    :cond_8
    move v1, v0

    goto :goto_3

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
