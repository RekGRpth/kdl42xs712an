.class Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static final TAG:Ljava/lang/String; = "settingui-colorPicker"


# instance fields
.field private drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field mColorPickerColorImage:Landroid/view/View;

.field mColorPickerCurrentColor:Landroid/view/View;

.field mColorPickerHandle:Landroid/view/View;

.field mColorPickerdExitBtn:Landroid/view/View;

.field private mCurrentColor:I

.field private final mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

.field mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

.field private mParentRelativeLayout:Landroid/widget/RelativeLayout;

.field mSpoidExitListener:Landroid/view/View$OnTouchListener;

.field mSpoidSettingListener:Landroid/view/View$OnTouchListener;

.field mSpuitSettings:Landroid/view/View;

.field mSpuitdBG:Landroid/view/View;

.field private final mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

.field private mXDelta:I

.field private mYDelta:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/widget/RelativeLayout;FII)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    const/4 v0, 0x0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$2;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidSettingListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$3;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2$3;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidExitListener:Landroid/view/View$OnTouchListener;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    invoke-direct {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, ""

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-direct {p0, p4, p5}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->initSpuitSetting(II)V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->checkPosition()V

    return-void
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mXDelta:I

    return-void
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mYDelta:I

    return-void
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mXDelta:I

    return v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mYDelta:I

    return v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method

.method static synthetic access$6(Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;)Landroid/widget/RelativeLayout;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    return-object v0
.end method

.method private bodyLayout()Landroid/view/ViewGroup;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitdBg()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitdHandle()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitExitBtn()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitColorImage()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->spuitCurrentColor()Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method

.method private checkPosition()V
    .locals 14

    const/4 v0, 0x2

    const/high16 v13, 0x40e00000    # 7.0f

    const/4 v12, 0x1

    const/4 v11, 0x0

    new-array v5, v0, [I

    new-array v0, v0, [I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42d40000    # 106.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x42180000    # 38.0f

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->getLocationOnScreen([I)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->getLocationOnScreen([I)V

    new-instance v8, Landroid/graphics/Rect;

    aget v1, v0, v11

    aget v2, v0, v12

    aget v3, v0, v11

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v4

    add-int/2addr v3, v4

    aget v0, v0, v12

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v4}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v4

    add-int/2addr v0, v4

    invoke-direct {v8, v1, v2, v3, v0}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    aget v9, v5, v11

    iget v10, v8, Landroid/graphics/Rect;->left:I

    if-ge v9, v10, :cond_0

    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_0
    aget v9, v5, v12

    iget v10, v8, Landroid/graphics/Rect;->top:I

    if-ge v9, v10, :cond_1

    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_1
    iget v9, v8, Landroid/graphics/Rect;->right:I

    aget v10, v5, v11

    sub-int/2addr v9, v10

    if-ge v9, v6, :cond_2

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v9

    sub-int v6, v9, v6

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v6, :cond_2

    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_2
    iget v6, v8, Landroid/graphics/Rect;->bottom:I

    aget v5, v5, v12

    sub-int v5, v6, v5

    if-ge v5, v7, :cond_3

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v5

    sub-int/2addr v5, v7

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v5, :cond_3

    iput v11, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_3
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41c80000    # 25.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42480000    # 50.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v13}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x429c0000    # 78.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method

.method private initSpuitSetting(II)V
    .locals 5

    const/16 v4, 0x8

    const/4 v3, -0x2

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->totalLayout()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidSettingListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    invoke-direct {v0, v3, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v2, v2, v2, v2}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    invoke-virtual {v0, v1}, Landroid/widget/RelativeLayout;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->movePosition(II)V

    invoke-virtual {p0, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setVisibility(I)V

    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private setCanvasSize()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->invalidate()V

    :cond_0
    return-void
.end method

.method private spuitColorImage()Landroid/view/View;
    .locals 7

    const/16 v5, 0x16

    const/4 v6, 0x0

    const/high16 v4, 0x41b00000    # 22.0f

    new-instance v1, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v0, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41c80000    # 25.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40e00000    # 7.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v0, v2, v3, v6, v6}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_icon_spoid_hover"

    const-string/jumbo v3, "snote_color_spoid_press"

    const-string/jumbo v4, "snote_color_spoid_focus"

    move v6, v5

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    return-object v1
.end method

.method private spuitCurrentColor()Landroid/view/View;
    .locals 6

    const/4 v5, 0x0

    const/high16 v4, 0x41b00000    # 22.0f

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42480000    # 50.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40e00000    # 7.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-virtual {v1, v2, v3, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v3, "snote_colorchip_shadow"

    invoke-virtual {v2, v0, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    new-instance v2, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    const/high16 v1, -0x10000

    invoke-virtual {v2, v1}, Landroid/widget/LinearLayout;->setBackgroundColor(I)V

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v2
.end method

.method private spuitExitBtn()Landroid/view/View;
    .locals 13

    const/16 v7, 0x12

    const/high16 v12, 0x41b00000    # 22.0f

    const/4 v11, 0x1

    const/4 v10, 0x0

    new-instance v1, Landroid/widget/RelativeLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;)V

    new-instance v9, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x41e00000    # 28.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42180000    # 38.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-direct {v9, v0, v2}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_bg_right_normal"

    const-string/jumbo v3, "snote_toolbar_bg_right_selectedl"

    const-string/jumbo v4, "snote_toolbar_bg_right_focus"

    const/16 v5, 0x1c

    const/16 v6, 0x26

    invoke-virtual/range {v0 .. v6}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v3, Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v3, v0}, Landroid/widget/ImageButton;-><init>(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v4, "snote_popup_icon_close"

    const-string/jumbo v5, "snote_popup_icon_close"

    const-string/jumbo v6, "snote_popup_icon_close"

    move v8, v7

    invoke-virtual/range {v2 .. v8}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewBackgroundSelectableImageLoad(Landroid/view/View;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    new-instance v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v2, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v4, v12}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-direct {v0, v2, v4}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x40000000    # 2.0f

    invoke-virtual {v2, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v5, 0x40e00000    # 7.0f

    invoke-virtual {v4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v4

    invoke-virtual {v0, v2, v4, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v3, v11}, Landroid/widget/ImageButton;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpoidExitListener:Landroid/view/View$OnTouchListener;

    invoke-virtual {v3, v0}, Landroid/widget/ImageButton;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    iput-boolean v11, v9, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v2, 0x429c0000    # 78.0f

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v0

    invoke-virtual {v9, v0, v10, v10, v10}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v1, v9}, Landroid/widget/RelativeLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {v1, v11}, Landroid/widget/RelativeLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mStringUtil:Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;

    const-string/jumbo v2, "string_close"

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenTextUtil;->setString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/RelativeLayout;->setContentDescription(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v3}, Landroid/widget/RelativeLayout;->addView(Landroid/view/View;)V

    return-object v1
.end method

.method private spuitdBg()Landroid/view/View;
    .locals 7

    const/4 v6, 0x1

    const/4 v5, 0x0

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x42600000    # 56.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    iput-boolean v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    invoke-virtual {v1, v2, v5, v5, v5}, Landroid/widget/RelativeLayout$LayoutParams;->setMargins(IIII)V

    invoke-virtual {v0, v5}, Landroid/widget/ImageView;->setFocusable(Z)V

    invoke-virtual {v0, v6}, Landroid/widget/ImageView;->setClickable(Z)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_bg_center2_normal"

    invoke-virtual {v1, v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;)V

    return-object v0
.end method

.method private spuitdHandle()Landroid/view/View;
    .locals 5

    new-instance v0, Landroid/widget/ImageView;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v3, 0x41b00000    # 22.0f

    invoke-virtual {v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v2

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v4, 0x42180000    # 38.0f

    invoke-virtual {v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v3

    invoke-direct {v1, v2, v3}, Landroid/widget/RelativeLayout$LayoutParams;-><init>(II)V

    const/4 v2, 0x1

    iput-boolean v2, v1, Landroid/widget/RelativeLayout$LayoutParams;->alignWithParent:Z

    const/16 v2, 0x9

    invoke-virtual {v1, v2}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    const-string/jumbo v2, "snote_toolbar_handle"

    const/16 v3, 0x16

    const/16 v4, 0x26

    invoke-virtual {v1, v0, v2, v3, v4}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->addViewSetBackgroundImageLoad(Landroid/view/View;Ljava/lang/String;II)V

    return-object v0
.end method

.method private totalLayout()Landroid/view/View;
    .locals 3

    const/4 v2, -0x2

    new-instance v0, Landroid/widget/LinearLayout;

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/LinearLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->bodyLayout()Landroid/view/ViewGroup;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->addView(Landroid/view/View;)V

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 3

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->unbindDrawables(Landroid/view/View;)V

    iput-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-void
.end method

.method public getColorPickerCurrentColor()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    return v0
.end method

.method public getColorPickerSettingVisible()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    return v0
.end method

.method public hide()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method movePosition(II)V
    .locals 10

    const/4 v6, 0x0

    const/high16 v9, 0x40e00000    # 7.0f

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    iput p1, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iput p2, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    if-gez v5, :cond_1

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_1
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    if-gez v5, :cond_2

    iput v6, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_2
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v6, 0x42d40000    # 106.0f

    invoke-virtual {v5, v6}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v5

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42180000    # 38.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    if-lez v7, :cond_3

    iget v7, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v8}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v8

    sub-int/2addr v8, v5

    if-le v7, v8, :cond_3

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getWidth()I

    move-result v7

    sub-int v5, v7, v5

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    :cond_3
    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    if-lez v5, :cond_4

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v7}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v7

    sub-int/2addr v7, v6

    if-le v5, v7, :cond_4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v5}, Landroid/widget/RelativeLayout;->getHeight()I

    move-result v5

    sub-int/2addr v5, v6

    iput v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    :cond_4
    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41b00000    # 22.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v5, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x41c80000    # 25.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x42480000    # 50.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-virtual {v6, v9}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget-object v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v7, 0x429c0000    # 78.0f

    invoke-virtual {v6, v7}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v6

    add-int/2addr v5, v6

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v5, v0, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v5, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v5, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v0, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method protected resetPositionWhenRotateSpuitOut(II)V
    .locals 17

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v5}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/RelativeLayout$LayoutParams;

    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7}, Landroid/graphics/Rect;-><init>()V

    new-instance v8, Landroid/graphics/Rect;

    invoke-direct {v8}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v6, v7}, Landroid/widget/RelativeLayout;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual {v7}, Landroid/graphics/Rect;->width()I

    move-result v6

    invoke-virtual {v7}, Landroid/graphics/Rect;->height()I

    move-result v9

    if-le v6, v9, :cond_3

    move/from16 v0, p1

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v0, p2

    iput v0, v7, Landroid/graphics/Rect;->right:I

    :goto_1
    iget v6, v7, Landroid/graphics/Rect;->left:I

    iput v6, v8, Landroid/graphics/Rect;->left:I

    iget v6, v7, Landroid/graphics/Rect;->top:I

    iput v6, v8, Landroid/graphics/Rect;->top:I

    iget v6, v7, Landroid/graphics/Rect;->bottom:I

    iput v6, v8, Landroid/graphics/Rect;->right:I

    iget v6, v7, Landroid/graphics/Rect;->right:I

    iput v6, v8, Landroid/graphics/Rect;->bottom:I

    new-instance v6, Landroid/graphics/Rect;

    invoke-direct {v6}, Landroid/graphics/Rect;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mParentRelativeLayout:Landroid/widget/RelativeLayout;

    invoke-virtual {v9}, Landroid/widget/RelativeLayout;->getRootView()Landroid/view/View;

    move-result-object v9

    invoke-virtual {v9, v6}, Landroid/view/View;->getGlobalVisibleRect(Landroid/graphics/Rect;)Z

    invoke-virtual/range {p0 .. p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->getContext()Landroid/content/Context;

    move-result-object v6

    const-string/jumbo v9, "window"

    invoke-virtual {v6, v9}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/view/WindowManager;

    invoke-interface {v6}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v6

    new-instance v9, Landroid/graphics/Point;

    invoke-direct {v9}, Landroid/graphics/Point;-><init>()V

    invoke-virtual {v6, v9}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    const-string/jumbo v6, "settingui-colorPicker"

    const-string/jumbo v9, "==== colorPicker ===="

    invoke-static {v6, v9}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v6, "settingui-colorPicker"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "old  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v7, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v7, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v6, "settingui-colorPicker"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string/jumbo v10, "new  = "

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v8, Landroid/graphics/Rect;->left:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->top:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->right:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string/jumbo v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, v8, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    iget v6, v7, Landroid/graphics/Rect;->left:I

    iget v10, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v6, v10

    iput v6, v9, Landroid/graphics/Rect;->left:I

    iget v6, v7, Landroid/graphics/Rect;->top:I

    iget v10, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    add-int/2addr v6, v10

    iput v6, v9, Landroid/graphics/Rect;->top:I

    iget v6, v9, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x42d40000    # 106.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    add-int/2addr v6, v10

    iput v6, v9, Landroid/graphics/Rect;->right:I

    iget v6, v9, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v11, 0x42180000    # 38.0f

    invoke-virtual {v10, v11}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v10

    add-int/2addr v6, v10

    iput v6, v9, Landroid/graphics/Rect;->bottom:I

    const-string/jumbo v6, "settingui-colorPicker"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string/jumbo v11, "view = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v9, Landroid/graphics/Rect;->left:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v9, Landroid/graphics/Rect;->top:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v9, Landroid/graphics/Rect;->right:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, v9, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v6, v10}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, v9, Landroid/graphics/Rect;->left:I

    iget v10, v7, Landroid/graphics/Rect;->left:I

    sub-int/2addr v6, v10

    int-to-float v10, v6

    iget v6, v7, Landroid/graphics/Rect;->right:I

    iget v11, v9, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v11

    int-to-float v11, v6

    iget v6, v9, Landroid/graphics/Rect;->top:I

    iget v12, v7, Landroid/graphics/Rect;->top:I

    sub-int/2addr v6, v12

    int-to-float v12, v6

    iget v6, v7, Landroid/graphics/Rect;->bottom:I

    iget v7, v9, Landroid/graphics/Rect;->bottom:I

    sub-int/2addr v6, v7

    int-to-float v13, v6

    add-float v6, v10, v11

    div-float v7, v10, v6

    add-float v6, v12, v13

    div-float v6, v12, v6

    const-string/jumbo v14, "settingui-colorPicker"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string/jumbo v16, "left :"

    invoke-direct/range {v15 .. v16}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v15, v10}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string/jumbo v15, ", right :"

    invoke-virtual {v10, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v14, v10}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v10, "settingui-colorPicker"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v14, "top :"

    invoke-direct {v11, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", bottom :"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v13}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string/jumbo v10, "settingui-colorPicker"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string/jumbo v12, "hRatio = "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v7}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string/jumbo v12, ", vRatio = "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-static {v10, v11}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    float-to-double v10, v7

    const-wide v12, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v10, v10, v12

    if-lez v10, :cond_4

    const/high16 v7, 0x3f800000    # 1.0f

    :cond_1
    :goto_2
    float-to-double v10, v6

    const-wide v12, 0x3fefae147ae147aeL    # 0.99

    cmpl-double v10, v10, v12

    if-lez v10, :cond_5

    const/high16 v6, 0x3f800000    # 1.0f

    :cond_2
    :goto_3
    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v10

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v11

    sub-int/2addr v10, v11

    int-to-float v10, v10

    mul-float/2addr v7, v10

    invoke-static {v7}, Ljava/lang/Math;->round(F)I

    move-result v7

    iput v7, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v8}, Landroid/graphics/Rect;->height()I

    move-result v7

    invoke-virtual {v9}, Landroid/graphics/Rect;->height()I

    move-result v8

    sub-int/2addr v7, v8

    int-to-float v7, v7

    mul-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->round(F)I

    move-result v6

    iput v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    const-string/jumbo v6, "settingui-colorPicker"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string/jumbo v8, "lMargin = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ", tMargin = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41b00000    # 22.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v6, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x41c80000    # 25.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40e00000    # 7.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v3, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x42480000    # 50.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x40e00000    # 7.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v5, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->drawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const/high16 v8, 0x429c0000    # 78.0f

    invoke-virtual {v7, v8}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->getIntValueAppliedDensity(F)I

    move-result v7

    add-int/2addr v6, v7

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    iget v6, v1, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerHandle:Landroid/view/View;

    invoke-virtual {v6, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitdBG:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerColorImage:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    invoke-virtual {v1, v5}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerdExitBtn:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0

    :cond_3
    move/from16 v0, p2

    iput v0, v7, Landroid/graphics/Rect;->bottom:I

    move/from16 v0, p1

    iput v0, v7, Landroid/graphics/Rect;->right:I

    goto/16 :goto_1

    :cond_4
    const/4 v10, 0x0

    cmpg-float v10, v7, v10

    if-gez v10, :cond_1

    const/4 v7, 0x0

    goto/16 :goto_2

    :cond_5
    const/4 v10, 0x0

    cmpg-float v10, v6, v10

    if-gez v10, :cond_2

    const/4 v6, 0x0

    goto/16 :goto_3
.end method

.method public setColorPickerColor(I)V
    .locals 2

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mCurrentColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mColorPickerCurrentColor:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    return-void
.end method

.method public show()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->mLoaded:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mImageLoader:Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenImageLoader;->loadImage()V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->bringToFront()V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->setCanvasSize()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->checkPosition()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorPickerLayout2;->mSpuitSettings:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->invalidate()V

    return-void
.end method
