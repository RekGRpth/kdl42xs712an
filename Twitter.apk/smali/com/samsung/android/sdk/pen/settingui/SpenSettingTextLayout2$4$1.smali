.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/samsung/android/sdk/pen/settingui/SPenFontSizeDropdown2$SizeDropdownSelectListner;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSelectItem(I)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getCanvasWidth()I

    move-result v1

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    :goto_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    int-to-double v0, v0

    const-wide v2, 0x4076800000000000L    # 360.0

    div-double/2addr v0, v2

    double-to-float v1, v0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-float v0, v0

    mul-float/2addr v0, v1

    iput v0, v2, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->setPreviewTextSize(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextFontSizeList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mFontSizeButton:Landroid/widget/Button;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Landroid/widget/Button;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCurrentFontSize:Ljava/lang/String;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mTextPreView:Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenTextPreView2;->invalidate()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->getTextSettingInfo()Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    move-result-object v0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mSettingInfo:Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;

    iget v1, v1, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iput v1, v0, Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;->size:F

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasView:Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;

    invoke-interface {v1, v0}, Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;->setTextSettingInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    :cond_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$4;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    const/16 v1, 0x404

    iput v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCanvasWidth:I

    goto/16 :goto_0
.end method
