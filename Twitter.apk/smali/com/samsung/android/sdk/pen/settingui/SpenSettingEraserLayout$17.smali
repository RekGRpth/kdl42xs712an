.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field final synthetic this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    return-object v0
.end method


# virtual methods
.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mMovableRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->getMovableRect()Landroid/graphics/Rect;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)Landroid/graphics/Rect;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    iget-boolean v0, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mIsRotated:Z

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->rotatePosition()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$7(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->mIsRotated:Z

    :goto_1
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;

    # invokes: Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->checkPosition()V
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;->access$8(Lcom/samsung/android/sdk/pen/settingui/SpenSettingEraserLayout;)V
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method
