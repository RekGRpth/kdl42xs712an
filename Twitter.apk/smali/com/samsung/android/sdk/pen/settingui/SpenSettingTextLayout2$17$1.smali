.class Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

.field private final synthetic val$fromFinal:I

.field private final synthetic val$toFinal:I


# direct methods
.method constructor <init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;II)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    iput p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$fromFinal:I

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$toFinal:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCount:I
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    invoke-static {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$14(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;I)V

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$fromFinal:I

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$toFinal:I

    if-le v0, v1, :cond_1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$fromFinal:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)I

    move-result v1

    sub-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutHeight(I)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$toFinal:I

    if-gt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$toFinal:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutHeight(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$fromFinal:I

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mCount:I
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$13(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)I

    move-result v1

    add-int/2addr v0, v1

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutHeight(I)V

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$toFinal:I

    if-lt v0, v1, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->val$toFinal:I

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setLayoutHeight(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/util/Timer;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17$1;->this$1:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->this$0:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;->access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$17;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-result-object v0

    # getter for: Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->mScrollTimer:Ljava/util/Timer;
    invoke-static {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->access$15(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;)Ljava/util/Timer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Timer;->cancel()V

    goto :goto_0
.end method
