.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;
.super Landroid/view/View;
.source "Twttr"


# static fields
.field private static NUM_POINTS:I


# instance fields
.field private mAdvancedSetting:Ljava/lang/String;

.field private mBitmap:Landroid/graphics/Bitmap;

.field private mBitmapPaint:Landroid/graphics/Paint;

.field private mColor:I

.field private mContext:Landroid/content/Context;

.field private mPenPluginInfoList:Ljava/util/ArrayList;

.field private mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

.field private mPenType:Ljava/lang/String;

.field private mPoints:[F

.field private mPressures:[F

.field private mRect:Landroid/graphics/RectF;

.field private mStrokeWidth:F


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0xa

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    const/high16 v0, 0x41a00000    # 20.0f

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v0, -0x1000000

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v0, v0, 0x2

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x4

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v1, 0x0

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 21

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginIndexByPenName(Ljava/lang/String;)I

    move-result v18

    const/4 v1, -0x1

    move/from16 v0, v18

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    if-nez v1, :cond_2

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->loadPenPlugin(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    if-eqz v1, :cond_0

    :cond_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    const/4 v2, 0x4

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->getPenAttribute(I)Z

    move-result v1

    if-eqz v1, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mAdvancedSetting:Ljava/lang/String;

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setAdvancedSetting(Ljava/lang/String;)V

    :cond_3
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setSize(F)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    move-object/from16 v0, p0

    iget v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setColor(I)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/Bitmap;->eraseColor(I)V

    const-wide/16 v3, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v5, 0x0

    aget v2, v2, v5

    mul-float/2addr v2, v1

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v6, v6, -0x1

    aget v5, v5, v6

    mul-float/2addr v1, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string/jumbo v6, "Marker"

    invoke-virtual {v5, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v5, 0x40000000    # 2.0f

    div-float/2addr v1, v5

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, -0x2

    aget v5, v5, v6

    sub-float/2addr v5, v1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v7, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v7, v7, 0x2

    add-int/lit8 v7, v7, -0x4

    aget v6, v6, v7

    cmpg-float v5, v5, v6

    if-gez v5, :cond_4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v6, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v6, v6, 0x2

    add-int/lit8 v6, v6, -0x4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v8, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v8, v8, 0x2

    add-int/lit8 v8, v8, -0x2

    aget v7, v7, v8

    sub-float/2addr v7, v1

    const/high16 v8, 0x3f800000    # 1.0f

    sub-float/2addr v7, v8

    aput v7, v5, v6

    :cond_4
    move v15, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    const-string/jumbo v5, ".Brush"

    invoke-virtual {v2, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/high16 v2, 0x40000000    # 2.0f

    div-float/2addr v1, v2

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const/4 v5, 0x0

    aget v2, v2, v5

    mul-float/2addr v1, v2

    move/from16 v16, v1

    :goto_1
    const/4 v1, 0x0

    move/from16 v17, v1

    move-wide v1, v3

    :goto_2
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    move/from16 v0, v17

    if-lt v0, v3, :cond_5

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v1}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->setBitmap(Landroid/graphics/Bitmap;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    const/4 v2, 0x0

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmapPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    goto/16 :goto_0

    :cond_5
    if-nez v17, :cond_6

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v1

    const/4 v5, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v4, v17, 0x2

    aget v3, v3, v4

    add-float v6, v3, v15

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v4, v17, 0x2

    add-int/lit8 v4, v4, 0x1

    aget v7, v3, v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v8, v3, v17

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    move-wide v3, v1

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    move-wide v2, v1

    :goto_3
    add-int/lit8 v1, v17, 0x1

    move/from16 v17, v1

    move-wide/from16 v19, v2

    move-wide/from16 v1, v19

    goto :goto_2

    :cond_6
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v17

    if-ne v0, v3, :cond_7

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    sub-int v3, v3, v17

    mul-int/lit8 v3, v3, 0x5

    int-to-long v3, v3

    add-long/2addr v3, v1

    const/4 v5, 0x1

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v7, v17, 0x2

    aget v6, v6, v7

    sub-float v6, v6, v16

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v17, 0x2

    add-int/lit8 v8, v8, 0x1

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v8, v8, v17

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    move-wide v2, v1

    goto :goto_3

    :cond_7
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    sub-int v3, v3, v17

    mul-int/lit8 v3, v3, 0x5

    int-to-long v3, v3

    add-long/2addr v3, v1

    const/4 v5, 0x2

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v7, v17, 0x2

    aget v6, v6, v7

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    mul-int/lit8 v8, v17, 0x2

    add-int/lit8 v8, v8, 0x1

    aget v7, v7, v8

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    aget v8, v8, v17

    move-object/from16 v0, p0

    iget v9, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static/range {v1 .. v14}, Landroid/view/MotionEvent;->obtain(JJIFFFFIFFII)Landroid/view/MotionEvent;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    move/from16 v0, v18

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;

    invoke-virtual {v3}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginInfo;->getPenPluginObject()Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mRect:Landroid/graphics/RectF;

    invoke-interface {v3, v4, v5}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPenInterface;->draw(Landroid/view/MotionEvent;Landroid/graphics/RectF;)V

    invoke-virtual {v4}, Landroid/view/MotionEvent;->recycle()V

    move-wide v2, v1

    goto/16 :goto_3

    :cond_8
    move/from16 v16, v1

    goto/16 :goto_1
.end method

.method protected onLayout(ZIIII)V
    .locals 9

    const/high16 v8, 0x41600000    # 14.0f

    const/4 v1, 0x1

    const/4 v7, 0x0

    const/high16 v6, 0x41800000    # 16.0f

    if-eqz p1, :cond_0

    sub-int v0, p4, p2

    sub-int v2, p5, p3

    int-to-float v0, v0

    div-float/2addr v0, v6

    int-to-float v2, v2

    div-float/2addr v2, v6

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    int-to-float v4, p2

    const/high16 v5, 0x3f800000    # 1.0f

    mul-float/2addr v5, v0

    add-float/2addr v4, v5

    aput v4, v3, v7

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    int-to-float v4, p3

    mul-float v5, v2, v8

    add-float/2addr v4, v5

    aput v4, v3, v1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v4, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v4, v4, 0x2

    add-int/lit8 v4, v4, -0x2

    int-to-float v5, p2

    mul-float/2addr v0, v6

    add-float/2addr v0, v5

    aput v0, v3, v4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x1

    int-to-float v4, p3

    mul-float/2addr v2, v8

    add-float/2addr v2, v4

    aput v2, v0, v3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v2, v2, 0x2

    add-int/lit8 v2, v2, -0x2

    aget v0, v0, v2

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    aget v2, v2, v7

    sub-float/2addr v0, v2

    sget v2, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    add-int/lit8 v2, v2, -0x1

    int-to-float v2, v2

    div-float v2, v0, v2

    const/4 v0, 0x2

    :goto_0
    sget v3, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    mul-int/lit8 v3, v3, 0x2

    add-int/lit8 v3, v3, -0x2

    if-lt v0, v3, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    const v2, 0x3f333333    # 0.7f

    aput v2, v0, v7

    move v0, v1

    :goto_1
    sget v1, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->NUM_POINTS:I

    if-lt v0, v1, :cond_2

    :cond_0
    return-void

    :cond_1
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v5, v0, -0x2

    aget v4, v4, v5

    add-float/2addr v4, v2

    aput v4, v3, v0

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    add-int/lit8 v4, v0, 0x1

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPoints:[F

    aget v5, v5, v1

    aput v5, v3, v4

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPressures:[F

    add-int/lit8 v3, v0, -0x1

    aget v2, v2, v3

    const v3, 0x3ccccccd    # 0.025f

    sub-float/2addr v2, v3

    aput v2, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method protected onSizeChanged(IIII)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_0
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mBitmap:Landroid/graphics/Bitmap;

    return-void
.end method

.method public setPenPlugin(Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;)V
    .locals 1

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginManager:Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenPenPluginManager;->getPenPluginInfoList()Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenPluginInfoList:Ljava/util/ArrayList;

    return-void
.end method

.method public setPenType(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mPenType:Ljava/lang/String;

    return-void
.end method

.method public setStrokeAdvancedSetting(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mAdvancedSetting:Ljava/lang/String;

    return-void
.end method

.method public setStrokeAlpha(I)V
    .locals 3

    shl-int/lit8 v0, p1, 0x18

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    const v2, 0xffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    return-void
.end method

.method public setStrokeColor(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mColor:I

    return-void
.end method

.method public setStrokeSize(F)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPreview;->mStrokeWidth:F

    return-void
.end method
