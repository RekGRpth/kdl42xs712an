.class Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private mBitmap:Landroid/graphics/Bitmap;

.field private mFlag:Z

.field private final mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

.field private mPresetImage:Ljava/lang/String;

.field private mPresetOrder:I


# direct methods
.method constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    new-instance v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mFlag:Z

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetImage:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public getAdvancedSetting()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public getColor()I
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    return v0
.end method

.method public getFlag()Z
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mFlag:Z

    return v0
.end method

.method public getPenName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    return-object v0
.end method

.method public getPenSize()F
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iget v0, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    return v0
.end method

.method public getPresetImageName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetImage:Ljava/lang/String;

    return-object v0
.end method

.method public getPresetOrder()I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetOrder:I

    return v0
.end method

.method public hashCode()I
    .locals 1

    invoke-super {p0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    return v0
.end method

.method public setAdvancedSetting(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->advancedSetting:Ljava/lang/String;

    return-void
.end method

.method public setBitmapSize(II)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, p2, v0}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mBitmap:Landroid/graphics/Bitmap;

    :cond_0
    return-void
.end method

.method public setColor(I)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->color:I

    return-void
.end method

.method public setFlag(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mFlag:Z

    return-void
.end method

.method public setPenName(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput-object p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->name:Ljava/lang/String;

    return-void
.end method

.method public setPenSize(F)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPenData:Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;

    iput p1, v0, Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;->size:F

    return-void
.end method

.method public setPresetImageName(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetImage:Ljava/lang/String;

    return-void
.end method

.method public setPresetOrder(I)V
    .locals 0

    iput p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPenPresetInfo;->mPresetOrder:I

    return-void
.end method
