.class Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;
.super Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final context:Landroid/content/Context;

.field private final mHandler:Landroid/os/Handler;

.field private final mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private final mItemList:Ljava/util/ArrayList;

.field private final mListView:Landroid/widget/ListView;

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;

.field private final root:Landroid/view/View;

.field private final runnable:Ljava/lang/Runnable;

.field private windowHeight:I

.field private windowWidth:I


# direct methods
.method public constructor <init>(Landroid/view/View;Ljava/util/ArrayList;IIF)V
    .locals 5

    const/4 v1, 0x0

    const/4 v4, -0x1

    const/4 v3, -0x2

    invoke-direct {p0, p1}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;-><init>(Landroid/view/View;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowWidth:I

    iput v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowHeight:I

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->runnable:Ljava/lang/Runnable;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mHandler:Landroid/os/Handler;

    iput p3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowWidth:I

    iput p4, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowHeight:I

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mItemList:Ljava/util/ArrayList;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;

    const-string/jumbo v2, ""

    invoke-direct {v0, v1, v2, p5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v3, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    new-instance v1, Landroid/widget/ListView;

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;

    invoke-direct {v1, v2}, Landroid/widget/ListView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v2, v4, v4}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->root:Landroid/view/View;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->root:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->setContentView(Landroid/view/View;)V

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;

    const v2, 0x1090003    # android.R.layout.simple_list_item_1

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mItemList:Ljava/util/ArrayList;

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$ListAdapter;-><init>(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;Landroid/content/Context;ILjava/util/List;)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v2, "tw_list_divider_holo_light"

    invoke-virtual {v1, v2}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setDivider(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "tw_menu_dropdown_panel_holo_light"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p3}, Landroid/widget/PopupWindow;->setWidth(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->window:Landroid/widget/PopupWindow;

    invoke-virtual {v0, p4}, Landroid/widget/PopupWindow;->setHeight(I)V

    return-void
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowWidth:I

    return v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)I
    .locals 1

    iget v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->windowHeight:I

    return v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->runnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;)Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mImageUtil:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    return-object v0
.end method


# virtual methods
.method public changeOrientation(Landroid/content/res/Configuration;)V
    .locals 0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->dismiss()V

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;->onSelectItem(I)V

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->dismiss()V

    return-void
.end method

.method public setOnItemSelectListner(Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListener:Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2$NameDropdownSelectListner;

    return-void
.end method

.method public show(IILjava/lang/String;)V
    .locals 3

    const/4 v2, -0x2

    invoke-super {p0}, Lcom/samsung/android/sdk/pen/settingui/SPenDropdownView;->show()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mItemList:Ljava/util/ArrayList;

    invoke-virtual {v0, p3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mListView:Landroid/widget/ListView;

    invoke-virtual {v1, v0}, Landroid/widget/ListView;->setSelection(I)V

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->root:Landroid/view/View;

    new-instance v1, Landroid/view/ViewGroup$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->root:Landroid/view/View;

    invoke-virtual {v0, v2, v2}, Landroid/view/View;->measure(II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->window:Landroid/widget/PopupWindow;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->anchor:Landroid/view/View;

    invoke-virtual {v0, v1, p1, p2}, Landroid/widget/PopupWindow;->showAsDropDown(Landroid/view/View;II)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SPenFontNameDropdown2;->runnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
