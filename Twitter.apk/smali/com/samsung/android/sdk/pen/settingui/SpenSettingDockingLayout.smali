.class public Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# static fields
.field protected static mSdkVersion:I


# instance fields
.field protected mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mEnable:Z

.field private mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

.field private mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

.field private mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

.field private mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

.field private mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    sput v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSdkVersion:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/HashMap;Ljava/util/HashMap;Ljava/lang/String;Landroid/widget/RelativeLayout;)V
    .locals 9

    const/16 v2, 0x640

    const/4 v8, 0x1

    const/4 v7, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v6, 0x0

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->widthPixels:I

    iget v0, v0, Landroid/util/DisplayMetrics;->heightPixels:I

    if-lt v1, v2, :cond_0

    if-ge v0, v2, :cond_1

    :cond_0
    const-string/jumbo v0, "It does not support the current resolution."

    invoke-static {p1, v0, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    iput-boolean v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    :goto_0
    return-void

    :cond_1
    iput-boolean v8, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {v0, p1, p4, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-direct {v0, p1, p4, p5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p5

    invoke-direct/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/util/HashMap;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-direct {v0, p1, p4, p5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-direct {v0, p1, p4, p5, v5}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/widget/RelativeLayout;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    sget v0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSdkVersion:I

    const/16 v1, 0x10

    if-ge v0, v1, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    :goto_1
    new-instance v0, Landroid/widget/LinearLayout$LayoutParams;

    const/4 v1, -0x2

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    invoke-virtual {p0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setOrientation(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->addView(Landroid/view/View;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setVisibility(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v8}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v6}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;

    invoke-direct {v1, p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onPenPopupListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2$PopupListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onTextPopupListener;

    invoke-direct {v1, p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onTextPopupListener;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onTextPopupListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2$PopupListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onRemoverPopupListener;

    invoke-direct {v1, p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onRemoverPopupListener;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onRemoverPopupListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$PopupListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onSelectPopupListener;

    invoke-direct {v1, p0, v7}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onSelectPopupListener;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onSelectPopupListener;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopupListenr(Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2$PopupListener;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onEraserClearListener;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$onEraserClearListener;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setEraserListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2$EventListener;)V

    iput-object v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    goto/16 :goto_0

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mDrawableImg:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "vienna_popup_bg"

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    return-object v0
.end method

.method static synthetic access$1(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    return-object v0
.end method

.method static synthetic access$2(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    return-object v0
.end method

.method static synthetic access$3(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    return-object v0
.end method

.method static synthetic access$4(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;
    .locals 1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    return-object v0
.end method

.method static synthetic access$5(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V
    .locals 0

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    return-void
.end method

.method private setTopBottomToZeroMarginItem()V
    .locals 4

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$1;-><init>(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;)V

    const-wide/16 v2, 0x64

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    :cond_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->close()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    goto :goto_0
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    invoke-super {p0, p1}, Landroid/widget/LinearLayout;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    goto :goto_0
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-super {p0}, Landroid/widget/LinearLayout;->onDetachedFromWindow()V

    goto :goto_0
.end method

.method protected onVisibilityChanged(Landroid/view/View;I)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setTopBottomToZeroMarginItem()V

    invoke-super {p0, p1, p2}, Landroid/widget/LinearLayout;->onVisibilityChanged(Landroid/view/View;I)V

    goto :goto_0
.end method

.method public setEraserInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingRemoverInfo;)V

    goto :goto_0
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-eqz p1, :cond_0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    goto :goto_0
.end method

.method public setPenInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingPenInfo;)V

    goto :goto_0
.end method

.method public setPenInfoList(Ljava/util/List;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPenInfoList(Ljava/util/List;)V

    goto :goto_0
.end method

.method public setPopupMode(I)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    packed-switch p1, :pswitch_data_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    invoke-interface {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x2

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setPopup(Z)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mListener:Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;

    const/4 v1, 0x4

    invoke-interface {v0, v1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout$Listener;->onPopup(I)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public setPosition(II)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    iput p2, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public setSelectionInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingSelectionInfo;)V

    goto :goto_0
.end method

.method public setSpenView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mPenSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingPenLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mRemoverSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingRemoverLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mSelectionSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingSelectionLayout2;->setCanvasView(Lcom/samsung/android/sdk/pen/SpenSettingViewInterface;)V

    goto :goto_0
.end method

.method public setTextInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V
    .locals 1

    iget-boolean v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mEnable:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenSettingDockingLayout;->mTextSettingView:Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/settingui/SpenSettingTextLayout2;->setInfo(Lcom/samsung/android/sdk/pen/SpenSettingTextInfo;)V

    goto :goto_0
.end method
