.class Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;
.super Landroid/widget/ImageView;
.source "Twttr"


# instance fields
.field private mBorderPaint:Landroid/graphics/Paint;

.field private mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

.field private mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mCursorRect:Landroid/graphics/Rect;

.field private mCustom_imagepath:Ljava/lang/String;

.field private mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

.field mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

.field private mHeight:I

.field private mSpectrum:Landroid/graphics/Bitmap;

.field private mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

.field private mWidth:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;F)V
    .locals 2

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const-string/jumbo v0, ""

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCustom_imagepath:Ljava/lang/String;

    iput-object p2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCustom_imagepath:Ljava/lang/String;

    new-instance v0, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCustom_imagepath:Ljava/lang/String;

    invoke-direct {v0, p1, v1, p3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;-><init>(Landroid/content/Context;Ljava/lang/String;F)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    invoke-direct {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->initView()V

    return-void
.end method

.method private initView()V
    .locals 7

    const/16 v6, 0x105

    const/16 v5, 0x41

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_select_kit"

    const/16 v2, 0xf

    const/16 v3, 0x11

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v0

    iput v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    new-instance v0, Landroid/graphics/Rect;

    iget v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    iget v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v0, v1}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box_shadow"

    invoke-virtual {v0, v1, v6, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box_shadow"

    invoke-virtual {v0, v1, v6, v5}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    new-instance v0, Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicWidth()I

    move-result v1

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v2}, Landroid/graphics/drawable/BitmapDrawable;->getIntrinsicHeight()I

    move-result v2

    invoke-direct {v0, v4, v4, v1, v2}, Landroid/graphics/Rect;-><init>(IIII)V

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    :cond_0
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    const v1, -0x737374

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    sget-object v1, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    return-void
.end method


# virtual methods
.method public close()V
    .locals 2

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    :cond_0
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_1
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    :cond_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_3
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    :cond_5
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    :cond_6
    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    iput-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 6

    const/4 v1, 0x0

    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getWidth()I

    move-result v0

    int-to-float v3, v0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getHeight()I

    move-result v0

    int-to-float v4, v0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mBorderPaint:Landroid/graphics/Paint;

    move-object v0, p1

    move v2, v1

    invoke-virtual/range {v0 .. v5}, Landroid/graphics/Canvas;->drawRect(FFFFLandroid/graphics/Paint;)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrColorShadow:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/BitmapDrawable;->draw(Landroid/graphics/Canvas;)V

    :cond_0
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 4

    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/ImageView;->onSizeChanged(IIII)V

    if-ne p1, p3, :cond_0

    if-eq p2, p4, :cond_2

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mDrawimage:Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;

    const-string/jumbo v1, "snote_color_box"

    const/16 v2, 0x105

    const/16 v3, 0x41

    invoke-virtual {v0, v1, v2, v3}, Lcom/samsung/android/sdk/pen/settingui/SPenImageUtil;->setDrawableImg(Ljava/lang/String;II)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/BitmapDrawable;

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrumDrawable:Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {p0, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :cond_2
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 10

    const/4 v9, 0x1

    const/4 v0, 0x0

    const v8, 0xffffff

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->getParent()Landroid/view/ViewParent;

    move-result-object v1

    invoke-interface {v1, v9}, Landroid/view/ViewParent;->requestDisallowInterceptTouchEvent(Z)V

    :cond_0
    const/4 v3, -0x1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    float-to-int v2, v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v1

    float-to-int v1, v1

    if-gez v2, :cond_1

    move v2, v0

    :cond_1
    if-gez v1, :cond_7

    :goto_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-gt v1, v2, :cond_5

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    :goto_1
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v2

    if-gt v2, v0, :cond_2

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :cond_2
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mSpectrum:Landroid/graphics/Bitmap;

    invoke-virtual {v2, v1, v0}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v2

    :goto_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    iget v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    div-int/lit8 v4, v4, 0x2

    sub-int v4, v1, v4

    iget v5, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    div-int/lit8 v5, v5, 0x2

    sub-int v5, v0, v5

    iget v6, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mWidth:I

    div-int/lit8 v6, v6, 0x2

    add-int/2addr v6, v1

    iget v7, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int/2addr v7, v0

    invoke-virtual {v3, v4, v5, v6, v7}, Landroid/graphics/Rect;->set(IIII)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorDrawable:Landroid/graphics/drawable/BitmapDrawable;

    iget-object v4, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mCursorRect:Landroid/graphics/Rect;

    invoke-virtual {v3, v4}, Landroid/graphics/drawable/BitmapDrawable;->setBounds(Landroid/graphics/Rect;)V

    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    if-eqz v3, :cond_4

    and-int v3, v2, v8

    if-ne v3, v8, :cond_3

    const v2, 0xfefefe

    :cond_3
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    const/high16 v4, -0x2000000

    and-int/2addr v2, v8

    or-int/2addr v2, v4

    invoke-interface {v3, v2, v1, v0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;->onColorChanged(III)V

    :cond_4
    invoke-virtual {p0}, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->invalidate()V

    return v9

    :cond_5
    move v1, v2

    goto :goto_1

    :cond_6
    move v1, v2

    move v2, v3

    goto :goto_2

    :cond_7
    move v0, v1

    goto :goto_0
.end method

.method public setColorPickerColorChangeListener(Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2;->mColorPickerColorChangeListener:Lcom/samsung/android/sdk/pen/settingui/SpenColorGradationView2$onColorChangedListener;

    return-void
.end method
