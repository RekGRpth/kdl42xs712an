.class Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;
.super Landroid/widget/LinearLayout;
.source "Twttr"


# instance fields
.field private mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0, p1}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    return-void
.end method


# virtual methods
.method protected onLayout(ZIIII)V
    .locals 6

    invoke-super/range {p0 .. p5}, Landroid/widget/LinearLayout;->onLayout(ZIIII)V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;->onLayout(ZIIII)V

    :cond_0
    return-void
.end method

.method public setOnLayoutListener(Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;)V
    .locals 0

    iput-object p1, p0, Lcom/samsung/android/sdk/pen/settingui/SpenPalletView;->mPalletViewOnLayoutListner:Lcom/samsung/android/sdk/pen/settingui/SpenPalletView$onLayoutListner;

    return-void
.end method
