.class public Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final APK_PLUGIN_CHECK_INTENT:Ljava/lang/String; = "com.samsung.android.sdk.pen.plugin.PICK"

.field private static final BINARY_TYPE_INDEX:I = 0x4

.field private static final BINARY_TYPE_JAVA:I = 0x0

.field private static final BINARY_TYPE_NATIVE:I = 0x1

.field private static final BUILTIN_PLUGIN_LIST:[[Ljava/lang/String;

.field private static final CLASS_NAME_INDEX:I = 0xd

.field private static final EXTRA_INFO_INDEX:I = 0xa

.field private static final FOCUSED_ICON_IMAGE_URI_INDEX:I = 0x9

.field private static final HAS_PRIVATE_KEY_INDEX:I = 0x5

.field private static final ICON_IMAGE_URI_INDEX:I = 0x6

.field private static final INTERFACE_NAME_INDEX:I = 0x2

.field private static final INTERFACE_VERSION_INDEX:I = 0x3

.field private static final META_DATA_KEY_SPEN_PLUGIN_INFO:Ljava/lang/String; = "SPEN_PLUGIN_INFO"

.field private static final PACKAGE_NAME_INDEX:I = 0xc

.field private static final PLUGIN_NAME_URI_INDEX:I = 0xb

.field private static final PRESET_ICON_IMAGE_URI_INDEX:I = 0x8

.field private static final SELECTED_ICON_IMAGE_URI_INDEX:I = 0x7

.field private static final TYPE_INDEX:I = 0x0

.field private static final VERSION_INDEX:I = 0x1

.field private static mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;


# instance fields
.field private mAPKPluginList:Ljava/util/List;

.field private mBuiltInPluginList:Ljava/util/List;

.field private mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

.field private final mLoadedPluginList:Ljava/util/ArrayList;

.field private mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x4

    const/4 v8, 0x3

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/16 v0, 0xf

    new-array v0, v0, [[Ljava/lang/String;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string/jumbo v2, "1"

    aput-object v2, v1, v8

    const-string/jumbo v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string/jumbo v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "snote_popup_pensetting_pen"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "snote_popup_pensetting_pen_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "pen_preset_pen"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "snote_popup_pensetting_pen_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "extraInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "InkPen"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "InkPen"

    aput-object v3, v1, v2

    aput-object v1, v0, v5

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string/jumbo v2, "1"

    aput-object v2, v1, v8

    const-string/jumbo v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string/jumbo v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "snote_popup_pensetting_pencil"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "snote_popup_pensetting_pencil_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "pen_preset_pencil"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "snote_popup_pensetting_pencil_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "extraInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "Pencil"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "Pencil"

    aput-object v3, v1, v2

    aput-object v1, v0, v6

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string/jumbo v2, "1"

    aput-object v2, v1, v8

    const-string/jumbo v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string/jumbo v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "snote_popup_pensetting_brush"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "snote_popup_pensetting_brush_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "pen_preset_brush"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "snote_popup_pensetting_brush_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "extraInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "Brush"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "Brush"

    aput-object v3, v1, v2

    aput-object v1, v0, v7

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string/jumbo v2, "1"

    aput-object v2, v1, v8

    const-string/jumbo v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string/jumbo v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "snote_popup_pensetting_chinabrush"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "snote_popup_pensetting_chinabrush_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "pen_preset_chinabrush"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "snote_popup_pensetting_chinabrush_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "extraInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "ChineseBrush"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "ChineseBrush"

    aput-object v3, v1, v2

    aput-object v1, v0, v8

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    aput-object v2, v1, v5

    const-string/jumbo v2, "1"

    aput-object v2, v1, v6

    const-string/jumbo v2, "SpenPenInterface"

    aput-object v2, v1, v7

    const-string/jumbo v2, "1"

    aput-object v2, v1, v8

    const-string/jumbo v2, "native"

    aput-object v2, v1, v9

    const/4 v2, 0x5

    const-string/jumbo v3, "0"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string/jumbo v3, "snote_popup_pensetting_marker"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string/jumbo v3, "snote_popup_pensetting_marker_select"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string/jumbo v3, "pen_preset_marker"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string/jumbo v3, "snote_popup_pensetting_marker_focus"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string/jumbo v3, "extraInfo"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string/jumbo v3, "Marker"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string/jumbo v3, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string/jumbo v3, "Marker"

    aput-object v3, v1, v2

    aput-object v1, v0, v9

    const/4 v1, 0x5

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Pen"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "snote_popup_pensetting_alpha"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "snote_popup_pensetting_alpha_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "pen_preset_alpha"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "snote_popup_pensetting_alpha_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "MagicPen"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "MagicPen"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "Pen"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenPenInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "native"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "snote_popup_pensetting_chinabrush"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "snote_popup_pensetting_chinabrush_select"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "pen_preset_chinabrush"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "snote_popup_pensetting_chinabrush_focus"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "Beautify"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.pen.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "Beautify"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "ObjectRuntime"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenObjectRuntimeInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "defaultIconImageURI"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "Video"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.objectruntime.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "Video"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "ObjectRuntime"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenObjectRuntimeInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "defaultIconImageURI"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "SandPaper"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.objectruntime.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "SandPaper"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "ObjectRuntime"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenObjectRuntimeInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "defaultIconImageURI"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "SnowPaper"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.objectruntime.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "SnowPaper"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "recognition"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenLanguageRecognitionInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "Text"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.text"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "TextRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "TextRecognition"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenLanguageRecognitionInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "SpenText"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "TextRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "EquationRecognition"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenRecognitionInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "SpenEquation"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "EquationRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "ShapeRecognition"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenRecognitionInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "1"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "1/4"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "SpenShape"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "ShapeRecognitionPlugin"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const/16 v2, 0xe

    new-array v2, v2, [Ljava/lang/String;

    const-string/jumbo v3, "SignatureVerification"

    aput-object v3, v2, v5

    const-string/jumbo v3, "1"

    aput-object v3, v2, v6

    const-string/jumbo v3, "SpenSignatureVerificationInterface"

    aput-object v3, v2, v7

    const-string/jumbo v3, "1"

    aput-object v3, v2, v8

    const-string/jumbo v3, "java"

    aput-object v3, v2, v9

    const/4 v3, 0x5

    const-string/jumbo v4, "0"

    aput-object v4, v2, v3

    const/4 v3, 0x6

    const-string/jumbo v4, "uriInfo"

    aput-object v4, v2, v3

    const/4 v3, 0x7

    const-string/jumbo v4, "selectedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x8

    const-string/jumbo v4, "presetIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0x9

    const-string/jumbo v4, "focusedIconImageURI"

    aput-object v4, v2, v3

    const/16 v3, 0xa

    const-string/jumbo v4, "extraInfo"

    aput-object v4, v2, v3

    const/16 v3, 0xb

    const-string/jumbo v4, "SpenSignature"

    aput-object v4, v2, v3

    const/16 v3, 0xc

    const-string/jumbo v4, "com.samsung.android.sdk.pen.recognition.preload"

    aput-object v4, v2, v3

    const/16 v3, 0xd

    const-string/jumbo v4, "Signature"

    aput-object v4, v2, v3

    aput-object v2, v0, v1

    sput-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->BUILTIN_PLUGIN_LIST:[[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
    .locals 11

    const/4 v1, 0x1

    const/4 v2, 0x0

    const-class v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    monitor-enter v4

    if-nez p0, :cond_0

    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'context\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_0
    :try_start_1
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    if-nez v0, :cond_8

    new-instance v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;-><init>()V

    sput-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v3, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;-><init>()V

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Fail to create PackageReceiver"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string/jumbo v3, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v3, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string/jumbo v3, "package"

    invoke-virtual {v0, v3}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    sget-object v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v5, v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    invoke-virtual {v3, v5, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v3, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;-><init>()V

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Fail to create JniPluginManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    if-nez v0, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Fail to create ArrayList for BuiltInPluginList"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    if-nez v0, :cond_4

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Fail to create ArrayList for APKPluginList"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    sget-object v5, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->BUILTIN_PLUGIN_LIST:[[Ljava/lang/String;

    array-length v6, v5

    move v3, v2

    :goto_0
    if-lt v3, v6, :cond_9

    const/4 v0, 0x0

    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v3

    :try_start_2
    const-string/jumbo v5, "plugin.ini"

    invoke-virtual {v3, v5}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    move v10, v1

    move-object v1, v0

    move v0, v10

    :goto_1
    if-eqz v0, :cond_6

    :try_start_3
    new-instance v2, Ljava/io/BufferedReader;

    new-instance v0, Ljava/io/InputStreamReader;

    invoke-direct {v0, v1}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v2, v0}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;)V

    :cond_5
    :goto_2
    invoke-virtual {v2}, Ljava/io/BufferedReader;->readLine()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_e

    invoke-virtual {v2}, Ljava/io/BufferedReader;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_6
    if-eqz v1, :cond_7

    :try_start_4
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :cond_7
    :goto_3
    :try_start_5
    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "Constructor is completed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    monitor-exit v4

    return-object v0

    :cond_9
    :try_start_6
    aget-object v0, v5, v3

    array-length v7, v0

    const/16 v8, 0xe

    if-eq v7, v8, :cond_a

    :goto_4
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    :cond_a
    new-instance v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v7}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    const/4 v8, 0x0

    aget-object v8, v0, v8

    iput-object v8, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    const/4 v8, 0x1

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    const/4 v8, 0x2

    aget-object v8, v0, v8

    iput-object v8, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    const/4 v8, 0x3

    aget-object v8, v0, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    iput v8, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    const/4 v8, 0x4

    aget-object v8, v0, v8

    const/4 v9, 0x5

    aget-object v9, v0, v9

    invoke-static {v9}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v9

    iput-boolean v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    const/4 v9, 0x6

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    const/4 v9, 0x7

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    const/16 v9, 0x8

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    const/16 v9, 0x9

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    const/16 v9, 0xa

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const/16 v9, 0xb

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    const/16 v9, 0xc

    aget-object v9, v0, v9

    iput-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    const/16 v9, 0xd

    aget-object v0, v0, v9

    iput-object v0, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v0, "native"

    invoke-virtual {v8, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    const/4 v0, 0x1

    iput v0, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    :goto_5
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_b
    :goto_6
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_d

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    :cond_c
    const/4 v0, 0x0

    iput v0, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    goto :goto_5

    :cond_d
    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v9, v7, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    goto :goto_6

    :catch_0
    move-exception v1

    const-string/jumbo v1, "PluginManager"

    const-string/jumbo v3, "Fail to read Plugin List of App"

    invoke-static {v1, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-object v1, v0

    move v0, v2

    goto/16 :goto_1

    :cond_e
    :try_start_7
    new-instance v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v3}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    const-string/jumbo v5, ";"

    invoke-virtual {v0, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v5, v0

    const/16 v6, 0xe

    if-ne v5, v6, :cond_5

    const/4 v5, 0x0

    aget-object v5, v0, v5

    iput-object v5, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    const/4 v5, 0x1

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    const/4 v5, 0x2

    aget-object v5, v0, v5

    iput-object v5, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    const/4 v5, 0x3

    aget-object v5, v0, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    iput v5, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    const/4 v5, 0x4

    aget-object v5, v0, v5

    const/4 v6, 0x5

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    const/4 v6, 0x6

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    const/16 v6, 0x8

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    const/16 v6, 0x9

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    const/16 v6, 0xa

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const/16 v6, 0xb

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    const/16 v6, 0xc

    aget-object v6, v0, v6

    iput-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    const/16 v6, 0xd

    aget-object v0, v0, v6

    iput-object v0, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v0, "native"

    invoke-virtual {v5, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    const/4 v0, 0x1

    iput v0, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    :goto_7
    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_f
    :goto_8
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_11

    sget-object v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mInstance:Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto/16 :goto_2

    :catch_1
    move-exception v0

    :try_start_8
    const-string/jumbo v1, "PluginManager"

    const-string/jumbo v2, "I/O error occurs"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Fail to update ArrayList for BuiltInPluginList of App"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    :cond_10
    const/4 v0, 0x0

    :try_start_9
    iput v0, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_9 .. :try_end_9} :catch_2
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    goto :goto_7

    :catch_2
    move-exception v0

    :try_start_a
    const-string/jumbo v1, "PluginManager"

    const-string/jumbo v2, "I/O error occurs"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Ljava/lang/NumberFormatException;->printStackTrace()V

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Fail to update ArrayList for BuiltInPluginList of App"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_a
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :cond_11
    :try_start_b
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v6, v3, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catch Ljava/lang/NumberFormatException; {:try_start_b .. :try_end_b} :catch_2
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    move-result v0

    if-eqz v0, :cond_f

    goto :goto_8

    :catch_3
    move-exception v0

    goto/16 :goto_3
.end method


# virtual methods
.method deleteAPKPluginInfo(Ljava/lang/String;)V
    .locals 4

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    :goto_0
    return-void

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getNativeHandle(Ljava/lang/Object;)I
    .locals 8

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "getNativeHandle()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'object\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v5

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_2

    move v0, v3

    :goto_0
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-string/jumbo v1, "PluginManager"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string/jumbo v3, "getNativeHandle() is completed. returns "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_2
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v2, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "."

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v7, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v7, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public getPluginInfo(Ljava/lang/String;)Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;
    .locals 4

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "getPluginInfo()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'uuid\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_4

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "getPluginInfo() returns false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    :cond_4
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    new-instance v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v1}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    iget v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    iget v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    iget v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    iput v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    iget-boolean v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iput-boolean v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    iget-object v2, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iput-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v2, "getPluginInfo() is completed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    goto/16 :goto_0
.end method

.method public getPluginList(Ljava/lang/String;)Ljava/util/List;
    .locals 4

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "getPluginList()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'type\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v2, "getPluginList() is completed."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-object v1

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    const-string/jumbo v3, "all"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    :cond_4
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_5
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v3, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    const-string/jumbo v3, "all"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_6
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1
.end method

.method public getPrivateKeyHint(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;)Ljava/lang/String;
    .locals 6

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_2

    move-object v0, v1

    :goto_0
    monitor-exit v2

    return-object v0

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v4, p1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v5, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->getPrivateKeyHint()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public loadPlugin(Landroid/app/Activity;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "loadPlugin()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Unable to Use loadPlugin by null Activity"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Unable to Use loadPlugin by null JniPluginManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    move-object v1, v2

    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v5

    :goto_1
    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-static {v1, v4, v6}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v0

    :goto_2
    if-eqz v1, :cond_c

    new-instance v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;-><init>()V

    iget-object v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    iget v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    iput v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    iget-object v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    iget-object v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    iput-object v1, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    iput-object v2, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    iget v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    if-ne v0, v4, :cond_9

    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;->getNativeHandle()I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    :goto_3
    iput-object v3, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->unlock(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'key\' is wrong"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    goto/16 :goto_1

    :cond_7
    :try_start_2
    invoke-virtual {p1}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "PackageManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    :goto_4
    return-object v1

    :cond_8
    iget-object v3, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    const/16 v6, 0x80

    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-class v6, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual {v6}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    if-nez v1, :cond_e

    new-instance v0, Ldalvik/system/PathClassLoader;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-direct {v0, v7, v1}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    :goto_5
    invoke-virtual {p1}, Landroid/app/Activity;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-static {v3, v4, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, v6, v0}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v1, v2

    goto :goto_4

    :cond_9
    iget-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->construct()V

    iget-object v2, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->setObject(Ljava/lang/Object;)V

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->getNativeHandle()I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    goto/16 :goto_3

    :cond_a
    iput v5, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    goto/16 :goto_3

    :cond_b
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    iget v2, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    invoke-virtual {v0, v2, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onLoad(ILandroid/content/Context;)V

    :cond_c
    :goto_6
    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v2, "loadPlugin() is completed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_d
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->onLoad(Landroid/content/Context;)V

    goto :goto_6

    :cond_e
    move-object v0, v1

    goto :goto_5
.end method

.method public loadPlugin(Landroid/content/Context;Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;Ljava/lang/String;)Ljava/lang/Object;
    .locals 8

    const/4 v5, 0x0

    const/4 v2, 0x0

    const/4 v4, 0x1

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "loadPlugin()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Unable to Use loadPlugin by null Context"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Unable to Use loadPlugin by null JniPluginManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'info\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v3, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_5

    move-object v1, v2

    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_6

    move v0, v5

    :goto_1
    if-eqz v0, :cond_7

    new-instance v0, Ljava/lang/StringBuilder;

    const-class v1, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Ljava/lang/StringBuilder;

    iget-object v3, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v3, "."

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v3, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v6

    invoke-static {v1, v4, v6}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, v3, v0}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v0

    :goto_2
    if-eqz v1, :cond_c

    new-instance v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    invoke-direct {v6}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;-><init>()V

    iget-object v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    iget v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    iput v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    iget-object v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    iget-object v0, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    iput-object v1, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    iput-object v2, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    iget v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->binaryType:I

    if-ne v0, v4, :cond_9

    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;

    invoke-interface {v0}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenNativeHandleInterface;->getNativeHandle()I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    :goto_3
    iput-object v3, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {v0, p3}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->unlock(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_b

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'key\' is wrong"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    :try_start_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    iget-object v1, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->classLoader:Ljava/lang/ClassLoader;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    iget-object v0, v0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    goto/16 :goto_1

    :cond_7
    :try_start_2
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    if-nez v0, :cond_8

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "PackageManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    :goto_4
    return-object v1

    :cond_8
    iget-object v3, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    const/16 v6, 0x80

    invoke-virtual {v0, v3, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    move-result-object v0

    new-instance v3, Ljava/lang/StringBuilder;

    const-class v6, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-virtual {v6}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Package;->getName()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v6, "."

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v6, p2, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    iget-object v6, v0, Landroid/content/pm/ApplicationInfo;->className:Ljava/lang/String;

    iget-object v7, v0, Landroid/content/pm/ApplicationInfo;->sourceDir:Ljava/lang/String;

    if-nez v1, :cond_e

    new-instance v0, Ldalvik/system/PathClassLoader;

    invoke-static {}, Ljava/lang/ClassLoader;->getSystemClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-direct {v0, v7, v1}, Ldalvik/system/PathClassLoader;-><init>(Ljava/lang/String;Ljava/lang/ClassLoader;)V

    :goto_5
    invoke-virtual {p1}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-static {v3, v4, v1}, Ljava/lang/Class;->forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;

    move-result-object v1

    invoke-static {v1, v6, v0}, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder;->build(Ljava/lang/Class;Ljava/lang/String;Ljava/lang/ClassLoader;)Ljava/lang/Object;

    move-result-object v1

    move-object v3, v0

    goto/16 :goto_2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    move-object v1, v2

    goto :goto_4

    :cond_9
    iget-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->type:Ljava/lang/String;

    const-string/jumbo v2, "Pen"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    new-instance v0, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;

    invoke-direct {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;-><init>()V

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->construct()V

    iget-object v2, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->setObject(Ljava/lang/Object;)V

    iput-object v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->dummyObject:Ljava/lang/Object;

    invoke-virtual {v0}, Lcom/samsung/android/sdk/pen/pen/preload/NativePen;->getNativeHandle()I

    move-result v0

    iput v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    goto/16 :goto_3

    :cond_a
    iput v5, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    goto/16 :goto_3

    :cond_b
    iget-object v2, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_3
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    iget v0, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    iget v2, v6, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    invoke-virtual {v0, v2, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onLoad(ILandroid/content/Context;)V

    :cond_c
    :goto_6
    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v2, "loadPlugin() is completed"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_4

    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    throw v0

    :cond_d
    move-object v0, v1

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->onLoad(Landroid/content/Context;)V

    goto :goto_6

    :cond_e
    move-object v0, v1

    goto :goto_5
.end method

.method public setListener(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;)V
    .locals 2

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "setListener()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Unable to Use setPluginListener by null PackageReceiver"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'listener\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mPackageReceiver:Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;

    invoke-virtual {v0, p1}, Lcom/samsung/android/sdk/pen/plugin/framework/PackageReceiver;->setNotifyListener(Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager$PluginListener;)V

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "setPluginListener() is completed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public unloadPlugin(Ljava/lang/Object;)V
    .locals 8

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "unloadPlugin()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "E_INVALID_STATE : Unable to Use unloadPlugin by null JniPluginManager"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : parameter \'object\' is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-static {p1}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v0

    check-cast v0, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "E_INVALID_ARG : proxy handler of object is null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iget-object v1, v0, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    const/4 v2, 0x0

    iget-object v5, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    monitor-enter v5

    :try_start_0
    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    :goto_0
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    move v0, v2

    :goto_1
    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_6

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "unloadPlugin() returns false"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "E_INVALID_ARG : Data to unload does not found"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    :try_start_1
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v7, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->packageName:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v7, "."

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v7, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->className:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    const/4 v3, 0x1

    iget-object v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    invoke-static {v2}, Ljava/lang/reflect/Proxy;->getInvocationHandler(Ljava/lang/Object;)Ljava/lang/reflect/InvocationHandler;

    move-result-object v2

    check-cast v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;

    iget-object v7, v0, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    iget-object v2, v2, Lcom/samsung/android/sdk/pen/plugin/framework/DoubleClassLoaderProxyBuilder$DoubleClassLoaderInvocationHandler;->instance:Ljava/lang/Object;

    invoke-virtual {v7, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    iget v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mJniPluginManager:Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;

    iget v2, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->nativeHandle:I

    invoke-virtual {v0, v2}, Lcom/samsung/android/sdk/pen/plugin/framework/JniPluginManager;->onUnload(I)V

    :goto_2
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mLoadedPluginList:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    const/4 v0, 0x0

    iput-object v0, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginObject;->object:Ljava/lang/Object;

    move v0, v3

    goto :goto_1

    :cond_5
    check-cast p1, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;

    invoke-interface {p1}, Lcom/samsung/android/sdk/pen/plugin/interfaces/SpenPluginInterface;->onUnload()V

    goto :goto_2

    :catchall_0
    move-exception v0

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_6
    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "unloadPlugin() is completed"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void

    :cond_7
    move v2, v3

    goto :goto_0
.end method

.method updateAPKPluginList(Landroid/content/Context;)V
    .locals 10

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string/jumbo v1, "com.samsung.android.sdk.pen.plugin.PICK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-nez v0, :cond_1

    return-void

    :cond_1
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mBuiltInPluginList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_4

    move v4, v2

    :goto_1
    if-nez v4, :cond_0

    iget-object v1, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_3
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_5

    move v1, v4

    :goto_2
    if-nez v1, :cond_0

    new-instance v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    invoke-direct {v4}, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;-><init>()V

    iget-object v1, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v1, v1, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const-string/jumbo v6, "\\."

    invoke-virtual {v1, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    new-instance v7, Ljava/lang/StringBuffer;

    invoke-direct {v7}, Ljava/lang/StringBuffer;-><init>()V

    move v1, v2

    :goto_3
    array-length v8, v6

    add-int/lit8 v8, v8, -0x1

    if-lt v1, v8, :cond_6

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->length()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-virtual {v7, v1}, Ljava/lang/StringBuffer;->deleteCharAt(I)Ljava/lang/StringBuffer;

    invoke-virtual {v7}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    array-length v1, v6

    add-int/lit8 v1, v1, -0x1

    aget-object v1, v6, v1

    iput-object v1, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    :try_start_0
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    if-nez v1, :cond_7

    const-string/jumbo v0, "PluginManager"

    const-string/jumbo v1, "PackageManager is null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0

    :cond_4
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v6, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v6, v6, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v6, v6, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    iget-object v8, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v8}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v4, v3

    goto/16 :goto_1

    :cond_5
    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;

    iget-object v7, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v7, v7, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v7, v7, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    iget-object v9, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->packageName:Ljava/lang/String;

    invoke-static {v9}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string/jumbo v9, "."

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    iget-object v1, v1, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->canonicalClassName:Ljava/lang/String;

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v7, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    move v1, v3

    goto/16 :goto_2

    :cond_6
    aget-object v8, v6, v1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const-string/jumbo v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_3

    :cond_7
    :try_start_1
    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    const/16 v6, 0x80

    invoke-virtual {v1, v0, v6}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string/jumbo v1, "SPEN_PLUGIN_INFO"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/16 v6, 0xe

    if-ne v1, v6, :cond_0

    aget-object v1, v0, v2

    iput-object v1, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->type:Ljava/lang/String;

    aget-object v1, v0, v3

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->version:I

    const/4 v1, 0x2

    aget-object v1, v0, v1

    iput-object v1, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceName:Ljava/lang/String;

    const/4 v1, 0x3

    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    iput v1, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->interfaceVersion:I

    const/4 v1, 0x4

    aget-object v1, v0, v1

    const/4 v6, 0x5

    aget-object v6, v0, v6

    invoke-static {v6}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v6

    iput-boolean v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->hasPrivateKey:Z

    const/4 v6, 0x6

    aget-object v6, v0, v6

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->iconImageUri:Ljava/lang/String;

    const/4 v6, 0x7

    aget-object v6, v0, v6

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->selectedIconImageUri:Ljava/lang/String;

    const/16 v6, 0x8

    aget-object v6, v0, v6

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->presetIconImageUri:Ljava/lang/String;

    const/16 v6, 0x9

    aget-object v6, v0, v6

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->focusedIconImageUri:Ljava/lang/String;

    const/16 v6, 0xa

    aget-object v6, v0, v6

    iput-object v6, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->extraInfo:Ljava/lang/String;

    const/16 v6, 0xb

    aget-object v0, v0, v6

    iput-object v0, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->pluginNameUri:Ljava/lang/String;

    const-string/jumbo v0, "native"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    iput v3, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    :goto_4
    iget-object v0, p0, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginManager;->mAPKPluginList:Ljava/util/List;

    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_8
    iput v2, v4, Lcom/samsung/android/sdk/pen/plugin/framework/SpenPluginInfo;->binaryType:I

    goto :goto_4
.end method
