.class public final Lcom/squareup/okhttp/o;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Ljava/lang/String;

.field final b:I

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "host == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p3, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "userAgent == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/squareup/okhttp/o;->a:Ljava/lang/String;

    iput p2, p0, Lcom/squareup/okhttp/o;->b:I

    iput-object p3, p0, Lcom/squareup/okhttp/o;->c:Ljava/lang/String;

    iput-object p4, p0, Lcom/squareup/okhttp/o;->d:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method a()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "CONNECT "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/o;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, " HTTP/1.1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method b()Lcom/squareup/okhttp/internal/http/aa;
    .locals 6

    new-instance v0, Lcom/squareup/okhttp/internal/http/ad;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/ad;-><init>()V

    new-instance v1, Ljava/net/URL;

    const-string/jumbo v2, "https"

    iget-object v3, p0, Lcom/squareup/okhttp/o;->a:Ljava/lang/String;

    iget v4, p0, Lcom/squareup/okhttp/o;->b:I

    const-string/jumbo v5, "/"

    invoke-direct {v1, v2, v3, v4, v5}, Ljava/net/URL;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/net/URL;)Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v1

    const-string/jumbo v2, "Host"

    iget v0, p0, Lcom/squareup/okhttp/o;->b:I

    const-string/jumbo v3, "https"

    invoke-static {v3}, Lfd;->a(Ljava/lang/String;)I

    move-result v3

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/o;->a:Ljava/lang/String;

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    const-string/jumbo v0, "User-Agent"

    iget-object v2, p0, Lcom/squareup/okhttp/o;->c:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    iget-object v0, p0, Lcom/squareup/okhttp/o;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    const-string/jumbo v0, "Proxy-Authorization"

    iget-object v2, p0, Lcom/squareup/okhttp/o;->d:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_0
    const-string/jumbo v0, "Proxy-Connection"

    const-string/jumbo v2, "Keep-Alive"

    invoke-virtual {v1, v0, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    return-object v0

    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/squareup/okhttp/o;->a:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v3, ":"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/squareup/okhttp/o;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
