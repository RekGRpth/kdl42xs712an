.class public final Lcom/squareup/okhttp/internal/http/w;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/squareup/okhttp/internal/http/au;


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/http/u;

.field private final b:Lcom/squareup/okhttp/internal/http/k;


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/internal/http/u;Lcom/squareup/okhttp/internal/http/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/internal/http/aa;)Lga;
    .locals 7

    const-wide/16 v5, -0x1

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/aa;)J

    move-result-wide v1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    iget-boolean v0, v0, Lcom/squareup/okhttp/internal/http/u;->c:Z

    if-eqz v0, :cond_2

    const-wide/32 v3, 0x7fffffff

    cmp-long v0, v1, v3

    if-lez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Use setFixedLengthStreamingMode() or setChunkedStreamingMode() for requests larger than 2 GiB."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    cmp-long v0, v1, v5

    if-eqz v0, :cond_1

    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/internal/http/w;->b(Lcom/squareup/okhttp/internal/http/aa;)V

    new-instance v0, Lcom/squareup/okhttp/internal/http/ap;

    long-to-int v1, v1

    invoke-direct {v0, v1}, Lcom/squareup/okhttp/internal/http/ap;-><init>(I)V

    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/squareup/okhttp/internal/http/ap;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/ap;-><init>()V

    goto :goto_0

    :cond_2
    const-string/jumbo v0, "chunked"

    const-string/jumbo v3, "Transfer-Encoding"

    invoke-virtual {p1, v3}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/internal/http/w;->b(Lcom/squareup/okhttp/internal/http/aa;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/k;->f()Lga;

    move-result-object v0

    goto :goto_0

    :cond_3
    cmp-long v0, v1, v5

    if-eqz v0, :cond_4

    invoke-virtual {p0, p1}, Lcom/squareup/okhttp/internal/http/w;->b(Lcom/squareup/okhttp/internal/http/aa;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/k;->a(J)Lga;

    move-result-object v0

    goto :goto_0

    :cond_4
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot stream a request body without chunked encoding or a known content length!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public a(Ljava/net/CacheRequest;)Lgb;
    .locals 4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->o()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    const-wide/16 v1, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/squareup/okhttp/internal/http/k;->a(Ljava/net/CacheRequest;J)Lgb;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "chunked"

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v1

    const-string/jumbo v2, "Transfer-Encoding"

    invoke-virtual {v1, v2}, Lcom/squareup/okhttp/internal/http/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0, p1, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Ljava/net/CacheRequest;Lcom/squareup/okhttp/internal/http/u;)Lgb;

    move-result-object v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/ag;)J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v2, p1, v0, v1}, Lcom/squareup/okhttp/internal/http/k;->a(Ljava/net/CacheRequest;J)Lgb;

    move-result-object v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/k;->a(Ljava/net/CacheRequest;)Lgb;

    move-result-object v0

    goto :goto_0
.end method

.method public a()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/k;->d()V

    return-void
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ap;)V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/ap;)V

    return-void
.end method

.method public b()Lcom/squareup/okhttp/internal/http/aj;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/k;->e()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    return-object v0
.end method

.method public b(Lcom/squareup/okhttp/internal/http/aa;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->b()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->k()Lcom/squareup/okhttp/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/m;->b()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/u;->k()Lcom/squareup/okhttp/c;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/c;->k()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/squareup/okhttp/internal/http/af;->a(Lcom/squareup/okhttp/internal/http/aa;Ljava/net/Proxy$Type;I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/aa;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)V

    return-void
.end method

.method public c()V
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/w;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/k;->a()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/k;->b()V

    goto :goto_0
.end method

.method public d()Z
    .locals 4

    const/4 v0, 0x0

    const-string/jumbo v1, "close"

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/u;->g()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v2

    const-string/jumbo v3, "Connection"

    invoke-virtual {v2, v3}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const-string/jumbo v1, "close"

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/w;->a:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v2

    const-string/jumbo v3, "Connection"

    invoke-virtual {v2, v3}, Lcom/squareup/okhttp/internal/http/ag;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/k;->c()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0
.end method

.method public e()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/w;->b:Lcom/squareup/okhttp/internal/http/k;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/k;->g()V

    return-void
.end method
