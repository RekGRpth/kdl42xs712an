.class public Lcom/squareup/okhttp/internal/http/ad;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Ljava/net/URL;

.field private b:Ljava/lang/String;

.field private c:Lcom/squareup/okhttp/internal/http/h;

.field private d:Lcom/squareup/okhttp/internal/http/ac;

.field private e:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string/jumbo v0, "GET"

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->b:Ljava/lang/String;

    new-instance v0, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->c:Lcom/squareup/okhttp/internal/http/h;

    return-void
.end method

.method private constructor <init>(Lcom/squareup/okhttp/internal/http/aa;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aa;->a(Lcom/squareup/okhttp/internal/http/aa;)Ljava/net/URL;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->a:Ljava/net/URL;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aa;->b(Lcom/squareup/okhttp/internal/http/aa;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->b:Ljava/lang/String;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aa;->c(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/ac;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->d:Lcom/squareup/okhttp/internal/http/ac;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aa;->d(Lcom/squareup/okhttp/internal/http/aa;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->e:Ljava/lang/Object;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aa;->e(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/f;->b()Lcom/squareup/okhttp/internal/http/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->c:Lcom/squareup/okhttp/internal/http/h;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ab;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/ad;-><init>(Lcom/squareup/okhttp/internal/http/aa;)V

    return-void
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/http/ad;)Ljava/net/URL;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->a:Ljava/net/URL;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/http/ad;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/http/ad;)Lcom/squareup/okhttp/internal/http/h;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->c:Lcom/squareup/okhttp/internal/http/h;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/http/ad;)Lcom/squareup/okhttp/internal/http/ac;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->d:Lcom/squareup/okhttp/internal/http/ac;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/http/ad;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->e:Ljava/lang/Object;

    return-object v0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/internal/http/aa;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->a:Ljava/net/URL;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/aa;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/aa;-><init>(Lcom/squareup/okhttp/internal/http/ad;Lcom/squareup/okhttp/internal/http/ab;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;
    .locals 1

    const-string/jumbo v0, "User-Agent"

    invoke-virtual {p0, v0, p1}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Lcom/squareup/okhttp/internal/http/ac;)Lcom/squareup/okhttp/internal/http/ad;
    .locals 2

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "method == null || method.length() == 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/ad;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/ad;->d:Lcom/squareup/okhttp/internal/http/ac;

    return-object p0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->c:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    return-object p0
.end method

.method public a(Ljava/net/URL;)Lcom/squareup/okhttp/internal/http/ad;
    .locals 2

    if-nez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "url == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/ad;->a:Ljava/net/URL;

    return-object p0
.end method

.method public b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ad;->c:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    return-object p0
.end method
