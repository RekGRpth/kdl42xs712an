.class public Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;
.super Ljava/net/HttpURLConnection;
.source "Twttr"


# instance fields
.field final a:Lcom/squareup/okhttp/j;

.field protected b:Ljava/io/IOException;

.field protected c:Lcom/squareup/okhttp/internal/http/u;

.field d:Lcom/squareup/okhttp/f;

.field private e:Lcom/squareup/okhttp/internal/http/h;

.field private f:J

.field private g:I

.field private h:Lcom/squareup/okhttp/m;


# direct methods
.method public constructor <init>(Ljava/net/URL;Lcom/squareup/okhttp/j;)V
    .locals 2

    invoke-direct {p0, p1}, Ljava/net/HttpURLConnection;-><init>(Ljava/net/URL;)V

    new-instance v0, Lcom/squareup/okhttp/internal/http/h;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/h;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->f:J

    iput-object p2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    return-void
.end method

.method private a(Ljava/lang/String;Lcom/squareup/okhttp/c;Lcom/squareup/okhttp/internal/http/ap;)Lcom/squareup/okhttp/internal/http/u;
    .locals 8

    const/4 v5, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/squareup/okhttp/internal/http/ad;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/ad;-><init>()V

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/net/URL;)Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v0

    invoke-virtual {v0, p1, v5}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Lcom/squareup/okhttp/internal/http/ac;)Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v3

    move v0, v1

    :goto_0
    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v4

    if-ge v0, v4, :cond_0

    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v0}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v4, v6}, Lcom/squareup/okhttp/internal/http/ad;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-wide v3, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->f:J

    const-wide/16 v6, -0x1

    cmp-long v0, v3, v6

    if-eqz v0, :cond_2

    const-string/jumbo v0, "Content-Length"

    iget-wide v3, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->f:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    move v3, v1

    :goto_1
    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->f()Lcom/squareup/okhttp/l;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getUseCaches()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->o()Lcom/squareup/okhttp/j;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/squareup/okhttp/j;->a(Lcom/squareup/okhttp/l;)Lcom/squareup/okhttp/j;

    move-result-object v1

    :cond_1
    new-instance v0, Lcom/squareup/okhttp/internal/http/u;

    move-object v4, p2

    move-object v6, p3

    invoke-direct/range {v0 .. v6}, Lcom/squareup/okhttp/internal/http/u;-><init>(Lcom/squareup/okhttp/j;Lcom/squareup/okhttp/internal/http/aa;ZLcom/squareup/okhttp/c;Lcom/squareup/okhttp/internal/http/aq;Lcom/squareup/okhttp/internal/http/ap;)V

    return-object v0

    :cond_2
    iget v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->chunkLength:I

    if-lez v0, :cond_3

    const-string/jumbo v0, "Transfer-Encoding"

    const-string/jumbo v3, "chunked"

    invoke-virtual {v2, v0, v3}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    move v3, v1

    goto :goto_1

    :cond_3
    const/4 v1, 0x1

    move v3, v1

    goto :goto_1
.end method

.method private a()V
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b:Ljava/io/IOException;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b:Ljava/io/IOException;

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    if-eqz v0, :cond_1

    :goto_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    :try_start_0
    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->doOutput:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string/jumbo v0, "POST"

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a(Ljava/lang/String;Lcom/squareup/okhttp/c;Lcom/squareup/okhttp/internal/http/ap;)Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b:Ljava/io/IOException;

    throw v0

    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/v;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " does not support writing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
.end method

.method private a(Ljava/lang/String;Z)V
    .locals 5

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :cond_0
    const-string/jumbo v0, ","

    const/4 v2, -0x1

    invoke-virtual {p1, v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;I)[Ljava/lang/String;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_1

    aget-object v4, v2, v0

    :try_start_0
    invoke-static {v4}, Lfi;->a(Ljava/lang/String;)Lfi;

    move-result-object v4

    invoke-static {v4}, Lfd;->a(Lfi;)Lcom/squareup/okhttp/Protocol;

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/j;->a(Ljava/util/List;)Lcom/squareup/okhttp/j;

    return-void
.end method

.method private a(Z)Z
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->a()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->l()Lcom/squareup/okhttp/m;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->h:Lcom/squareup/okhttp/m;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->k()Lcom/squareup/okhttp/c;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->k()Lcom/squareup/okhttp/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->i()Lcom/squareup/okhttp/f;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->d:Lcom/squareup/okhttp/f;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->q()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/internal/http/u;->a(Ljava/io/IOException;)Lcom/squareup/okhttp/internal/http/u;

    move-result-object v1

    if-eqz v1, :cond_2

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    const/4 v0, 0x0

    goto :goto_1

    :cond_2
    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b:Ljava/io/IOException;

    throw v0
.end method

.method private b()Lcom/squareup/okhttp/internal/http/u;
    .locals 5

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    :goto_0
    return-object v0

    :cond_0
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c()Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    move-result-object v2

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    if-ne v2, v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->m()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->d()Lga;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v3

    invoke-virtual {v3}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v3

    const/16 v4, 0x12c

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12d

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12e

    if-eq v3, v4, :cond_2

    const/16 v4, 0x12f

    if-ne v3, v4, :cond_3

    :cond_2
    const-string/jumbo v1, "GET"

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    const-string/jumbo v4, "Content-Length"

    invoke-virtual {v0, v4}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    const/4 v0, 0x0

    :cond_3
    if-eqz v0, :cond_4

    instance-of v4, v0, Lcom/squareup/okhttp/internal/http/ap;

    if-nez v4, :cond_4

    new-instance v0, Ljava/net/HttpRetryException;

    const-string/jumbo v1, "Cannot retry streamed HTTP body"

    invoke-direct {v0, v1, v3}, Ljava/net/HttpRetryException;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_4
    sget-object v3, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->c:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    if-ne v2, v3, :cond_5

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/u;->m()V

    :cond_5
    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/u;->n()Lcom/squareup/okhttp/c;

    move-result-object v2

    check-cast v0, Lcom/squareup/okhttp/internal/http/ap;

    invoke-direct {p0, v1, v2, v0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a(Ljava/lang/String;Lcom/squareup/okhttp/c;Lcom/squareup/okhttp/internal/http/ap;)Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    goto :goto_1
.end method

.method private c()Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;
    .locals 4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->k()Lcom/squareup/okhttp/c;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/m;->b()Ljava/net/Proxy;

    move-result-object v0

    :goto_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    :goto_1
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->c()Ljava/net/Proxy;

    move-result-object v0

    goto :goto_0

    :sswitch_0
    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v1

    sget-object v2, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v1, v2, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "Received HTTP_PROXY_AUTH (407) code while not using proxy"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    :sswitch_1
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v1}, Lcom/squareup/okhttp/j;->i()Lcom/squareup/okhttp/g;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/squareup/okhttp/internal/http/i;->a(Lcom/squareup/okhttp/g;Lcom/squareup/okhttp/internal/http/ag;Ljava/net/Proxy;)Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    if-nez v0, :cond_2

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :cond_2
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/f;->b()Lcom/squareup/okhttp/internal/http/h;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->b:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :sswitch_2
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getInstanceFollowRedirects()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto :goto_1

    :cond_3
    iget v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->g:I

    const/16 v2, 0x14

    if-le v0, v2, :cond_4

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Too many redirects: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    const/16 v0, 0x133

    if-ne v1, v0, :cond_5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const-string/jumbo v1, "GET"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    const-string/jumbo v1, "HEAD"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_5
    const-string/jumbo v0, "Location"

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_6

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_6
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1, v0}, Ljava/net/URL;-><init>(Ljava/net/URL;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "https"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v0}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v2, "http"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_7
    invoke-virtual {v1}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v2}, Ljava/net/URL;->getProtocol()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->k()Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->a:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_8
    invoke-virtual {v1}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v0

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v3}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    invoke-static {v1}, Lfd;->a(Ljava/net/URL;)I

    move-result v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-static {v1}, Lfd;->a(Ljava/net/URL;)I

    move-result v1

    if-ne v0, v1, :cond_9

    const/4 v0, 0x1

    :goto_2
    if-eqz v3, :cond_a

    if-eqz v0, :cond_a

    if-eqz v2, :cond_a

    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->b:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :cond_9
    const/4 v0, 0x0

    goto :goto_2

    :cond_a
    sget-object v0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;->c:Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl$Retry;

    goto/16 :goto_1

    :sswitch_data_0
    .sparse-switch
        0x12c -> :sswitch_2
        0x12d -> :sswitch_2
        0x12e -> :sswitch_2
        0x12f -> :sswitch_2
        0x133 -> :sswitch_2
        0x191 -> :sswitch_1
        0x197 -> :sswitch_0
    .end sparse-switch
.end method


# virtual methods
.method public final addRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot add request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    invoke-static {}, Ley;->a()Ley;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " because its value was null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ley;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const-string/jumbo v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/h;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    goto :goto_0
.end method

.method public final connect()V
    .locals 1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a()V

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    return-void
.end method

.method public final disconnect()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->n()Lcom/squareup/okhttp/c;

    :cond_0
    return-void
.end method

.method public getConnectTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->a()I

    move-result v0

    return v0
.end method

.method public final getErrorStream()Ljava/io/InputStream;
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/u;->o()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v2

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v2

    const/16 v3, 0x190

    if-lt v2, v3, :cond_0

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/u;->j()Ljava/io/InputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final getHeaderField(I)Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderField(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    if-nez p1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->b()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/f;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFieldKey(I)Ljava/lang/String;
    .locals 1

    :try_start_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final getHeaderFields()Ljava/util/Map;
    .locals 2

    :try_start_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/Map;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final getInputStream()Ljava/io/InputStream;
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->doInput:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "This protocol does not support input"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v1

    const/16 v2, 0x190

    if-lt v1, v2, :cond_1

    new-instance v0, Ljava/io/FileNotFoundException;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->url:Ljava/net/URL;

    invoke-virtual {v1}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->j()Ljava/io/InputStream;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "No response body exists; responseCode="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getResponseCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    return-object v0
.end method

.method public final getOutputStream()Ljava/io/OutputStream;
    .locals 3

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->connect()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->e()Lfg;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "method does not support a request body: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->c:Lcom/squareup/okhttp/internal/http/u;

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/u;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    const-string/jumbo v1, "cannot write request body after response has been read"

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-interface {v0}, Lfg;->d()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final getPermission()Ljava/security/Permission;
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getHost()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->getURL()Ljava/net/URL;

    move-result-object v0

    invoke-static {v0}, Lfd;->a(Ljava/net/URL;)I

    move-result v0

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->usingProxy()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->c()Ljava/net/Proxy;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Proxy;->address()Ljava/net/SocketAddress;

    move-result-object v0

    check-cast v0, Ljava/net/InetSocketAddress;

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getHostName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/net/InetSocketAddress;->getPort()I

    move-result v0

    :cond_0
    new-instance v2, Ljava/net/SocketPermission;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v3, ":"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "connect, resolve"

    invoke-direct {v2, v0, v1}, Ljava/net/SocketPermission;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v2
.end method

.method public getReadTimeout()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->b()I

    move-result v0

    return v0
.end method

.method public final getRequestProperties()Ljava/util/Map;
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot access request header fields after connection is set"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/y;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final getRequestProperty(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    if-nez p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/h;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final getResponseCode()I
    .locals 1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v0

    return v0
.end method

.method public getResponseMessage()Ljava/lang/String;
    .locals 1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->b()Lcom/squareup/okhttp/internal/http/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/u;->h()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setConnectTimeout(I)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    int-to-long v1, p1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/okhttp/j;->a(JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public setFixedLengthStreamingMode(I)V
    .locals 2

    int-to-long v0, p1

    invoke-virtual {p0, v0, v1}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->setFixedLengthStreamingMode(J)V

    return-void
.end method

.method public setFixedLengthStreamingMode(J)V
    .locals 2

    iget-boolean v0, p0, Ljava/net/HttpURLConnection;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->chunkLength:I

    if-lez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Already in chunked mode"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "contentLength < 0"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    iput-wide p1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->f:J

    const-wide/32 v0, 0x7fffffff

    invoke-static {p1, p2, v0, v1}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Ljava/net/HttpURLConnection;->fixedContentLength:I

    return-void
.end method

.method public setIfModifiedSince(J)V
    .locals 5

    invoke-super {p0, p1, p2}, Ljava/net/HttpURLConnection;->setIfModifiedSince(J)V

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->ifModifiedSince:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    const-string/jumbo v1, "If-Modified-Since"

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->ifModifiedSince:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/s;->a(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    const-string/jumbo v1, "If-Modified-Since"

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    goto :goto_0
.end method

.method public setReadTimeout(I)V
    .locals 4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    int-to-long v1, p1

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v1, v2, v3}, Lcom/squareup/okhttp/j;->b(JLjava/util/concurrent/TimeUnit;)V

    return-void
.end method

.method public setRequestMethod(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/squareup/okhttp/internal/http/v;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Expected one of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Lcom/squareup/okhttp/internal/http/v;->a:Ljava/util/Set;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " but was "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iput-object p1, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->method:Ljava/lang/String;

    return-void
.end method

.method public final setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->connected:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "Cannot set request property after connection is made"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    if-nez p1, :cond_1

    new-instance v0, Ljava/lang/NullPointerException;

    const-string/jumbo v1, "field == null"

    invoke-direct {v0, v1}, Ljava/lang/NullPointerException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    if-nez p2, :cond_2

    invoke-static {}, Ley;->a()Ley;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Ignoring header "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " because its value was null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ley;->a(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_2
    const-string/jumbo v0, "X-Android-Transports"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string/jumbo v0, "X-Android-Protocols"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->e:Lcom/squareup/okhttp/internal/http/h;

    invoke-virtual {v0, p1, p2}, Lcom/squareup/okhttp/internal/http/h;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/h;

    goto :goto_0
.end method

.method public final usingProxy()Z
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->h:Lcom/squareup/okhttp/m;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->h:Lcom/squareup/okhttp/m;

    invoke-virtual {v0}, Lcom/squareup/okhttp/m;->b()Ljava/net/Proxy;

    move-result-object v0

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->DIRECT:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/HttpURLConnectionImpl;->a:Lcom/squareup/okhttp/j;

    invoke-virtual {v0}, Lcom/squareup/okhttp/j;->c()Ljava/net/Proxy;

    move-result-object v0

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
