.class public abstract Lcom/squareup/okhttp/internal/http/ai;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private a:Lgb;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public abstract a()Ljava/io/InputStream;
.end method

.method public b()Lgb;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ai;->a:Lgb;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ai;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-static {v0}, Lfr;->a(Ljava/io/InputStream;)Lgb;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ai;->a:Lgb;

    goto :goto_0
.end method

.method public close()V
    .locals 1

    invoke-virtual {p0}, Lcom/squareup/okhttp/internal/http/ai;->a()Ljava/io/InputStream;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    return-void
.end method
