.class final Lcom/squareup/okhttp/internal/http/ap;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lga;


# instance fields
.field private a:Z

.field private final b:I

.field private final c:Lfo;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/http/ap;-><init>(I)V

    return-void
.end method

.method public constructor <init>(I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    iput p1, p0, Lcom/squareup/okhttp/internal/http/ap;->b:I

    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    return-void
.end method

.method public a(Lfg;)V
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v0}, Lfo;->p()Lfo;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v1}, Lfo;->l()J

    move-result-wide v1

    invoke-interface {p1, v0, v1, v2}, Lfg;->a(Lfo;J)V

    return-void
.end method

.method public a(Lfo;J)V
    .locals 6

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/ap;->a:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "closed"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Lfo;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    move-wide v4, p2

    invoke-static/range {v0 .. v5}, Lfd;->a(JJJ)V

    iget v0, p0, Lcom/squareup/okhttp/internal/http/ap;->b:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    iget v2, p0, Lcom/squareup/okhttp/internal/http/ap;->b:I

    int-to-long v2, v2

    sub-long/2addr v2, p2

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "exceeded content-length limit of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/ap;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v0, p1, p2, p3}, Lfo;->a(Lfo;J)V

    return-void
.end method

.method public b()J
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    return-wide v0
.end method

.method public close()V
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/http/ap;->a:Z

    if-eqz v0, :cond_1

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/http/ap;->a:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    iget v2, p0, Lcom/squareup/okhttp/internal/http/ap;->b:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    new-instance v0, Ljava/net/ProtocolException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "content-length promised "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/squareup/okhttp/internal/http/ap;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " bytes, but received "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/ap;->c:Lfo;

    invoke-virtual {v2}, Lfo;->l()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/net/ProtocolException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
