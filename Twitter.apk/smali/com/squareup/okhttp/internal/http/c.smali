.class public Lcom/squareup/okhttp/internal/http/c;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:J

.field final b:Lcom/squareup/okhttp/internal/http/aa;

.field final c:Lcom/squareup/okhttp/internal/http/ag;

.field private d:Ljava/util/Date;

.field private e:Ljava/lang/String;

.field private f:Ljava/util/Date;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Date;

.field private i:J

.field private j:J

.field private k:Ljava/lang/String;

.field private l:I


# direct methods
.method public constructor <init>(JLcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;)V
    .locals 4

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/http/c;->l:I

    iput-wide p1, p0, Lcom/squareup/okhttp/internal/http/c;->a:J

    iput-object p3, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    iput-object p4, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    if-eqz p4, :cond_7

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p4}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/internal/http/f;->a()I

    move-result v1

    if-ge v0, v1, :cond_7

    invoke-virtual {p4}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/squareup/okhttp/internal/http/f;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4}, Lcom/squareup/okhttp/internal/http/ag;->g()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/squareup/okhttp/internal/http/f;->b(I)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v3, "Date"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    iput-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->e:Ljava/lang/String;

    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const-string/jumbo v3, "Expires"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->h:Ljava/util/Date;

    goto :goto_1

    :cond_2
    const-string/jumbo v3, "Last-Modified"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/s;->a(Ljava/lang/String;)Ljava/util/Date;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->f:Ljava/util/Date;

    iput-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->g:Ljava/lang/String;

    goto :goto_1

    :cond_3
    const-string/jumbo v3, "ETag"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    iput-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->k:Ljava/lang/String;

    goto :goto_1

    :cond_4
    const-string/jumbo v3, "Age"

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_5

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/e;->a(Ljava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/squareup/okhttp/internal/http/c;->l:I

    goto :goto_1

    :cond_5
    sget-object v3, Lcom/squareup/okhttp/internal/http/y;->b:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/okhttp/internal/http/c;->i:J

    goto :goto_1

    :cond_6
    sget-object v3, Lcom/squareup/okhttp/internal/http/y;->c:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v2}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v1

    iput-wide v1, p0, Lcom/squareup/okhttp/internal/http/c;->j:J

    goto :goto_1

    :cond_7
    return-void
.end method

.method private static a(Lcom/squareup/okhttp/internal/http/aa;)Z
    .locals 1

    const-string/jumbo v0, "If-Modified-Since"

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "If-None-Match"

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/http/aa;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()Lcom/squareup/okhttp/internal/http/a;
    .locals 13

    const-wide/16 v4, 0x0

    const/4 v12, -0x1

    const/4 v11, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    if-nez v0, :cond_0

    new-instance v0, Lcom/squareup/okhttp/internal/http/a;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    sget-object v3, Lcom/squareup/okhttp/ResponseSource;->c:Lcom/squareup/okhttp/ResponseSource;

    invoke-direct {v0, v1, v2, v3, v11}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->f()Lcom/squareup/okhttp/f;

    move-result-object v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/squareup/okhttp/internal/http/a;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    sget-object v3, Lcom/squareup/okhttp/ResponseSource;->c:Lcom/squareup/okhttp/ResponseSource;

    invoke-direct {v0, v1, v2, v3, v11}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/http/a;->a(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/aa;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Lcom/squareup/okhttp/internal/http/a;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    sget-object v3, Lcom/squareup/okhttp/ResponseSource;->c:Lcom/squareup/okhttp/ResponseSource;

    invoke-direct {v0, v1, v2, v3, v11}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->j()Lcom/squareup/okhttp/b;

    move-result-object v6

    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->a()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/http/c;->a(Lcom/squareup/okhttp/internal/http/aa;)Z

    move-result v0

    if-eqz v0, :cond_4

    :cond_3
    new-instance v0, Lcom/squareup/okhttp/internal/http/a;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    sget-object v3, Lcom/squareup/okhttp/ResponseSource;->c:Lcom/squareup/okhttp/ResponseSource;

    invoke-direct {v0, v1, v2, v3, v11}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    goto :goto_0

    :cond_4
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/c;->d()J

    move-result-wide v7

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/c;->c()J

    move-result-wide v0

    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->c()I

    move-result v2

    if-eq v2, v12, :cond_5

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->c()I

    move-result v3

    int-to-long v9, v3

    invoke-virtual {v2, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    :cond_5
    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->h()I

    move-result v2

    if-eq v2, v12, :cond_e

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->h()I

    move-result v3

    int-to-long v9, v3

    invoke-virtual {v2, v9, v10}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    :goto_1
    iget-object v9, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v9}, Lcom/squareup/okhttp/internal/http/ag;->j()Lcom/squareup/okhttp/b;

    move-result-object v9

    invoke-virtual {v9}, Lcom/squareup/okhttp/b;->f()Z

    move-result v10

    if-nez v10, :cond_6

    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->g()I

    move-result v10

    if-eq v10, v12, :cond_6

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v6}, Lcom/squareup/okhttp/b;->g()I

    move-result v5

    int-to-long v5, v5

    invoke-virtual {v4, v5, v6}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    :cond_6
    invoke-virtual {v9}, Lcom/squareup/okhttp/b;->a()Z

    move-result v6

    if-nez v6, :cond_9

    add-long v9, v7, v2

    add-long/2addr v4, v0

    cmp-long v4, v9, v4

    if-gez v4, :cond_9

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v4}, Lcom/squareup/okhttp/internal/http/ag;->i()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v4

    sget-object v5, Lcom/squareup/okhttp/ResponseSource;->a:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v4, v5}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/ResponseSource;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v4

    add-long/2addr v2, v7

    cmp-long v0, v2, v0

    if-ltz v0, :cond_7

    const-string/jumbo v0, "Warning"

    const-string/jumbo v1, "110 HttpURLConnection \"Response is stale\""

    invoke-virtual {v4, v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    :cond_7
    const-wide/32 v0, 0x5265c00

    cmp-long v0, v7, v0

    if-lez v0, :cond_8

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/c;->e()Z

    move-result v0

    if-eqz v0, :cond_8

    const-string/jumbo v0, "Warning"

    const-string/jumbo v1, "113 HttpURLConnection \"Heuristic expiration\""

    invoke-virtual {v4, v0, v1}, Lcom/squareup/okhttp/internal/http/aj;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/aj;

    :cond_8
    new-instance v0, Lcom/squareup/okhttp/internal/http/a;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v4}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v2

    sget-object v3, Lcom/squareup/okhttp/ResponseSource;->a:Lcom/squareup/okhttp/ResponseSource;

    invoke-direct {v0, v1, v2, v3, v11}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    goto/16 :goto_0

    :cond_9
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->f()Lcom/squareup/okhttp/internal/http/ad;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->f:Ljava/util/Date;

    if-eqz v1, :cond_c

    const-string/jumbo v1, "If-Modified-Since"

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_a
    :goto_2
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->k:Ljava/lang/String;

    if-eqz v1, :cond_b

    const-string/jumbo v1, "If-None-Match"

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->k:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    :cond_b
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ad;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v2

    invoke-static {v2}, Lcom/squareup/okhttp/internal/http/c;->a(Lcom/squareup/okhttp/internal/http/aa;)Z

    move-result v0

    if-eqz v0, :cond_d

    sget-object v0, Lcom/squareup/okhttp/ResponseSource;->b:Lcom/squareup/okhttp/ResponseSource;

    :goto_3
    new-instance v1, Lcom/squareup/okhttp/internal/http/a;

    iget-object v3, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-direct {v1, v2, v3, v0, v11}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    move-object v0, v1

    goto/16 :goto_0

    :cond_c
    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    if-eqz v1, :cond_a

    const-string/jumbo v1, "If-Modified-Since"

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ad;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/squareup/okhttp/internal/http/ad;

    goto :goto_2

    :cond_d
    sget-object v0, Lcom/squareup/okhttp/ResponseSource;->c:Lcom/squareup/okhttp/ResponseSource;

    goto :goto_3

    :cond_e
    move-wide v2, v4

    goto/16 :goto_1
.end method

.method private c()J
    .locals 6

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->j()Lcom/squareup/okhttp/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/b;->c()I

    move-result v1

    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0}, Lcom/squareup/okhttp/b;->c()I

    move-result v0

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    :cond_0
    :goto_0
    return-wide v2

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->h:Ljava/util/Date;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_1
    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/c;->h:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long v0, v4, v0

    cmp-long v4, v0, v2

    if-lez v4, :cond_3

    :goto_2
    move-wide v2, v0

    goto :goto_0

    :cond_2
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/c;->j:J

    goto :goto_1

    :cond_3
    move-wide v0, v2

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->f:Ljava/util/Date;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->a()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->a()Ljava/net/URL;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URL;->getQuery()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    invoke-virtual {v0}, Ljava/util/Date;->getTime()J

    move-result-wide v0

    :goto_3
    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/c;->f:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v0, v4

    cmp-long v4, v0, v2

    if-lez v4, :cond_0

    const-wide/16 v2, 0xa

    div-long v2, v0, v2

    goto :goto_0

    :cond_5
    iget-wide v0, p0, Lcom/squareup/okhttp/internal/http/c;->i:J

    goto :goto_3
.end method

.method private d()J
    .locals 8

    const-wide/16 v0, 0x0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    if-eqz v2, :cond_0

    iget-wide v2, p0, Lcom/squareup/okhttp/internal/http/c;->j:J

    iget-object v4, p0, Lcom/squareup/okhttp/internal/http/c;->d:Ljava/util/Date;

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    sub-long/2addr v2, v4

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_0
    iget v2, p0, Lcom/squareup/okhttp/internal/http/c;->l:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_1

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v3, p0, Lcom/squareup/okhttp/internal/http/c;->l:I

    int-to-long v3, v3

    invoke-virtual {v2, v3, v4}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    :cond_1
    iget-wide v2, p0, Lcom/squareup/okhttp/internal/http/c;->j:J

    iget-wide v4, p0, Lcom/squareup/okhttp/internal/http/c;->i:J

    sub-long/2addr v2, v4

    iget-wide v4, p0, Lcom/squareup/okhttp/internal/http/c;->a:J

    iget-wide v6, p0, Lcom/squareup/okhttp/internal/http/c;->j:J

    sub-long/2addr v4, v6

    add-long/2addr v0, v2

    add-long/2addr v0, v4

    return-wide v0
.end method

.method private e()Z
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->c:Lcom/squareup/okhttp/internal/http/ag;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->j()Lcom/squareup/okhttp/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/b;->c()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->h:Ljava/util/Date;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/internal/http/a;
    .locals 5

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/c;->b()Lcom/squareup/okhttp/internal/http/a;

    move-result-object v1

    iget-object v0, v1, Lcom/squareup/okhttp/internal/http/a;->c:Lcom/squareup/okhttp/ResponseSource;

    sget-object v2, Lcom/squareup/okhttp/ResponseSource;->a:Lcom/squareup/okhttp/ResponseSource;

    if-eq v0, v2, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/c;->b:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->j()Lcom/squareup/okhttp/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/b;->i()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/okhttp/internal/http/aj;

    invoke-direct {v0}, Lcom/squareup/okhttp/internal/http/aj;-><init>()V

    iget-object v2, v1, Lcom/squareup/okhttp/internal/http/a;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-static {}, Lcom/squareup/okhttp/internal/http/a;->b()Lcom/squareup/okhttp/internal/http/at;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/at;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    sget-object v2, Lcom/squareup/okhttp/ResponseSource;->d:Lcom/squareup/okhttp/ResponseSource;

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/ResponseSource;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-static {}, Lcom/squareup/okhttp/internal/http/a;->a()Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/ai;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v2

    new-instance v0, Lcom/squareup/okhttp/internal/http/a;

    iget-object v1, v1, Lcom/squareup/okhttp/internal/http/a;->a:Lcom/squareup/okhttp/internal/http/aa;

    sget-object v3, Lcom/squareup/okhttp/ResponseSource;->d:Lcom/squareup/okhttp/ResponseSource;

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/squareup/okhttp/internal/http/a;-><init>(Lcom/squareup/okhttp/internal/http/aa;Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/ResponseSource;Lcom/squareup/okhttp/internal/http/b;)V

    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
