.class public final Lcom/squareup/okhttp/internal/http/ag;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private final a:Lcom/squareup/okhttp/internal/http/aa;

.field private final b:Lcom/squareup/okhttp/internal/http/at;

.field private final c:Lcom/squareup/okhttp/f;

.field private final d:Lcom/squareup/okhttp/internal/http/f;

.field private final e:Lcom/squareup/okhttp/internal/http/ai;

.field private final f:Lcom/squareup/okhttp/internal/http/ag;

.field private volatile g:Lcom/squareup/okhttp/internal/http/ak;

.field private volatile h:Lcom/squareup/okhttp/b;


# direct methods
.method private constructor <init>(Lcom/squareup/okhttp/internal/http/aj;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->a:Lcom/squareup/okhttp/internal/http/aa;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aj;->b(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/at;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aj;->c(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->c:Lcom/squareup/okhttp/f;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aj;->d(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/h;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/h;->a()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->d:Lcom/squareup/okhttp/internal/http/f;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aj;->e(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/ai;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->e:Lcom/squareup/okhttp/internal/http/ai;

    invoke-static {p1}, Lcom/squareup/okhttp/internal/http/aj;->f(Lcom/squareup/okhttp/internal/http/aj;)Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->f:Lcom/squareup/okhttp/internal/http/ag;

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/http/aj;Lcom/squareup/okhttp/internal/http/ah;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/http/ag;-><init>(Lcom/squareup/okhttp/internal/http/aj;)V

    return-void
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/aa;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->a:Lcom/squareup/okhttp/internal/http/aa;

    return-object v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/at;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->b:Lcom/squareup/okhttp/internal/http/at;

    return-object v0
.end method

.method static synthetic d(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->c:Lcom/squareup/okhttp/f;

    return-object v0
.end method

.method static synthetic e(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->d:Lcom/squareup/okhttp/internal/http/f;

    return-object v0
.end method

.method static synthetic f(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/ai;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->e:Lcom/squareup/okhttp/internal/http/ai;

    return-object v0
.end method

.method static synthetic g(Lcom/squareup/okhttp/internal/http/ag;)Lcom/squareup/okhttp/internal/http/ag;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->f:Lcom/squareup/okhttp/internal/http/ag;

    return-object v0
.end method

.method private k()Lcom/squareup/okhttp/internal/http/ak;
    .locals 3

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->g:Lcom/squareup/okhttp/internal/http/ak;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/ak;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/http/ag;->d:Lcom/squareup/okhttp/internal/http/f;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/squareup/okhttp/internal/http/ak;-><init>(Lcom/squareup/okhttp/internal/http/f;Lcom/squareup/okhttp/internal/http/ah;)V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->g:Lcom/squareup/okhttp/internal/http/ak;

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/squareup/okhttp/internal/http/aa;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->a:Lcom/squareup/okhttp/internal/http/aa;

    return-object v0
.end method

.method public a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/squareup/okhttp/internal/http/ag;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->d:Lcom/squareup/okhttp/internal/http/f;

    invoke-virtual {v0, p1}, Lcom/squareup/okhttp/internal/http/f;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    move-object p2, v0

    :cond_0
    return-object p2
.end method

.method public a(Lcom/squareup/okhttp/internal/http/ag;)Z
    .locals 5

    const/4 v0, 0x1

    invoke-virtual {p1}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v1

    const/16 v2, 0x130

    if-ne v1, v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-direct {p1}, Lcom/squareup/okhttp/internal/http/ag;->k()Lcom/squareup/okhttp/internal/http/ak;

    move-result-object v1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/ag;->k()Lcom/squareup/okhttp/internal/http/ak;

    move-result-object v2

    iget-object v2, v2, Lcom/squareup/okhttp/internal/http/ak;->a:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v2, v1, Lcom/squareup/okhttp/internal/http/ak;->a:Ljava/util/Date;

    if-eqz v2, :cond_2

    iget-object v1, v1, Lcom/squareup/okhttp/internal/http/ak;->a:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/http/ag;->k()Lcom/squareup/okhttp/internal/http/ak;

    move-result-object v3

    iget-object v3, v3, Lcom/squareup/okhttp/internal/http/ak;->a:Ljava/util/Date;

    invoke-virtual {v3}, Ljava/util/Date;->getTime()J

    move-result-wide v3

    cmp-long v1, v1, v3

    if-ltz v1, :cond_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/at;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public c()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/at;->c()I

    move-result v0

    return v0
.end method

.method public d()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/at;->d()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public e()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->b:Lcom/squareup/okhttp/internal/http/at;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/at;->b()I

    move-result v0

    return v0
.end method

.method public f()Lcom/squareup/okhttp/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->c:Lcom/squareup/okhttp/f;

    return-object v0
.end method

.method public g()Lcom/squareup/okhttp/internal/http/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->d:Lcom/squareup/okhttp/internal/http/f;

    return-object v0
.end method

.method public h()Lcom/squareup/okhttp/internal/http/ai;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->e:Lcom/squareup/okhttp/internal/http/ai;

    return-object v0
.end method

.method public i()Lcom/squareup/okhttp/internal/http/aj;
    .locals 2

    new-instance v0, Lcom/squareup/okhttp/internal/http/aj;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/squareup/okhttp/internal/http/aj;-><init>(Lcom/squareup/okhttp/internal/http/ag;Lcom/squareup/okhttp/internal/http/ah;)V

    return-object v0
.end method

.method public j()Lcom/squareup/okhttp/b;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->h:Lcom/squareup/okhttp/b;

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->d:Lcom/squareup/okhttp/internal/http/f;

    invoke-static {v0}, Lcom/squareup/okhttp/b;->a(Lcom/squareup/okhttp/internal/http/f;)Lcom/squareup/okhttp/b;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/http/ag;->h:Lcom/squareup/okhttp/b;

    goto :goto_0
.end method
