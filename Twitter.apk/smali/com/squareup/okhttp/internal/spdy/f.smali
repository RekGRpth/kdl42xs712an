.class final Lcom/squareup/okhttp/internal/spdy/f;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field a:[Lcom/squareup/okhttp/internal/spdy/d;

.field b:I

.field c:I

.field d:Ler;

.field e:J

.field f:I

.field private final g:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

.field private final h:Ljava/util/List;

.field private final i:Lfh;

.field private j:I


# direct methods
.method constructor <init>(ZILgb;)V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    const/16 v0, 0x8

    new-array v0, v0, [Lcom/squareup/okhttp/internal/spdy/d;

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    new-instance v0, Let;

    invoke-direct {v0}, Let;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->e:J

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    if-eqz p1, :cond_0

    sget-object v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->b:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->g:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    iput p2, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    invoke-static {p3}, Lfr;->a(Lgb;)Lfh;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->i:Lfh;

    return-void

    :cond_0
    sget-object v0, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    goto :goto_0
.end method

.method private a(ILcom/squareup/okhttp/internal/spdy/d;)V
    .locals 6

    const/4 v3, -0x1

    iget v0, p2, Lcom/squareup/okhttp/internal/spdy/d;->j:I

    if-eq p1, v3, :cond_4

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->d(I)I

    move-result v2

    aget-object v1, v1, v2

    iget v1, v1, Lcom/squareup/okhttp/internal/spdy/d;->j:I

    sub-int/2addr v0, v1

    move v1, v0

    :goto_0
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    if-le v1, v0, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->d()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :goto_1
    return-void

    :cond_0
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    add-int/2addr v0, v1

    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    sub-int/2addr v0, v2

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->b(I)I

    move-result v0

    if-ne p1, v3, :cond_3

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    add-int/lit8 v0, v0, 0x1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v2, v2

    if-le v0, v2, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x2

    new-array v2, v0, [Lcom/squareup/okhttp/internal/spdy/d;

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v4, v4

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v5, v5

    invoke-static {v0, v3, v2, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v0, v2

    const/16 v3, 0x40

    if-ne v0, v3, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    check-cast v0, Let;

    invoke-virtual {v0}, Let;->b()Ler;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v3, v3

    invoke-interface {v0, v3}, Ler;->d(I)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    iput-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    :cond_2
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    invoke-interface {v2, v0}, Ler;->a(I)V

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    aput-object p2, v2, v0

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    :goto_2
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    goto :goto_1

    :cond_3
    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->d(I)I

    move-result v2

    add-int/2addr v0, v2

    add-int/2addr v0, p1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    invoke-interface {v2, v0}, Ler;->a(I)V

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    aput-object p2, v2, v0

    goto :goto_2

    :cond_4
    move v1, v0

    goto/16 :goto_0
.end method

.method private b(I)I
    .locals 6

    const/4 v1, 0x0

    if-lez p1, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    if-lt v0, v2, :cond_0

    if-lez p1, :cond_0

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    aget-object v2, v2, v0

    iget v2, v2, Lcom/squareup/okhttp/internal/spdy/d;->j:I

    sub-int/2addr p1, v2

    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/squareup/okhttp/internal/spdy/d;->j:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    invoke-interface {v0, v1}, Ler;->d(I)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    iget v4, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v1

    iget v5, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    invoke-static {v0, v2, v3, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    add-int/2addr v0, v1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    :cond_1
    return v1
.end method

.method private c(I)V
    .locals 5

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->h(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->e:J

    const-wide/16 v2, 0x1

    iget v4, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    sub-int v4, p1, v4

    shl-long/2addr v2, v4

    or-long/2addr v0, v2

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->e:J

    :goto_0
    return-void

    :cond_0
    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/e;->a()[Lcom/squareup/okhttp/internal/spdy/d;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    const/4 v1, -0x1

    invoke-direct {p0, v1, v0}, Lcom/squareup/okhttp/internal/spdy/f;->a(ILcom/squareup/okhttp/internal/spdy/d;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->d(I)I

    move-result v1

    invoke-interface {v0, v1}, Ler;->b(I)V

    goto :goto_0
.end method

.method private d(I)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v0, p1

    return v0
.end method

.method private d()V
    .locals 3

    const/4 v2, 0x0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->e()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    iput v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    return-void
.end method

.method private e()V
    .locals 2

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->e:J

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    invoke-interface {v0}, Ler;->a()V

    return-void
.end method

.method private e(I)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->g(I)Lfi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/internal/spdy/f;->a(Z)Lfi;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    new-instance v3, Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Lfi;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private f()V
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->a(Z)Lfi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/internal/spdy/f;->a(Z)Lfi;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    new-instance v3, Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Lfi;)V

    invoke-interface {v2, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method private f(I)V
    .locals 4

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->g(I)Lfi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/internal/spdy/f;->a(Z)Lfi;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Lfi;)V

    invoke-direct {p0, v2, v3}, Lcom/squareup/okhttp/internal/spdy/f;->a(ILcom/squareup/okhttp/internal/spdy/d;)V

    return-void
.end method

.method private g(I)Lfi;
    .locals 2

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->h(I)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/e;->a()[Lcom/squareup/okhttp/internal/spdy/d;

    move-result-object v0

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    sub-int v1, p1, v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/internal/spdy/f;->d(I)I

    move-result v1

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/squareup/okhttp/internal/spdy/d;->h:Lfi;

    goto :goto_0
.end method

.method private g()V
    .locals 4

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->a(Z)Lfi;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/squareup/okhttp/internal/spdy/f;->a(Z)Lfi;

    move-result-object v1

    const/4 v2, -0x1

    new-instance v3, Lcom/squareup/okhttp/internal/spdy/d;

    invoke-direct {v3, v0, v1}, Lcom/squareup/okhttp/internal/spdy/d;-><init>(Lfi;Lfi;)V

    invoke-direct {p0, v2, v3}, Lcom/squareup/okhttp/internal/spdy/f;->a(ILcom/squareup/okhttp/internal/spdy/d;)V

    return-void
.end method

.method private h()I
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->i:Lfh;

    invoke-interface {v0}, Lfh;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private h(I)Z
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->c:I

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method a(II)I
    .locals 3

    and-int v0, p1, p2

    if-ge v0, p2, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    :goto_1
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->h()I

    move-result v1

    and-int/lit16 v2, v1, 0x80

    if-eqz v2, :cond_1

    and-int/lit8 v1, v1, 0x7f

    shl-int/2addr v1, v0

    add-int/2addr p2, v1

    add-int/lit8 v0, v0, 0x7

    goto :goto_1

    :cond_1
    shl-int v0, v1, v0

    add-int/2addr v0, p2

    goto :goto_0
.end method

.method a(Z)Lfi;
    .locals 5

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->h()I

    move-result v1

    and-int/lit16 v0, v1, 0x80

    const/16 v2, 0x80

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    const/16 v2, 0x7f

    invoke-virtual {p0, v1, v2}, Lcom/squareup/okhttp/internal/spdy/f;->a(II)I

    move-result v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->i:Lfh;

    int-to-long v3, v1

    invoke-interface {v2, v3, v4}, Lfh;->c(J)Lfi;

    move-result-object v1

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->g:Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/Huffman$Codec;->a(Lfi;)Lfi;

    move-result-object v0

    :goto_1
    if-eqz p1, :cond_0

    invoke-virtual {v0}, Lfi;->d()Lfi;

    move-result-object v0

    :cond_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :cond_2
    move-object v0, v1

    goto :goto_1
.end method

.method a()V
    .locals 5

    const/16 v4, 0x80

    const/16 v3, 0x40

    const/16 v2, 0x3f

    :goto_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->i:Lfh;

    invoke-interface {v0}, Lfh;->e()Z

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->i:Lfh;

    invoke-interface {v0}, Lfh;->f()B

    move-result v0

    and-int/lit16 v0, v0, 0xff

    if-ne v0, v4, :cond_0

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->e()V

    goto :goto_0

    :cond_0
    and-int/lit16 v1, v0, 0x80

    if-ne v1, v4, :cond_1

    const/16 v1, 0x7f

    invoke-virtual {p0, v0, v1}, Lcom/squareup/okhttp/internal/spdy/f;->a(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->c(I)V

    goto :goto_0

    :cond_1
    if-ne v0, v3, :cond_2

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->f()V

    goto :goto_0

    :cond_2
    and-int/lit8 v1, v0, 0x40

    if-ne v1, v3, :cond_3

    invoke-virtual {p0, v0, v2}, Lcom/squareup/okhttp/internal/spdy/f;->a(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->e(I)V

    goto :goto_0

    :cond_3
    if-nez v0, :cond_4

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->g()V

    goto :goto_0

    :cond_4
    and-int/lit16 v1, v0, 0xc0

    if-nez v1, :cond_5

    invoke-virtual {p0, v0, v2}, Lcom/squareup/okhttp/internal/spdy/f;->a(II)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->f(I)V

    goto :goto_0

    :cond_5
    new-instance v1, Ljava/lang/AssertionError;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "unhandled byte: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toBinaryString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    :cond_6
    return-void
.end method

.method a(I)V
    .locals 2

    iput p1, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/f;->d()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->f:I

    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->j:I

    sub-int/2addr v0, v1

    invoke-direct {p0, v0}, Lcom/squareup/okhttp/internal/spdy/f;->b(I)I

    goto :goto_0
.end method

.method b()V
    .locals 5

    const-wide/16 v3, 0x1

    const/4 v0, 0x0

    :goto_0
    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/e;->a()[Lcom/squareup/okhttp/internal/spdy/d;

    move-result-object v1

    array-length v1, v1

    if-ge v0, v1, :cond_1

    iget-wide v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->e:J

    shr-long/2addr v1, v0

    and-long/2addr v1, v3

    cmp-long v1, v1, v3

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    invoke-static {}, Lcom/squareup/okhttp/internal/spdy/e;->a()[Lcom/squareup/okhttp/internal/spdy/d;

    move-result-object v2

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    array-length v0, v0

    add-int/lit8 v0, v0, -0x1

    :goto_1
    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->b:I

    if-eq v0, v1, :cond_3

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->d:Ler;

    invoke-interface {v1, v0}, Ler;->c(I)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/f;->a:[Lcom/squareup/okhttp/internal/spdy/d;

    aget-object v2, v2, v0

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    :cond_3
    return-void
.end method

.method c()Ljava/util/List;
    .locals 2

    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/f;->h:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    return-object v0
.end method
