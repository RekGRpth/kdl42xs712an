.class final Lcom/squareup/okhttp/internal/spdy/i;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lgb;


# instance fields
.field a:I

.field b:B

.field c:I

.field d:I

.field private final e:Lfh;


# direct methods
.method public constructor <init>(Lfh;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/i;->e:Lfh;

    return-void
.end method

.method private a()V
    .locals 5

    const/4 v4, 0x0

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/i;->c:I

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/i;->e:Lfh;

    invoke-interface {v1}, Lfh;->i()I

    move-result v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/i;->e:Lfh;

    invoke-interface {v2}, Lfh;->i()I

    move-result v2

    const/high16 v3, 0x3fff0000    # 1.9921875f

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x10

    int-to-short v3, v3

    iput v3, p0, Lcom/squareup/okhttp/internal/spdy/i;->d:I

    iput v3, p0, Lcom/squareup/okhttp/internal/spdy/i;->a:I

    const v3, 0xff00

    and-int/2addr v3, v1

    shr-int/lit8 v3, v3, 0x8

    int-to-byte v3, v3

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    iput-byte v1, p0, Lcom/squareup/okhttp/internal/spdy/i;->b:B

    const v1, 0x7fffffff

    and-int/2addr v1, v2

    iput v1, p0, Lcom/squareup/okhttp/internal/spdy/i;->c:I

    const/16 v1, 0xa

    if-eq v3, v1, :cond_0

    const-string/jumbo v0, "%s != TYPE_CONTINUATION"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v2

    aput-object v2, v1, v4

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_0
    iget v1, p0, Lcom/squareup/okhttp/internal/spdy/i;->c:I

    if-eq v1, v0, :cond_1

    const-string/jumbo v0, "TYPE_CONTINUATION streamId changed"

    new-array v1, v4, [Ljava/lang/Object;

    invoke-static {v0, v1}, Lcom/squareup/okhttp/internal/spdy/h;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/IOException;

    move-result-object v0

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public b(Lfo;J)J
    .locals 5

    const-wide/16 v0, -0x1

    :goto_0
    iget v2, p0, Lcom/squareup/okhttp/internal/spdy/i;->d:I

    if-nez v2, :cond_2

    iget-byte v2, p0, Lcom/squareup/okhttp/internal/spdy/i;->b:B

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_1

    :cond_0
    :goto_1
    return-wide v0

    :cond_1
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/i;->a()V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/i;->e:Lfh;

    iget v3, p0, Lcom/squareup/okhttp/internal/spdy/i;->d:I

    int-to-long v3, v3

    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-interface {v2, p1, v3, v4}, Lfh;->b(Lfo;J)J

    move-result-wide v2

    cmp-long v4, v2, v0

    if-eqz v4, :cond_0

    iget v0, p0, Lcom/squareup/okhttp/internal/spdy/i;->d:I

    int-to-long v0, v0

    sub-long/2addr v0, v2

    long-to-int v0, v0

    iput v0, p0, Lcom/squareup/okhttp/internal/spdy/i;->d:I

    move-wide v0, v2

    goto :goto_1
.end method

.method public close()V
    .locals 0

    return-void
.end method
