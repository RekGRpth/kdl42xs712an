.class final Lcom/squareup/okhttp/internal/spdy/an;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lgb;


# static fields
.field static final synthetic a:Z


# instance fields
.field final synthetic b:Lcom/squareup/okhttp/internal/spdy/ak;

.field private final c:Lfo;

.field private final d:Lfo;

.field private final e:J

.field private f:Z

.field private g:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lcom/squareup/okhttp/internal/spdy/an;->a:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private constructor <init>(Lcom/squareup/okhttp/internal/spdy/ak;J)V
    .locals 1

    iput-object p1, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->c:Lfo;

    new-instance v0, Lfo;

    invoke-direct {v0}, Lfo;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    iput-wide p2, p0, Lcom/squareup/okhttp/internal/spdy/an;->e:J

    return-void
.end method

.method synthetic constructor <init>(Lcom/squareup/okhttp/internal/spdy/ak;JLcom/squareup/okhttp/internal/spdy/al;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/squareup/okhttp/internal/spdy/an;-><init>(Lcom/squareup/okhttp/internal/spdy/ak;J)V

    return-void
.end method

.method private a()V
    .locals 10

    const-wide/32 v4, 0xf4240

    const-wide/16 v2, 0x0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->c(Lcom/squareup/okhttp/internal/spdy/ak;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_3

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    div-long v4, v0, v4

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->c(Lcom/squareup/okhttp/internal/spdy/ak;)J

    move-result-wide v0

    :goto_0
    :try_start_0
    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    invoke-virtual {v6}, Lfo;->l()J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->g:Z

    if-nez v6, :cond_2

    iget-boolean v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->f:Z

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v6}, Lcom/squareup/okhttp/internal/spdy/ak;->d(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    move-result-object v6

    if-nez v6, :cond_2

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v6}, Lcom/squareup/okhttp/internal/spdy/ak;->c(Lcom/squareup/okhttp/internal/spdy/ak;)J

    move-result-wide v6

    cmp-long v6, v6, v2

    if-nez v6, :cond_0

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v6}, Ljava/lang/Object;->wait()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Ljava/io/InterruptedIOException;

    invoke-direct {v0}, Ljava/io/InterruptedIOException;-><init>()V

    throw v0

    :cond_0
    cmp-long v6, v0, v2

    if-lez v6, :cond_1

    :try_start_1
    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v6, v0, v1}, Ljava/lang/Object;->wait(J)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->c(Lcom/squareup/okhttp/internal/spdy/ak;)J

    move-result-wide v0

    add-long/2addr v0, v4

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    const-wide/32 v8, 0xf4240

    div-long/2addr v6, v8

    sub-long/2addr v0, v6

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/net/SocketTimeoutException;

    const-string/jumbo v1, "Read timed out"

    invoke-direct {v0, v1}, Ljava/net/SocketTimeoutException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :cond_2
    return-void

    :cond_3
    move-wide v0, v2

    move-wide v4, v2

    goto :goto_0
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/an;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->g:Z

    return v0
.end method

.method static synthetic a(Lcom/squareup/okhttp/internal/spdy/an;Z)Z
    .locals 0

    iput-boolean p1, p0, Lcom/squareup/okhttp/internal/spdy/an;->g:Z

    return p1
.end method

.method private b()V
    .locals 3

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->f:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "stream closed"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->d(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "stream was reset: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/ak;->d(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method

.method static synthetic b(Lcom/squareup/okhttp/internal/spdy/an;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->f:Z

    return v0
.end method


# virtual methods
.method a(Lfh;J)V
    .locals 11

    const-wide/16 v9, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    sget-boolean v0, Lcom/squareup/okhttp/internal/spdy/an;->a:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    sub-long/2addr p2, v3

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v3

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v4

    cmp-long v0, v4, v9

    if-nez v0, :cond_7

    move v0, v1

    :goto_0
    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/an;->c:Lfo;

    iget-object v6, p0, Lcom/squareup/okhttp/internal/spdy/an;->c:Lfo;

    invoke-virtual {v6}, Lfo;->l()J

    move-result-wide v6

    invoke-virtual {v4, v5, v6, v7}, Lfo;->a(Lfo;J)V

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :cond_2
    cmp-long v0, p2, v9

    if-lez v0, :cond_3

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v3

    :try_start_1
    iget-boolean v4, p0, Lcom/squareup/okhttp/internal/spdy/an;->g:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v5

    add-long/2addr v5, p2

    iget-wide v7, p0, Lcom/squareup/okhttp/internal/spdy/an;->e:J

    cmp-long v0, v5, v7

    if-lez v0, :cond_4

    move v0, v1

    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_5

    invoke-interface {p1, p2, p3}, Lfh;->b(J)V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    sget-object v1, Lcom/squareup/okhttp/internal/spdy/ErrorCode;->h:Lcom/squareup/okhttp/internal/spdy/ErrorCode;

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ErrorCode;)V

    :cond_3
    :goto_2
    return-void

    :cond_4
    move v0, v2

    goto :goto_1

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :cond_5
    if-eqz v4, :cond_6

    invoke-interface {p1, p2, p3}, Lfh;->b(J)V

    goto :goto_2

    :cond_6
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->c:Lfo;

    invoke-interface {p1, v0, p2, p3}, Lfh;->b(Lfo;J)J

    move-result-wide v3

    const-wide/16 v5, -0x1

    cmp-long v0, v3, v5

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/EOFException;

    invoke-direct {v0}, Ljava/io/EOFException;-><init>()V

    throw v0

    :cond_7
    move v0, v2

    goto :goto_0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    throw v0
.end method

.method public b(Lfo;J)J
    .locals 7

    const-wide/16 v3, 0x0

    cmp-long v0, p2, v3

    if-gez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "byteCount < 0: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v2

    :try_start_0
    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/an;->a()V

    invoke-direct {p0}, Lcom/squareup/okhttp/internal/spdy/an;->b()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    cmp-long v0, v0, v3

    if-nez v0, :cond_1

    const-wide/16 v0, -0x1

    monitor-exit v2

    :goto_0
    return-wide v0

    :cond_1
    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    invoke-virtual {v1}, Lfo;->l()J

    move-result-wide v3

    invoke-static {p2, p3, v3, v4}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v3

    invoke-virtual {v0, p1, v3, v4}, Lfo;->b(Lfo;J)J

    move-result-wide v0

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-wide v4, v3, Lcom/squareup/okhttp/internal/spdy/ak;->a:J

    add-long/2addr v4, v0

    iput-wide v4, v3, Lcom/squareup/okhttp/internal/spdy/ak;->a:J

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-wide v3, v3, Lcom/squareup/okhttp/internal/spdy/ak;->a:J

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v5}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    const/high16 v6, 0x10000

    invoke-virtual {v5, v6}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-ltz v3, :cond_2

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v3

    iget-object v4, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v4}, Lcom/squareup/okhttp/internal/spdy/ak;->b(Lcom/squareup/okhttp/internal/spdy/ak;)I

    move-result v4

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    iget-wide v5, v5, Lcom/squareup/okhttp/internal/spdy/ak;->a:J

    invoke-virtual {v3, v4, v5, v6}, Lcom/squareup/okhttp/internal/spdy/y;->a(IJ)V

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lcom/squareup/okhttp/internal/spdy/ak;->a:J

    :cond_2
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    iget-object v2, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v2}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v2

    monitor-enter v2

    :try_start_1
    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v3

    iget-wide v4, v3, Lcom/squareup/okhttp/internal/spdy/y;->c:J

    add-long/2addr v4, v0

    iput-wide v4, v3, Lcom/squareup/okhttp/internal/spdy/y;->c:J

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v3

    iget-wide v3, v3, Lcom/squareup/okhttp/internal/spdy/y;->c:J

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v5}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v5

    iget-object v5, v5, Lcom/squareup/okhttp/internal/spdy/y;->f:Lcom/squareup/okhttp/internal/spdy/u;

    const/high16 v6, 0x10000

    invoke-virtual {v5, v6}, Lcom/squareup/okhttp/internal/spdy/u;->d(I)I

    move-result v5

    div-int/lit8 v5, v5, 0x2

    int-to-long v5, v5

    cmp-long v3, v3, v5

    if-ltz v3, :cond_3

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v5}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v5

    iget-wide v5, v5, Lcom/squareup/okhttp/internal/spdy/y;->c:J

    invoke-virtual {v3, v4, v5, v6}, Lcom/squareup/okhttp/internal/spdy/y;->a(IJ)V

    iget-object v3, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v3}, Lcom/squareup/okhttp/internal/spdy/ak;->a(Lcom/squareup/okhttp/internal/spdy/ak;)Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v3

    const-wide/16 v4, 0x0

    iput-wide v4, v3, Lcom/squareup/okhttp/internal/spdy/y;->c:J

    :cond_3
    monitor-exit v2

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public close()V
    .locals 2

    iget-object v1, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->f:Z

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->d:Lfo;

    invoke-virtual {v0}, Lfo;->o()V

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/squareup/okhttp/internal/spdy/an;->b:Lcom/squareup/okhttp/internal/spdy/ak;

    invoke-static {v0}, Lcom/squareup/okhttp/internal/spdy/ak;->e(Lcom/squareup/okhttp/internal/spdy/ak;)V

    return-void

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method
