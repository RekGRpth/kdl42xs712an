.class public final Lcom/squareup/okhttp/c;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:Lcom/squareup/okhttp/d;

.field private final b:Lcom/squareup/okhttp/m;

.field private c:Ljava/net/Socket;

.field private d:Ljava/io/InputStream;

.field private e:Ljava/io/OutputStream;

.field private f:Lfh;

.field private g:Lfg;

.field private h:Z

.field private i:Lcom/squareup/okhttp/internal/http/k;

.field private j:Lcom/squareup/okhttp/internal/spdy/y;

.field private k:I

.field private l:J

.field private m:Lcom/squareup/okhttp/f;

.field private n:I


# direct methods
.method public constructor <init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/m;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/squareup/okhttp/c;->h:Z

    const/4 v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/c;->k:I

    iput-object p1, p0, Lcom/squareup/okhttp/c;->a:Lcom/squareup/okhttp/d;

    iput-object p2, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    return-void
.end method

.method private a(Lcom/squareup/okhttp/o;)V
    .locals 6

    const/4 v5, 0x1

    invoke-static {}, Ley;->a()Ley;

    move-result-object v1

    invoke-virtual {p0}, Lcom/squareup/okhttp/c;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0, p1}, Lcom/squareup/okhttp/c;->b(Lcom/squareup/okhttp/o;)V

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v0, v0, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v0, v0, Lcom/squareup/okhttp/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    iget-object v2, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    iget-object v3, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v3, v3, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v3, v3, Lcom/squareup/okhttp/a;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v4, v4, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget v4, v4, Lcom/squareup/okhttp/a;->c:I

    invoke-virtual {v0, v2, v3, v4, v5}, Ljavax/net/ssl/SSLSocketFactory;->createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    check-cast v0, Ljavax/net/ssl/SSLSocket;

    iget-object v2, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-boolean v2, v2, Lcom/squareup/okhttp/m;->d:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v2, v2, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v2, v2, Lcom/squareup/okhttp/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Ley;->a(Ljavax/net/ssl/SSLSocket;Ljava/lang/String;)V

    :goto_0
    iget-object v1, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v1, v1, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v1, v1, Lcom/squareup/okhttp/a;->g:Ljava/util/List;

    sget-object v2, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    invoke-interface {v1, v2}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->startHandshake()V

    iget-object v1, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v1, v1, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v1, v1, Lcom/squareup/okhttp/a;->e:Ljavax/net/ssl/HostnameVerifier;

    iget-object v3, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v3, v3, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v3, v3, Lcom/squareup/okhttp/a;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljavax/net/ssl/HostnameVerifier;->verify(Ljava/lang/String;Ljavax/net/ssl/SSLSession;)Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Hostname \'"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v2, v2, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v2, v2, Lcom/squareup/okhttp/a;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, "\' was not verified"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    invoke-virtual {v1, v0}, Ley;->a(Ljavax/net/ssl/SSLSocket;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/c;->e:Ljava/io/OutputStream;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/c;->d:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljavax/net/ssl/SSLSocket;->getSession()Ljavax/net/ssl/SSLSession;

    move-result-object v1

    invoke-static {v1}, Lcom/squareup/okhttp/f;->a(Ljavax/net/ssl/SSLSession;)Lcom/squareup/okhttp/f;

    move-result-object v1

    iput-object v1, p0, Lcom/squareup/okhttp/c;->m:Lcom/squareup/okhttp/f;

    invoke-direct {p0}, Lcom/squareup/okhttp/c;->o()V

    sget-object v1, Lcom/squareup/okhttp/Protocol;->c:Lcom/squareup/okhttp/Protocol;

    if-eqz v2, :cond_3

    sget-object v1, Lcom/squareup/okhttp/Protocol;->b:Lcom/squareup/okhttp/Protocol;

    :cond_3
    iget-boolean v2, v1, Lcom/squareup/okhttp/Protocol;->spdyVariant:Z

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljavax/net/ssl/SSLSocket;->setSoTimeout(I)V

    new-instance v0, Lcom/squareup/okhttp/internal/spdy/ag;

    iget-object v2, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v2, v2, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    invoke-virtual {v2}, Lcom/squareup/okhttp/a;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/squareup/okhttp/c;->f:Lfh;

    iget-object v4, p0, Lcom/squareup/okhttp/c;->g:Lfg;

    invoke-direct {v0, v2, v5, v3, v4}, Lcom/squareup/okhttp/internal/spdy/ag;-><init>(Ljava/lang/String;ZLfh;Lfg;)V

    invoke-virtual {v0, v1}, Lcom/squareup/okhttp/internal/spdy/ag;->a(Lcom/squareup/okhttp/Protocol;)Lcom/squareup/okhttp/internal/spdy/ag;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/ag;->a()Lcom/squareup/okhttp/internal/spdy/y;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/y;->e()V

    :goto_1
    return-void

    :cond_4
    new-instance v0, Lcom/squareup/okhttp/internal/http/k;

    iget-object v1, p0, Lcom/squareup/okhttp/c;->a:Lcom/squareup/okhttp/d;

    iget-object v2, p0, Lcom/squareup/okhttp/c;->f:Lfh;

    iget-object v3, p0, Lcom/squareup/okhttp/c;->g:Lfg;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/okhttp/internal/http/k;-><init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/c;Lfh;Lfg;)V

    iput-object v0, p0, Lcom/squareup/okhttp/c;->i:Lcom/squareup/okhttp/internal/http/k;

    goto :goto_1
.end method

.method private b(Lcom/squareup/okhttp/o;)V
    .locals 6

    iget-object v0, p0, Lcom/squareup/okhttp/c;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lfr;->a(Ljava/io/InputStream;)Lgb;

    move-result-object v0

    invoke-static {v0}, Lfr;->a(Lgb;)Lfh;

    move-result-object v1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Lfr;->a(Ljava/io/OutputStream;)Lga;

    move-result-object v0

    invoke-static {v0}, Lfr;->a(Lga;)Lfg;

    move-result-object v0

    new-instance v2, Lcom/squareup/okhttp/internal/http/k;

    iget-object v3, p0, Lcom/squareup/okhttp/c;->a:Lcom/squareup/okhttp/d;

    invoke-direct {v2, v3, p0, v1, v0}, Lcom/squareup/okhttp/internal/http/k;-><init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/c;Lfh;Lfg;)V

    invoke-virtual {p1}, Lcom/squareup/okhttp/o;->b()Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/okhttp/o;->a()Ljava/lang/String;

    move-result-object v3

    :cond_0
    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aa;->d()Lcom/squareup/okhttp/internal/http/f;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Lcom/squareup/okhttp/internal/http/k;->a(Lcom/squareup/okhttp/internal/http/f;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/k;->d()V

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/k;->e()Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v4

    invoke-virtual {v4, v0}, Lcom/squareup/okhttp/internal/http/aj;->a(Lcom/squareup/okhttp/internal/http/aa;)Lcom/squareup/okhttp/internal/http/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/aj;->a()Lcom/squareup/okhttp/internal/http/ag;

    move-result-object v0

    invoke-virtual {v2}, Lcom/squareup/okhttp/internal/http/k;->g()V

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unexpected response code for CONNECT: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/http/ag;->c()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    :sswitch_0
    invoke-interface {v1}, Lfh;->b()Lfo;

    move-result-object v0

    invoke-virtual {v0}, Lfo;->l()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "TLS tunnel buffered too many bytes!"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_1
    iget-object v4, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v4, v4, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v4, v4, Lcom/squareup/okhttp/a;->f:Lcom/squareup/okhttp/g;

    iget-object v5, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v5, v5, Lcom/squareup/okhttp/m;->b:Ljava/net/Proxy;

    invoke-static {v4, v0, v5}, Lcom/squareup/okhttp/internal/http/i;->a(Lcom/squareup/okhttp/g;Lcom/squareup/okhttp/internal/http/ag;Ljava/net/Proxy;)Lcom/squareup/okhttp/internal/http/aa;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "Failed to authenticate with proxy"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void

    :sswitch_data_0
    .sparse-switch
        0xc8 -> :sswitch_0
        0x197 -> :sswitch_1
    .end sparse-switch
.end method

.method private o()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->d:Ljava/io/InputStream;

    invoke-static {v0}, Lfr;->a(Ljava/io/InputStream;)Lgb;

    move-result-object v0

    invoke-static {v0}, Lfr;->a(Lgb;)Lfh;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/c;->f:Lfh;

    iget-object v0, p0, Lcom/squareup/okhttp/c;->e:Ljava/io/OutputStream;

    invoke-static {v0}, Lfr;->a(Ljava/io/OutputStream;)Lga;

    move-result-object v0

    invoke-static {v0}, Lfr;->a(Lga;)Lfg;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/c;->g:Lfg;

    return-void
.end method


# virtual methods
.method public a(Lcom/squareup/okhttp/internal/http/u;)Ljava/lang/Object;
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/squareup/okhttp/internal/http/ar;

    iget-object v1, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/internal/http/ar;-><init>(Lcom/squareup/okhttp/internal/http/u;Lcom/squareup/okhttp/internal/spdy/y;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/squareup/okhttp/internal/http/w;

    iget-object v1, p0, Lcom/squareup/okhttp/c;->i:Lcom/squareup/okhttp/internal/http/k;

    invoke-direct {v0, p1, v1}, Lcom/squareup/okhttp/internal/http/w;-><init>(Lcom/squareup/okhttp/internal/http/u;Lcom/squareup/okhttp/internal/http/k;)V

    goto :goto_0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/squareup/okhttp/c;->k:I

    return-void
.end method

.method public a(IILcom/squareup/okhttp/o;)V
    .locals 4

    iget-boolean v0, p0, Lcom/squareup/okhttp/c;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "already connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v0, v0, Lcom/squareup/okhttp/m;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-eq v0, v1, :cond_1

    new-instance v0, Ljava/net/Socket;

    iget-object v1, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v1, v1, Lcom/squareup/okhttp/m;->b:Ljava/net/Proxy;

    invoke-direct {v0, v1}, Ljava/net/Socket;-><init>(Ljava/net/Proxy;)V

    :goto_0
    iput-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-static {}, Ley;->a()Ley;

    move-result-object v0

    iget-object v1, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    iget-object v2, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v2, v2, Lcom/squareup/okhttp/m;->c:Ljava/net/InetSocketAddress;

    invoke-virtual {v0, v1, v2, p1}, Ley;->a(Ljava/net/Socket;Ljava/net/InetSocketAddress;I)V

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0, p2}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/c;->d:Ljava/io/InputStream;

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    iput-object v0, p0, Lcom/squareup/okhttp/c;->e:Ljava/io/OutputStream;

    iget-object v0, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v0, v0, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v0, v0, Lcom/squareup/okhttp/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_2

    invoke-direct {p0, p3}, Lcom/squareup/okhttp/c;->a(Lcom/squareup/okhttp/o;)V

    :goto_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/squareup/okhttp/c;->h:Z

    return-void

    :cond_1
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    goto :goto_0

    :cond_2
    invoke-direct {p0}, Lcom/squareup/okhttp/c;->o()V

    new-instance v0, Lcom/squareup/okhttp/internal/http/k;

    iget-object v1, p0, Lcom/squareup/okhttp/c;->a:Lcom/squareup/okhttp/d;

    iget-object v2, p0, Lcom/squareup/okhttp/c;->f:Lfh;

    iget-object v3, p0, Lcom/squareup/okhttp/c;->g:Lfg;

    invoke-direct {v0, v1, p0, v2, v3}, Lcom/squareup/okhttp/internal/http/k;-><init>(Lcom/squareup/okhttp/d;Lcom/squareup/okhttp/c;Lfh;Lfg;)V

    iput-object v0, p0, Lcom/squareup/okhttp/c;->i:Lcom/squareup/okhttp/internal/http/k;

    goto :goto_1
.end method

.method public a()Z
    .locals 1

    iget-boolean v0, p0, Lcom/squareup/okhttp/c;->h:Z

    return v0
.end method

.method public a(J)Z
    .locals 4

    invoke-virtual {p0}, Lcom/squareup/okhttp/c;->h()J

    move-result-wide v0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()Lcom/squareup/okhttp/m;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    return-object v0
.end method

.method public b(I)V
    .locals 2

    iget-boolean v0, p0, Lcom/squareup/okhttp/c;->h:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "updateReadTimeout - not connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0, p1}, Ljava/net/Socket;->setSoTimeout(I)V

    return-void
.end method

.method public c()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    return-object v0
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->close()V

    return-void
.end method

.method public d()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isClosed()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isInputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v0}, Ljava/net/Socket;->isOutputShutdown()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public e()Z
    .locals 5

    const/4 v1, 0x0

    const/4 v0, 0x1

    iget-object v2, p0, Lcom/squareup/okhttp/c;->f:Lfh;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p0}, Lcom/squareup/okhttp/c;->j()Z

    move-result v2

    if-nez v2, :cond_0

    :try_start_0
    iget-object v2, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v2}, Ljava/net/Socket;->getSoTimeout()I
    :try_end_0
    .catch Ljava/net/SocketTimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v3

    :try_start_1
    iget-object v2, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Ljava/net/Socket;->setSoTimeout(I)V

    iget-object v2, p0, Lcom/squareup/okhttp/c;->f:Lfh;

    invoke-interface {v2}, Lfh;->e()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    :try_start_2
    iget-object v2, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    move v0, v1

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v2, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :catchall_0
    move-exception v2

    iget-object v4, p0, Lcom/squareup/okhttp/c;->c:Ljava/net/Socket;

    invoke-virtual {v4, v3}, Ljava/net/Socket;->setSoTimeout(I)V

    throw v2
    :try_end_2
    .catch Ljava/net/SocketTimeoutException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public f()V
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "spdyConnection != null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/squareup/okhttp/c;->l:J

    return-void
.end method

.method public g()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/y;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public h()J
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/squareup/okhttp/c;->l:J

    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    invoke-virtual {v0}, Lcom/squareup/okhttp/internal/spdy/y;->c()J

    move-result-wide v0

    goto :goto_0
.end method

.method public i()Lcom/squareup/okhttp/f;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->m:Lcom/squareup/okhttp/f;

    return-object v0
.end method

.method public j()Z
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/c;->j:Lcom/squareup/okhttp/internal/spdy/y;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public k()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/c;->k:I

    return v0
.end method

.method public l()Z
    .locals 2

    iget-object v0, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v0, v0, Lcom/squareup/okhttp/m;->a:Lcom/squareup/okhttp/a;

    iget-object v0, v0, Lcom/squareup/okhttp/a;->d:Ljavax/net/ssl/SSLSocketFactory;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/squareup/okhttp/c;->b:Lcom/squareup/okhttp/m;

    iget-object v0, v0, Lcom/squareup/okhttp/m;->b:Ljava/net/Proxy;

    invoke-virtual {v0}, Ljava/net/Proxy;->type()Ljava/net/Proxy$Type;

    move-result-object v0

    sget-object v1, Ljava/net/Proxy$Type;->HTTP:Ljava/net/Proxy$Type;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public m()V
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/c;->n:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/squareup/okhttp/c;->n:I

    return-void
.end method

.method public n()I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/c;->n:I

    return v0
.end method
