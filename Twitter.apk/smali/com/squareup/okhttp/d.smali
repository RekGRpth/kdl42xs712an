.class public Lcom/squareup/okhttp/d;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static final a:Lcom/squareup/okhttp/d;


# instance fields
.field private final b:I

.field private final c:J

.field private final d:Ljava/util/LinkedList;

.field private final e:Ljava/util/concurrent/ExecutorService;

.field private final f:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const-string/jumbo v0, "http.keepAlive"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string/jumbo v0, "http.keepAliveDuration"

    invoke-static {v0}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string/jumbo v1, "http.maxConnections"

    invoke-static {v1}, Ljava/lang/System;->getProperty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    :goto_0
    if-eqz v2, :cond_1

    invoke-static {v2}, Ljava/lang/Boolean;->parseBoolean(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    new-instance v2, Lcom/squareup/okhttp/d;

    const/4 v3, 0x0

    invoke-direct {v2, v3, v0, v1}, Lcom/squareup/okhttp/d;-><init>(IJ)V

    sput-object v2, Lcom/squareup/okhttp/d;->a:Lcom/squareup/okhttp/d;

    :goto_1
    return-void

    :cond_0
    const-wide/32 v0, 0x493e0

    goto :goto_0

    :cond_1
    if-eqz v3, :cond_2

    new-instance v2, Lcom/squareup/okhttp/d;

    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    invoke-direct {v2, v3, v0, v1}, Lcom/squareup/okhttp/d;-><init>(IJ)V

    sput-object v2, Lcom/squareup/okhttp/d;->a:Lcom/squareup/okhttp/d;

    goto :goto_1

    :cond_2
    new-instance v2, Lcom/squareup/okhttp/d;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v0, v1}, Lcom/squareup/okhttp/d;-><init>(IJ)V

    sput-object v2, Lcom/squareup/okhttp/d;->a:Lcom/squareup/okhttp/d;

    goto :goto_1
.end method

.method public constructor <init>(IJ)V
    .locals 10

    const-wide/16 v8, 0x3e8

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    new-instance v0, Ljava/util/concurrent/ThreadPoolExecutor;

    const/4 v1, 0x0

    const-wide/16 v3, 0x3c

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v6, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v6}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    const-string/jumbo v7, "OkHttp ConnectionPool"

    invoke-static {v7, v2}, Lfd;->a(Ljava/lang/String;Z)Ljava/util/concurrent/ThreadFactory;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    iput-object v0, p0, Lcom/squareup/okhttp/d;->e:Ljava/util/concurrent/ExecutorService;

    new-instance v0, Lcom/squareup/okhttp/e;

    invoke-direct {v0, p0}, Lcom/squareup/okhttp/e;-><init>(Lcom/squareup/okhttp/d;)V

    iput-object v0, p0, Lcom/squareup/okhttp/d;->f:Ljava/lang/Runnable;

    iput p1, p0, Lcom/squareup/okhttp/d;->b:I

    mul-long v0, p2, v8

    mul-long/2addr v0, v8

    iput-wide v0, p0, Lcom/squareup/okhttp/d;->c:J

    return-void
.end method

.method public static a()Lcom/squareup/okhttp/d;
    .locals 1

    sget-object v0, Lcom/squareup/okhttp/d;->a:Lcom/squareup/okhttp/d;

    return-object v0
.end method

.method static synthetic a(Lcom/squareup/okhttp/d;)Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic b(Lcom/squareup/okhttp/d;)J
    .locals 2

    iget-wide v0, p0, Lcom/squareup/okhttp/d;->c:J

    return-wide v0
.end method

.method static synthetic c(Lcom/squareup/okhttp/d;)I
    .locals 1

    iget v0, p0, Lcom/squareup/okhttp/d;->b:I

    return v0
.end method


# virtual methods
.method public declared-synchronized a(Lcom/squareup/okhttp/a;)Lcom/squareup/okhttp/c;
    .locals 8

    monitor-enter p0

    const/4 v2, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    iget-object v1, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/LinkedList;->listIterator(I)Ljava/util/ListIterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/ListIterator;->hasPrevious()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/ListIterator;->previous()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/squareup/okhttp/c;

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->b()Lcom/squareup/okhttp/m;

    move-result-object v1

    invoke-virtual {v1}, Lcom/squareup/okhttp/m;->a()Lcom/squareup/okhttp/a;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/squareup/okhttp/a;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v4

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->h()J

    move-result-wide v6

    sub-long/2addr v4, v6

    iget-wide v6, p0, Lcom/squareup/okhttp/d;->c:J

    cmp-long v1, v4, v6

    if-gez v1, :cond_0

    invoke-interface {v3}, Ljava/util/ListIterator;->remove()V

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->j()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    :try_start_1
    invoke-static {}, Ley;->a()Ley;

    move-result-object v1

    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->c()Ljava/net/Socket;

    move-result-object v4

    invoke-virtual {v1, v4}, Ley;->a(Ljava/net/Socket;)V
    :try_end_1
    .catch Ljava/net/SocketException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_1
    if-eqz v0, :cond_2

    :try_start_2
    invoke-virtual {v0}, Lcom/squareup/okhttp/c;->j()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    :cond_2
    iget-object v1, p0, Lcom/squareup/okhttp/d;->e:Ljava/util/concurrent/ExecutorService;

    iget-object v2, p0, Lcom/squareup/okhttp/d;->f:Ljava/lang/Runnable;

    invoke-interface {v1, v2}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    monitor-exit p0

    return-object v0

    :catch_0
    move-exception v1

    :try_start_3
    invoke-static {v0}, Lfd;->a(Ljava/io/Closeable;)V

    invoke-static {}, Ley;->a()Ley;

    move-result-object v0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v5, "Unable to tagSocket(): "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ley;->a(Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_3
    move-object v0, v2

    goto :goto_1
.end method

.method public a(Lcom/squareup/okhttp/c;)V
    .locals 4

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->d()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {p1}, Lfd;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :cond_1
    :try_start_0
    invoke-static {}, Ley;->a()Ley;

    move-result-object v0

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->c()Ljava/net/Socket;

    move-result-object v1

    invoke-virtual {v0, v1}, Ley;->b(Ljava/net/Socket;)V
    :try_end_0
    .catch Ljava/net/SocketException; {:try_start_0 .. :try_end_0} :catch_0

    monitor-enter p0

    :try_start_1
    iget-object v0, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->m()V

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->f()V

    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    iget-object v0, p0, Lcom/squareup/okhttp/d;->e:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/squareup/okhttp/d;->f:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-static {}, Ley;->a()Ley;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Unable to untagSocket(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ley;->a(Ljava/lang/String;)V

    invoke-static {p1}, Lfd;->a(Ljava/io/Closeable;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0
.end method

.method public b(Lcom/squareup/okhttp/c;)V
    .locals 2

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->j()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/squareup/okhttp/d;->e:Ljava/util/concurrent/ExecutorService;

    iget-object v1, p0, Lcom/squareup/okhttp/d;->f:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/ExecutorService;->execute(Ljava/lang/Runnable;)V

    invoke-virtual {p1}, Lcom/squareup/okhttp/c;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/squareup/okhttp/d;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->addFirst(Ljava/lang/Object;)V

    monitor-exit p0

    :cond_1
    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
