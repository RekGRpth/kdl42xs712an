.class public Lcom/google/android/gms/internal/ev;
.super Lcom/google/android/gms/internal/cm;


# instance fields
.field private final f:Lcom/google/android/gms/internal/ez;

.field private final g:Lcom/google/android/gms/internal/es;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;Ljava/lang/String;)V
    .locals 2

    const/4 v0, 0x0

    new-array v0, v0, [Ljava/lang/String;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/internal/cm;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V

    new-instance v0, Lcom/google/android/gms/internal/ex;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/ex;-><init>(Lcom/google/android/gms/internal/ev;Lcom/google/android/gms/internal/ew;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ev;->f:Lcom/google/android/gms/internal/ez;

    new-instance v0, Lcom/google/android/gms/internal/es;

    iget-object v1, p0, Lcom/google/android/gms/internal/ev;->f:Lcom/google/android/gms/internal/ez;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/internal/es;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ez;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/ev;->g:Lcom/google/android/gms/internal/es;

    iput-object p4, p0, Lcom/google/android/gms/internal/ev;->h:Ljava/lang/String;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/ev;)V
    .locals 0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ev;->j()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/ev;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ev;->k()Landroid/os/IInterface;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/ev;->c(Landroid/os/IBinder;)Lcom/google/android/gms/internal/ep;

    move-result-object v0

    return-object v0
.end method

.method protected a()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.location.internal.GoogleLocationManagerService.START"

    return-object v0
.end method

.method protected a(Lcom/google/android/gms/internal/dc;Lcom/google/android/gms/internal/cr;)V
    .locals 3

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string/jumbo v1, "client_name"

    iget-object v2, p0, Lcom/google/android/gms/internal/ev;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const v1, 0x3d8024

    invoke-virtual {p0}, Lcom/google/android/gms/internal/ev;->f()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/internal/dc;->e(Lcom/google/android/gms/internal/cy;ILjava/lang/String;Landroid/os/Bundle;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/d;)V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/internal/ev;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/d;Landroid/os/Looper;)V

    return-void
.end method

.method public a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/d;Landroid/os/Looper;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ev;->g:Lcom/google/android/gms/internal/es;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ev;->g:Lcom/google/android/gms/internal/es;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/internal/es;->a(Lcom/google/android/gms/location/LocationRequest;Lcom/google/android/gms/location/d;Landroid/os/Looper;)V

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public a(Lcom/google/android/gms/location/d;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ev;->g:Lcom/google/android/gms/internal/es;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/internal/es;->a(Lcom/google/android/gms/location/d;)V

    return-void
.end method

.method protected b()Ljava/lang/String;
    .locals 1

    const-string/jumbo v0, "com.google.android.gms.location.internal.IGoogleLocationManagerService"

    return-object v0
.end method

.method protected c(Landroid/os/IBinder;)Lcom/google/android/gms/internal/ep;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/eq;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/ep;

    move-result-object v0

    return-object v0
.end method

.method public l()Landroid/location/Location;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ev;->g:Lcom/google/android/gms/internal/es;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/es;->a()Landroid/location/Location;

    move-result-object v0

    return-object v0
.end method
