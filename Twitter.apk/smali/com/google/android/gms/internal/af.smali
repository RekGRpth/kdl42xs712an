.class public final Lcom/google/android/gms/internal/af;
.super Ljava/lang/Object;


# direct methods
.method public static a(I)Lcom/google/ads/AdRequest$Gender;
    .locals 1

    packed-switch p0, :pswitch_data_0

    sget-object v0, Lcom/google/ads/AdRequest$Gender;->a:Lcom/google/ads/AdRequest$Gender;

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/google/ads/AdRequest$Gender;->c:Lcom/google/ads/AdRequest$Gender;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/google/ads/AdRequest$Gender;->b:Lcom/google/ads/AdRequest$Gender;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/internal/x;)Lcom/google/ads/a;
    .locals 5

    new-instance v0, Lcom/google/ads/a;

    new-instance v1, Lcom/google/android/gms/ads/c;

    iget v2, p0, Lcom/google/android/gms/internal/x;->f:I

    iget v3, p0, Lcom/google/android/gms/internal/x;->c:I

    iget-object v4, p0, Lcom/google/android/gms/internal/x;->b:Ljava/lang/String;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/ads/c;-><init>(IILjava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/ads/a;-><init>(Lcom/google/android/gms/ads/c;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/internal/v;)Lcom/google/ads/mediation/a;
    .locals 5

    iget-object v0, p0, Lcom/google/android/gms/internal/v;->e:Ljava/util/List;

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p0, Lcom/google/android/gms/internal/v;->e:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    :goto_0
    new-instance v1, Lcom/google/ads/mediation/a;

    new-instance v2, Ljava/util/Date;

    iget-wide v3, p0, Lcom/google/android/gms/internal/v;->b:J

    invoke-direct {v2, v3, v4}, Ljava/util/Date;-><init>(J)V

    iget v3, p0, Lcom/google/android/gms/internal/v;->d:I

    invoke-static {v3}, Lcom/google/android/gms/internal/af;->a(I)Lcom/google/ads/AdRequest$Gender;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/internal/v;->f:Z

    invoke-direct {v1, v2, v3, v0, v4}, Lcom/google/ads/mediation/a;-><init>(Ljava/util/Date;Lcom/google/ads/AdRequest$Gender;Ljava/util/Set;Z)V

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
