.class public final Lcom/google/android/gms/internal/ad;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/ads/mediation/d;
.implements Lcom/google/ads/mediation/f;


# instance fields
.field private final a:Lcom/google/android/gms/internal/y;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/internal/y;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/ad;->a:Lcom/google/android/gms/internal/y;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/ad;)Lcom/google/android/gms/internal/y;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ad;->a:Lcom/google/android/gms/internal/y;

    return-object v0
.end method


# virtual methods
.method public onClick(Lcom/google/ads/mediation/c;)V
    .locals 2

    const-string/jumbo v0, "Adapter called onClick."

    invoke-static {v0}, Lcom/google/android/gms/internal/bo;->a(Ljava/lang/String;)V

    invoke-static {}, Lcom/google/android/gms/internal/bn;->a()Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "onClick must be called on the main UI thread."

    invoke-static {v0}, Lcom/google/android/gms/internal/bo;->e(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/internal/bn;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/internal/ae;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/ae;-><init>(Lcom/google/android/gms/internal/ad;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ad;->a:Lcom/google/android/gms/internal/y;

    invoke-interface {v0}, Lcom/google/android/gms/internal/y;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string/jumbo v1, "Could not call onAdClicked."

    invoke-static {v1, v0}, Lcom/google/android/gms/internal/bo;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
