.class Lcom/crashlytics/android/internal/n;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Lcom/crashlytics/android/internal/bk;


# instance fields
.field a:Lcom/crashlytics/android/internal/t;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/lang/String;

.field private final j:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/j;Lcom/crashlytics/android/internal/bu;)V
    .locals 11

    const-string/jumbo v0, "Crashlytics SAM"

    invoke-static {v0}, Lcom/crashlytics/android/internal/bi;->b(Ljava/lang/String;)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v9

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    move-object/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/crashlytics/android/internal/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/j;Ljava/util/concurrent/ScheduledExecutorService;Lcom/crashlytics/android/internal/bu;)V

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/j;Ljava/util/concurrent/ScheduledExecutorService;Lcom/crashlytics/android/internal/bu;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/crashlytics/android/internal/n;->b:Ljava/lang/String;

    iput-object p2, p0, Lcom/crashlytics/android/internal/n;->c:Ljava/lang/String;

    iput-object p3, p0, Lcom/crashlytics/android/internal/n;->d:Ljava/lang/String;

    iput-object p4, p0, Lcom/crashlytics/android/internal/n;->e:Ljava/lang/String;

    iput-object p5, p0, Lcom/crashlytics/android/internal/n;->f:Ljava/lang/String;

    iput-object p6, p0, Lcom/crashlytics/android/internal/n;->g:Ljava/lang/String;

    iput-object p7, p0, Lcom/crashlytics/android/internal/n;->h:Ljava/lang/String;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/crashlytics/android/internal/n;->i:Ljava/lang/String;

    iput-object p9, p0, Lcom/crashlytics/android/internal/n;->j:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v0, Lcom/crashlytics/android/internal/i;

    invoke-direct {v0, p9, p8, p10}, Lcom/crashlytics/android/internal/i;-><init>(Ljava/util/concurrent/ScheduledExecutorService;Lcom/crashlytics/android/internal/j;Lcom/crashlytics/android/internal/bu;)V

    iput-object v0, p0, Lcom/crashlytics/android/internal/n;->a:Lcom/crashlytics/android/internal/t;

    invoke-virtual {p8, p0}, Lcom/crashlytics/android/internal/j;->a(Lcom/crashlytics/android/internal/bk;)V

    return-void
.end method

.method static synthetic a(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->b:Ljava/lang/String;

    return-object v0
.end method

.method private a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V
    .locals 10

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/crashlytics/android/internal/n;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/crashlytics/android/internal/n;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/crashlytics/android/internal/n;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/crashlytics/android/internal/n;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/crashlytics/android/internal/n;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/crashlytics/android/internal/n;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/crashlytics/android/internal/n;->h:Ljava/lang/String;

    const-string/jumbo v8, "activity"

    invoke-virtual {p2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v9

    move-object v8, p1

    invoke-static/range {v0 .. v9}, Lcom/crashlytics/android/internal/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/W;Ljava/util/Map;)Lcom/crashlytics/android/internal/u;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/u;Z)V

    return-void
.end method

.method private a(Lcom/crashlytics/android/internal/u;Z)V
    .locals 1

    new-instance v0, Lcom/crashlytics/android/internal/p;

    invoke-direct {v0, p0, p1, p2}, Lcom/crashlytics/android/internal/p;-><init>(Lcom/crashlytics/android/internal/n;Lcom/crashlytics/android/internal/u;Z)V

    invoke-direct {p0, v0}, Lcom/crashlytics/android/internal/n;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->j:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0, p1}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "Crashlytics failed to submit analytics task"

    invoke-static {v0}, Lcom/crashlytics/android/internal/bd;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic b(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->c:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic h(Lcom/crashlytics/android/internal/n;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->h:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method a()V
    .locals 1

    new-instance v0, Lcom/crashlytics/android/internal/s;

    invoke-direct {v0, p0}, Lcom/crashlytics/android/internal/s;-><init>(Lcom/crashlytics/android/internal/n;)V

    invoke-direct {p0, v0}, Lcom/crashlytics/android/internal/n;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->a:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method

.method final a(Lcom/crashlytics/android/internal/ak;Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/crashlytics/android/internal/q;

    invoke-direct {v0, p0, p1, p2}, Lcom/crashlytics/android/internal/q;-><init>(Lcom/crashlytics/android/internal/n;Lcom/crashlytics/android/internal/ak;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/crashlytics/android/internal/n;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "onCrash called from main thread!!!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    new-instance v0, Lcom/crashlytics/android/internal/o;

    invoke-direct {v0, p0, p1}, Lcom/crashlytics/android/internal/o;-><init>(Lcom/crashlytics/android/internal/n;Ljava/lang/String;)V

    :try_start_0
    iget-object v1, p0, Lcom/crashlytics/android/internal/n;->j:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v1, v0}, Ljava/util/concurrent/ScheduledExecutorService;->submit(Ljava/lang/Runnable;)Ljava/util/concurrent/Future;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/concurrent/Future;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string/jumbo v0, "Crashlytics failed to run analytics task"

    invoke-static {v0}, Lcom/crashlytics/android/internal/bd;->d(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 10

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/crashlytics/android/internal/n;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/crashlytics/android/internal/n;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/crashlytics/android/internal/n;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/crashlytics/android/internal/n;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/crashlytics/android/internal/n;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/crashlytics/android/internal/n;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/crashlytics/android/internal/n;->h:Ljava/lang/String;

    sget-object v8, Lcom/crashlytics/android/internal/W;->j:Lcom/crashlytics/android/internal/W;

    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    invoke-static/range {v0 .. v9}, Lcom/crashlytics/android/internal/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/W;Ljava/util/Map;)Lcom/crashlytics/android/internal/u;

    move-result-object v0

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/u;Z)V

    return-void
.end method

.method public final b(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->g:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 10

    iget-object v0, p0, Lcom/crashlytics/android/internal/n;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/crashlytics/android/internal/n;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/crashlytics/android/internal/n;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/crashlytics/android/internal/n;->d:Ljava/lang/String;

    iget-object v4, p0, Lcom/crashlytics/android/internal/n;->e:Ljava/lang/String;

    iget-object v5, p0, Lcom/crashlytics/android/internal/n;->f:Ljava/lang/String;

    iget-object v6, p0, Lcom/crashlytics/android/internal/n;->g:Ljava/lang/String;

    iget-object v7, p0, Lcom/crashlytics/android/internal/n;->h:Ljava/lang/String;

    const-string/jumbo v8, "sessionId"

    invoke-static {v8, p1}, Ljava/util/Collections;->singletonMap(Ljava/lang/Object;Ljava/lang/Object;)Ljava/util/Map;

    move-result-object v9

    sget-object v8, Lcom/crashlytics/android/internal/W;->h:Lcom/crashlytics/android/internal/W;

    invoke-static/range {v0 .. v9}, Lcom/crashlytics/android/internal/u;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/crashlytics/android/internal/W;Ljava/util/Map;)Lcom/crashlytics/android/internal/u;

    move-result-object v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/u;Z)V

    return-void
.end method

.method public final c()V
    .locals 1

    new-instance v0, Lcom/crashlytics/android/internal/r;

    invoke-direct {v0, p0}, Lcom/crashlytics/android/internal/r;-><init>(Lcom/crashlytics/android/internal/n;)V

    invoke-direct {p0, v0}, Lcom/crashlytics/android/internal/n;->a(Ljava/lang/Runnable;)V

    return-void
.end method

.method public final c(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->e:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method

.method public final d(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->c:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method

.method public final e(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->d:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method

.method public final f(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->b:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method

.method public final g(Landroid/app/Activity;)V
    .locals 2

    sget-object v0, Lcom/crashlytics/android/internal/W;->f:Lcom/crashlytics/android/internal/W;

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/crashlytics/android/internal/n;->a(Lcom/crashlytics/android/internal/W;Landroid/app/Activity;Z)V

    return-void
.end method
