.class public Lkl;
.super Ljava/lang/Object;
.source "Twttr"


# direct methods
.method public static a()V
    .locals 1

    invoke-static {}, Lkl;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_inline_reply_actions_1999"

    invoke-static {v0}, Lkk;->b(Ljava/lang/String;)V

    :cond_0
    return-void
.end method

.method public static b()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lkl;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_inline_reply_actions_1999"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const-string/jumbo v4, "inline_actions_hidden_reply"

    aput-object v4, v3, v1

    const-string/jumbo v4, "sticky_actions_hidden_reply"

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static c()Z
    .locals 1

    invoke-static {}, Lkl;->d()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lkl;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static d()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lkl;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_inline_reply_actions_1999"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "inline_actions_sticky_reply"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static e()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lkl;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_inline_reply_actions_1999"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "sticky_actions_hidden_reply"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method public static f()Z
    .locals 1

    invoke-static {}, Lkl;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "android_inline_reply_actions_1999"

    invoke-static {v0}, Lkk;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static g()Z
    .locals 5

    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-static {}, Lkl;->h()Z

    move-result v2

    if-eqz v2, :cond_0

    const-string/jumbo v2, "android_inline_reply_actions_1999"

    new-array v3, v0, [Ljava/lang/String;

    const-string/jumbo v4, "unassigned"

    aput-object v4, v3, v1

    invoke-static {v2, v3}, Lkk;->a(Ljava/lang/String;[Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private static h()Z
    .locals 1

    invoke-static {}, Lkn;->h()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/twitter/library/featureswitch/a;->aM()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
