.class public Lin;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;)V
    .locals 1

    const-class v0, Lin;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    return-void
.end method

.method private a(Ljava/lang/String;)Z
    .locals 8

    const/4 v7, 0x1

    const/4 v3, 0x0

    const/4 v6, 0x0

    iget-object v0, p0, Lin;->l:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/twitter/library/provider/i;->a:Landroid/net/Uri;

    invoke-static {v1, p1}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    new-array v2, v7, [Ljava/lang/String;

    const-string/jumbo v4, "notif_tweet"

    aput-object v4, v2, v6

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->isNull(I)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :goto_1
    if-eqz v0, :cond_0

    move v6, v7

    :cond_0
    return v6

    :cond_1
    move v0, v6

    goto :goto_0

    :cond_2
    move v0, v6

    goto :goto_1
.end method

.method private c(Lcom/twitter/library/service/e;)V
    .locals 14

    invoke-virtual {p0}, Lin;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/p;->c:J

    iget-object v0, p0, Lin;->l:Landroid/content/Context;

    invoke-static {v0, v2, v3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    new-instance v4, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v4, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iget-object v10, p0, Lin;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "user_id"

    invoke-virtual {v10, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    const-string/jumbo v1, "device_follow"

    invoke-virtual {v10, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    const-string/jumbo v5, "email_follow"

    invoke-virtual {v10, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v5

    iget-object v6, p0, Lin;->m:Lcom/twitter/library/network/aa;

    iget-object v6, v6, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v7, 0x3

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    const-string/jumbo v9, "1.1"

    aput-object v9, v7, v8

    const/4 v8, 0x1

    const-string/jumbo v9, "favorite_users"

    aput-object v9, v7, v8

    const/4 v8, 0x2

    const-string/jumbo v9, "create"

    aput-object v9, v7, v8

    invoke-static {v6, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, ".json"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v7, "send_error_codes"

    const/4 v8, 0x1

    invoke-static {v6, v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    new-instance v8, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "user_id"

    invoke-static {v11, v12}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v8, v9, v13}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v8, "device_follow"

    invoke-virtual {v10, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_0

    if-eqz v1, :cond_4

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "device"

    const-string/jumbo v9, "true"

    invoke-direct {v1, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    const-string/jumbo v1, "email_follow"

    invoke-virtual {v10, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-eqz v5, :cond_5

    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "email"

    const-string/jumbo v8, "default"

    invoke-direct {v1, v5, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    const/16 v1, 0x1e

    new-instance v5, Lcom/twitter/library/util/f;

    iget-object v8, p0, Lin;->l:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v5, v8, v2, v3, v9}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v1, v5}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    new-instance v5, Lcom/twitter/library/network/d;

    iget-object v8, p0, Lin;->l:Landroid/content/Context;

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v8, v6}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v6, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v5, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v7}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v13

    invoke-virtual {v13}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/16 v4, 0x800

    invoke-static {v1, v4}, Lcom/twitter/library/provider/ay;->a(II)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v0, v11, v12, v4}, Lcom/twitter/library/provider/az;->c(JI)V

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-static {v4}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v4

    if-eqz v4, :cond_2

    const-string/jumbo v4, "account_name"

    invoke-virtual {v10, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v4}, Lin;->a(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_2

    const-string/jumbo v4, "notification_not_enabled"

    const/4 v5, 0x1

    invoke-virtual {v10, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    invoke-virtual {v0, v11, v12}, Lcom/twitter/library/provider/az;->a(J)Lcom/twitter/library/api/TwitterUser;

    move-result-object v4

    if-eqz v4, :cond_3

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v4, Lcom/twitter/library/api/TwitterUser;->friendship:I

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v1, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const/16 v4, 0x10

    const-wide/16 v5, -0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_3
    const-string/jumbo v1, "friendship"

    invoke-virtual {v0, v11, v12}, Lcom/twitter/library/provider/az;->h(J)I

    move-result v0

    invoke-virtual {v10, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p1, v13}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_4
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v8, "device"

    const-string/jumbo v9, "false"

    invoke-direct {v1, v8, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_5
    new-instance v1, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v5, "email"

    const-string/jumbo v8, "none"

    invoke-direct {v1, v5, v8}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method

.method private d(Lcom/twitter/library/service/e;)V
    .locals 13

    invoke-virtual {p0}, Lin;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    iget-wide v2, v1, Lcom/twitter/library/service/p;->c:J

    iget-object v0, p0, Lin;->l:Landroid/content/Context;

    invoke-static {v0, v2, v3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v0

    new-instance v6, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v6, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iget-object v7, p0, Lin;->k:Landroid/os/Bundle;

    const-string/jumbo v1, "user_id"

    invoke-virtual {v7, v1}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    iget-object v1, p0, Lin;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v8, v9

    const/4 v9, 0x1

    const-string/jumbo v10, "favorite_users"

    aput-object v10, v8, v9

    const/4 v9, 0x2

    const-string/jumbo v10, "destroy"

    aput-object v10, v8, v9

    invoke-static {v1, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v8, ".json"

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v10, "user_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const/16 v9, 0x1e

    new-instance v10, Lcom/twitter/library/util/f;

    iget-object v11, p0, Lin;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-direct {v10, v11, v2, v3, v12}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    invoke-static {v9, v10}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v9

    new-instance v10, Lcom/twitter/library/network/d;

    iget-object v11, p0, Lin;->l:Landroid/content/Context;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v10, v11, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v10, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v6}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v8}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1, v9}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v6

    invoke-virtual {v6}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    const/16 v1, 0x800

    invoke-virtual {v0, v4, v5, v1}, Lcom/twitter/library/provider/az;->e(JI)V

    const/16 v1, 0x10

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IJJ)V

    :cond_0
    const-string/jumbo v1, "friendship"

    invoke-virtual {v0, v4, v5}, Lcom/twitter/library/provider/az;->h(J)I

    move-result v0

    invoke-virtual {v7, v1, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p1, v6}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method private e(Lcom/twitter/library/service/e;)V
    .locals 13

    const/4 v12, 0x1

    invoke-virtual {p0}, Lin;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v1, v0, Lcom/twitter/library/service/p;->c:J

    iget-object v3, p0, Lin;->l:Landroid/content/Context;

    invoke-static {v3, v1, v2}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v0, v0, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v0}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    iget-object v3, p0, Lin;->k:Landroid/os/Bundle;

    const-string/jumbo v0, "user_id"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    const-string/jumbo v0, "device_follow"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    const-string/jumbo v6, "email_follow"

    invoke-virtual {v3, v6}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v6

    iget-object v7, p0, Lin;->m:Lcom/twitter/library/network/aa;

    iget-object v7, v7, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-string/jumbo v10, "1.1"

    aput-object v10, v8, v9

    const-string/jumbo v9, "favorite_users"

    aput-object v9, v8, v12

    const/4 v9, 0x2

    const-string/jumbo v10, "update"

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string/jumbo v8, ".json"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v9, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v10, "user_id"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v11

    invoke-direct {v9, v10, v11}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v9}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v9, "device_follow"

    invoke-virtual {v3, v9}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v9

    if-eqz v9, :cond_0

    if-eqz v0, :cond_3

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "device"

    const-string/jumbo v10, "true"

    invoke-direct {v0, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    :goto_0
    const-string/jumbo v0, "email_follow"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    if-eqz v6, :cond_4

    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "email"

    const-string/jumbo v9, "default"

    invoke-direct {v0, v6, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    :goto_1
    const/16 v0, 0x1e

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    new-instance v6, Lcom/twitter/library/network/d;

    iget-object v9, p0, Lin;->l:Landroid/content/Context;

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v9, v7}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v7, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v6, v7}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v8}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual {v1, v4, v5, v6}, Lcom/twitter/library/provider/az;->c(JI)V

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/twitter/library/provider/ay;->h(I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string/jumbo v0, "account_name"

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lin;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "notification_not_enabled"

    invoke-virtual {v3, v0, v12}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    :cond_2
    const-string/jumbo v0, "friendship"

    invoke-virtual {v1, v4, v5}, Lcom/twitter/library/provider/az;->h(J)I

    move-result v1

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {p1, v2}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_3
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v9, "device"

    const-string/jumbo v10, "false"

    invoke-direct {v0, v9, v10}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    :cond_4
    new-instance v0, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v6, "email"

    const-string/jumbo v9, "none"

    invoke-direct {v0, v6, v9}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1
.end method


# virtual methods
.method protected b(Lcom/twitter/library/service/e;)V
    .locals 2

    invoke-virtual {p0}, Lin;->r()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string/jumbo v1, "Invalid action"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    invoke-direct {p0, p1}, Lin;->c(Lcom/twitter/library/service/e;)V

    :goto_0
    return-void

    :pswitch_1
    invoke-direct {p0, p1}, Lin;->d(Lcom/twitter/library/service/e;)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0, p1}, Lin;->e(Lcom/twitter/library/service/e;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
