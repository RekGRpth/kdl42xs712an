.class public Lje;
.super Lcom/twitter/library/api/account/e;
.source "Twttr"


# instance fields
.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;I)V
    .locals 1

    const-class v0, Lje;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/api/account/e;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Lje;->d:Ljava/lang/String;

    iput p4, p0, Lje;->e:I

    const/16 v0, 0x39

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Lje;->f:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;Lcom/twitter/library/network/a;)Lcom/twitter/internal/network/HttpOperation;
    .locals 8

    const/4 v2, 0x5

    const/4 v7, 0x1

    invoke-virtual {p0}, Lje;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v3, v0, Lcom/twitter/library/service/p;->c:J

    iget-object v0, p0, Lje;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v1, v5

    const-string/jumbo v5, "beta"

    aput-object v5, v1, v7

    const/4 v5, 0x2

    const-string/jumbo v6, "timelines"

    aput-object v6, v1, v5

    const/4 v5, 0x3

    const-string/jumbo v6, "timeline"

    aput-object v6, v1, v5

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string/jumbo v0, "pc"

    const-string/jumbo v1, "true"

    invoke-static {v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    const-string/jumbo v0, "id"

    iget-object v1, p0, Lje;->d:Ljava/lang/String;

    invoke-static {v6, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    iget v0, p0, Lje;->e:I

    packed-switch v0, :pswitch_data_0

    :cond_0
    :goto_0
    const-string/jumbo v0, "include_cards"

    invoke-static {v6, v0, v7}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-object v0, p0, Lje;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v0, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Lje;->l:Landroid/content/Context;

    invoke-direct {v0, v1, v6}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Lje;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0

    :pswitch_0
    invoke-virtual {p0}, Lje;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    const/16 v1, 0xa

    iget-object v5, p0, Lje;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IIJLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "min_position"

    invoke-static {v6, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {p0}, Lje;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    const/16 v1, 0x9

    iget-object v5, p0, Lje;->d:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IIJLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string/jumbo v1, "max_position"

    invoke-static {v6, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 13

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lje;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    move-object v10, v0

    check-cast v10, Lcom/twitter/library/api/ah;

    if-eqz v10, :cond_0

    iget v0, p0, Lje;->e:I

    packed-switch v0, :pswitch_data_0

    iget-object v8, v10, Lcom/twitter/library/api/ah;->d:Ljava/lang/String;

    iget-object v9, v10, Lcom/twitter/library/api/ah;->e:Ljava/lang/String;

    :goto_0
    invoke-virtual {p0}, Lje;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, v10, Lcom/twitter/library/api/ah;->a:Lcom/twitter/library/api/TwitterTopic;

    iget-object v2, v10, Lcom/twitter/library/api/ah;->b:Lcom/twitter/library/api/TwitterUser;

    iget-object v3, v10, Lcom/twitter/library/api/ah;->c:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lje;->s()Lcom/twitter/library/service/p;

    move-result-object v4

    iget-wide v4, v4, Lcom/twitter/library/service/p;->c:J

    const/4 v6, 0x5

    iget-object v7, p0, Lje;->d:Ljava/lang/String;

    iget v11, p0, Lje;->e:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_1

    iget-object v10, v10, Lcom/twitter/library/api/ah;->c:Ljava/util/ArrayList;

    invoke-virtual {v10}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v10

    if-eqz v10, :cond_1

    const/4 v10, 0x1

    :goto_1
    invoke-virtual/range {v0 .. v10}, Lcom/twitter/library/provider/az;->a(Lcom/twitter/library/api/TwitterTopic;Lcom/twitter/library/api/TwitterUser;Ljava/util/ArrayList;JILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)I

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :pswitch_0
    iget-object v9, v10, Lcom/twitter/library/api/ah;->e:Ljava/lang/String;

    move-object v8, v1

    goto :goto_0

    :pswitch_1
    iget-object v8, v10, Lcom/twitter/library/api/ah;->d:Ljava/lang/String;

    move-object v9, v1

    goto :goto_0

    :cond_1
    const/4 v10, 0x0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
