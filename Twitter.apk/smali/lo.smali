.class public interface abstract Llo;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/spongycastle/asn1/l;

.field public static final b:Lorg/spongycastle/asn1/l;

.field public static final c:Lorg/spongycastle/asn1/l;

.field public static final d:Lorg/spongycastle/asn1/l;

.field public static final e:Lorg/spongycastle/asn1/l;

.field public static final f:Lorg/spongycastle/asn1/l;

.field public static final g:Lorg/spongycastle/asn1/l;

.field public static final h:Lorg/spongycastle/asn1/l;

.field public static final i:Lorg/spongycastle/asn1/l;

.field public static final j:Lorg/spongycastle/asn1/l;

.field public static final k:Lorg/spongycastle/asn1/l;

.field public static final l:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->a:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->b:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.4"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->c:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.6"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->d:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->e:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->f:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.9"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->g:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.17"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->h:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.26"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->i:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.27"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->j:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->k:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.7.2.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llo;->l:Lorg/spongycastle/asn1/l;

    return-void
.end method
