.class public Lkp;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final a:Lkr;

.field b:Z

.field c:I

.field final d:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final e:Lks;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lkr;)V
    .locals 3

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Lkq;

    invoke-direct {v0, p0}, Lkq;-><init>(Lkp;)V

    iput-object v0, p0, Lkp;->d:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    iput-object p2, p0, Lkp;->a:Lkr;

    new-instance v1, Lkt;

    const-string/jumbo v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v0, v2}, Lkt;-><init>(Lkp;Landroid/media/AudioManager;Lkq;)V

    iput-object v1, p0, Lkp;->e:Lks;

    return-void
.end method

.method public static a(II)F
    .locals 6

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    sub-int v2, p0, p1

    int-to-double v2, v2

    invoke-static {v2, v3}, Ljava/lang/Math;->log(D)D

    move-result-wide v2

    int-to-double v4, p0

    invoke-static {v4, v5}, Ljava/lang/Math;->log(D)D

    move-result-wide v4

    div-double/2addr v2, v4

    sub-double/2addr v0, v2

    double-to-float v0, v0

    return v0
.end method


# virtual methods
.method public a()Z
    .locals 4

    const/4 v0, 0x1

    iget-boolean v1, p0, Lkp;->b:Z

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lkp;->c()Lks;

    move-result-object v1

    iget-object v2, p0, Lkp;->d:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    const/4 v3, 0x3

    invoke-interface {v1, v2, v3, v0}, Lks;->a(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v1

    if-ne v1, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lkp;->b:Z

    :cond_0
    iget-boolean v0, p0, Lkp;->b:Z

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-boolean v0, p0, Lkp;->b:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lkp;->c()Lks;

    move-result-object v0

    iget-object v1, p0, Lkp;->d:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-interface {v0, v1}, Lks;->a(Landroid/media/AudioManager$OnAudioFocusChangeListener;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lkp;->b:Z

    :cond_0
    return-void
.end method

.method c()Lks;
    .locals 1

    iget-object v0, p0, Lkp;->e:Lks;

    return-object v0
.end method
