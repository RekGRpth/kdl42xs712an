.class public Lorg/spongycastle/asn1/aj;
.super Lorg/spongycastle/asn1/w;


# direct methods
.method public constructor <init>(ZILorg/spongycastle/asn1/d;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lorg/spongycastle/asn1/w;-><init>(ZILorg/spongycastle/asn1/d;)V

    return-void
.end method


# virtual methods
.method a(Lorg/spongycastle/asn1/o;)V
    .locals 3

    const/4 v2, 0x0

    const/16 v0, 0xa0

    iget v1, p0, Lorg/spongycastle/asn1/aj;->a:I

    invoke-virtual {p1, v0, v1}, Lorg/spongycastle/asn1/o;->a(II)V

    const/16 v0, 0x80

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->b(I)V

    iget-boolean v0, p0, Lorg/spongycastle/asn1/aj;->b:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lorg/spongycastle/asn1/aj;->c:Z

    if-nez v0, :cond_4

    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    instance-of v0, v0, Lorg/spongycastle/asn1/m;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    instance-of v0, v0, Lorg/spongycastle/asn1/ac;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    check-cast v0, Lorg/spongycastle/asn1/ac;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/ac;->j()Ljava/util/Enumeration;

    move-result-object v0

    move-object v1, v0

    :goto_0
    invoke-interface {v1}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/spongycastle/asn1/d;

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    check-cast v0, Lorg/spongycastle/asn1/m;

    new-instance v1, Lorg/spongycastle/asn1/ac;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/m;->d()[B

    move-result-object v0

    invoke-direct {v1, v0}, Lorg/spongycastle/asn1/ac;-><init>([B)V

    invoke-virtual {v1}, Lorg/spongycastle/asn1/ac;->j()Ljava/util/Enumeration;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    instance-of v0, v0, Lorg/spongycastle/asn1/r;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    check-cast v0, Lorg/spongycastle/asn1/r;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/r;->d()Ljava/util/Enumeration;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    instance-of v0, v0, Lorg/spongycastle/asn1/t;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    check-cast v0, Lorg/spongycastle/asn1/t;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/t;->c()Ljava/util/Enumeration;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "not implemented: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/o;->a(Lorg/spongycastle/asn1/d;)V

    :cond_5
    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/o;->b(I)V

    invoke-virtual {p1, v2}, Lorg/spongycastle/asn1/o;->b(I)V

    return-void
.end method

.method h()Z
    .locals 2

    const/4 v0, 0x1

    iget-boolean v1, p0, Lorg/spongycastle/asn1/aj;->b:Z

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/aj;->c:Z

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->f()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->h()Z

    move-result v0

    goto :goto_0
.end method

.method i()I
    .locals 3

    iget-boolean v0, p0, Lorg/spongycastle/asn1/aj;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lorg/spongycastle/asn1/aj;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0}, Lorg/spongycastle/asn1/q;->i()I

    move-result v0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/aj;->c:Z

    if-eqz v1, :cond_0

    iget v1, p0, Lorg/spongycastle/asn1/aj;->a:I

    invoke-static {v1}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v1

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->a(I)I

    move-result v2

    add-int/2addr v1, v2

    add-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, -0x1

    iget v1, p0, Lorg/spongycastle/asn1/aj;->a:I

    invoke-static {v1}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0

    :cond_1
    iget v0, p0, Lorg/spongycastle/asn1/aj;->a:I

    invoke-static {v0}, Lorg/spongycastle/asn1/ca;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
