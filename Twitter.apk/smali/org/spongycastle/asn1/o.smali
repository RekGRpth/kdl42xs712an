.class public Lorg/spongycastle/asn1/o;
.super Ljava/lang/Object;


# instance fields
.field private a:Ljava/io/OutputStream;


# direct methods
.method public constructor <init>(Ljava/io/OutputStream;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    return-void
.end method


# virtual methods
.method a()Lorg/spongycastle/asn1/o;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/bd;

    iget-object v1, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bd;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method a(I)V
    .locals 2

    const/16 v0, 0x7f

    if-le p1, v0, :cond_1

    const/4 v0, 0x1

    move v1, v0

    move v0, p1

    :goto_0
    ushr-int/lit8 v0, v0, 0x8

    if-eqz v0, :cond_0

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    or-int/lit16 v0, v1, 0x80

    int-to-byte v0, v0

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/o;->b(I)V

    add-int/lit8 v0, v1, -0x1

    mul-int/lit8 v0, v0, 0x8

    :goto_1
    if-ltz v0, :cond_2

    shr-int v1, p1, v0

    int-to-byte v1, v1

    invoke-virtual {p0, v1}, Lorg/spongycastle/asn1/o;->b(I)V

    add-int/lit8 v0, v0, -0x8

    goto :goto_1

    :cond_1
    int-to-byte v0, p1

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/o;->b(I)V

    :cond_2
    return-void
.end method

.method a(II)V
    .locals 3

    const/16 v0, 0x1f

    if-ge p2, v0, :cond_0

    or-int v0, p1, p2

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/o;->b(I)V

    :goto_0
    return-void

    :cond_0
    or-int/lit8 v0, p1, 0x1f

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/o;->b(I)V

    const/16 v0, 0x80

    if-ge p2, v0, :cond_1

    invoke-virtual {p0, p2}, Lorg/spongycastle/asn1/o;->b(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x5

    new-array v1, v0, [B

    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    and-int/lit8 v2, p2, 0x7f

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    :cond_2
    shr-int/lit8 p2, p2, 0x7

    add-int/lit8 v0, v0, -0x1

    and-int/lit8 v2, p2, 0x7f

    or-int/lit16 v2, v2, 0x80

    int-to-byte v2, v2

    aput-byte v2, v1, v0

    const/16 v2, 0x7f

    if-gt p2, v2, :cond_2

    array-length v2, v1

    sub-int/2addr v2, v0

    invoke-virtual {p0, v1, v0, v2}, Lorg/spongycastle/asn1/o;->a([BII)V

    goto :goto_0
.end method

.method a(II[B)V
    .locals 1

    invoke-virtual {p0, p1, p2}, Lorg/spongycastle/asn1/o;->a(II)V

    array-length v0, p3

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/o;->a(I)V

    invoke-virtual {p0, p3}, Lorg/spongycastle/asn1/o;->a([B)V

    return-void
.end method

.method a(I[B)V
    .locals 1

    invoke-virtual {p0, p1}, Lorg/spongycastle/asn1/o;->b(I)V

    array-length v0, p2

    invoke-virtual {p0, v0}, Lorg/spongycastle/asn1/o;->a(I)V

    invoke-virtual {p0, p2}, Lorg/spongycastle/asn1/o;->a([B)V

    return-void
.end method

.method public a(Lorg/spongycastle/asn1/d;)V
    .locals 2

    if-eqz p1, :cond_0

    invoke-interface {p1}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v0, p0}, Lorg/spongycastle/asn1/q;->a(Lorg/spongycastle/asn1/o;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "null object detected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a(Lorg/spongycastle/asn1/q;)V
    .locals 2

    if-eqz p1, :cond_0

    new-instance v0, Lorg/spongycastle/asn1/p;

    iget-object v1, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    invoke-direct {v0, p0, v1}, Lorg/spongycastle/asn1/p;-><init>(Lorg/spongycastle/asn1/o;Ljava/io/OutputStream;)V

    invoke-virtual {p1, v0}, Lorg/spongycastle/asn1/q;->a(Lorg/spongycastle/asn1/o;)V

    return-void

    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "null object detected"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method a([B)V
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write([B)V

    return-void
.end method

.method a([BII)V
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1, p2, p3}, Ljava/io/OutputStream;->write([BII)V

    return-void
.end method

.method b()Lorg/spongycastle/asn1/o;
    .locals 2

    new-instance v0, Lorg/spongycastle/asn1/bp;

    iget-object v1, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bp;-><init>(Ljava/io/OutputStream;)V

    return-object v0
.end method

.method b(I)V
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/o;->a:Ljava/io/OutputStream;

    invoke-virtual {v0, p1}, Ljava/io/OutputStream;->write(I)V

    return-void
.end method
