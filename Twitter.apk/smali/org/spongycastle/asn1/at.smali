.class Lorg/spongycastle/asn1/at;
.super Ljava/lang/Object;


# static fields
.field static final a:Lorg/spongycastle/asn1/r;

.field static final b:Lorg/spongycastle/asn1/t;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lorg/spongycastle/asn1/bf;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bf;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/at;->a:Lorg/spongycastle/asn1/r;

    new-instance v0, Lorg/spongycastle/asn1/bh;

    invoke-direct {v0}, Lorg/spongycastle/asn1/bh;-><init>()V

    sput-object v0, Lorg/spongycastle/asn1/at;->b:Lorg/spongycastle/asn1/t;

    return-void
.end method

.method static a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/r;
    .locals 2

    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/at;->a:Lorg/spongycastle/asn1/r;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/bq;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/bq;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method

.method static b(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/t;
    .locals 2

    invoke-virtual {p0}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    const/4 v1, 0x1

    if-ge v0, v1, :cond_0

    sget-object v0, Lorg/spongycastle/asn1/at;->b:Lorg/spongycastle/asn1/t;

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lorg/spongycastle/asn1/br;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/br;-><init>(Lorg/spongycastle/asn1/e;)V

    goto :goto_0
.end method
