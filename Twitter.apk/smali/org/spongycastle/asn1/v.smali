.class public Lorg/spongycastle/asn1/v;
.super Ljava/lang/Object;


# instance fields
.field private final a:Ljava/io/InputStream;

.field private final b:I

.field private final c:[[B


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 1

    invoke-static {p1}, Lorg/spongycastle/asn1/ca;->a(Ljava/io/InputStream;)I

    move-result v0

    invoke-direct {p0, p1, v0}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;I)V

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    iput p2, p0, Lorg/spongycastle/asn1/v;->b:I

    const/16 v0, 0xb

    new-array v0, v0, [[B

    iput-object v0, p0, Lorg/spongycastle/asn1/v;->c:[[B

    return-void
.end method

.method private a(Z)V
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    instance-of v0, v0, Lorg/spongycastle/asn1/bv;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    check-cast v0, Lorg/spongycastle/asn1/bv;

    invoke-virtual {v0, p1}, Lorg/spongycastle/asn1/bv;->a(Z)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()Lorg/spongycastle/asn1/d;
    .locals 6

    const/4 v1, 0x1

    const/4 v0, 0x0

    iget-object v2, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    invoke-virtual {v2}, Ljava/io/InputStream;->read()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, v0}, Lorg/spongycastle/asn1/v;->a(Z)V

    iget-object v3, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    invoke-static {v3, v2}, Lorg/spongycastle/asn1/h;->a(Ljava/io/InputStream;I)I

    move-result v3

    and-int/lit8 v4, v2, 0x20

    if-eqz v4, :cond_1

    move v0, v1

    :cond_1
    iget-object v4, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    iget v5, p0, Lorg/spongycastle/asn1/v;->b:I

    invoke-static {v4, v5}, Lorg/spongycastle/asn1/h;->b(Ljava/io/InputStream;I)I

    move-result v4

    if-gez v4, :cond_5

    if-nez v0, :cond_2

    new-instance v0, Ljava/io/IOException;

    const-string/jumbo v1, "indefinite length primitive encoding encountered"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    new-instance v0, Lorg/spongycastle/asn1/bv;

    iget-object v4, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    iget v5, p0, Lorg/spongycastle/asn1/v;->b:I

    invoke-direct {v0, v4, v5}, Lorg/spongycastle/asn1/bv;-><init>(Ljava/io/InputStream;I)V

    new-instance v4, Lorg/spongycastle/asn1/v;

    iget v5, p0, Lorg/spongycastle/asn1/v;->b:I

    invoke-direct {v4, v0, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;I)V

    and-int/lit8 v0, v2, 0x40

    if-eqz v0, :cond_3

    new-instance v0, Lorg/spongycastle/asn1/aa;

    invoke-direct {v0, v3, v4}, Lorg/spongycastle/asn1/aa;-><init>(ILorg/spongycastle/asn1/v;)V

    goto :goto_0

    :cond_3
    and-int/lit16 v0, v2, 0x80

    if-eqz v0, :cond_4

    new-instance v0, Lorg/spongycastle/asn1/ak;

    invoke-direct {v0, v1, v3, v4}, Lorg/spongycastle/asn1/ak;-><init>(ZILorg/spongycastle/asn1/v;)V

    goto :goto_0

    :cond_4
    invoke-virtual {v4, v3}, Lorg/spongycastle/asn1/v;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v0

    goto :goto_0

    :cond_5
    new-instance v5, Lorg/spongycastle/asn1/bt;

    iget-object v1, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    invoke-direct {v5, v1, v4}, Lorg/spongycastle/asn1/bt;-><init>(Ljava/io/InputStream;I)V

    and-int/lit8 v1, v2, 0x40

    if-eqz v1, :cond_6

    new-instance v1, Lorg/spongycastle/asn1/am;

    invoke-virtual {v5}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v2

    invoke-direct {v1, v0, v3, v2}, Lorg/spongycastle/asn1/am;-><init>(ZI[B)V

    move-object v0, v1

    goto :goto_0

    :cond_6
    and-int/lit16 v1, v2, 0x80

    if-eqz v1, :cond_7

    new-instance v1, Lorg/spongycastle/asn1/ak;

    new-instance v2, Lorg/spongycastle/asn1/v;

    invoke-direct {v2, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v1, v0, v3, v2}, Lorg/spongycastle/asn1/ak;-><init>(ZILorg/spongycastle/asn1/v;)V

    move-object v0, v1

    goto :goto_0

    :cond_7
    if-eqz v0, :cond_8

    sparse-switch v3, :sswitch_data_0

    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown tag "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, " encountered"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-instance v0, Lorg/spongycastle/asn1/ae;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ae;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    :sswitch_1
    new-instance v0, Lorg/spongycastle/asn1/bg;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bg;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    :sswitch_2
    new-instance v0, Lorg/spongycastle/asn1/bi;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/bi;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    :sswitch_3
    new-instance v0, Lorg/spongycastle/asn1/as;

    new-instance v1, Lorg/spongycastle/asn1/v;

    invoke-direct {v1, v5}, Lorg/spongycastle/asn1/v;-><init>(Ljava/io/InputStream;)V

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/as;-><init>(Lorg/spongycastle/asn1/v;)V

    goto/16 :goto_0

    :cond_8
    packed-switch v3, :pswitch_data_0

    :try_start_0
    iget-object v0, p0, Lorg/spongycastle/asn1/v;->c:[[B

    invoke-static {v3, v5, v0}, Lorg/spongycastle/asn1/h;->a(ILorg/spongycastle/asn1/bt;[[B)Lorg/spongycastle/asn1/q;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto/16 :goto_0

    :pswitch_0
    new-instance v0, Lorg/spongycastle/asn1/bc;

    invoke-direct {v0, v5}, Lorg/spongycastle/asn1/bc;-><init>(Lorg/spongycastle/asn1/bt;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    new-instance v1, Lorg/spongycastle/asn1/ASN1Exception;

    const-string/jumbo v2, "corrupted stream detected"

    invoke-direct {v1, v2, v0}, Lorg/spongycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_0
        0x8 -> :sswitch_3
        0x10 -> :sswitch_1
        0x11 -> :sswitch_2
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
    .end packed-switch
.end method

.method a(I)Lorg/spongycastle/asn1/d;
    .locals 3

    sparse-switch p1, :sswitch_data_0

    new-instance v0, Lorg/spongycastle/asn1/ASN1Exception;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "unknown BER object encountered: 0x"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/ASN1Exception;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    new-instance v0, Lorg/spongycastle/asn1/as;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/as;-><init>(Lorg/spongycastle/asn1/v;)V

    :goto_0
    return-object v0

    :sswitch_1
    new-instance v0, Lorg/spongycastle/asn1/ae;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/ae;-><init>(Lorg/spongycastle/asn1/v;)V

    goto :goto_0

    :sswitch_2
    new-instance v0, Lorg/spongycastle/asn1/ag;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/ag;-><init>(Lorg/spongycastle/asn1/v;)V

    goto :goto_0

    :sswitch_3
    new-instance v0, Lorg/spongycastle/asn1/ai;

    invoke-direct {v0, p0}, Lorg/spongycastle/asn1/ai;-><init>(Lorg/spongycastle/asn1/v;)V

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x4 -> :sswitch_1
        0x8 -> :sswitch_0
        0x10 -> :sswitch_2
        0x11 -> :sswitch_3
    .end sparse-switch
.end method

.method a(ZI)Lorg/spongycastle/asn1/q;
    .locals 4

    const/4 v2, 0x1

    const/4 v3, 0x0

    if-nez p1, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    check-cast v0, Lorg/spongycastle/asn1/bt;

    new-instance v1, Lorg/spongycastle/asn1/bk;

    new-instance v2, Lorg/spongycastle/asn1/bb;

    invoke-virtual {v0}, Lorg/spongycastle/asn1/bt;->b()[B

    move-result-object v0

    invoke-direct {v2, v0}, Lorg/spongycastle/asn1/bb;-><init>([B)V

    invoke-direct {v1, v3, p2, v2}, Lorg/spongycastle/asn1/bk;-><init>(ZILorg/spongycastle/asn1/d;)V

    move-object v0, v1

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/v;->b()Lorg/spongycastle/asn1/e;

    move-result-object v1

    iget-object v0, p0, Lorg/spongycastle/asn1/v;->a:Ljava/io/InputStream;

    instance-of v0, v0, Lorg/spongycastle/asn1/bv;

    if-eqz v0, :cond_2

    invoke-virtual {v1}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    if-ne v0, v2, :cond_1

    new-instance v0, Lorg/spongycastle/asn1/aj;

    invoke-virtual {v1, v3}, Lorg/spongycastle/asn1/e;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-direct {v0, v2, p2, v1}, Lorg/spongycastle/asn1/aj;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lorg/spongycastle/asn1/aj;

    invoke-static {v1}, Lorg/spongycastle/asn1/ab;->a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/af;

    move-result-object v1

    invoke-direct {v0, v3, p2, v1}, Lorg/spongycastle/asn1/aj;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Lorg/spongycastle/asn1/e;->a()I

    move-result v0

    if-ne v0, v2, :cond_3

    new-instance v0, Lorg/spongycastle/asn1/bk;

    invoke-virtual {v1, v3}, Lorg/spongycastle/asn1/e;->a(I)Lorg/spongycastle/asn1/d;

    move-result-object v1

    invoke-direct {v0, v2, p2, v1}, Lorg/spongycastle/asn1/bk;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_3
    new-instance v0, Lorg/spongycastle/asn1/bk;

    invoke-static {v1}, Lorg/spongycastle/asn1/at;->a(Lorg/spongycastle/asn1/e;)Lorg/spongycastle/asn1/r;

    move-result-object v1

    invoke-direct {v0, v3, p2, v1}, Lorg/spongycastle/asn1/bk;-><init>(ZILorg/spongycastle/asn1/d;)V

    goto :goto_0
.end method

.method b()Lorg/spongycastle/asn1/e;
    .locals 3

    new-instance v1, Lorg/spongycastle/asn1/e;

    invoke-direct {v1}, Lorg/spongycastle/asn1/e;-><init>()V

    :goto_0
    invoke-virtual {p0}, Lorg/spongycastle/asn1/v;->a()Lorg/spongycastle/asn1/d;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v2, v0, Lorg/spongycastle/asn1/bu;

    if-eqz v2, :cond_0

    check-cast v0, Lorg/spongycastle/asn1/bu;

    invoke-interface {v0}, Lorg/spongycastle/asn1/bu;->e()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_0
    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    invoke-virtual {v1, v0}, Lorg/spongycastle/asn1/e;->a(Lorg/spongycastle/asn1/d;)V

    goto :goto_0

    :cond_1
    return-object v1
.end method
