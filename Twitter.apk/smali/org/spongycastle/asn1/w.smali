.class public abstract Lorg/spongycastle/asn1/w;
.super Lorg/spongycastle/asn1/q;

# interfaces
.implements Lorg/spongycastle/asn1/x;


# instance fields
.field a:I

.field b:Z

.field c:Z

.field d:Lorg/spongycastle/asn1/d;


# direct methods
.method public constructor <init>(ZILorg/spongycastle/asn1/d;)V
    .locals 2

    const/4 v1, 0x1

    invoke-direct {p0}, Lorg/spongycastle/asn1/q;-><init>()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lorg/spongycastle/asn1/w;->b:Z

    iput-boolean v1, p0, Lorg/spongycastle/asn1/w;->c:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    instance-of v0, p3, Lorg/spongycastle/asn1/c;

    if-eqz v0, :cond_0

    iput-boolean v1, p0, Lorg/spongycastle/asn1/w;->c:Z

    :goto_0
    iput p2, p0, Lorg/spongycastle/asn1/w;->a:I

    iget-boolean v0, p0, Lorg/spongycastle/asn1/w;->c:Z

    if-eqz v0, :cond_1

    iput-object p3, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    :goto_1
    return-void

    :cond_0
    iput-boolean p1, p0, Lorg/spongycastle/asn1/w;->c:Z

    goto :goto_0

    :cond_1
    invoke-interface {p3}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    instance-of v0, v0, Lorg/spongycastle/asn1/t;

    if-eqz v0, :cond_2

    :cond_2
    iput-object p3, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    goto :goto_1
.end method


# virtual methods
.method abstract a(Lorg/spongycastle/asn1/o;)V
.end method

.method a(Lorg/spongycastle/asn1/q;)Z
    .locals 3

    const/4 v0, 0x0

    instance-of v1, p1, Lorg/spongycastle/asn1/w;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    check-cast p1, Lorg/spongycastle/asn1/w;

    iget v1, p0, Lorg/spongycastle/asn1/w;->a:I

    iget v2, p1, Lorg/spongycastle/asn1/w;->a:I

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/w;->b:Z

    iget-boolean v2, p1, Lorg/spongycastle/asn1/w;->b:Z

    if-ne v1, v2, :cond_0

    iget-boolean v1, p0, Lorg/spongycastle/asn1/w;->c:Z

    iget-boolean v2, p1, Lorg/spongycastle/asn1/w;->c:Z

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    if-nez v1, :cond_3

    iget-object v1, p1, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    if-nez v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v1}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v1

    iget-object v2, p1, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v2}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v2

    invoke-virtual {v1, v2}, Lorg/spongycastle/asn1/q;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    goto :goto_0
.end method

.method public c()I
    .locals 1

    iget v0, p0, Lorg/spongycastle/asn1/w;->a:I

    return v0
.end method

.method public d()Z
    .locals 1

    iget-boolean v0, p0, Lorg/spongycastle/asn1/w;->c:Z

    return v0
.end method

.method public e()Lorg/spongycastle/asn1/q;
    .locals 1

    invoke-virtual {p0}, Lorg/spongycastle/asn1/w;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    return-object v0
.end method

.method f()Lorg/spongycastle/asn1/q;
    .locals 4

    new-instance v0, Lorg/spongycastle/asn1/bk;

    iget-boolean v1, p0, Lorg/spongycastle/asn1/w;->c:Z

    iget v2, p0, Lorg/spongycastle/asn1/w;->a:I

    iget-object v3, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-direct {v0, v1, v2, v3}, Lorg/spongycastle/asn1/bk;-><init>(ZILorg/spongycastle/asn1/d;)V

    return-object v0
.end method

.method g()Lorg/spongycastle/asn1/q;
    .locals 4

    new-instance v0, Lorg/spongycastle/asn1/bs;

    iget-boolean v1, p0, Lorg/spongycastle/asn1/w;->c:Z

    iget v2, p0, Lorg/spongycastle/asn1/w;->a:I

    iget-object v3, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-direct {v0, v1, v2, v3}, Lorg/spongycastle/asn1/bs;-><init>(ZILorg/spongycastle/asn1/d;)V

    return-object v0
.end method

.method public hashCode()I
    .locals 2

    iget v0, p0, Lorg/spongycastle/asn1/w;->a:I

    iget-object v1, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    :cond_0
    return v0
.end method

.method public j()Lorg/spongycastle/asn1/q;
    .locals 1

    iget-object v0, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-interface {v0}, Lorg/spongycastle/asn1/d;->a()Lorg/spongycastle/asn1/q;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lorg/spongycastle/asn1/w;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lorg/spongycastle/asn1/w;->d:Lorg/spongycastle/asn1/d;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
