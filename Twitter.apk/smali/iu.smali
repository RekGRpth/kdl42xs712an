.class public Liu;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;

.field private final f:Lcom/twitter/library/api/ao;

.field private g:Lcom/twitter/library/api/conversations/v;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/List;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Liu;-><init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/List;Ljava/util/List;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    const-class v0, Liu;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    if-nez p3, :cond_0

    new-instance p3, Ljava/util/ArrayList;

    invoke-direct {p3}, Ljava/util/ArrayList;-><init>()V

    :cond_0
    iput-object p3, p0, Liu;->d:Ljava/util/List;

    if-nez p4, :cond_1

    new-instance p4, Ljava/util/ArrayList;

    invoke-direct {p4}, Ljava/util/ArrayList;-><init>()V

    :cond_1
    iput-object p4, p0, Liu;->e:Ljava/util/List;

    const/16 v0, 0x4c

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Liu;->f:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 6

    const/16 v5, 0x64

    const/4 v4, 0x0

    const-string/jumbo v0, "https://api-staging1.smf1.twitter.com"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v4

    const/4 v2, 0x1

    const-string/jumbo v3, "dm"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string/jumbo v3, "permissions"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v0, p0, Liu;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_0

    const-string/jumbo v0, "recipient_ids"

    iget-object v2, p0, Liu;->d:Ljava/util/List;

    invoke-static {v2}, Lcom/twitter/library/util/Util;->b(Ljava/util/Collection;)[J

    move-result-object v2

    invoke-static {v1, v0, v2, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[JII)I

    :cond_0
    iget-object v0, p0, Liu;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_1

    const-string/jumbo v2, "recipient_screen_names"

    iget-object v0, p0, Liu;->e:Ljava/util/List;

    iget-object v3, p0, Liu;->e:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-static {v1, v2, v0, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;[Ljava/lang/String;II)I

    :cond_1
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v2, p0, Liu;->l:Landroid/content/Context;

    invoke-direct {v0, v2, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v1, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->a:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/n;

    invoke-virtual {p0}, Liu;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Liu;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 10

    const-wide/16 v2, -0x1

    const/4 v7, 0x0

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Liu;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/twitter/library/api/conversations/v;

    iput-object v0, p0, Liu;->g:Lcom/twitter/library/api/conversations/v;

    invoke-virtual {p0}, Liu;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Liu;->g:Lcom/twitter/library/api/conversations/v;

    iget-object v1, v1, Lcom/twitter/library/api/conversations/v;->c:Ljava/util/List;

    const/4 v4, -0x1

    const/4 v9, 0x1

    move-wide v5, v2

    move-object v8, v7

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method

.method public e()Lcom/twitter/library/api/conversations/v;
    .locals 1

    iget-object v0, p0, Liu;->g:Lcom/twitter/library/api/conversations/v;

    return-object v0
.end method
