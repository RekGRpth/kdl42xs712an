.class public Lkf;
.super Lkb;
.source "Twttr"

# interfaces
.implements Landroid/widget/AdapterView$OnItemClickListener;


# instance fields
.field private final a:Lcom/twitter/library/view/i;

.field private b:Landroid/widget/AdapterView;

.field private c:I

.field private d:J


# direct methods
.method public constructor <init>(Lcom/twitter/library/view/i;)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lkf;-><init>(Lcom/twitter/library/view/i;Z)V

    return-void
.end method

.method public constructor <init>(Lcom/twitter/library/view/i;Z)V
    .locals 0

    invoke-direct {p0, p2}, Lkb;-><init>(Z)V

    iput-object p1, p0, Lkf;->a:Lcom/twitter/library/view/i;

    return-void
.end method


# virtual methods
.method protected b(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lkf;->a:Lcom/twitter/library/view/i;

    iget-object v1, p0, Lkf;->b:Landroid/widget/AdapterView;

    iget v3, p0, Lkf;->c:I

    iget-wide v4, p0, Lkf;->d:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/view/i;->onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    return-void
.end method

.method protected c(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lkf;->a:Lcom/twitter/library/view/i;

    iget-object v1, p0, Lkf;->b:Landroid/widget/AdapterView;

    iget v3, p0, Lkf;->c:I

    iget-wide v4, p0, Lkf;->d:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/view/i;->b(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    return-void
.end method

.method protected d(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lkf;->a:Lcom/twitter/library/view/i;

    iget-object v1, p0, Lkf;->b:Landroid/widget/AdapterView;

    iget v3, p0, Lkf;->c:I

    iget-wide v4, p0, Lkf;->d:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/view/i;->c(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lkf;->b:Landroid/widget/AdapterView;

    return-void
.end method

.method protected e(Landroid/view/View;)V
    .locals 6

    iget-object v0, p0, Lkf;->a:Lcom/twitter/library/view/i;

    iget-object v1, p0, Lkf;->b:Landroid/widget/AdapterView;

    iget v3, p0, Lkf;->c:I

    iget-wide v4, p0, Lkf;->d:J

    move-object v2, p1

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/view/i;->d(Landroid/widget/AdapterView;Landroid/view/View;IJ)V

    const/4 v0, 0x0

    iput-object v0, p0, Lkf;->b:Landroid/widget/AdapterView;

    return-void
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 6

    iput-object p1, p0, Lkf;->b:Landroid/widget/AdapterView;

    iput p3, p0, Lkf;->c:I

    iput-wide p4, p0, Lkf;->d:J

    iget-object v0, p0, Lkf;->a:Lcom/twitter/library/view/i;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-wide v4, p4

    invoke-interface/range {v0 .. v5}, Lcom/twitter/library/view/i;->a(Landroid/widget/AdapterView;Landroid/view/View;IJ)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p2}, Lkf;->a(Landroid/view/View;)V

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0, p2}, Lkf;->b(Landroid/view/View;)V

    goto :goto_0
.end method
