.class public Ljq;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:I

.field private final e:[D

.field private final f:Lcom/twitter/library/api/ao;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;I[D)V
    .locals 1

    const-class v0, Ljq;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput p3, p0, Ljq;->d:I

    iput-object p4, p0, Ljq;->e:[D

    const/16 v0, 0x3c

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Ljq;->f:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 10

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v0, 0x0

    iget-object v1, p0, Ljq;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v2, v9, [Ljava/lang/Object;

    const-string/jumbo v3, "1.1"

    aput-object v3, v2, v0

    const-string/jumbo v3, "discover"

    aput-object v3, v2, v7

    const-string/jumbo v3, "nearby"

    aput-object v3, v2, v8

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".json"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Ljq;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-wide v3, v2, Lcom/twitter/library/service/p;->c:J

    const-string/jumbo v5, "user_id"

    invoke-static {v1, v5, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    iget v5, p0, Ljq;->d:I

    if-nez v5, :cond_1

    const-string/jumbo v5, "count"

    const/16 v6, 0xa

    invoke-static {v1, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :goto_0
    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    const-string/jumbo v6, "lat1"

    aput-object v6, v5, v0

    const-string/jumbo v6, "long1"

    aput-object v6, v5, v7

    const-string/jumbo v6, "lat2"

    aput-object v6, v5, v8

    const-string/jumbo v6, "long2"

    aput-object v6, v5, v9

    :goto_1
    array-length v6, v5

    if-ge v0, v6, :cond_2

    iget-object v6, p0, Ljq;->e:[D

    aget-wide v6, v6, v0

    invoke-static {v6, v7}, Ljava/lang/Double;->isNaN(D)Z

    move-result v6

    if-nez v6, :cond_0

    aget-object v6, v5, v0

    iget-object v7, p0, Ljq;->e:[D

    aget-wide v7, v7, v0

    invoke-static {v1, v6, v7, v8}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;D)V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    const-string/jumbo v5, "count"

    iget v6, p0, Ljq;->d:I

    invoke-static {v1, v5, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v5, p0, Ljq;->l:Landroid/content/Context;

    invoke-direct {v0, v5, v1}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    invoke-virtual {v0, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v1, Lcom/twitter/library/network/n;

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Ljq;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 4

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Ljq;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {p0}, Ljq;->w()Lcom/twitter/library/provider/az;

    move-result-object v1

    invoke-virtual {p0}, Ljq;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v1, v2, v3, v0}, Lcom/twitter/library/provider/az;->b(JLjava/util/ArrayList;)I

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void
.end method
