.class public Leo;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field public a:Landroid/graphics/Point;

.field public b:Landroid/graphics/Point;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Leo;->a:Landroid/graphics/Point;

    return-object v0
.end method

.method public a(Landroid/graphics/Point;)V
    .locals 0

    iput-object p1, p0, Leo;->a:Landroid/graphics/Point;

    return-void
.end method

.method public a(Leo;Z)Z
    .locals 4

    const/4 v1, 0x1

    const/4 v0, 0x0

    if-eqz p2, :cond_3

    invoke-virtual {p0}, Leo;->a()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Leo;->a()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {p0}, Leo;->b()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Leo;->b()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_0
    invoke-virtual {p0}, Leo;->a()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Leo;->b()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Leo;->b()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Leo;->a()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Leo;->a()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Leo;->a()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Leo;->b()Landroid/graphics/Point;

    move-result-object v2

    invoke-virtual {p1}, Leo;->b()Landroid/graphics/Point;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Point;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public b()Landroid/graphics/Point;
    .locals 1

    iget-object v0, p0, Leo;->b:Landroid/graphics/Point;

    return-object v0
.end method

.method public b(Landroid/graphics/Point;)V
    .locals 0

    iput-object p1, p0, Leo;->b:Landroid/graphics/Point;

    return-void
.end method
