.class final Lp;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field protected final a:Lt;

.field protected final b:Lp;

.field private final c:I


# direct methods
.method constructor <init>(Lt;Lp;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lp;->a:Lt;

    iput-object p2, p0, Lp;->b:Lp;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput v0, p0, Lp;->c:I

    return-void

    :cond_0
    iget v0, p2, Lp;->c:I

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lp;->c:I

    return v0
.end method

.method public a(III)Lt;
    .locals 3

    iget-object v0, p0, Lp;->a:Lt;

    invoke-virtual {v0}, Lt;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lp;->a:Lt;

    invoke-virtual {v0, p2, p3}, Lt;->a(II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lp;->a:Lt;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lp;->b:Lp;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, v1, Lp;->a:Lt;

    invoke-virtual {v0}, Lt;->hashCode()I

    move-result v2

    if-ne v2, p1, :cond_2

    invoke-virtual {v0, p2, p3}, Lt;->a(II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v0, v1, Lp;->b:Lp;

    move-object v1, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public a(I[II)Lt;
    .locals 3

    iget-object v0, p0, Lp;->a:Lt;

    invoke-virtual {v0}, Lt;->hashCode()I

    move-result v0

    if-ne v0, p1, :cond_1

    iget-object v0, p0, Lp;->a:Lt;

    invoke-virtual {v0, p2, p3}, Lt;->a([II)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lp;->a:Lt;

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lp;->b:Lp;

    move-object v1, v0

    :goto_1
    if-eqz v1, :cond_3

    iget-object v0, v1, Lp;->a:Lt;

    invoke-virtual {v0}, Lt;->hashCode()I

    move-result v2

    if-ne v2, p1, :cond_2

    invoke-virtual {v0, p2, p3}, Lt;->a([II)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_2
    iget-object v0, v1, Lp;->b:Lp;

    move-object v1, v0

    goto :goto_1

    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
