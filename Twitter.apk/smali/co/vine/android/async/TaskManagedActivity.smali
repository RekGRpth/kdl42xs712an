.class public Lco/vine/android/async/TaskManagedActivity;
.super Landroid/app/Activity;
.source "Twttr"


# instance fields
.field private final a:Lco/vine/android/async/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    new-instance v0, Lco/vine/android/async/c;

    invoke-direct {v0, p0}, Lco/vine/android/async/c;-><init>(Landroid/app/Activity;)V

    iput-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    invoke-super {p0, p1, p2, p3}, Landroid/app/Activity;->onActivityResult(IILandroid/content/Intent;)V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0, p1, p2, p3}, Lco/vine/android/async/c;->a(IILandroid/content/Intent;)V

    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0, p1}, Lco/vine/android/async/c;->a(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onDestroy()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0}, Lco/vine/android/async/c;->c()V

    return-void
.end method

.method protected onPause()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0}, Lco/vine/android/async/c;->b()V

    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onRestoreInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0, p1}, Lco/vine/android/async/c;->c(Landroid/os/Bundle;)V

    return-void
.end method

.method protected onResume()V
    .locals 1

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0}, Lco/vine/android/async/c;->a()V

    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 1

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lco/vine/android/async/TaskManagedActivity;->a:Lco/vine/android/async/c;

    invoke-virtual {v0, p1}, Lco/vine/android/async/c;->b(Landroid/os/Bundle;)V

    return-void
.end method
