.class Lco/vine/android/player/u;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/view/TextureView$SurfaceTextureListener;


# instance fields
.field final synthetic a:Lco/vine/android/player/SdkVideoView;


# direct methods
.method constructor <init>(Lco/vine/android/player/SdkVideoView;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSurfaceTextureAvailable(Landroid/graphics/SurfaceTexture;II)V
    .locals 2

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;Landroid/view/Surface;)Landroid/view/Surface;

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->m(Lco/vine/android/player/SdkVideoView;)V

    return-void
.end method

.method public onSurfaceTextureDestroyed(Landroid/graphics/SurfaceTexture;)Z
    .locals 3

    const/4 v2, 0x1

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;Landroid/view/Surface;)Landroid/view/Surface;

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0, v2}, Lco/vine/android/player/SdkVideoView;->d(Lco/vine/android/player/SdkVideoView;Z)V

    return v2
.end method

.method public onSurfaceTextureSizeChanged(Landroid/graphics/SurfaceTexture;II)V
    .locals 4

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0, p2}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0, p3}, Lco/vine/android/player/SdkVideoView;->f(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->i(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    move v0, v1

    :goto_0
    iget-object v3, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v3}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;)I

    move-result v3

    if-ne v3, p2, :cond_3

    iget-object v3, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v3}, Lco/vine/android/player/SdkVideoView;->b(Lco/vine/android/player/SdkVideoView;)I

    move-result v3

    if-ne v3, p3, :cond_3

    :goto_1
    iget-object v2, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v2}, Lco/vine/android/player/SdkVideoView;->d(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer;

    move-result-object v2

    if-eqz v2, :cond_1

    if-eqz v0, :cond_1

    if-eqz v1, :cond_1

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->f(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    iget-object v1, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->f(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->seekTo(I)V

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->start()V

    :cond_1
    return-void

    :cond_2
    move v0, v2

    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method public onSurfaceTextureUpdated(Landroid/graphics/SurfaceTexture;)V
    .locals 2

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    new-instance v1, Landroid/view/Surface;

    invoke-direct {v1, p1}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;Landroid/view/Surface;)Landroid/view/Surface;

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->d(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->i(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->f(Lco/vine/android/player/SdkVideoView;)I

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    iget-object v1, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->f(Lco/vine/android/player/SdkVideoView;)I

    move-result v1

    invoke-virtual {v0, v1}, Lco/vine/android/player/SdkVideoView;->seekTo(I)V

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/u;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->start()V

    :cond_1
    return-void
.end method
