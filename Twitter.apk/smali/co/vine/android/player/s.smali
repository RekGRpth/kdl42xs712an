.class Lco/vine/android/player/s;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;


# instance fields
.field final synthetic a:Lco/vine/android/player/SdkVideoView;


# direct methods
.method constructor <init>(Lco/vine/android/player/SdkVideoView;)V
    .locals 0

    iput-object p1, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5

    const/4 v4, 0x1

    const/4 v3, -0x1

    invoke-static {}, Lco/vine/android/player/SdkVideoView;->d()Ljava/lang/String;

    move-result-object v0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v2, "Error: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    iput v3, v0, Lco/vine/android/player/SdkVideoView;->a:I

    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0, v3}, Lco/vine/android/player/SdkVideoView;->c(Lco/vine/android/player/SdkVideoView;I)I

    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lco/vine/android/player/SdkVideoView;->a(Lco/vine/android/player/SdkVideoView;Ljava/lang/String;)Ljava/lang/String;

    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->e(Lco/vine/android/player/SdkVideoView;)Landroid/widget/MediaController;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/MediaController;->hide()V

    :cond_0
    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->k(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v0}, Lco/vine/android/player/SdkVideoView;->k(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer$OnErrorListener;

    move-result-object v0

    iget-object v1, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->d(Lco/vine/android/player/SdkVideoView;)Landroid/media/MediaPlayer;

    move-result-object v1

    invoke-interface {v0, v1, p2, p3}, Landroid/media/MediaPlayer$OnErrorListener;->onError(Landroid/media/MediaPlayer;II)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    :goto_0
    return v4

    :cond_2
    iget-object v0, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-virtual {v0}, Lco/vine/android/player/SdkVideoView;->getWindowToken()Landroid/os/IBinder;

    move-result-object v0

    if-eqz v0, :cond_1

    const/16 v0, 0xc8

    if-ne p2, v0, :cond_3

    const-string/jumbo v0, "This video cannot be played due to invalid progressive playback."

    :goto_1
    iget-object v1, p0, Lco/vine/android/player/s;->a:Lco/vine/android/player/SdkVideoView;

    invoke-static {v1}, Lco/vine/android/player/SdkVideoView;->l(Lco/vine/android/player/SdkVideoView;)Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    :cond_3
    const-string/jumbo v0, "This video cannot be played due to an unknown error."

    goto :goto_1
.end method
