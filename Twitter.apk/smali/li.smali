.class public final Lli;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Ljava/io/Closeable;


# instance fields
.field private final a:[Ljava/io/InputStream;

.field private final b:[Ljava/io/File;


# direct methods
.method private constructor <init>([Ljava/io/InputStream;[Ljava/io/File;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lli;->a:[Ljava/io/InputStream;

    iput-object p2, p0, Lli;->b:[Ljava/io/File;

    return-void
.end method

.method synthetic constructor <init>([Ljava/io/InputStream;[Ljava/io/File;Lle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lli;-><init>([Ljava/io/InputStream;[Ljava/io/File;)V

    return-void
.end method


# virtual methods
.method public a(I)Ljava/io/InputStream;
    .locals 1

    iget-object v0, p0, Lli;->a:[Ljava/io/InputStream;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public b(I)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lli;->b:[Ljava/io/File;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public close()V
    .locals 4

    iget-object v1, p0, Lli;->a:[Ljava/io/InputStream;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {v3}, Llj;->a(Ljava/io/Closeable;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    return-void
.end method
