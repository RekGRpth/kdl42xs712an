.class public Lgg;
.super Landroid/support/v4/view/PagerAdapter;
.source "Twttr"


# static fields
.field private static final a:[Lgi;

.field private static final b:Landroid/graphics/BitmapFactory$Options;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Landroid/view/LayoutInflater;

.field private final e:Lcom/twitter/android/client/c;

.field private final f:Lcom/twitter/library/client/aa;

.field private g:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    const/4 v9, 0x1

    const/4 v1, 0x0

    const/4 v0, 0x4

    new-array v8, v0, [Lgi;

    new-instance v0, Lgi;

    const v2, 0x7f0200a4    # com.twitter.android.R.drawable.carousel_figure_1

    const v3, 0x7f0f0090    # com.twitter.android.R.string.carousel_screen_1_title

    const-string/jumbo v5, "front:front_screen:::impression"

    move v4, v1

    invoke-direct/range {v0 .. v5}, Lgi;-><init>(IIIILjava/lang/String;)V

    aput-object v0, v8, v1

    new-instance v2, Lgi;

    const v3, 0x7f0b005a    # com.twitter.android.R.color.light_blue

    const v4, 0x7f0200a5    # com.twitter.android.R.drawable.carousel_figure_2

    const v5, 0x7f0f0092    # com.twitter.android.R.string.carousel_screen_2_title

    const v6, 0x7f0f0091    # com.twitter.android.R.string.carousel_screen_2_subtitle

    const-string/jumbo v7, "front:tweet_screen:::impression"

    invoke-direct/range {v2 .. v7}, Lgi;-><init>(IIIILjava/lang/String;)V

    aput-object v2, v8, v9

    const/4 v0, 0x2

    new-instance v2, Lgi;

    const v3, 0x7f0b0042    # com.twitter.android.R.color.deep_blue

    const v4, 0x7f0200a6    # com.twitter.android.R.drawable.carousel_figure_3

    const v5, 0x7f0f0094    # com.twitter.android.R.string.carousel_screen_3_title

    const v6, 0x7f0f0093    # com.twitter.android.R.string.carousel_screen_3_subtitle

    const-string/jumbo v7, "front:convo_screen:::impression"

    invoke-direct/range {v2 .. v7}, Lgi;-><init>(IIIILjava/lang/String;)V

    aput-object v2, v8, v0

    const/4 v0, 0x3

    new-instance v2, Lgi;

    const v3, 0x7f0b0077    # com.twitter.android.R.color.text

    const v4, 0x7f0200a7    # com.twitter.android.R.drawable.carousel_figure_4

    const v5, 0x7f0f0096    # com.twitter.android.R.string.carousel_screen_4_title

    const v6, 0x7f0f0095    # com.twitter.android.R.string.carousel_screen_4_subtitle

    const-string/jumbo v7, "front:now_screen:::impression"

    invoke-direct/range {v2 .. v7}, Lgi;-><init>(IIIILjava/lang/String;)V

    aput-object v2, v8, v0

    sput-object v8, Lgg;->a:[Lgi;

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    sput-object v0, Lgg;->b:Landroid/graphics/BitmapFactory$Options;

    sget-object v0, Lgg;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    sget-object v0, Lgg;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v9, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    sget-object v0, Lgg;->b:Landroid/graphics/BitmapFactory$Options;

    iput-boolean v9, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Landroid/support/v4/view/PagerAdapter;-><init>()V

    iput-object p1, p0, Lgg;->c:Landroid/content/Context;

    iget-object v0, p0, Lgg;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    iput-object v0, p0, Lgg;->d:Landroid/view/LayoutInflater;

    invoke-static {p1}, Lcom/twitter/android/client/c;->b(Landroid/content/Context;)Lcom/twitter/android/client/c;

    move-result-object v0

    iput-object v0, p0, Lgg;->e:Lcom/twitter/android/client/c;

    invoke-static {p1}, Lcom/twitter/library/client/aa;->a(Landroid/content/Context;)Lcom/twitter/library/client/aa;

    move-result-object v0

    iput-object v0, p0, Lgg;->f:Lcom/twitter/library/client/aa;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lgg;->g:Ljava/util/HashMap;

    return-void
.end method

.method static synthetic a()Landroid/graphics/BitmapFactory$Options;
    .locals 1

    sget-object v0, Lgg;->b:Landroid/graphics/BitmapFactory$Options;

    return-object v0
.end method

.method private a(Landroid/widget/ImageView;I)V
    .locals 2

    iget-object v0, p0, Lgg;->g:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    :goto_1
    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    new-instance v0, Lgh;

    iget-object v1, p0, Lgg;->c:Landroid/content/Context;

    invoke-direct {v0, v1, p0, p1, p2}, Lgh;-><init>(Landroid/content/Context;Lgg;Landroid/widget/ImageView;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lgh;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_1
.end method


# virtual methods
.method public a(I)I
    .locals 1

    invoke-virtual {p0}, Lgg;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-nez p1, :cond_0

    add-int/lit8 v0, v0, -0x2

    :goto_0
    return v0

    :cond_0
    if-ne p1, v0, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    add-int/lit8 v0, p1, -0x1

    goto :goto_0
.end method

.method public a(Landroid/widget/ImageView;Landroid/graphics/Bitmap;I)V
    .locals 3

    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lgg;->g:Ljava/util/HashMap;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-void
.end method

.method public destroyItem(Landroid/view/ViewGroup;ILjava/lang/Object;)V
    .locals 0

    check-cast p3, Landroid/view/View;

    invoke-virtual {p1, p3}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    return-void
.end method

.method public getCount()I
    .locals 1

    sget-object v0, Lgg;->a:[Lgi;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    return v0
.end method

.method public instantiateItem(Landroid/view/ViewGroup;I)Ljava/lang/Object;
    .locals 8

    const/4 v0, 0x0

    const/4 v7, 0x0

    if-ltz p2, :cond_0

    invoke-virtual {p0}, Lgg;->getCount()I

    move-result v1

    if-le p2, v1, :cond_1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    invoke-virtual {p0, p2}, Lgg;->a(I)I

    move-result v3

    iget-object v1, p0, Lgg;->d:Landroid/view/LayoutInflater;

    const v2, 0x7f030025    # com.twitter.android.R.layout.carousel_page_fragment

    invoke-virtual {v1, v2, v0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    sget-object v0, Lgg;->a:[Lgi;

    aget-object v4, v0, v3

    iget-object v0, p0, Lgg;->c:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    iget v0, v4, Lgi;->a:I

    if-eqz v0, :cond_2

    iget v0, v4, Lgi;->a:I

    invoke-virtual {v5, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    const v1, 0x7f0900c1    # com.twitter.android.R.id.carousel_page

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    :cond_2
    const v0, 0x7f0900c4    # com.twitter.android.R.id.carousel_title

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget v1, v4, Lgi;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    const v1, 0x7f0900c5    # com.twitter.android.R.id.carousel_subtitle

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget v6, v4, Lgi;->d:I

    if-eqz v6, :cond_4

    iget v6, v4, Lgi;->d:I

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setText(I)V

    :goto_1
    if-nez v3, :cond_5

    const v1, 0x7f0900c3    # com.twitter.android.R.id.carousel_logo

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget v3, v4, Lgi;->b:I

    invoke-direct {p0, v1, v3}, Lgg;->a(Landroid/widget/ImageView;I)V

    invoke-virtual {v1, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    const v1, 0x7f0b0031    # com.twitter.android.R.color.dark_gray

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    const v1, 0x7f0c0033    # com.twitter.android.R.dimen.carousel_padding_between_logo

    invoke-virtual {v5, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    const v3, 0x7f0c0032    # com.twitter.android.R.dimen.carousel_padding_between

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    invoke-virtual {v0, v7, v1, v7, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_3
    :goto_2
    invoke-virtual {p1, v2, v7}, Landroid/view/ViewGroup;->addView(Landroid/view/View;I)V

    iget-object v0, p0, Lgg;->e:Lcom/twitter/android/client/c;

    iget-object v1, p0, Lgg;->f:Lcom/twitter/library/client/aa;

    invoke-virtual {v1}, Lcom/twitter/library/client/aa;->b()Lcom/twitter/library/client/Session;

    move-result-object v1

    invoke-virtual {v1}, Lcom/twitter/library/client/Session;->g()J

    move-result-wide v5

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    iget-object v3, v4, Lgi;->e:Ljava/lang/String;

    aput-object v3, v1, v7

    invoke-virtual {v0, v5, v6, v1}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    move-object v0, v2

    goto/16 :goto_0

    :cond_4
    const/16 v6, 0x8

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_5
    const v0, 0x7f0900c2    # com.twitter.android.R.id.carousel_content

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_6

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/RelativeLayout$LayoutParams;

    const/16 v6, 0xc

    invoke-virtual {v0, v6}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(I)V

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    :cond_6
    const v0, 0x7f0900c6    # com.twitter.android.R.id.carousel_figure

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    if-eqz v0, :cond_3

    iget v1, v4, Lgi;->b:I

    invoke-direct {p0, v0, v1}, Lgg;->a(Landroid/widget/ImageView;I)V

    const/4 v1, 0x2

    if-ne v3, v1, :cond_3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingRight()I

    move-result v1

    const v3, 0x7f0c0034    # com.twitter.android.R.dimen.carousel_padding_figure3_right

    invoke-virtual {v5, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingLeft()I

    move-result v3

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingTop()I

    move-result v5

    invoke-virtual {v0}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v6

    invoke-virtual {v0, v3, v5, v1, v6}, Landroid/widget/ImageView;->setPadding(IIII)V

    goto :goto_2
.end method

.method public isViewFromObject(Landroid/view/View;Ljava/lang/Object;)Z
    .locals 1

    if-ne p1, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
