.class public Ljf;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:J

.field private final e:I

.field private final f:Lcom/twitter/library/api/ao;

.field private g:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;JI)V
    .locals 1

    const-class v0, Ljf;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p3, p0, Ljf;->d:J

    iput p5, p0, Ljf;->e:I

    const/16 v0, 0x3f

    invoke-static {v0}, Lcom/twitter/library/api/ao;->a(I)Lcom/twitter/library/api/ao;

    move-result-object v0

    iput-object v0, p0, Ljf;->f:Lcom/twitter/library/api/ao;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 7

    const/4 v5, 0x1

    const/4 v2, 0x0

    iget-object v0, p0, Ljf;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v1, 0x5

    new-array v1, v1, [Ljava/lang/Object;

    const-string/jumbo v3, "1.1"

    aput-object v3, v1, v2

    const-string/jumbo v3, "beta"

    aput-object v3, v1, v5

    const/4 v3, 0x2

    const-string/jumbo v4, "timelines"

    aput-object v4, v1, v3

    const/4 v3, 0x3

    const-string/jumbo v4, "custom"

    aput-object v4, v1, v3

    const/4 v3, 0x4

    const-string/jumbo v4, "list"

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p0}, Ljf;->r()I

    move-result v0

    if-nez v0, :cond_2

    const-string/jumbo v0, "user_id"

    iget-wide v3, p0, Ljf;->d:J

    invoke-static {v6, v0, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_0
    :goto_0
    invoke-virtual {p0}, Ljf;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    const/16 v1, 0xb

    iget-wide v3, p0, Ljf;->d:J

    iget v5, p0, Ljf;->e:I

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(IIJI)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string/jumbo v1, "cursor"

    invoke-static {v6, v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    new-instance v0, Lcom/twitter/library/network/d;

    iget-object v1, p0, Ljf;->l:Landroid/content/Context;

    invoke-direct {v0, v1, v6}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/twitter/library/network/n;

    invoke-virtual {p0}, Ljf;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-object v2, v2, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v1, v2}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Ljf;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0

    :cond_2
    invoke-virtual {p0}, Ljf;->r()I

    move-result v0

    if-ne v0, v5, :cond_0

    const-string/jumbo v0, "followings_by_user_id"

    iget-wide v3, p0, Ljf;->d:J

    invoke-static {v6, v0, v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto :goto_0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 13

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p0}, Ljf;->w()Lcom/twitter/library/provider/az;

    move-result-object v0

    iget-object v1, p0, Ljf;->f:Lcom/twitter/library/api/ao;

    invoke-virtual {v1}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v1

    move-object v10, v1

    check-cast v10, Lcom/twitter/library/api/ag;

    invoke-virtual {p0}, Ljf;->r()I

    move-result v1

    if-nez v1, :cond_2

    const/4 v1, 0x0

    move v11, v1

    :goto_0
    iget v1, p0, Ljf;->e:I

    if-nez v1, :cond_3

    const/4 v1, 0x1

    move v12, v1

    :goto_1
    iget-object v1, v10, Lcom/twitter/library/api/ag;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/16 v1, 0xb

    const/4 v2, 0x0

    iget-wide v3, p0, Ljf;->d:J

    iget v5, p0, Ljf;->e:I

    int-to-long v5, v5

    iget-object v7, v10, Lcom/twitter/library/api/ag;->c:Ljava/lang/String;

    invoke-virtual/range {v0 .. v7}, Lcom/twitter/library/provider/az;->a(IIJJLjava/lang/String;)V

    :cond_0
    iget-object v1, v10, Lcom/twitter/library/api/ag;->b:Ljava/util/Collection;

    invoke-virtual {p0}, Ljf;->s()Lcom/twitter/library/service/p;

    move-result-object v2

    iget-wide v2, v2, Lcom/twitter/library/service/p;->c:J

    const/4 v4, -0x1

    const-wide/16 v5, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x1

    invoke-virtual/range {v0 .. v9}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJLjava/lang/String;Ljava/lang/String;Z)I

    iget-object v1, v10, Lcom/twitter/library/api/ag;->a:Ljava/util/List;

    iget-wide v2, p0, Ljf;->d:J

    move v4, v11

    move v5, v12

    invoke-virtual/range {v0 .. v5}, Lcom/twitter/library/provider/az;->a(Ljava/util/List;JIZ)V

    iget-object v1, v10, Lcom/twitter/library/api/ag;->a:Ljava/util/List;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;Z)Ljava/util/Collection;

    move-result-object v0

    iget v1, p0, Ljf;->e:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_1

    invoke-interface {v0}, Ljava/util/Collection;->isEmpty()Z

    move-result v0

    iput-boolean v0, p0, Ljf;->g:Z

    :cond_1
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_2
    const/4 v1, 0x2

    move v11, v1

    goto :goto_0

    :cond_3
    const/4 v1, 0x0

    move v12, v1

    goto :goto_1
.end method

.method public e()Z
    .locals 1

    iget-boolean v0, p0, Ljf;->g:Z

    return v0
.end method
