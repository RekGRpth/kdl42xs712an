.class public Leg;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field private a:Landroid/graphics/PointF;

.field private b:F

.field private c:F

.field private d:F

.field private e:F

.field private f:F


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public a()Landroid/graphics/PointF;
    .locals 1

    iget-object v0, p0, Leg;->a:Landroid/graphics/PointF;

    return-object v0
.end method

.method public a(F)V
    .locals 0

    iput p1, p0, Leg;->b:F

    return-void
.end method

.method public a(Landroid/graphics/PointF;)V
    .locals 0

    iput-object p1, p0, Leg;->a:Landroid/graphics/PointF;

    return-void
.end method

.method public b()F
    .locals 1

    iget v0, p0, Leg;->b:F

    return v0
.end method

.method public b(F)V
    .locals 0

    iput p1, p0, Leg;->c:F

    return-void
.end method

.method public c()F
    .locals 1

    iget v0, p0, Leg;->c:F

    return v0
.end method

.method public c(F)V
    .locals 0

    iput p1, p0, Leg;->d:F

    return-void
.end method

.method public d()F
    .locals 1

    iget v0, p0, Leg;->d:F

    return v0
.end method

.method public d(F)V
    .locals 0

    iput p1, p0, Leg;->e:F

    return-void
.end method

.method public e()F
    .locals 1

    iget v0, p0, Leg;->e:F

    return v0
.end method

.method public e(F)V
    .locals 0

    iput p1, p0, Leg;->f:F

    return-void
.end method

.method public f()F
    .locals 1

    iget v0, p0, Leg;->f:F

    return v0
.end method
