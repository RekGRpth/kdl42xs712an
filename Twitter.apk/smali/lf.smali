.class public final Llf;
.super Ljava/lang/Object;
.source "Twttr"


# instance fields
.field final synthetic a:Lld;

.field private final b:Llh;

.field private c:Z


# direct methods
.method private constructor <init>(Lld;Llh;)V
    .locals 0

    iput-object p1, p0, Llf;->a:Lld;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Llf;->b:Llh;

    return-void
.end method

.method synthetic constructor <init>(Lld;Llh;Lle;)V
    .locals 0

    invoke-direct {p0, p1, p2}, Llf;-><init>(Lld;Llh;)V

    return-void
.end method

.method static synthetic a(Llf;)Llh;
    .locals 1

    iget-object v0, p0, Llf;->b:Llh;

    return-object v0
.end method

.method static synthetic a(Llf;Z)Z
    .locals 0

    iput-boolean p1, p0, Llf;->c:Z

    return p1
.end method


# virtual methods
.method public a(I)Ljava/io/OutputStream;
    .locals 4

    iget-object v1, p0, Llf;->a:Lld;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Llf;->b:Llh;

    invoke-static {v0}, Llh;->b(Llh;)Llf;

    move-result-object v0

    if-eq v0, p0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Llg;

    new-instance v2, Ljava/io/FileOutputStream;

    iget-object v3, p0, Llf;->b:Llh;

    invoke-virtual {v3, p1}, Llh;->b(I)Ljava/io/File;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    const/4 v3, 0x0

    invoke-direct {v0, p0, v2, v3}, Llg;-><init>(Llf;Ljava/io/OutputStream;Lle;)V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public a()V
    .locals 2

    iget-boolean v0, p0, Llf;->c:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Llf;->a:Lld;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lld;->a(Lld;Llf;Z)V

    iget-object v0, p0, Llf;->a:Lld;

    iget-object v1, p0, Llf;->b:Llh;

    invoke-static {v1}, Llh;->c(Llh;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lld;->c(Ljava/lang/String;)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Llf;->a:Lld;

    const/4 v1, 0x1

    invoke-static {v0, p0, v1}, Lld;->a(Lld;Llf;Z)V

    goto :goto_0
.end method

.method public b()V
    .locals 2

    iget-object v0, p0, Llf;->a:Lld;

    const/4 v1, 0x0

    invoke-static {v0, p0, v1}, Lld;->a(Lld;Llf;Z)V

    return-void
.end method
