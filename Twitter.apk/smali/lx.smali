.class public interface abstract Llx;
.super Ljava/lang/Object;


# static fields
.field public static final a:Lorg/spongycastle/asn1/l;

.field public static final b:Lorg/spongycastle/asn1/l;

.field public static final c:Lorg/spongycastle/asn1/l;

.field public static final d:Lorg/spongycastle/asn1/l;

.field public static final e:Lorg/spongycastle/asn1/l;

.field public static final f:Lorg/spongycastle/asn1/l;

.field public static final g:Lorg/spongycastle/asn1/l;

.field public static final h:Lorg/spongycastle/asn1/l;

.field public static final i:Lorg/spongycastle/asn1/l;

.field public static final j:Lorg/spongycastle/asn1/l;

.field public static final k:Lorg/spongycastle/asn1/l;

.field public static final l:Lorg/spongycastle/asn1/l;

.field public static final m:Lorg/spongycastle/asn1/l;

.field public static final n:Lorg/spongycastle/asn1/l;

.field public static final o:Lorg/spongycastle/asn1/l;

.field public static final p:Lorg/spongycastle/asn1/l;

.field public static final q:Lorg/spongycastle/asn1/l;

.field public static final r:Lorg/spongycastle/asn1/l;

.field public static final s:Lorg/spongycastle/asn1/l;

.field public static final t:Lorg/spongycastle/asn1/l;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.3"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->a:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.6"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->b:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->c:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.8"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->d:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.10"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->e:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.11"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->f:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.20"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->g:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.4.41"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->h:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.14.3.2.26"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->i:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.36.3.2.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->j:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.36.3.3.1.2"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->k:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.8.1.1"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->l:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "1.3.6.1.5.5.7"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->m:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Llx;->m:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->n:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    const-string/jumbo v1, "2.5.29"

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->o:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Llx;->m:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".48"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->p:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Llx;->p:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".2"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->q:Lorg/spongycastle/asn1/l;

    new-instance v0, Lorg/spongycastle/asn1/l;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Llx;->p:Lorg/spongycastle/asn1/l;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string/jumbo v2, ".1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lorg/spongycastle/asn1/l;-><init>(Ljava/lang/String;)V

    sput-object v0, Llx;->r:Lorg/spongycastle/asn1/l;

    sget-object v0, Llx;->r:Lorg/spongycastle/asn1/l;

    sput-object v0, Llx;->s:Lorg/spongycastle/asn1/l;

    sget-object v0, Llx;->q:Lorg/spongycastle/asn1/l;

    sput-object v0, Llx;->t:Lorg/spongycastle/asn1/l;

    return-void
.end method
