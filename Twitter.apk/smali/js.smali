.class public Ljs;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:J

.field private e:J

.field private f:J

.field private g:J

.field private n:I

.field private o:Lcom/twitter/library/api/ao;

.field private p:I

.field private q:I

.field private r:I

.field private s:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;J)V
    .locals 1

    const-class v0, Ljs;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-wide p3, p0, Ljs;->d:J

    return-void
.end method

.method public static a(Landroid/os/Bundle;)Ljava/lang/String;
    .locals 2

    const/4 v1, 0x0

    const-string/jumbo v0, "include_descendent_reply_count"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string/jumbo v0, "include_descendent_reply_count"

    :goto_0
    return-object v0

    :cond_0
    const-string/jumbo v0, "include_reply_count"

    invoke-virtual {p0, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string/jumbo v0, "include_reply_count"

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 10

    const/4 v2, 0x3

    const/4 v4, 0x0

    const-wide/16 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    invoke-virtual {p0}, Ljs;->r()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v3, "Invalid type: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :sswitch_0
    iget-object v0, p0, Ljs;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v4

    const-string/jumbo v2, "statuses"

    aput-object v2, v1, v6

    const-string/jumbo v2, "user_timeline"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Ljs;->d:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    const-string/jumbo v1, "include_rts"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "earned"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :goto_0
    const-string/jumbo v1, "include_entities"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_media_features"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_cards"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    const-string/jumbo v1, "include_user_entities"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    iget-object v1, p0, Ljs;->m:Lcom/twitter/library/network/aa;

    invoke-virtual {v1, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;)V

    iget-object v1, p0, Ljs;->k:Landroid/os/Bundle;

    invoke-static {v1}, Ljs;->a(Landroid/os/Bundle;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_0
    iget-boolean v1, p0, Ljs;->s:Z

    if-eqz v1, :cond_1

    const-string/jumbo v1, "include_view_count"

    invoke-static {v0, v1, v6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_1
    iget v1, p0, Ljs;->n:I

    if-lez v1, :cond_2

    const-string/jumbo v1, "count"

    iget v2, p0, Ljs;->n:I

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;I)V

    :cond_2
    iget-wide v1, p0, Ljs;->e:J

    cmp-long v1, v1, v8

    if-nez v1, :cond_3

    iget-wide v1, p0, Ljs;->g:J

    cmp-long v1, v1, v8

    if-nez v1, :cond_3

    iget-wide v1, p0, Ljs;->f:J

    cmp-long v1, v1, v8

    if-lez v1, :cond_3

    const-string/jumbo v1, "since_id"

    iget-wide v2, p0, Ljs;->f:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_3
    iget-wide v1, p0, Ljs;->g:J

    cmp-long v1, v1, v8

    if-lez v1, :cond_4

    const-string/jumbo v1, "max_id"

    iget-wide v2, p0, Ljs;->g:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_4
    new-instance v1, Lcom/twitter/library/util/f;

    iget-object v2, p0, Ljs;->l:Landroid/content/Context;

    iget-wide v3, p0, Ljs;->d:J

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/twitter/library/util/f;-><init>(Landroid/content/Context;JLjava/lang/String;)V

    iget-wide v2, p0, Ljs;->e:J

    cmp-long v2, v2, v8

    if-lez v2, :cond_5

    invoke-static {v7, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    iput-object v1, p0, Ljs;->o:Lcom/twitter/library/api/ao;

    :goto_1
    invoke-virtual {p0}, Ljs;->s()Lcom/twitter/library/service/p;

    move-result-object v1

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Ljs;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    iget-wide v3, v1, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/n;

    iget-object v1, v1, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v1}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    iget-object v1, p0, Ljs;->o:Lcom/twitter/library/api/ao;

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/i;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0

    :sswitch_1
    iget-object v0, p0, Ljs;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v2, v2, [Ljava/lang/Object;

    const-string/jumbo v3, "1.1"

    aput-object v3, v2, v4

    const-string/jumbo v3, "statuses"

    aput-object v3, v2, v6

    const-string/jumbo v3, "mentions_timeline"

    aput-object v3, v2, v7

    invoke-static {v0, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v2, ".json"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    packed-switch v1, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    const-string/jumbo v1, "filters"

    const-string/jumbo v2, "filtered"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    const-string/jumbo v1, "filters"

    const-string/jumbo v2, "following"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    const-string/jumbo v1, "filters"

    const-string/jumbo v2, "verified"

    invoke-static {v0, v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :sswitch_2
    iget-object v0, p0, Ljs;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v4

    const-string/jumbo v2, "statuses"

    aput-object v2, v1, v6

    const-string/jumbo v2, "show"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "id"

    iget-wide v2, p0, Ljs;->e:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto/16 :goto_0

    :sswitch_3
    iget-object v0, p0, Ljs;->m:Lcom/twitter/library/network/aa;

    iget-object v0, v0, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    new-array v1, v2, [Ljava/lang/Object;

    const-string/jumbo v2, "1.1"

    aput-object v2, v1, v4

    const-string/jumbo v2, "statuses"

    aput-object v2, v1, v6

    const-string/jumbo v2, "media_timeline"

    aput-object v2, v1, v7

    invoke-static {v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, "user_id"

    iget-wide v2, p0, Ljs;->d:J

    invoke-static {v0, v1, v2, v3}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    goto/16 :goto_0

    :cond_5
    invoke-static {v6, v1}, Lcom/twitter/library/api/ao;->a(ILcom/twitter/library/util/w;)Lcom/twitter/library/api/ao;

    move-result-object v1

    iput-object v1, p0, Ljs;->o:Lcom/twitter/library/api/ao;

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x5 -> :sswitch_1
        0xa -> :sswitch_2
        0x11 -> :sswitch_3
        0x17 -> :sswitch_1
        0x18 -> :sswitch_1
        0x19 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x17
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public a(I)Ljs;
    .locals 0

    iput p1, p0, Ljs;->n:I

    return-object p0
.end method

.method public a(J)Ljs;
    .locals 0

    iput-wide p1, p0, Ljs;->e:J

    return-object p0
.end method

.method public a(Z)Ljs;
    .locals 0

    iput-boolean p1, p0, Ljs;->s:Z

    return-object p0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 17

    invoke-virtual/range {p1 .. p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v2

    if-eqz v2, :cond_1

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/twitter/library/service/e;->a:Landroid/os/Bundle;

    move-object/from16 v0, p0

    iget-wide v2, v0, Ljs;->e:J

    const-wide/16 v5, 0x0

    cmp-long v2, v2, v5

    if-lez v2, :cond_2

    move-object/from16 v0, p0

    iget-object v2, v0, Ljs;->o:Lcom/twitter/library/api/ao;

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/twitter/library/api/TwitterStatus;

    new-instance v3, Ljava/util/ArrayList;

    const/4 v5, 0x1

    invoke-direct {v3, v5}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string/jumbo v5, "status_id"

    iget-wide v6, v2, Lcom/twitter/library/api/TwitterStatus;->a:J

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const/4 v10, 0x0

    const/4 v12, 0x0

    :goto_0
    invoke-virtual/range {p0 .. p0}, Ljs;->w()Lcom/twitter/library/provider/az;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljs;->r()I

    move-result v6

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljs;->d:J

    const-wide/16 v7, -0x1

    move-object/from16 v0, p0

    iget-wide v13, v0, Ljs;->g:J

    const-wide/16 v15, 0x0

    cmp-long v9, v13, v15

    if-lez v9, :cond_5

    const/4 v9, 0x1

    :goto_1
    if-nez v12, :cond_6

    const/4 v11, 0x1

    :goto_2
    const/4 v13, 0x1

    const/4 v14, 0x1

    const/16 v15, 0x11

    if-eq v6, v15, :cond_7

    const/4 v15, 0x1

    :goto_3
    invoke-virtual/range {v2 .. v15}, Lcom/twitter/library/provider/az;->a(Ljava/util/Collection;JIJZZZLjava/lang/String;ZZZ)Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->size()I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Ljs;->p:I

    move-object/from16 v0, p0

    iput v4, v0, Ljs;->q:I

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/16 v5, 0x64

    if-lt v4, v5, :cond_0

    move-object/from16 v0, p0

    iget-wide v4, v0, Ljs;->d:J

    const-wide/16 v7, -0x1

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    invoke-virtual {v3, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterStatus;

    iget-wide v9, v3, Lcom/twitter/library/api/TwitterStatus;->a:J

    move-object v3, v2

    invoke-virtual/range {v3 .. v10}, Lcom/twitter/library/provider/az;->a(JIJJ)V

    :cond_0
    invoke-static {v6}, Lcom/twitter/library/provider/ar;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object/from16 v0, p0

    iget-wide v3, v0, Ljs;->d:J

    invoke-virtual {v2, v3, v4, v6}, Lcom/twitter/library/provider/az;->j(JI)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Ljs;->r:I

    move-object/from16 v0, p0

    iget v2, v0, Ljs;->r:I

    if-lez v2, :cond_1

    move-object/from16 v0, p0

    iget-object v2, v0, Ljs;->l:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/provider/f;->a(Landroid/content/Context;)Lcom/twitter/library/provider/f;

    move-result-object v2

    invoke-virtual/range {p0 .. p0}, Ljs;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/service/p;->e:Ljava/lang/String;

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lcom/twitter/library/provider/f;->a(Ljava/lang/String;I)I

    :cond_1
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Ljs;->o:Lcom/twitter/library/api/ao;

    invoke-virtual {v2}, Lcom/twitter/library/api/ao;->a()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    move-object/from16 v0, p0

    iget-wide v3, v0, Ljs;->g:J

    const-wide/16 v5, 0x0

    cmp-long v3, v3, v5

    if-lez v3, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_3

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/twitter/library/api/TwitterStatus;

    iget-wide v3, v3, Lcom/twitter/library/api/TwitterStatus;->a:J

    invoke-static {v3, v4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    :goto_4
    move-object/from16 v0, p0

    iget-wide v4, v0, Ljs;->g:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_4

    const/4 v4, 0x1

    :goto_5
    move-object v12, v3

    move v10, v4

    move-object v3, v2

    goto/16 :goto_0

    :cond_3
    const/4 v3, 0x0

    goto :goto_4

    :cond_4
    const/4 v4, 0x0

    goto :goto_5

    :cond_5
    const/4 v9, 0x0

    goto/16 :goto_1

    :cond_6
    const/4 v11, 0x0

    goto/16 :goto_2

    :cond_7
    const/4 v15, 0x0

    goto/16 :goto_3
.end method

.method public b(J)Ljs;
    .locals 0

    iput-wide p1, p0, Ljs;->f:J

    return-object p0
.end method

.method public c(J)Ljs;
    .locals 0

    iput-wide p1, p0, Ljs;->g:J

    return-object p0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Ljs;->p:I

    return v0
.end method
