.class public Lkk;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field private static a:Landroid/content/Context;

.field private static b:J

.field private static c:Ljava/lang/String;

.field private static d:Z


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a()J
    .locals 2

    invoke-static {}, Lkk;->b()V

    sget-wide v0, Lkk;->b:J

    return-wide v0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    invoke-static {}, Lkk;->b()V

    sget-object v0, Lkk;->a:Landroid/content/Context;

    sget-wide v1, Lkk;->b:J

    invoke-static {v0, v1, v2, p0}, Lju;->a(Landroid/content/Context;JLjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static declared-synchronized a(JZ)V
    .locals 4

    const-class v1, Lkk;

    monitor-enter v1

    if-nez p2, :cond_0

    :try_start_0
    invoke-static {p0, p1}, Lju;->a(J)Ljava/util/HashMap;

    move-result-object v0

    if-nez v0, :cond_1

    :cond_0
    sget-object v0, Lkk;->a:Landroid/content/Context;

    invoke-static {v0, p0, p1}, Lju;->b(Landroid/content/Context;J)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    sget-object v0, Lkk;->a:Landroid/content/Context;

    sget-object v2, Lkk;->c:Ljava/lang/String;

    invoke-static {p0, p1, v0, v2}, Lju;->a(JLandroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    :cond_2
    :try_start_1
    sget-object v2, Lkk;->a:Landroid/content/Context;

    sget-object v3, Lkk;->c:Ljava/lang/String;

    invoke-static {v0, p0, p1, v2, v3}, Lju;->a(Ljava/util/HashMap;JLandroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;JLjava/lang/String;)V
    .locals 4

    const/4 v0, 0x0

    const-class v1, Lkk;

    monitor-enter v1

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    sput-object v2, Lkk;->a:Landroid/content/Context;

    sput-wide p1, Lkk;->b:J

    sput-object p3, Lkk;->c:Ljava/lang/String;

    const/4 v2, 0x1

    sput-boolean v2, Lkk;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {v0}, Lkk;->a(Z)V

    sget-object v1, Lcom/twitter/library/platform/a;->b:[Ljava/lang/String;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    invoke-static {p0, p1, p2, v3}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0

    :cond_0
    return-void
.end method

.method public static a(Z)V
    .locals 2

    sget-wide v0, Lkk;->b:J

    invoke-static {v0, v1, p0}, Lkk;->a(JZ)V

    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    invoke-static {}, Lkk;->a()J

    move-result-wide v0

    sget-object v2, Lkk;->a:Landroid/content/Context;

    invoke-static {p0, v0, v1, v2, p1}, Lju;->a(Ljava/lang/String;JLandroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static varargs a(Ljava/lang/String;[Ljava/lang/String;)Z
    .locals 5

    const/4 v0, 0x0

    invoke-static {p0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    array-length v3, p1

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, p1, v1

    invoke-virtual {v4, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v0, 0x1

    :cond_0
    return v0

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    const-string/jumbo v0, "%s:%s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static b()V
    .locals 2

    sget-boolean v0, Lkk;->d:Z

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string/jumbo v1, "initialize() must be called first."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method public static b(Ljava/lang/String;)V
    .locals 3

    invoke-static {}, Lkk;->b()V

    sget-object v0, Lkk;->a:Landroid/content/Context;

    sget-wide v1, Lkk;->b:J

    invoke-static {v0, v1, v2, p0}, Lju;->b(Landroid/content/Context;JLjava/lang/String;)V

    return-void
.end method

.method public static c(Ljava/lang/String;)Z
    .locals 1

    invoke-static {p0}, Lkk;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lkk;->d(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static d(Ljava/lang/String;)Z
    .locals 1

    const-string/jumbo v0, "control"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string/jumbo v0, "unassigned"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
