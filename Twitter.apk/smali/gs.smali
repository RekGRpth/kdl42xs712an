.class final Lgs;
.super Ljava/lang/Object;
.source "Twttr"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:Lcom/twitter/library/provider/NotificationSetting;

.field final synthetic d:Landroid/content/Context;

.field final synthetic e:Ljava/lang/String;

.field final synthetic f:Landroid/accounts/Account;

.field final synthetic g:I

.field final synthetic h:Lcom/twitter/android/client/c;

.field final synthetic i:I

.field final synthetic j:J

.field private k:I


# direct methods
.method constructor <init>(IILcom/twitter/library/provider/NotificationSetting;Landroid/content/Context;Ljava/lang/String;Landroid/accounts/Account;ILcom/twitter/android/client/c;IJ)V
    .locals 1

    iput p1, p0, Lgs;->a:I

    iput p2, p0, Lgs;->b:I

    iput-object p3, p0, Lgs;->c:Lcom/twitter/library/provider/NotificationSetting;

    iput-object p4, p0, Lgs;->d:Landroid/content/Context;

    iput-object p5, p0, Lgs;->e:Ljava/lang/String;

    iput-object p6, p0, Lgs;->f:Landroid/accounts/Account;

    iput p7, p0, Lgs;->g:I

    iput-object p8, p0, Lgs;->h:Lcom/twitter/android/client/c;

    iput p9, p0, Lgs;->i:I

    iput-wide p10, p0, Lgs;->j:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget v0, p0, Lgs;->a:I

    iput v0, p0, Lgs;->k:I

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 7

    const/4 v6, 0x0

    invoke-static {p2}, Lgr;->a(I)I

    move-result v0

    iget v1, p0, Lgs;->k:I

    iget v2, p0, Lgs;->b:I

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    iget-object v2, p0, Lgs;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v2, v0}, Lcom/twitter/library/provider/NotificationSetting;->b(I)I

    move-result v2

    or-int/2addr v1, v2

    iget v2, p0, Lgs;->k:I

    if-eq v1, v2, :cond_2

    new-instance v2, Lgt;

    iget-object v3, p0, Lgs;->d:Landroid/content/Context;

    iget-object v4, p0, Lgs;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4, v1}, Lgt;-><init>(Landroid/content/Context;Ljava/lang/String;I)V

    new-array v3, v6, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lgt;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    iget-object v2, p0, Lgs;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/twitter/library/platform/PushService;->c(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lgs;->d:Landroid/content/Context;

    iget-object v3, p0, Lgs;->f:Landroid/accounts/Account;

    invoke-static {v2, v3}, Lcom/twitter/library/platform/PushService;->b(Landroid/content/Context;Landroid/accounts/Account;)I

    move-result v2

    iget v3, p0, Lgs;->g:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    iget-object v3, p0, Lgs;->c:Lcom/twitter/library/provider/NotificationSetting;

    invoke-virtual {v3, v0}, Lcom/twitter/library/provider/NotificationSetting;->a(I)I

    move-result v3

    or-int/2addr v2, v3

    iget-object v3, p0, Lgs;->h:Lcom/twitter/android/client/c;

    iget-object v4, p0, Lgs;->f:Landroid/accounts/Account;

    invoke-virtual {v3, v4, v2}, Lcom/twitter/android/client/c;->a(Landroid/accounts/Account;I)Ljava/lang/String;

    :cond_0
    iget v2, p0, Lgs;->i:I

    invoke-static {v2, v0}, Lgr;->a(II)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v2, p0, Lgs;->h:Lcom/twitter/android/client/c;

    iget-wide v3, p0, Lgs;->j:J

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-virtual {v2, v3, v4, v5}, Lcom/twitter/android/client/c;->a(J[Ljava/lang/String;)V

    :cond_1
    iput v1, p0, Lgs;->k:I

    :cond_2
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
