.class public Lix;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# instance fields
.field private final d:Lcom/twitter/library/api/PromotedEvent;

.field private e:J

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private n:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    new-instance v0, Lcom/twitter/library/service/p;

    invoke-direct {v0, p2}, Lcom/twitter/library/service/p;-><init>(Lcom/twitter/library/client/Session;)V

    invoke-direct {p0, p1, v0, p3}, Lix;-><init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/PromotedEvent;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/service/p;Lcom/twitter/library/api/PromotedEvent;)V
    .locals 1

    const-class v0, Lix;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/service/p;)V

    iput-object p3, p0, Lix;->d:Lcom/twitter/library/api/PromotedEvent;

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/twitter/library/network/aa;JLcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;JLjava/lang/String;ZZZ)V
    .locals 9

    invoke-static {p0, p2, p3}, Lcom/twitter/library/provider/az;->a(Landroid/content/Context;J)Lcom/twitter/library/provider/az;

    move-result-object v2

    iget-object v3, p1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string/jumbo v6, "1.1"

    aput-object v6, v4, v5

    const/4 v5, 0x1

    const-string/jumbo v6, "promoted_content"

    aput-object v6, v4, v5

    const/4 v5, 0x2

    const-string/jumbo v6, "log"

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string/jumbo v4, ".json"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz p6, :cond_0

    const-string/jumbo v4, "impression_id"

    invoke-static {v3, v4, p6}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    const-wide/16 v4, 0x0

    cmp-long v4, p7, v4

    if-lez v4, :cond_1

    const-string/jumbo v4, "promoted_trend_id"

    move-wide/from16 v0, p7

    invoke-static {v3, v4, v0, v1}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;J)V

    :cond_1
    if-eqz p10, :cond_2

    const-string/jumbo v4, "earned"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Z)V

    :cond_2
    const-string/jumbo v4, "event"

    invoke-virtual {p5}, Lcom/twitter/library/api/PromotedEvent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static/range {p9 .. p9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    const-string/jumbo v4, "url"

    move-object/from16 v0, p9

    invoke-static {v3, v4, v0}, Lcom/twitter/library/network/aa;->a(Ljava/lang/StringBuilder;Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    new-instance v4, Lcom/twitter/library/network/d;

    invoke-direct {v4, p0, v3}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v3, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v4, v3}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3, p2, p3}, Lcom/twitter/library/network/d;->a(J)Lcom/twitter/library/network/d;

    move-result-object v3

    new-instance v4, Lcom/twitter/library/network/n;

    invoke-direct {v4, p4}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v3, v4}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v3

    move/from16 v0, p12

    invoke-virtual {v3, v0}, Lcom/twitter/library/network/d;->a(Z)Lcom/twitter/library/network/d;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v3

    invoke-virtual {v3}, Lcom/twitter/internal/network/HttpOperation;->e()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v4

    invoke-virtual {p5}, Lcom/twitter/library/api/PromotedEvent;->ordinal()I

    move-result v3

    invoke-virtual {v4}, Lcom/twitter/internal/network/HttpOperation;->j()Z

    move-result v4

    if-nez v4, :cond_5

    move-object v4, p6

    move-wide/from16 v5, p7

    move-object/from16 v7, p9

    move/from16 v8, p10

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/library/provider/az;->a(ILjava/lang/String;JLjava/lang/String;Z)V

    :cond_4
    :goto_0
    return-void

    :cond_5
    if-eqz p11, :cond_4

    move-object v4, p6

    move-wide/from16 v5, p7

    move-object/from16 v7, p9

    move/from16 v8, p10

    invoke-virtual/range {v2 .. v8}, Lcom/twitter/library/provider/az;->b(ILjava/lang/String;JLjava/lang/String;Z)V

    goto :goto_0
.end method


# virtual methods
.method public a(J)Lix;
    .locals 0

    iput-wide p1, p0, Lix;->e:J

    return-object p0
.end method

.method public a(Ljava/lang/String;)Lix;
    .locals 0

    iput-object p1, p0, Lix;->f:Ljava/lang/String;

    return-object p0
.end method

.method public a(Z)Lix;
    .locals 0

    iput-boolean p1, p0, Lix;->n:Z

    return-object p0
.end method

.method public b(Ljava/lang/String;)Lix;
    .locals 0

    iput-object p1, p0, Lix;->g:Ljava/lang/String;

    return-object p0
.end method

.method protected b(Lcom/twitter/library/service/e;)V
    .locals 13

    invoke-virtual {p0}, Lix;->s()Lcom/twitter/library/service/p;

    move-result-object v4

    iget-object v0, p0, Lix;->l:Landroid/content/Context;

    iget-object v1, p0, Lix;->m:Lcom/twitter/library/network/aa;

    iget-wide v2, v4, Lcom/twitter/library/service/p;->c:J

    iget-object v4, v4, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    iget-object v5, p0, Lix;->d:Lcom/twitter/library/api/PromotedEvent;

    iget-object v6, p0, Lix;->g:Ljava/lang/String;

    iget-wide v7, p0, Lix;->e:J

    iget-object v9, p0, Lix;->f:Ljava/lang/String;

    iget-boolean v10, p0, Lix;->n:Z

    const/4 v11, 0x0

    invoke-virtual {p0}, Lix;->t()Z

    move-result v12

    invoke-static/range {v0 .. v12}, Lix;->a(Landroid/content/Context;Lcom/twitter/library/network/aa;JLcom/twitter/library/network/OAuthToken;Lcom/twitter/library/api/PromotedEvent;Ljava/lang/String;JLjava/lang/String;ZZZ)V

    return-void
.end method
