.class public abstract La;
.super Lcom/fasterxml/jackson/core/JsonGenerator;
.source "Twttr"


# instance fields
.field protected b:Lcom/fasterxml/jackson/core/b;

.field protected c:I

.field protected d:Z

.field protected e:Li;

.field protected f:Z


# direct methods
.method protected constructor <init>(ILcom/fasterxml/jackson/core/b;)V
    .locals 1

    invoke-direct {p0}, Lcom/fasterxml/jackson/core/JsonGenerator;-><init>()V

    iput p1, p0, La;->c:I

    invoke-static {}, Li;->g()Li;

    move-result-object v0

    iput-object v0, p0, La;->e:Li;

    iput-object p2, p0, La;->b:Lcom/fasterxml/jackson/core/b;

    sget-object v0, Lcom/fasterxml/jackson/core/JsonGenerator$Feature;->e:Lcom/fasterxml/jackson/core/JsonGenerator$Feature;

    invoke-virtual {p0, v0}, La;->a(Lcom/fasterxml/jackson/core/JsonGenerator$Feature;)Z

    move-result v0

    iput-boolean v0, p0, La;->d:Z

    return-void
.end method


# virtual methods
.method public final a(Lcom/fasterxml/jackson/core/JsonGenerator$Feature;)Z
    .locals 2

    iget v0, p0, La;->c:I

    invoke-virtual {p1}, Lcom/fasterxml/jackson/core/JsonGenerator$Feature;->c()I

    move-result v1

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public close()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, La;->f:Z

    return-void
.end method

.method public final e()Li;
    .locals 1

    iget-object v0, p0, La;->e:Li;

    return-object v0
.end method

.method protected abstract f()V
.end method

.method protected abstract f(Ljava/lang/String;)V
.end method

.method protected final g()V
    .locals 0

    invoke-static {}, Lcom/fasterxml/jackson/core/util/d;->a()V

    return-void
.end method

.method protected g(Ljava/lang/String;)V
    .locals 1

    new-instance v0, Lcom/fasterxml/jackson/core/JsonGenerationException;

    invoke-direct {v0, p1}, Lcom/fasterxml/jackson/core/JsonGenerationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
