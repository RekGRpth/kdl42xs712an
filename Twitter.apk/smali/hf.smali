.class public Lhf;
.super Lcom/twitter/library/metrics/b;
.source "Twttr"


# instance fields
.field private final e:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/metrics/g;Z)V
    .locals 8

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v7, p4

    invoke-direct/range {v0 .. v7}, Lcom/twitter/library/metrics/b;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;JILcom/twitter/library/metrics/g;)V

    iput-boolean p5, p0, Lhf;->e:Z

    return-void
.end method

.method public static a(Lcom/twitter/library/metrics/h;Z)Lhf;
    .locals 6

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string/jumbo v1, "NetworkUsageapi::::rxbytes"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p1, :cond_1

    const-string/jumbo v0, "_f"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0, v3}, Lcom/twitter/library/metrics/h;->a(Ljava/lang/String;)Lcom/twitter/library/metrics/d;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lhf;

    invoke-virtual {p0}, Lcom/twitter/library/metrics/h;->b()Landroid/content/Context;

    move-result-object v1

    const-string/jumbo v2, "api::::rxbytes"

    move-object v4, p0

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lhf;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/twitter/library/metrics/g;Z)V

    invoke-virtual {p0, v0}, Lcom/twitter/library/metrics/h;->d(Lcom/twitter/library/metrics/d;)Lcom/twitter/library/metrics/d;

    move-result-object v0

    :cond_0
    check-cast v0, Lhf;

    return-object v0

    :cond_1
    const-string/jumbo v0, "_b"

    goto :goto_0
.end method


# virtual methods
.method public a()Lcom/twitter/library/scribe/ScribeLog;
    .locals 2

    invoke-super {p0}, Lcom/twitter/library/metrics/b;->a()Lcom/twitter/library/scribe/ScribeLog;

    move-result-object v1

    iget-boolean v0, p0, Lhf;->e:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, "foreground"

    :goto_0
    invoke-virtual {v1, v0}, Lcom/twitter/library/scribe/ScribeLog;->d(Ljava/lang/String;)Lcom/twitter/library/scribe/ScribeLog;

    return-object v1

    :cond_0
    const-string/jumbo v0, "background"

    goto :goto_0
.end method

.method public b()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-super {p0}, Lcom/twitter/library/metrics/b;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lhf;->e:Z

    if-eqz v0, :cond_0

    const-string/jumbo v0, " foreground"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const-string/jumbo v0, " background"

    goto :goto_0
.end method
