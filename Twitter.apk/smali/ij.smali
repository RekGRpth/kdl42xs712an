.class public final Lij;
.super Ljava/lang/Object;
.source "Twttr"


# static fields
.field public static final days:I = 0x7f0e0001

.field public static final months:I = 0x7f0e0007

.field public static final notification_replied_someone_you_others:I = 0x7f0e000a

.field public static final notification_replied_you_someone_others:I = 0x7f0e000b

.field public static final replied_users_and_count:I = 0x7f0e0011

.field public static final social_fav_and_retweets_count:I = 0x7f0e0015

.field public static final social_fav_count:I = 0x7f0e0016

.field public static final social_fav_count_with_user:I = 0x7f0e0017

.field public static final social_fav_count_with_user_accessibility_description:I = 0x7f0e0018

.field public static final social_proof_in_reply_multiple_names_and_count:I = 0x7f0e0019

.field public static final social_proof_in_reply_name_and_count:I = 0x7f0e001a

.field public static final social_retweet_and_favs_count:I = 0x7f0e001b

.field public static final social_retweet_count:I = 0x7f0e001c

.field public static final social_view_count:I = 0x7f0e001d

.field public static final time_days:I = 0x7f0e0022

.field public static final time_days_ago:I = 0x7f0e0023

.field public static final time_hours:I = 0x7f0e0024

.field public static final time_hours_ago:I = 0x7f0e0025

.field public static final time_mins:I = 0x7f0e0026

.field public static final time_mins_ago:I = 0x7f0e0027

.field public static final time_secs:I = 0x7f0e0028

.field public static final weeks:I = 0x7f0e002a

.field public static final years:I = 0x7f0e002b
