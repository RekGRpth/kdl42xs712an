.class public Lhr;
.super Ljavax/net/ssl/SSLSocketFactory;
.source "Twttr"


# instance fields
.field private final a:Lhs;


# direct methods
.method public constructor <init>(Lhu;[Ljava/lang/String;J[Ljava/lang/String;)V
    .locals 6

    invoke-direct {p0}, Ljavax/net/ssl/SSLSocketFactory;-><init>()V

    new-instance v0, Lhs;

    move-object v1, p1

    move-object v2, p2

    move-wide v3, p3

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lhs;-><init>(Lhu;[Ljava/lang/String;J[Ljava/lang/String;)V

    iput-object v0, p0, Lhr;->a:Lhs;

    return-void
.end method


# virtual methods
.method public createSocket(Ljava/lang/String;I)Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0, p1, p2}, Lhs;->a(Ljava/lang/String;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhs;->a(Ljava/lang/String;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0, p1, p2}, Lhs;->a(Ljava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhs;->a(Ljava/net/InetAddress;ILjava/net/InetAddress;I)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public createSocket(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0, p1, p2, p3, p4}, Lhs;->a(Ljava/net/Socket;Ljava/lang/String;IZ)Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultCipherSuites()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0}, Lhs;->a()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getSupportedCipherSuites()[Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lhr;->a:Lhs;

    invoke-virtual {v0}, Lhs;->b()[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
