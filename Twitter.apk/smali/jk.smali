.class public Ljk;
.super Lcom/twitter/library/service/o;
.source "Twttr"


# static fields
.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string/jumbo v2, "follow"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string/jumbo v2, "unfollow"

    aput-object v2, v0, v1

    sput-object v0, Ljk;->d:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/twitter/library/client/Session;Ljava/lang/String;)V
    .locals 1

    const-class v0, Ljk;

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, p1, v0, p2}, Lcom/twitter/library/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/twitter/library/client/Session;)V

    iput-object p3, p0, Ljk;->e:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method protected a(Lcom/twitter/library/service/e;)Lcom/twitter/internal/network/HttpOperation;
    .locals 5

    sget-object v0, Ljk;->d:[Ljava/lang/String;

    invoke-virtual {p0}, Ljk;->r()I

    move-result v1

    aget-object v0, v0, v1

    iget-object v1, p0, Ljk;->m:Lcom/twitter/library/network/aa;

    iget-object v1, v1, Lcom/twitter/library/network/aa;->a:Ljava/lang/String;

    const/4 v2, 0x4

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string/jumbo v4, "1.1"

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-string/jumbo v4, "timelines"

    aput-object v4, v2, v3

    const/4 v3, 0x2

    const-string/jumbo v4, "custom"

    aput-object v4, v2, v3

    const/4 v3, 0x3

    aput-object v0, v2, v3

    invoke-static {v1, v2}, Lcom/twitter/library/network/aa;->a(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string/jumbo v1, ".json"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    new-instance v2, Lorg/apache/http/message/BasicNameValuePair;

    const-string/jumbo v3, "id"

    iget-object v4, p0, Ljk;->e:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lorg/apache/http/message/BasicNameValuePair;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v2, Lcom/twitter/library/network/d;

    iget-object v3, p0, Ljk;->l:Landroid/content/Context;

    invoke-direct {v2, v3, v0}, Lcom/twitter/library/network/d;-><init>(Landroid/content/Context;Ljava/lang/CharSequence;)V

    sget-object v0, Lcom/twitter/internal/network/HttpOperation$RequestMethod;->b:Lcom/twitter/internal/network/HttpOperation$RequestMethod;

    invoke-virtual {v2, v0}, Lcom/twitter/library/network/d;->a(Lcom/twitter/internal/network/HttpOperation$RequestMethod;)Lcom/twitter/library/network/d;

    move-result-object v0

    new-instance v2, Lcom/twitter/library/network/n;

    invoke-virtual {p0}, Ljk;->s()Lcom/twitter/library/service/p;

    move-result-object v3

    iget-object v3, v3, Lcom/twitter/library/service/p;->d:Lcom/twitter/library/network/OAuthToken;

    invoke-direct {v2, v3}, Lcom/twitter/library/network/n;-><init>(Lcom/twitter/library/network/OAuthToken;)V

    invoke-virtual {v0, v2}, Lcom/twitter/library/network/d;->a(Lcom/twitter/library/network/a;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/twitter/library/network/d;->a(Ljava/util/ArrayList;)Lcom/twitter/library/network/d;

    move-result-object v0

    invoke-virtual {v0}, Lcom/twitter/library/network/d;->a()Lcom/twitter/internal/network/HttpOperation;

    move-result-object v0

    return-object v0
.end method

.method protected a(Lcom/twitter/internal/network/HttpOperation;Lcom/twitter/library/service/e;)V
    .locals 7

    const/4 v2, 0x2

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/twitter/internal/network/HttpOperation;->k()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Ljk;->w()Lcom/twitter/library/provider/az;

    move-result-object v3

    invoke-virtual {p0}, Ljk;->s()Lcom/twitter/library/service/p;

    move-result-object v0

    iget-wide v4, v0, Lcom/twitter/library/service/p;->c:J

    invoke-virtual {p0}, Ljk;->r()I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    iget-object v6, p0, Ljk;->e:Ljava/lang/String;

    invoke-virtual {v3, v6, v4, v5, v0}, Lcom/twitter/library/provider/az;->a(Ljava/lang/String;JI)V

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Ljk;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v0, v1}, Lcom/twitter/library/provider/az;->a(JLjava/lang/String;Z)I

    :cond_0
    invoke-virtual {p2, p1}, Lcom/twitter/library/service/e;->a(Lcom/twitter/internal/network/HttpOperation;)V

    return-void

    :cond_1
    move v0, v2

    goto :goto_0
.end method
