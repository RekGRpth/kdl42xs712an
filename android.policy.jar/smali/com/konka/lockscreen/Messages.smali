.class public Lcom/konka/lockscreen/Messages;
.super Landroid/widget/LinearLayout;
.source "Messages.java"


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "HandlerLeak"
    }
.end annotation


# instance fields
.field private final MSG_UPDATE:I

.field private mCallback:Lcom/konka/lockscreen/service/IMessageCallback;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mMsgNum:Landroid/widget/TextView;

.field private mUpdateInfo:Ljava/lang/String;

.field private mUpdateNum:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x3

    iput v1, p0, Lcom/konka/lockscreen/Messages;->MSG_UPDATE:I

    new-instance v1, Lcom/konka/lockscreen/Messages$1;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Messages$1;-><init>(Lcom/konka/lockscreen/Messages;)V

    iput-object v1, p0, Lcom/konka/lockscreen/Messages;->mCallback:Lcom/konka/lockscreen/service/IMessageCallback;

    iput-object p1, p0, Lcom/konka/lockscreen/Messages;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x109007f    # android.R.layout.konka_lockscreen_messages

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-direct {p0}, Lcom/konka/lockscreen/Messages;->initUI()V

    new-instance v1, Lcom/konka/lockscreen/Messages$2;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lcom/konka/lockscreen/Messages$2;-><init>(Lcom/konka/lockscreen/Messages;Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/konka/lockscreen/Messages;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/konka/lockscreen/Messages;->mCallback:Lcom/konka/lockscreen/service/IMessageCallback;

    invoke-static {p1, v1}, Lcom/konka/lockscreen/service/MessageService;->getUpdatesService(Landroid/content/Context;Lcom/konka/lockscreen/service/IMessageCallback;)V

    return-void
.end method

.method static synthetic access$000(Lcom/konka/lockscreen/Messages;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Messages;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages;->mUpdateInfo:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Lcom/konka/lockscreen/Messages;Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0    # Lcom/konka/lockscreen/Messages;
    .param p1    # Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/lockscreen/Messages;->mUpdateInfo:Ljava/lang/String;

    return-object p1
.end method

.method static synthetic access$100(Lcom/konka/lockscreen/Messages;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Messages;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/konka/lockscreen/Messages;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Messages;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/konka/lockscreen/Messages;)Landroid/widget/TextView;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Messages;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages;->mUpdateNum:Landroid/widget/TextView;

    return-object v0
.end method

.method private initUI()V
    .locals 3

    const v0, 0x1020418    # android.R.id.konka_lockscreen_update_num

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Messages;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/lockscreen/Messages;->mUpdateNum:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages;->mUpdateNum:Landroid/widget/TextView;

    const-string v1, "0"

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    const v0, 0x1020419    # android.R.id.konka_lockscreen_msg_num

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Messages;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/lockscreen/Messages;->mMsgNum:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/Messages;->mMsgNum:Landroid/widget/TextView;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/lockscreen/Messages;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/lockscreen/service/MessageService;->getSystemMsgCount(Landroid/content/Context;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_1

    :catch_1
    move-exception v0

    goto :goto_0
.end method
