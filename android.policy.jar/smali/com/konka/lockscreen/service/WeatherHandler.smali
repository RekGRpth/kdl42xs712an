.class public Lcom/konka/lockscreen/service/WeatherHandler;
.super Lorg/xml/sax/helpers/DefaultHandler;
.source "WeatherHandler.java"


# instance fields
.field final RSS_CONDITION:I

.field final RSS_RECORD:I

.field final RSS_TEMP:I

.field final RSS_TIME:I

.field private buffer:Ljava/lang/StringBuffer;

.field currentstate:I

.field lastElementName:Ljava/lang/String;

.field private mCallback:Lcom/konka/lockscreen/service/IWeatherCallback;

.field private mContext:Landroid/content/Context;

.field private mTemp:Ljava/lang/String;

.field private mWeather:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/konka/lockscreen/service/IWeatherCallback;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/konka/lockscreen/service/IWeatherCallback;

    invoke-direct {p0}, Lorg/xml/sax/helpers/DefaultHandler;-><init>()V

    const-string v0, ""

    iput-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->lastElementName:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->RSS_RECORD:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->RSS_TIME:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->RSS_CONDITION:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->RSS_TEMP:I

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    iput-object p1, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    iput-object p2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mCallback:Lcom/konka/lockscreen/service/IWeatherCallback;

    return-void
.end method

.method private getWeatherDes(I)Ljava/lang/String;
    .locals 2
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    const-string v0, ""

    :goto_0
    return-object v0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400c9    # android.R.string.konka_lockscreen_weather1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400ca    # android.R.string.konka_lockscreen_weather2

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400cb    # android.R.string.konka_lockscreen_weather3

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400cc    # android.R.string.konka_lockscreen_weather4

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400cd    # android.R.string.konka_lockscreen_weather5

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400ce    # android.R.string.konka_lockscreen_weather6

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_6
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400cf    # android.R.string.konka_lockscreen_weather7

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_7
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d0    # android.R.string.konka_lockscreen_weather8

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_8
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d1    # android.R.string.konka_lockscreen_weather9

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_9
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d2    # android.R.string.konka_lockscreen_weather10

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_a
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d3    # android.R.string.konka_lockscreen_weather11

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_b
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d4    # android.R.string.konka_lockscreen_weather12

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :pswitch_c
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d5    # android.R.string.konka_lockscreen_weather13

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_d
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d6    # android.R.string.konka_lockscreen_weather14

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_e
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d7    # android.R.string.konka_lockscreen_weather15

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_f
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d8    # android.R.string.konka_lockscreen_weather16

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_10
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400d9    # android.R.string.konka_lockscreen_weather17

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_11
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400da    # android.R.string.konka_lockscreen_weather18

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_12
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400db    # android.R.string.konka_lockscreen_weather19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_13
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400dc    # android.R.string.konka_lockscreen_weather20

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_14
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400dd    # android.R.string.konka_lockscreen_weather21

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_15
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400de    # android.R.string.konka_lockscreen_weather22

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_16
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400df    # android.R.string.konka_lockscreen_weather23

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_17
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e0    # android.R.string.konka_lockscreen_weather24

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_18
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e1    # android.R.string.konka_lockscreen_weather25

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_19
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e2    # android.R.string.konka_lockscreen_weather26

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1a
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e3    # android.R.string.konka_lockscreen_weather27

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1b
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e4    # android.R.string.konka_lockscreen_weather28

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1c
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e5    # android.R.string.konka_lockscreen_weather29

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1d
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e6    # android.R.string.konka_lockscreen_weather30

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1e
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e7    # android.R.string.konka_lockscreen_weather31

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_1f
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e8    # android.R.string.konka_lockscreen_weather32

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :pswitch_20
    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mContext:Landroid/content/Context;

    const v1, 0x10400e9    # android.R.string.konka_lockscreen_weather33

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
    .end packed-switch
.end method


# virtual methods
.method public characters([CII)V
    .locals 2
    .param p1    # [C
    .param p2    # I
    .param p3    # I

    new-instance v1, Ljava/lang/String;

    invoke-direct {v1, p1, p2, p3}, Ljava/lang/String;-><init>([CII)V

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/service/WeatherHandler;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    return-void
.end method

.method public endDocument()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    iget-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mCallback:Lcom/konka/lockscreen/service/IWeatherCallback;

    iget-object v1, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mWeather:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mTemp:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/konka/lockscreen/service/IWeatherCallback;->weather(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public endElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const/4 v5, 0x0

    iget v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    packed-switch v2, :pswitch_data_0

    :goto_0
    :pswitch_0
    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->buffer:Ljava/lang/StringBuffer;

    iget-object v3, p0, Lcom/konka/lockscreen/service/WeatherHandler;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v3

    invoke-virtual {v2, v5, v3}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    iput v5, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    return-void

    :pswitch_1
    :try_start_0
    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mWeather:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mWeather:Ljava/lang/String;

    const-string v3, ","

    invoke-virtual {v2, v3}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x1

    if-ltz v0, :cond_0

    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mWeather:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    :goto_1
    invoke-direct {p0, v1}, Lcom/konka/lockscreen/service/WeatherHandler;->getWeatherDes(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mWeather:Ljava/lang/String;

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_0
    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mWeather:Ljava/lang/String;

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_1

    :pswitch_2
    iget-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->buffer:Ljava/lang/StringBuffer;

    invoke-virtual {v2}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, ","

    const-string v4, "-"

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/lockscreen/service/WeatherHandler;->mTemp:Ljava/lang/String;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public startDocument()V
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    iput-object v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->buffer:Ljava/lang/StringBuffer;

    return-void
.end method

.method public startElement(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lorg/xml/sax/Attributes;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Ljava/lang/String;
    .param p4    # Lorg/xml/sax/Attributes;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/xml/sax/SAXException;
        }
    .end annotation

    const-string v0, "RECORD"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    :goto_0
    return-void

    :cond_0
    const-string v0, "time"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    goto :goto_0

    :cond_1
    const-string v0, "condition"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    goto :goto_0

    :cond_2
    const-string v0, "temperature"

    invoke-virtual {p2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    goto :goto_0

    :cond_3
    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/lockscreen/service/WeatherHandler;->currentstate:I

    goto :goto_0
.end method
