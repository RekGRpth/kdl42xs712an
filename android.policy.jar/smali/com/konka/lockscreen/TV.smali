.class public Lcom/konka/lockscreen/TV;
.super Landroid/view/SurfaceView;
.source "TV.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation build Landroid/annotation/SuppressLint;
    value = {
        "UseSparseArrays"
    }
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/lockscreen/TV$TvThread;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v0, "TV"

    iput-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/lockscreen/TV;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "TV window init"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/konka/lockscreen/TV;->mContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$000(Lcom/konka/lockscreen/TV;)Ljava/lang/String;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/TV;

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private switchTVChannel()V
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/konka/lockscreen/TV;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/android/tv/KKCommonManager;->getInstance(Landroid/content/Context;)Lcom/konka/android/tv/KKCommonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/android/tv/KKCommonManager;->getInputSourceFromDB()Lcom/konka/android/tv/KKCommonManager$EN_KK_INPUT_SOURCE;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/lockscreen/TV;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/android/tv/KKCommonManager;->getInstance(Landroid/content/Context;)Lcom/konka/android/tv/KKCommonManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/android/tv/KKCommonManager;->getCurrentInputSource()Lcom/konka/android/tv/KKCommonManager$EN_KK_INPUT_SOURCE;

    move-result-object v1

    if-eq v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/lockscreen/TV;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/android/tv/KKCommonManager;->getInstance(Landroid/content/Context;)Lcom/konka/android/tv/KKCommonManager;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/konka/android/tv/KKCommonManager;->setInputSource(Lcom/konka/android/tv/KKCommonManager$EN_KK_INPUT_SOURCE;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public close()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "close"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected onFinishInflate()V
    .locals 0

    return-void
.end method

.method public open()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "open"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/konka/lockscreen/TV;->switchTVChannel()V

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/TV;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    if-eqz v0, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {p0}, Lcom/konka/lockscreen/TV;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/lockscreen/TV;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 8
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "surfaceChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "Surface is invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v7, v0, [I

    invoke-virtual {p0, v7}, Lcom/konka/lockscreen/TV;->getLocationInWindow([I)V

    new-instance v0, Lcom/konka/lockscreen/TV$TvThread;

    const/4 v1, 0x0

    aget v3, v7, v1

    const/4 v1, 0x1

    aget v4, v7, v1

    move-object v1, p0

    move-object v2, p1

    move v5, p3

    move v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/konka/lockscreen/TV$TvThread;-><init>(Lcom/konka/lockscreen/TV;Landroid/view/SurfaceHolder;IIII)V

    invoke-virtual {v0}, Lcom/konka/lockscreen/TV$TvThread;->start()V

    goto :goto_0
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 9
    .param p1    # Landroid/view/SurfaceHolder;

    const/4 v8, 0x0

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "surfaceCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-interface {p1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Surface;->isValid()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "Surface is invalid"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x2

    new-array v7, v0, [I

    invoke-virtual {p0, v7}, Lcom/konka/lockscreen/TV;->getLocationInWindow([I)V

    new-instance v0, Lcom/konka/lockscreen/TV$TvThread;

    aget v3, v7, v8

    const/4 v1, 0x1

    aget v4, v7, v1

    invoke-virtual {p0}, Lcom/konka/lockscreen/TV;->getWidth()I

    move-result v5

    invoke-virtual {p0}, Lcom/konka/lockscreen/TV;->getHeight()I

    move-result v6

    move-object v1, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v6}, Lcom/konka/lockscreen/TV$TvThread;-><init>(Lcom/konka/lockscreen/TV;Landroid/view/SurfaceHolder;IIII)V

    invoke-virtual {v0}, Lcom/konka/lockscreen/TV$TvThread;->start()V

    invoke-virtual {p0, v8}, Lcom/konka/lockscreen/TV;->setBackgroundResource(I)V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    iget-object v0, p0, Lcom/konka/lockscreen/TV;->TAG:Ljava/lang/String;

    const-string v1, "surfaceDestroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
