.class public Lcom/konka/lockscreen/UserIcon;
.super Landroid/widget/ImageView;
.source "UserIcon.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/lockscreen/UserIcon$3;
    }
.end annotation


# instance fields
.field private bActionDown:Z

.field private mContext:Landroid/content/Context;

.field private mUserMode:Lcom/konka/lockscreen/UserMode;

.field private mUsers:Lcom/konka/lockscreen/Users;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/lockscreen/UserIcon;->bActionDown:Z

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/lockscreen/UserIcon;->addEvent()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/lockscreen/UserIcon;->bActionDown:Z

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/lockscreen/UserIcon;->addEvent()V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;
    .param p3    # I

    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/lockscreen/UserIcon;->bActionDown:Z

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon;->mContext:Landroid/content/Context;

    invoke-direct {p0}, Lcom/konka/lockscreen/UserIcon;->addEvent()V

    return-void
.end method

.method static synthetic access$000(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/UserMode;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/UserIcon;

    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon;->mUserMode:Lcom/konka/lockscreen/UserMode;

    return-object v0
.end method

.method static synthetic access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/UserIcon;

    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;

    return-object v0
.end method

.method static synthetic access$200(Lcom/konka/lockscreen/UserIcon;)Z
    .locals 1
    .param p0    # Lcom/konka/lockscreen/UserIcon;

    iget-boolean v0, p0, Lcom/konka/lockscreen/UserIcon;->bActionDown:Z

    return v0
.end method

.method static synthetic access$202(Lcom/konka/lockscreen/UserIcon;Z)Z
    .locals 0
    .param p0    # Lcom/konka/lockscreen/UserIcon;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/lockscreen/UserIcon;->bActionDown:Z

    return p1
.end method

.method private addEvent()V
    .locals 1

    :try_start_0
    new-instance v0, Lcom/konka/lockscreen/UserIcon$1;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/UserIcon$1;-><init>(Lcom/konka/lockscreen/UserIcon;)V

    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/UserIcon;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v0, Lcom/konka/lockscreen/UserIcon$2;

    invoke-direct {v0, p0}, Lcom/konka/lockscreen/UserIcon$2;-><init>(Lcom/konka/lockscreen/UserIcon;)V

    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/UserIcon;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private scale(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 7
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v1, 0x0

    const v0, 0x3f99999a    # 1.2f

    new-instance v5, Landroid/graphics/Matrix;

    invoke-direct {v5}, Landroid/graphics/Matrix;-><init>()V

    invoke-virtual {v5, v0, v0}, Landroid/graphics/Matrix;->postScale(FF)Z

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    const/4 v6, 0x1

    move-object v0, p1

    move v2, v1

    invoke-static/range {v0 .. v6}, Landroid/graphics/Bitmap;->createBitmap(Landroid/graphics/Bitmap;IIIILandroid/graphics/Matrix;Z)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private setLeftAlpha(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 15
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int v0, v3, v7

    new-array v1, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    const/4 v9, 0x0

    const/4 v14, 0x0

    :goto_0
    if-ge v14, v7, :cond_2

    const/4 v13, 0x0

    :goto_1
    if-ge v13, v3, :cond_1

    aget v0, v1, v9

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v12, v0, 0xff

    if-ge v12, v8, :cond_0

    move v11, v12

    :goto_2
    shl-int/lit8 v0, v11, 0x18

    aget v2, v1, v9

    const v4, 0xffffff

    and-int/2addr v2, v4

    or-int/2addr v0, v2

    aput v0, v1, v9

    add-int/lit8 v0, v8, 0x1

    rem-int/lit16 v8, v0, 0xff

    add-int/lit8 v9, v9, 0x1

    add-int/lit8 v13, v13, 0x1

    goto :goto_1

    :cond_0
    move v11, v8

    goto :goto_2

    :cond_1
    const/4 v8, 0x0

    add-int/lit8 v14, v14, 0x1

    goto :goto_0

    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v7, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method

.method private setRightAlpha(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 15
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v10, 0x0

    const/4 v8, 0x0

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v3

    invoke-virtual/range {p1 .. p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    mul-int v0, v3, v7

    new-array v1, v0, [I

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object/from16 v0, p1

    move v6, v3

    invoke-virtual/range {v0 .. v7}, Landroid/graphics/Bitmap;->getPixels([IIIIIII)V

    mul-int v0, v3, v7

    add-int/lit8 v9, v0, -0x1

    move v14, v7

    :goto_0
    if-lez v14, :cond_2

    move v13, v3

    :goto_1
    if-lez v13, :cond_1

    aget v0, v1, v9

    shr-int/lit8 v0, v0, 0x18

    and-int/lit16 v12, v0, 0xff

    if-ge v12, v8, :cond_0

    move v11, v12

    :goto_2
    shl-int/lit8 v0, v11, 0x18

    aget v2, v1, v9

    const v4, 0xffffff

    and-int/2addr v2, v4

    or-int/2addr v0, v2

    aput v0, v1, v9

    add-int/lit8 v0, v8, 0x1

    rem-int/lit16 v8, v0, 0xff

    add-int/lit8 v9, v9, -0x1

    add-int/lit8 v13, v13, -0x1

    goto :goto_1

    :cond_0
    move v11, v8

    goto :goto_2

    :cond_1
    const/4 v8, 0x0

    add-int/lit8 v14, v14, -0x1

    goto :goto_0

    :cond_2
    sget-object v0, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {v1, v3, v7, v0}, Landroid/graphics/Bitmap;->createBitmap([IIILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object p1

    return-object p1
.end method


# virtual methods
.method public setImageBitmap(Landroid/graphics/Bitmap;)V
    .locals 3
    .param p1    # Landroid/graphics/Bitmap;

    const/4 v0, 0x0

    sget-object v1, Lcom/konka/lockscreen/UserIcon$3;->$SwitchMap$com$konka$lockscreen$UserMode:[I

    iget-object v2, p0, Lcom/konka/lockscreen/UserIcon;->mUserMode:Lcom/konka/lockscreen/UserMode;

    invoke-virtual {v2}, Lcom/konka/lockscreen/UserMode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    invoke-direct {p0, p1}, Lcom/konka/lockscreen/UserIcon;->scale(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    :goto_0
    invoke-super {p0, v0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    return-void

    :pswitch_0
    invoke-direct {p0, p1}, Lcom/konka/lockscreen/UserIcon;->setLeftAlpha(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0, p1}, Lcom/konka/lockscreen/UserIcon;->setRightAlpha(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;

    move-result-object v0

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setUserMode(Lcom/konka/lockscreen/UserMode;)V
    .locals 0
    .param p1    # Lcom/konka/lockscreen/UserMode;

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon;->mUserMode:Lcom/konka/lockscreen/UserMode;

    return-void
.end method

.method public setUsers(Lcom/konka/lockscreen/Users;)V
    .locals 0
    .param p1    # Lcom/konka/lockscreen/Users;

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;

    return-void
.end method
