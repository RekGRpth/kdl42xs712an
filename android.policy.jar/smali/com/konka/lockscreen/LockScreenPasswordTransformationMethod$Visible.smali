.class Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;
.super Landroid/os/Handler;
.source "LockScreenPasswordTransformationMethod.java"

# interfaces
.implements Landroid/text/style/UpdateLayout;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Visible"
.end annotation


# instance fields
.field private mText:Landroid/text/Spannable;

.field private mTransformer:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;


# direct methods
.method public constructor <init>(Landroid/text/Spannable;Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;)V
    .locals 4
    .param p1    # Landroid/text/Spannable;
    .param p2    # Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    iput-object p1, p0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;->mText:Landroid/text/Spannable;

    iput-object p2, p0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;->mTransformer:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    const-wide/16 v2, 0x96

    add-long/2addr v0, v2

    invoke-virtual {p0, p0, v0, v1}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;->postAtTime(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic access$100(Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;)Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;->mTransformer:Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    return-object v0
.end method


# virtual methods
.method public run()V
    .locals 1

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod$Visible;->mText:Landroid/text/Spannable;

    invoke-interface {v0, p0}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    return-void
.end method
