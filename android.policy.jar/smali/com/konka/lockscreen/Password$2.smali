.class Lcom/konka/lockscreen/Password$2;
.super Ljava/lang/Object;
.source "Password.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/Password;->initUI()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/Password;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/Password;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 0
    .param p1    # Landroid/text/Editable;

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 4
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    const/4 v1, 0x4

    if-lt v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    # getter for: Lcom/konka/lockscreen/Password;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/Password;->access$100(Lcom/konka/lockscreen/Password;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/Users;->chechPassword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    # getter for: Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/lockscreen/Password;->access$200(Lcom/konka/lockscreen/Password;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x10803cb    # android.R.drawable.konka_lockscreen_correct

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    sget-object v1, Lcom/konka/lockscreen/PasswordMode;->LOCK:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    invoke-virtual {v0}, Lcom/konka/lockscreen/Password;->login()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    new-instance v1, Lcom/konka/lockscreen/Password$2$1;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$2$1;-><init>(Lcom/konka/lockscreen/Password$2;)V

    const-wide/16 v2, 0x3e8

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    # getter for: Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/lockscreen/Password;->access$200(Lcom/konka/lockscreen/Password;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x10803d2    # android.R.drawable.konka_lockscreen_error

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/lockscreen/Password$2;->this$0:Lcom/konka/lockscreen/Password;

    # getter for: Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/konka/lockscreen/Password;->access$200(Lcom/konka/lockscreen/Password;)Landroid/widget/ImageView;

    move-result-object v0

    const v1, 0x10803de    # android.R.drawable.konka_lockscreen_locked

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0
.end method
