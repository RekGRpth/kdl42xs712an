.class public Lcom/konka/lockscreen/LockScreen;
.super Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;
.source "LockScreen.java"


# instance fields
.field private TAG:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

.field private mPassword:Lcom/konka/lockscreen/Password;

.field private mTV:Lcom/konka/lockscreen/TV;

.field private mUser:Lcom/konka/lockscreen/Users;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    invoke-direct {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;-><init>(Landroid/content/Context;)V

    const-string v2, "LockScreen"

    iput-object v2, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/lockscreen/LockScreen;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x109007c    # android.R.layout.konka_lockscreen

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/LockScreen;->addView(Landroid/view/View;)V

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/LockScreen;->initUI(Landroid/view/View;)V

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v2, "LockScreen"

    iput-object v2, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/lockscreen/LockScreen;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v2, 0x109007c    # android.R.layout.konka_lockscreen

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/LockScreen;->addView(Landroid/view/View;)V

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/LockScreen;->initUI(Landroid/view/View;)V

    return-void
.end method

.method private initUI(Landroid/view/View;)V
    .locals 6
    .param p1    # Landroid/view/View;

    const v2, 0x1020410    # android.R.id.konka_lockscreen_home

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    new-instance v2, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v3, p0, Lcom/konka/lockscreen/LockScreen;->mContext:Landroid/content/Context;

    const v4, 0x10803ca    # android.R.drawable.konka_lockscreen_bg

    invoke-static {v3, v4}, Lcom/konka/lockscreen/bitmap/BitmapTools;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setBackground(Landroid/graphics/drawable/Drawable;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :goto_0
    const v2, 0x1020411    # android.R.id.konka_lockscreen_user

    :try_start_1
    invoke-virtual {p0, v2}, Lcom/konka/lockscreen/LockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/lockscreen/Users;

    iput-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mUser:Lcom/konka/lockscreen/Users;

    const v2, 0x1020412    # android.R.id.konka_lockscreen_password

    invoke-virtual {p0, v2}, Lcom/konka/lockscreen/LockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/lockscreen/Password;

    iput-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mPassword:Lcom/konka/lockscreen/Password;

    iget-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mUser:Lcom/konka/lockscreen/Users;

    invoke-virtual {v2, p0}, Lcom/konka/lockscreen/Users;->setLockScreen(Lcom/konka/lockscreen/LockScreen;)V

    iget-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mUser:Lcom/konka/lockscreen/Users;

    iget-object v3, p0, Lcom/konka/lockscreen/LockScreen;->mPassword:Lcom/konka/lockscreen/Password;

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/Users;->setPassword(Lcom/konka/lockscreen/Password;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    const v2, 0x1020413    # android.R.id.konka_lockscreen_tv

    :try_start_2
    invoke-virtual {p0, v2}, Lcom/konka/lockscreen/LockScreen;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/konka/lockscreen/TV;

    iput-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mTV:Lcom/konka/lockscreen/TV;

    iget-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mContext:Landroid/content/Context;

    const v3, 0x10803e7    # android.R.drawable.konka_lockscreen_tv_default

    invoke-static {v2, v3}, Lcom/konka/lockscreen/bitmap/BitmapTools;->getSize(Landroid/content/Context;I)Landroid/graphics/BitmapFactory$Options;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mTV:Lcom/konka/lockscreen/TV;

    new-instance v3, Landroid/widget/LinearLayout$LayoutParams;

    iget v4, v1, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    iget v5, v1, Landroid/graphics/BitmapFactory$Options;->outHeight:I

    invoke-direct {v3, v4, v5}, Landroid/widget/LinearLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/TV;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    iget-object v2, p0, Lcom/konka/lockscreen/LockScreen;->mTV:Lcom/konka/lockscreen/TV;

    const v3, 0x10803e7    # android.R.drawable.konka_lockscreen_tv_default

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/TV;->setBackgroundResource(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0

    :goto_2
    return-void

    :catch_0
    move-exception v2

    goto :goto_2

    :catch_1
    move-exception v2

    goto :goto_1

    :catch_2
    move-exception v2

    goto :goto_0
.end method


# virtual methods
.method public cleanUp()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "cleanUp"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/konka/lockscreen/LockScreen;->closeTV()V

    return-void
.end method

.method public closeTV()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mTV:Lcom/konka/lockscreen/TV;

    invoke-virtual {v0}, Lcom/konka/lockscreen/TV;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public dismiss()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "dismiss"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method protected dispatchDraw(Landroid/graphics/Canvas;)V
    .locals 1
    .param p1    # Landroid/graphics/Canvas;

    invoke-super {p0, p1}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->dispatchDraw(Landroid/graphics/Canvas;)V

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDoneDrawing()V

    :cond_0
    return-void
.end method

.method public getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;
    .locals 1

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    return-object v0
.end method

.method public getUserActivityTimeout()J
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "getUserActivityTimeout"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public getViewMediatorCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;
    .locals 1

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    return-object v0
.end method

.method public handleBackKey()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public handleMenuKey()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onAttachedToWindow()V

    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    invoke-super {p0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewBase;->onDetachedFromWindow()V

    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    iget-object v1, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v2, "onFinishInflate"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/konka/lockscreen/LockScreen;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/lockscreen/service/FaceService;->getFaceService(Landroid/content/Context;)Lcom/konka/lockscreen/service/FaceService;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/konka/lockscreen/service/FaceService;->bindService()V

    iget-object v1, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v2, "face.bindService();"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public onScreenTurnedOff()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "onScreenTurnedOff"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onScreenTurnedOn()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "onScreenTurnedOn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onUserActivityTimeoutChanged()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "onUserActivityTimeoutChanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->onUserActivityTimeoutChanged()V

    :cond_0
    return-void
.end method

.method public openTV(Z)V
    .locals 1
    .param p1    # Z

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mTV:Lcom/konka/lockscreen/TV;

    invoke-virtual {v0}, Lcom/konka/lockscreen/TV;->open()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public refresh()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "refresh"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mPassword:Lcom/konka/lockscreen/Password;

    invoke-virtual {v0}, Lcom/konka/lockscreen/Password;->refresh()V

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mUser:Lcom/konka/lockscreen/Users;

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->refresh()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/LockScreen;->openTV(Z)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "reset"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public setLockPatternUtils(Lcom/android/internal/widget/LockPatternUtils;)V
    .locals 0
    .param p1    # Lcom/android/internal/widget/LockPatternUtils;

    iput-object p1, p0, Lcom/konka/lockscreen/LockScreen;->mLockPatternUtils:Lcom/android/internal/widget/LockPatternUtils;

    return-void
.end method

.method public show()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "show"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public showAssistant()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "showAssistant"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public userActivity()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "userActivity"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->userActivity()V

    :cond_0
    return-void
.end method

.method public verifyUnlock()V
    .locals 2

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "verifyUnlock"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public wakeWhenReadyTq(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->TAG:Ljava/lang/String;

    const-string v1, "wakeWhenReadyTq"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/LockScreen;->mViewMediatorCallback:Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    invoke-interface {v0}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->wakeUp()V

    :cond_0
    return-void
.end method
