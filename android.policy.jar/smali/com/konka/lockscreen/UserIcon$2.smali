.class Lcom/konka/lockscreen/UserIcon$2;
.super Ljava/lang/Object;
.source "UserIcon.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/UserIcon;->addEvent()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/UserIcon;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/UserIcon;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "UserIcon"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "KeyEvent.ACTION_DOWN keyCode = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    # setter for: Lcom/konka/lockscreen/UserIcon;->bActionDown:Z
    invoke-static {v0, v4}, Lcom/konka/lockscreen/UserIcon;->access$202(Lcom/konka/lockscreen/UserIcon;Z)Z

    :cond_0
    :goto_0
    return v3

    :cond_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v4, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->bActionDown:Z
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$200(Lcom/konka/lockscreen/UserIcon;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    # setter for: Lcom/konka/lockscreen/UserIcon;->bActionDown:Z
    invoke-static {v0, v3}, Lcom/konka/lockscreen/UserIcon;->access$202(Lcom/konka/lockscreen/UserIcon;Z)Z

    packed-switch p2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->prev()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->next()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/lockscreen/UserIcon$2;->this$0:Lcom/konka/lockscreen/UserIcon;

    # getter for: Lcom/konka/lockscreen/UserIcon;->mUsers:Lcom/konka/lockscreen/Users;
    invoke-static {v0}, Lcom/konka/lockscreen/UserIcon;->access$100(Lcom/konka/lockscreen/UserIcon;)Lcom/konka/lockscreen/Users;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->enter()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x15
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
