.class public Lcom/konka/lockscreen/Password;
.super Landroid/widget/LinearLayout;
.source "Password.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/lockscreen/Password$18;
    }
.end annotation


# instance fields
.field private final PASSWORD_NUM:I

.field private mClearButton:Landroid/widget/Button;

.field private mContext:Landroid/content/Context;

.field private mEditText:Landroid/widget/EditText;

.field private mFaceButton:Landroid/widget/Button;

.field private mPasswordButton:Landroid/widget/Button;

.field private mPasswordClear:Landroid/widget/LinearLayout;

.field private mPasswordInput:Landroid/widget/LinearLayout;

.field private mPasswordLock:Landroid/widget/LinearLayout;

.field private mStatus:Landroid/widget/ImageView;

.field private mUserStatusImg:Landroid/widget/ImageView;

.field private mUserStatusLog:Landroid/widget/TextView;

.field private mUsers:Lcom/konka/lockscreen/Users;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const/4 v1, 0x4

    iput v1, p0, Lcom/konka/lockscreen/Password;->PASSWORD_NUM:I

    iput-object p1, p0, Lcom/konka/lockscreen/Password;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x1090080    # android.R.layout.konka_lockscreen_password

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    invoke-direct {p0}, Lcom/konka/lockscreen/Password;->initUI()V

    return-void
.end method

.method static synthetic access$000(Lcom/konka/lockscreen/Password;)Landroid/widget/EditText;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Password;

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/konka/lockscreen/Password;)Lcom/konka/lockscreen/Users;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Password;

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mUsers:Lcom/konka/lockscreen/Users;

    return-object v0
.end method

.method static synthetic access$200(Lcom/konka/lockscreen/Password;)Landroid/widget/ImageView;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Password;

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;

    return-object v0
.end method

.method static synthetic access$300(Lcom/konka/lockscreen/Password;)Landroid/widget/Button;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Password;

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mClearButton:Landroid/widget/Button;

    return-object v0
.end method

.method private hideClear()V
    .locals 2

    const/4 v1, 0x4

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordClear:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordClear:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordInput:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordLock:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private initUI()V
    .locals 3

    const v1, 0x102041c    # android.R.id.konka_lockscreen_password_show

    :try_start_0
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordInput:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordInput:Landroid/widget/LinearLayout;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_11

    :goto_0
    const v1, 0x102042b    # android.R.id.konka_lockscreen_password_default

    :try_start_1
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordLock:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordLock:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_10

    :goto_1
    const v1, 0x102041e    # android.R.id.konka_lockscreen_password_status

    :try_start_2
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;

    const v1, 0x102042d    # android.R.id.konka_lockscreen_password_userlog

    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mUserStatusLog:Landroid/widget/TextView;

    const v1, 0x102042c    # android.R.id.konka_lockscreen_password_userlock

    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mUserStatusImg:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mUserStatusImg:Landroid/widget/ImageView;

    const v2, 0x10803de    # android.R.drawable.konka_lockscreen_locked

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_f

    :goto_2
    const v1, 0x102041d    # android.R.id.konka_lockscreen_password_input

    :try_start_3
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    invoke-static {}, Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;->getInstance()Lcom/konka/lockscreen/LockScreenPasswordTransformationMethod;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/konka/lockscreen/Password$1;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Password$1;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    new-instance v2, Lcom/konka/lockscreen/Password$2;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Password$2;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v1, v2}, Landroid/widget/EditText;->addTextChangedListener(Landroid/text/TextWatcher;)V
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_e

    :goto_3
    const v1, 0x102041a    # android.R.id.konka_lockscreen_password_clear

    :try_start_4
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordClear:Landroid/widget/LinearLayout;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_d

    :goto_4
    const v1, 0x102041b    # android.R.id.konka_lockscreen_password_clear_op

    :try_start_5
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mClearButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mClearButton:Landroid/widget/Button;

    new-instance v2, Lcom/konka/lockscreen/Password$3;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Password$3;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mClearButton:Landroid/widget/Button;

    new-instance v2, Lcom/konka/lockscreen/Password$4;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Password$4;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_c

    :goto_5
    const v1, 0x102042e    # android.R.id.konka_lockscreen_password_lock

    :try_start_6
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mPasswordButton:Landroid/widget/Button;

    new-instance v2, Lcom/konka/lockscreen/Password$5;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Password$5;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const v1, 0x102042f    # android.R.id.konka_lockscreen_face_lock

    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/lockscreen/Password;->mFaceButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mFaceButton:Landroid/widget/Button;

    new-instance v2, Lcom/konka/lockscreen/Password$6;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Password$6;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->showVerification(Z)V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_b

    :goto_6
    const v1, 0x1020429    # android.R.id.konka_lockscreen_num0

    :try_start_7
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$7;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$7;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_a

    :goto_7
    const v1, 0x102041f    # android.R.id.konka_lockscreen_num1

    :try_start_8
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$8;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$8;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_9

    :goto_8
    const v1, 0x1020420    # android.R.id.konka_lockscreen_num2

    :try_start_9
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$9;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$9;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_8

    :goto_9
    const v1, 0x1020421    # android.R.id.konka_lockscreen_num3

    :try_start_a
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$10;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$10;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    :goto_a
    const v1, 0x1020422    # android.R.id.konka_lockscreen_num4

    :try_start_b
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$11;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$11;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_6

    :goto_b
    const v1, 0x1020423    # android.R.id.konka_lockscreen_num5

    :try_start_c
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$12;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$12;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_5

    :goto_c
    const v1, 0x1020424    # android.R.id.konka_lockscreen_num6

    :try_start_d
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$13;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$13;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_d
    .catch Ljava/lang/Exception; {:try_start_d .. :try_end_d} :catch_4

    :goto_d
    const v1, 0x1020425    # android.R.id.konka_lockscreen_num7

    :try_start_e
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$14;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$14;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_e
    .catch Ljava/lang/Exception; {:try_start_e .. :try_end_e} :catch_3

    :goto_e
    const v1, 0x1020426    # android.R.id.konka_lockscreen_num8

    :try_start_f
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$15;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$15;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_2

    :goto_f
    const v1, 0x1020427    # android.R.id.konka_lockscreen_num9

    :try_start_10
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$16;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$16;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_10
    .catch Ljava/lang/Exception; {:try_start_10 .. :try_end_10} :catch_1

    :goto_10
    const v1, 0x1020428    # android.R.id.konka_lockscreen_delete

    :try_start_11
    invoke-virtual {p0, v1}, Lcom/konka/lockscreen/Password;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    new-instance v1, Lcom/konka/lockscreen/Password$17;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Password$17;-><init>(Lcom/konka/lockscreen/Password;)V

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V
    :try_end_11
    .catch Ljava/lang/Exception; {:try_start_11 .. :try_end_11} :catch_0

    :goto_11
    return-void

    :catch_0
    move-exception v1

    goto :goto_11

    :catch_1
    move-exception v1

    goto :goto_10

    :catch_2
    move-exception v1

    goto :goto_f

    :catch_3
    move-exception v1

    goto :goto_e

    :catch_4
    move-exception v1

    goto :goto_d

    :catch_5
    move-exception v1

    goto :goto_c

    :catch_6
    move-exception v1

    goto :goto_b

    :catch_7
    move-exception v1

    goto/16 :goto_a

    :catch_8
    move-exception v1

    goto/16 :goto_9

    :catch_9
    move-exception v1

    goto/16 :goto_8

    :catch_a
    move-exception v1

    goto/16 :goto_7

    :catch_b
    move-exception v1

    goto/16 :goto_6

    :catch_c
    move-exception v1

    goto/16 :goto_5

    :catch_d
    move-exception v1

    goto/16 :goto_4

    :catch_e
    move-exception v1

    goto/16 :goto_3

    :catch_f
    move-exception v1

    goto/16 :goto_2

    :catch_10
    move-exception v1

    goto/16 :goto_1

    :catch_11
    move-exception v1

    goto/16 :goto_0
.end method

.method private showClear()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordClear:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mClearButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordClear:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordInput:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordLock:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public editRequestFocus()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public login()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mUserStatusImg:Landroid/widget/ImageView;

    const v1, 0x10803df    # android.R.drawable.konka_lockscreen_login

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mUserStatusLog:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10400c4    # android.R.string.konka_lockscreen_unlock

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public refresh()V
    .locals 1

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->clearFocus()V

    sget-object v0, Lcom/konka/lockscreen/PasswordMode;->LOCK:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setFaceError()V
    .locals 3

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mUserStatusLog:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10400c7    # android.R.string.konka_lockscreen_face_fail

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V
    .locals 2
    .param p1    # Lcom/konka/lockscreen/PasswordMode;

    :try_start_0
    invoke-direct {p0}, Lcom/konka/lockscreen/Password;->hideClear()V

    sget-object v0, Lcom/konka/lockscreen/Password$18;->$SwitchMap$com$konka$lockscreen$PasswordMode:[I

    invoke-virtual {p1}, Lcom/konka/lockscreen/PasswordMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;

    const v1, 0x10803de    # android.R.drawable.konka_lockscreen_locked

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordClear:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordInput:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordLock:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Password;->showVerification(Z)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mStatus:Landroid/widget/ImageView;

    const v1, 0x10803de    # android.R.drawable.konka_lockscreen_locked

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageResource(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordLock:Landroid/widget/LinearLayout;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordInput:Landroid/widget/LinearLayout;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mEditText:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/konka/lockscreen/Password;->showClear()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setUserStatus(I)V
    .locals 3
    .param p1    # I

    const/4 v0, 0x1

    if-ne p1, v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mUserStatusLog:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10400c0    # android.R.string.konka_lockscreen_canceled

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mUserStatusLog:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/lockscreen/Password;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x10400bf    # android.R.string.konka_lockscreen_locked

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public setUsers(Lcom/konka/lockscreen/Users;)V
    .locals 0
    .param p1    # Lcom/konka/lockscreen/Users;

    iput-object p1, p0, Lcom/konka/lockscreen/Password;->mUsers:Lcom/konka/lockscreen/Users;

    return-void
.end method

.method public showVerification(Z)V
    .locals 2
    .param p1    # Z

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mFaceButton:Landroid/widget/Button;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mPasswordButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Password;->mFaceButton:Landroid/widget/Button;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
