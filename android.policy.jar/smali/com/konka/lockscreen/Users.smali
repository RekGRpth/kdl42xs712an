.class public Lcom/konka/lockscreen/Users;
.super Landroid/widget/LinearLayout;
.source "Users.java"


# instance fields
.field private final GO_TO_TV_MSG:Ljava/lang/String;

.field private final MAX_TRY_NUM:I

.field private final SHOW_PASSWORD_INPUT_UI:I

.field private final SYSTEM_UNLOCK:Ljava/lang/String;

.field private final TAG:Ljava/lang/String;

.field private bDone:Z

.field private bInitSwitch:Z

.field private bUnlockDoing:Z

.field private mAnimLeft:Landroid/view/animation/Animation;

.field private mAnimRight:Landroid/view/animation/Animation;

.field private mContext:Landroid/content/Context;

.field private mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

.field private mCurrent:I

.field private mFaceHandler:Landroid/os/Handler;

.field private mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

.field private mLockScreen:Lcom/konka/lockscreen/LockScreen;

.field private mName:Landroid/widget/TextView;

.field private mPassword:Lcom/konka/lockscreen/Password;

.field private mPasswordError:I

.field private mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

.field private mUserList:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/util/AttributeSet;

    const/4 v3, 0x0

    invoke-direct {p0, p1, p2}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    const-string v2, "com.konka.GO_TO_TV"

    iput-object v2, p0, Lcom/konka/lockscreen/Users;->GO_TO_TV_MSG:Ljava/lang/String;

    const-string v2, "com.konka.action.USER_SWITCHED"

    iput-object v2, p0, Lcom/konka/lockscreen/Users;->SYSTEM_UNLOCK:Ljava/lang/String;

    const-string v2, "Users"

    iput-object v2, p0, Lcom/konka/lockscreen/Users;->TAG:Ljava/lang/String;

    const/4 v2, 0x3

    iput v2, p0, Lcom/konka/lockscreen/Users;->MAX_TRY_NUM:I

    iput v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    iput v3, p0, Lcom/konka/lockscreen/Users;->mPasswordError:I

    iput-boolean v3, p0, Lcom/konka/lockscreen/Users;->bDone:Z

    iput-boolean v3, p0, Lcom/konka/lockscreen/Users;->bInitSwitch:Z

    iput-boolean v3, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    const/16 v2, 0x3e8

    iput v2, p0, Lcom/konka/lockscreen/Users;->SHOW_PASSWORD_INPUT_UI:I

    new-instance v2, Lcom/konka/lockscreen/Users$1;

    invoke-direct {v2, p0}, Lcom/konka/lockscreen/Users$1;-><init>(Lcom/konka/lockscreen/Users;)V

    iput-object v2, p0, Lcom/konka/lockscreen/Users;->mFaceHandler:Landroid/os/Handler;

    iput-object p1, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x1090081    # android.R.layout.konka_lockscreen_users

    invoke-virtual {v1, v2, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    :try_start_0
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    new-instance v3, Lcom/konka/lockscreen/Users$2;

    invoke-direct {v3, p0}, Lcom/konka/lockscreen/Users$2;-><init>(Lcom/konka/lockscreen/Users;)V

    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->registerUserSwitchObserver(Landroid/app/IUserSwitchObserver;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->initUI()V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/konka/lockscreen/Users;)Lcom/konka/lockscreen/Password;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    return-object v0
.end method

.method static synthetic access$100(Lcom/konka/lockscreen/Users;)Z
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget-boolean v0, p0, Lcom/konka/lockscreen/Users;->bDone:Z

    return v0
.end method

.method static synthetic access$102(Lcom/konka/lockscreen/Users;Z)Z
    .locals 0
    .param p0    # Lcom/konka/lockscreen/Users;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/lockscreen/Users;->bDone:Z

    return p1
.end method

.method static synthetic access$200(Lcom/konka/lockscreen/Users;)V
    .locals 0
    .param p0    # Lcom/konka/lockscreen/Users;

    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->done_real()V

    return-void
.end method

.method static synthetic access$300(Lcom/konka/lockscreen/Users;)Z
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget-boolean v0, p0, Lcom/konka/lockscreen/Users;->bInitSwitch:Z

    return v0
.end method

.method static synthetic access$302(Lcom/konka/lockscreen/Users;Z)Z
    .locals 0
    .param p0    # Lcom/konka/lockscreen/Users;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/konka/lockscreen/Users;->bInitSwitch:Z

    return p1
.end method

.method static synthetic access$400(Lcom/konka/lockscreen/Users;)I
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    return v0
.end method

.method static synthetic access$500(Lcom/konka/lockscreen/Users;)Ljava/util/List;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$600(Lcom/konka/lockscreen/Users;)Lcom/konka/lockscreen/LockScreen;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    return-object v0
.end method

.method static synthetic access$700(Lcom/konka/lockscreen/Users;)V
    .locals 0
    .param p0    # Lcom/konka/lockscreen/Users;

    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->switchUser()V

    return-void
.end method

.method static synthetic access$800(Lcom/konka/lockscreen/Users;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/konka/lockscreen/Users;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mFaceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method private done_real()V
    .locals 5

    const/4 v4, 0x1

    :try_start_0
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/lockscreen/service/FaceService;->getFaceService(Landroid/content/Context;)Lcom/konka/lockscreen/service/FaceService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/konka/lockscreen/service/FaceService;->unbindService()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->getInstance(Landroid/content/Context;)Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;

    move-result-object v2

    invoke-virtual {v2, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardUpdateMonitor;->setAlternateUnlockEnabled(Z)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v2}, Lcom/konka/lockscreen/LockScreen;->getViewMediatorCallback()Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    invoke-direct {p0, v2}, Lcom/konka/lockscreen/Users;->sendUnlockMsg(I)V

    invoke-interface {v1, v4}, Lcom/android/internal/policy/impl/keyguard/KeyguardViewMediator$ViewMediatorCallback;->keyguardDone(Z)V

    :cond_0
    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method private getSystemUsers()Ljava/util/List;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Landroid/content/pm/UserInfo;",
            ">;"
        }
    .end annotation

    const/4 v1, 0x0

    invoke-static {}, Landroid/os/UserManager;->supportsMultipleUsers()Z

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-object v1

    :cond_1
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    const-string v3, "user"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/UserManager;

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/UserManager;->getUsers(Z)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private getUserIcon(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1    # I

    :try_start_0
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v1, v0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, v0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, v0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    const v2, 0x10803d3    # android.R.drawable.konka_lockscreen_head1

    invoke-static {v1, v2}, Lcom/konka/lockscreen/bitmap/BitmapTools;->getBitmap(Landroid/content/Context;I)Landroid/graphics/Bitmap;

    move-result-object v1

    :goto_0
    return-object v1

    :cond_1
    iget-object v1, v0, Landroid/content/pm/UserInfo;->iconPath:Ljava/lang/String;

    invoke-static {v1}, Lcom/konka/lockscreen/bitmap/BitmapTools;->getBitmap(Ljava/lang/String;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v1

    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initUI()V
    .locals 2

    const v0, 0x1020430    # android.R.id.konka_lockscreen_left_user

    :try_start_0
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Users;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/lockscreen/UserIcon;

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    sget-object v1, Lcom/konka/lockscreen/UserMode;->LEFT_MODE:Lcom/konka/lockscreen/UserMode;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setUserMode(Lcom/konka/lockscreen/UserMode;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0, p0}, Lcom/konka/lockscreen/UserIcon;->setUsers(Lcom/konka/lockscreen/Users;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_3

    :goto_0
    const v0, 0x1020431    # android.R.id.konka_lockscreen_activity_user

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Users;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/lockscreen/UserIcon;

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    sget-object v1, Lcom/konka/lockscreen/UserMode;->ACTIVE_MODE:Lcom/konka/lockscreen/UserMode;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setUserMode(Lcom/konka/lockscreen/UserMode;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0, p0}, Lcom/konka/lockscreen/UserIcon;->setUsers(Lcom/konka/lockscreen/Users;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_2

    :goto_1
    const v0, 0x1020432    # android.R.id.konka_lockscreen_right_user

    :try_start_2
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Users;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/konka/lockscreen/UserIcon;

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    sget-object v1, Lcom/konka/lockscreen/UserMode;->RIGHT_MODE:Lcom/konka/lockscreen/UserMode;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setUserMode(Lcom/konka/lockscreen/UserMode;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0, p0}, Lcom/konka/lockscreen/UserIcon;->setUsers(Lcom/konka/lockscreen/Users;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :goto_2
    const v0, 0x1020433    # android.R.id.konka_lockscreen_name

    :try_start_3
    invoke-virtual {p0, v0}, Lcom/konka/lockscreen/Users;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mName:Landroid/widget/TextView;
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0

    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_0
.end method

.method private initUserShow()V
    .locals 6

    :try_start_0
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    if-nez v2, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-static {}, Lcom/konka/lockscreen/service/UserStatusLog;->getLastLoginUser()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/konka/lockscreen/Users;->searchUser(I)I

    move-result v2

    iput v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->switchUser()V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    const v3, 0x10a002d    # android.R.anim.konka_lockscreen_image_zoom_left

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/lockscreen/Users;->mAnimLeft:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    const v3, 0x10a002e    # android.R.anim.konka_lockscreen_image_zoom_right

    invoke-static {v2, v3}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/lockscreen/Users;->mAnimRight:Landroid/view/animation/Animation;

    new-instance v1, Lcom/konka/lockscreen/Users$3;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Users$3;-><init>(Lcom/konka/lockscreen/Users;)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mAnimLeft:Landroid/view/animation/Animation;

    invoke-virtual {v2, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mAnimRight:Landroid/view/animation/Animation;

    invoke-virtual {v2, v1}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    iget v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    if-lez v2, :cond_2

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v3, v3, -0x1

    invoke-direct {p0, v3}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_2
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-direct {p0, v3}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mName:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v4, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v2, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    iget-object v2, v2, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v5, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v4, v2}, Lcom/konka/lockscreen/service/UserStatusLog;->getUserStatus(Landroid/content/Context;I)I

    move-result v2

    invoke-virtual {v3, v2}, Lcom/konka/lockscreen/Password;->setUserStatus(I)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    iget v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v2, v2, 0x1

    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_3

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v3, v3, 0x1

    invoke-direct {p0, v3}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_3
    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/app/IActivityManager;->getCurrentUser()Landroid/content/pm/UserInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/UserInfo;->id:I

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/lockscreen/Users;->bInitSwitch:Z

    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/konka/lockscreen/Users;->bDone:Z

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v2}, Lcom/konka/lockscreen/LockScreen;->userActivity()V

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Landroid/app/IActivityManager;->switchUser(I)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v2

    goto/16 :goto_0
.end method

.method private searchUser(I)I
    .locals 4
    .param p1    # I

    const/4 v1, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/pm/UserInfo;

    iget v3, v2, Landroid/content/pm/UserInfo;->id:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    if-ne p1, v3, :cond_1

    :cond_0
    :goto_1
    return v1

    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v3

    const/4 v1, 0x0

    goto :goto_1
.end method

.method private sendUnlockMsg(I)V
    .locals 6
    .param p1    # I

    :try_start_0
    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    const-string v5, "user"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/os/UserManager;

    invoke-virtual {v2, p1}, Landroid/os/UserManager;->getUserPrefenence(I)I

    move-result v3

    const/4 v4, 0x1

    if-ne v4, v3, :cond_0

    new-instance v0, Landroid/content/Intent;

    const-string v4, "com.konka.GO_TO_TV"

    invoke-direct {v0, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/os/UserHandle;

    invoke-direct {v5, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v4, v0, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V

    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v4, "com.konka.action.USER_SWITCHED"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    new-instance v5, Landroid/os/UserHandle;

    invoke-direct {v5, p1}, Landroid/os/UserHandle;-><init>(I)V

    invoke-virtual {v4, v1, v5}, Landroid/content/Context;->sendBroadcastAsUser(Landroid/content/Intent;Landroid/os/UserHandle;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v4

    goto :goto_0
.end method

.method private sort()V
    .locals 2

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    new-instance v1, Lcom/konka/lockscreen/Users$4;

    invoke-direct {v1, p0}, Lcom/konka/lockscreen/Users$4;-><init>(Lcom/konka/lockscreen/Users;)V

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private switchUser()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v1}, Lcom/konka/lockscreen/LockScreen;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v1

    iget v2, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-virtual {v1, v2}, Lcom/android/internal/widget/LockPatternUtils;->setCurrentUser(I)V

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/lockscreen/Users;->bDone:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public chechPassword(Ljava/lang/String;)Z
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    :try_start_0
    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v3}, Lcom/konka/lockscreen/LockScreen;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v3

    invoke-virtual {v3, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    :cond_0
    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v4, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/pm/UserInfo;

    if-nez v0, :cond_1

    invoke-virtual {v1}, Landroid/content/pm/UserInfo;->isAdmin()Z

    move-result v3

    if-eqz v3, :cond_1

    iget v3, p0, Lcom/konka/lockscreen/Users;->mPasswordError:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/konka/lockscreen/Users;->mPasswordError:I

    iget v3, p0, Lcom/konka/lockscreen/Users;->mPasswordError:I

    const/4 v4, 0x3

    if-le v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    sget-object v4, Lcom/konka/lockscreen/PasswordMode;->CLEAR:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {v3, v4}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V

    const/4 v3, 0x0

    iput v3, p0, Lcom/konka/lockscreen/Users;->mPasswordError:I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    return v0

    :catch_0
    move-exception v3

    move v0, v2

    goto :goto_0
.end method

.method public chechPasswordNotLog(Ljava/lang/String;)Z
    .locals 1
    .param p1    # Ljava/lang/String;

    :try_start_0
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v0}, Lcom/konka/lockscreen/LockScreen;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/internal/widget/LockPatternUtils;->checkPassword(Ljava/lang/String;)Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :goto_0
    return v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public clearUsers()V
    .locals 6

    :try_start_0
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v2}, Lcom/konka/lockscreen/LockScreen;->getLockPatternUtils()Lcom/android/internal/widget/LockPatternUtils;

    move-result-object v2

    const-string v3, ""

    const/high16 v4, 0x20000

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/internal/widget/LockPatternUtils;->saveLockPassword(Ljava/lang/String;IZ)V

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/lockscreen/service/FaceService;->getFaceService(Landroid/content/Context;)Lcom/konka/lockscreen/service/FaceService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/lockscreen/service/FaceService;->getIFaceRecognitionService()Lcom/konka/facerecognition/IFaceRecognitionService;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/konka/facerecognition/IFaceRecognitionService;->hasPerson(I)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/konka/facerecognition/IFaceRecognitionService;->deletePerson(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->done_real()V

    return-void

    :catch_0
    move-exception v2

    goto :goto_0
.end method

.method public done()V
    .locals 3

    :try_start_0
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget v1, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v1}, Lcom/konka/lockscreen/service/UserStatusLog;->setLastLoginUser(I)V

    iget v1, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v1}, Lcom/konka/lockscreen/service/UserStatusLog;->clearUserStatus(I)V

    iget v1, v0, Landroid/content/pm/UserInfo;->id:I

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/lockscreen/Users;->bDone:Z

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    invoke-virtual {v1}, Lcom/konka/lockscreen/LockScreen;->userActivity()V

    invoke-static {}, Landroid/app/ActivityManagerNative;->getDefault()Landroid/app/IActivityManager;

    move-result-object v1

    iget v2, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v1, v2}, Landroid/app/IActivityManager;->switchUser(I)Z

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->done_real()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public enter()V
    .locals 6

    iget-boolean v2, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    if-eqz v2, :cond_0

    :goto_0
    return-void

    :cond_0
    :try_start_0
    const-string v2, ""

    invoke-virtual {p0, v2}, Lcom/konka/lockscreen/Users;->chechPasswordNotLog(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    invoke-virtual {v2}, Lcom/konka/lockscreen/Password;->login()V

    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lcom/konka/lockscreen/Users$5;

    invoke-direct {v3, p0}, Lcom/konka/lockscreen/Users$5;-><init>(Lcom/konka/lockscreen/Users;)V

    const-wide/16 v4, 0x3e8

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/konka/lockscreen/service/FaceService;->getFaceService(Landroid/content/Context;)Lcom/konka/lockscreen/service/FaceService;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/lockscreen/service/FaceService;->isCameraExist()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v1}, Lcom/konka/lockscreen/service/FaceService;->isFaceRec()Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/Password;->showVerification(Z)V

    goto :goto_0

    :cond_2
    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    sget-object v3, Lcom/konka/lockscreen/PasswordMode;->INPUT:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {v2, v3}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method

.method public gotoFaceRec()V
    .locals 6

    const-string v4, "Users"

    const-string v5, "gotoFaceRec"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/konka/lockscreen/service/FaceService;->getFaceService(Landroid/content/Context;)Lcom/konka/lockscreen/service/FaceService;

    move-result-object v2

    invoke-virtual {v2}, Lcom/konka/lockscreen/service/FaceService;->getIFaceRecognitionService()Lcom/konka/facerecognition/IFaceRecognitionService;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v4, "Users"

    const-string v5, "aidl == null"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    invoke-virtual {v4}, Lcom/konka/lockscreen/Password;->setFaceError()V

    :goto_0
    return-void

    :cond_0
    new-instance v4, Lcom/konka/lockscreen/Users$6;

    invoke-direct {v4, p0, v0}, Lcom/konka/lockscreen/Users$6;-><init>(Lcom/konka/lockscreen/Users;Lcom/konka/facerecognition/IFaceRecognitionService;)V

    invoke-interface {v0, v4}, Lcom/konka/facerecognition/IFaceRecognitionService;->setOnFaceRecResultCallback(Lcom/konka/facerecognition/IOnFaceRecResultCallback;)V

    iget-object v4, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v5, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v4, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/content/pm/UserInfo;

    iget v4, v3, Landroid/content/pm/UserInfo;->id:I

    invoke-interface {v0, v4}, Lcom/konka/facerecognition/IFaceRecognitionService;->showFaceRecognitionUI(I)V

    const-string v4, "Users"

    const-string v5, "aidl.showFaceRecognitionUI"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method public next()V
    .locals 4

    :try_start_0
    iget-boolean v0, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-lt v0, v1, :cond_2

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v1, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v2, v0}, Lcom/konka/lockscreen/service/UserStatusLog;->getUserStatus(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/konka/lockscreen/Password;->setUserStatus(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mAnimRight:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->startAnimation(Landroid/view/animation/Animation;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v1, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_4
    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_6

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v1, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    sget-object v1, Lcom/konka/lockscreen/PasswordMode;->LOCK:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public prev()V
    .locals 4

    :try_start_0
    iget-boolean v0, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    if-eqz v0, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    if-gez v0, :cond_2

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    goto :goto_0

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_2
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v1, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mName:Landroid/widget/TextView;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v2, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget-object v0, v0, Landroid/content/pm/UserInfo;->name:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    iget-object v2, p0, Lcom/konka/lockscreen/Users;->mContext:Landroid/content/Context;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    iget v3, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    invoke-interface {v0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/UserInfo;

    iget v0, v0, Landroid/content/pm/UserInfo;->id:I

    invoke-static {v2, v0}, Lcom/konka/lockscreen/service/UserStatusLog;->getUserStatus(Landroid/content/Context;I)I

    move-result v0

    invoke-virtual {v1, v0}, Lcom/konka/lockscreen/Password;->setUserStatus(I)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_3
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mAnimLeft:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->startAnimation(Landroid/view/animation/Animation;)V

    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_4
    :goto_1
    iget v0, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_7

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v1, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v1, v1, 0x1

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    :cond_5
    :goto_2
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    sget-object v1, Lcom/konka/lockscreen/PasswordMode;->LOCK:Lcom/konka/lockscreen/PasswordMode;

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/Password;->setPasswordMode(Lcom/konka/lockscreen/PasswordMode;)V

    goto/16 :goto_0

    :cond_6
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    iget v1, p0, Lcom/konka/lockscreen/Users;->mCurrent:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Lcom/konka/lockscreen/Users;->getUserIcon(I)Landroid/graphics/Bitmap;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setImageBitmap(Landroid/graphics/Bitmap;)V

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mLeftUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V

    goto :goto_1

    :cond_7
    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v0}, Lcom/konka/lockscreen/UserIcon;->getVisibility()I

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mRightUserIcon:Lcom/konka/lockscreen/UserIcon;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/konka/lockscreen/UserIcon;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2
.end method

.method public refresh()V
    .locals 4

    const/4 v3, 0x0

    const-string v1, "Users"

    const-string v2, "refresh"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean v3, p0, Lcom/konka/lockscreen/Users;->bUnlockDoing:Z

    :try_start_0
    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->getSystemUsers()Ljava/util/List;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    if-nez v1, :cond_3

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    :cond_2
    :goto_1
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/konka/lockscreen/Password;->showVerification(Z)V

    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->sort()V

    invoke-direct {p0}, Lcom/konka/lockscreen/Users;->initUserShow()V

    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mCurUserIcon:Lcom/konka/lockscreen/UserIcon;

    invoke-virtual {v1}, Lcom/konka/lockscreen/UserIcon;->requestFocus()Z

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_3
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    if-eq v1, v2, :cond_4

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;

    invoke-virtual {v1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/konka/lockscreen/Users;->mUserList:Ljava/util/List;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1
.end method

.method public setLockScreen(Lcom/konka/lockscreen/LockScreen;)V
    .locals 0
    .param p1    # Lcom/konka/lockscreen/LockScreen;

    iput-object p1, p0, Lcom/konka/lockscreen/Users;->mLockScreen:Lcom/konka/lockscreen/LockScreen;

    return-void
.end method

.method public setPassword(Lcom/konka/lockscreen/Password;)V
    .locals 1
    .param p1    # Lcom/konka/lockscreen/Password;

    iput-object p1, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    iget-object v0, p0, Lcom/konka/lockscreen/Users;->mPassword:Lcom/konka/lockscreen/Password;

    invoke-virtual {v0, p0}, Lcom/konka/lockscreen/Password;->setUsers(Lcom/konka/lockscreen/Users;)V

    return-void
.end method
