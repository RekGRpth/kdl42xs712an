.class Lcom/konka/lockscreen/Users$6;
.super Lcom/konka/facerecognition/IOnFaceRecResultCallback$Stub;
.source "Users.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/lockscreen/Users;->gotoFaceRec()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/lockscreen/Users;

.field final synthetic val$aidl:Lcom/konka/facerecognition/IFaceRecognitionService;


# direct methods
.method constructor <init>(Lcom/konka/lockscreen/Users;Lcom/konka/facerecognition/IFaceRecognitionService;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/lockscreen/Users$6;->this$0:Lcom/konka/lockscreen/Users;

    iput-object p2, p0, Lcom/konka/lockscreen/Users$6;->val$aidl:Lcom/konka/facerecognition/IFaceRecognitionService;

    invoke-direct {p0}, Lcom/konka/facerecognition/IOnFaceRecResultCallback$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public onClickPWLoginBtn()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Users"

    const-string v1, "onClickPWLoginBtn"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/Users$6;->val$aidl:Lcom/konka/facerecognition/IFaceRecognitionService;

    invoke-interface {v0}, Lcom/konka/facerecognition/IFaceRecognitionService;->closeFaceRecognitionUI()V

    iget-object v0, p0, Lcom/konka/lockscreen/Users$6;->this$0:Lcom/konka/lockscreen/Users;

    # getter for: Lcom/konka/lockscreen/Users;->mFaceHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/konka/lockscreen/Users;->access$800(Lcom/konka/lockscreen/Users;)Landroid/os/Handler;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    return-void
.end method

.method public onFaceRecFailed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Users"

    const-string v1, "onFaceRecFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onFaceRecSucceed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Users"

    const-string v1, "onFaceRecSucceed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/Users$6;->val$aidl:Lcom/konka/facerecognition/IFaceRecognitionService;

    invoke-interface {v0}, Lcom/konka/facerecognition/IFaceRecognitionService;->closeFaceRecognitionUI()V

    iget-object v0, p0, Lcom/konka/lockscreen/Users$6;->this$0:Lcom/konka/lockscreen/Users;

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->done()V

    return-void
.end method

.method public onFaceRegistFailed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Users"

    const-string v1, "onFaceRegistFailed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onFaceRegistSucceed()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "Users"

    const-string v1, "onFaceRegistSucceed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/konka/lockscreen/Users$6;->val$aidl:Lcom/konka/facerecognition/IFaceRecognitionService;

    invoke-interface {v0}, Lcom/konka/facerecognition/IFaceRecognitionService;->closeFaceRecognitionUI()V

    iget-object v0, p0, Lcom/konka/lockscreen/Users$6;->this$0:Lcom/konka/lockscreen/Users;

    invoke-virtual {v0}, Lcom/konka/lockscreen/Users;->done()V

    return-void
.end method
