.class Lcom/android/dreams/phototable/BummerView$1;
.super Landroid/os/Handler;
.source "BummerView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/dreams/phototable/BummerView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/dreams/phototable/BummerView;


# direct methods
.method constructor <init>(Lcom/android/dreams/phototable/BummerView;)V
    .locals 0

    iput-object p1, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 13
    .param p1    # Landroid/os/Message;

    const/4 v12, 0x2

    const/4 v0, 0x0

    iget v8, p1, Landroid/os/Message;->what:I

    packed-switch v8, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    # getter for: Lcom/android/dreams/phototable/BummerView;->mAnimate:Z
    invoke-static {v8}, Lcom/android/dreams/phototable/BummerView;->access$000(Lcom/android/dreams/phototable/BummerView;)Z

    move-result v0

    :pswitch_1
    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-virtual {v8}, Lcom/android/dreams/phototable/BummerView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    if-eqz v5, :cond_0

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredWidth()I

    move-result v8

    int-to-float v2, v8

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v8

    int-to-float v1, v8

    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-virtual {v8}, Lcom/android/dreams/phototable/BummerView;->getMeasuredWidth()I

    move-result v8

    int-to-float v7, v8

    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-virtual {v8}, Lcom/android/dreams/phototable/BummerView;->getMeasuredHeight()I

    move-result v8

    int-to-float v6, v8

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    sub-float v10, v2, v7

    float-to-double v10, v10

    mul-double/2addr v8, v10

    double-to-float v3, v8

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v8

    sub-float v10, v1, v6

    float-to-double v10, v10

    mul-double/2addr v8, v10

    double-to-float v4, v8

    if-eqz v0, :cond_1

    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-virtual {v8}, Lcom/android/dreams/phototable/BummerView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/view/ViewPropertyAnimator;->x(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/view/ViewPropertyAnimator;->y(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    iget-object v9, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    # getter for: Lcom/android/dreams/phototable/BummerView;->mAnimTime:I
    invoke-static {v9}, Lcom/android/dreams/phototable/BummerView;->access$100(Lcom/android/dreams/phototable/BummerView;)I

    move-result v9

    int-to-long v9, v9

    invoke-virtual {v8, v9, v10}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    move-result-object v8

    invoke-virtual {v8}, Landroid/view/ViewPropertyAnimator;->start()V

    :goto_1
    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    const/4 v9, 0x0

    invoke-virtual {v8, v9}, Lcom/android/dreams/phototable/BummerView;->setVisibility(I)V

    invoke-virtual {p0, v12}, Lcom/android/dreams/phototable/BummerView$1;->removeMessages(I)V

    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    # getter for: Lcom/android/dreams/phototable/BummerView;->mDelay:I
    invoke-static {v8}, Lcom/android/dreams/phototable/BummerView;->access$200(Lcom/android/dreams/phototable/BummerView;)I

    move-result v8

    int-to-long v8, v8

    invoke-virtual {p0, v12, v8, v9}, Lcom/android/dreams/phototable/BummerView$1;->sendEmptyMessageDelayed(IJ)Z

    goto :goto_0

    :cond_1
    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-virtual {v8, v3}, Lcom/android/dreams/phototable/BummerView;->setX(F)V

    iget-object v8, p0, Lcom/android/dreams/phototable/BummerView$1;->this$0:Lcom/android/dreams/phototable/BummerView;

    invoke-virtual {v8, v4}, Lcom/android/dreams/phototable/BummerView;->setY(F)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
