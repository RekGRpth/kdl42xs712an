.class public Lcom/android/dreams/phototable/SoftLandingInterpolator;
.super Ljava/lang/Object;
.source "SoftLandingInterpolator.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# instance fields
.field private final bottom:F

.field private final fly:Landroid/view/animation/LinearInterpolator;

.field private final lowerRange:F

.field private final mI:F

.field private final mO:F

.field private final slide:Landroid/view/animation/DecelerateInterpolator;

.field private final top:F

.field private final upperRange:F


# direct methods
.method public constructor <init>(FF)V
    .locals 5
    .param p1    # F
    .param p2    # F

    const/high16 v4, 0x40000000    # 2.0f

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v1, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v1}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->fly:Landroid/view/animation/LinearInterpolator;

    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    iput-object v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->slide:Landroid/view/animation/DecelerateInterpolator;

    iput p1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mI:F

    iput p2, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mO:F

    iget v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mI:F

    div-float/2addr v1, v4

    iget v2, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mI:F

    sub-float v2, v3, v2

    div-float/2addr v2, v4

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v0

    iget v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mI:F

    sub-float/2addr v1, v0

    iput v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->bottom:F

    iget v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mI:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->top:F

    iget v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->top:F

    iput v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->lowerRange:F

    iget v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->bottom:F

    sub-float v1, v3, v1

    iput v1, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->upperRange:F

    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 8
    .param p1    # F

    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v4, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->fly:Landroid/view/animation/LinearInterpolator;

    iget v5, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->upperRange:F

    div-float v5, p1, v5

    invoke-virtual {v4, v5}, Landroid/view/animation/LinearInterpolator;->getInterpolation(F)F

    move-result v4

    iget v5, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mO:F

    mul-float v1, v4, v5

    iget-object v4, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->slide:Landroid/view/animation/DecelerateInterpolator;

    iget v5, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->bottom:F

    sub-float v5, p1, v5

    iget v6, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->upperRange:F

    div-float/2addr v5, v6

    invoke-virtual {v4, v5}, Landroid/view/animation/DecelerateInterpolator;->getInterpolation(F)F

    move-result v4

    iget v5, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mO:F

    sub-float v5, v7, v5

    mul-float/2addr v4, v5

    iget v5, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->mO:F

    add-float v2, v4, v5

    iget v4, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->bottom:F

    cmpg-float v4, p1, v4

    if-gez v4, :cond_0

    move v3, v1

    :goto_0
    return v3

    :cond_0
    iget v4, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->top:F

    cmpg-float v4, p1, v4

    if-gez v4, :cond_1

    iget v4, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->bottom:F

    sub-float v4, p1, v4

    iget v5, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->top:F

    iget v6, p0, Lcom/android/dreams/phototable/SoftLandingInterpolator;->bottom:F

    sub-float/2addr v5, v6

    div-float v0, v4, v5

    sub-float v4, v7, v0

    mul-float/2addr v4, v1

    mul-float v5, v0, v2

    add-float v3, v4, v5

    goto :goto_0

    :cond_1
    move v3, v2

    goto :goto_0
.end method
