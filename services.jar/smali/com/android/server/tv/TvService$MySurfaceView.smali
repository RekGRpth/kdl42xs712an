.class public Lcom/android/server/tv/TvService$MySurfaceView;
.super Ljava/lang/Object;
.source "TvService.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "MySurfaceView"
.end annotation


# instance fields
.field private PIPView:Landroid/view/View;

.field private mSurfaceHolder:Landroid/view/SurfaceHolder;

.field private mode:I

.field private sufaceView3D:Landroid/view/SurfaceView;

.field final synthetic this$0:Lcom/android/server/tv/TvService;

.field private wm:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/android/server/tv/TvService;Landroid/content/Context;I)V
    .locals 7
    .param p2    # Landroid/content/Context;
    .param p3    # I

    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/android/server/tv/TvService$MySurfaceView;->this$0:Lcom/android/server/tv/TvService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v4, p0, Lcom/android/server/tv/TvService$MySurfaceView;->wm:Landroid/view/WindowManager;

    iput-object v4, p0, Lcom/android/server/tv/TvService$MySurfaceView;->sufaceView3D:Landroid/view/SurfaceView;

    iput v5, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mode:I

    iput-object v4, p0, Lcom/android/server/tv/TvService$MySurfaceView;->PIPView:Landroid/view/View;

    iput-object v4, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iput p3, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mode:I

    # setter for: Lcom/android/server/tv/TvService;->mySurfaceViewMode:I
    invoke-static {p3}, Lcom/android/server/tv/TvService;->access$202(I)I

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "window"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->wm:Landroid/view/WindowManager;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    const-string v3, "layout_inflater"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    const v2, 0x1090040    # android.R.layout.dualviewsurface

    invoke-virtual {v0, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->PIPView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->PIPView:Landroid/view/View;

    const v3, 0x1020387    # android.R.id.dualview_surface

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/SurfaceView;

    iput-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->sufaceView3D:Landroid/view/SurfaceView;

    iget-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->sufaceView3D:Landroid/view/SurfaceView;

    invoke-virtual {v2}, Landroid/view/SurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    iget-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    invoke-interface {v2, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mSurfaceHolder:Landroid/view/SurfaceHolder;

    const/4 v3, 0x3

    invoke-interface {v2, v3}, Landroid/view/SurfaceHolder;->setType(I)V

    new-instance v1, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v1}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    const/16 v2, 0x7d6

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->type:I

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v5, v1, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v6, v1, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v6, v1, Landroid/view/WindowManager$LayoutParams;->height:I

    const/16 v2, 0x33

    iput v2, v1, Landroid/view/WindowManager$LayoutParams;->gravity:I

    iget-object v2, p0, Lcom/android/server/tv/TvService$MySurfaceView;->wm:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/server/tv/TvService$MySurfaceView;->PIPView:Landroid/view/View;

    invoke-interface {v2, v3, v1}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const-string v0, "TvService"

    const-string v1, "---->Desmond---->Cheanged"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 5
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v3, "TvService"

    const-string v4, "---->Desmond---->Created"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-interface {p1}, Landroid/view/SurfaceHolder;->lockCanvas()Landroid/graphics/Canvas;

    move-result-object v0

    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/graphics/Canvas;->drawColor(I)V

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    invoke-interface {p1, v0}, Landroid/view/SurfaceHolder;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v3

    invoke-interface {v3, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget v3, p0, Lcom/android/server/tv/TvService$MySurfaceView;->mode:I

    invoke-static {v3}, Lcom/mstar/android/MDisplay;->set3DDisplayMode(I)V

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 2
    .param p1    # Landroid/view/SurfaceHolder;

    const-string v0, "TvService"

    const-string v1, "---->Desmond---->Destroyed"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
