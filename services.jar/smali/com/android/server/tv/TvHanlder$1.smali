.class Lcom/android/server/tv/TvHanlder$1;
.super Ljava/lang/Object;
.source "TvHanlder.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/tv/TvHanlder;->openSurfaceView(IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvHanlder;


# direct methods
.method constructor <init>(Lcom/android/server/tv/TvHanlder;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tv/TvHanlder$1;->this$0:Lcom/android/server/tv/TvHanlder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 4
    .param p1    # Landroid/view/SurfaceHolder;

    :try_start_0
    iget-object v2, p0, Lcom/android/server/tv/TvHanlder$1;->this$0:Lcom/android/server/tv/TvHanlder;

    # getter for: Lcom/android/server/tv/TvHanlder;->mTvPlayer:Lcom/mstar/android/tvapi/common/TvPlayer;
    invoke-static {v2}, Lcom/android/server/tv/TvHanlder;->access$000(Lcom/android/server/tv/TvHanlder;)Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-interface {v2, p1}, Lcom/mstar/android/tvapi/common/TvPlayer;->setDisplay(Landroid/view/SurfaceHolder;)V

    new-instance v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;-><init>()V

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder$1;->this$0:Lcom/android/server/tv/TvHanlder;

    # getter for: Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v2}, Lcom/android/server/tv/TvHanlder;->access$100(Lcom/android/server/tv/TvHanlder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->height:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder$1;->this$0:Lcom/android/server/tv/TvHanlder;

    # getter for: Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v2}, Lcom/android/server/tv/TvHanlder;->access$100(Lcom/android/server/tv/TvHanlder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->width:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder$1;->this$0:Lcom/android/server/tv/TvHanlder;

    # getter for: Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v2}, Lcom/android/server/tv/TvHanlder;->access$100(Lcom/android/server/tv/TvHanlder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->x:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->x:I

    iget-object v2, p0, Lcom/android/server/tv/TvHanlder$1;->this$0:Lcom/android/server/tv/TvHanlder;

    # getter for: Lcom/android/server/tv/TvHanlder;->surfaceParams:Landroid/view/WindowManager$LayoutParams;
    invoke-static {v2}, Lcom/android/server/tv/TvHanlder;->access$100(Lcom/android/server/tv/TvHanlder;)Landroid/view/WindowManager$LayoutParams;

    move-result-object v2

    iget v2, v2, Landroid/view/WindowManager$LayoutParams;->y:I

    iput v2, v1, Lcom/mstar/android/tvapi/common/vo/VideoWindowType;->y:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->selectWindow(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/mstar/android/tvapi/common/PictureManager;->setDisplayWindow(Lcom/mstar/android/tvapi/common/vo/VideoWindowType;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->scaleWindow()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 0
    .param p1    # Landroid/view/SurfaceHolder;

    return-void
.end method
