.class public Lcom/android/server/tv/DeskThreeDimensionEventListener;
.super Ljava/lang/Object;
.source "DeskThreeDimensionEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnThreeDimensionEventListener;


# static fields
.field private static threeDimensionEventListener:Lcom/android/server/tv/DeskThreeDimensionEventListener;


# instance fields
.field private iThreeDimensionEventClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/IThreeDimensionEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->threeDimensionEventListener:Lcom/android/server/tv/DeskThreeDimensionEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->iThreeDimensionEventClients:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->iThreeDimensionEventClients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskThreeDimensionEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->threeDimensionEventListener:Lcom/android/server/tv/DeskThreeDimensionEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskThreeDimensionEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskThreeDimensionEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->threeDimensionEventListener:Lcom/android/server/tv/DeskThreeDimensionEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->threeDimensionEventListener:Lcom/android/server/tv/DeskThreeDimensionEventListener;

    return-object v0
.end method


# virtual methods
.method public on4k2kUnsupportDualView(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskThreeDimensionEventListener"

    const-string v1, "on4k2kUnsupportDualView\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onEnable3D(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskThreeDimensionEventListener"

    const-string v1, "onEnable3D\n"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->iThreeDimensionEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/IThreeDimensionEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/IThreeDimensionEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskThreeDimensionEventListener;->iThreeDimensionEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
