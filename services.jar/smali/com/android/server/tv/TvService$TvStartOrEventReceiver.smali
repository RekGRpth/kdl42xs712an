.class Lcom/android/server/tv/TvService$TvStartOrEventReceiver;
.super Landroid/content/BroadcastReceiver;
.source "TvService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TvStartOrEventReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvService;


# direct methods
.method private constructor <init>(Lcom/android/server/tv/TvService;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tv/TvService$TvStartOrEventReceiver;->this$0:Lcom/android/server/tv/TvService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/android/server/tv/TvService;Lcom/android/server/tv/TvService$1;)V
    .locals 0
    .param p1    # Lcom/android/server/tv/TvService;
    .param p2    # Lcom/android/server/tv/TvService$1;

    invoke-direct {p0, p1}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver;-><init>(Lcom/android/server/tv/TvService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.mstar.android.tv.ui.tvstart"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    sput-boolean v1, Lcom/android/server/tv/TvService;->sIsTvStart:Z

    const-string v1, "TvStartOrNoEventReceiver"

    const-string v2, "------xxxxx--------true----------"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    const-string v1, "com.mstar.android.tv.ui.tvend"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    sput-boolean v1, Lcom/android/server/tv/TvService;->sIsTvStart:Z

    const-string v1, "TvStartOrNoEventReceiver"

    const-string v2, "------xxxxx--------false----------"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const-string v1, "com.konka.tv.hotkey.POWER_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$1;

    invoke-direct {v1, p0}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$1;-><init>(Lcom/android/server/tv/TvService$TvStartOrEventReceiver;)V

    invoke-virtual {v1}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$1;->start()V

    :cond_2
    const-string v1, "com.konka.tv.hotkey.VGA_NOSIGNALPOWER_OFF"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$2;

    invoke-direct {v1, p0}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$2;-><init>(Lcom/android/server/tv/TvService$TvStartOrEventReceiver;)V

    invoke-virtual {v1}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$2;->start()V

    :cond_3
    const-string v1, "com.konka.tv.hotkey.KEYCODE_TV_INPUT"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "TvStartOrNoEventReceiver"

    const-string v2, "KEYCODE_TV_INPUT"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$3;

    invoke-direct {v1, p0}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$3;-><init>(Lcom/android/server/tv/TvService$TvStartOrEventReceiver;)V

    invoke-virtual {v1}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver$3;->start()V

    :cond_4
    return-void
.end method
