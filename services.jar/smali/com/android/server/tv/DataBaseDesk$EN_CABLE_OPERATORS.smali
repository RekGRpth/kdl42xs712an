.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_CABLE_OPERATORS"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_CABLEREADY:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_CDCABLE:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_CDSMATV:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_COMHEM:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_OTHER:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_UPC:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_YOUSEE:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

.field public static final enum EN_CABLEOP_ZIGGO:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_OTHER"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_OTHER:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_CDSMATV"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDSMATV:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_CDCABLE"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDCABLE:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_COMHEM"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_COMHEM:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_UPC"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_UPC:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_YOUSEE"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_YOUSEE:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_CABLEREADY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CABLEREADY:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_ZIGGO"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_ZIGGO:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const-string v1, "EN_CABLEOP_NUM"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    const/16 v0, 0x9

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_OTHER:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDSMATV:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CDCABLE:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_COMHEM:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_UPC:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_YOUSEE:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_CABLEREADY:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_ZIGGO:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->EN_CABLEOP_NUM:Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_CABLE_OPERATORS;

    return-object v0
.end method
