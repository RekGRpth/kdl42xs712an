.class public Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MS_CEC_SETTING"
.end annotation


# instance fields
.field public mARCStatus:S

.field public mAudioModeStatus:S

.field public mAutoStandby:S

.field public mCECStatus:S

.field public mCheckSum:I


# direct methods
.method public constructor <init>(ISSSS)V
    .locals 0
    .param p1    # I
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;->mCheckSum:I

    iput-short p2, p0, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;->mCECStatus:S

    iput-short p3, p0, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;->mAutoStandby:S

    iput-short p4, p0, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;->mARCStatus:S

    iput-short p5, p0, Lcom/android/server/tv/DataBaseDesk$MS_CEC_SETTING;->mAudioModeStatus:S

    return-void
.end method
