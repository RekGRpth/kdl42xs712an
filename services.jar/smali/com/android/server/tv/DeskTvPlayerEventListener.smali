.class public Lcom/android/server/tv/DeskTvPlayerEventListener;
.super Ljava/lang/Object;
.source "DeskTvPlayerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnTvPlayerEventListener;


# static fields
.field private static tvEventListener:Lcom/android/server/tv/DeskTvPlayerEventListener;


# instance fields
.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/ITvPlayerEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskTvPlayerEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskTvPlayerEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvPlayerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskTvPlayerEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskTvPlayerEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskTvPlayerEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvPlayerEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskTvPlayerEventListener;->tvEventListener:Lcom/android/server/tv/DeskTvPlayerEventListener;

    return-object v0
.end method


# virtual methods
.method public on4k2kHDMIDisableDualView(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskTvPlayerEventListener"

    const-string v1, "on4k2kHDMIDisableDualView"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePip(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskTvPlayerEventListener"

    const-string v1, "on4k2kHDMIDisablePip"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePop(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskTvPlayerEventListener"

    const-string v1, "on4k2kHDMIDisablePop"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisableTravelingMode(III)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "DeskTvPlayerEventListener"

    const-string v1, "on4k2kHDMIDisableTravelingMode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public onEpgUpdateList(II)Z
    .locals 1
    .param p1    # I
    .param p2    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z
    .locals 5
    .param p1    # I
    .param p2    # Lcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onHbbtvUiEvent(ILcom/mstar/android/tvapi/common/vo/HbbtvEventInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPopupDialog(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2, p3}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPopupDialog(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyAlwaysTimeShiftProgramNotReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyAlwaysTimeShiftProgramReady(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyAlwaysTimeShiftProgramReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyCiPlusProtection(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyCiPlusProtection(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyCiPlusRetentionLimitUpdate(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyCiPlusRetentionLimitUpdate(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyOverRun(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyOverRun(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyParentalControl(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyParentalControl(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackBegin(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyPlaybackBegin(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackSpeedChange(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyPlaybackSpeedChange(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackStop(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyPlaybackStop(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyPlaybackTime(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyPlaybackTime(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyRecordSize(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyRecordSize(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyRecordStop(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyRecordStop(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyRecordTime(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyRecordTime(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyTimeShiftOverwritesAfter(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyTimeShiftOverwritesAfter(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyTimeShiftOverwritesBefore(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyTimeShiftOverwritesBefore(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyUsbRemoved(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onPvrNotifyUsbRemoved(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onScreenSaverMode(II)Z
    .locals 5
    .param p1    # I
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1, p2}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onScreenSaverMode(II)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSignalLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onSignalLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSignalUnLock(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onSignalUnLock(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onTvProgramInfoReady(I)Z
    .locals 5
    .param p1    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITvPlayerEventClient;

    invoke-interface {v0, p1}, Lcom/mstar/android/tv/ITvPlayerEventClient;->onTvProgramInfoReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/ITvPlayerEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/ITvPlayerEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskTvPlayerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
