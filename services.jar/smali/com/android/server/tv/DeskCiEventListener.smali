.class public Lcom/android/server/tv/DeskCiEventListener;
.super Ljava/lang/Object;
.source "DeskCiEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/CiManager$OnCiEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/DeskCiEventListener$EVENT;
    }
.end annotation


# static fields
.field private static ciEventListener:Lcom/android/server/tv/DeskCiEventListener;


# instance fields
.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/ICiEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskCiEventListener;->ciEventListener:Lcom/android/server/tv/DeskCiEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskCiEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskCiEventListener;->ciEventListener:Lcom/android/server/tv/DeskCiEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskCiEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskCiEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskCiEventListener;->ciEventListener:Lcom/android/server/tv/DeskCiEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskCiEventListener;->ciEventListener:Lcom/android/server/tv/DeskCiEventListener;

    return-object v0
.end method


# virtual methods
.method public onUiAutotestMessageShown(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ICiEventClient;

    invoke-interface {v0, p2}, Lcom/mstar/android/tv/ICiEventClient;->onUiAutotestMessageShown(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUiCardInserted(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ICiEventClient;

    invoke-interface {v0, p2}, Lcom/mstar/android/tv/ICiEventClient;->onUiCardInserted(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUiCardRemoved(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ICiEventClient;

    invoke-interface {v0, p2}, Lcom/mstar/android/tv/ICiEventClient;->onUiCardRemoved(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUiCloseMmi(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ICiEventClient;

    invoke-interface {v0, p2}, Lcom/mstar/android/tv/ICiEventClient;->onUiCloseMmi(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUiDataReady(Lcom/mstar/android/tvapi/dtv/common/CiManager;I)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CiManager;
    .param p2    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ICiEventClient;

    invoke-interface {v0, p2}, Lcom/mstar/android/tv/ICiEventClient;->onUiDataReady(I)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/ICiEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/ICiEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskCiEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
