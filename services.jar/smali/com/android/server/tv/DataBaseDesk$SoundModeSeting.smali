.class public Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SoundModeSeting"
.end annotation


# instance fields
.field public Balance:S

.field public Bass:S

.field public EqBand1:S

.field public EqBand2:S

.field public EqBand3:S

.field public EqBand4:S

.field public EqBand5:S

.field public EqBand6:S

.field public EqBand7:S

.field public Treble:S

.field public UserMode:Z

.field public enSoundAudioChannel:Lcom/android/server/tv/DataBaseDesk$EN_AUD_MODE;


# direct methods
.method public constructor <init>(SSSSSSS)V
    .locals 0
    .param p1    # S
    .param p2    # S
    .param p3    # S
    .param p4    # S
    .param p5    # S
    .param p6    # S
    .param p7    # S

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-short p1, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Bass:S

    iput-short p2, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->Treble:S

    iput-short p3, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand1:S

    iput-short p4, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand2:S

    iput-short p5, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand3:S

    iput-short p6, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand4:S

    iput-short p7, p0, Lcom/android/server/tv/DataBaseDesk$SoundModeSeting;->EqBand5:S

    return-void
.end method
