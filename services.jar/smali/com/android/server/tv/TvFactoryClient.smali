.class public Lcom/android/server/tv/TvFactoryClient;
.super Lcom/mstar/android/tv/ITvFactory$Stub;
.source "TvFactoryClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/TvFactoryClient$2;
    }
.end annotation


# static fields
.field static final AUTOTUNE_END_FAILED:I = 0x4e91

.field static final AUTOTUNE_END_SUCESSED:I = 0x4e90

.field static final AUTOTUNE_START:I = 0x4e8f

.field private static final TAG:Ljava/lang/String; = "TvFactoryClient"

.field private static final log:Landroid/util/Log;


# instance fields
.field private adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

.field private f_phase:I

.field private factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

.field private factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

.field private factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

.field private factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

.field private factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

.field private facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

.field private final max_handler:I

.field private mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

.field private overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field pHandler:[Landroid/os/Handler;

.field private sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

.field private st:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field private vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/TvFactoryClient;->log:Landroid/util/Log;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v2, 0x4

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvFactory$Stub;-><init>()V

    const/16 v0, 0x3b

    iput v0, p0, Lcom/android/server/tv/TvFactoryClient;->f_phase:I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput v2, p0, Lcom/android/server/tv/TvFactoryClient;->max_handler:I

    new-array v0, v2, [Landroid/os/Handler;

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->pHandler:[Landroid/os/Handler;

    invoke-static {p1}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    return-void
.end method

.method private GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetResolutionMapping, paras enCurrentInputType = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v2

    invoke-virtual {v2, p1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getResolutionMappingIndex(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)S

    move-result v0

    :cond_0
    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "GetResolutionMapping, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private InitVif()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    const-string v2, "InitVif"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/atv/AtvManager;->getAtvPlayerManager()Lcom/mstar/android/tvapi/atv/AtvPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/atv/AtvPlayer;->initAtvVif()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;
    .locals 3
    .param p1    # Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    sget-object v1, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    invoke-virtual {p1}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    :goto_0
    return-object v0

    :pswitch_0
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_1
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_ATV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_CVBS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_3
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SVIDEO:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_4
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_YPBPR:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_5
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SCART:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_6
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_HDMI:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_7
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_DTV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_8
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_OTHERS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_9
    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_5
        :pswitch_1
        :pswitch_4
        :pswitch_4
        :pswitch_4
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_7
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_9
    .end packed-switch
.end method

.method private UpdateSscPara()V
    .locals 3

    const-string v1, "TvFactoryClient"

    const-string v2, "UpdateSscPara"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->updateSscParameter()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private chmodFile(Ljava/io/File;)V
    .locals 6
    .param p1    # Ljava/io/File;

    const-string v3, "TvFactoryClient"

    const-string v4, "chmodFile"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "chmod 666 "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {p1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v3, "zyl"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "command = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Ljava/lang/Runtime;->getRuntime()Ljava/lang/Runtime;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/Runtime;->exec(Ljava/lang/String;)Ljava/lang/Process;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v1

    const-string v3, "zyl"

    const-string v4, "chmod fail!!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0
.end method

.method private copyFile(Ljava/io/File;Ljava/io/File;)Z
    .locals 5
    .param p1    # Ljava/io/File;
    .param p2    # Ljava/io/File;

    const-string v3, "TvFactoryClient"

    const-string v4, "copyFile"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    :try_start_0
    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, p1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    invoke-direct {p0, v1, p2}, Lcom/android/server/tv/TvFactoryClient;->copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    :try_start_2
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :goto_0
    invoke-direct {p0, p2}, Lcom/android/server/tv/TvFactoryClient;->chmodFile(Ljava/io/File;)V

    return v2

    :catchall_0
    move-exception v3

    :try_start_3
    invoke-virtual {v1}, Ljava/io/InputStream;->close()V

    throw v3
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    :catch_0
    move-exception v0

    const-string v3, "copyFile(File srcFile, File destFile)"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private copyToFile(Ljava/io/InputStream;Ljava/io/File;)Z
    .locals 7
    .param p1    # Ljava/io/InputStream;
    .param p2    # Ljava/io/File;

    const/4 v4, 0x0

    const-string v5, "TvFactoryClient"

    const-string v6, "copyToFile"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-virtual {p2}, Ljava/io/File;->exists()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-virtual {p2}, Ljava/io/File;->delete()Z

    :cond_0
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, p2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    const/16 v5, 0x1000

    :try_start_1
    new-array v0, v5, [B

    :goto_0
    invoke-virtual {p1, v0}, Ljava/io/InputStream;->read([B)I

    move-result v1

    if-ltz v1, :cond_1

    const-string v5, " out.write(buffer, 0, bytesRead);"

    const-string v6, " out.write(buffer, 0, bytesRead);"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v5, 0x0

    invoke-virtual {v3, v0, v5, v1}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v5

    :try_start_2
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    :try_start_3
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v6

    invoke-virtual {v6}, Ljava/io/FileDescriptor;->sync()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    :goto_1
    :try_start_4
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V

    throw v5
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    :catch_0
    move-exception v2

    const-string v5, "copyToFile(InputStream inputStream, File destFile)"

    invoke-virtual {v2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    return v4

    :cond_1
    :try_start_5
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->flush()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_0

    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v5

    invoke-virtual {v5}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1

    :goto_3
    :try_start_7
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_0

    const/4 v4, 0x1

    goto :goto_2

    :catch_1
    move-exception v5

    goto :goto_3

    :catch_2
    move-exception v6

    goto :goto_1
.end method

.method private setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;
    .param p2    # I

    const-string v3, "TvFactoryClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setADCGainOffset, paras enWin = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", iAdcIdx = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v1

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v3

    iput-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;

    move-result-object v3

    aget-object v2, v3, p2

    const-string v3, "FACTORY"

    const-string v4, "setADCGainOffset"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    invoke-virtual {v3, p1, v2, v4}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setAdcGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;Lcom/mstar/android/tvapi/factory/vo/EnumAdcSetIndexType;Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;)V

    :cond_0
    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    invoke-virtual {v3, v4, p2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateADCAdjust(Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;I)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private setPEQ()Z
    .locals 8

    const/4 v7, 0x1

    const/4 v6, 0x3

    const-string v3, "TvFactoryClient"

    const-string v4, "setPEQ"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v2}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v6, :cond_0

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    new-instance v4, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    invoke-direct {v4}, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;-><init>()V

    aput-object v4, v3, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_0
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v6, :cond_1

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    mul-int/lit8 v4, v4, 0x64

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    mul-int/lit8 v5, v5, 0x64

    div-int/lit16 v5, v5, 0xff

    add-int/2addr v4, v5

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqGc:I

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqGain:I

    iget-object v3, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->soundParameterPeqs:[Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;

    aget-object v3, v3, v1

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v4, v4, v1

    iget v4, v4, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    iput v4, v3, Lcom/mstar/android/tvapi/common/vo/SoundParameterPeq;->peqQvalue:I

    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_1
    iput-short v6, v2, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->peqBandNumber:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    const/4 v5, 0x1

    invoke-virtual {v3, v4, v5}, Lcom/mstar/android/tvapi/common/AudioManager;->enableBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Z)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PEQ:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v3, v4, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_2
    :goto_2
    return v7

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_2
.end method

.method private setWBGainOffset(III)V
    .locals 10
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const-string v0, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setWBGainOffset, paras iColorTemp = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", curInputSrc = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", srcId = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;->values()[Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;

    move-result-object v0

    add-int/lit8 v2, p1, 0x1

    aget-object v1, v0, v2

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v0

    aget-object v8, v0, p2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v0

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, p1

    aget-object v2, v2, p3

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v3, v3, p1

    aget-object v3, v3, p3

    iget v3, v3, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, p1

    aget-object v4, v4, p3

    iget v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, p1

    aget-object v5, v5, p3

    iget v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v6, v6, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v6, v6, p1

    aget-object v6, v6, p3

    iget v6, v6, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v7, v7, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v7, v7, p1

    aget-object v7, v7, p3

    iget v7, v7, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-virtual/range {v0 .. v8}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setWbGainOffsetEx(Lcom/mstar/android/tvapi/common/vo/EnumColorTemperature;IIIIIILcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v2, v2, p1

    aget-object v2, v2, p3

    invoke-virtual {v0, v2, p3, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryColorTempExData(Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;II)V

    return-void

    :catch_0
    move-exception v9

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public changeWbParameterWhenSourceChange()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "changeWbParameterWhenSourceChange"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public enableUartDebug()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    const-string v1, "FactoryDeskImpl"

    const-string v2, "---------mstv_tool------1----"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->enableUartDebug()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public execAutoAdc()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "execAutoAdc"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/android/server/tv/TvFactoryClient$1;

    invoke-direct {v1, p0}, Lcom/android/server/tv/TvFactoryClient$1;-><init>(Lcom/android/server/tv/TvFactoryClient;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    const/4 v0, 0x1

    return v0
.end method

.method public execSetInputSource(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "execSetInputSource, paras inputSource = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public get3DSelfAdaptiveLevel()I
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySelfAdaptiveLevel(I)I

    move-result v1

    return v1
.end method

.method public getAdcBlueGain()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdcBlueGain, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v3, v3, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    return v1
.end method

.method public getAdcBlueOffset()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdcBlueOffset, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v3, v3, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    return v1
.end method

.method public getAdcGreenGain()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdcGreenGain, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v3, v3, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    return v1
.end method

.method public getAdcGreenOffset()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdcGreenOffset, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v3, v3, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    return v1
.end method

.method public getAdcIndex()I
    .locals 4

    const/4 v0, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    :cond_0
    :goto_0
    sget-object v3, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_VGA:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    if-ne v0, v3, :cond_1

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ADC_SET_VGA:Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;

    :goto_1
    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ordinal()I

    move-result v3

    return v3

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;->ADC_SET_YPBPR_SD:Lcom/android/server/tv/DataBaseDesk$E_ADC_SET_INDEX;

    goto :goto_1
.end method

.method public getAdcPhase()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget v0, p0, Lcom/android/server/tv/TvFactoryClient;->f_phase:I

    return v0
.end method

.method public getAdcRedGain()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdcRedGain, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v3, v3, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    return v1
.end method

.method public getAdcRedOffset()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getAdcRedOffset, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v3, v3, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget v1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    return v1
.end method

.method public getAefc43()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefc43, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    return v0
.end method

.method public getAefc44()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefc44, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    return v0
.end method

.method public getAefc66Bit76()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefc66Bit76, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    return v0
.end method

.method public getAefc6EBit3210()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefc6EBit3210, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    return v0
.end method

.method public getAefc6EBit7654()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefc6EBit7654, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    return v0
.end method

.method public getAefcA0()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcA0, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    return v0
.end method

.method public getAefcA1()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcA1, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    return v0
.end method

.method public getAefcCb()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcCb, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    return v0
.end method

.method public getAefcCfBit2Atv()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    return v0
.end method

.method public getAefcCfBit2Av()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    return v0
.end method

.method public getAefcD4()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcD4, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    return v0
.end method

.method public getAefcD5Bit2()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcD5Bit2, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    return v0
.end method

.method public getAefcD7HighBound()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcD7HighBound, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    return v0
.end method

.method public getAefcD7LowBound()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcD7LowBound, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    return v0
.end method

.method public getAefcD8Bit3210()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcD8Bit3210, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    return v0
.end method

.method public getAefcD9Bit0()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAefcD9Bit0, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v2, v2, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-short v0, v0, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    return v0
.end method

.method public getAudioDspVersion()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAudioDspVersion, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    return v0
.end method

.method public getAudioHiDevMode()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAudioHiDevMode, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    return v0
.end method

.method public getAudioNrThreshold()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAudioNrThreshold, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    return v0
.end method

.method public getAudioPrescale()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryAudioPrescale()I

    move-result v0

    int-to-short v0, v0

    return v0
.end method

.method public getAudioSifThreshold()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getAudioSifThreshold, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    return v0
.end method

.method public getBoardType()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSystemBoardName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    :cond_0
    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getBoardType, return String "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->boardType:Ljava/lang/String;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getChinaDescramblerBox()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getChinaDescramblerBox, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    return v0
.end method

.method public getCompileTime()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getCompileTime, return String "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dayAndTime:Ljava/lang/String;

    return-object v0
.end method

.method public getCurveType()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v0

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getCurveType, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getDelayReduce()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getDelayReduce, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    return v0
.end method

.method public getDtvAvAbnormalDelay()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-boolean v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    return v0
.end method

.method public getFactoryPreSetFeature()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    return v0
.end method

.method public getGainDistributionThreshold()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getGainDistributionThreshold, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getHandler(I)Landroid/os/Handler;
    .locals 1
    .param p1    # I

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->pHandler:[Landroid/os/Handler;

    aget-object v0, v0, p1

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getLvdsModulation()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLvdsModulation, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    return v0
.end method

.method public getLvdsPercentage()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getLvdsPercentage, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    return v0
.end method

.method public getLvdsenable()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "getLvdsenable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-boolean v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    return v0
.end method

.method public getMiuEnable()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "getMiuEnable"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-boolean v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    return v0
.end method

.method public getMiuModulation()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMiuModulation, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    return v0
.end method

.method public getMiuPercentage()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getMiuPercentage, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    return v0
.end method

.method public getOsdV0Nonlinear()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iget-short v0, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOsdV0Nonlinear, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getOsdV100Nonlinear()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iget-short v0, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOsdV100Nonlinear, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getOsdV25Nonlinear()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iget-short v0, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOsdV25Nonlinear, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getOsdV50Nonlinear()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iget-short v0, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOsdV50Nonlinear, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getOsdV75Nonlinear()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    iget-short v0, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOsdV75Nonlinear, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getOverScanHPosition()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v6, "TvFactoryClient"

    const-string v7, "getOverScanHPosition"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v6

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto :goto_1

    :pswitch_1
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto :goto_1

    :pswitch_9
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_a
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto :goto_1

    :pswitch_11
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto/16 :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public getOverScanHSize()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v6, "TvFactoryClient"

    const-string v7, "getOverScanHSize"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v6

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto :goto_1

    :pswitch_1
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto :goto_1

    :pswitch_9
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_a
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto :goto_1

    :pswitch_11
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto/16 :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public getOverScanSourceType()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DVI4:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v2

    if-le v1, v2, :cond_0

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    :goto_0
    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getOverScanSourceType, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0

    :cond_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, v1

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    goto :goto_0
.end method

.method public getOverScanVPosition()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v6, "TvFactoryClient"

    const-string v7, "getOverScanVPosition"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v6

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto :goto_1

    :pswitch_1
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto :goto_1

    :pswitch_9
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_a
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto :goto_1

    :pswitch_11
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto/16 :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public getOverScanVSize()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v6, "TvFactoryClient"

    const-string v7, "getOverScanVSize"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v3

    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v6, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    const/4 v5, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v6

    invoke-interface {v6}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v6

    aget-object v4, v6, v5

    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v7}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    const/4 v6, -0x1

    :goto_1
    return v6

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_1

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto :goto_1

    :pswitch_1
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_8
    sget-object v6, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_2

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_3
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto :goto_1

    :pswitch_9
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_a
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_b
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_c
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_d
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_e
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_f
    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_3

    :pswitch_10
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto :goto_1

    :pswitch_11
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto/16 :goto_1

    :pswitch_12
    iget-object v6, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v7, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v7}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v7

    aget-object v6, v6, v7

    aget-object v6, v6, v0

    iget-short v6, v6, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    goto/16 :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public getPanelSwing()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    return v0
.end method

.method public getPanelType()Ljava/lang/String;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getSystemPanelName()Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    :cond_0
    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPanelType, return String "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v3, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelType:Ljava/lang/String;

    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getPeqFoCoarse(I)I
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqFoCoarse, paras index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqFoCoarse, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPeqFoFine(I)I
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqFoFine, paras index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqFoFine, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPeqGain(I)I
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqGain, paras index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqGain, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPeqQ(I)I
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPeqQ, paras index = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getPowerOnMode, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getPowerOnMode()I
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getEnvironmentPowerMode()Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->ordinal()I

    move-result v1

    :cond_0
    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "getPowerOnMode, return int "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getSoftWareVersion()Ljava/lang/String;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getSoftWareVersion, return String "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->softVersion:Ljava/lang/String;

    return-object v0
.end method

.method public getTestPattern()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getTestPattern, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getUartOnOff()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvFactoryClient"

    const-string v3, "getUartOnOff"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->getUartEnv()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :cond_0
    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public getVdDspVersion()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVdDspVersion, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    return v0
.end method

.method public getVifAgcRef()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAgcRefNegative:S

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVifAgcRef, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVifAsiaSignalOption()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "getVifAsiaSignalOption"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-boolean v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    return v0
.end method

.method public getVifClampGainOvNegative()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVifClampGainOvNegative, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVifCrKi()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVifCrKi, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVifCrKp()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVifCrKp, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVifCrKpKiAdjust()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "getVifCrKpKiAdjust"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-boolean v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    return v0
.end method

.method public getVifCrThreshold()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVifCrThreshold, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    return v0
.end method

.method public getVifOverModulation()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "getVifOverModulation"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-boolean v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    return v0
.end method

.method public getVifTop()I
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v1, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "getVifTop, return int "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public getVifVersion()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVifVersion, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-short v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    return v0
.end method

.method public getVifVgaMaximum()I
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "getVifVgaMaximum, return int "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget v0, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    return v0
.end method

.method public getWatchDogMode()Z
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x1

    const-string v1, "TvFactoryClient"

    const-string v2, "getWatchDogMode"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v2, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->bEnableWDT:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryUserSysSetting(Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getWbBlueGain()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, v1

    aget-object v5, v5, v4

    iget v3, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    const-string v5, "TvFactoryClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWbBlueGain, return int "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method

.method public getWbBlueOffset()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, v1

    aget-object v5, v5, v4

    iget v3, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    const-string v5, "TvFactoryClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWbBlueOffset, return int "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method

.method public getWbGreenGain()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, v1

    aget-object v5, v5, v4

    iget v3, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    const-string v5, "TvFactoryClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWbGreenGain, return int "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method

.method public getWbGreenOffset()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, v1

    aget-object v5, v5, v4

    iget v3, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    const-string v5, "TvFactoryClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWbGreenOffset, return int "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method

.method public getWbRedGain()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, v1

    aget-object v5, v5, v4

    iget v3, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    const-string v5, "TvFactoryClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWbRedGain, return int "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method

.method public getWbRedOffset()I
    .locals 8
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v5

    aget-object v5, v5, v0

    invoke-direct {p0, v5}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v5

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v4

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v5

    iput-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v5, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v5, v5, v1

    aget-object v5, v5, v4

    iget v3, v5, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    const-string v5, "TvFactoryClient"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "getWbRedOffset, return int "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return v3
.end method

.method public releaseHandler(I)V
    .locals 2
    .param p1    # I

    const/4 v0, 0x4

    if-ge p1, v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->pHandler:[Landroid/os/Handler;

    const/4 v1, 0x0

    aput-object v1, v0, p1

    :cond_0
    return-void
.end method

.method public restoreToDefault()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    const-string v5, "restoreToDefault"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x1

    const/4 v1, 0x0

    new-instance v3, Ljava/io/File;

    const-string v4, "/tvdatabase/DatabaseBackup/"

    const-string v5, "user_setting.db"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    const-string v4, "/tvdatabase/Database/"

    const-string v5, "user_setting.db"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0}, Lcom/android/server/tv/TvFactoryClient;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v1

    const/4 v1, 0x0

    if-nez v1, :cond_0

    const/4 v2, 0x0

    :cond_0
    new-instance v3, Ljava/io/File;

    const-string v4, "/tvdatabase/DatabaseBackup/"

    const-string v5, "factory.db"

    invoke-direct {v3, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/io/File;

    const-string v4, "/tvdatabase/Database/"

    const-string v5, "factory.db"

    invoke-direct {v0, v4, v5}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, v3, v0}, Lcom/android/server/tv/TvFactoryClient;->copyFile(Ljava/io/File;Ljava/io/File;)Z

    move-result v1

    const/4 v1, 0x0

    if-nez v1, :cond_1

    const/4 v2, 0x0

    :cond_1
    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v5, 0x4

    invoke-virtual {v4, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryOverscanAdjusts(I)[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    return v2
.end method

.method public set3DSelfAdaptiveLevel(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v3

    invoke-virtual {v2, p1, v3}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSelfAdaptiveLevel(II)V

    new-instance v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;

    invoke-direct {v0}, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;-><init>()V

    if-nez p1, :cond_1

    const/4 v2, 0x4

    :try_start_0
    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    const/4 v2, 0x4

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    const/4 v2, 0x4

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    const/4 v2, 0x7

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    const/4 v2, 0x7

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    const/16 v2, 0x5a

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return v4

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    if-ne p1, v4, :cond_2

    const/16 v2, 0x8

    :try_start_1
    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    const/16 v2, 0x8

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    const/16 v2, 0x8

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    const/4 v2, 0x6

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    const/4 v2, 0x6

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    const/16 v2, 0x46

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_2
    const/4 v2, 0x2

    if-ne p1, v2, :cond_0

    const/16 v2, 0xc

    :try_start_2
    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->gYPixelThreshold:I

    const/16 v2, 0xc

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->rCrPixelThreshold:I

    const/16 v2, 0xc

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->bCbPixelThreshold:I

    const/4 v2, 0x5

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->horSampleCount:I

    const/4 v2, 0x5

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->verSampleCount:I

    const/16 v2, 0x32

    iput v2, v0, Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;->hitPixelPercentage:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v2

    invoke-virtual {v2, v0}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->setDetect3dFormatParameters(Lcom/mstar/android/tvapi/common/vo/Detect3dFormatParameter;)Z
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcBlueGain(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdcBlueGain, paras blueGain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueGain:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/android/server/tv/TvFactoryClient;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAdcBlueOffset(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdcBlueOffset, paras blueOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->blueOffset:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/android/server/tv/TvFactoryClient;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAdcGreenGain(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdcGreenGain, paras greenGain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenGain:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/android/server/tv/TvFactoryClient;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAdcGreenOffset(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdcGreenOffset, paras greenOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->greenOffset:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/android/server/tv/TvFactoryClient;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAdcIndex(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAdcIndex, paras eIdx = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    return v0
.end method

.method public setAdcPhase(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iput p1, p0, Lcom/android/server/tv/TvFactoryClient;->f_phase:I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v1

    iget v2, p0, Lcom/android/server/tv/TvFactoryClient;->f_phase:I

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/common/TvPlayer;->setPhase(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAdcRedGain(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdcRedGain, paras redGain = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redGain:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/android/server/tv/TvFactoryClient;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAdcRedOffset(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAdcRedOffset, paras redOffset = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0}, Lcom/android/server/tv/TvFactoryClient;->getAdcIndex()I

    move-result v0

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryADCAdjust(I)Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->adcCalibData:Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;

    iput p1, v1, Lcom/mstar/android/tvapi/factory/vo/PqlCalibrationData;->redOffset:I

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;->E_MAIN_WINDOW:Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;

    invoke-direct {p0, v1, v0}, Lcom/android/server/tv/TvFactoryClient;->setADCGainOffset(Lcom/mstar/android/tvapi/common/vo/EnumScalerWindow;I)V

    const/4 v1, 0x1

    return v1
.end method

.method public setAefc43(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefc43, paras aefc43 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_43:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefc44(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefc44, paras aefc44 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_44:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefc66Bit76(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefc66Bit76, paras aefc66Bit76 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_66_Bit76:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefc6EBit3210(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefc6EBit3210, paras aefc6eBit3210 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit3210:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefc6EBit7654(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefc6EBit7654, paras aefc6eBit7654 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_6E_Bit7654:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcA0(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcA0, paras aefcA0 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A0:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcA1(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcA1, paras aefcA1 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_A1:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcCB(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcCB, paras aefcCB = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CB:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcCfBit2Atv(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_ATV:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcCfBit2Av(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_CF_Bit2_AV:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcD4(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcD4, paras aefcD4 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D4:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcD5Bit2(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcD5Bit2, paras aefcD5Bit2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D5_Bit2:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcD7HighBound(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcD7HighBound, paras aefcD7HighBound = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_HIGH_BOUND:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcD7LowBound(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcD7LowBound, paras aefcD7LowBound = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D7_LOW_BOUND:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcD8Bit3210(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcD8Bit3210, paras aefcD8Bit3210 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D8_Bit3210:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAefcD9Bit0(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAefcD9Bit0, paras aefcD9Bit0 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandSet()Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;->aFEC_D9_Bit0:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdInitParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setFactoryVdParameter(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->facvdpara:Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/mstar/android/tvapi/factory/vo/FactoryNsVdSet;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAudioDspVersion(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAudioDspVersion, paras aduioDspVersion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioDspVersion:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setAudioHiDevMode(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setAudioHiDevMode, paras audioHiDevMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->eHidevMode:I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;->E_ATV_HIDEV_INFO:Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;->values()[Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;

    move-result-object v3

    aget-object v3, v3, p1

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/AudioManager;->setAtvInfo(Lcom/mstar/android/tvapi/common/vo/EnumAtvInfoType;Lcom/mstar/android/tvapi/common/vo/EnumSoundHidevMode;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setAudioNrThreshold(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAudioNrThreshold, paras audioNrThreshold = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioNrThr:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v0

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;->E_SOUND_SET_PARAM_NR_THRESHOLD_:Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p1, v2}, Lcom/mstar/android/tvapi/common/AudioManager;->setSoundParameter(Lcom/mstar/android/tvapi/common/vo/EnumSoundSetParamType;II)S

    :cond_0
    const/4 v0, 0x1

    return v0
.end method

.method public setAudioPrescale(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    int-to-short v4, p1

    iput-short v4, v3, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioPreScale:S

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v3, v4}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    :try_start_0
    new-instance v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;

    invoke-direct {v1}, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;-><init>()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iput p1, v1, Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;->preScale:I

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v3

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;->E_PRESCALE:Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;

    invoke-virtual {v3, v4, v1}, Lcom/mstar/android/tvapi/common/AudioManager;->setBasicSoundEffect(Lcom/mstar/android/tvapi/common/vo/EnumSoundEffectType;Lcom/mstar/android/tvapi/common/vo/DtvSoundEffect;)Lcom/mstar/android/tvapi/common/vo/EnumAudioReturn;
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_0
    move-object v0, v1

    :goto_0
    const/4 v3, 0x1

    return v3

    :catch_0
    move-exception v2

    :goto_1
    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v2

    move-object v0, v1

    goto :goto_1
.end method

.method public setAudioSifThreshold(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setAudioSifThreshold, paras audioSifThreshold = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->audioSifThreshold:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setChinaDescramblerBox(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setChinaDescramblerBox, paras chinaDescramblerBox = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->ChinaDescramblerBox:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setCurveType(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setCurveType, paras curveTypeIndex = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v1

    aget-object v1, v1, p1

    iput-object v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    const/4 v0, 0x1

    return v0
.end method

.method public setDelayReduce(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setDelayReduce, paras delayReduce = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifDelayReduce:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setDtvAvAbnormalDelay(Z)Z
    .locals 2
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iput-boolean p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->dtvAvAbnormalDelay:Z

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    const-string v1, "setEnvironment"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    return v0
.end method

.method public setFactoryPreSetFeature(I)Z
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->factoryPreset:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setGainDistributionThreshold(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setGainDistributionThreshold, paras gainDistributionThreshold = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->GainDistributionThr:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setHandler(Landroid/os/Handler;I)Z
    .locals 2
    .param p1    # Landroid/os/Handler;
    .param p2    # I

    const/4 v0, 0x4

    if-ge p2, v0, :cond_1

    if-lez p2, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->pHandler:[Landroid/os/Handler;

    aget-object v0, v0, p2

    if-eqz v0, :cond_0

    const-string v0, "TvApp"

    const-string v1, "Warning ,!!!Some Activity lose release activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "TvApp"

    const-string v1, "Warning ,!!!Some Activity lose release activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->pHandler:[Landroid/os/Handler;

    aput-object p1, v0, p2

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setLvdsEnable(Z)Z
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLvdsEnable, paras lvdsSscEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput-boolean p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscEnable:Z

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setLvdsModulation(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLvdsModulation, paras lvdsSscSpan = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscSpan:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setLvdsPercentage(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setLvdsPercentage, paras lvdsSscStepze = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Lvds_SscStep:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMiuEnable(Z)Z
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMiuEnable, paras miuSscEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput-boolean p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu_SscEnable:Z

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMiuModulation(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMiuModulation, paras miuSscSpan = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscSpan:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscSpan:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscSpan:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setMiuPercentage(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setMiuPercentage, paras miuSscStep = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->querySSCAdjust()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu0_SscStep:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu1_SscStep:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;->Miu2_SscStep:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->sscSet:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateSSCAdjust(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_SSC_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->UpdateSscPara()V

    const/4 v0, 0x1

    return v0
.end method

.method public setOsdV0Nonlinear(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV0Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    int-to-short v3, p1

    iput-short v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V0:S

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonLinearAdjust(Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_6
    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV100Nonlinear(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV100Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    int-to-short v3, p1

    iput-short v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V100:S

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonLinearAdjust(Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_6
    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x64

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV25Nonlinear(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV25Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    int-to-short v3, p1

    iput-short v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V25:S

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonLinearAdjust(Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_6
    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x19

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV50Nonlinear(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV50Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    int-to-short v3, p1

    iput-short v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V50:S

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonLinearAdjust(Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_6
    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x32

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOsdV75Nonlinear(I)Z
    .locals 5
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v2, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setOsdV75Nonlinear, paras nonlinearVal = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNonLinearAdjusts()Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->values()[Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    move-result-object v3

    aget-object v3, v3, v1

    iput-object v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v2, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v2, v2, v1

    int-to-short v3, p1

    iput-short v3, v2, Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;->u8OSD_V75:S

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->stNLASetting:[Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;

    aget-object v3, v3, v1

    invoke-virtual {v2, v3, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonLinearAdjust(Lcom/android/server/tv/DataBaseDesk$MS_NLA_POINT;I)V

    sget-object v2, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MS_NLA_SET_INDEX:[I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->mfactoryNLASet:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;

    iget-object v3, v3, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SETTING;->msNlaSetIndex:Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;

    invoke-virtual {v3}, Lcom/android/server/tv/DataBaseDesk$MS_NLA_SET_INDEX;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :cond_0
    :goto_0
    const/4 v2, 0x1

    return v2

    :pswitch_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getAudioManager()Lcom/mstar/android/tvapi/common/AudioManager;

    move-result-object v2

    sget-object v3, Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;->E_VOL_SOURCE_SPEAKER_OUT:Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;

    int-to-byte v4, p1

    invoke-virtual {v2, v3, v4}, Lcom/mstar/android/tvapi/common/AudioManager;->setAudioVolume(Lcom/mstar/android/tvapi/common/vo/EnumAudioVolumeSourceType;B)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_1
    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeBrightness(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_2
    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeContrast(S)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_3
    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeColor(S)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    goto :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_4
    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeSharpness(S)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_5
    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setPictureModeTint(S)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    :pswitch_6
    :try_start_6
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    const/16 v3, 0x4b

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/PictureManager;->setBacklight(I)V
    :try_end_6
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_6 .. :try_end_6} :catch_6

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public setOverScanHPosition(I)Z
    .locals 14
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v8, "TvFactoryClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setOverScanHPosition, paras hPosition = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v8

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_1
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_8
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_5
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateATVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_9
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_a
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_10
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_6
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateYPbPrOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_11
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_4
    :goto_7
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateHDMIOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_12
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_5
    :goto_8
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateDTVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public setOverScanHSize(I)Z
    .locals 14
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v8, "TvFactoryClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setOverScanHSize, paras hSize = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v8

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_1
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_8
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_5
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateATVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_9
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_a
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_10
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_6
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateYPbPrOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_11
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_4
    :goto_7
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateHDMIOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_12
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_5
    :goto_8
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateDTVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public setOverScanSourceType(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setOverScanSourceType, paras SourceType = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, p1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setOverScanVPosition(I)Z
    .locals 14
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v8, "TvFactoryClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setOverScanVPosition, paras vPosition = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v8

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_1
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_8
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_5
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateATVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_9
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_a
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_10
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_6
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateYPbPrOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_11
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_4
    :goto_7
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateHDMIOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_12
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_5
    :goto_8
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateDTVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public setOverScanVSize(I)Z
    .locals 14
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v8, "TvFactoryClient"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "setOverScanVSize, paras vSize = "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v5

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v8, v5}, Lcom/android/server/tv/DataBaseDeskImpl;->queryArcMode(I)I

    move-result v0

    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-direct {p0, v8}, Lcom/android/server/tv/TvFactoryClient;->GetResolutionMapping(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)I

    move-result v1

    const/4 v7, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v8

    invoke-interface {v8}, Lcom/mstar/android/tvapi/common/TvPlayer;->getVideoStandard()Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/vo/EnumAvdVideoStandardType;->ordinal()I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v7

    :cond_0
    :goto_0
    invoke-static {}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->values()[Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;

    move-result-object v8

    aget-object v6, v8, v7

    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$mstar$android$tvapi$common$vo$TvOsType$EnumInputSource:[I

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->overSacnSourceType:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v9}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_0

    const/4 v8, 0x0

    :goto_1
    return v8

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :pswitch_0
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_1

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_2
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_1
    :goto_3
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryVDOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto :goto_1

    :pswitch_1
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_2
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_3
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_4
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_5
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_6
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :pswitch_7
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_2

    :catch_1
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_3

    :pswitch_8
    sget-object v8, Lcom/android/server/tv/TvFactoryClient$2;->$SwitchMap$com$android$server$tv$DataBaseDesk$MAPI_AVD_VideoStandardType:[I

    invoke-virtual {v6}, Lcom/android/server/tv/DataBaseDesk$MAPI_AVD_VideoStandardType;->ordinal()I

    move-result v9

    aget v8, v8, v9

    packed-switch v8, :pswitch_data_2

    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    :goto_4
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    aget-object v8, v8, v9

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    :try_start_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v10

    aget-object v9, v9, v10

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v11

    aget-object v10, v10, v11

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v12

    aget-object v11, v11, v12

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v13

    aget-object v12, v12, v13

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_2
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_2

    :cond_2
    :goto_5
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->ordinal()I

    move-result v9

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryATVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v9, v0, v10}, Lcom/android/server/tv/DataBaseDeskImpl;->updateATVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :pswitch_9
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_a
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_b
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_SECAM:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_c
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_d
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_M:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_e
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_PAL_NC:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :pswitch_f
    sget-object v4, Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;->SIG_NTSC_443:Lcom/android/server/tv/DataBaseDesk$EN_VD_SIGNALTYPE;

    goto :goto_4

    :catch_2
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_5

    :pswitch_10
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    :try_start_3
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_3

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_3
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_3 .. :try_end_3} :catch_3

    :cond_3
    :goto_6
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryYPbPrOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateYPbPrOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_3
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_6

    :pswitch_11
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    :try_start_4
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_4

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_4
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_4 .. :try_end_4} :catch_4

    :cond_4
    :goto_7
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryHDMIOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateHDMIOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_4
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :pswitch_12
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v8, v8, v1

    aget-object v8, v8, v0

    int-to-short v9, p1

    iput-short v9, v8, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    :try_start_5
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    if-eqz v8, :cond_5

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v8

    invoke-virtual {v8}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v8

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v9, v9, v1

    aget-object v9, v9, v0

    iget-short v9, v9, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Down:S

    iget-object v10, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v10, v10, v1

    aget-object v10, v10, v0

    iget-short v10, v10, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8VCrop_Up:S

    iget-object v11, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v11, v11, v1

    aget-object v11, v11, v0

    iget-short v11, v11, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Right:S

    iget-object v12, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    aget-object v12, v12, v1

    aget-object v12, v12, v0

    iget-short v12, v12, Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;->u8HCrop_Left:S

    invoke-virtual {v8, v9, v10, v11, v12}, Lcom/mstar/android/tvapi/common/PictureManager;->setOverscan(IIII)V
    :try_end_5
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_5 .. :try_end_5} :catch_5

    :cond_5
    :goto_8
    iget-object v8, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v9, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDTVOverscanSet:[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;

    invoke-virtual {v8, v1, v0, v9}, Lcom/android/server/tv/DataBaseDeskImpl;->updateDTVOverscanAdjust(II[[Lcom/android/server/tv/DataBaseDesk$ST_MAPI_VIDEO_WINDOW_INFO;)V

    const/4 v8, 0x1

    goto/16 :goto_1

    :catch_5
    move-exception v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_8

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_10
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_11
        :pswitch_12
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
    .end packed-switch
.end method

.method public setPanelSwing(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    int-to-short v2, p1

    iput-short v2, v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->panelSwingVal:S

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v1

    int-to-short v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/PictureManager;->setSwingLevel(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPeqFoCoarse(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPeqFoCoarse, paras index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", coarseVal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Foh:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePEQAdjust(Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;I)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->setPEQ()Z

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqFoFine(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPeqFoFine, paras index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", fineVal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Fol:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePEQAdjust(Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;I)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->setPEQ()Z

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqGain(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPeqGain, paras index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", gainVal = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->Gain:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePEQAdjust(Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;I)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->setPEQ()Z

    const/4 v0, 0x1

    return v0
.end method

.method public setPeqQ(II)Z
    .locals 3
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setPeqQ, paras index = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", QValue = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryPEQAdjusts()Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v0, v0, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v0, v0, p1

    iput p2, v0, Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;->QValue:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryPEQSet:Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;

    iget-object v1, v1, Lcom/android/server/tv/DataBaseDesk$ST_FACTORY_PEQ_SETTING;->stPEQParam:[Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;

    aget-object v1, v1, p1

    invoke-virtual {v0, v1, p1}, Lcom/android/server/tv/DataBaseDeskImpl;->updatePEQAdjust(Lcom/android/server/tv/DataBaseDesk$AUDIO_PEQ_PARAM;I)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->setPEQ()Z

    const/4 v0, 0x1

    return v0
.end method

.method public setPowerOnMode(I)Z
    .locals 6
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v3, "TvFactoryClient"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setPowerOnMode, paras factoryPowerMode = "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;

    move-result-object v3

    aget-object v1, v3, p1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v3

    invoke-virtual {v3, v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setEnvironmentPowerMode(Lcom/mstar/android/tvapi/factory/vo/EnumAcOnPowerOnMode;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    :cond_0
    :goto_0
    return v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setTestPattern(I)Z
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setTestPattern, paras testPatternMode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v1}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iput p1, v1, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->testPatternMode:I

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;->values()[Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setVideoTestPattern(Lcom/mstar/android/tvapi/factory/vo/EnumScreenMute;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUartOnOff(Z)Z
    .locals 4
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvFactoryClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setUartOnOff, paras isEnable = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->setUartEnv(Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setVdDspVersion(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVdDspVersion, paras vdDspVersion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryExtern()Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;->vdDspVersion:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->factoryExternSetting:Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateFactoryExtern(Lcom/android/server/tv/DataBaseDesk$MS_FACTORY_EXTERN_SETTING;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifAgcRef(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifAgcRef, paras vifAgcRef = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAgcRefNegative:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifAsiaSignalOption(Z)Z
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifAsiaSignalOption, paras vifAsiaSignalOption = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput-boolean p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifAsiaSignalOption:Z

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifClampGainOvNegative(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifClampGainOvNegative, paras vifClampGainOvNegative = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifClampgainGainOvNegative:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrKi(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifCrKp, paras vifCrKp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKi:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrKp(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifCrKp, paras vifCrKp = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKp:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrKpKiAdjust(Z)Z
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifCrKpKiAdjust, paras vifCrKpKiAdjust = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput-boolean p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrKpKiAdjust:Z

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifCrThreshold(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifCrThreshold, paras vifCrThreshold = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifCrThr:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifOverModulation(Z)Z
    .locals 3
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifOverModulation, paras vifOverModulation = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput-boolean p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifOverModulation:Z

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifTop(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifTop, paras vifTop = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifTop:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifVersion(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifVersion, paras vifVersion = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    int-to-short v1, p1

    iput-short v1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVersion:S

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    const/4 v0, 0x1

    return v0
.end method

.method public setVifVgaMaximum(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v0, "TvFactoryClient"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "setVifVgaMaximum, paras vifVgaMaximum = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryNoStandVifSet()Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    iput p1, v0, Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;->VifVgaMaximum:I

    iget-object v0, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvFactoryClient;->vifSet:Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;

    invoke-virtual {v0, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateNonStandardAdjust(Lcom/android/server/tv/DataBaseDesk$MS_Factory_NS_VIF_SET;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvFactoryClient;->InitVif()V

    const/4 v0, 0x1

    return v0
.end method

.method public setWatchDogMode(Z)Z
    .locals 5
    .param p1    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x1

    const-string v1, "TvFactoryClient"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setWatchDogMode, paras isEnable = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    sget-object v4, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->bEnableWDT:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    if-eqz p1, :cond_1

    move v1, v2

    :goto_0
    invoke-virtual {v3, v4, v1}, Lcom/android/server/tv/DataBaseDeskImpl;->updateUserSysSetting(Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;I)V

    if-ne p1, v2, :cond_2

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->enableWdt()Z

    :cond_0
    :goto_1
    return v2

    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/factory/FactoryManager;->disableWdt()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_1
.end method

.method public setWbBlueGain(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWbBlueGain, paras blueGain = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    invoke-direct {p0, v1, v0, v3}, Lcom/android/server/tv/TvFactoryClient;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbBlueOffset(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWbBlueOffset, paras blueOffset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    invoke-direct {p0, v1, v0, v3}, Lcom/android/server/tv/TvFactoryClient;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbGreenGain(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWbGreenGain, paras greenGain = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    invoke-direct {p0, v1, v0, v3}, Lcom/android/server/tv/TvFactoryClient;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbGreenOffset(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWbGreenOffset, paras greenOffset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    invoke-direct {p0, v1, v0, v3}, Lcom/android/server/tv/TvFactoryClient;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbRedGain(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWbRedGain, paras redGain = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    invoke-direct {p0, v1, v0, v3}, Lcom/android/server/tv/TvFactoryClient;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method

.method public setWbRedOffset(I)Z
    .locals 7
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v4, "TvFactoryClient"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "setWbRedOffset, paras redOffset = "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryCurInputSrc()I

    move-result v0

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0}, Lcom/android/server/tv/DataBaseDeskImpl;->queryePicMode(I)I

    move-result v2

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4, v0, v2}, Lcom/android/server/tv/DataBaseDeskImpl;->queryColorTmpIdx(II)I

    move-result v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v4

    aget-object v4, v4, v0

    invoke-direct {p0, v4}, Lcom/android/server/tv/TvFactoryClient;->InputSourceTransfer(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    move-result-object v4

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->ordinal()I

    move-result v3

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryDB:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-virtual {v4}, Lcom/android/server/tv/DataBaseDeskImpl;->queryFactoryColorTempExData()Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    move-result-object v4

    iput-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, p0, Lcom/android/server/tv/TvFactoryClient;->factoryColorTmpEx:Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;

    iget-object v4, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX;->astColorTempEx:[[Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;

    aget-object v4, v4, v1

    aget-object v4, v4, v3

    iput p1, v4, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    invoke-direct {p0, v1, v0, v3}, Lcom/android/server/tv/TvFactoryClient;->setWBGainOffset(III)V

    const/4 v4, 0x1

    return v4
.end method
