.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_DISPLAY_RES_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_CMO_CMO260J2_WUXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080I_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080I_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080P_24:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080P_25:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080P_30:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080P_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_1080P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_480I:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_480P:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_576I:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_576P:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_720P_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_720P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_DACOUT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_RES_FULL_HD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_RES_MAX_NUM:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_RES_SXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_RES_WSXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_RES_WXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_RES_WXGA_PLUS:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_SEC32_LE32A_FULLHD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_TTLOUT_480X272_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

.field public static final enum DISPLAY_VGAOUT_640x480P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_SEC32_LE32A_FULLHD"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_SEC32_LE32A_FULLHD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_RES_SXGA"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_SXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_RES_WXGA"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_WXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_RES_WXGA_PLUS"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_WXGA_PLUS:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_RES_WSXGA"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_WSXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_RES_FULL_HD"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_FULL_HD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_576I"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_576I:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_576P"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_576P:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_720P_50"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_720P_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080P_24"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_24:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080P_25"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_25:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080I_50"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080I_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080P_50"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_480I"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_480I:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_480P"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_480P:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_720P_60"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_720P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080P_30"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_30:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080I_60"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080I_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_1080P_60"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_DACOUT_AUTO"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_CMO_CMO260J2_WUXGA"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_CMO_CMO260J2_WUXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_VGAOUT_640x480P_60"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_VGAOUT_640x480P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_TTLOUT_480X272_60"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_TTLOUT_480X272_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const-string v1, "DISPLAY_RES_MAX_NUM"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_MAX_NUM:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    const/16 v0, 0x18

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_SEC32_LE32A_FULLHD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_SXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_WXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_WXGA_PLUS:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_WSXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_FULL_HD:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_576I:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_576P:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_720P_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_24:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_25:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080I_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_50:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_480I:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_480P:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_720P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_30:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080I_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_1080P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_DACOUT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_CMO_CMO260J2_WUXGA:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_VGAOUT_640x480P_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_TTLOUT_480X272_60:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->DISPLAY_RES_MAX_NUM:Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_DISPLAY_RES_TYPE;

    return-object v0
.end method
