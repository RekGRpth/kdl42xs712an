.class public Lcom/android/server/tv/TvAtscChannelClient;
.super Lcom/mstar/android/tv/ITvAtscChannel$Stub;
.source "TvAtscChannelClient.java"


# static fields
.field private static final TAG:Ljava/lang/String; = "TvAtscChannelClient"


# instance fields
.field private channelManager:Lcom/mstar/android/tvapi/common/ChannelManager;

.field private matscplayer:Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvAtscChannel$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public changeDtvToManualFirstService(I)Z
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/ChannelManager;->changeDtvToManualFirstService(I)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public changeProgramList()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->changeProgramList()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteAllMainList()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->deleteAllMainList()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteAtvMainList()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->deleteAtvMainList()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteChannelInformationByRf(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    int-to-short v2, p1

    invoke-virtual {v1, v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->deleteChannelInformationByRf(S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public deleteDtvMainList()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->deleteDtvMainList()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCanadaEngRatingLock()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public getCanadaFreRatingLock()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public getCurrentChannelInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrentChannelInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/AtscMainListChannelInformation;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->getCurrentProgramInfo()Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentRatingInformation()Ljava/lang/String;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->getCurrentRatingInformation()Ljava/lang/String;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getCurrentVChipBlockStatus()Z
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->getCurrentVChipBlockStatus()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getProgramInfo(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    .locals 2
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(I)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getRRTInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-interface {v1}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->getRRTInformation()Lcom/mstar/android/tvapi/dtv/atsc/vo/Region5RatingInformation;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getUsaMpaaRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getUsaTvRatingLock()Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return-object v0
.end method

.method public getVChipInputSourceBlockStatus(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public programSel(II)Z
    .locals 2
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(II)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    const/4 v1, 0x0

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public resetRRTSetting()Z
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setCanadaEngGuideline(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setCanadaEngGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaEngRatingType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCanadaEngRatingLock(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setCanadaFreGuideline(I)Z
    .locals 3
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setCanadaFreGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumCanadaFreRatingType;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setCanadaFreRatingLock(I)Z
    .locals 1
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setDtvAntennaType(I)V
    .locals 4
    .param p1    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const-string v1, "TvAtscChannelClient"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "setDtvAntennaType , paras type is "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-interface {v1, v2}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setAntennaType(Lcom/mstar/android/tvapi/common/vo/EnumAntennaType;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDynamicGuideline(III)Z
    .locals 5
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    int-to-short v2, p1

    int-to-short v3, p2

    int-to-short v4, p3

    invoke-interface {v1, v2, v3, v4}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setDynamicGuideline(SSS)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setProgramName(IILjava/lang/String;)V
    .locals 2
    .param p1    # I
    .param p2    # I
    .param p3    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v1

    invoke-virtual {v1, p1, p2, p3}, Lcom/mstar/android/tvapi/common/ChannelManager;->setProgramName(IILjava/lang/String;)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method public setUsaMpaaGuideline(IZ)Z
    .locals 3
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;

    move-result-object v2

    aget-object v2, v2, p1

    invoke-interface {v1, v2, p2}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setUsaMpaaGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType$EnumUsaMpaaRatingType;Z)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsaMpaaRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaMpaaRatingType;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setUsaTvGuideline(II)Z
    .locals 4
    .param p1    # I
    .param p2    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;->values()[Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;

    move-result-object v2

    aget-object v2, v2, p1

    int-to-short v3, p2

    invoke-interface {v1, v2, v3}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setUsaTvGuideline(Lcom/mstar/android/tvapi/dtv/atsc/vo/EnumUsaTvRatingType;S)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setUsaTvRatingLock(Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/dtv/atsc/vo/UsaTvRatingInformation;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v0, 0x0

    return v0
.end method

.method public setVChipGuideline(IIII)Z
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/dtv/common/DtvManager;->getAtscPlayerManager()Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;

    move-result-object v1

    int-to-short v2, p1

    int-to-short v3, p2

    int-to-short v4, p3

    int-to-short v5, p4

    invoke-interface {v1, v2, v3, v4, v5}, Lcom/mstar/android/tvapi/dtv/atsc/AtscPlayer;->setVChipGuideline(SSSS)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setVChipInputSourceBlockStatus(IZ)V
    .locals 0
    .param p1    # I
    .param p2    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method
