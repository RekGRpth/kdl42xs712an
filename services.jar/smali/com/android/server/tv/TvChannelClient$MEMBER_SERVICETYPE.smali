.class public final enum Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;
.super Ljava/lang/Enum;
.source "TvChannelClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvChannelClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "MEMBER_SERVICETYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

.field public static final enum E_SERVICETYPE_ATV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

.field public static final enum E_SERVICETYPE_DATA:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

.field public static final enum E_SERVICETYPE_DTV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

.field public static final enum E_SERVICETYPE_INVALID:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

.field public static final enum E_SERVICETYPE_RADIO:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

.field public static final enum E_SERVICETYPE_UNITED_TV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const-string v1, "E_SERVICETYPE_ATV"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    new-instance v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const-string v1, "E_SERVICETYPE_DTV"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    new-instance v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const-string v1, "E_SERVICETYPE_RADIO"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    new-instance v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const-string v1, "E_SERVICETYPE_DATA"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_DATA:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    new-instance v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const-string v1, "E_SERVICETYPE_UNITED_TV"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_UNITED_TV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    new-instance v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const-string v1, "E_SERVICETYPE_INVALID"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_INVALID:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    const/4 v0, 0x6

    new-array v0, v0, [Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    sget-object v1, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_ATV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_DTV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_RADIO:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_DATA:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_UNITED_TV:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->E_SERVICETYPE_INVALID:Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->$VALUES:[Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->$VALUES:[Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    invoke-virtual {v0}, [Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/TvChannelClient$MEMBER_SERVICETYPE;

    return-object v0
.end method
