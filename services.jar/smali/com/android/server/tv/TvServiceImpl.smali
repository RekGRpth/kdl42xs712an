.class public Lcom/android/server/tv/TvServiceImpl;
.super Lcom/mstar/android/tv/ITvService$Stub;
.source "TvServiceImpl.java"


# instance fields
.field private audioBinder:Lcom/android/server/tv/TvAudioClient;

.field private channelBinder:Lcom/android/server/tv/TvChannelClient;

.field private commonBinder:Lcom/android/server/tv/TvCommonClient;

.field private iTvAtscChannel:Lcom/android/server/tv/TvAtscChannelClient;

.field private iTvCc:Lcom/android/server/tv/TvCcClient;

.field private iTvCec:Lcom/android/server/tv/TvCecClient;

.field private iTvEpg:Lcom/android/server/tv/TvEpgClient;

.field private iTvFactory:Lcom/android/server/tv/TvFactoryClient;

.field private iTvPipPop:Lcom/mstar/android/tv/ITvPipPop;

.field private mContext:Landroid/content/Context;

.field private mHandler:Lcom/android/server/tv/TvHanlder;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private pictureBinder:Lcom/android/server/tv/TvPictureClient;

.field private s3DBinder:Lcom/android/server/tv/TvS3DClient;

.field private timerBinder:Lcom/android/server/tv/TvTimerClient;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/android/server/tv/DataBaseDeskImpl;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Lcom/android/server/tv/DataBaseDeskImpl;

    const/4 v1, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvService$Stub;-><init>()V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mContext:Landroid/content/Context;

    const-string v1, "TvService"

    const-string v2, "-----------------ready start mstar tv service."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iput-object p1, p0, Lcom/android/server/tv/TvServiceImpl;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/os/HandlerThread;

    const-string v2, "tv_view_handler"

    invoke-direct {v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    iget-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->start()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    new-instance v1, Lcom/android/server/tv/TvHanlder;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v3

    invoke-direct {v1, v2, p1, v3, p0}, Lcom/android/server/tv/TvHanlder;-><init>(Landroid/os/HandlerThread;Landroid/content/Context;Lcom/mstar/android/tvapi/common/TvPlayer;Lcom/mstar/android/tv/ITvService;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    :cond_0
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->disableTvosIr()V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    :goto_0
    new-instance v1, Lcom/android/server/tv/TvCommonClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, p1, v2, p2}, Lcom/android/server/tv/TvCommonClient;-><init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->commonBinder:Lcom/android/server/tv/TvCommonClient;

    new-instance v1, Lcom/android/server/tv/TvTimerClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, p1, v2}, Lcom/android/server/tv/TvTimerClient;-><init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->timerBinder:Lcom/android/server/tv/TvTimerClient;

    new-instance v1, Lcom/android/server/tv/TvPictureClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, p1, v2, p2}, Lcom/android/server/tv/TvPictureClient;-><init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->pictureBinder:Lcom/android/server/tv/TvPictureClient;

    new-instance v1, Lcom/android/server/tv/TvAudioClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, p1, v2, p2}, Lcom/android/server/tv/TvAudioClient;-><init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->audioBinder:Lcom/android/server/tv/TvAudioClient;

    new-instance v1, Lcom/android/server/tv/TvS3DClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->commonBinder:Lcom/android/server/tv/TvCommonClient;

    iget-object v3, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, v2, p1, v3, p2}, Lcom/android/server/tv/TvS3DClient;-><init>(Lcom/android/server/tv/TvCommonClient;Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->s3DBinder:Lcom/android/server/tv/TvS3DClient;

    new-instance v1, Lcom/android/server/tv/TvChannelClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, p1, v2, p2}, Lcom/android/server/tv/TvChannelClient;-><init>(Landroid/content/Context;Lcom/android/server/tv/TvHanlder;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->channelBinder:Lcom/android/server/tv/TvChannelClient;

    new-instance v1, Lcom/android/server/tv/TvPipPopClient;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-direct {v1, v2, p1}, Lcom/android/server/tv/TvPipPopClient;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->iTvPipPop:Lcom/mstar/android/tv/ITvPipPop;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method


# virtual methods
.method public getTvAtscChannel()Lcom/mstar/android/tv/ITvAtscChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvAtscChannel:Lcom/android/server/tv/TvAtscChannelClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvAtscChannelClient;

    invoke-direct {v0}, Lcom/android/server/tv/TvAtscChannelClient;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvAtscChannel:Lcom/android/server/tv/TvAtscChannelClient;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvAtscChannel:Lcom/android/server/tv/TvAtscChannelClient;

    return-object v0
.end method

.method public getTvAudio()Lcom/mstar/android/tv/ITvAudio;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->audioBinder:Lcom/android/server/tv/TvAudioClient;

    return-object v0
.end method

.method public getTvCc()Lcom/mstar/android/tv/ITvCc;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvCc:Lcom/android/server/tv/TvCcClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvCcClient;

    invoke-direct {v0}, Lcom/android/server/tv/TvCcClient;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvCc:Lcom/android/server/tv/TvCcClient;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvCc:Lcom/android/server/tv/TvCcClient;

    return-object v0
.end method

.method public getTvCec()Lcom/mstar/android/tv/ITvCec;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvCec:Lcom/android/server/tv/TvCecClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvCecClient;

    invoke-direct {v0}, Lcom/android/server/tv/TvCecClient;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvCec:Lcom/android/server/tv/TvCecClient;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvCec:Lcom/android/server/tv/TvCecClient;

    return-object v0
.end method

.method public getTvChannel()Lcom/mstar/android/tv/ITvChannel;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->channelBinder:Lcom/android/server/tv/TvChannelClient;

    return-object v0
.end method

.method public getTvCommon()Lcom/mstar/android/tv/ITvCommon;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->commonBinder:Lcom/android/server/tv/TvCommonClient;

    return-object v0
.end method

.method public getTvEpg()Lcom/mstar/android/tv/ITvEpg;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvEpg:Lcom/android/server/tv/TvEpgClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvEpgClient;

    invoke-direct {v0}, Lcom/android/server/tv/TvEpgClient;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvEpg:Lcom/android/server/tv/TvEpgClient;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvEpg:Lcom/android/server/tv/TvEpgClient;

    return-object v0
.end method

.method public getTvFactory()Lcom/mstar/android/tv/ITvFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvFactory:Lcom/android/server/tv/TvFactoryClient;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvFactoryClient;

    iget-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/android/server/tv/TvFactoryClient;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvFactory:Lcom/android/server/tv/TvFactoryClient;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvFactory:Lcom/android/server/tv/TvFactoryClient;

    return-object v0
.end method

.method public getTvPicture()Lcom/mstar/android/tv/ITvPicture;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->pictureBinder:Lcom/android/server/tv/TvPictureClient;

    return-object v0
.end method

.method public getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvPipPop:Lcom/mstar/android/tv/ITvPipPop;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvPipPopClient;

    iget-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    iget-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mContext:Landroid/content/Context;

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/TvPipPopClient;-><init>(Landroid/os/Handler;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvPipPop:Lcom/mstar/android/tv/ITvPipPop;

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->iTvPipPop:Lcom/mstar/android/tv/ITvPipPop;

    return-object v0
.end method

.method public getTvS3D()Lcom/mstar/android/tv/ITvS3D;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->s3DBinder:Lcom/android/server/tv/TvS3DClient;

    return-object v0
.end method

.method public getTvTimer()Lcom/mstar/android/tv/ITvTimer;
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvServiceImpl;->timerBinder:Lcom/android/server/tv/TvTimerClient;

    return-object v0
.end method

.method public resume()V
    .locals 0
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    return-void
.end method

.method public shutdown()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    invoke-virtual {v1}, Lcom/android/server/tv/TvHanlder;->shutdown()V

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mHandler:Lcom/android/server/tv/TvHanlder;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->channelBinder:Lcom/android/server/tv/TvChannelClient;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->s3DBinder:Lcom/android/server/tv/TvS3DClient;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->audioBinder:Lcom/android/server/tv/TvAudioClient;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->pictureBinder:Lcom/android/server/tv/TvPictureClient;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->timerBinder:Lcom/android/server/tv/TvTimerClient;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->commonBinder:Lcom/android/server/tv/TvCommonClient;

    iput-object v2, p0, Lcom/android/server/tv/TvServiceImpl;->mContext:Landroid/content/Context;

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->finalizeAllManager()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0
.end method
