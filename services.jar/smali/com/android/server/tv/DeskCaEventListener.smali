.class public Lcom/android/server/tv/DeskCaEventListener;
.super Ljava/lang/Object;
.source "DeskCaEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/dtv/common/CaManager$OnCaEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/DeskCaEventListener$CA_EVENT;
    }
.end annotation


# static fields
.field private static caEventListener:Lcom/android/server/tv/DeskCaEventListener;


# instance fields
.field private iCaEventClients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/ICaEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskCaEventListener;->caEventListener:Lcom/android/server/tv/DeskCaEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskCaEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskCaEventListener;->caEventListener:Lcom/android/server/tv/DeskCaEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskCaEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskCaEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskCaEventListener;->caEventListener:Lcom/android/server/tv/DeskCaEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskCaEventListener;->caEventListener:Lcom/android/server/tv/DeskCaEventListener;

    return-object v0
.end method


# virtual methods
.method public onActionRequest(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onActionRequest(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onDetitleReceived(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onDetitleReceived(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onEmailNotifyIcon(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onEmailNotifyIcon(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onEntitleChanged(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onEntitleChanged(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onHideIPPVDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onHideIPPVDlg(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onHideOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onHideOSDMessage(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaLockService;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/mstar/android/tvapi/dtv/vo/CaLockService;

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4, p5}, Lcom/mstar/android/tv/ICaEventClient;->onLockService(IIILcom/mstar/android/tvapi/dtv/vo/CaLockService;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOtaState(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onOtaState(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onRequestFeeding(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onRequestFeeding(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onShowBuyMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 6
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v5

    :cond_1
    const-string v3, "DeskCaEventListener"

    const-string v4, "//////////////////////////////////EV_CA_SHOW_BUY_MESSAGE/////////////////////////////////////////////////"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onShowBuyMessage(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onShowFingerMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onShowFingerMessage(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onShowOSDMessage(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILjava/lang/String;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4, p5}, Lcom/mstar/android/tv/ICaEventClient;->onShowOSDMessage(IIILjava/lang/String;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onShowProgressStrip(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onShowProgressStrip(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onStartIppvBuyDlg(Lcom/mstar/android/tvapi/dtv/common/CaManager;IIILcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Lcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4, p5}, Lcom/mstar/android/tv/ICaEventClient;->onStartIppvBuyDlg(IIILcom/mstar/android/tvapi/dtv/vo/CaStartIPPVBuyDlgInfo;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onUNLockService(Lcom/mstar/android/tvapi/dtv/common/CaManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/dtv/common/CaManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/mstar/android/tv/ICaEventClient;

    invoke-interface {v2, p2, p3, p4}, Lcom/mstar/android/tv/ICaEventClient;->onUNLockService(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/ICaEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/ICaEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskCaEventListener;->iCaEventClients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
