.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_AUDYSSEY_DYNAMIC_VOLUME_MODE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

.field public static final enum AUDYSSEY_DYNAMIC_VOLUME_NUM:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

.field public static final enum AUDYSSEY_DYNAMIC_VOLUME_OFF:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

.field public static final enum AUDYSSEY_DYNAMIC_VOLUME_ON:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    const-string v1, "AUDYSSEY_DYNAMIC_VOLUME_OFF"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->AUDYSSEY_DYNAMIC_VOLUME_OFF:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    const-string v1, "AUDYSSEY_DYNAMIC_VOLUME_ON"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->AUDYSSEY_DYNAMIC_VOLUME_ON:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    const-string v1, "AUDYSSEY_DYNAMIC_VOLUME_NUM"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->AUDYSSEY_DYNAMIC_VOLUME_NUM:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    const/4 v0, 0x3

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->AUDYSSEY_DYNAMIC_VOLUME_OFF:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->AUDYSSEY_DYNAMIC_VOLUME_ON:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->AUDYSSEY_DYNAMIC_VOLUME_NUM:Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    aput-object v1, v0, v4

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_AUDYSSEY_DYNAMIC_VOLUME_MODE;

    return-object v0
.end method
