.class public Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;
.super Ljava/lang/Object;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "T_MS_COLOR_TEMPEX_DATA"
.end annotation


# instance fields
.field public bluegain:I

.field public blueoffset:I

.field public greengain:I

.field public greenoffset:I

.field public redgain:I

.field public redoffset:I


# direct methods
.method public constructor <init>(IIIIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redgain:I

    iput p2, p0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greengain:I

    iput p3, p0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->bluegain:I

    iput p4, p0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->redoffset:I

    iput p5, p0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->greenoffset:I

    iput p6, p0, Lcom/android/server/tv/DataBaseDesk$T_MS_COLOR_TEMPEX_DATA;->blueoffset:I

    return-void
.end method
