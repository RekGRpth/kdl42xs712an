.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_3D_INPUT_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_CHECK_BORAD:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_FIELD_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_FRAME_PACKING_OPT:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_L_DEPTH:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_L_DEPTH_GRAPHICS_GRAPHICS_DEPTH:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_MODE_USER:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_NORMAL_2D:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_NORMAL_2D_HW:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_NORMAL_2D_INTERLACE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_NORMAL_2D_INTERLACE_PTP:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_SIDE_BY_SIDE_FULL:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_SIDE_BY_SIDE_HALF:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_SIDE_BY_SIDE_HALF_INTERLACE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_TOP_BOTTOM_OPT:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

.field public static final enum E_3D_INPUT_TYPE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_FRAME_PACKING"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_FIELD_ALTERNATIVE"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FIELD_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_LINE_ALTERNATIVE"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_SIDE_BY_SIDE_FULL"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_SIDE_BY_SIDE_FULL:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_L_DEPTH"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_L_DEPTH:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_L_DEPTH_GRAPHICS_GRAPHICS_DEPTH"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_L_DEPTH_GRAPHICS_GRAPHICS_DEPTH:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_TOP_BOTTOM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_SIDE_BY_SIDE_HALF"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_SIDE_BY_SIDE_HALF:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_CHECK_BORAD"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_CHECK_BORAD:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_MODE_USER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_MODE_USER:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_FRAME_ALTERNATIVE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_SIDE_BY_SIDE_HALF_INTERLACE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_SIDE_BY_SIDE_HALF_INTERLACE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_FRAME_PACKING_OPT"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FRAME_PACKING_OPT:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_TOP_BOTTOM_OPT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_TOP_BOTTOM_OPT:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_NORMAL_2D"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_NORMAL_2D_INTERLACE"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D_INTERLACE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_NORMAL_2D_INTERLACE_PTP"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D_INTERLACE_PTP:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_NORMAL_2D_HW"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D_HW:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const-string v1, "E_3D_INPUT_TYPE_NUM"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_TYPE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    const/16 v0, 0x13

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FIELD_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_SIDE_BY_SIDE_FULL:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_L_DEPTH:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_L_DEPTH_GRAPHICS_GRAPHICS_DEPTH:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_SIDE_BY_SIDE_HALF:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_CHECK_BORAD:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_MODE_USER:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_SIDE_BY_SIDE_HALF_INTERLACE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_FRAME_PACKING_OPT:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_TOP_BOTTOM_OPT:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D_INTERLACE:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D_INTERLACE_PTP:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_NORMAL_2D_HW:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->E_3D_INPUT_TYPE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_3D_INPUT_TYPE;

    return-object v0
.end method
