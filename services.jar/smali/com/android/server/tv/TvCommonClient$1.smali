.class Lcom/android/server/tv/TvCommonClient$1;
.super Ljava/lang/Object;
.source "TvCommonClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/tv/TvCommonClient;->recoverySystem(Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvCommonClient;

.field final synthetic val$file:Ljava/io/File;


# direct methods
.method constructor <init>(Lcom/android/server/tv/TvCommonClient;Ljava/io/File;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tv/TvCommonClient$1;->this$0:Lcom/android/server/tv/TvCommonClient;

    iput-object p2, p0, Lcom/android/server/tv/TvCommonClient$1;->val$file:Ljava/io/File;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    new-instance v1, Lcom/android/server/tv/TvCommonClient$1$1;

    invoke-direct {v1, p0}, Lcom/android/server/tv/TvCommonClient$1$1;-><init>(Lcom/android/server/tv/TvCommonClient$1;)V

    :try_start_0
    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient$1;->val$file:Ljava/io/File;

    const/4 v3, 0x0

    invoke-static {v2, v1, v3}, Landroid/os/RecoverySystem;->verifyPackage(Ljava/io/File;Landroid/os/RecoverySystem$ProgressListener;Ljava/io/File;)V

    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient$1;->this$0:Lcom/android/server/tv/TvCommonClient;

    # getter for: Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvCommonClient;->access$000(Lcom/android/server/tv/TvCommonClient;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/os/RecoverySystem;->rebootWipeUserData(Landroid/content/Context;)V

    iget-object v2, p0, Lcom/android/server/tv/TvCommonClient$1;->this$0:Lcom/android/server/tv/TvCommonClient;

    # getter for: Lcom/android/server/tv/TvCommonClient;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvCommonClient;->access$000(Lcom/android/server/tv/TvCommonClient;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/android/server/tv/TvCommonClient$1;->val$file:Ljava/io/File;

    invoke-static {v2, v3}, Landroid/os/RecoverySystem;->installPackage(Landroid/content/Context;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/GeneralSecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/security/GeneralSecurityException;->printStackTrace()V

    goto :goto_0
.end method
