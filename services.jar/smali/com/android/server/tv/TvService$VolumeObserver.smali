.class Lcom/android/server/tv/TvService$VolumeObserver;
.super Landroid/database/ContentObserver;
.source "TvService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "VolumeObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvService;


# direct methods
.method constructor <init>(Lcom/android/server/tv/TvService;)V
    .locals 3

    iput-object p1, p0, Lcom/android/server/tv/TvService$VolumeObserver;->this$0:Lcom/android/server/tv/TvService;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {p1}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "volume_master"

    invoke-static {v1}, Landroid/provider/Settings$System;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2, p0}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 8
    .param p1    # Z

    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    monitor-enter p0

    :try_start_0
    iget-object v4, p0, Lcom/android/server/tv/TvService$VolumeObserver;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "volume_master"

    const/high16 v6, -0x40800000    # -1.0f

    invoke-static {v4, v5, v6}, Landroid/provider/Settings$System;->getFloat(Landroid/content/ContentResolver;Ljava/lang/String;F)F

    move-result v0

    const/4 v4, 0x0

    cmpl-float v4, v0, v4

    if-ltz v4, :cond_1

    const/high16 v4, 0x42c80000    # 100.0f

    mul-float/2addr v4, v0

    float-to-int v3, v4

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "Volume"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tv/TvPipPopManager;->getPipMode()Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    move-result-object v4

    sget-object v5, Lcom/mstar/android/tvapi/common/vo/EnumPipModes;->E_PIP_MODE_OFF:Lcom/mstar/android/tvapi/common/vo/EnumPipModes;

    if-ne v4, v5, :cond_0

    const-string v4, "HPVolume"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_0
    iget-object v4, p0, Lcom/android/server/tv/TvService$VolumeObserver;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v4}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual {v4, v5, v2, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    if-eqz v4, :cond_1

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/mstar/android/tvapi/common/TvManager;->getDatabaseManager()Lcom/mstar/android/tvapi/common/DatabaseManager;

    move-result-object v4

    const/16 v5, 0x17

    invoke-virtual {v4, v5}, Lcom/mstar/android/tvapi/common/DatabaseManager;->setDatabaseDirtyByApplication(S)V
    :try_end_1
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    :goto_0
    :try_start_2
    monitor-exit p0

    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4
.end method
