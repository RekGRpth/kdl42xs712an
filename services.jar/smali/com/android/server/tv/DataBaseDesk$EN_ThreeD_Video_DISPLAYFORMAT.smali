.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_ThreeD_Video_DISPLAYFORMAT"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

.field public static final enum DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_NONE"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_AUTO"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const-string v1, "DB_ThreeD_Video_DISPLAYFORMAT_COUNT"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_NONE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_SIDE_BY_SIDE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_TOP_BOTTOM:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_PACKING:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_LINE_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_2DTO3D:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_AUTO:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_CHECK_BOARD:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_PIXEL_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_FRAME_ALTERNATIVE:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->DB_ThreeD_Video_DISPLAYFORMAT_COUNT:Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_ThreeD_Video_DISPLAYFORMAT;

    return-object v0
.end method
