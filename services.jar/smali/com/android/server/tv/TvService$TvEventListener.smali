.class public Lcom/android/server/tv/TvService$TvEventListener;
.super Ljava/lang/Object;
.source "TvService.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "TvEventListener"
.end annotation


# static fields
.field public static final ENABLE_3D_FORMAT:I = 0x767

.field public static final UNITY_EVENT_3D_FMT_BOTTOM_LA:I = 0xc

.field public static final UNITY_EVENT_3D_FMT_LR:I = 0x9

.field public static final UNITY_EVENT_3D_FMT_NONE:I = 0x8

.field public static final UNITY_EVENT_3D_FMT_TOP_LA:I = 0xb

.field public static final UNITY_EVENT_3D_FMT_UD:I = 0xa

.field public static final UNITY_EVENT_DISABLE_DUALVIEW:I = 0x4

.field public static final UNITY_EVENT_DISABLE_PIP:I = 0x6

.field public static final UNITY_EVENT_DISABLE_POP:I = 0x5

.field public static final UNITY_EVENT_MM_3DDISABLE:I = 0x3

.field public static final UNITY_EVENT_MM_3DENABLE:I = 0x2

.field public static final UNITY_EVENT_REBOOT:I = 0x7

.field public static final UNITY_EVENT_TIMER_OFF_WARNING:I = 0x1

.field public static final UNITY_EVENT_VIDEO_FREEZE_REMOVE:I


# instance fields
.field refreshView:Landroid/view/View;

.field refreshViewparams:Landroid/view/WindowManager$LayoutParams;

.field final synthetic this$0:Lcom/android/server/tv/TvService;

.field windowManager:Landroid/view/WindowManager;


# direct methods
.method public constructor <init>(Lcom/android/server/tv/TvService;)V
    .locals 3

    const/4 v2, 0x0

    iput-object p1, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iget-object v0, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v0}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/android/server/tv/TvService$TvEventListener;->windowManager:Landroid/view/WindowManager;

    iput-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    iput-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    return-void
.end method


# virtual methods
.method public on4k2kHDMIDisableDualView(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePip(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisablePop(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public on4k2kHDMIDisableTravelingMode(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onAtscPopupDialog(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onDeadthEvent(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onDtvReadyPopupDialog(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onScartMuteOsdMode(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onScreenSaverMode(III)Z
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onSignalLock(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onSignalUnlock(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onUnityEvent(III)Z
    .locals 6
    .param p1    # I
    .param p2    # I
    .param p3    # I

    const/4 v4, -0x1

    const/4 v5, 0x0

    const-string v2, "aaron"

    const-string v3, "TVService onUnityEvent in"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    sparse-switch p2, :sswitch_data_0

    :goto_0
    :sswitch_0
    const/4 v2, 0x1

    return v2

    :sswitch_1
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.service.UNFREEZE.OnlyRemoveImg"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_2
    const-string v2, "TvService"

    const-string v3, "sendBroadcast >> konka.action.TIMING_OFF_WARNING"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "konka.action.TIMING_OFF_WARNING"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    const-string v2, "com.konka.tv.hotkey.TIMER_OFF_WARNING"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_3
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.MM_3DENABLE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_4
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.tv.hotkey.MM_3DDISABLE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :sswitch_5
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v2, v5}, Lcom/android/server/tv/TvService;->setMySurfaceViewMode(I)V

    goto :goto_0

    :sswitch_6
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    const/4 v3, 0x5

    invoke-virtual {v2, v3}, Lcom/android/server/tv/TvService;->setMySurfaceViewMode(I)V

    goto :goto_0

    :sswitch_7
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/android/server/tv/TvService;->setMySurfaceViewMode(I)V

    goto :goto_0

    :sswitch_8
    const-string v2, "TvService"

    const-string v3, "sendBroadcast >> com.konka.hotkey.disable3dDualView"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disable3dDualView"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :try_start_0
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v2}, Lcom/android/server/tv/TvService;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/ITvPipPop;->setDualViewOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_9
    const-string v2, "TvService"

    const-string v3, "sendBroadcast >> com.konka.hotkey.disablePop"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePop"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :try_start_1
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v2}, Lcom/android/server/tv/TvService;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/ITvPipPop;->setPopOnFlag(Z)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_a
    const-string v2, "TvService"

    const-string v3, "sendBroadcast >> com.konka.hotkey.disablePip"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.konka.hotkey.disablePip"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :try_start_2
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v2}, Lcom/android/server/tv/TvService;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/mstar/android/tv/ITvPipPop;->setPipOnFlag(Z)Z

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    invoke-virtual {v2}, Lcom/android/server/tv/TvService;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v2

    invoke-interface {v2}, Lcom/mstar/android/tv/ITvPipPop;->disablePip()Z
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto/16 :goto_0

    :catch_2
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto/16 :goto_0

    :sswitch_b
    const-string v2, "TvService"

    const-string v3, "sendBroadcast >> reboot"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.REBOOT"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_c
    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    if-nez v2, :cond_0

    const-string v2, "sam"

    const-string v3, "refreshView add ===="

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v2, Landroid/view/View;

    iget-object v3, p0, Lcom/android/server/tv/TvService$TvEventListener;->this$0:Lcom/android/server/tv/TvService;

    # getter for: Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;
    invoke-static {v3}, Lcom/android/server/tv/TvService;->access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    const v3, 0x106000c    # android.R.color.black

    invoke-virtual {v2, v3}, Landroid/view/View;->setBackgroundResource(I)V

    new-instance v2, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v2}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    iput-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7d6

    iput v3, v2, Landroid/view/WindowManager$LayoutParams;->type:I

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->width:I

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    iput v4, v2, Landroid/view/WindowManager$LayoutParams;->height:I

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->windowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    :goto_1
    sget-object v2, Lcom/android/server/tv/TvService$4;->$SwitchMap$com$mstar$android$tvapi$common$vo$Enum3dType:[I

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->values()[Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v3

    aget-object v3, v3, p3

    invoke-virtual {v3}, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    const-string v2, "aaron"

    const-string v3, "default mode in\n"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "mstar.desk-display-mode"

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    :goto_2
    const-string v2, "sam"

    const-string v3, "refreshView removeview  ===="

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->windowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    invoke-interface {v2, v3}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    goto/16 :goto_0

    :cond_0
    const-string v2, "sam"

    const-string v3, "refreshView invalidate ===="

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/android/server/tv/TvService$TvEventListener;->windowManager:Landroid/view/WindowManager;

    iget-object v3, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshView:Landroid/view/View;

    iget-object v4, p0, Lcom/android/server/tv/TvService$TvEventListener;->refreshViewparams:Landroid/view/WindowManager$LayoutParams;

    invoke-interface {v2, v3, v4}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_1

    :pswitch_0
    const-string v2, "aaron"

    const-string v3, "3d mode in\n"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v2, "mstar.desk-display-mode"

    invoke-static {p3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/os/SystemProperties;->set(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_8
        0x5 -> :sswitch_9
        0x6 -> :sswitch_a
        0x7 -> :sswitch_b
        0x8 -> :sswitch_5
        0x9 -> :sswitch_0
        0xa -> :sswitch_0
        0xb -> :sswitch_6
        0xc -> :sswitch_7
        0x767 -> :sswitch_c
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
