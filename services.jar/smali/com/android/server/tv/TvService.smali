.class public Lcom/android/server/tv/TvService;
.super Lcom/mstar/android/tv/ITvService$Stub;
.source "TvService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/TvService$4;,
        Lcom/android/server/tv/TvService$VolumeObserver;,
        Lcom/android/server/tv/TvService$TvStartOrEventReceiver;,
        Lcom/android/server/tv/TvService$TvMsgHandler;,
        Lcom/android/server/tv/TvService$MySurfaceView;,
        Lcom/android/server/tv/TvService$TvEventListener;
    }
.end annotation


# static fields
.field static final DEBUG:Z = true

.field public static final EarPhone:I = 0x1e

.field public static final MUTE:I = 0x0

.field private static final SLEEP_TIME_MS:I = 0x12c

.field static final TAG:Ljava/lang/String; = "TvService"

.field protected static isHotkeyEnable:Z

.field private static mySurfaceViewMode:I

.field static sIsTvStart:Z


# instance fields
.field db:Lcom/android/server/tv/DataBaseDeskImpl;

.field private freezeImgFlag:Z

.field private mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

.field private mContext:Landroid/content/Context;

.field private mHandlerThread:Landroid/os/HandlerThread;

.field private mHotKeyReceiver:Landroid/content/BroadcastReceiver;

.field private mShutdownReceiver:Landroid/content/BroadcastReceiver;

.field private mTvHandlerThread:Landroid/os/HandlerThread;

.field private mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

.field private mTvstartedReceiver:Lcom/android/server/tv/TvService$TvStartOrEventReceiver;

.field private mViewHandler:Lcom/android/server/tv/TvHanlder;

.field private mVolumeObserver:Lcom/android/server/tv/TvService$VolumeObserver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/4 v0, 0x1

    sput-boolean v0, Lcom/android/server/tv/TvService;->isHotkeyEnable:Z

    sput-boolean v1, Lcom/android/server/tv/TvService;->sIsTvStart:Z

    sput v1, Lcom/android/server/tv/TvService;->mySurfaceViewMode:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    const/4 v0, 0x0

    invoke-direct {p0}, Lcom/mstar/android/tv/ITvService$Stub;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mTvHandlerThread:Landroid/os/HandlerThread;

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mVolumeObserver:Lcom/android/server/tv/TvService$VolumeObserver;

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/android/server/tv/TvService;->freezeImgFlag:Z

    new-instance v0, Lcom/android/server/tv/TvService$2;

    invoke-direct {v0, p0}, Lcom/android/server/tv/TvService$2;-><init>(Lcom/android/server/tv/TvService;)V

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mHotKeyReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/android/server/tv/TvService$3;

    invoke-direct {v0, p0}, Lcom/android/server/tv/TvService$3;-><init>(Lcom/android/server/tv/TvService;)V

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "tv_view_handler"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mHandlerThread:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/android/server/tv/TvHanlder;

    iget-object v1, p0, Lcom/android/server/tv/TvService;->mHandlerThread:Landroid/os/HandlerThread;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPlayerManager()Lcom/mstar/android/tvapi/common/TvPlayer;

    move-result-object v2

    invoke-direct {v0, v1, p1, v2, p0}, Lcom/android/server/tv/TvHanlder;-><init>(Landroid/os/HandlerThread;Landroid/content/Context;Lcom/mstar/android/tvapi/common/TvPlayer;Lcom/mstar/android/tv/ITvService;)V

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;

    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/android/server/tv/TvService;)Landroid/content/Context;
    .locals 1
    .param p0    # Lcom/android/server/tv/TvService;

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lcom/android/server/tv/TvService;)V
    .locals 0
    .param p0    # Lcom/android/server/tv/TvService;

    invoke-direct {p0}, Lcom/android/server/tv/TvService;->initByHotelMenu()V

    return-void
.end method

.method static synthetic access$202(I)I
    .locals 0
    .param p0    # I

    sput p0, Lcom/android/server/tv/TvService;->mySurfaceViewMode:I

    return p0
.end method

.method static synthetic access$400(Lcom/android/server/tv/TvService;)Lcom/android/server/tv/TvHanlder;
    .locals 1
    .param p0    # Lcom/android/server/tv/TvService;

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mViewHandler:Lcom/android/server/tv/TvHanlder;

    return-object v0
.end method

.method static synthetic access$500(Lcom/android/server/tv/TvService;)Z
    .locals 1
    .param p0    # Lcom/android/server/tv/TvService;

    iget-boolean v0, p0, Lcom/android/server/tv/TvService;->freezeImgFlag:Z

    return v0
.end method

.method static synthetic access$502(Lcom/android/server/tv/TvService;Z)Z
    .locals 0
    .param p0    # Lcom/android/server/tv/TvService;
    .param p1    # Z

    iput-boolean p1, p0, Lcom/android/server/tv/TvService;->freezeImgFlag:Z

    return p1
.end method

.method public static get4K2KMode()Z
    .locals 2

    const/4 v0, 0x1

    const-string v1, "isIn4K2KMode"

    invoke-static {v1}, Lcom/android/server/tv/TvService;->getValueByTvosCommon(Ljava/lang/String;)S

    move-result v1

    if-ne v0, v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static getValueByTvosCommon(Ljava/lang/String;)S
    .locals 5
    .param p0    # Ljava/lang/String;

    const/4 v2, -0x1

    if-eqz p0, :cond_0

    const-string v3, ""

    if-ne p0, v3, :cond_1

    :cond_0
    :goto_0
    return v2

    :cond_1
    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    if-eqz v3, :cond_2

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v3

    invoke-virtual {v3, p0}, Lcom/mstar/android/tvapi/common/TvManager;->setTvosCommonCommand(Ljava/lang/String;)[S

    move-result-object v1

    if-eqz v1, :cond_0

    array-length v3, v1

    if-eqz v3, :cond_0

    const/4 v3, 0x0

    aget-short v2, v1, v3

    goto :goto_0

    :cond_2
    const-string v3, "TvService"

    const-string v4, "TvManger.getInstance() == null"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private initByHotelMenu()V
    .locals 15

    const/4 v13, 0x1

    const/4 v2, 0x0

    const/4 v14, 0x0

    const-string v0, "TvService"

    const-string v1, "initByHotelMenu"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://com.konka.hotelmenu/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    if-eqz v11, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v11}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "OnOff"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    if-ne v13, v0, :cond_0

    :goto_0
    const-string v0, "TvService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "hotelEnable = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    if-nez v13, :cond_1

    :goto_1
    return-void

    :cond_0
    move v13, v14

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/media/AudioManager;

    invoke-virtual {p0}, Lcom/android/server/tv/TvService;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v0

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvCommon;->getCurrentInputSource()I

    move-result v8

    invoke-virtual {v6}, Landroid/media/AudioManager;->getMasterVolume()I

    move-result v9

    invoke-virtual {p0}, Lcom/android/server/tv/TvService;->getTvChannel()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v0

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvChannel;->getCurrentChannelNumber()I

    move-result v7

    const-string v0, "TvService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bootSource = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bootVolume = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bootChannel = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, v7

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "BootSource"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    const-string v0, "BootVolume"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const-string v0, "BootChannel"

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/TvManager;->getChannelManager()Lcom/mstar/android/tvapi/common/ChannelManager;

    move-result-object v10

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v0

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v1

    aget-object v1, v1, v8

    invoke-virtual {v0, v1}, Lcom/mstar/android/tvapi/common/TvManager;->setInputSource(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    const-string v0, "TvService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "currServiceType = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->values()[Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    move-result-object v2

    new-instance v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;

    invoke-direct {v3}, Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;-><init>()V

    sget-object v4, Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;->E_INFO_CURRENT:Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;

    invoke-virtual {v10, v3, v4}, Lcom/mstar/android/tvapi/common/ChannelManager;->getProgramInfo(Lcom/mstar/android/tvapi/common/vo/ProgramInfoQueryCriteria;Lcom/mstar/android/tvapi/common/vo/EnumProgramInfoType;)Lcom/mstar/android/tvapi/common/vo/ProgramInfo;

    move-result-object v3

    iget-short v3, v3, Lcom/mstar/android/tvapi/common/vo/ProgramInfo;->serviceType:S

    aget-object v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_ATV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    if-ne v0, v8, :cond_3

    add-int/lit8 v0, v7, -0x1

    sget-object v1, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_ATV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v1

    int-to-short v1, v1

    const/4 v2, 0x0

    invoke-virtual {v10, v0, v1, v2}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V

    :cond_2
    :goto_2
    const/4 v0, 0x0

    invoke-virtual {v6, v9, v0}, Landroid/media/AudioManager;->setMasterVolume(II)V

    const-string v0, "TvService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "bootSource = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v2

    aget-object v2, v2, v8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bootChannel = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "bootVolume = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :goto_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    :cond_3
    :try_start_1
    sget-object v0, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->E_INPUT_SOURCE_DTV:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->ordinal()I

    move-result v0

    if-ne v0, v8, :cond_2

    sget-object v0, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->E_SERVICETYPE_DTV:Lcom/mstar/android/tvapi/common/vo/EnumServiceType;

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/vo/EnumServiceType;->ordinal()I

    move-result v0

    int-to-short v0, v0

    const/4 v1, 0x0

    invoke-virtual {v10, v7, v0, v1}, Lcom/mstar/android/tvapi/common/ChannelManager;->selectProgram(ISI)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    :catch_0
    move-exception v12

    :try_start_2
    invoke-virtual {v12}, Landroid/os/RemoteException;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_3

    :catchall_0
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    :catch_1
    move-exception v12

    :try_start_3
    invoke-virtual {v12}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_3

    :cond_4
    const-string v0, "TvService"

    const-string v1, "initByHotelMenu wrong cursor = null"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method private registerTvStartReceiver()V
    .locals 5

    new-instance v3, Lcom/android/server/tv/TvService$TvStartOrEventReceiver;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/android/server/tv/TvService$TvStartOrEventReceiver;-><init>(Lcom/android/server/tv/TvService;Lcom/android/server/tv/TvService$1;)V

    iput-object v3, p0, Lcom/android/server/tv/TvService;->mTvstartedReceiver:Lcom/android/server/tv/TvService$TvStartOrEventReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.mstar.android.tv.ui.tvstart"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.mstar.android.tv.ui.tvend"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.POWER_OFF"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.VGA_NOSIGNALPOWER_OFF"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.KEYCODE_TV_INPUT"

    invoke-virtual {v1, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/tv/TvService;->mTvstartedReceiver:Lcom/android/server/tv/TvService$TvStartOrEventReceiver;

    invoke-virtual {v3, v4, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v2, Landroid/content/IntentFilter;

    invoke-direct {v2}, Landroid/content/IntentFilter;-><init>()V

    const-string v3, "com.mstar.android.intent.action.PICTURE_MODE_BUTTON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.mstar.android.intent.action.SOUND_MODE_BUTTON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.mstar.android.intent.action.ASPECT_RATIO_BUTTON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.mstar.android.intent.action.SLEEP_BUTTON"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.service.MUTE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.service.UNMUTE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.service.FREEZE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.service.UNFREEZE"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.tv.hotkey.service.UNFREEZE.OnlyRemoveImg"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "com.konka.MOVIE_PLAY_STATUS"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "konka.action.SET_VOLUME_UP"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v3, "konka.action.SET_VOLUME_DOWN"

    invoke-virtual {v2, v3}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    :try_start_1
    iget-object v3, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/tv/TvService;->mHotKeyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v3, v4, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method

.method private restoreTvVolume()V
    .locals 9

    const/4 v2, 0x0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "content://mstar.tv.usersetting/soundsetting"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    const/4 v8, 0x0

    if-eqz v6, :cond_0

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Volume"

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v8

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    const-string v0, "TvService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreTvVolume get from usersetting.db vol = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/media/AudioManager;

    if-eqz v7, :cond_1

    const-string v0, "TvService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "restoreTvVolume = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    invoke-virtual {v7, v8, v0}, Landroid/media/AudioManager;->setMasterVolume(II)V

    :cond_1
    return-void
.end method

.method private unRegisterTvStartReceiver()V
    .locals 4

    const/4 v3, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tv/TvService;->mTvstartedReceiver:Lcom/android/server/tv/TvService$TvStartOrEventReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iput-object v3, p0, Lcom/android/server/tv/TvService;->mTvstartedReceiver:Lcom/android/server/tv/TvService$TvStartOrEventReceiver;

    :try_start_1
    iget-object v1, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tv/TvService;->mHotKeyReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    iput-object v3, p0, Lcom/android/server/tv/TvService;->mHotKeyReceiver:Landroid/content/BroadcastReceiver;

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V

    goto :goto_1
.end method


# virtual methods
.method public getTvAtscChannel()Lcom/mstar/android/tv/ITvAtscChannel;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvAtscChannel()Lcom/mstar/android/tv/ITvAtscChannel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvAudio()Lcom/mstar/android/tv/ITvAudio;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvAudio()Lcom/mstar/android/tv/ITvAudio;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvCc()Lcom/mstar/android/tv/ITvCc;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvCc()Lcom/mstar/android/tv/ITvCc;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvCec()Lcom/mstar/android/tv/ITvCec;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvCec()Lcom/mstar/android/tv/ITvCec;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvChannel()Lcom/mstar/android/tv/ITvChannel;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvChannel()Lcom/mstar/android/tv/ITvChannel;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvCommon()Lcom/mstar/android/tv/ITvCommon;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvCommon()Lcom/mstar/android/tv/ITvCommon;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvEpg()Lcom/mstar/android/tv/ITvEpg;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvEpg()Lcom/mstar/android/tv/ITvEpg;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvFactory()Lcom/mstar/android/tv/ITvFactory;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvFactory()Lcom/mstar/android/tv/ITvFactory;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvPicture()Lcom/mstar/android/tv/ITvPicture;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvPicture()Lcom/mstar/android/tv/ITvPicture;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvPipPop()Lcom/mstar/android/tv/ITvPipPop;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvS3D()Lcom/mstar/android/tv/ITvS3D;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvS3D()Lcom/mstar/android/tv/ITvS3D;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getTvTimer()Lcom/mstar/android/tv/ITvTimer;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->getTvTimer()Lcom/mstar/android/tv/ITvTimer;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    const/16 v1, 0x101

    invoke-virtual {v0, v1}, Lcom/android/server/tv/TvService$TvMsgHandler;->sendEmptyMessage(I)Z

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized resume()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Requires %s permission"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "android.permission.SHUTDOWN"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    new-instance v0, Lcom/android/server/tv/TvServiceImpl;

    iget-object v1, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v2, p0, Lcom/android/server/tv/TvService;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/TvServiceImpl;-><init>(Landroid/content/Context;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-direct {p0}, Lcom/android/server/tv/TvService;->registerTvStartReceiver()V

    const-string v0, "TvService"

    const-string v1, "TvService resume!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void
.end method

.method public setMySurfaceViewMode(I)V
    .locals 4
    .param p1    # I

    sget v1, Lcom/android/server/tv/TvService;->mySurfaceViewMode:I

    if-ne v1, p1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    new-instance v0, Lcom/android/server/tv/TvService$MySurfaceView;

    iget-object v1, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    invoke-direct {v0, p0, v1, p1}, Lcom/android/server/tv/TvService$MySurfaceView;-><init>(Lcom/android/server/tv/TvService;Landroid/content/Context;I)V

    if-nez v0, :cond_0

    const-string v1, "TvService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "surfaceView fail. mode = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public declared-synchronized shutdown()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    const-string v1, "android.permission.SHUTDOWN"

    invoke-virtual {v0, v1}, Landroid/content/Context;->checkCallingOrSelfPermission(Ljava/lang/String;)I

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Requires %s permission"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "android.permission.SHUTDOWN"

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/android/server/tv/TvService;->unRegisterTvStartReceiver()V

    iget-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-interface {v0}, Lcom/mstar/android/tv/ITvService;->shutdown()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    const-string v0, "TvService"

    const-string v1, "TvService shutdown!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_1
    monitor-exit p0

    return-void
.end method

.method public declared-synchronized systemReady()V
    .locals 6

    monitor-enter p0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->disableTvosIr()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_0
    :goto_0
    :try_start_1
    iget-object v2, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/android/server/tv/DataBaseDeskImpl;->getDataBaseMgrInstance(Landroid/content/Context;)Lcom/android/server/tv/DataBaseDeskImpl;

    move-result-object v2

    iput-object v2, p0, Lcom/android/server/tv/TvService;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    new-instance v2, Landroid/os/HandlerThread;

    const-string v3, "tv_service_handler"

    invoke-direct {v2, v3}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/android/server/tv/TvService;->mTvHandlerThread:Landroid/os/HandlerThread;

    iget-object v2, p0, Lcom/android/server/tv/TvService;->mTvHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->start()V

    new-instance v2, Lcom/android/server/tv/TvService$TvMsgHandler;

    iget-object v3, p0, Lcom/android/server/tv/TvService;->mTvHandlerThread:Landroid/os/HandlerThread;

    invoke-virtual {v3}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/android/server/tv/TvService$TvMsgHandler;-><init>(Lcom/android/server/tv/TvService;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    new-instance v2, Lcom/android/server/tv/TvServiceImpl;

    iget-object v3, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v4, p0, Lcom/android/server/tv/TvService;->db:Lcom/android/server/tv/DataBaseDeskImpl;

    invoke-direct {v2, v3, v4}, Lcom/android/server/tv/TvServiceImpl;-><init>(Landroid/content/Context;Lcom/android/server/tv/DataBaseDeskImpl;)V

    iput-object v2, p0, Lcom/android/server/tv/TvService;->mTvServiceImpl:Lcom/mstar/android/tv/ITvService;

    invoke-direct {p0}, Lcom/android/server/tv/TvService;->registerTvStartReceiver()V

    const-string v2, "TvService"

    const-string v3, "TvService systemReady!"

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.ACTION_SHUTDOWN"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/android/server/tv/TvService;->mContext:Landroid/content/Context;

    iget-object v3, p0, Lcom/android/server/tv/TvService;->mShutdownReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    new-instance v3, Lcom/android/server/tv/TvService$TvEventListener;

    invoke-direct {v3, p0}, Lcom/android/server/tv/TvService$TvEventListener;-><init>(Lcom/android/server/tv/TvService;)V

    invoke-virtual {v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setOnTvEventListener(Lcom/mstar/android/tvapi/common/listener/OnTvEventListener;)V

    invoke-direct {p0}, Lcom/android/server/tv/TvService;->restoreTvVolume()V

    iget-object v2, p0, Lcom/android/server/tv/TvService;->mVolumeObserver:Lcom/android/server/tv/TvService$VolumeObserver;

    if-nez v2, :cond_2

    new-instance v2, Lcom/android/server/tv/TvService$VolumeObserver;

    invoke-direct {v2, p0}, Lcom/android/server/tv/TvService$VolumeObserver;-><init>(Lcom/android/server/tv/TvService;)V

    iput-object v2, p0, Lcom/android/server/tv/TvService;->mVolumeObserver:Lcom/android/server/tv/TvService$VolumeObserver;

    :cond_2
    iget-object v2, p0, Lcom/android/server/tv/TvService;->mBinderHandler:Lcom/android/server/tv/TvService$TvMsgHandler;

    new-instance v3, Lcom/android/server/tv/TvService$1;

    invoke-direct {v3, p0}, Lcom/android/server/tv/TvService$1;-><init>(Lcom/android/server/tv/TvService;)V

    const-wide/16 v4, 0x2328

    invoke-virtual {v2, v3, v4, v5}, Lcom/android/server/tv/TvService$TvMsgHandler;->postDelayed(Ljava/lang/Runnable;J)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    :try_start_2
    invoke-virtual {v0}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2
.end method
