.class Lcom/android/server/tv/TvFactoryClient$1;
.super Ljava/lang/Object;
.source "TvFactoryClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/android/server/tv/TvFactoryClient;->execAutoAdc()Z
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvFactoryClient;


# direct methods
.method constructor <init>(Lcom/android/server/tv/TvFactoryClient;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tv/TvFactoryClient$1;->this$0:Lcom/android/server/tv/TvFactoryClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    const/4 v4, 0x1

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient$1;->this$0:Lcom/android/server/tv/TvFactoryClient;

    invoke-virtual {v2, v4}, Lcom/android/server/tv/TvFactoryClient;->getHandler(I)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_1

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient$1;->this$0:Lcom/android/server/tv/TvFactoryClient;

    invoke-virtual {v2, v4}, Lcom/android/server/tv/TvFactoryClient;->getHandler(I)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x4e8f

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getFactoryManager()Lcom/mstar/android/tvapi/factory/FactoryManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/factory/FactoryManager;->autoAdc()Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    :cond_0
    :goto_0
    const-wide/16 v2, 0x3e8

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient$1;->this$0:Lcom/android/server/tv/TvFactoryClient;

    invoke-virtual {v2, v4}, Lcom/android/server/tv/TvFactoryClient;->getHandler(I)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x4e90

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    :cond_1
    :goto_2
    return-void

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_2
    iget-object v2, p0, Lcom/android/server/tv/TvFactoryClient$1;->this$0:Lcom/android/server/tv/TvFactoryClient;

    invoke-virtual {v2, v4}, Lcom/android/server/tv/TvFactoryClient;->getHandler(I)Landroid/os/Handler;

    move-result-object v2

    const/16 v3, 0x4e91

    invoke-virtual {v2, v3}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    goto :goto_2
.end method
