.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_VIDEOITEM"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_BACKLIGHT:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_BRIGHTNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_CONTRAST:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_HUE:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_SATURATION:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

.field public static final enum MS_VIDEOITEM_SHARPNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_BRIGHTNESS"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_CONTRAST"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_SATURATION"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_SHARPNESS"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_HUE"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_BACKLIGHT"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const-string v1, "MS_VIDEOITEM_NUM"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    const/4 v0, 0x7

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BRIGHTNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_CONTRAST:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SATURATION:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_SHARPNESS:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_HUE:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_BACKLIGHT:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->MS_VIDEOITEM_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_VIDEOITEM;

    return-object v0
.end method
