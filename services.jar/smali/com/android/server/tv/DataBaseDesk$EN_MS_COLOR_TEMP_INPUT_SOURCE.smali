.class public final enum Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;
.super Ljava/lang/Enum;
.source "DataBaseDesk.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDesk;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "EN_MS_COLOR_TEMP_INPUT_SOURCE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_ATV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_CVBS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_DTV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_HDMI:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_OTHERS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_SCART:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_SVIDEO:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_VGA:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

.field public static final enum E_INPUT_SOURCE_YPBPR:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_VGA"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_ATV"

    invoke-direct {v0, v1, v4}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_ATV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_CVBS"

    invoke-direct {v0, v1, v5}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_CVBS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_SVIDEO"

    invoke-direct {v0, v1, v6}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SVIDEO:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_YPBPR"

    invoke-direct {v0, v1, v7}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_YPBPR:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_SCART"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SCART:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_HDMI"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_HDMI:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_DTV"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_DTV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_OTHERS"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_OTHERS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_NUM"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    new-instance v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const-string v1, "E_INPUT_SOURCE_NONE"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    const/16 v0, 0xb

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_VGA:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_ATV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_CVBS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SVIDEO:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_YPBPR:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_SCART:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_HDMI:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_DTV:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_OTHERS:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NUM:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->E_INPUT_SOURCE_NONE:Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->$VALUES:[Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDesk$EN_MS_COLOR_TEMP_INPUT_SOURCE;

    return-object v0
.end method
