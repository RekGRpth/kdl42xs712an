.class public Lcom/android/server/tv/DeskTimerEventListener;
.super Ljava/lang/Object;
.source "DeskTimerEventListener.java"

# interfaces
.implements Lcom/mstar/android/tvapi/common/TimerManager$OnTimerEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/server/tv/DeskTimerEventListener$EVENT;
    }
.end annotation


# static fields
.field private static timerEventListener:Lcom/android/server/tv/DeskTimerEventListener;


# instance fields
.field private clients:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/mstar/android/tv/ITimerEventClient;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/android/server/tv/DeskTimerEventListener;->timerEventListener:Lcom/android/server/tv/DeskTimerEventListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    return-void
.end method

.method public static getInstance()Lcom/android/server/tv/DeskTimerEventListener;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DeskTimerEventListener;->timerEventListener:Lcom/android/server/tv/DeskTimerEventListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/android/server/tv/DeskTimerEventListener;

    invoke-direct {v0}, Lcom/android/server/tv/DeskTimerEventListener;-><init>()V

    sput-object v0, Lcom/android/server/tv/DeskTimerEventListener;->timerEventListener:Lcom/android/server/tv/DeskTimerEventListener;

    :cond_0
    sget-object v0, Lcom/android/server/tv/DeskTimerEventListener;->timerEventListener:Lcom/android/server/tv/DeskTimerEventListener;

    return-object v0
.end method


# virtual methods
.method public onDestroyCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onDestroyCountDown(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onEpgTimeUp(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onEpgTimeUp(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onEpgTimerCountDown(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onEpgTimerCountDown(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onEpgTimerRecordStart(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onEpgTimerRecordStart(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onLastMinuteWarn(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onLastMinuteWarn(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOadTimeScan(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onOadTimeScan(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onOneSecondBeat(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onOneSecondBeat(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPowerDownTime(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onPowerDownTime(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onPvrNotifyRecordStop(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onPvrNotifyRecordStop(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSignalLock(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onSignalLock(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public onSystemClkChg(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 1
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v0, 0x0

    return v0
.end method

.method public onUpdateLastMinute(Lcom/mstar/android/tvapi/common/TimerManager;III)Z
    .locals 5
    .param p1    # Lcom/mstar/android/tvapi/common/TimerManager;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    const/4 v4, 0x0

    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v4

    :cond_1
    :try_start_0
    iget-object v3, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/android/tv/ITimerEventClient;

    invoke-interface {v0, p2, p3, p4}, Lcom/mstar/android/tv/ITimerEventClient;->onUpdateLastMinute(III)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public releaseClient(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public setClient(Ljava/lang/String;Lcom/mstar/android/tv/ITimerEventClient;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .param p2    # Lcom/mstar/android/tv/ITimerEventClient;

    iget-object v0, p0, Lcom/android/server/tv/DeskTimerEventListener;->clients:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method
