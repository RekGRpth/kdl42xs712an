.class public final enum Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;
.super Ljava/lang/Enum;
.source "DataBaseDeskImpl.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/DataBaseDeskImpl;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "USER_SETTING_FIELD"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

.field public static final enum bEnableWDT:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

.field public static final enum bUartBus:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    const-string v1, "bEnableWDT"

    invoke-direct {v0, v1, v2}, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->bEnableWDT:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    new-instance v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    const-string v1, "bUartBus"

    invoke-direct {v0, v1, v3}, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->bUartBus:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->bEnableWDT:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    aput-object v1, v0, v2

    sget-object v1, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->bUartBus:Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    aput-object v1, v0, v3

    sput-object v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->$VALUES:[Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;
    .locals 1
    .param p0    # Ljava/lang/String;

    const-class v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    return-object v0
.end method

.method public static values()[Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;
    .locals 1

    sget-object v0, Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->$VALUES:[Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    invoke-virtual {v0}, [Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/android/server/tv/DataBaseDeskImpl$USER_SETTING_FIELD;

    return-object v0
.end method
