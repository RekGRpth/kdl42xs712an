.class Lcom/android/server/tv/TvPictureClient$MyThread;
.super Ljava/lang/Thread;
.source "TvPictureClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/tv/TvPictureClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "MyThread"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/android/server/tv/TvPictureClient;


# direct methods
.method constructor <init>(Lcom/android/server/tv/TvPictureClient;)V
    .locals 0

    iput-object p1, p0, Lcom/android/server/tv/TvPictureClient$MyThread;->this$0:Lcom/android/server/tv/TvPictureClient;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :goto_0
    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient$MyThread;->this$0:Lcom/android/server/tv/TvPictureClient;

    # getter for: Lcom/android/server/tv/TvPictureClient;->bforceThreadSleep:Z
    invoke-static {v1}, Lcom/android/server/tv/TvPictureClient;->access$000(Lcom/android/server/tv/TvPictureClient;)Z

    move-result v1

    if-eqz v1, :cond_0

    :try_start_0
    const-string v1, "TvApp"

    const-string v2, " Thread  forceThreadSleep"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-wide/16 v1, 0x1388

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient$MyThread;->this$0:Lcom/android/server/tv/TvPictureClient;

    # getter for: Lcom/android/server/tv/TvPictureClient;->enableDBC:Z
    invoke-static {v1}, Lcom/android/server/tv/TvPictureClient;->access$100(Lcom/android/server/tv/TvPictureClient;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient$MyThread;->this$0:Lcom/android/server/tv/TvPictureClient;

    # invokes: Lcom/android/server/tv/TvPictureClient;->dbchandler()V
    invoke-static {v1}, Lcom/android/server/tv/TvPictureClient;->access$200(Lcom/android/server/tv/TvPictureClient;)V

    :cond_1
    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient$MyThread;->this$0:Lcom/android/server/tv/TvPictureClient;

    # getter for: Lcom/android/server/tv/TvPictureClient;->enableDCC:Z
    invoke-static {v1}, Lcom/android/server/tv/TvPictureClient;->access$300(Lcom/android/server/tv/TvPictureClient;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/android/server/tv/TvPictureClient$MyThread;->this$0:Lcom/android/server/tv/TvPictureClient;

    # invokes: Lcom/android/server/tv/TvPictureClient;->dcchandler()V
    invoke-static {v1}, Lcom/android/server/tv/TvPictureClient;->access$400(Lcom/android/server/tv/TvPictureClient;)V

    :cond_2
    :try_start_1
    # getter for: Lcom/android/server/tv/TvPictureClient;->sleeptimeCnt:I
    invoke-static {}, Lcom/android/server/tv/TvPictureClient;->access$500()I

    move-result v1

    int-to-long v1, v1

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
