.class public interface abstract Lcom/android/server/tv/InnerConstants;
.super Ljava/lang/Object;
.source "InnerConstants.java"


# static fields
.field public static final CLOSE_SURFACE:I = 0x5

.field public static final FREEZE:I = 0x9

.field public static final MUTE:I = 0x7

.field public static final PIP_CREATE:I = 0x1

.field public static final PIP_DESTROY:I = 0x2

.field public static final SET_SURFACE:I = 0x6

.field public static final SHOW_SURFACE:I = 0x4

.field public static final T_3DInfo_IDX:S = 0x0s

.field public static final T_3DSetting_IDX:S = 0x1s

.field public static final T_ADCAdjust_IDX:S = 0x24s

.field public static final T_ATVDefaultPrograms_IDX:S = 0x31s

.field public static final T_BlockSysSetting_IDX:S = 0x2s

.field public static final T_CECSetting_IDX:S = 0x3s

.field public static final T_CISettineUpInfo_IDX:S = 0x13s

.field public static final T_CISetting_IDX:S = 0x4s

.field public static final T_DB_VERSION_IDX:S = 0x5s

.field public static final T_DTVDefaultPrograms_IDX:S = 0x32s

.field public static final T_DTVOverscanSetting_IDX:S = 0x30s

.field public static final T_DvbtPresetting_IDX:S = 0x6s

.field public static final T_EpgTimer_IDX:S = 0x7s

.field public static final T_FacrotyColorTempEx_IDX:S = 0x26s

.field public static final T_FacrotyColorTemp_IDX:S = 0x25s

.field public static final T_FactoryExtern_IDX:S = 0x27s

.field public static final T_Factory_DB_VERSION_IDX:S = 0x2ds

.field public static final T_FavTypeName_IDX:S = 0x8s

.field public static final T_HDMIOverscanSetting_IDX:S = 0x2es

.field public static final T_InputSource_Type_IDX:S = 0x9s

.field public static final T_IsdbSysSetting_IDX:S = 0xas

.field public static final T_IsdbUserSetting_IDX:S = 0xbs

.field public static final T_MAX_IDX:S = 0x33s

.field public static final T_MediumSetting_IDX:S = 0xcs

.field public static final T_MfcMode_IDX:S = 0xds

.field public static final T_NRMode_IDX:S = 0xes

.field public static final T_NitInfo_IDX:S = 0xfs

.field public static final T_Nit_TSInfo_IDX:S = 0x10s

.field public static final T_NonLinearAdjust_IDX:S = 0x2as

.field public static final T_NonStarndardAdjust_IDX:S = 0x28s

.field public static final T_OADInfo_IDX:S = 0x11s

.field public static final T_OADInfo_UntDescriptor_IDX:S = 0x12s

.field public static final T_OverscanAdjust_IDX:S = 0x2bs

.field public static final T_PEQAdjust_IDX:S = 0x2cs

.field public static final T_PicMode_Setting_IDX:S = 0x14s

.field public static final T_PipSetting_IDX:S = 0x15s

.field public static final T_SSCAdjust_IDX:S = 0x29s

.field public static final T_SoundMode_Setting_IDX:S = 0x16s

.field public static final T_SoundSetting_IDX:S = 0x17s

.field public static final T_SubtitleSetting_IDX:S = 0x18s

.field public static final T_SystemSetting_IDX:S = 0x19s

.field public static final T_ThreeDVideoMode_IDX:S = 0x1as

.field public static final T_ThreeDVideoRouterSetting_IDX:S = 0x23s

.field public static final T_TimeSetting_IDX:S = 0x1bs

.field public static final T_USER_COLORTEMP_EX_IDX:S = 0x1ds

.field public static final T_USER_COLORTEMP_IDX:S = 0x1cs

.field public static final T_UserLocationSetting_IDX:S = 0x1es

.field public static final T_UserMMSetting_IDX:S = 0x1fs

.field public static final T_UserOverScanMode_IDX:S = 0x20s

.field public static final T_UserPCModeSetting_IDX:S = 0x21s

.field public static final T_VideoSetting_IDX:S = 0x22s

.field public static final T_YPbPrOverscanSetting_IDX:S = 0x2fs

.field public static final UNFREEZE:I = 0xa

.field public static final UNMUTE:I = 0x8
