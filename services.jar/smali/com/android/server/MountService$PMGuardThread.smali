.class Lcom/android/server/MountService$PMGuardThread;
.super Ljava/lang/Thread;
.source "MountService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/server/MountService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "PMGuardThread"
.end annotation


# static fields
.field private static final MEDIA_IDLE_EVENT:I = 0x1

.field public static final MEDIA_MOUNT:I = 0x1

.field public static final MEDIA_MOUNT_EVENT:I = 0x3

.field private static final MEDIA_NOEVENT:I = 0x0

.field public static final MEDIA_UNMOUNT:I = 0x0

.field public static final MEDIA_UNMOUNT_EVENT:I = 0x2


# instance fields
.field private currentMediaEvent:Ljava/lang/Integer;

.field private finishUnumountApksOnSD:Ljava/lang/Boolean;

.field private pmMediaStatus:I

.field final synthetic this$0:Lcom/android/server/MountService;


# direct methods
.method constructor <init>(Lcom/android/server/MountService;)V
    .locals 2

    const/4 v1, 0x1

    iput-object p1, p0, Lcom/android/server/MountService$PMGuardThread;->this$0:Lcom/android/server/MountService;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MountService$PMGuardThread;->pmMediaStatus:I

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    return-void
.end method


# virtual methods
.method public notifyFinishUnumountApksOnSD()V
    .locals 2

    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    monitor-exit v1

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public registerMediaEvents(I)V
    .locals 4
    .param p1    # I

    const/4 v3, 0x2

    const/4 v2, 0x1

    if-eq p1, v3, :cond_0

    const/4 v0, 0x3

    if-eq p1, v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/android/server/MountService$PMGuardThread;->pmMediaStatus:I

    if-ne v0, v2, :cond_2

    if-ne p1, v3, :cond_1

    const/4 v0, 0x0

    iput v0, p0, Lcom/android/server/MountService$PMGuardThread;->pmMediaStatus:I

    const/4 v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v0, 0x0

    :try_start_1
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->this$0:Lcom/android/server/MountService;

    # getter for: Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;
    invoke-static {v0}, Lcom/android/server/MountService;->access$100(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    move-result-object v0

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    :goto_1
    monitor-exit v1

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v0

    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    throw v0

    :cond_1
    const-string v0, "MountService"

    const-string v2, "pm media status is mounted, but the next event is not a media unmount event!"

    invoke-static {v0, v2}, Landroid/util/Slog;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_2
    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method public run()V
    .locals 6

    const/4 v5, 0x1

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    monitor-enter v2

    :try_start_0
    iget v1, p0, Lcom/android/server/MountService$PMGuardThread;->pmMediaStatus:I

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v5, :cond_2

    :cond_1
    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const-wide/16 v1, 0x7d0

    :try_start_1
    invoke-static {v1, v2}, Lcom/android/server/MountService$PMGuardThread;->sleep(J)V

    :goto_2
    const/4 v0, 0x0

    iget-object v2, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    monitor-enter v2
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :try_start_2
    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->finishUnumountApksOnSD:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    if-nez v0, :cond_0

    const-wide/16 v1, 0x64

    :try_start_3
    invoke-static {v1, v2}, Lcom/android/server/MountService$PMGuardThread;->sleep(J)V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_2

    :catch_0
    move-exception v1

    goto :goto_0

    :cond_2
    :try_start_4
    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    iput v1, p0, Lcom/android/server/MountService$PMGuardThread;->pmMediaStatus:I

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->this$0:Lcom/android/server/MountService;

    # getter for: Lcom/android/server/MountService;->mPms:Lcom/android/server/pm/PackageManagerService;
    invoke-static {v1}, Lcom/android/server/MountService;->access$100(Lcom/android/server/MountService;)Lcom/android/server/pm/PackageManagerService;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4}, Lcom/android/server/pm/PackageManagerService;->updateExternalMediaStatus(ZZ)V

    goto :goto_1

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    throw v1

    :cond_3
    :try_start_5
    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    goto :goto_1

    :cond_4
    iget-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v3, 0x3

    if-ne v1, v3, :cond_1

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iput-object v1, p0, Lcom/android/server/MountService$PMGuardThread;->currentMediaEvent:Ljava/lang/Integer;
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_1

    :catchall_1
    move-exception v1

    :try_start_6
    monitor-exit v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :try_start_7
    throw v1
    :try_end_7
    .catch Ljava/lang/InterruptedException; {:try_start_7 .. :try_end_7} :catch_0
.end method
