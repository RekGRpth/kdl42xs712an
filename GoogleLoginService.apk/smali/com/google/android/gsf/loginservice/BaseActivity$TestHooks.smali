.class public Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;
.super Ljava/lang/Object;
.source "BaseActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/gsf/loginservice/BaseActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "TestHooks"
.end annotation


# instance fields
.field public mBackupUserSet:Ljava/lang/String;

.field public mForceNoNetwork:Z

.field public mGPlus:Ljava/lang/String;

.field public mMarketIntent:Landroid/content/Intent;

.field public mSkipCreditCard:Z

.field public mSkipExistingAccountCheck:Z

.field public mSyncEnabledCount:I


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mForceNoNetwork:Z

    iput-boolean v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSkipExistingAccountCheck:Z

    iput-boolean v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSkipCreditCard:Z

    iput v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mSyncEnabledCount:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gsf/loginservice/BaseActivity$TestHooks;->mGPlus:Ljava/lang/String;

    return-void
.end method
