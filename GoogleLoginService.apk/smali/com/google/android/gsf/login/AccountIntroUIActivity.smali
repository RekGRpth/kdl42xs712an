.class public Lcom/google/android/gsf/login/AccountIntroUIActivity;
.super Lcom/google/android/gsf/login/BaseActivity;
.source "AccountIntroUIActivity.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# instance fields
.field private mCreateButton:Landroid/widget/Button;

.field private mLearnMoreButton:Landroid/widget/ImageButton;

.field private mNextButton:Landroid/widget/Button;

.field private mSkipButton:Landroid/widget/Button;

.field private mSupportingMessage:Landroid/widget/TextView;


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gsf/login/BaseActivity;-><init>()V

    return-void
.end method

.method private initView()V
    .locals 6

    const/4 v5, 0x0

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-eqz v3, :cond_6

    const/high16 v3, 0x7f030000    # com.google.android.gsf.login.R.layout.account_intro_activity

    :goto_0
    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->setContentView(I)V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const v3, 0x7f0b0008    # com.google.android.gsf.login.R.id.skip_button

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSkipButton:Landroid/widget/Button;

    const v3, 0x7f0b000a    # com.google.android.gsf.login.R.id.next_button

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mNextButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mNextButton:Landroid/widget/Button;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mNextButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_0
    const v3, 0x7f0b0003    # com.google.android.gsf.login.R.id.learn_more_button

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mLearnMoreButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mLearnMoreButton:Landroid/widget/ImageButton;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mLearnMoreButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_1
    const v3, 0x7f0b0007    # com.google.android.gsf.login.R.id.create_button

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mCreateButton:Landroid/widget/Button;

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mCreateButton:Landroid/widget/Button;

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mCreateButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :cond_2
    const v3, 0x7f0b0002    # com.google.android.gsf.login.R.id.title

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v2, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const v3, 0x7f0b0005    # com.google.android.gsf.login.R.id.supporting_message

    invoke-virtual {p0, v3}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSupportingMessage:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSupportingMessage:Landroid/widget/TextView;

    if-eqz v3, :cond_3

    const-string v3, "introMessage"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_7

    const-string v3, "GLSActivity"

    const-string v4, "AccountSetupActivity: setting custom intro message"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSupportingMessage:Landroid/widget/TextView;

    const-string v4, "introMessage"

    invoke-virtual {v2, v4}, Landroid/os/Bundle;->getCharSequence(Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_3
    :goto_1
    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-object v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mAccountManagerOptions:Landroid/os/Bundle;

    const-string v4, "allowSkip"

    invoke-virtual {v3, v4, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    const-string v3, "customTitleGoogle"

    invoke-virtual {v2, v3, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "GLSActivity"

    const-string v4, "AccountSetupActivity: setting Required mode"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    const v4, 0x7f0800a2    # com.google.android.gsf.login.R.string.account_intro_required_title

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_4
    :goto_2
    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSkipButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez v0, :cond_5

    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSkipButton:Landroid/widget/Button;

    const/16 v4, 0x8

    invoke-virtual {v3, v4}, Landroid/widget/Button;->setVisibility(I)V

    :cond_5
    return-void

    :cond_6
    const v3, 0x7f030001    # com.google.android.gsf.login.R.layout.account_intro_dialog

    goto/16 :goto_0

    :cond_7
    iget-object v3, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSupportingMessage:Landroid/widget/TextView;

    const v4, 0x7f0800a4    # com.google.android.gsf.login.R.string.account_intro_default_message

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    :cond_8
    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gsf/loginservice/BaseActivity;->mSession:Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;

    iget-boolean v3, v3, Lcom/google/android/gsf/loginservice/GLSUser$GLSSession;->mSetupWizard:Z

    if-nez v3, :cond_4

    const-string v3, "GLSActivity"

    const-string v4, "AccountSetupActivity: setting Recommended mode"

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/google/android/gsf/login/BaseActivity;->mTitle:Landroid/widget/TextView;

    const v4, 0x7f0800a3    # com.google.android.gsf.login.R.string.account_intro_recommended_title

    invoke-virtual {p0, v4}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2
.end method


# virtual methods
.method protected disableBackKey()Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->setResult(I)V

    invoke-super {p0}, Lcom/google/android/gsf/login/BaseActivity;->onBackPressed()V

    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mSkipButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->setResult(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->finish()V

    :goto_1
    return-void

    :cond_0
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mNextButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_1

    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->setResult(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gsf/login/AccountIntroUIActivity;->mLearnMoreButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gsf/login/LearnMoreActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "title_id"

    const v2, 0x7f0800a9    # com.google.android.gsf.login.R.string.learn_more_account_intro_title

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v1, "msg_id"

    const v2, 0x7f0800a8    # com.google.android.gsf.login.R.string.learn_more_account_intro

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_1

    :cond_2
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->setResult(I)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 0
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/google/android/gsf/login/BaseActivity;->onCreate(Landroid/os/Bundle;)V

    invoke-direct {p0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->initView()V

    invoke-virtual {p0}, Lcom/google/android/gsf/login/AccountIntroUIActivity;->overrideAllowBackHardkey()V

    return-void
.end method
