.class Lcom/konka/help/PhotoHelpActivity$1;
.super Landroid/os/Handler;
.source "PhotoHelpActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/help/PhotoHelpActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/help/PhotoHelpActivity;


# direct methods
.method constructor <init>(Lcom/konka/help/PhotoHelpActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1    # Landroid/os/Message;

    const/4 v3, 0x1

    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    # getter for: Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z
    invoke-static {v0}, Lcom/konka/help/PhotoHelpActivity;->access$0(Lcom/konka/help/PhotoHelpActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-virtual {v0, v3}, Lcom/konka/help/PhotoHelpActivity;->moveNextOrPrevious(I)V

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->handler:Landroid/os/Handler;

    const-wide/16 v1, 0x1770

    invoke-virtual {v0, v3, v1, v2}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x11

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    # getter for: Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z
    invoke-static {v0}, Lcom/konka/help/PhotoHelpActivity;->access$0(Lcom/konka/help/PhotoHelpActivity;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    invoke-virtual {v0}, Lcom/konka/help/PhotoHelpActivity;->hideController()V

    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_2

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    # getter for: Lcom/konka/help/PhotoHelpActivity;->isAutoPlaying:Z
    invoke-static {v0}, Lcom/konka/help/PhotoHelpActivity;->access$0(Lcom/konka/help/PhotoHelpActivity;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v1, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v1, v1, Lcom/konka/help/PhotoHelpActivity;->playStateDrawable:[I

    aget v1, v1, v3

    invoke-virtual {v0, v1}, Lcom/konka/help/PhotoHelpActivity;->setStateDrawable(I)V

    :cond_2
    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :cond_3
    iget-object v0, p0, Lcom/konka/help/PhotoHelpActivity$1;->this$0:Lcom/konka/help/PhotoHelpActivity;

    iget-object v0, v0, Lcom/konka/help/PhotoHelpActivity;->stateDrawable:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
