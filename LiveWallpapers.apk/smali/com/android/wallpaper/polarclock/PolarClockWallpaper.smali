.class public Lcom/android/wallpaper/polarclock/PolarClockWallpaper;
.super Landroid/service/wallpaper/WallpaperService;
.source "PolarClockWallpaper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;,
        Lcom/android/wallpaper/polarclock/PolarClockWallpaper$CyclingClockPalette;,
        Lcom/android/wallpaper/polarclock/PolarClockWallpaper$FixedClockPalette;,
        Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockPalette;
    }
.end annotation


# instance fields
.field private mFilter:Landroid/content/IntentFilter;

.field private final mHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/service/wallpaper/WallpaperService;-><init>()V

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$100(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/os/Handler;
    .locals 1
    .param p0    # Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)Landroid/content/IntentFilter;
    .locals 1
    .param p0    # Lcom/android/wallpaper/polarclock/PolarClockWallpaper;

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->mFilter:Landroid/content/IntentFilter;

    return-object v0
.end method


# virtual methods
.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService;->onCreate()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    return-void
.end method

.method public onCreateEngine()Landroid/service/wallpaper/WallpaperService$Engine;
    .locals 1

    new-instance v0, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;

    invoke-direct {v0, p0}, Lcom/android/wallpaper/polarclock/PolarClockWallpaper$ClockEngine;-><init>(Lcom/android/wallpaper/polarclock/PolarClockWallpaper;)V

    return-object v0
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Landroid/service/wallpaper/WallpaperService;->onDestroy()V

    return-void
.end method
