.class public Lcom/android/wallpaper/grass/ScriptField_Blade;
.super Landroid/renderscript/Script$FieldBase;
.source "ScriptField_Blade.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    }
.end annotation


# static fields
.field private static mElementCache:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/renderscript/Element;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private mIOBuffer:Landroid/renderscript/FieldPacker;

.field private mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    new-instance v0, Ljava/lang/ref/WeakReference;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    sput-object v0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mElementCache:Ljava/lang/ref/WeakReference;

    return-void
.end method

.method public constructor <init>(Landroid/renderscript/RenderScript;I)V
    .locals 1
    .param p1    # Landroid/renderscript/RenderScript;
    .param p2    # I

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/renderscript/Script$FieldBase;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-static {p1}, Lcom/android/wallpaper/grass/ScriptField_Blade;->createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v0

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mElement:Landroid/renderscript/Element;

    invoke-virtual {p0, p1, p2}, Lcom/android/wallpaper/grass/ScriptField_Blade;->init(Landroid/renderscript/RenderScript;I)V

    return-void
.end method

.method private copyToArray(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;I)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .param p2    # I

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    if-nez v0, :cond_0

    new-instance v0, Landroid/renderscript/FieldPacker;

    invoke-virtual {p0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    mul-int/lit8 v1, v1, 0x34

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    iput-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    :cond_0
    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    mul-int/lit8 v1, p2, 0x34

    invoke-virtual {v0, v1}, Landroid/renderscript/FieldPacker;->reset(I)V

    iget-object v0, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-direct {p0, p1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArrayLocal(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;Landroid/renderscript/FieldPacker;)V

    return-void
.end method

.method private copyToArrayLocal(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;Landroid/renderscript/FieldPacker;)V
    .locals 1
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .param p2    # Landroid/renderscript/FieldPacker;

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->angle:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->size:I

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addI32(I)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->xPos:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->yPos:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->offset:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->scale:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthX:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->lengthY:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->hardness:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->h:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->s:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->b:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    iget v0, p1, Lcom/android/wallpaper/grass/ScriptField_Blade$Item;->turbulencex:F

    invoke-virtual {p2, v0}, Landroid/renderscript/FieldPacker;->addF32(F)V

    return-void
.end method

.method public static createElement(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;
    .locals 3
    .param p0    # Landroid/renderscript/RenderScript;

    new-instance v0, Landroid/renderscript/Element$Builder;

    invoke-direct {v0, p0}, Landroid/renderscript/Element$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "angle"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->I32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "size"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "xPos"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "yPos"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "offset"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "scale"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "lengthX"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "lengthY"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "hardness"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "h"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "s"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "b"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-static {p0}, Landroid/renderscript/Element;->F32(Landroid/renderscript/RenderScript;)Landroid/renderscript/Element;

    move-result-object v1

    const-string v2, "turbulencex"

    invoke-virtual {v0, v1, v2}, Landroid/renderscript/Element$Builder;->add(Landroid/renderscript/Element;Ljava/lang/String;)Landroid/renderscript/Element$Builder;

    invoke-virtual {v0}, Landroid/renderscript/Element$Builder;->create()Landroid/renderscript/Element;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public declared-synchronized copyAll()V
    .locals 4

    monitor-enter p0

    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aget-object v1, v1, v0

    invoke-direct {p0, v1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArray(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mAllocation:Landroid/renderscript/Allocation;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mIOBuffer:Landroid/renderscript/FieldPacker;

    invoke-virtual {v1, v2, v3}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public declared-synchronized set(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;IZ)V
    .locals 2
    .param p1    # Lcom/android/wallpaper/grass/ScriptField_Blade$Item;
    .param p2    # I
    .param p3    # Z

    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->getType()Landroid/renderscript/Type;

    move-result-object v1

    invoke-virtual {v1}, Landroid/renderscript/Type;->getX()I

    move-result v1

    new-array v1, v1, [Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    iput-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    :cond_0
    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mItemArray:[Lcom/android/wallpaper/grass/ScriptField_Blade$Item;

    aput-object p1, v1, p2

    if-eqz p3, :cond_1

    invoke-direct {p0, p1, p2}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArray(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;I)V

    new-instance v0, Landroid/renderscript/FieldPacker;

    const/16 v1, 0x34

    invoke-direct {v0, v1}, Landroid/renderscript/FieldPacker;-><init>(I)V

    invoke-direct {p0, p1, v0}, Lcom/android/wallpaper/grass/ScriptField_Blade;->copyToArrayLocal(Lcom/android/wallpaper/grass/ScriptField_Blade$Item;Landroid/renderscript/FieldPacker;)V

    iget-object v1, p0, Lcom/android/wallpaper/grass/ScriptField_Blade;->mAllocation:Landroid/renderscript/Allocation;

    invoke-virtual {v1, p2, v0}, Landroid/renderscript/Allocation;->setFromFieldPacker(ILandroid/renderscript/FieldPacker;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_1
    monitor-exit p0

    return-void

    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method
