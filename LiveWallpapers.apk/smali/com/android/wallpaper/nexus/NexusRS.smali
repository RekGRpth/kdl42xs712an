.class Lcom/android/wallpaper/nexus/NexusRS;
.super Lcom/android/wallpaper/RenderScriptScene;
.source "NexusRS.java"


# instance fields
.field private mInitialHeight:I

.field private mInitialWidth:I

.field private final mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

.field private mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

.field private mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

.field private mWorldScaleX:F

.field private mWorldScaleY:F

.field private mXOffset:F


# direct methods
.method public constructor <init>(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-direct {p0, p1, p2}, Lcom/android/wallpaper/RenderScriptScene;-><init>(II)V

    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    iput p1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mInitialWidth:I

    iput p2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mInitialHeight:I

    iput v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleX:F

    iput v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleY:F

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/graphics/BitmapFactory$Options;->inScaled:Z

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    sget-object v1, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    iput-object v1, v0, Landroid/graphics/BitmapFactory$Options;->inPreferredConfig:Landroid/graphics/Bitmap$Config;

    return-void
.end method

.method private createProgramFragment()V
    .locals 6

    const/4 v5, 0x0

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v3}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v3, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->MODULATE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v4, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGBA:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v3, v4, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v1

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v3}, Landroid/renderscript/Sampler;->WRAP_LINEAR(Landroid/renderscript/RenderScript;)Landroid/renderscript/Sampler;

    move-result-object v3

    invoke-virtual {v1, v3, v5}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v3, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gPFTexture(Landroid/renderscript/ProgramFragment;)V

    new-instance v0, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v3}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v3, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;->MODULATE:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;

    sget-object v4, Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;->RGB:Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;

    invoke-virtual {v0, v3, v4, v5}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->setTexture(Landroid/renderscript/ProgramFragmentFixedFunction$Builder$EnvMode;Landroid/renderscript/ProgramFragmentFixedFunction$Builder$Format;I)Landroid/renderscript/ProgramFragmentFixedFunction$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramFragmentFixedFunction$Builder;->create()Landroid/renderscript/ProgramFragmentFixedFunction;

    move-result-object v2

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v3}, Landroid/renderscript/Sampler;->CLAMP_NEAREST(Landroid/renderscript/RenderScript;)Landroid/renderscript/Sampler;

    move-result-object v3

    invoke-virtual {v2, v3, v5}, Landroid/renderscript/ProgramFragment;->bindSampler(Landroid/renderscript/Sampler;I)V

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v3, v2}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gPFTexture565(Landroid/renderscript/ProgramFragment;)V

    return-void
.end method

.method private createProgramFragmentStore()V
    .locals 4

    new-instance v0, Landroid/renderscript/ProgramStore$Builder;

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v0, v2}, Landroid/renderscript/ProgramStore$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    sget-object v2, Landroid/renderscript/ProgramStore$DepthFunc;->ALWAYS:Landroid/renderscript/ProgramStore$DepthFunc;

    invoke-virtual {v0, v2}, Landroid/renderscript/ProgramStore$Builder;->setDepthFunc(Landroid/renderscript/ProgramStore$DepthFunc;)Landroid/renderscript/ProgramStore$Builder;

    sget-object v2, Landroid/renderscript/ProgramStore$BlendSrcFunc;->ONE:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v3, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v2, v3}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/renderscript/ProgramStore$Builder;->setDitherEnabled(Z)Landroid/renderscript/ProgramStore$Builder;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v1

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v2, v1}, Landroid/renderscript/RenderScriptGL;->bindProgramStore(Landroid/renderscript/ProgramStore;)V

    sget-object v2, Landroid/renderscript/ProgramStore$BlendSrcFunc;->SRC_ALPHA:Landroid/renderscript/ProgramStore$BlendSrcFunc;

    sget-object v3, Landroid/renderscript/ProgramStore$BlendDstFunc;->ONE:Landroid/renderscript/ProgramStore$BlendDstFunc;

    invoke-virtual {v0, v2, v3}, Landroid/renderscript/ProgramStore$Builder;->setBlendFunc(Landroid/renderscript/ProgramStore$BlendSrcFunc;Landroid/renderscript/ProgramStore$BlendDstFunc;)Landroid/renderscript/ProgramStore$Builder;

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v0}, Landroid/renderscript/ProgramStore$Builder;->create()Landroid/renderscript/ProgramStore;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gPSBlend(Landroid/renderscript/ProgramStore;)V

    return-void
.end method

.method private createProgramVertex()V
    .locals 5

    new-instance v3, Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    iget-object v4, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v3, v4}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;-><init>(Landroid/renderscript/RenderScript;)V

    iput-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    new-instance v0, Landroid/renderscript/Matrix4f;

    invoke-direct {v0}, Landroid/renderscript/Matrix4f;-><init>()V

    iget v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWidth:I

    iget v4, p0, Lcom/android/wallpaper/nexus/NexusRS;->mHeight:I

    invoke-virtual {v0, v3, v4}, Landroid/renderscript/Matrix4f;->loadOrthoWindow(II)V

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v3, v0}, Landroid/renderscript/ProgramVertexFixedFunction$Constants;->setProjection(Landroid/renderscript/Matrix4f;)V

    new-instance v2, Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-direct {v2, v3}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;-><init>(Landroid/renderscript/RenderScript;)V

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->setTextureMatrixEnable(Z)Landroid/renderscript/ProgramVertexFixedFunction$Builder;

    invoke-virtual {v2}, Landroid/renderscript/ProgramVertexFixedFunction$Builder;->create()Landroid/renderscript/ProgramVertexFixedFunction;

    move-result-object v1

    move-object v3, v1

    check-cast v3, Landroid/renderscript/ProgramVertexFixedFunction;

    iget-object v4, p0, Lcom/android/wallpaper/nexus/NexusRS;->mPvOrthoAlloc:Landroid/renderscript/ProgramVertexFixedFunction$Constants;

    invoke-virtual {v3, v4}, Landroid/renderscript/ProgramVertexFixedFunction;->bindConstants(Landroid/renderscript/ProgramVertexFixedFunction$Constants;)V

    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-virtual {v3, v1}, Landroid/renderscript/RenderScriptGL;->bindProgramVertex(Landroid/renderscript/ProgramVertex;)V

    return-void
.end method

.method private createState()V
    .locals 4

    :try_start_0
    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mResources:Landroid/content/res/Resources;

    const/high16 v3, 0x7f070000    # com.android.wallpaper.R.integer.nexus_mode

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    iget-object v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {p0}, Lcom/android/wallpaper/nexus/NexusRS;->isPreview()Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_1
    invoke-virtual {v3, v2}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gIsPreview(I)V

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v2, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gMode(I)V

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gXOffset(F)V

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    iget v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleX:F

    invoke-virtual {v2, v3}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gWorldScaleX(F)V

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    iget v3, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleY:F

    invoke-virtual {v2, v3}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gWorldScaleY(F)V

    return-void

    :catch_0
    move-exception v0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private loadTexture(I)Landroid/renderscript/Allocation;
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mResources:Landroid/content/res/Resources;

    invoke-static {v0, v1, p1}, Landroid/renderscript/Allocation;->createFromBitmapResource(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)Landroid/renderscript/Allocation;

    move-result-object v0

    return-object v0
.end method

.method private loadTextureARGB(I)Landroid/renderscript/Allocation;
    .locals 3
    .param p1    # I

    iget-object v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mResources:Landroid/content/res/Resources;

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mOptionsARGB:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v1, p1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    iget-object v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    invoke-static {v1, v0}, Landroid/renderscript/Allocation;->createFromBitmap(Landroid/renderscript/RenderScript;Landroid/graphics/Bitmap;)Landroid/renderscript/Allocation;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method protected createScript()Landroid/renderscript/ScriptC;
    .locals 4

    new-instance v0, Lcom/android/wallpaper/nexus/ScriptC_nexus;

    iget-object v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mRS:Landroid/renderscript/RenderScriptGL;

    iget-object v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mResources:Landroid/content/res/Resources;

    const v3, 0x7f040003    # com.android.wallpaper.R.raw.nexus

    invoke-direct {v0, v1, v2, v3}, Lcom/android/wallpaper/nexus/ScriptC_nexus;-><init>(Landroid/renderscript/RenderScript;Landroid/content/res/Resources;I)V

    iput-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-direct {p0}, Lcom/android/wallpaper/nexus/NexusRS;->createProgramFragmentStore()V

    invoke-direct {p0}, Lcom/android/wallpaper/nexus/NexusRS;->createProgramFragment()V

    invoke-direct {p0}, Lcom/android/wallpaper/nexus/NexusRS;->createProgramVertex()V

    invoke-direct {p0}, Lcom/android/wallpaper/nexus/NexusRS;->createState()V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    const v1, 0x7f020010    # com.android.wallpaper.R.drawable.pyramid_background

    invoke-direct {p0, v1}, Lcom/android/wallpaper/nexus/NexusRS;->loadTexture(I)Landroid/renderscript/Allocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gTBackground(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    const v1, 0x7f02000f    # com.android.wallpaper.R.drawable.pulse

    invoke-direct {p0, v1}, Lcom/android/wallpaper/nexus/NexusRS;->loadTextureARGB(I)Landroid/renderscript/Allocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gTPulse(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    const v1, 0x7f020002    # com.android.wallpaper.R.drawable.glow

    invoke-direct {p0, v1}, Lcom/android/wallpaper/nexus/NexusRS;->loadTextureARGB(I)Landroid/renderscript/Allocation;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gTGlow(Landroid/renderscript/Allocation;)V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->setTimeZone(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v0}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->invoke_initPulses()V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    return-object v0
.end method

.method public onCommand(Ljava/lang/String;IIILandroid/os/Bundle;Z)Landroid/os/Bundle;
    .locals 3
    .param p1    # Ljava/lang/String;
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # Landroid/os/Bundle;
    .param p6    # Z

    iget v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWidth:I

    iget v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mHeight:I

    if-ge v0, v1, :cond_0

    int-to-float v0, p2

    iget v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mXOffset:F

    iget v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWidth:I

    int-to-float v2, v2

    mul-float/2addr v1, v2

    iget v2, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleX:F

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-int p2, v0

    :cond_0
    const-string v0, "android.wallpaper.tap"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "android.wallpaper.secondaryTap"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v0, "android.home.drop"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v0, p2, p3}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->invoke_addTap(II)V

    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method public resize(II)V
    .locals 2
    .param p1    # I
    .param p2    # I

    invoke-super {p0, p1, p2}, Lcom/android/wallpaper/RenderScriptScene;->resize(II)V

    iget v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mInitialWidth:I

    int-to-float v0, v0

    int-to-float v1, p1

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleX:F

    iget v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mInitialHeight:I

    int-to-float v0, v0

    int-to-float v1, p2

    div-float/2addr v0, v1

    iput v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleY:F

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    iget v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleX:F

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gWorldScaleX(F)V

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    iget v1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mWorldScaleY:F

    invoke-virtual {v0, v1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gWorldScaleY(F)V

    return-void
.end method

.method public setOffset(FFII)V
    .locals 1
    .param p1    # F
    .param p2    # F
    .param p3    # I
    .param p4    # I

    iput p1, p0, Lcom/android/wallpaper/nexus/NexusRS;->mXOffset:F

    iget-object v0, p0, Lcom/android/wallpaper/nexus/NexusRS;->mScript:Lcom/android/wallpaper/nexus/ScriptC_nexus;

    invoke-virtual {v0, p1}, Lcom/android/wallpaper/nexus/ScriptC_nexus;->set_gXOffset(F)V

    return-void
.end method

.method public start()V
    .locals 0

    invoke-super {p0}, Lcom/android/wallpaper/RenderScriptScene;->start()V

    return-void
.end method
