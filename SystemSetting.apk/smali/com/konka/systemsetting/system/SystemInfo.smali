.class public Lcom/konka/systemsetting/system/SystemInfo;
.super Ljava/lang/Object;
.source "SystemInfo.java"


# instance fields
.field private CURRENT_LANGUAGE:Ljava/lang/String;

.field private mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

.field private mPlatform:Ljava/lang/String;

.field private m_context:Lcom/konka/systemsetting/MainActivity;

.field private m_openSourceLicenses:Landroid/widget/Button;

.field private m_tvAndroidVersion:Landroid/widget/TextView;

.field private m_tvMacAddr:Landroid/widget/TextView;

.field private m_tvRAMSize:Landroid/widget/TextView;

.field private m_tvResolution:Landroid/widget/TextView;

.field private m_tvSerialNum:Landroid/widget/TextView;

.field private m_tvTVVersion:Landroid/widget/TextView;

.field private m_tveMMCSize:Landroid/widget/TextView;

.field private serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field private ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 2
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->initViews()V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_openSourceLicenses:Landroid/widget/Button;

    const v1, 0x7f0900ad    # com.konka.systemsetting.R.id.btn_open_source_licenses

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusUpId(I)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/system/SystemInfo;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method private getKernelVersion()Ljava/lang/String;
    .locals 1

    const-string v0, "2.6.35.11"

    return-object v0
.end method

.method private getMacAddr()Ljava/lang/String;
    .locals 3

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Landroid/net/ethernet/EthernetDevInfo;

    invoke-direct {v0}, Landroid/net/ethernet/EthernetDevInfo;-><init>()V

    const-string v2, "dhcp"

    invoke-virtual {v0, v2}, Landroid/net/ethernet/EthernetDevInfo;->setConnectMode(Ljava/lang/String;)Z

    :cond_0
    invoke-virtual {v0}, Landroid/net/ethernet/EthernetDevInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getResolution()Ljava/lang/String;
    .locals 4

    const/4 v1, 0x0

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/TvManager;->getPictureManager()Lcom/mstar/android/tvapi/common/PictureManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/mstar/android/tvapi/common/PictureManager;->getPanelWidthHeight()Lcom/mstar/android/tvapi/common/vo/PanelProperty;
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemInfo;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    sget-object v3, Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;->SIXM40_IC:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    if-ne v2, v3, :cond_1

    const-string v2, "4K\u00d72K"

    :goto_1
    return-object v2

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    const/16 v3, 0x780

    if-eq v2, v3, :cond_2

    iget v2, v1, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    const/16 v3, 0x556

    if-eq v2, v3, :cond_2

    const/4 v2, 0x0

    goto :goto_1

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->width:I

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, "X"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v1, Lcom/mstar/android/tvapi/common/vo/PanelProperty;->height:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1
.end method

.method private getSerialNum()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getSN()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTVAndroidVersion()Ljava/lang/String;
    .locals 2

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u7eef\u8364\u7cba\u9428\u52ed\u5897\u93c8\ue100\u5f7f"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\u7eef\u8364\u7cba\u9428\u52ed\u5897\u93c8\ue100\u5f7f11"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    const-string v0, "\u7eef\u8364\u7cba\u9428\u52ed\u5897\u93c8\ue100\u5f7f2214"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v0, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    return-object v0
.end method

.method private getTVExStorageSize()Ljava/lang/String;
    .locals 3

    new-instance v2, Lcom/konka/utilities/Storage;

    invoke-direct {v2}, Lcom/konka/utilities/Storage;-><init>()V

    invoke-virtual {v2}, Lcom/konka/utilities/Storage;->getDiskStorage()Lcom/konka/utilities/StorageInfor;

    move-result-object v2

    iget-wide v0, v2, Lcom/konka/utilities/StorageInfor;->mTotalStorage:J

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v2, v0, v1}, Lcom/konka/utilities/Tools;->sizeToAutoUnit(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    return-object v2
.end method

.method private getTVRAMSize()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v0}, Lcom/konka/utilities/Tools;->getTotalMemory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getTVVersion()Ljava/lang/String;
    .locals 2

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strVersion:Ljava/lang/String;

    return-object v1
.end method

.method private geteMMCSize()Ljava/lang/String;
    .locals 6

    const-wide/16 v4, 0x400

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v2}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f060001    # com.konka.systemsetting.R.string.lang_flag

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/systemsetting/system/SystemInfo;->CURRENT_LANGUAGE:Ljava/lang/String;

    new-instance v2, Lcom/konka/utilities/Storage;

    invoke-direct {v2}, Lcom/konka/utilities/Storage;-><init>()V

    invoke-virtual {v2}, Lcom/konka/utilities/Storage;->getInternalStorage()Lcom/konka/utilities/StorageInfor;

    move-result-object v2

    iget-wide v0, v2, Lcom/konka/utilities/StorageInfor;->mTotalStorage:J

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "eMMC size:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v3, v0, v1}, Lcom/konka/utilities/Tools;->sizeToAutoUnit(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    div-long v2, v0, v4

    div-long/2addr v2, v4

    div-long/2addr v2, v4

    const-wide/16 v4, 0x2

    cmp-long v2, v2, v4

    if-ltz v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemInfo;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v3, "rus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "4\u0413\u0431"

    :goto_0
    return-object v2

    :cond_0
    const-string v2, "4GB"

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/system/SystemInfo;->CURRENT_LANGUAGE:Ljava/lang/String;

    const-string v3, "rus"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "2\u0413\u0431"

    goto :goto_0

    :cond_2
    const-string v2, "2GB"

    goto :goto_0
.end method

.method private initViews()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->mCommonDesk:Lcom/konka/kkinterface/tv/CommonDesk;

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getUrsaSelect()Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->ursaType:Lcom/konka/kkinterface/tv/CommonDesk$UrsaSelectEnumType;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900a5    # com.konka.systemsetting.R.id.tv_ram_size

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvRAMSize:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900a6    # com.konka.systemsetting.R.id.tv_emmc_size

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tveMMCSize:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900a7    # com.konka.systemsetting.R.id.tv_android_version

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvAndroidVersion:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900a8    # com.konka.systemsetting.R.id.tv_tv_version

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvTVVersion:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900aa    # com.konka.systemsetting.R.id.tv_resolution

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvResolution:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900ab    # com.konka.systemsetting.R.id.tv_serialnum

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvSerialNum:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900ac    # com.konka.systemsetting.R.id.tv_macaddr

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvMacAddr:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900ad    # com.konka.systemsetting.R.id.btn_open_source_licenses

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_openSourceLicenses:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvRAMSize:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getTVRAMSize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tveMMCSize:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->geteMMCSize()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvAndroidVersion:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getTVAndroidVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvTVVersion:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getTVVersion()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getResolution()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_context:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0900a9    # com.konka.systemsetting.R.id.item_resolution

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvSerialNum:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getSerialNum()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvMacAddr:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getMacAddr()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Lcom/konka/systemsetting/system/SystemInfo$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemInfo$1;-><init>(Lcom/konka/systemsetting/system/SystemInfo;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_openSourceLicenses:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInfo;->m_tvResolution:Landroid/widget/TextView;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemInfo;->getResolution()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
