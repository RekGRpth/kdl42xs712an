.class public Lcom/konka/systemsetting/system/SystemFactoryReset;
.super Ljava/lang/Object;
.source "SystemFactoryReset.java"


# static fields
.field private static tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;


# instance fields
.field private m_buttonReset:Landroid/widget/Button;

.field private m_context:Lcom/konka/systemsetting/MainActivity;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090090    # com.konka.systemsetting.R.id.button_system_factoryreset

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_context:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/system/SystemFactoryReset;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_context:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_buttonReset:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemFactoryReset;->SetOnClickListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemFactoryReset;->setOnFocusChangeListener()V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_buttonReset:Landroid/widget/Button;

    const v1, 0x7f09009b    # com.konka.systemsetting.R.id.linearlayout_system_tab_factoryreset

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_buttonReset:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusUpId(I)V

    return-void
.end method

.method private SetOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemFactoryReset$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemFactoryReset$1;-><init>(Lcom/konka/systemsetting/system/SystemFactoryReset;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_buttonReset:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/system/SystemFactoryReset;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/system/SystemFactoryReset;->reset()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/system/SystemFactoryReset;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_context:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method private reset()V
    .locals 21

    const-string v18, "Start to reset"

    invoke-static/range {v18 .. v18}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    sget-object v18, Lcom/konka/systemsetting/system/SystemFactoryReset;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v18

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/DataBaseDesk;->getFactoryCusDefSetting()Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;

    move-result-object v17

    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, v17

    iput v0, v1, Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;->IsRestoreNeeded:I

    sget-object v18, Lcom/konka/systemsetting/system/SystemFactoryReset;->tvDeskProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

    invoke-interface/range {v18 .. v18}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDataBaseManagerInstance()Lcom/konka/kkinterface/tv/DataBaseDesk;

    move-result-object v18

    move-object/from16 v0, v18

    move-object/from16 v1, v17

    invoke-interface {v0, v1}, Lcom/konka/kkinterface/tv/DataBaseDesk;->setFactoryCusDefSetting(Lcom/konka/kkinterface/tv/DataBaseDesk$MS_FACTORY_CUS_DEF_SETTING;)Z

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v18

    const-string v19, "game_mode"

    const-string v20, "disable"

    invoke-virtual/range {v18 .. v20}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z

    const-string v18, "aaron"

    const-string v19, "delete begain"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v15, Ljava/io/FileInputStream;

    const-string v18, "/tvdatabase/DatabaseBackup/user_setting.db"

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v16, Ljava/io/FileOutputStream;

    const-string v18, "/tvdatabase/Database/user_setting.db"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v6, Ljava/io/FileInputStream;

    const-string v18, "/tvdatabase/DatabaseBackup/atv_cmdb_cable_bak.bin"

    move-object/from16 v0, v18

    invoke-direct {v6, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/io/FileInputStream;

    const-string v18, "/tvdatabase/DatabaseBackup/atv_cmdb_bak.bin"

    move-object/from16 v0, v18

    invoke-direct {v4, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/io/FileOutputStream;

    const-string v18, "/tvdatabase/Database/atv_cmdb.bin"

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v7, Ljava/io/FileOutputStream;

    const-string v18, "/tvdatabase/Database/atv_cmdb_cable.bin"

    move-object/from16 v0, v18

    invoke-direct {v7, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/io/FileInputStream;

    const-string v18, "/tvdatabase/DatabaseBackup/dtv_cmdb_0_bak.bin"

    move-object/from16 v0, v18

    invoke-direct {v8, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v10, Ljava/io/FileInputStream;

    const-string v18, "/tvdatabase/DatabaseBackup/dtv_cmdb_1_bak.bin"

    move-object/from16 v0, v18

    invoke-direct {v10, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v12, Ljava/io/FileInputStream;

    const-string v18, "/tvdatabase/DatabaseBackup/dtv_cmdb_2_bak.bin"

    move-object/from16 v0, v18

    invoke-direct {v12, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v9, Ljava/io/FileOutputStream;

    const-string v18, "/tvdatabase/Database/dtv_cmdb_0.bin"

    move-object/from16 v0, v18

    invoke-direct {v9, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v11, Ljava/io/FileOutputStream;

    const-string v18, "/tvdatabase/Database/dtv_cmdb_1.bin"

    move-object/from16 v0, v18

    invoke-direct {v11, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    new-instance v13, Ljava/io/FileOutputStream;

    const-string v18, "/tvdatabase/Database/dtv_cmdb_2.bin"

    move-object/from16 v0, v18

    invoke-direct {v13, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    const/16 v18, 0x2000

    move/from16 v0, v18

    new-array v2, v0, [B

    const/4 v3, 0x0

    :goto_0
    invoke-virtual {v6, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_0

    :goto_1
    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_1

    :goto_2
    invoke-virtual {v8, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_2

    :goto_3
    invoke-virtual {v10, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_3

    :goto_4
    invoke-virtual {v12, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_4

    :goto_5
    invoke-virtual {v15, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_5

    invoke-virtual {v6}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v7}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v5}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v8}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v10}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v12}, Ljava/io/FileInputStream;->close()V

    invoke-virtual {v9}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v11}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v13}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    new-instance v15, Ljava/io/FileInputStream;

    const-string v18, "/customercfg/user_settingbackup.db"

    move-object/from16 v0, v18

    invoke-direct {v15, v0}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    new-instance v16, Ljava/io/FileOutputStream;

    const-string v18, "/customercfg/user_setting.db"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;)V

    :goto_6
    invoke-virtual {v15, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v3

    if-gtz v3, :cond_6

    invoke-virtual/range {v16 .. v16}, Ljava/io/FileOutputStream;->close()V

    invoke-virtual {v15}, Ljava/io/FileInputStream;->close()V

    const-string v18, "aaron"

    const-string v19, "delete end"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_context:Lcom/konka/systemsetting/MainActivity;

    move-object/from16 v18, v0

    new-instance v19, Landroid/content/Intent;

    const-string v20, "android.intent.action.MASTER_CLEAR"

    invoke-direct/range {v19 .. v20}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v18 .. v19}, Lcom/konka/systemsetting/MainActivity;->sendBroadcast(Landroid/content/Intent;)V

    :goto_7
    return-void

    :cond_0
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v7, v2, v0, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v14

    sget-object v18, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v19, "reset fail!"

    invoke-virtual/range {v18 .. v19}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/IOException;->printStackTrace()V

    goto :goto_7

    :cond_1
    const/16 v18, 0x0

    :try_start_1
    move/from16 v0, v18

    invoke-virtual {v5, v2, v0, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    :catch_1
    move-exception v14

    invoke-virtual {v14}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_7

    :cond_2
    const/16 v18, 0x0

    :try_start_2
    move/from16 v0, v18

    invoke-virtual {v9, v2, v0, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_2

    :cond_3
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v11, v2, v0, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_3

    :cond_4
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v13, v2, v0, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_4

    :cond_5
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v3}, Ljava/io/FileOutputStream;->write([BII)V

    goto/16 :goto_5

    :cond_6
    const/16 v18, 0x0

    move-object/from16 v0, v16

    move/from16 v1, v18

    invoke-virtual {v0, v2, v1, v3}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_6
.end method

.method private setOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/system/SystemFactoryReset$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/system/SystemFactoryReset$2;-><init>(Lcom/konka/systemsetting/system/SystemFactoryReset;)V

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemFactoryReset;->m_buttonReset:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method
