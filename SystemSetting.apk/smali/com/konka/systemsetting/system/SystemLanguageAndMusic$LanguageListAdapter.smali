.class Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;
.super Landroid/widget/BaseAdapter;
.source "SystemLanguageAndMusic.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/system/SystemLanguageAndMusic;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "LanguageListAdapter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public getCount()I
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$0(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)[Lcom/konka/kkimplements/tv/LanguageItem;

    move-result-object v0

    array-length v0, v0

    return v0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1    # I

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1    # I

    int-to-long v0, p1

    return-wide v0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1    # I
    .param p2    # Landroid/view/View;
    .param p3    # Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$1(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x1090012    # android.R.layout.select_dialog_singlechoice

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object p2

    const v1, 0x1020014    # android.R.id.text1

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckedTextView;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$LanguageListAdapter;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->languages:[Lcom/konka/kkimplements/tv/LanguageItem;
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$0(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)[Lcom/konka/kkimplements/tv/LanguageItem;

    move-result-object v1

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/konka/kkimplements/tv/LanguageItem;->mDisplayName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/CheckedTextView;->setText(Ljava/lang/CharSequence;)V

    return-object p2
.end method
