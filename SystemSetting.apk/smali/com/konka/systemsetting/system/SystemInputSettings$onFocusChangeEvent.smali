.class Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;
.super Ljava/lang/Object;
.source "SystemInputSettings.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/system/SystemInputSettings;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "onFocusChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemInputSettings;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/system/SystemInputSettings;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/system/SystemInputSettings;Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;-><init>(Lcom/konka/systemsetting/system/SystemInputSettings;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v2, 0x1

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$3(Lcom/konka/systemsetting/system/SystemInputSettings;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$3(Lcom/konka/systemsetting/system/SystemInputSettings;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    iget-object v1, p0, Lcom/konka/systemsetting/system/SystemInputSettings$onFocusChangeEvent;->this$0:Lcom/konka/systemsetting/system/SystemInputSettings;

    # getter for: Lcom/konka/systemsetting/system/SystemInputSettings;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/system/SystemInputSettings;->access$3(Lcom/konka/systemsetting/system/SystemInputSettings;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_systemPageManager:Lcom/konka/systemsetting/system/SystemPageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-virtual {v0, v2, v2}, Lcom/konka/systemsetting/system/SystemPageManager;->setLastFocus(II)V

    const v0, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    const v0, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0
.end method
