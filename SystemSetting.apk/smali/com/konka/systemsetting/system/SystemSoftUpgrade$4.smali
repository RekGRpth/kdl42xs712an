.class Lcom/konka/systemsetting/system/SystemSoftUpgrade$4;
.super Ljava/lang/Object;
.source "SystemSoftUpgrade.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemSoftUpgrade;->startUpgradeByUSB()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemSoftUpgrade;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemSoftUpgrade;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemSoftUpgrade$4;->this$0:Lcom/konka/systemsetting/system/SystemSoftUpgrade;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    const-string v1, "startUpgradeByUSB"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :try_start_0
    const-string v1, "setEnvironment :upgrade_mode usb"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    const-string v2, "upgrade_mode"

    const-string v3, "usb"

    invoke-virtual {v1, v2, v3}, Lcom/mstar/android/tvapi/common/TvManager;->setEnvironment(Ljava/lang/String;Ljava/lang/String;)Z
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    invoke-static {}, Lcom/konka/systemsetting/SysRootApp;->getCommonSkin()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v1

    const-string v2, "reboot"

    invoke-virtual {v1, v2}, Lcom/mstar/android/tv/TvCommonManager;->rebootSystem(Ljava/lang/String;)V

    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method
