.class Lcom/konka/systemsetting/system/SystemLanguageAndMusic$4;
.super Ljava/lang/Object;
.source "SystemLanguageAndMusic.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->showMusicSettingDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$4;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    invoke-static {p2}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$10(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$4;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # invokes: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->updateState()V
    invoke-static {v0}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$7(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;)V

    iget-object v0, p0, Lcom/konka/systemsetting/system/SystemLanguageAndMusic$4;->this$0:Lcom/konka/systemsetting/system/SystemLanguageAndMusic;

    # getter for: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->m_iCurrMusicIndex:I
    invoke-static {}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$11()I

    move-result v1

    # invokes: Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->switchMusic(I)V
    invoke-static {v0, v1}, Lcom/konka/systemsetting/system/SystemLanguageAndMusic;->access$12(Lcom/konka/systemsetting/system/SystemLanguageAndMusic;I)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
