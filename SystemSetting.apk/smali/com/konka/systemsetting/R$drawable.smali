.class public final Lcom/konka/systemsetting/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final app_icon_systemset:I = 0x7f020000

.field public static final disclaimer:I = 0x7f020001

.field public static final en_off:I = 0x7f020002

.field public static final en_on:I = 0x7f020003

.field public static final ic_bt_config:I = 0x7f020004

.field public static final ic_btn_next:I = 0x7f020005

.field public static final ic_launcher:I = 0x7f020006

.field public static final ic_list_sync_anim:I = 0x7f020007

.field public static final ic_menu_delete_holo_dark:I = 0x7f020008

.field public static final ic_menu_refresh_holo_dark:I = 0x7f020009

.field public static final ic_sync_anim_holo:I = 0x7f02000a

.field public static final ic_sync_error_holo:I = 0x7f02000b

.field public static final ic_sync_green_holo:I = 0x7f02000c

.field public static final ic_sync_grey_holo:I = 0x7f02000d

.field public static final ic_sync_red_holo:I = 0x7f02000e

.field public static final nav_divider:I = 0x7f02000f

.field public static final off:I = 0x7f020010

.field public static final on:I = 0x7f020011

.field public static final one_px:I = 0x7f020012

.field public static final shortcut_icon_energy:I = 0x7f020013

.field public static final shortcut_icon_input_method:I = 0x7f020014

.field public static final shortcut_icon_ipset:I = 0x7f020015

.field public static final shortcut_icon_timing:I = 0x7f020016

.field public static final shortcut_icon_uptate:I = 0x7f020017

.field public static final sys_firstlevel_menu_item_state:I = 0x7f020018

.field public static final sys_second_menu_item_state:I = 0x7f020019

.field public static final sys_third_menu_button_state:I = 0x7f02001a

.field public static final sys_third_menu_item_state:I = 0x7f02001b

.field public static final sysnetthirdswitch:I = 0x7f02001c

.field public static final syssettingfirstb:I = 0x7f02001d

.field public static final syssettingfirsticonfocus:I = 0x7f02001e

.field public static final syssettingfirsticonnetlastfocus:I = 0x7f02001f

.field public static final syssettingfirsticonnetunfocus:I = 0x7f020020

.field public static final syssettingfirsticonpersonallastfocus:I = 0x7f020021

.field public static final syssettingfirsticonpersonalunfocus:I = 0x7f020022

.field public static final syssettingfirsticonsavelastfocus:I = 0x7f020023

.field public static final syssettingfirsticonsaveunfocus:I = 0x7f020024

.field public static final syssettingfirsticonsyslastfocus:I = 0x7f020025

.field public static final syssettingfirsticonsysunfocus:I = 0x7f020026

.field public static final syssettingsecondfocus:I = 0x7f020027

.field public static final syssettingsecondnetbg:I = 0x7f020028

.field public static final syssettingsecondpersonalbg:I = 0x7f020029

.field public static final syssettingsecondstoragebg:I = 0x7f02002a

.field public static final syssettingsecondsysbg:I = 0x7f02002b

.field public static final syssettingthirdbg:I = 0x7f02002c

.field public static final syssettingthirditembottom:I = 0x7f02002d

.field public static final syssettingthirditembottomfocus:I = 0x7f02002e

.field public static final syssettingthirditemmid:I = 0x7f02002f

.field public static final syssettingthirditemmidfocus:I = 0x7f020030

.field public static final syssettingthirditemsingle:I = 0x7f020031

.field public static final syssettingthirditemsinglefocus:I = 0x7f020032

.field public static final syssettingthirditemtop:I = 0x7f020033

.field public static final syssettingthirditemtopfocus:I = 0x7f020034

.field public static final toast_bg:I = 0x7f020035

.field public static final wifi1:I = 0x7f020036

.field public static final wifi2:I = 0x7f020037

.field public static final wifi3:I = 0x7f020038

.field public static final wifi4:I = 0x7f020039

.field public static final wifilock:I = 0x7f02003a

.field public static final wps_bg:I = 0x7f02003b

.field public static final wps_button_focus:I = 0x7f02003c

.field public static final wps_button_nofocus:I = 0x7f02003d

.field public static final wps_button_selector:I = 0x7f02003e

.field public static final wps_progress:I = 0x7f02003f

.field public static final wps_progress_bg:I = 0x7f020040

.field public static final wps_progress_fg:I = 0x7f020041

.field public static final wps_toast_bg:I = 0x7f020042

.field public static final wps_toast_icon:I = 0x7f020043

.field public static final wps_wait:I = 0x7f020044

.field public static final zhtw_off:I = 0x7f020045

.field public static final zhtw_on:I = 0x7f020046


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
