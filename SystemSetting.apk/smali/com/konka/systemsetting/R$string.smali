.class public final Lcom/konka/systemsetting/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final accessibility_sync_disabled:I = 0x7f0600f6

.field public static final accessibility_sync_enabled:I = 0x7f0600f5

.field public static final accessibility_sync_error:I = 0x7f0600f7

.field public static final account_sync_settings_title:I = 0x7f0600f8

.field public static final active_device_admin_msg:I = 0x7f06014d

.field public static final add_account_label:I = 0x7f0600f9

.field public static final add_device_admin:I = 0x7f06014b

.field public static final add_device_admin_msg:I = 0x7f06014c

.field public static final app_name:I = 0x7f060000

.field public static final app_version:I = 0x7f06010d

.field public static final button_cancel:I = 0x7f06019a

.field public static final buttons_input:I = 0x7f060198

.field public static final buttons_ok:I = 0x7f060199

.field public static final buttons_set:I = 0x7f06019b

.field public static final camera:I = 0x7f06019e

.field public static final camera_preview_ok:I = 0x7f0601a6

.field public static final camera_title_show_bad:I = 0x7f0601a7

.field public static final camera_title_show_ok:I = 0x7f0601a8

.field public static final cant_sync_dialog_message:I = 0x7f0600eb

.field public static final cant_sync_dialog_title:I = 0x7f0600ea

.field public static final check_default:I = 0x7f060060

.field public static final check_default_failure:I = 0x7f060064

.field public static final check_dns_failure:I = 0x7f060066

.field public static final check_first_dns:I = 0x7f060062

.field public static final check_ip:I = 0x7f06005f

.field public static final check_ip_failure:I = 0x7f060063

.field public static final check_net_mask:I = 0x7f060061

.field public static final check_netmask_failure:I = 0x7f060065

.field public static final check_username_password:I = 0x7f0600a9

.field public static final common_exit:I = 0x7f060002

.field public static final common_no:I = 0x7f060004

.field public static final common_warning_title:I = 0x7f060005

.field public static final common_yes:I = 0x7f060003

.field public static final connected_ok:I = 0x7f0600d0

.field public static final dev0:I = 0x7f0601a4

.field public static final dev1:I = 0x7f0601a5

.field public static final device_address:I = 0x7f06017c

.field public static final device_admin_add_title:I = 0x7f06014f

.field public static final device_admin_status:I = 0x7f060149

.field public static final device_admin_warning:I = 0x7f06014a

.field public static final device_discover:I = 0x7f06017e

.field public static final device_list:I = 0x7f06017f

.field public static final device_name:I = 0x7f06017a

.field public static final device_search_failed:I = 0x7f060184

.field public static final device_search_success:I = 0x7f060185

.field public static final device_status:I = 0x7f06017b

.field public static final disconnected_now:I = 0x7f0600d2

.field public static final down:I = 0x7f06019d

.field public static final eth_able:I = 0x7f060111

.field public static final eth_connected:I = 0x7f06006c

.field public static final eth_connecting:I = 0x7f06010e

.field public static final eth_disconnected:I = 0x7f0600b3

.field public static final eth_disconnecting:I = 0x7f060110

.field public static final eth_enable:I = 0x7f0600b6

.field public static final eth_nocable:I = 0x7f060072

.field public static final eth_scan:I = 0x7f0600b5

.field public static final eth_suspended:I = 0x7f06010f

.field public static final eth_unkown:I = 0x7f0600b4

.field public static final exitUI:I = 0x7f0601a3

.field public static final finish_button_label:I = 0x7f0600ed

.field public static final gesture_help_button:I = 0x7f0601ab

.field public static final gesture_help_show:I = 0x7f0601ac

.field public static final gesture_init_0:I = 0x7f0601af

.field public static final gesture_init_1:I = 0x7f0601b0

.field public static final gesture_init_2:I = 0x7f0601b1

.field public static final gesture_init_3:I = 0x7f0601b2

.field public static final gesture_init_4:I = 0x7f0601b3

.field public static final gesture_init_5:I = 0x7f0601b4

.field public static final gesture_no_camera:I = 0x7f0601ad

.field public static final gesture_prograssbar_title:I = 0x7f0601ae

.field public static final header_add_an_account:I = 0x7f0600e4

.field public static final header_data_and_synchronization:I = 0x7f0600ee

.field public static final input_password:I = 0x7f0600a6

.field public static final input_username:I = 0x7f0600a5

.field public static final is_dialing:I = 0x7f0600a8

.field public static final lang_flag:I = 0x7f060001

.field public static final lock_settings_picker_title:I = 0x7f0600e5

.field public static final new_msg:I = 0x7f0600a2

.field public static final null_username_or_pwd:I = 0x7f0600aa

.field public static final operate_title_for_user_bad:I = 0x7f0601a9

.field public static final operate_title_for_user_ok:I = 0x7f0601aa

.field public static final password_error:I = 0x7f0600d1

.field public static final please_hangup_pppoe:I = 0x7f06018e

.field public static final please_insert_dongle:I = 0x7f06018f

.field public static final please_open_direct:I = 0x7f060186

.field public static final ppoe_dial_failed:I = 0x7f0600ac

.field public static final ppoe_dial_success:I = 0x7f0600ab

.field public static final pppoe_auto_failure:I = 0x7f0600a1

.field public static final pppoe_auto_success:I = 0x7f0600a0

.field public static final pppoe_cutdown:I = 0x7f06009e

.field public static final pppoe_dial:I = 0x7f06009d

.field public static final pppoe_dialing_warning:I = 0x7f06009f

.field public static final pppoe_disconnect:I = 0x7f0600a7

.field public static final pppoe_tips:I = 0x7f0600a3

.field public static final progress_scanning:I = 0x7f060135

.field public static final progress_tap_to_pair:I = 0x7f060147

.field public static final really_remove_account_message:I = 0x7f0600e7

.field public static final really_remove_account_title:I = 0x7f0600e6

.field public static final remove_account_failed:I = 0x7f0600e9

.field public static final remove_account_label:I = 0x7f0600e8

.field public static final remove_device_admin:I = 0x7f06014e

.field public static final runVts:I = 0x7f0601a1

.field public static final saveCali:I = 0x7f0601a0

.field public static final settings_license_activity_loading:I = 0x7f060151

.field public static final settings_license_activity_title:I = 0x7f060150

.field public static final settings_license_activity_unavailable:I = 0x7f060152

.field public static final shortcuts_set_energy:I = 0x7f060108

.field public static final shortcuts_set_input:I = 0x7f060109

.field public static final shortcuts_set_network:I = 0x7f06010a

.field public static final shortcuts_set_time:I = 0x7f06010b

.field public static final shortcuts_set_update:I = 0x7f06010c

.field public static final softap:I = 0x7f0600d8

.field public static final softap_band:I = 0x7f060193

.field public static final softap_config_success:I = 0x7f060102

.field public static final softap_detect:I = 0x7f0600fe

.field public static final softap_disabled:I = 0x7f060178

.field public static final softap_disabling:I = 0x7f060177

.field public static final softap_dnssvr:I = 0x7f0600fc

.field public static final softap_dongle_notfound:I = 0x7f060103

.field public static final softap_enabled:I = 0x7f060176

.field public static final softap_enabling:I = 0x7f060175

.field public static final softap_ipaddr:I = 0x7f0600fb

.field public static final softap_password:I = 0x7f0600dc

.field public static final softap_please_turnon_ap:I = 0x7f060105

.field public static final softap_pwd_notice:I = 0x7f060100

.field public static final softap_pwdhint:I = 0x7f0600df

.field public static final softap_security:I = 0x7f0600db

.field public static final softap_security_open:I = 0x7f0600e2

.field public static final softap_security_wpa:I = 0x7f0600e0

.field public static final softap_security_wpa2:I = 0x7f0600e1

.field public static final softap_set:I = 0x7f0600fd

.field public static final softap_showpwd:I = 0x7f0600dd

.field public static final softap_ssid:I = 0x7f0600da

.field public static final softap_ssid_empty:I = 0x7f0600ff

.field public static final softap_ssid_pwd_too_long:I = 0x7f060101

.field public static final softap_ssidhint:I = 0x7f0600de

.field public static final softap_switch:I = 0x7f0600d9

.field public static final softap_tipturnoffwifi:I = 0x7f060106

.field public static final softap_wiredpppoeconnecting:I = 0x7f060104

.field public static final startCali:I = 0x7f06019f

.field public static final stopVts:I = 0x7f0601a2

.field public static final str_app_install_position:I = 0x7f0600ba

.field public static final str_arr_standby_mode_deep:I = 0x7f060166

.field public static final str_arr_standby_mode_normal:I = 0x7f060165

.field public static final str_arr_standby_mode_quick:I = 0x7f060167

.field public static final str_cable_connected:I = 0x7f06006a

.field public static final str_cable_mount:I = 0x7f060070

.field public static final str_cable_network:I = 0x7f060056

.field public static final str_cable_unmount:I = 0x7f060071

.field public static final str_del_epg_schedule_msg:I = 0x7f060132

.field public static final str_eth_config_exists:I = 0x7f06006d

.field public static final str_eth_enable:I = 0x7f06006b

.field public static final str_eth_state_notactive:I = 0x7f06006f

.field public static final str_ethernet_state_disabled:I = 0x7f06006e

.field public static final str_individ_appsetting_allowed:I = 0x7f06001e

.field public static final str_individ_appsetting_appinstall:I = 0x7f06001c

.field public static final str_individ_appsetting_attention:I = 0x7f060020

.field public static final str_individ_appsetting_context:I = 0x7f060021

.field public static final str_individ_appsetting_notallowed:I = 0x7f06001f

.field public static final str_individ_appsetting_unknowapp:I = 0x7f06001d

.field public static final str_individ_com_switch_off:I = 0x7f06000f

.field public static final str_individ_com_switch_on:I = 0x7f06000e

.field public static final str_individ_dialog_cancel:I = 0x7f06000d

.field public static final str_individ_dialog_set:I = 0x7f06000c

.field public static final str_individ_energysv_item_backlight:I = 0x7f060026

.field public static final str_individ_energysv_item_energysaving:I = 0x7f060022

.field public static final str_individ_energysv_item_noactionstandby:I = 0x7f060025

.field public static final str_individ_energysv_item_nosignalstandby:I = 0x7f060024

.field public static final str_individ_energysv_item_screensaver:I = 0x7f060023

.field public static final str_individ_energysv_item_standbymode:I = 0x7f060168

.field public static final str_individ_gesture_recognition:I = 0x7f060163

.field public static final str_individ_gesture_service:I = 0x7f060164

.field public static final str_individ_timesetting_initing:I = 0x7f060010

.field public static final str_individ_timing_item_autoadjust:I = 0x7f060011

.field public static final str_individ_timing_item_boot_channel:I = 0x7f06001a

.field public static final str_individ_timing_item_boot_timezone:I = 0x7f06001b

.field public static final str_individ_timing_item_bootswitch:I = 0x7f060018

.field public static final str_individ_timing_item_boottime:I = 0x7f060019

.field public static final str_individ_timing_item_datesetting:I = 0x7f060012

.field public static final str_individ_timing_item_haltswitch:I = 0x7f060016

.field public static final str_individ_timing_item_halttime:I = 0x7f060017

.field public static final str_individ_timing_item_timesetting:I = 0x7f060013

.field public static final str_individ_timing_item_timezonesetting:I = 0x7f060015

.field public static final str_individ_timing_item_timezoneswitch:I = 0x7f060014

.field public static final str_individpage_energysaving:I = 0x7f06000b

.field public static final str_individpage_timesetting:I = 0x7f06000a

.field public static final str_input_android_input_setting:I = 0x7f06004f

.field public static final str_input_current:I = 0x7f06004e

.field public static final str_input_settings:I = 0x7f06004d

.field public static final str_ip_hint:I = 0x7f060130

.field public static final str_network_com_switch_off:I = 0x7f060055

.field public static final str_network_com_switch_on:I = 0x7f060054

.field public static final str_network_eth_set_done:I = 0x7f060069

.field public static final str_network_pppoe_autodial:I = 0x7f06009a

.field public static final str_network_pppoe_connectionmode:I = 0x7f060096

.field public static final str_network_pppoe_password:I = 0x7f060098

.field public static final str_network_pppoe_showpwd:I = 0x7f060099

.field public static final str_network_pppoe_username:I = 0x7f060097

.field public static final str_network_pppoe_wiredmode:I = 0x7f06009b

.field public static final str_network_pppoe_wirelessmode:I = 0x7f06009c

.field public static final str_network_wired_cancel:I = 0x7f06005d

.field public static final str_network_wired_defaultgateway:I = 0x7f06005a

.field public static final str_network_wired_defaultgateway_hint:I = 0x7f060197

.field public static final str_network_wired_detect:I = 0x7f06005e

.field public static final str_network_wired_dnsserver:I = 0x7f06005b

.field public static final str_network_wired_dnsserver_hint:I = 0x7f060195

.field public static final str_network_wired_ipaddr:I = 0x7f060057

.field public static final str_network_wired_ipaddr_hint:I = 0x7f060194

.field public static final str_network_wired_ipmode:I = 0x7f060058

.field public static final str_network_wired_set:I = 0x7f06005c

.field public static final str_network_wired_subnetmask:I = 0x7f060059

.field public static final str_network_wired_subnetmask_hint:I = 0x7f060196

.field public static final str_networkpage_pppoe:I = 0x7f060053

.field public static final str_networkpage_wifidirect:I = 0x7f060173

.field public static final str_networkpage_wired:I = 0x7f060051

.field public static final str_networkpage_wireless:I = 0x7f060052

.field public static final str_root_alert_dialog_cancel:I = 0x7f060134

.field public static final str_root_alert_dialog_confirm:I = 0x7f060133

.field public static final str_root_alert_dialog_title:I = 0x7f060131

.field public static final str_storage_flash:I = 0x7f0600b7

.field public static final str_storage_flash_freespace:I = 0x7f0600be

.field public static final str_storage_flash_usedspace:I = 0x7f0600bd

.field public static final str_storage_sd:I = 0x7f0600bc

.field public static final str_storage_sdcard:I = 0x7f0600b8

.field public static final str_storage_sdcard_format:I = 0x7f0600c0

.field public static final str_storage_sdcard_freespace:I = 0x7f0600c2

.field public static final str_storage_sdcard_unmount:I = 0x7f0600bf

.field public static final str_storage_sdcard_usedspace:I = 0x7f0600c1

.field public static final str_storage_totalspace:I = 0x7f0600c3

.field public static final str_sys_main_individ:I = 0x7f060008

.field public static final str_sys_main_network:I = 0x7f060006

.field public static final str_sys_main_storage:I = 0x7f060009

.field public static final str_sys_main_system:I = 0x7f060007

.field public static final str_system_alertdialog_button_cancle:I = 0x7f060045

.field public static final str_system_alertdialog_button_set:I = 0x7f060044

.field public static final str_system_item_bootingmusic:I = 0x7f06004c

.field public static final str_system_item_com_switch_off:I = 0x7f060047

.field public static final str_system_item_com_switch_on:I = 0x7f060046

.field public static final str_system_item_currlanguage:I = 0x7f06004a

.field public static final str_system_item_mic:I = 0x7f06004b

.field public static final str_system_language_title:I = 0x7f060049

.field public static final str_systempage_disclaimer:I = 0x7f060029

.field public static final str_systempage_factoryreset:I = 0x7f06002a

.field public static final str_systempage_languageandmusic:I = 0x7f060048

.field public static final str_systempage_reset_caution:I = 0x7f06016a

.field public static final str_systempage_reset_dialogmsg:I = 0x7f060169

.field public static final str_systempage_reset_query:I = 0x7f06002e

.field public static final str_systempage_safetyandsoftupgrade:I = 0x7f060028

.field public static final str_systempage_systeminfo:I = 0x7f060027

.field public static final str_systempage_unknow_sources:I = 0x7f06002b

.field public static final str_systempage_upgrade_byusb:I = 0x7f06002d

.field public static final str_systempage_upgrade_online:I = 0x7f06002c

.field public static final str_title_app_install_position:I = 0x7f0600bb

.field public static final string_dot:I = 0x7f060116

.field public static final sync_disabled:I = 0x7f0600f3

.field public static final sync_enabled:I = 0x7f0600f2

.field public static final sync_error:I = 0x7f0600f4

.field public static final sync_is_failing:I = 0x7f0600ec

.field public static final sync_item_title:I = 0x7f0600f1

.field public static final sync_menu_sync_cancel:I = 0x7f0600f0

.field public static final sync_menu_sync_now:I = 0x7f0600ef

.field public static final sync_one_time_sync:I = 0x7f0600fa

.field public static final sys_individ_msg_switcher:I = 0x7f060162

.field public static final sys_network_wifidirect_autoconnect:I = 0x7f060188

.field public static final sys_network_wifidirect_item_operate_str_devicelist:I = 0x7f060189

.field public static final sys_network_wifidirect_str_connected:I = 0x7f060191

.field public static final sys_network_wifidirect_str_device:I = 0x7f060190

.field public static final sys_network_wifidirect_str_disconnected:I = 0x7f060192

.field public static final sys_network_wifidirect_tips:I = 0x7f060187

.field public static final sys_network_wifidirect_tv_cancel:I = 0x7f06018c

.field public static final sys_network_wifidirect_tv_confirm:I = 0x7f06018b

.field public static final sys_network_wifidirect_tv_nullname:I = 0x7f06018d

.field public static final sys_network_wifidirect_tv_rename:I = 0x7f06018a

.field public static final sysinfo_android_version:I = 0x7f060035

.field public static final sysinfo_build_in:I = 0x7f060041

.field public static final sysinfo_cpu:I = 0x7f060030

.field public static final sysinfo_cpu_value:I = 0x7f060031

.field public static final sysinfo_cpu_value_2992:I = 0x7f06016f

.field public static final sysinfo_cpu_value_800:I = 0x7f06016d

.field public static final sysinfo_cpu_value_801:I = 0x7f06016c

.field public static final sysinfo_cpu_value_818:I = 0x7f06016e

.field public static final sysinfo_cpu_value_901:I = 0x7f06016b

.field public static final sysinfo_emmc:I = 0x7f060033

.field public static final sysinfo_factory_info:I = 0x7f06003b

.field public static final sysinfo_firmware_version:I = 0x7f060170

.field public static final sysinfo_macaddr:I = 0x7f06003e

.field public static final sysinfo_other_info:I = 0x7f06003f

.field public static final sysinfo_panel_info:I = 0x7f060037

.field public static final sysinfo_panel_level:I = 0x7f060039

.field public static final sysinfo_panel_level_value:I = 0x7f06003a

.field public static final sysinfo_platform_info:I = 0x7f06002f

.field public static final sysinfo_ram:I = 0x7f060032

.field public static final sysinfo_resulotion:I = 0x7f060038

.field public static final sysinfo_serialnum:I = 0x7f06003d

.field public static final sysinfo_soundbox:I = 0x7f060042

.field public static final sysinfo_soundbox_value:I = 0x7f060043

.field public static final sysinfo_system_info:I = 0x7f060034

.field public static final sysinfo_tv_version:I = 0x7f060036

.field public static final sysinfo_type:I = 0x7f06003c

.field public static final sysinfo_wifi_external:I = 0x7f060172

.field public static final sysinfo_wifi_info:I = 0x7f060040

.field public static final sysinfo_wifi_internal:I = 0x7f060171

.field public static final up:I = 0x7f06019c

.field public static final update_packages_download:I = 0x7f0600a4

.field public static final usbupgrade_cancel:I = 0x7f0600d6

.field public static final usbupgrade_exec:I = 0x7f0600d7

.field public static final usbupgrade_filenotfound:I = 0x7f0600d4

.field public static final usbupgrade_nodevice:I = 0x7f0600d3

.field public static final usbupgrade_reboot:I = 0x7f0600d5

.field public static final warning_formate_complete:I = 0x7f0600c8

.field public static final warning_formating:I = 0x7f0600c7

.field public static final warning_mounted:I = 0x7f0600c4

.field public static final warning_no_sdcard:I = 0x7f0600b9

.field public static final warning_requestreboot:I = 0x7f060050

.field public static final warning_unmount_complete:I = 0x7f0600c6

.field public static final warning_unmounting:I = 0x7f0600c5

.field public static final wifi:I = 0x7f060073

.field public static final wifi_ap_disabled:I = 0x7f06012d

.field public static final wifi_ap_disabling:I = 0x7f06012c

.field public static final wifi_ap_enabled:I = 0x7f06012b

.field public static final wifi_ap_enabling:I = 0x7f06012a

.field public static final wifi_authenticating:I = 0x7f06008f

.field public static final wifi_btn_cancel:I = 0x7f06007c

.field public static final wifi_btn_connect:I = 0x7f060079

.field public static final wifi_btn_detect:I = 0x7f060075

.field public static final wifi_btn_forget:I = 0x7f0600b0

.field public static final wifi_btn_save:I = 0x7f0600af

.field public static final wifi_btn_scan:I = 0x7f06007a

.field public static final wifi_btn_set:I = 0x7f06007b

.field public static final wifi_cancel:I = 0x7f06015e

.field public static final wifi_connect_blocked:I = 0x7f060095

.field public static final wifi_connect_failed:I = 0x7f060094

.field public static final wifi_connected:I = 0x7f060091

.field public static final wifi_connecting:I = 0x7f06008e

.field public static final wifi_device_mount:I = 0x7f060083

.field public static final wifi_device_nofound:I = 0x7f060085

.field public static final wifi_device_unmount:I = 0x7f060084

.field public static final wifi_diabling:I = 0x7f060080

.field public static final wifi_direct_cancel:I = 0x7f060182

.field public static final wifi_direct_connect:I = 0x7f060180

.field public static final wifi_direct_disconnect:I = 0x7f060181

.field public static final wifi_direct_search:I = 0x7f060183

.field public static final wifi_disabled:I = 0x7f060082

.field public static final wifi_disabling:I = 0x7f060081

.field public static final wifi_disconnected:I = 0x7f060093

.field public static final wifi_disconnecting:I = 0x7f060092

.field public static final wifi_display:I = 0x7f0601b5

.field public static final wifi_display_available_devices:I = 0x7f06013a

.field public static final wifi_display_details:I = 0x7f060143

.field public static final wifi_display_disconnect_text:I = 0x7f060140

.field public static final wifi_display_disconnect_title:I = 0x7f060141

.field public static final wifi_display_no_devices_found:I = 0x7f060148

.field public static final wifi_display_options_done:I = 0x7f060145

.field public static final wifi_display_options_forget:I = 0x7f060142

.field public static final wifi_display_options_name:I = 0x7f060144

.field public static final wifi_display_options_title:I = 0x7f060146

.field public static final wifi_display_paired_devices:I = 0x7f060138

.field public static final wifi_display_search_for_devices:I = 0x7f060139

.field public static final wifi_display_searching_for_devices:I = 0x7f060137

.field public static final wifi_display_settings_empty_list_wifi_display_disabled:I = 0x7f06013c

.field public static final wifi_display_settings_empty_list_wifi_display_off:I = 0x7f06013b

.field public static final wifi_display_settings_title:I = 0x7f060136

.field public static final wifi_display_status_available:I = 0x7f06013f

.field public static final wifi_display_status_connected:I = 0x7f06013d

.field public static final wifi_display_status_connecting:I = 0x7f06013e

.field public static final wifi_display_tips_open:I = 0x7f0601b8

.field public static final wifi_display_tips_opened:I = 0x7f0601b9

.field public static final wifi_display_tips_wifi_not_open:I = 0x7f0601b7

.field public static final wifi_display_tv_name:I = 0x7f0601b6

.field public static final wifi_emptypwd:I = 0x7f0600cf

.field public static final wifi_enabled:I = 0x7f06007f

.field public static final wifi_enabling:I = 0x7f06007e

.field public static final wifi_list_multipage:I = 0x7f06012f

.field public static final wifi_list_singlepage:I = 0x7f06012e

.field public static final wifi_listpage_counter:I = 0x7f0600e3

.field public static final wifi_net_wired_dog:I = 0x7f0601ba

.field public static final wifi_noactivenetwork:I = 0x7f060087

.field public static final wifi_obtaining_ipaddr:I = 0x7f060090

.field public static final wifi_pwd_error:I = 0x7f0600ce

.field public static final wifi_pwdemptyerror:I = 0x7f06008b

.field public static final wifi_reachable:I = 0x7f060089

.field public static final wifi_scan:I = 0x7f06008c

.field public static final wifi_scanning:I = 0x7f06008d

.field public static final wifi_scanresult:I = 0x7f060086

.field public static final wifi_security:I = 0x7f060076

.field public static final wifi_showpwd:I = 0x7f060078

.field public static final wifi_signal_less:I = 0x7f0600cb

.field public static final wifi_signal_normal:I = 0x7f0600ca

.field public static final wifi_signal_strength:I = 0x7f060077

.field public static final wifi_signal_strong:I = 0x7f0600cc

.field public static final wifi_signal_weak:I = 0x7f0600c9

.field public static final wifi_ssid_connected:I = 0x7f0600cd

.field public static final wifi_state_unknown:I = 0x7f060179

.field public static final wifi_suspended:I = 0x7f060088

.field public static final wifi_tipturnoffsoftap:I = 0x7f060107

.field public static final wifi_title:I = 0x7f060074

.field public static final wifi_unkown:I = 0x7f06007d

.field public static final wifi_unreachable:I = 0x7f06008a

.field public static final wifi_usingsecondcard:I = 0x7f060174

.field public static final wifi_wps_complete:I = 0x7f060157

.field public static final wifi_wps_connected:I = 0x7f06015f

.field public static final wifi_wps_failed_generic:I = 0x7f06015c

.field public static final wifi_wps_failed_overlap:I = 0x7f060158

.field public static final wifi_wps_failed_tkip:I = 0x7f06015a

.field public static final wifi_wps_failed_wep:I = 0x7f060159

.field public static final wifi_wps_in_progress:I = 0x7f06015b

.field public static final wifi_wps_onstart_pbc:I = 0x7f060156

.field public static final wifi_wps_onstart_pin:I = 0x7f060155

.field public static final wifi_wps_setup_msg:I = 0x7f06015d

.field public static final wifi_wps_success:I = 0x7f060160

.field public static final wificonf_advancedoption:I = 0x7f060114

.field public static final wificonf_linkspeed:I = 0x7f060112

.field public static final wificonf_password:I = 0x7f0600b1

.field public static final wificonf_security:I = 0x7f060113

.field public static final wificonf_showpwd:I = 0x7f0600b2

.field public static final wificonf_signalstrength:I = 0x7f0600ad

.field public static final wifiip_address:I = 0x7f060120

.field public static final wifiip_addresshint:I = 0x7f060121

.field public static final wifiip_dns1:I = 0x7f060126

.field public static final wifiip_dns1hint:I = 0x7f060127

.field public static final wifiip_dns2:I = 0x7f060128

.field public static final wifiip_dns2hint:I = 0x7f060129

.field public static final wifiip_gateway:I = 0x7f060122

.field public static final wifiip_gatewayhint:I = 0x7f060123

.field public static final wifiip_mode_dhcp:I = 0x7f06011e

.field public static final wifiip_mode_static:I = 0x7f06011f

.field public static final wifiip_netprefixlen:I = 0x7f060124

.field public static final wifiip_netprefixlenhint:I = 0x7f060125

.field public static final wifiip_title:I = 0x7f0600ae

.field public static final wifiproxy_exception:I = 0x7f06011c

.field public static final wifiproxy_exceptionhint:I = 0x7f06011d

.field public static final wifiproxy_host:I = 0x7f060118

.field public static final wifiproxy_hosthint:I = 0x7f060119

.field public static final wifiproxy_port:I = 0x7f06011a

.field public static final wifiproxy_porthint:I = 0x7f06011b

.field public static final wifiproxy_statement:I = 0x7f060117

.field public static final wifiproxy_title:I = 0x7f060115

.field public static final wire_con_failure:I = 0x7f060067

.field public static final wire_has_wire:I = 0x7f060068

.field public static final wlan_direct_device:I = 0x7f06017d

.field public static final wps_cancel:I = 0x7f060153

.field public static final wps_connection:I = 0x7f060161

.field public static final wps_ok:I = 0x7f060154


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
