.class Lcom/konka/systemsetting/MainActivity$1;
.super Ljava/lang/Object;
.source "MainActivity.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/MainActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/MainActivity;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/MainActivity$1;->this$0:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1    # Landroid/content/ComponentName;
    .param p2    # Landroid/os/IBinder;

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$1;->this$0:Lcom/konka/systemsetting/MainActivity;

    invoke-static {p2}, Lcom/konka/passport/service/UserInfo$Stub;->asInterface(Landroid/os/IBinder;)Lcom/konka/passport/service/UserInfo;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$0(Lcom/konka/systemsetting/MainActivity;Lcom/konka/passport/service/UserInfo;)V

    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$1;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->iPerson:Lcom/konka/passport/service/UserInfo;
    invoke-static {v1}, Lcom/konka/systemsetting/MainActivity;->access$1(Lcom/konka/systemsetting/MainActivity;)Lcom/konka/passport/service/UserInfo;

    move-result-object v1

    if-eqz v1, :cond_0

    :try_start_0
    iget-object v1, p0, Lcom/konka/systemsetting/MainActivity$1;->this$0:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$1;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->iPerson:Lcom/konka/passport/service/UserInfo;
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$1(Lcom/konka/systemsetting/MainActivity;)Lcom/konka/passport/service/UserInfo;

    move-result-object v2

    invoke-interface {v2}, Lcom/konka/passport/service/UserInfo;->GetSerialWithCRC()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/MainActivity;->access$2(Lcom/konka/systemsetting/MainActivity;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "get the SN"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/MainActivity$1;->this$0:Lcom/konka/systemsetting/MainActivity;

    # getter for: Lcom/konka/systemsetting/MainActivity;->gstrSN:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/systemsetting/MainActivity;->access$3(Lcom/konka/systemsetting/MainActivity;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0

    :cond_0
    const-string v1, "get the UserInfo failed"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 0
    .param p1    # Landroid/content/ComponentName;

    return-void
.end method
