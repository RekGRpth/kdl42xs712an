.class public Lcom/konka/systemsetting/shortcuts/TimeSet;
.super Landroid/app/Activity;
.source "TimeSet.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    const v0, 0x7f03000a    # com.konka.systemsetting.R.layout.short_cut_view

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/shortcuts/TimeSet;->setContentView(I)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/TimeSet;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f06010b    # com.konka.systemsetting.R.string.shortcuts_set_time

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-class v1, Lcom/konka/systemsetting/shortcuts/MenuName$SystemSetting;

    const-string v2, "individual_timing"

    invoke-static {v1, v2}, Lcom/konka/systemsetting/shortcuts/ShortCutsManager;->creatOpenIntent(Ljava/lang/Class;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const v2, 0x7f020016    # com.konka.systemsetting.R.drawable.shortcut_icon_timing

    invoke-static {p0, v0, v1, v2}, Lcom/konka/systemsetting/shortcuts/ShortCutsManager;->addShortCutToLauncher(Landroid/app/Activity;Ljava/lang/String;Landroid/content/Intent;I)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/shortcuts/TimeSet;->finish()V

    return-void
.end method
