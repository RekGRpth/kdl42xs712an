.class Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;
.super Ljava/lang/Object;
.source "StorageAppInstallPosition.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # getter for: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$3(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # getter for: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$3(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # getter for: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$3(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_storagePgaeManager:Lcom/konka/systemsetting/storage/StoragePageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/storage/StoragePageManager;->setLastFocus(II)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # getter for: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$4(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Landroid/widget/LinearLayout;

    move-result-object v0

    const v1, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;->this$0:Lcom/konka/systemsetting/storage/StorageAppInstallPosition;

    # getter for: Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->access$4(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Landroid/widget/LinearLayout;

    move-result-object v0

    const v1, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setBackgroundResource(I)V

    goto :goto_0
.end method
