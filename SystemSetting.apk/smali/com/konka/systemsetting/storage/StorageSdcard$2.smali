.class Lcom/konka/systemsetting/storage/StorageSdcard$2;
.super Ljava/lang/Object;
.source "StorageSdcard.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/storage/StorageSdcard;->setOnClickListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/storage/StorageSdcard;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/storage/StorageSdcard;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v0, v1}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$6(Lcom/konka/systemsetting/storage/StorageSdcard;I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->m_currItemId:I
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$7(Lcom/konka/systemsetting/storage/StorageSdcard;)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$0(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    const v1, 0x7f0600c5    # com.konka.systemsetting.R.string.warning_unmounting

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # invokes: Lcom/konka/systemsetting/storage/StorageSdcard;->unmountSDCard()V
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$8(Lcom/konka/systemsetting/storage/StorageSdcard;)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # getter for: Lcom/konka/systemsetting/storage/StorageSdcard;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$0(Lcom/konka/systemsetting/storage/StorageSdcard;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    const v1, 0x7f0600c7    # com.konka.systemsetting.R.string.warning_formating

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageSdcard$2;->this$0:Lcom/konka/systemsetting/storage/StorageSdcard;

    # invokes: Lcom/konka/systemsetting/storage/StorageSdcard;->formatSDCard()V
    invoke-static {v0}, Lcom/konka/systemsetting/storage/StorageSdcard;->access$9(Lcom/konka/systemsetting/storage/StorageSdcard;)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090080
        :pswitch_0    # com.konka.systemsetting.R.id.sys_storage_sdcard_unmount
        :pswitch_1    # com.konka.systemsetting.R.id.sys_storage_sdcard_format
    .end packed-switch
.end method
