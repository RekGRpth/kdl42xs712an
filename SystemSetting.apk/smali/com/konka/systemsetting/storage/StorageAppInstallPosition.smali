.class public Lcom/konka/systemsetting/storage/StorageAppInstallPosition;
.super Ljava/lang/Object;
.source "StorageAppInstallPosition.java"


# instance fields
.field protected final POSITION_EXTERNAL_STORAGE:I

.field protected final POSITION_INTERNAL_STORAGE:I

.field private language_values:[Ljava/lang/CharSequence;

.field private linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private m_iCurAppInstallPosition:I


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 5
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v1, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->language_values:[Ljava/lang/CharSequence;

    iput v3, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->POSITION_INTERNAL_STORAGE:I

    iput v4, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->POSITION_EXTERNAL_STORAGE:I

    iput v3, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    iput-object p1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    sget-boolean v1, Lcom/konka/systemsetting/SysRootApp;->isSDCardSupportable:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000a    # com.konka.systemsetting.R.array.str_arr_app_install_position_for_sd

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->language_values:[Ljava/lang/CharSequence;

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_app_location_type"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    iput v4, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    :goto_1
    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->initUIComponents()V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07000b    # com.konka.systemsetting.R.array.str_arr_app_install_position_for_usb

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->language_values:[Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    if-ne v0, v4, :cond_2

    iput v3, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    goto :goto_1

    :cond_2
    iput v3, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    goto :goto_1
.end method

.method private SetOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$2;-><init>(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$4;-><init>(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private SetOnKeyListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$1;-><init>(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->showLanguageSettingDialog()V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->updateUI()V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method private initUIComponents()V
    .locals 3

    const v2, 0x7f09007b    # com.konka.systemsetting.R.id.sys_storage_app_install_position

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    const v1, 0x7f090087    # com.konka.systemsetting.R.id.linearlayout_storage_tab_app_install_position

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->updateUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->SetOnKeyListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->SetOnClickListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->SetOnFocusChangeListener()V

    return-void
.end method

.method private setTextCurrentLang()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    if-nez v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->linearlayout_itemAppInstallPosition:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->language_values:[Ljava/lang/CharSequence;

    iget v2, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    aget-object v1, v1, v2

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private showLanguageSettingDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0600bb    # com.konka.systemsetting.R.string.str_title_app_install_position

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020026    # com.konka.systemsetting.R.drawable.syssettingfirsticonsysunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition$3;-><init>(Lcom/konka/systemsetting/storage/StorageAppInstallPosition;)V

    iget-object v2, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->language_values:[Ljava/lang/CharSequence;

    iget v3, p0, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->m_iCurAppInstallPosition:I

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private updateUI()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/storage/StorageAppInstallPosition;->setTextCurrentLang()V

    return-void
.end method
