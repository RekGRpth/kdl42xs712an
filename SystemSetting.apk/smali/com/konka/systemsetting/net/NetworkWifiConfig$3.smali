.class Lcom/konka/systemsetting/net/NetworkWifiConfig$3;
.super Ljava/lang/Object;
.source "NetworkWifiConfig.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemOnClick(I)Z

    move-result v0

    goto :goto_0

    :sswitch_1
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$7(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$0(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v0

    if-lez v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$8(Lcom/konka/systemsetting/net/NetworkWifiConfig;)[Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$9(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_1
    move v0, v1

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$10(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_1

    :sswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$11(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/Button;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    move v0, v1

    goto :goto_0

    :cond_2
    :sswitch_4
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    sparse-switch v2, :sswitch_data_2

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$5(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    move v0, v1

    goto :goto_0

    :sswitch_6
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$7(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/WifiManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$0(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v0

    if-lez v0, :cond_3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$8(Lcom/konka/systemsetting/net/NetworkWifiConfig;)[Landroid/widget/LinearLayout;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$9(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v2

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    :goto_2
    move v0, v1

    goto/16 :goto_0

    :cond_3
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;
    invoke-static {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$5(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/LinearLayout;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_2

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_4
        0x14 -> :sswitch_1
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x7f09006e -> :sswitch_2    # com.konka.systemsetting.R.id.sys_network_wireless_item_wifiswitch
        0x7f090079 -> :sswitch_3    # com.konka.systemsetting.R.id.sys_network_wireless_item_detect
    .end sparse-switch

    :sswitch_data_2
    .sparse-switch
        0x7f09006e -> :sswitch_5    # com.konka.systemsetting.R.id.sys_network_wireless_item_wifiswitch
        0x7f090078 -> :sswitch_6    # com.konka.systemsetting.R.id.sys_network_wireless_item_scan
    .end sparse-switch
.end method
