.class Lcom/konka/systemsetting/net/IPEditer$2;
.super Ljava/lang/Object;
.source "IPEditer.java"

# interfaces
.implements Landroid/text/TextWatcher;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/IPEditer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/IPEditer;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/IPEditer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/IPEditer$2;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public afterTextChanged(Landroid/text/Editable;)V
    .locals 3
    .param p1    # Landroid/text/Editable;

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer$2;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    invoke-interface {p1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/IPEditer;->access$1(Lcom/konka/systemsetting/net/IPEditer;Ljava/lang/String;)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "current "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/IPEditer$2;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # getter for: Lcom/konka/systemsetting/net/IPEditer;->strIP:Ljava/lang/String;
    invoke-static {v2}, Lcom/konka/systemsetting/net/IPEditer;->access$2(Lcom/konka/systemsetting/net/IPEditer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/IPEditer$2;->this$0:Lcom/konka/systemsetting/net/IPEditer;

    # invokes: Lcom/konka/systemsetting/net/IPEditer;->updateDotsPos()V
    invoke-static {v0}, Lcom/konka/systemsetting/net/IPEditer;->access$3(Lcom/konka/systemsetting/net/IPEditer;)V

    return-void
.end method

.method public beforeTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method

.method public onTextChanged(Ljava/lang/CharSequence;III)V
    .locals 0
    .param p1    # Ljava/lang/CharSequence;
    .param p2    # I
    .param p3    # I
    .param p4    # I

    return-void
.end method
