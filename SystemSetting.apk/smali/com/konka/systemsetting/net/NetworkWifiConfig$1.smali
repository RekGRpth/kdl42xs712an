.class Lcom/konka/systemsetting/net/NetworkWifiConfig$1;
.super Ljava/lang/Object;
.source "NetworkWifiConfig.java"

# interfaces
.implements Landroid/view/View$OnGenericMotionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onGenericMotion(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6
    .param p1    # Landroid/view/View;
    .param p2    # Landroid/view/MotionEvent;

    const/4 v2, 0x1

    const/4 v5, 0x0

    const/4 v1, 0x0

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$0(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    :goto_0
    return v1

    :cond_1
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v3

    const/16 v4, 0x8

    if-ne v3, v4, :cond_0

    const/4 v0, 0x0

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getMetaState()I

    move-result v3

    and-int/lit8 v3, v3, 0x1

    if-nez v3, :cond_0

    const/16 v3, 0x9

    invoke-virtual {p2, v3}, Landroid/view/MotionEvent;->getAxisValue(I)F

    move-result v0

    cmpl-float v3, v0, v5

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-static {v3, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$1(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V

    cmpl-float v3, v0, v5

    if-lez v3, :cond_2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpOnUpKeyDown()V
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$2(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    :goto_1
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-static {v3, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$1(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V

    move v1, v2

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # invokes: Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpOnDownKeyDown()V
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$3(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    goto :goto_1
.end method
