.class public Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;
.super Ljava/lang/Object;
.source "NetworkSoftApViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnClickEvent;,
        Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;,
        Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;
    }
.end annotation


# instance fields
.field private TAG:Ljava/lang/String;

.field public etPassword:Landroid/widget/EditText;

.field public etSSID:Landroid/widget/EditText;

.field public itemDetect:Landroid/widget/Button;

.field public itemDnsSvr:Landroid/widget/LinearLayout;

.field public itemIpAddr:Landroid/widget/LinearLayout;

.field public itemPassword:Landroid/widget/LinearLayout;

.field public itemSSID:Landroid/widget/LinearLayout;

.field public itemSecurity:Landroid/widget/LinearLayout;

.field public itemSet:Landroid/widget/Button;

.field public itemShowPwd:Landroid/widget/LinearLayout;

.field public itemSwitch:Landroid/widget/LinearLayout;

.field mContext:Lcom/konka/systemsetting/MainActivity;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 1
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "NET_WIFI_AP ==>"

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->TAG:Ljava/lang/String;

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->initUIComponents()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->setListeners()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->getFocusdItemBGById(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;I)I
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->getUnfocusdItemBGById(I)I

    move-result v0

    return v0
.end method

.method private enableEdit(Landroid/widget/EditText;Z)V
    .locals 1
    .param p1    # Landroid/widget/EditText;
    .param p2    # Z

    if-eqz p2, :cond_0

    const v0, -0xcdcdce

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/EditText;->setTextColor(I)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/EditText;->setEnabled(Z)V

    return-void

    :cond_0
    const v0, -0x515152

    goto :goto_0
.end method

.method private getFocusdItemBGById(I)I
    .locals 1
    .param p1    # I

    const v0, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const v0, 0x7f020034    # com.konka.systemsetting.R.drawable.syssettingthirditemtopfocus

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020030    # com.konka.systemsetting.R.drawable.syssettingthirditemmidfocus

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090046
        :pswitch_0    # com.konka.systemsetting.R.id.softap_switch
        :pswitch_1    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_2    # com.konka.systemsetting.R.id.softap_security
        :pswitch_2    # com.konka.systemsetting.R.id.softap_password
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_password
        :pswitch_2    # com.konka.systemsetting.R.id.softap_showpwd
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_dnssvr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_set
        :pswitch_0    # com.konka.systemsetting.R.id.softap_detect
    .end packed-switch
.end method

.method private getUnfocusdItemBGById(I)I
    .locals 1
    .param p1    # I

    const v0, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const v0, 0x7f020033    # com.konka.systemsetting.R.drawable.syssettingthirditemtop

    goto :goto_0

    :pswitch_2
    const v0, 0x7f02002f    # com.konka.systemsetting.R.drawable.syssettingthirditemmid

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x7f090046
        :pswitch_0    # com.konka.systemsetting.R.id.softap_switch
        :pswitch_1    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_2    # com.konka.systemsetting.R.id.softap_security
        :pswitch_2    # com.konka.systemsetting.R.id.softap_password
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_password
        :pswitch_2    # com.konka.systemsetting.R.id.softap_showpwd
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_dnssvr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_set
        :pswitch_0    # com.konka.systemsetting.R.id.softap_detect
    .end packed-switch
.end method

.method private initUIComponents()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090046    # com.konka.systemsetting.R.id.softap_switch

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090047    # com.konka.systemsetting.R.id.softap_ssid

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090049    # com.konka.systemsetting.R.id.softap_security

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09004a    # com.konka.systemsetting.R.id.softap_password

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09004c    # com.konka.systemsetting.R.id.softap_showpwd

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09004d    # com.konka.systemsetting.R.id.softap_ipaddr

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemIpAddr:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09004e    # com.konka.systemsetting.R.id.softap_dnssvr

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDnsSvr:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09004f    # com.konka.systemsetting.R.id.softap_set

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090050    # com.konka.systemsetting.R.id.softap_detect

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDetect:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    const v2, 0x7f090048    # com.konka.systemsetting.R.id.et_softap_ssid

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    const v2, 0x7f09004b    # com.konka.systemsetting.R.id.et_softap_password

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->showIPAddrAndDNS()V

    return-void
.end method

.method private setListeners()V
    .locals 7

    const v6, 0x7f090049    # com.konka.systemsetting.R.id.softap_security

    const v5, 0x7f09003f    # com.konka.systemsetting.R.id.tab_softap

    new-instance v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v1}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDetect:Landroid/widget/Button;

    invoke-virtual {v3, v1}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    new-instance v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDetect:Landroid/widget/Button;

    invoke-virtual {v3, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnClickEvent;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnClickEvent;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSSID:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemPassword:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDetect:Landroid/widget/Button;

    invoke-virtual {v3, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    new-instance v4, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;

    invoke-direct {v4, p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$1;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    new-instance v4, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$2;

    invoke-direct {v4, p0}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$2;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSecurity:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v5}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemSet:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDetect:Landroid/widget/Button;

    invoke-virtual {v3, v5}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    const v4, 0x7f090046    # com.konka.systemsetting.R.id.softap_switch

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v3, v6}, Landroid/widget/EditText;->setNextFocusUpId(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    const v4, 0x7f09004c    # com.konka.systemsetting.R.id.softap_showpwd

    invoke-virtual {v3, v4}, Landroid/widget/EditText;->setNextFocusDownId(I)V

    return-void
.end method


# virtual methods
.method public enableItem(Landroid/widget/Button;Z)V
    .locals 1
    .param p1    # Landroid/widget/Button;
    .param p2    # Z

    if-eqz p2, :cond_0

    const v0, -0xcdcdce

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/Button;->setTextColor(I)V

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/Button;->setEnabled(Z)V

    return-void

    :cond_0
    const v0, -0x515152

    goto :goto_0
.end method

.method public enableItem(Landroid/widget/LinearLayout;Z)V
    .locals 4
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    invoke-virtual {p1}, Landroid/widget/LinearLayout;->getId()I

    move-result v1

    if-eqz p2, :cond_1

    const v0, -0xcdcdce

    :goto_0
    const/4 v3, 0x0

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setTextColor(I)V

    packed-switch v1, :pswitch_data_0

    :goto_1
    :pswitch_0
    const v3, 0x7f090046    # com.konka.systemsetting.R.id.softap_switch

    if-eq v1, v3, :cond_0

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    :cond_0
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    return-void

    :cond_1
    const v0, -0x515152

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-direct {p0, v3, p2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableEdit(Landroid/widget/EditText;Z)V

    goto :goto_1

    :pswitch_2
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-direct {p0, v3, p2}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->enableEdit(Landroid/widget/EditText;Z)V

    goto :goto_1

    :pswitch_3
    const/4 v3, 0x1

    invoke-virtual {p1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f090047
        :pswitch_1    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_3    # com.konka.systemsetting.R.id.softap_security
        :pswitch_2    # com.konka.systemsetting.R.id.softap_password
    .end packed-switch
.end method

.method public itemOnClick(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    packed-switch p1, :pswitch_data_0

    :pswitch_0
    const/4 v0, 0x0

    :pswitch_1
    return v0

    :pswitch_data_0
    .packed-switch 0x7f090046
        :pswitch_1    # com.konka.systemsetting.R.id.softap_switch
        :pswitch_1    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_1    # com.konka.systemsetting.R.id.softap_security
        :pswitch_1    # com.konka.systemsetting.R.id.softap_password
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_password
        :pswitch_1    # com.konka.systemsetting.R.id.softap_showpwd
        :pswitch_0    # com.konka.systemsetting.R.id.softap_ipaddr
        :pswitch_0    # com.konka.systemsetting.R.id.softap_dnssvr
        :pswitch_1    # com.konka.systemsetting.R.id.softap_set
        :pswitch_1    # com.konka.systemsetting.R.id.softap_detect
    .end packed-switch
.end method

.method public itemOnKeyDown(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public itemOnKeyUp(I)Z
    .locals 1
    .param p1    # I

    const/4 v0, 0x1

    return v0
.end method

.method public showIPAddrAndDNS()V
    .locals 6

    const/4 v5, 0x1

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v0

    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemIpAddr:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemDnsSvr:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/ethernet/EthernetDevInfo;->getIpAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-virtual {v0}, Landroid/net/ethernet/EthernetDevInfo;->getDnsAddr()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    const-string v4, "0.0.0.0"

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const-string v4, "0.0.0.0"

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
