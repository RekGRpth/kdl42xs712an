.class Lcom/konka/systemsetting/net/WifiDisplaySettings$4;
.super Ljava/lang/Object;
.source "WifiDisplaySettings.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/WifiDisplaySettings;->showDisconnectDialog(Landroid/hardware/display/WifiDisplay;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

.field private final synthetic val$display:Landroid/hardware/display/WifiDisplay;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/hardware/display/WifiDisplay;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    iput-object p2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;->val$display:Landroid/hardware/display/WifiDisplay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;->val$display:Landroid/hardware/display/WifiDisplay;

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    # getter for: Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;
    invoke-static {v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->access$6(Lcom/konka/systemsetting/net/WifiDisplaySettings;)Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v1

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;->this$0:Lcom/konka/systemsetting/net/WifiDisplaySettings;

    # getter for: Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;
    invoke-static {v0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->access$7(Lcom/konka/systemsetting/net/WifiDisplaySettings;)Landroid/hardware/display/DisplayManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    :cond_0
    return-void
.end method
