.class public Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;
.super Ljava/lang/Object;
.source "AccessPointConfigDialogViewHolder.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnClickEvent;,
        Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;,
        Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;,
        Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "==========>Net_Wifi_Cfg==>APConfigDialogViewHolder"


# instance fields
.field private box1:Landroid/widget/LinearLayout;

.field private box2:Landroid/widget/LinearLayout;

.field private btnCancel:Landroid/widget/Button;

.field private btnConnect:Landroid/widget/Button;

.field private btnForget:Landroid/widget/Button;

.field private btnSave:Landroid/widget/Button;

.field public cbSetIP:Landroid/widget/CheckBox;

.field public cbShowPwd:Landroid/widget/CheckBox;

.field private container:Landroid/widget/LinearLayout;

.field public etDns1:Landroid/widget/EditText;

.field public etGateway:Landroid/widget/EditText;

.field public etIpAddr:Landroid/widget/EditText;

.field public etPassword:Landroid/widget/EditText;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

.field private mResult:Landroid/net/wifi/ScanResult;

.field private pwdInputBox:Landroid/widget/LinearLayout;

.field public spIpMode:Landroid/widget/Spinner;

.field private tvSSIDTitle:Landroid/widget/TextView;

.field private tvSecurity:Landroid/widget/TextView;

.field private tvSignalLvl:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;Lcom/konka/systemsetting/net/AccessPointConfigDialog;)V
    .locals 1
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object p2, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v0, v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mResult:Landroid/net/wifi/ScanResult;

    iput-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->initUIComponents()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->setListeners()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->changeCBShowPwdState(Z)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->changeCBSetIPState(Z)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnOnEnterKeyDown(I)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->setIPConfigurable(Z)V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;)Lcom/konka/systemsetting/net/AccessPointConfigDialog;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    return-object v0
.end method

.method private autoFillIPSettings(Landroid/net/wifi/WifiConfiguration;)V
    .locals 19
    .param p1    # Landroid/net/wifi/WifiConfiguration;

    if-nez p1, :cond_1

    const-string v17, "autoFillIPSettings null error!!!"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/4 v8, 0x0

    const/4 v6, 0x0

    const/4 v14, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v9, 0x0

    check-cast v9, [Ljava/lang/String;

    const/4 v7, 0x0

    check-cast v7, [Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v12, v0, Landroid/net/wifi/WifiConfiguration;->linkProperties:Landroid/net/LinkProperties;

    invoke-virtual {v12}, Landroid/net/LinkProperties;->getLinkAddresses()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_2

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Landroid/net/LinkAddress;

    invoke-virtual {v11}, Landroid/net/LinkAddress;->getAddress()Ljava/net/InetAddress;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v8

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "autoFillIPSettings(cfg)-> ip: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    invoke-static {v8}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_2
    invoke-virtual {v12}, Landroid/net/LinkProperties;->getRoutes()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v17

    :cond_3
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->hasNext()Z

    move-result v18

    if-nez v18, :cond_6

    :cond_4
    :goto_1
    invoke-virtual {v12}, Landroid/net/LinkProperties;->getDnses()Ljava/util/Collection;

    move-result-object v17

    invoke-interface/range {v17 .. v17}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v17

    if-eqz v17, :cond_5

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v17

    check-cast v17, Ljava/net/InetAddress;

    invoke-virtual/range {v17 .. v17}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v16

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "autoFillIPSettings(cfg)-> dns1: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    invoke-static/range {v16 .. v16}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_5

    move-object/from16 v3, v16

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :cond_5
    if-nez v8, :cond_7

    const-string v17, "autoFillIPSettings: ip is null!!!!"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    invoke-interface/range {v17 .. v17}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Landroid/net/RouteInfo;

    invoke-virtual {v15}, Landroid/net/RouteInfo;->isDefaultRoute()Z

    move-result v18

    if-eqz v18, :cond_3

    invoke-virtual {v15}, Landroid/net/RouteInfo;->getGateway()Ljava/net/InetAddress;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/net/InetAddress;->getHostAddress()Ljava/lang/String;

    move-result-object v6

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "autoFillIPSettings(cfg)-> gateway: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    invoke-static {v6}, Lcom/konka/utilities/Tools;->matchIP(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v6}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_7
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "autoFillIPSettings: ip = "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    invoke-static {v8}, Lcom/konka/utilities/Tools;->resolutionIP(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    invoke-static {v6}, Lcom/konka/utilities/Tools;->resolutionIP(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    if-eqz v9, :cond_e

    if-eqz v7, :cond_e

    array-length v0, v9

    move/from16 v17, v0

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-lt v0, v1, :cond_8

    array-length v0, v7

    move/from16 v17, v0

    const/16 v18, 0x4

    move/from16 v0, v17

    move/from16 v1, v18

    if-ge v0, v1, :cond_9

    :cond_8
    const-string v17, "autoFillIPSettings \uff1aips.length < 4 || gateways.length < 4"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_9
    const/16 v17, 0x0

    aget-object v17, v9, v17

    if-eqz v17, :cond_a

    const/16 v17, 0x0

    aget-object v17, v7, v17

    if-eqz v17, :cond_a

    const/16 v17, 0x0

    aget-object v17, v9, v17

    const/16 v18, 0x0

    aget-object v18, v7, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_a

    const-string v14, "255"

    :goto_2
    const/16 v17, 0x1

    aget-object v17, v9, v17

    if-eqz v17, :cond_b

    const/16 v17, 0x1

    aget-object v17, v7, v17

    if-eqz v17, :cond_b

    const/16 v17, 0x1

    aget-object v17, v9, v17

    const/16 v18, 0x1

    aget-object v18, v7, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_b

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ".255"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    :goto_3
    const/16 v17, 0x2

    aget-object v17, v9, v17

    if-eqz v17, :cond_c

    const/16 v17, 0x2

    aget-object v17, v7, v17

    if-eqz v17, :cond_c

    const/16 v17, 0x2

    aget-object v17, v9, v17

    const/16 v18, 0x2

    aget-object v18, v7, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_c

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ".255"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    :goto_4
    const/16 v17, 0x3

    aget-object v17, v9, v17

    if-eqz v17, :cond_d

    const/16 v17, 0x3

    aget-object v17, v7, v17

    if-eqz v17, :cond_d

    const/16 v17, 0x3

    aget-object v17, v9, v17

    const/16 v18, 0x3

    aget-object v18, v7, v18

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_d

    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ".255"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    :goto_5
    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "autoFillIPSettings(cfg)-> mask: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    :goto_6
    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-object v0, v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    if-eqz v2, :cond_0

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v18, "autoFillIPSettings(cfg)-> mac: "

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, v17

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_a
    const-string v14, "0"

    goto/16 :goto_2

    :cond_b
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ".0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_3

    :cond_c
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ".0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_4

    :cond_d
    new-instance v17, Ljava/lang/StringBuilder;

    invoke-static {v14}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    invoke-direct/range {v17 .. v18}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v18, ".0"

    invoke-virtual/range {v17 .. v18}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    goto/16 :goto_5

    :cond_e
    const-string v17, "autoFillIPSettings(cfg)-> mask: null"

    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    goto :goto_6
.end method

.method private btnOnEnterKeyDown(I)V
    .locals 1
    .param p1    # I

    packed-switch p1, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->onCancel()V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->onForget()V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->onConnect()V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->onSave()V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f09005f
        :pswitch_0    # com.konka.systemsetting.R.id.wifi_box_btn_cancel
        :pswitch_3    # com.konka.systemsetting.R.id.wifi_box_btn_save
        :pswitch_1    # com.konka.systemsetting.R.id.wifi_box_btn_forget
        :pswitch_2    # com.konka.systemsetting.R.id.wifi_box_btn_connect
    .end packed-switch
.end method

.method private changeCBSetIPState(Z)V
    .locals 3
    .param p1    # Z

    const/16 v2, 0x8

    const/4 v1, 0x0

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box1:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box2:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto :goto_0
.end method

.method private changeCBShowPwdState(Z)V
    .locals 2
    .param p1    # Z

    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v0

    if-eqz p1, :cond_0

    invoke-static {}, Landroid/text/method/HideReturnsTransformationMethod;->getInstance()Landroid/text/method/HideReturnsTransformationMethod;

    move-result-object v0

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setTransformationMethod(Landroid/text/method/TransformationMethod;)V

    return-void

    :cond_0
    invoke-static {}, Landroid/text/method/PasswordTransformationMethod;->getInstance()Landroid/text/method/PasswordTransformationMethod;

    move-result-object v0

    goto :goto_0
.end method

.method private initUIComponents()V
    .locals 13

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v11, "layout_inflater"

    invoke-virtual {v10, v11}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/LayoutInflater;

    const v10, 0x7f030014    # com.konka.systemsetting.R.layout.sys_network_wifi_box

    const/4 v11, 0x0

    invoke-virtual {v5, v10, v11}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090053    # com.konka.systemsetting.R.id.wifi_box1

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box1:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f09005a    # com.konka.systemsetting.R.id.wifi_box2

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box2:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box1:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->box2:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090056    # com.konka.systemsetting.R.id.wifi_box_input

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/LinearLayout;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->pwdInputBox:Landroid/widget/LinearLayout;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090058    # com.konka.systemsetting.R.id.wifi_box_cb_showpwd

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbShowPwd:Landroid/widget/CheckBox;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090059    # com.konka.systemsetting.R.id.wifi_box_cb_setip

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbSetIP:Landroid/widget/CheckBox;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f09005b    # com.konka.systemsetting.R.id.wifi_ipmode

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Spinner;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v10}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v10

    const v11, 0x7f07000e    # com.konka.systemsetting.R.array.wifiip_switch

    invoke-virtual {v10, v11}, Landroid/content/res/Resources;->getStringArray(I)[Ljava/lang/String;

    move-result-object v6

    new-instance v0, Landroid/widget/ArrayAdapter;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v11, 0x1090008    # android.R.layout.simple_spinner_item

    invoke-direct {v0, v10, v11, v6}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "initUIComponents(): ipModes[0] = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x0

    aget-object v11, v6, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "initUIComponents(): ipModes[1] = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v11, 0x1

    aget-object v11, v6, v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    const v10, 0x1090009    # android.R.layout.simple_spinner_dropdown_item

    invoke-virtual {v0, v10}, Landroid/widget/ArrayAdapter;->setDropDownViewResource(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v10, v0}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090057    # com.konka.systemsetting.R.id.wifi_box_et_pwd

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etPassword:Landroid/widget/EditText;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f09005c    # com.konka.systemsetting.R.id.wifiipet_ipaddr

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f09005d    # com.konka.systemsetting.R.id.wifiipet_gatway

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f09005e    # com.konka.systemsetting.R.id.wifiipet_dns1

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/EditText;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f09005f    # com.konka.systemsetting.R.id.wifi_box_btn_cancel

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnCancel:Landroid/widget/Button;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090061    # com.konka.systemsetting.R.id.wifi_box_btn_forget

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnForget:Landroid/widget/Button;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090062    # com.konka.systemsetting.R.id.wifi_box_btn_connect

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090060    # com.konka.systemsetting.R.id.wifi_box_btn_save

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnSave:Landroid/widget/Button;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090052    # com.konka.systemsetting.R.id.wifi_box_title

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->tvSSIDTitle:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->tvSSIDTitle:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    invoke-virtual {v10, v11}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getSecurityString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090054    # com.konka.systemsetting.R.id.wifi_box_security

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->tvSecurity:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->tvSecurity:Landroid/widget/TextView;

    invoke-virtual {v10, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "initUIComponents(): tvSSIDTitle.setText("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ");"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "initUIComponents(): mResult.capabilities = "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ";"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "initUIComponents(): tvSecurity.setText("

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ");"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    const v11, 0x7f090055    # com.konka.systemsetting.R.id.wifi_box_signalstrength

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    iput-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->tvSignalLvl:Landroid/widget/TextView;

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->tvSignalLvl:Landroid/widget/TextView;

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-virtual {v11}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->getSignalStrength()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v10, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v2

    if-eqz v2, :cond_3

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_3

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v10

    const-string v11, "0x"

    if-eq v10, v11, :cond_3

    const/4 v10, -0x1

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v11

    if-eq v10, v11, :cond_3

    invoke-static {}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->getbConnectFlag()Z

    move-result v10

    if-eqz v10, :cond_3

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "initUIComponents(): getConnectionInfo(): "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v10, v10, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    const-string v10, "initUIComponents(): currAP is connected!!!"

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-virtual {v2}, Landroid/net/wifi/WifiInfo;->getNetworkId()I

    move-result v11

    iput v11, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->networkId:I

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConnected:Z

    const/4 v7, 0x0

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v10, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_1

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v10, v7}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbSetIP:Landroid/widget/CheckBox;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnSave:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnForget:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->pwdInputBox:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    :goto_1
    return-void

    :cond_1
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    iget-object v11, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v11}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_0

    iget-object v11, v1, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    sget-object v12, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    if-ne v11, v12, :cond_2

    const/4 v7, 0x1

    :cond_2
    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->autoFillIPSettings(Landroid/net/wifi/WifiConfiguration;)V

    goto :goto_0

    :cond_3
    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-boolean v10, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConnected:Z

    if-nez v10, :cond_4

    const/4 v3, 0x0

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v10, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getConfiguredNetworks()Ljava/util/List;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-nez v11, :cond_5

    :cond_4
    const-string v10, "initUIComponents(): currAP is neither configed, nor connected!!!"

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbSetIP:Landroid/widget/CheckBox;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnSave:Landroid/widget/Button;

    const/4 v11, 0x4

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnForget:Landroid/widget/Button;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnCancel:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    const-string v10, "OPEN"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->pwdInputBox:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    invoke-virtual {v10}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_1

    :cond_5
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/WifiConfiguration;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "initUIComponents(): getConfiguredNetworks()["

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v4, v3, 0x1

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "] : "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "initUIComponents(): detail:"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Landroid/net/wifi/WifiConfiguration;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v11}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    iget-object v11, v1, Landroid/net/wifi/WifiConfiguration;->SSID:Ljava/lang/String;

    invoke-static {v11}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    iget-object v11, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mResult:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v9, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_8

    const-string v10, "initUIComponents(): currAP is configed!!!"

    invoke-direct {p0, v10}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget v11, v1, Landroid/net/wifi/WifiConfiguration;->networkId:I

    iput v11, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->networkId:I

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->mCurrAP:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    const/4 v11, 0x1

    iput-boolean v11, v10, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->isConfiged:Z

    const/4 v7, 0x0

    iget-object v10, v1, Landroid/net/wifi/WifiConfiguration;->ipAssignment:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    sget-object v11, Landroid/net/wifi/WifiConfiguration$IpAssignment;->STATIC:Landroid/net/wifi/WifiConfiguration$IpAssignment;

    if-ne v10, v11, :cond_6

    const/4 v7, 0x1

    :cond_6
    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v10, v7}, Landroid/widget/Spinner;->setSelection(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbSetIP:Landroid/widget/CheckBox;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnSave:Landroid/widget/Button;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnForget:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/Button;->setVisibility(I)V

    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->pwdInputBox:Landroid/widget/LinearLayout;

    const/16 v11, 0x8

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    :cond_7
    iget-object v10, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->pwdInputBox:Landroid/widget/LinearLayout;

    const/4 v11, 0x0

    invoke-virtual {v10, v11}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_1

    :cond_8
    move v3, v4

    goto/16 :goto_2
.end method

.method private log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "==========>Net_Wifi_Cfg==>APConfigDialogViewHolder"

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method private setIPConfigurable(Z)V
    .locals 2
    .param p1    # Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "setIPConfigurable : "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->log(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etIpAddr:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etGateway:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setEnabled(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->etDns1:Landroid/widget/EditText;

    invoke-virtual {v0, p1}, Landroid/widget/EditText;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private setListeners()V
    .locals 6

    const/4 v5, 0x0

    new-instance v2, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;

    invoke-direct {v2, p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$OnCheckedChangeEvent;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbShowPwd:Landroid/widget/CheckBox;

    invoke-virtual {v4, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->cbSetIP:Landroid/widget/CheckBox;

    invoke-virtual {v4, v2}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    new-instance v0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnClickEvent;

    invoke-direct {v0, p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnClickEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnClickEvent;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnCancel:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnForget:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnSave:Landroid/widget/Button;

    invoke-virtual {v4, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v1, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;

    invoke-direct {v1, p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$BtnOnKeyEvent;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnCancel:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnForget:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnConnect:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->btnSave:Landroid/widget/Button;

    invoke-virtual {v4, v1}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    new-instance v3, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;

    invoke-direct {v3, p0, v5}, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;-><init>(Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder$SpinnerOnItemSelectedEvent;)V

    iget-object v4, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->spIpMode:Landroid/widget/Spinner;

    invoke-virtual {v4, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    return-void
.end method


# virtual methods
.method public getDialogView()Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialogViewHolder;->container:Landroid/widget/LinearLayout;

    return-object v0
.end method
