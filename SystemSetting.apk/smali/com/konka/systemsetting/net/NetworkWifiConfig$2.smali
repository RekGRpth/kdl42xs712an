.class Lcom/konka/systemsetting/net/NetworkWifiConfig$2;
.super Ljava/lang/Object;
.source "NetworkWifiConfig.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v4, 0x1

    const/high16 v0, -0x1000000

    if-eqz p2, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v2

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v3

    iget-object v3, v3, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/konka/systemsetting/net/NetworkPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    const v0, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$5(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$6(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/TextView;

    move-result-object v1

    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    :cond_0
    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    return-void

    :cond_1
    const v0, 0x7f020031    # com.konka.systemsetting.R.drawable.syssettingthirditemsingle

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$5(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/LinearLayout;

    move-result-object v1

    if-ne p1, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$6(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/TextView;

    move-result-object v1

    const v2, -0x515152

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
