.class Lcom/konka/systemsetting/net/NetworkWireConfig$1;
.super Landroid/content/BroadcastReceiver;
.source "NetworkWireConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWireConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkWireConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x0

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "===>intent.getAction() ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.ETHERNET_STATE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    const-string v1, "ETHERNET_state"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    const-string v2, "previous_ETHERNET_state"

    invoke-virtual {p2, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/net/NetworkWireConfig;->handleEthStateChanged(II)V
    invoke-static {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkWireConfig;->access$0(Lcom/konka/systemsetting/net/NetworkWireConfig;II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.net.ethernet.STATE_CHANGE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWireConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkWireConfig;

    const-string v0, "networkInfo"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/NetworkInfo;

    # invokes: Lcom/konka/systemsetting/net/NetworkWireConfig;->handleNetworkStateChanged(Landroid/net/NetworkInfo;)V
    invoke-static {v1, v0}, Lcom/konka/systemsetting/net/NetworkWireConfig;->access$1(Lcom/konka/systemsetting/net/NetworkWireConfig;Landroid/net/NetworkInfo;)V

    goto :goto_0
.end method
