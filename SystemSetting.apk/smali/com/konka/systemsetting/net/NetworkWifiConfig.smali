.class public Lcom/konka/systemsetting/net/NetworkWifiConfig;
.super Ljava/lang/Object;
.source "NetworkWifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$bottomHPOnFocusChangeEvent;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;,
        Lcom/konka/systemsetting/net/NetworkWifiConfig$topHPOnFocusChangeEvent;
    }
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$android$net$NetworkInfo$DetailedState:[I = null

.field private static final HP_LINK_NUM:I = 0x5

.field private static final SECURITY_EAP:I = 0x3

.field private static final SECURITY_NONE:I = 0x0

.field private static final SECURITY_PSK:I = 0x2

.field private static final SECURITY_WEP:I = 0x1

.field private static final SIGNAL_0:I = 0x0

.field private static final SIGNAL_1:I = 0x1

.field private static final SIGNAL_2:I = 0x2

.field private static final SIGNAL_3:I = 0x3

.field private static final SIGNAL_4:I = 0x4

.field private static final SIGNAL_5:I = 0x5

.field private static final SIGNAL_6:I = 0x6

.field private static final SIGNAL_NUM_LEVELS:I = 0x7

.field private static final WIFI_RESCAN_INTERVAL_MS:I = 0x2710

.field private static bConnectFlag:Z


# instance fields
.field Tag:Ljava/lang/String;

.field private bScanOver:Z

.field private btnDetect:Landroid/widget/Button;

.field private btnScan:Landroid/widget/Button;

.field private btnWps:Landroid/widget/Button;

.field private hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

.field private hpSecurity:[Ljava/lang/String;

.field private iCurrFirstIndex:I

.field private iCurrSeletedIndex:I

.field private iHPCounter:I

.field private is2TurnOffSoftAp:Z

.field public isAPSetting:Z

.field private isRefresh:Z

.field private isScrolling:Z

.field private isUnregisted:Z

.field private itemHotspots:[Landroid/widget/LinearLayout;

.field private itemSwitch:Landroid/widget/LinearLayout;

.field private ivSwitch:Landroid/widget/ImageView;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field mCurrResult:Landroid/net/wifi/ScanResult;

.field private mFilter:Landroid/content/IntentFilter;

.field private mResults:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;"
        }
    .end annotation
.end field

.field private mSaveListener:Landroid/net/wifi/WifiManager$ActionListener;

.field mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

.field private mWifiContainer:Landroid/widget/LinearLayout;

.field public mWifiReceiver:Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;

.field private manager:Landroid/net/wifi/WifiManager;

.field onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

.field onGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

.field onKeyListener:Landroid/view/View$OnKeyListener;

.field private srCurLinkHP:Landroid/net/wifi/ScanResult;

.field private tvTip:Landroid/widget/TextView;


# direct methods
.method static synthetic $SWITCH_TABLE$android$net$NetworkInfo$DetailedState()[I
    .locals 3

    sget-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->$SWITCH_TABLE$android$net$NetworkInfo$DetailedState:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Landroid/net/NetworkInfo$DetailedState;->values()[Landroid/net/NetworkInfo$DetailedState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->AUTHENTICATING:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x4

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_c

    :goto_1
    :try_start_1
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->BLOCKED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/16 v2, 0xb

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_b

    :goto_2
    :try_start_2
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CAPTIVE_PORTAL_CHECK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/16 v2, 0xd

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_a

    :goto_3
    :try_start_3
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x6

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_9

    :goto_4
    :try_start_4
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->CONNECTING:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_8

    :goto_5
    :try_start_5
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/16 v2, 0x9

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_7

    :goto_6
    :try_start_6
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->DISCONNECTING:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/16 v2, 0x8

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_6

    :goto_7
    :try_start_7
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->FAILED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/16 v2, 0xa

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_5

    :goto_8
    :try_start_8
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->IDLE:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_4

    :goto_9
    :try_start_9
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->OBTAINING_IPADDR:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x5

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_3

    :goto_a
    :try_start_a
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->SCANNING:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_2

    :goto_b
    :try_start_b
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->SUSPENDED:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/4 v2, 0x7

    aput v2, v0, v1
    :try_end_b
    .catch Ljava/lang/NoSuchFieldError; {:try_start_b .. :try_end_b} :catch_1

    :goto_c
    :try_start_c
    sget-object v1, Landroid/net/NetworkInfo$DetailedState;->VERIFYING_POOR_LINK:Landroid/net/NetworkInfo$DetailedState;

    invoke-virtual {v1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v1

    const/16 v2, 0xc

    aput v2, v0, v1
    :try_end_c
    .catch Ljava/lang/NoSuchFieldError; {:try_start_c .. :try_end_c} :catch_0

    :goto_d
    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->$SWITCH_TABLE$android$net$NetworkInfo$DetailedState:[I

    goto/16 :goto_0

    :catch_0
    move-exception v1

    goto :goto_d

    :catch_1
    move-exception v1

    goto :goto_c

    :catch_2
    move-exception v1

    goto :goto_b

    :catch_3
    move-exception v1

    goto :goto_a

    :catch_4
    move-exception v1

    goto :goto_9

    :catch_5
    move-exception v1

    goto :goto_8

    :catch_6
    move-exception v1

    goto :goto_7

    :catch_7
    move-exception v1

    goto :goto_6

    :catch_8
    move-exception v1

    goto :goto_5

    :catch_9
    move-exception v1

    goto :goto_4

    :catch_a
    move-exception v1

    goto :goto_3

    :catch_b
    move-exception v1

    goto/16 :goto_2

    :catch_c
    move-exception v1

    goto/16 :goto_1
.end method

.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bConnectFlag:Z

    return-void
.end method

.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 4
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "==========>Net_Wifi_Cfg"

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->Tag:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    iput-boolean v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isRefresh:Z

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->srCurLinkHP:Landroid/net/wifi/ScanResult;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->is2TurnOffSoftAp:Z

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isScrolling:Z

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$1;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$2;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$3;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onKeyListener:Landroid/view/View$OnKeyListener;

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isAPSetting:Z

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->findViews()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070008    # com.konka.systemsetting.R.array.str_arr_network_wifi_switch

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    sput-object v0, Lcom/konka/systemsetting/common/Content$wifi;->wifiSwitch:[Ljava/lang/CharSequence;

    iput-boolean v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isUnregisted:Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    invoke-direct {v0, p0, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->registmWifiReceiver()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setOnListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->initUIData()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isScrolling:Z

    return-void
.end method

.method static synthetic access$10(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$11(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/Button;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic access$12(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showAPSettingDlg()V

    return-void
.end method

.method static synthetic access$13(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    return-object v0
.end method

.method static synthetic access$14(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    return v0
.end method

.method static synthetic access$15(Lcom/konka/systemsetting/net/NetworkWifiConfig;Landroid/net/wifi/ScanResult;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->srCurLinkHP:Landroid/net/wifi/ScanResult;

    return-void
.end method

.method static synthetic access$16(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/ScanResult;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->srCurLinkHP:Landroid/net/wifi/ScanResult;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bScanOver:Z

    return-void
.end method

.method static synthetic access$18(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateScanlist()V

    return-void
.end method

.method static synthetic access$19(Lcom/konka/systemsetting/net/NetworkWifiConfig;Landroid/net/NetworkInfo$DetailedState;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateConnectionState(Landroid/net/NetworkInfo$DetailedState;)V

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpOnUpKeyDown()V

    return-void
.end method

.method static synthetic access$20(Lcom/konka/systemsetting/net/NetworkWifiConfig;Z)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setWifiEnabled(Z)V

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpOnDownKeyDown()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/widget/TextView;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Landroid/net/wifi/WifiManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/net/NetworkWifiConfig;)[Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/net/NetworkWifiConfig;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    return v0
.end method

.method private clearHPLinksParams()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-lt v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->clear()V

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bScanOver:Z

    if-eqz v1, :cond_1

    iput-boolean v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bScanOver:Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v4

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v4

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    const v2, 0x7f060087    # com.konka.systemsetting.R.string.wifi_noactivenetwork

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_1
    return-void

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private compareResults(Ljava/util/List;Ljava/util/List;)Z
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)Z"
        }
    .end annotation

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v1, v2

    :cond_1
    :goto_0
    return v1

    :cond_2
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v4

    if-ne v3, v4, :cond_4

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    invoke-direct {p0, v0, p2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isScanResultInList(Landroid/net/wifi/ScanResult;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method private detectWifiNetwork()Z
    .locals 6

    const v5, 0x7f06007d    # com.konka.systemsetting.R.string.wifi_unkown

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const-string v4, "connectivity"

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0, v2}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/NetworkInfo;->getState()Landroid/net/NetworkInfo$State;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_0

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f06008e    # com.konka.systemsetting.R.string.wifi_connecting

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    :goto_0
    return v2

    :cond_0
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->CONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_1

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f060091    # com.konka.systemsetting.R.string.wifi_connected

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_1
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->SUSPENDED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f060088    # com.konka.systemsetting.R.string.wifi_suspended

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_2
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTING:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f060092    # com.konka.systemsetting.R.string.wifi_disconnecting

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_3
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->DISCONNECTED:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f060093    # com.konka.systemsetting.R.string.wifi_disconnected

    invoke-virtual {v3, v4}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_4
    invoke-virtual {v1}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v3

    sget-object v4, Landroid/net/NetworkInfo$State;->UNKNOWN:Landroid/net/NetworkInfo$State;

    invoke-virtual {v4}, Landroid/net/NetworkInfo$State;->ordinal()I

    move-result v4

    if-ne v3, v4, :cond_5

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3, v5}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_5
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v2, v5}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    const/4 v2, 0x0

    goto :goto_0
.end method

.method private enableHotspot(IZ)V
    .locals 1
    .param p1    # I
    .param p2    # Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v0, v0, p1

    invoke-virtual {v0, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private findViews()V
    .locals 6

    const/4 v5, 0x5

    const/4 v3, 0x1

    const v4, 0x7f09003d    # com.konka.systemsetting.R.id.linearlayout_network_tab_wireless

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090072    # com.konka.systemsetting.R.id.wifi_container

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09006e    # com.konka.systemsetting.R.id.sys_network_wireless_item_wifiswitch

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    new-array v1, v5, [Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    array-length v1, v1

    if-lt v0, v1, :cond_0

    new-array v1, v5, [Ljava/lang/String;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpSecurity:[Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090078    # com.konka.systemsetting.R.id.sys_network_wireless_item_scan

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090079    # com.konka.systemsetting.R.id.sys_network_wireless_item_detect

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09007a    # com.konka.systemsetting.R.id.sys_network_wireless_item_wps

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f09006f    # com.konka.systemsetting.R.id.sys_network_wireless_tip_wifi

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090070    # com.konka.systemsetting.R.id.sys_network_wireless_wifiswitch

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->ivSwitch:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    invoke-virtual {v1, v4}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    return-void

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    sget-object v3, Lcom/konka/systemsetting/common/Content$Ress;->ITEM_CONNECTION:[I

    aget v3, v3, v0

    invoke-virtual {v1, v3}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    aput-object v1, v2, v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private getImgByWifiLvl(I)I
    .locals 2
    .param p1    # I

    const v0, 0x7f020036    # com.konka.systemsetting.R.drawable.wifi1

    const/4 v1, 0x7

    invoke-static {p1, v1}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result p1

    packed-switch p1, :pswitch_data_0

    :goto_0
    return v0

    :pswitch_0
    const v0, 0x7f020036    # com.konka.systemsetting.R.drawable.wifi1

    goto :goto_0

    :pswitch_1
    const v0, 0x7f020037    # com.konka.systemsetting.R.drawable.wifi2

    goto :goto_0

    :pswitch_2
    const v0, 0x7f020038    # com.konka.systemsetting.R.drawable.wifi3

    goto :goto_0

    :pswitch_3
    const v0, 0x7f020039    # com.konka.systemsetting.R.drawable.wifi4

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
    .end packed-switch
.end method

.method private getSecurity(Ljava/lang/String;)I
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "WEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "PSK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x2

    goto :goto_0

    :cond_1
    const-string v0, "EAP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getSecurityString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1    # Ljava/lang/String;

    const-string v0, "WEP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "WEP"

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "PSK"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "WPA/WPA2 PSK"

    goto :goto_0

    :cond_1
    const-string v0, "EAP"

    invoke-virtual {p1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "EAP"

    goto :goto_0

    :cond_2
    const-string v0, "OPEN"

    goto :goto_0
.end method

.method private getText(I)Ljava/lang/String;
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, p1}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getbConnectFlag()Z
    .locals 1

    sget-boolean v0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bConnectFlag:Z

    return v0
.end method

.method private hpOnDownKeyDown()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x5

    if-gt v0, v1, :cond_1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_2

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0

    :cond_2
    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    add-int/lit8 v1, v1, -0x1

    if-ge v0, v1, :cond_3

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateCurrHPsParams()V

    goto :goto_0

    :cond_3
    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isScrolling:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    goto :goto_0
.end method

.method private hpOnUpKeyDown()V
    .locals 2

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    if-lez v0, :cond_1

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    aget-object v0, v0, v1

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    if-lez v0, :cond_2

    iget v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateCurrHPsParams()V

    goto :goto_0

    :cond_2
    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isScrolling:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    goto :goto_0
.end method

.method private initUIData()V
    .locals 3

    const/4 v2, 0x0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v1, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->forceScan()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateScanlist()V

    :goto_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateLinkState()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->ivSwitch:Landroid/widget/ImageView;

    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v1, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->pause()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->clearHPLinksParams()V

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setFocusMoveable(Z)V

    goto :goto_0
.end method

.method private isScanResultInList(Landroid/net/wifi/ScanResult;Ljava/util/List;)Z
    .locals 3
    .param p1    # Landroid/net/wifi/ScanResult;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/wifi/ScanResult;",
            "Ljava/util/List",
            "<",
            "Landroid/net/wifi/ScanResult;",
            ">;)Z"
        }
    .end annotation

    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_1
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/ScanResult;

    invoke-virtual {p1, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method static removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0    # Ljava/lang/String;

    const/16 v3, 0x22

    const/4 v2, 0x1

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, v2, :cond_0

    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v1

    if-ne v1, v3, :cond_0

    add-int/lit8 v1, v0, -0x1

    invoke-virtual {p0, v2, v1}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object p0

    :cond_0
    return-object p0
.end method

.method private setFocusMoveable(Z)V
    .locals 3
    .param p1    # Z

    const v2, -0x515152

    const/high16 v1, -0x1000000

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setTextColor(I)V

    :goto_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    invoke-virtual {v0, p1}, Landroid/widget/Button;->setClickable(Z)V

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setTextColor(I)V

    goto :goto_0
.end method

.method private setItemSwitchEnable(Z)V
    .locals 4
    .param p1    # Z

    if-eqz p1, :cond_0

    const v1, -0xcdcdce

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    invoke-virtual {v2, p1}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    if-eqz p1, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onKeyListener:Landroid/view/View$OnKeyListener;

    :goto_1
    invoke-virtual {v3, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void

    :cond_0
    const v1, -0x666667

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method private setWifiEnabled(Z)V
    .locals 5
    .param p1    # Z

    const/4 v4, 0x0

    if-eqz p1, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getWifiApConfiguration()Landroid/net/wifi/WifiConfiguration;

    move-result-object v2

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiApConfig:Landroid/net/wifi/WifiConfiguration;

    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2, p1}, Landroid/net/wifi/WifiManager;->setWifiEnabled(Z)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, p1

    if-eqz v1, :cond_4

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v2, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    :goto_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->ivSwitch:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    if-nez v1, :cond_1

    const v2, 0x7f060082    # com.konka.systemsetting.R.string.wifi_disabled

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showTip(I)V

    :cond_1
    invoke-direct {p0, v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setItemSwitchEnable(Z)V

    if-nez v1, :cond_2

    invoke-direct {p0, v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setFocusMoveable(Z)V

    :cond_2
    return-void

    :cond_3
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/net/wifi/WifiManager;->setWifiApEnabled(Landroid/net/wifi/WifiConfiguration;Z)Z

    goto :goto_0

    :cond_4
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v0, v2, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    goto :goto_1
.end method

.method private showAPSettingDlg()V
    .locals 4

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    iget v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    add-int/2addr v2, v3

    if-le v1, v2, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isAPSetting:Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    iget v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    add-int/2addr v2, v3

    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/wifi/ScanResult;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mCurrResult:Landroid/net/wifi/ScanResult;

    new-instance v0, Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mCurrResult:Landroid/net/wifi/ScanResult;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-direct {v0, v1, v2, v3}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;-><init>(Landroid/content/Context;Landroid/net/wifi/ScanResult;Landroid/net/wifi/WifiManager;)V

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->show()V

    :cond_0
    return-void
.end method

.method private showTip(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method private showTip(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method private sortResutlsByLvl()V
    .locals 14

    const/4 v13, 0x7

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v4

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v4, :cond_1

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v10}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v10

    if-eqz v10, :cond_0

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->srCurLinkHP:Landroid/net/wifi/ScanResult;

    if-eqz v10, :cond_0

    const/4 v1, 0x0

    :goto_1
    if-lt v1, v4, :cond_7

    :cond_0
    return-void

    :cond_1
    const/4 v3, 0x0

    :goto_2
    sub-int v10, v4, v1

    add-int/lit8 v10, v10, -0x1

    if-lt v3, v10, :cond_2

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_2
    const/4 v2, 0x0

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/ScanResult;

    iget v5, v10, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v5, v13}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v5

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    add-int/lit8 v11, v3, 0x1

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/ScanResult;

    iget v6, v10, Landroid/net/wifi/ScanResult;->level:I

    invoke-static {v6, v13}, Landroid/net/wifi/WifiManager;->calculateSignalLevel(II)I

    move-result v6

    if-ge v5, v6, :cond_5

    const/4 v2, 0x1

    :cond_3
    :goto_3
    if-eqz v2, :cond_4

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/net/wifi/ScanResult;

    iget-object v11, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    add-int/lit8 v12, v3, 0x1

    invoke-interface {v10, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/ScanResult;

    invoke-interface {v11, v3, v10}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    add-int/lit8 v11, v3, 0x1

    invoke-interface {v10, v11, v9}, Ljava/util/List;->set(ILjava/lang/Object;)Ljava/lang/Object;

    :cond_4
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    :cond_5
    if-ne v5, v6, :cond_3

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v10, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/ScanResult;

    iget-object v7, v10, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    add-int/lit8 v11, v3, 0x1

    invoke-interface {v10, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/ScanResult;

    iget-object v8, v10, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    if-eqz v7, :cond_6

    if-eqz v8, :cond_6

    invoke-virtual {v7, v8}, Ljava/lang/String;->compareToIgnoreCase(Ljava/lang/String;)I

    move-result v10

    if-lez v10, :cond_6

    const/4 v2, 0x1

    goto :goto_3

    :cond_6
    if-eqz v7, :cond_3

    if-nez v8, :cond_3

    const/4 v2, 0x1

    goto :goto_3

    :cond_7
    iget-object v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v10, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/net/wifi/ScanResult;

    iget-object v10, v10, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    iget-object v11, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->srCurLinkHP:Landroid/net/wifi/ScanResult;

    iget-object v11, v11, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "the postion of last link in the new list==="

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " and the last index is==="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "["

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    iget v11, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "]"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p0, v10}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    iget v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    sub-int v10, v1, v10

    iput v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    iget v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    if-gez v10, :cond_9

    const/4 v10, 0x0

    iput v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    :cond_8
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    :cond_9
    const/4 v10, 0x5

    if-le v4, v10, :cond_8

    iget v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/lit8 v10, v10, 0x5

    if-le v10, v4, :cond_8

    add-int/lit8 v10, v4, -0x5

    iput v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    goto :goto_4
.end method

.method private testremove()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v2

    add-int/lit8 v0, v2, -0x1

    :goto_0
    const/4 v2, 0x1

    if-gt v0, v2, :cond_0

    return-void

    :cond_0
    invoke-interface {v1, v0}, Ljava/util/List;->remove(I)Ljava/lang/Object;

    add-int/lit8 v0, v0, -0x1

    goto :goto_0
.end method

.method private tipTurnOffSoftAp()V
    .locals 3

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v1, 0x7f060107    # com.konka.systemsetting.R.string.wifi_tipturnoffsoftap

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060005    # com.konka.systemsetting.R.string.common_warning_title

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060003    # com.konka.systemsetting.R.string.common_yes

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$4;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$4;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    const v1, 0x7f060004    # com.konka.systemsetting.R.string.common_no

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$5;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$5;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    invoke-virtual {v0, v1, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private updateConnectionState(Landroid/net/NetworkInfo$DetailedState;)V
    .locals 5
    .param p1    # Landroid/net/NetworkInfo$DetailedState;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "the connection state ==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/net/NetworkInfo$DetailedState;->name()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    const/4 v1, -0x1

    invoke-static {}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->$SWITCH_TABLE$android$net$NetworkInfo$DetailedState()[I

    move-result-object v2

    invoke-virtual {p1}, Landroid/net/NetworkInfo$DetailedState;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    const v1, 0x7f06008a    # com.konka.systemsetting.R.string.wifi_unreachable

    :goto_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    const v3, 0x7f060082    # com.konka.systemsetting.R.string.wifi_disabled

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :pswitch_1
    const v1, 0x7f06008d    # com.konka.systemsetting.R.string.wifi_scanning

    goto :goto_1

    :pswitch_2
    const v1, 0x7f06008e    # com.konka.systemsetting.R.string.wifi_connecting

    goto :goto_1

    :pswitch_3
    const v1, 0x7f06008f    # com.konka.systemsetting.R.string.wifi_authenticating

    goto :goto_1

    :pswitch_4
    const v1, 0x7f060090    # com.konka.systemsetting.R.string.wifi_obtaining_ipaddr

    goto :goto_1

    :pswitch_5
    const v1, 0x7f060091    # com.konka.systemsetting.R.string.wifi_connected

    const/4 v2, 0x1

    sput-boolean v2, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bConnectFlag:Z

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v3, 0x7f060091    # com.konka.systemsetting.R.string.wifi_connected

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_1

    :pswitch_6
    const v1, 0x7f060088    # com.konka.systemsetting.R.string.wifi_suspended

    goto :goto_1

    :pswitch_7
    const v1, 0x7f060092    # com.konka.systemsetting.R.string.wifi_disconnecting

    goto :goto_1

    :pswitch_8
    const v1, 0x7f060093    # com.konka.systemsetting.R.string.wifi_disconnected

    goto :goto_1

    :pswitch_9
    const v1, 0x7f060094    # com.konka.systemsetting.R.string.wifi_connect_failed

    goto :goto_1

    :pswitch_a
    const v1, 0x7f060095    # com.konka.systemsetting.R.string.wifi_connect_blocked

    goto :goto_1

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v2}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    const-string v3, "0x"

    if-eq v2, v3, :cond_1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    const-string v3, "<unknown ssid>"

    if-ne v2, v3, :cond_2

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    invoke-virtual {v2, v1}, Landroid/widget/TextView;->setText(I)V

    goto :goto_0

    :cond_2
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "get the current connection SSID==="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->getText(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method

.method private updateCurrHPCounter()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    goto :goto_0
.end method

.method private updateCurrHPsParams()V
    .locals 14

    const/4 v13, 0x2

    const/4 v12, 0x4

    const/4 v11, 0x1

    const/4 v8, 0x5

    const/4 v10, 0x0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateCurrHPCounter()V

    const/4 v2, 0x0

    const/high16 v4, -0x1000000

    iget v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    if-le v6, v8, :cond_2

    move v1, v8

    :goto_0
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "update link list====iHPCounter=="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " iCurrFirstIndex=="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " iForEnd=="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0, v6}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    iget v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    if-le v6, v8, :cond_0

    iget v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/lit8 v6, v6, 0x5

    iget v7, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    if-le v6, v7, :cond_0

    iget v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    add-int/lit8 v6, v6, -0x5

    iput v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    :cond_0
    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v6

    const/4 v7, 0x6

    if-ge v6, v7, :cond_1

    iput v10, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    :cond_1
    const/4 v0, 0x0

    :goto_1
    if-lt v0, v1, :cond_3

    move v0, v1

    :goto_2
    if-lt v0, v8, :cond_5

    return-void

    :cond_2
    iget v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iHPCounter:I

    goto :goto_0

    :cond_3
    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    iget-object v7, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    iget v9, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/2addr v9, v0

    invoke-interface {v7, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/wifi/ScanResult;

    iget-object v7, v7, Landroid/net/wifi/ScanResult;->SSID:Ljava/lang/String;

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    iget v7, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/2addr v7, v0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/ScanResult;

    iget v2, v6, Landroid/net/wifi/ScanResult;->level:I

    invoke-direct {p0, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->getImgByWifiLvl(I)I

    move-result v4

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v4}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    iget v7, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrFirstIndex:I

    add-int/2addr v7, v0

    invoke-interface {v6, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/net/wifi/ScanResult;

    iget-object v5, v6, Landroid/net/wifi/ScanResult;->capabilities:Ljava/lang/String;

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hpSecurity:[Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->getSecurityString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v0

    const/4 v3, 0x0

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->getSecurity(Ljava/lang/String;)I

    move-result v6

    if-nez v6, :cond_4

    const/4 v3, 0x4

    :cond_4
    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    :cond_5
    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v10}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    invoke-virtual {v6, v12}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v11}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v6, v6, v0

    invoke-virtual {v6, v13}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    invoke-virtual {v6, v12}, Landroid/widget/ImageView;->setVisibility(I)V

    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_2
.end method

.method private updateLinkState()V
    .locals 4

    const v3, 0x7f060093    # com.konka.systemsetting.R.string.wifi_disconnected

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v1

    if-nez v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    const v2, 0x7f060082    # com.konka.systemsetting.R.string.wifi_disabled

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "0x"

    if-eq v1, v2, :cond_2

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    const-string v2, "<unknown ssid>"

    if-ne v1, v2, :cond_3

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1, v3}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0

    :cond_3
    sget-boolean v1, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bConnectFlag:Z

    if-eqz v1, :cond_0

    const/4 v1, 0x0

    sput-boolean v1, Lcom/konka/systemsetting/net/NetworkWifiConfig;->bConnectFlag:Z

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "get the current connection SSID==="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->tvTip:Landroid/widget/TextView;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->removeDoubleQuotes(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const v3, 0x7f060091    # com.konka.systemsetting.R.string.wifi_connected

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->getText(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method private updateScanlist()V
    .locals 2

    const-string v1, "updateDataToUI"

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->getScanResults()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_0

    const-string v1, "the scan result is null!!!"

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "the size of scan result is 0!!!"

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->clearHPLinksParams()V

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-direct {p0, v1, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->compareResults(Ljava/util/List;Ljava/util/List;)Z

    move-result v1

    if-nez v1, :cond_2

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mResults:Ljava/util/List;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->sortResutlsByLvl()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->updateCurrHPsParams()V

    goto :goto_0

    :cond_2
    const-string v1, "the result has no difference,so do not refresh the list"

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method public changeWifiSwitch()V
    .locals 2

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->manager:Landroid/net/wifi/WifiManager;

    invoke-virtual {v1}, Landroid/net/wifi/WifiManager;->isWifiEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setWifiEnabled(Z)V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method convertToQuotedString(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1    # Ljava/lang/String;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\""

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\""

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method handleWifiStateChanged(Landroid/content/Intent;)V
    .locals 5
    .param p1    # Landroid/content/Intent;

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v1, "wifi_state"

    const/4 v2, 0x4

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " handleWifiStateChanged : "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->initUIData()V

    const v1, 0x7f060082    # com.konka.systemsetting.R.string.wifi_disabled

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showTip(I)V

    invoke-direct {p0, v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setItemSwitchEnable(Z)V

    goto :goto_0

    :pswitch_1
    const v1, 0x7f060081    # com.konka.systemsetting.R.string.wifi_disabling

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showTip(I)V

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setItemSwitchEnable(Z)V

    goto :goto_0

    :pswitch_2
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->initUIData()V

    const v1, 0x7f06007f    # com.konka.systemsetting.R.string.wifi_enabled

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showTip(I)V

    invoke-direct {p0, v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setItemSwitchEnable(Z)V

    invoke-direct {p0, v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setFocusMoveable(Z)V

    goto :goto_0

    :pswitch_3
    const v1, 0x7f06007e    # com.konka.systemsetting.R.string.wifi_enabling

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showTip(I)V

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setItemSwitchEnable(Z)V

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->setFocusMoveable(Z)V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method itemOnClick(I)Z
    .locals 3
    .param p1    # I

    const v2, 0x7f060103    # com.konka.systemsetting.R.string.softap_dongle_notfound

    const/4 v0, 0x0

    const/4 v1, 0x1

    packed-switch p1, :pswitch_data_0

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const-string v0, "Test by zhoujun------------ press wifiSwitch----------"

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    invoke-static {}, Lcom/mstar/android/wifi/MWifiManager;->getInstance()Lcom/mstar/android/wifi/MWifiManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/mstar/android/wifi/MWifiManager;->isWifiDeviceExist()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->log(Ljava/lang/String;)V

    move v0, v1

    goto :goto_0

    :cond_0
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->changeWifiSwitch()V

    move v0, v1

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f06008d    # com.konka.systemsetting.R.string.wifi_scanning

    invoke-virtual {v0, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->forceScan()V

    move v0, v1

    goto :goto_0

    :pswitch_3
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->detectWifiNetwork()Z

    move v0, v1

    goto :goto_0

    :pswitch_4
    iput v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    :goto_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v2, v2, v0

    invoke-virtual {v2, v0}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    iget v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    aget-object v0, v0, v2

    invoke-virtual {v0}, Landroid/widget/LinearLayout;->requestFocus()Z

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->showAPSettingDlg()V

    :cond_1
    move v0, v1

    goto :goto_0

    :pswitch_5
    iput v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    goto :goto_1

    :pswitch_6
    const/4 v2, 0x2

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    goto :goto_1

    :pswitch_7
    const/4 v2, 0x3

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    goto :goto_1

    :pswitch_8
    const/4 v2, 0x4

    iput v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->iCurrSeletedIndex:I

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x7f09006e
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_wireless_item_wifiswitch
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wireless_tip_wifi
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wireless_wifiswitch
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_wireless_tip
        :pswitch_0    # com.konka.systemsetting.R.id.wifi_container
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_wireless_item_link1
        :pswitch_5    # com.konka.systemsetting.R.id.sys_network_wireless_item_link2
        :pswitch_6    # com.konka.systemsetting.R.id.sys_network_wireless_item_link3
        :pswitch_7    # com.konka.systemsetting.R.id.sys_network_wireless_item_link4
        :pswitch_8    # com.konka.systemsetting.R.id.sys_network_wireless_item_link5
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_wireless_item_scan
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_wireless_item_detect
    .end packed-switch
.end method

.method log(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->Tag:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->hWifiScanner:Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$Scanner;->pause()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->unregistWifiReceiver()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isRefresh:Z

    return-void
.end method

.method public onResume()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->registmWifiReceiver()V

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isRefresh:Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->initUIData()V

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isRefresh:Z

    :cond_0
    return-void
.end method

.method public registmWifiReceiver()V
    .locals 3

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isUnregisted:Z

    if-eqz v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiReceiver:Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.RSSI_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.LINK_CONFIGURATION_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.CONFIGURED_NETWORKS_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.SCAN_RESULTS"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.WIFI_STATE_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.NETWORK_IDS_CHANGED"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    const-string v1, "android.net.wifi.STATE_CHANGE"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiReceiver:Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mFilter:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isUnregisted:Z

    :cond_0
    return-void
.end method

.method setOnListener()V
    .locals 5

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onFocusChangeListener:Landroid/view/View$OnFocusChangeListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onKeyListener:Landroid/view/View$OnKeyListener;

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f090072    # com.konka.systemsetting.R.id.wifi_container

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiContainer:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v4}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiContainer:Landroid/widget/LinearLayout;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->onGenericMotionListener:Landroid/view/View$OnGenericMotionListener;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnGenericMotionListener(Landroid/view/View$OnGenericMotionListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemSwitch:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x5

    if-lt v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnScan:Landroid/widget/Button;

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnDetect:Landroid/widget/Button;

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->btnWps:Landroid/widget/Button;

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$6;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/net/NetworkWifiConfig$6;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$onClickListener;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$topHPOnFocusChangeEvent;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$topHPOnFocusChangeEvent;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$topHPOnFocusChangeEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$hpOnKeyEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$bottomHPOnFocusChangeEvent;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$bottomHPOnFocusChangeEvent;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$bottomHPOnFocusChangeEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->itemHotspots:[Landroid/widget/LinearLayout;

    aget-object v1, v1, v0

    new-instance v2, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;

    invoke-direct {v2, p0, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;-><init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;Lcom/konka/systemsetting/net/NetworkWifiConfig$midHPOnFocusChangeEvent;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_1
.end method

.method public unregistWifiReceiver()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isUnregisted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->mWifiReceiver:Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiReceiver;

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig;->isUnregisted:Z

    :cond_0
    return-void
.end method
