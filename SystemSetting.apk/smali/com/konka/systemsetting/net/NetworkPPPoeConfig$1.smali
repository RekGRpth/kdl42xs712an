.class Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;
.super Landroid/os/Handler;
.source "NetworkPPPoeConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkPPPoeConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    :goto_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updatePppoeStatus()Landroid/net/pppoe/PPPOE_STA;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPppoeConnected()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v0}, Landroid/net/pppoe/PppoeManager;->PppoeHangUp()V

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;->this$0:Lcom/konka/systemsetting/net/NetworkPPPoeConfig;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updatePppoeStatus()Landroid/net/pppoe/PPPOE_STA;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
