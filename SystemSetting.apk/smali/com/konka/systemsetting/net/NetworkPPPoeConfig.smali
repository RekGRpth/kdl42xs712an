.class public Lcom/konka/systemsetting/net/NetworkPPPoeConfig;
.super Ljava/lang/Object;
.source "NetworkPPPoeConfig.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "NetworkPPPoeConfig"

.field private static mThread:Ljava/lang/Thread;


# instance fields
.field private IMG_SWITCH_OFF:I

.field private IMG_SWITCH_ON:I

.field private final PPPOE_TIMER_OUT:I

.field private final UI_PPPOE_UPDATE_MASSAGE:I

.field private final UI_PPPOE_UPDATE_THREAD_EXIT:I

.field private isAutoDial:Z

.field private isPwdShown:Z

.field private mBtnSetPppoe:Landroid/widget/Button;

.field mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private mEditName:Landroid/widget/EditText;

.field private mEditPwd:Landroid/widget/EditText;

.field private mHandler:Landroid/os/Handler;

.field private mItemAutoDial:Landroid/widget/LinearLayout;

.field private mItemName:Landroid/widget/LinearLayout;

.field private mItemPwd:Landroid/widget/LinearLayout;

.field private mItemShowPwd:Landroid/widget/LinearLayout;

.field mPPPOE:Landroid/net/pppoe/PppoeManager;

.field public mPppoeReceiver:Landroid/content/BroadcastReceiver;

.field private mPreferance:Lcom/konka/utilities/PreferancesTools;

.field private mStrName:Ljava/lang/String;

.field private mStrPwd:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mThread:Ljava/lang/Thread;

    return-void
.end method

.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 4
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/high16 v3, -0x1000000

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPwdShown:Z

    iput-boolean v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isAutoDial:Z

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->UI_PPPOE_UPDATE_MASSAGE:I

    const/4 v1, 0x1

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->UI_PPPOE_UPDATE_THREAD_EXIT:I

    const/16 v1, 0x1e

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->PPPOE_TIMER_OUT:I

    iput-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    iput v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_ON:I

    iput v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_OFF:I

    new-instance v1, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$1;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mHandler:Landroid/os/Handler;

    new-instance v1, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$2;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$2;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPppoeReceiver:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-static {}, Landroid/net/pppoe/PppoeManager;->getInstance()Landroid/net/pppoe/PppoeManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    new-instance v1, Lcom/konka/utilities/PreferancesTools;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v1, v2}, Lcom/konka/utilities/PreferancesTools;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->initUIComponents()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->initPPPOEData()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateUIState()V

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.net.pppoe.PPPOE_STATE_ACTION"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPppoeReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p1, v1, v0}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/EditText;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;I)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->onKeyEnterDown(I)Z

    move-result v0

    return v0
.end method

.method private btnSetClick()V
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPppoeActive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->cutdownDailing()V

    :goto_0
    return-void

    :cond_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->startDailing()V

    goto :goto_0
.end method

.method private changeAutoDialMode()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isAutoDial:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isAutoDial:Z

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateAutoDialUI()V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private changeShowPwdMode()V
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPwdShown:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPwdShown:Z

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateShowPwdUI()V

    return-void

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private cutdownDailing()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->pppoeHangUp()V

    return-void
.end method

.method private getAutoDial()Ljava/lang/Boolean;
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v1, "pppoe_autodial"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/konka/utilities/PreferancesTools;->getBooleanData(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    return-object v0
.end method

.method private getSwitchImages()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f060001    # com.konka.systemsetting.R.string.lang_flag

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "zh_cn"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f020011    # com.konka.systemsetting.R.drawable.on

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_ON:I

    const v1, 0x7f020010    # com.konka.systemsetting.R.drawable.off

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_OFF:I

    :goto_0
    return-void

    :cond_0
    const-string v1, "zh_tw"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const v1, 0x7f020046    # com.konka.systemsetting.R.drawable.zhtw_on

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_ON:I

    const v1, 0x7f020045    # com.konka.systemsetting.R.drawable.zhtw_off

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_OFF:I

    goto :goto_0

    :cond_1
    const v1, 0x7f020003    # com.konka.systemsetting.R.drawable.en_on

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_ON:I

    const v1, 0x7f020002    # com.konka.systemsetting.R.drawable.en_off

    iput v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_OFF:I

    goto :goto_0
.end method

.method private getUserName()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v1, "pppoe_usr"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/konka/utilities/PreferancesTools;->getStringPref(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getUserPwd()Ljava/lang/String;
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v1, "pppoe_pwd"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/konka/utilities/PreferancesTools;->getStringPref(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private initUIComponents()V
    .locals 6

    const v5, 0x7f09003a    # com.konka.systemsetting.R.id.sys_network_pppoe_set

    const v4, 0x7f090034    # com.konka.systemsetting.R.id.sys_network_pppoe_username

    const/4 v3, 0x1

    const v2, 0x7f09003e    # com.konka.systemsetting.R.id.linearlayout_network_tab_pppoe

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v4}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090036    # com.konka.systemsetting.R.id.sys_network_pppoe_password

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090039    # com.konka.systemsetting.R.id.sys_network_pppoe_showpwd

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090038    # com.konka.systemsetting.R.id.sys_network_pppoe_autodial

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v5}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090035    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_username

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090037    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_password

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-direct {p0, v0, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-direct {p0, v0, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setItemEnabled(Landroid/view/View;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    invoke-direct {p0, v0, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setItemEnabled(Landroid/view/View;Z)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setOnClickListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setOnKeyListener()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->setOnFocusChangeListener()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-virtual {v0, v2}, Landroid/widget/EditText;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    invoke-virtual {v0, v5}, Landroid/widget/Button;->setNextFocusDownId(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->getSwitchImages()V

    return-void
.end method

.method private onKeyEnterDown(I)Z
    .locals 4
    .param p1    # I

    const-string v2, "input_method"

    const/4 v0, 0x2

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3, v2}, Lcom/konka/systemsetting/MainActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/inputmethod/InputMethodManager;

    packed-switch p1, :pswitch_data_0

    const/4 v3, 0x0

    :goto_0
    return v3

    :pswitch_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->changeAutoDialMode()V

    :goto_1
    const/4 v3, 0x1

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->changeShowPwdMode()V

    goto :goto_1

    :pswitch_2
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->btnSetClick()V

    goto :goto_1

    :pswitch_3
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    :pswitch_4
    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-virtual {v3}, Landroid/widget/EditText;->requestFocus()Z

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-virtual {v1, v3, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x7f090034
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_pppoe_username
        :pswitch_3    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_username
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_pppoe_password
        :pswitch_4    # com.konka.systemsetting.R.id.sys_network_pppoe_edittext_password
        :pswitch_0    # com.konka.systemsetting.R.id.sys_network_pppoe_autodial
        :pswitch_1    # com.konka.systemsetting.R.id.sys_network_pppoe_showpwd
        :pswitch_2    # com.konka.systemsetting.R.id.sys_network_pppoe_set
    .end packed-switch
.end method

.method private saveUserData(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;
    .param p3    # Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v1, "pppoe_usr"

    invoke-virtual {v0, v1, p1}, Lcom/konka/utilities/PreferancesTools;->setStringPref(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v1, "pppoe_pwd"

    invoke-virtual {v0, v1, p2}, Lcom/konka/utilities/PreferancesTools;->setStringPref(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v1, "pppoe_autodial"

    invoke-virtual {v0, v1, p3}, Lcom/konka/utilities/PreferancesTools;->setBooleanPref(Ljava/lang/String;Z)V

    return-void
.end method

.method private setItemEnabled(Landroid/view/View;Z)V
    .locals 0
    .param p1    # Landroid/view/View;
    .param p2    # Z

    invoke-virtual {p1, p2}, Landroid/view/View;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    return-void
.end method

.method private setOnClickListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$4;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$4;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    return-void
.end method

.method private setOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$3;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-virtual {v1, v0}, Landroid/widget/EditText;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method private setOnKeyListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$5;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$5;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemName:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    invoke-virtual {v1, v0}, Landroid/widget/Button;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method private startDailing()V
    .locals 4

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-virtual {v2}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v2

    invoke-interface {v2}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v3, 0x7f0600aa    # com.konka.systemsetting.R.string.null_username_or_pwd

    invoke-virtual {v2, v3}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    :goto_0
    return-void

    :cond_1
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->checkPPPOEDialer()V

    goto :goto_0
.end method

.method private updateAutoDialUI()V
    .locals 4

    iget-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isAutoDial:Z

    if-eqz v2, :cond_0

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_ON:I

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemAutoDial:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    return-void

    :cond_0
    iget v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_OFF:I

    goto :goto_0
.end method

.method private updateBtnUI()V
    .locals 2

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPppoeActive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    const v1, 0x7f06009e    # com.konka.systemsetting.R.string.pppoe_cutdown

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mBtnSetPppoe:Landroid/widget/Button;

    const v1, 0x7f06009d    # com.konka.systemsetting.R.string.pppoe_dial

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(I)V

    goto :goto_0
.end method

.method private updateShowPwdUI()V
    .locals 4

    iget-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPwdShown:Z

    if-eqz v2, :cond_0

    iget v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_ON:I

    :goto_0
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mItemShowPwd:Landroid/widget/LinearLayout;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    iget-boolean v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPwdShown:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    const/16 v3, 0x90

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    :goto_1
    return-void

    :cond_0
    iget v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->IMG_SWITCH_OFF:I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    const/16 v3, 0x81

    invoke-virtual {v2, v3}, Landroid/widget/EditText;->setInputType(I)V

    goto :goto_1
.end method

.method private updateUIState()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateShowPwdUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateAutoDialUI()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateBtnUI()V

    return-void
.end method


# virtual methods
.method public autoDailer()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v0}, Landroid/net/pppoe/PppoeManager;->PppoeMonitor()V

    return-void
.end method

.method public checkPPPOEDialer()V
    .locals 4

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    invoke-virtual {v1}, Landroid/widget/EditText;->getText()Landroid/text/Editable;

    move-result-object v1

    invoke-interface {v1}, Landroid/text/Editable;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    const/4 v0, 0x0

    if-nez v0, :cond_0

    const-string v0, "eth0"

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->PppoeHangUp()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1, v0}, Landroid/net/pppoe/PppoeManager;->PppoeSetInterface(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/net/pppoe/PppoeManager;->PppoeSetUser(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/net/pppoe/PppoeManager;->PppoeSetPW(Ljava/lang/String;)Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->PppoeDialup()Z

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0600a9    # com.konka.systemsetting.R.string.check_username_password

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->pppoeDialer()V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    iget-boolean v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isAutoDial:Z

    invoke-direct {p0, v1, v2, v3}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->saveUserData(Ljava/lang/String;Ljava/lang/String;Z)V

    :cond_1
    return-void
.end method

.method public getPPPOEstatus()Landroid/net/pppoe/PPPOE_STA;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v0}, Landroid/net/pppoe/PppoeManager;->PppoeGetStatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v0

    return-object v0
.end method

.method public initPPPOEData()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->getAutoDial()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isAutoDial:Z

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->isPwdShown:Z

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->getUserName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->getUserPwd()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mStrPwd:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditName:Landroid/widget/EditText;

    const v1, 0x7f0600a5    # com.konka.systemsetting.R.string.input_username

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mEditPwd:Landroid/widget/EditText;

    const v1, 0x7f0600a6    # com.konka.systemsetting.R.string.input_password

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(I)V

    goto :goto_0
.end method

.method public isPppoeActive()Z
    .locals 2

    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->getPPPOEstatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const-string v0, "===>Net_PPPOE_Cfg"

    const-string v1, "===>PPPOE_STA.Connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const-string v0, "===>Net_PPPOE_Cfg"

    const-string v1, "===>pppoe DISCONNECTED"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isPppoeConnected()Z
    .locals 2

    sget-object v0, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->getPPPOEstatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isWirePPPoE()Z
    .locals 7

    invoke-static {}, Landroid/net/ethernet/EthernetManager;->getInstance()Landroid/net/ethernet/EthernetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/ethernet/EthernetManager;->getSavedConfig()Landroid/net/ethernet/EthernetDevInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/ethernet/EthernetDevInfo;->getIfName()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v1, "eth0"

    :cond_0
    iget-object v4, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v4}, Landroid/net/pppoe/PppoeManager;->PppoeGetInterface()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "===>pppoe interface="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    if-eqz v3, :cond_1

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v4, 0x1

    :goto_0
    return v4

    :cond_1
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public pppoeDialer()V
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->cancelSelf()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    :cond_0
    new-instance v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    invoke-direct {v0, p0, v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;Landroid/net/pppoe/PPPOE_STA;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->start()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ThreadStart id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method public pppoeHangUp()V
    .locals 4

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->cancelSelf()V

    iput-object v3, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    :cond_0
    new-instance v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    invoke-direct {v0, p0, v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;-><init>(Lcom/konka/systemsetting/net/NetworkPPPoeConfig;Landroid/net/pppoe/PPPOE_STA;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->start()V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ThreadStart id="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mCheckThread:Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig$CheckPPPoEThread;->getId()J

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v0}, Landroid/net/pppoe/PppoeManager;->PppoeHangUp()V

    const-string v0, "ppppoe hang up"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    sget-object v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mThread:Ljava/lang/Thread;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    sput-object v3, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mThread:Ljava/lang/Thread;

    :cond_1
    return-void
.end method

.method public updatePppoeStatus()Landroid/net/pppoe/PPPOE_STA;
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->PppoeGetStatus()Landroid/net/pppoe/PPPOE_STA;

    move-result-object v0

    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTED:Landroid/net/pppoe/PPPOE_STA;

    if-ne v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0600ab    # com.konka.systemsetting.R.string.ppoe_dial_success

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "PPPOE_STA.CONNECTED"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->updateBtnUI()V

    return-object v0

    :cond_1
    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->DISCONNECTED:Landroid/net/pppoe/PPPOE_STA;

    if-ne v0, v1, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0600a7    # com.konka.systemsetting.R.string.pppoe_disconnect

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    sget-object v1, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v2, "PPPOE_STA.DISCONNECTED"

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    sget-object v1, Landroid/net/pppoe/PPPOE_STA;->CONNECTING:Landroid/net/pppoe/PPPOE_STA;

    if-ne v0, v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkPPPoeConfig;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v2, 0x7f0600a8    # com.konka.systemsetting.R.string.is_dialing

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/MainActivity;->toastText(I)V

    goto :goto_0
.end method
