.class final enum Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;
.super Ljava/lang/Enum;
.source "NetworkWifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4018
    name = "BG_TYPE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum BOTTOM:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

.field private static final synthetic ENUM$VALUES:[Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

.field public static final enum MID:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

.field public static final enum NULL:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

.field public static final enum SINGLE:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

.field public static final enum TOP:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    const-string v1, "SINGLE"

    invoke-direct {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->SINGLE:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    const-string v1, "TOP"

    invoke-direct {v0, v1, v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->TOP:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    const-string v1, "MID"

    invoke-direct {v0, v1, v4}, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->MID:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    const-string v1, "BOTTOM"

    invoke-direct {v0, v1, v5}, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->BOTTOM:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    new-instance v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    const-string v1, "NULL"

    invoke-direct {v0, v1, v6}, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->NULL:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    sget-object v1, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->SINGLE:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->TOP:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->MID:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->BOTTOM:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->NULL:Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    aput-object v1, v0, v6

    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->ENUM$VALUES:[Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;
    .locals 1

    const-class v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;->ENUM$VALUES:[Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/konka/systemsetting/net/NetworkWifiConfig$BG_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
