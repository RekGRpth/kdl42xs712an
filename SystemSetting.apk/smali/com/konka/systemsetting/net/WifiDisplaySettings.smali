.class public final Lcom/konka/systemsetting/net/WifiDisplaySettings;
.super Lcom/konka/systemsetting/SettingsPreferenceFragment;
.source "WifiDisplaySettings.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;
    }
.end annotation


# static fields
.field private static final MENU_ID_SCAN:I = 0x1

.field private static final TAG:Ljava/lang/String; = "WifiDisplaySettings"


# instance fields
.field private mActionBarSwitch:Landroid/widget/Switch;

.field private mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

.field private mDisplayManager:Landroid/hardware/display/DisplayManager;

.field private mEmptyView:Landroid/widget/TextView;

.field private mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private final mSettingsObserver:Landroid/database/ContentObserver;

.field private final mSwitchOnCheckedChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

.field private mWifiDisplayOnSetting:Z

.field private mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

.field private mWifiManager:Landroid/net/wifi/WifiManager;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;-><init>()V

    new-instance v0, Lcom/konka/systemsetting/net/WifiDisplaySettings$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings$1;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mSwitchOnCheckedChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    new-instance v0, Lcom/konka/systemsetting/net/WifiDisplaySettings$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings$2;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mReceiver:Landroid/content/BroadcastReceiver;

    new-instance v0, Lcom/konka/systemsetting/net/WifiDisplaySettings$3;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$3;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mSettingsObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/WifiDisplaySettings;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayOnSetting:Z

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/WifiDisplaySettings;)Landroid/content/ContentResolver;
    .locals 1

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/hardware/display/WifiDisplayStatus;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->applyState()V

    return-void
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/net/WifiDisplaySettings;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->update()V

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/hardware/display/WifiDisplay;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->showOptionsDialog(Landroid/hardware/display/WifiDisplay;)V

    return-void
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/net/WifiDisplaySettings;)Landroid/hardware/display/WifiDisplayStatus;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    return-object v0
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/net/WifiDisplaySettings;)Landroid/hardware/display/DisplayManager;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    return-object v0
.end method

.method private applyState()V
    .locals 11

    const/4 v6, 0x1

    const/4 v7, 0x0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v5}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v2

    const-string v5, "WifiDisplaySettings"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "featureState: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v8, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mActionBarSwitch:Landroid/widget/Switch;

    if-eq v2, v6, :cond_1

    move v5, v6

    :goto_0
    invoke-virtual {v8, v5}, Landroid/widget/Switch;->setEnabled(Z)V

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mActionBarSwitch:Landroid/widget/Switch;

    iget-boolean v8, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayOnSetting:Z

    invoke-virtual {v5, v8}, Landroid/widget/Switch;->setChecked(Z)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v4

    invoke-virtual {v4}, Landroid/preference/PreferenceScreen;->removeAll()V

    const/4 v5, 0x3

    if-ne v2, v5, :cond_8

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v5}, Landroid/hardware/display/WifiDisplayStatus;->getRememberedDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v3

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v5}, Landroid/hardware/display/WifiDisplayStatus;->getAvailableDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    if-nez v5, :cond_2

    new-instance v5, Landroid/preference/PreferenceCategory;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    invoke-direct {v5, v8}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    iput-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    const v8, 0x7f060138    # com.konka.systemsetting.R.string.wifi_display_paired_devices

    invoke-virtual {v5, v8}, Landroid/preference/PreferenceGroup;->setTitle(I)V

    :goto_1
    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    array-length v8, v3

    move v5, v7

    :goto_2
    if-lt v5, v8, :cond_3

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->getPreferenceCount()I

    move-result v5

    if-nez v5, :cond_0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    :cond_0
    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    if-nez v5, :cond_4

    new-instance v5, Lcom/konka/systemsetting/ProgressCategory;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v8

    const/4 v9, 0x0

    invoke-direct {v5, v8, v9}, Lcom/konka/systemsetting/ProgressCategory;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    iput-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    const v8, 0x7f06013a    # com.konka.systemsetting.R.string.wifi_display_available_devices

    invoke-virtual {v5, v8}, Lcom/konka/systemsetting/ProgressCategory;->setTitle(I)V

    :goto_3
    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    invoke-virtual {v4, v5}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    array-length v8, v0

    move v5, v7

    :goto_4
    if-lt v5, v8, :cond_5

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v5}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v5

    if-ne v5, v6, :cond_7

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    invoke-virtual {v5, v6}, Lcom/konka/systemsetting/ProgressCategory;->setProgress(Z)V

    :goto_5
    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->invalidateOptionsMenu()V

    return-void

    :cond_1
    move v5, v7

    goto/16 :goto_0

    :cond_2
    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    invoke-virtual {v5}, Landroid/preference/PreferenceGroup;->removeAll()V

    goto :goto_1

    :cond_3
    aget-object v1, v3, v5

    iget-object v9, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mPairedDevicesCategory:Landroid/preference/PreferenceGroup;

    invoke-direct {p0, v1, v6}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->createWifiDisplayPreference(Landroid/hardware/display/WifiDisplay;Z)Landroid/preference/Preference;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/preference/PreferenceGroup;->addPreference(Landroid/preference/Preference;)Z

    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    :cond_4
    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    invoke-virtual {v5}, Lcom/konka/systemsetting/ProgressCategory;->removeAll()V

    goto :goto_3

    :cond_5
    aget-object v1, v0, v5

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v9}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->contains([Landroid/hardware/display/WifiDisplay;Ljava/lang/String;)Z

    move-result v9

    if-nez v9, :cond_6

    iget-object v9, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    invoke-direct {p0, v1, v7}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->createWifiDisplayPreference(Landroid/hardware/display/WifiDisplay;Z)Landroid/preference/Preference;

    move-result-object v10

    invoke-virtual {v9, v10}, Lcom/konka/systemsetting/ProgressCategory;->addPreference(Landroid/preference/Preference;)Z

    :cond_6
    add-int/lit8 v5, v5, 0x1

    goto :goto_4

    :cond_7
    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mAvailableDevicesCategory:Lcom/konka/systemsetting/ProgressCategory;

    invoke-virtual {v5, v7}, Lcom/konka/systemsetting/ProgressCategory;->setProgress(Z)V

    goto :goto_5

    :cond_8
    iget-object v6, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mEmptyView:Landroid/widget/TextView;

    const/4 v5, 0x2

    if-ne v2, v5, :cond_9

    const v5, 0x7f06013b    # com.konka.systemsetting.R.string.wifi_display_settings_empty_list_wifi_display_off

    :goto_6
    invoke-virtual {v6, v5}, Landroid/widget/TextView;->setText(I)V

    goto :goto_5

    :cond_9
    const v5, 0x7f06013c    # com.konka.systemsetting.R.string.wifi_display_settings_empty_list_wifi_display_disabled

    goto :goto_6
.end method

.method private static contains([Landroid/hardware/display/WifiDisplay;Ljava/lang/String;)Z
    .locals 5
    .param p0    # [Landroid/hardware/display/WifiDisplay;
    .param p1    # Ljava/lang/String;

    const/4 v1, 0x0

    array-length v3, p0

    move v2, v1

    :goto_0
    if-lt v2, v3, :cond_0

    :goto_1
    return v1

    :cond_0
    aget-object v0, p0, v2

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method private createWifiDisplayPreference(Landroid/hardware/display/WifiDisplay;Z)Landroid/preference/Preference;
    .locals 3
    .param p1    # Landroid/hardware/display/WifiDisplay;
    .param p2    # Z

    new-instance v0, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-direct {v0, p0, v1, p1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/content/Context;Landroid/hardware/display/WifiDisplay;)V

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplayState()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    :cond_0
    :goto_0
    if-eqz p2, :cond_1

    const v1, 0x7f030028    # com.konka.systemsetting.R.layout.wifi_display_preference

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;->setWidgetLayoutResource(I)V

    :cond_1
    return-object v0

    :pswitch_0
    const v1, 0x7f06013d    # com.konka.systemsetting.R.string.wifi_display_status_connected

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;->setSummary(I)V

    goto :goto_0

    :pswitch_1
    const v1, 0x7f06013e    # com.konka.systemsetting.R.string.wifi_display_status_connecting

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;->setSummary(I)V

    goto :goto_0

    :cond_2
    if-eqz p2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getAvailableDisplays()[Landroid/hardware/display/WifiDisplay;

    move-result-object v1

    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->contains([Landroid/hardware/display/WifiDisplay;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    const v1, 0x7f06013f    # com.konka.systemsetting.R.string.wifi_display_status_available

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;->setSummary(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private showDisconnectDialog(Landroid/hardware/display/WifiDisplay;)V
    .locals 8
    .param p1    # Landroid/hardware/display/WifiDisplay;

    const/4 v5, 0x1

    new-instance v1, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;

    invoke-direct {v1, p0, p1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$4;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/hardware/display/WifiDisplay;)V

    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x7f060141    # com.konka.systemsetting.R.string.wifi_display_disconnect_title

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v4, 0x7f060140    # com.konka.systemsetting.R.string.wifi_display_disconnect_text

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getFriendlyDisplayName()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a    # android.R.string.ok

    invoke-virtual {v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const/high16 v3, 0x1040000    # android.R.string.cancel

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private showOptionsDialog(Landroid/hardware/display/WifiDisplay;)V
    .locals 8
    .param p1    # Landroid/hardware/display/WifiDisplay;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v5

    const v6, 0x7f030027    # com.konka.systemsetting.R.layout.wifi_display_options

    const/4 v7, 0x0

    invoke-virtual {v5, v6, v7}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    const v5, 0x7f0900b7    # com.konka.systemsetting.R.id.name

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/EditText;

    invoke-virtual {p1}, Landroid/hardware/display/WifiDisplay;->getFriendlyDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    new-instance v1, Lcom/konka/systemsetting/net/WifiDisplaySettings$5;

    invoke-direct {v1, p0, v3, p1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$5;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/widget/EditText;Landroid/hardware/display/WifiDisplay;)V

    new-instance v2, Lcom/konka/systemsetting/net/WifiDisplaySettings$6;

    invoke-direct {v2, p0, p1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$6;-><init>(Lcom/konka/systemsetting/net/WifiDisplaySettings;Landroid/hardware/display/WifiDisplay;)V

    new-instance v5, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v5, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v6, 0x1

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f060146    # com.konka.systemsetting.R.string.wifi_display_options_title

    invoke-virtual {v5, v6}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f060145    # com.konka.systemsetting.R.string.wifi_display_options_done

    invoke-virtual {v5, v6, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const v6, 0x7f060142    # com.konka.systemsetting.R.string.wifi_display_options_forget

    invoke-virtual {v5, v6, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    return-void
.end method

.method private update()V
    .locals 3

    const/4 v0, 0x0

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "wifi_display_on"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    iput-boolean v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayOnSetting:Z

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->getWifiDisplayStatus()Landroid/hardware/display/WifiDisplayStatus;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-direct {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->applyState()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 8
    .param p1    # Landroid/os/Bundle;

    const/16 v6, 0x10

    const/4 v7, -0x2

    const/4 v5, 0x0

    invoke-super {p0, p1}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v3, Landroid/widget/Switch;

    invoke-direct {v3, v0}, Landroid/widget/Switch;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mActionBarSwitch:Landroid/widget/Switch;

    instance-of v3, v0, Landroid/preference/PreferenceActivity;

    if-eqz v3, :cond_1

    move-object v2, v0

    check-cast v2, Landroid/preference/PreferenceActivity;

    invoke-virtual {v2}, Landroid/preference/PreferenceActivity;->onIsHidingHeaders()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v2}, Landroid/preference/PreferenceActivity;->onIsMultiPane()Z

    move-result v3

    if-nez v3, :cond_1

    :cond_0
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/high16 v4, 0x7f050000    # com.konka.systemsetting.R.dimen.action_bar_switch_padding

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iget-object v3, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mActionBarSwitch:Landroid/widget/Switch;

    invoke-virtual {v3, v5, v5, v1, v5}, Landroid/widget/Switch;->setPadding(IIII)V

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    invoke-virtual {v3, v6, v6}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    invoke-virtual {v0}, Landroid/app/Activity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mActionBarSwitch:Landroid/widget/Switch;

    new-instance v5, Landroid/app/ActionBar$LayoutParams;

    const v6, 0x800015

    invoke-direct {v5, v7, v7, v6}, Landroid/app/ActionBar$LayoutParams;-><init>(III)V

    invoke-virtual {v3, v4, v5}, Landroid/app/ActionBar;->setCustomView(Landroid/view/View;Landroid/app/ActionBar$LayoutParams;)V

    :cond_1
    iget-object v3, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mActionBarSwitch:Landroid/widget/Switch;

    iget-object v4, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mSwitchOnCheckedChangedListener:Landroid/widget/CompoundButton$OnCheckedChangeListener;

    invoke-virtual {v3, v4}, Landroid/widget/Switch;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "wifi_display_on"

    const/4 v5, 0x1

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getView()Landroid/view/View;

    move-result-object v3

    const v4, 0x1020004    # android.R.id.empty

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getListView()Landroid/widget/ListView;

    move-result-object v3

    iget-object v4, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mEmptyView:Landroid/widget/TextView;

    invoke-virtual {v3, v4}, Landroid/widget/ListView;->setEmptyView(Landroid/view/View;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->update()V

    iget-object v3, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v3}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    iput-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiManager:Landroid/net/wifi/WifiManager;

    const v0, 0x7f040004    # com.konka.systemsetting.R.xml.wifi_display_settings

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->addPreferencesFromResource(I)V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->setHasOptionsMenu(Z)V

    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 5
    .param p1    # Landroid/view/Menu;
    .param p2    # Landroid/view/MenuInflater;

    const/4 v3, 0x0

    const/4 v2, 0x1

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v1

    if-ne v1, v2, :cond_0

    const v1, 0x7f060137    # com.konka.systemsetting.R.string.wifi_display_searching_for_devices

    :goto_0
    invoke-interface {p1, v3, v2, v3, v1}, Landroid/view/Menu;->add(IIII)Landroid/view/MenuItem;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v1

    const/4 v4, 0x3

    if-ne v1, v4, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v1}, Landroid/hardware/display/WifiDisplayStatus;->getScanState()I

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-interface {v0, v1}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    invoke-interface {v0, v2}, Landroid/view/MenuItem;->setShowAsAction(I)V

    invoke-super {p0, p1, p2}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V

    return-void

    :cond_0
    const v1, 0x7f060139    # com.konka.systemsetting.R.string.wifi_display_search_for_devices

    goto :goto_0

    :cond_1
    move v1, v3

    goto :goto_1
.end method

.method public onDestroy()V
    .locals 0

    invoke-super {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onDestroy()V

    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1    # Landroid/view/MenuItem;

    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    invoke-super {p0, p1}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    :goto_0
    return v0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplayStatus;->getFeatureState()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onPause()V
    .locals 3

    invoke-super {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onPause()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    return-void
.end method

.method public onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4
    .param p1    # Landroid/preference/PreferenceScreen;
    .param p2    # Landroid/preference/Preference;

    instance-of v2, p2, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;

    if-eqz v2, :cond_0

    move-object v1, p2

    check-cast v1, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;

    invoke-virtual {v1}, Lcom/konka/systemsetting/net/WifiDisplaySettings$WifiDisplayPreference;->getDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v0

    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mWifiDisplayStatus:Landroid/hardware/display/WifiDisplayStatus;

    invoke-virtual {v2}, Landroid/hardware/display/WifiDisplayStatus;->getActiveDisplay()Landroid/hardware/display/WifiDisplay;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/hardware/display/WifiDisplay;->equals(Landroid/hardware/display/WifiDisplay;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->showDisconnectDialog(Landroid/hardware/display/WifiDisplay;)V

    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z

    move-result v2

    return v2

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/WifiDisplay;->getDeviceAddress()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/hardware/display/DisplayManager;->connectWifiDisplay(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onResume()V
    .locals 6

    invoke-super {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onResume()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getActivity()Landroid/app/Activity;

    move-result-object v0

    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    const-string v2, "android.hardware.display.action.WIFI_DISPLAY_STATUS_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "wifi_display_on"

    invoke-static {v3}, Landroid/provider/Settings$Secure;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mSettingsObserver:Landroid/database/ContentObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    iget-object v2, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v2}, Landroid/hardware/display/DisplayManager;->scanWifiDisplays()V

    invoke-direct {p0}, Lcom/konka/systemsetting/net/WifiDisplaySettings;->update()V

    return-void
.end method

.method public onStop()V
    .locals 1

    invoke-super {p0}, Lcom/konka/systemsetting/SettingsPreferenceFragment;->onStop()V

    iget-object v0, p0, Lcom/konka/systemsetting/net/WifiDisplaySettings;->mDisplayManager:Landroid/hardware/display/DisplayManager;

    invoke-virtual {v0}, Landroid/hardware/display/DisplayManager;->disconnectWifiDisplay()V

    return-void
.end method
