.class Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;
.super Ljava/lang/Object;
.source "NetworkSoftApViewHolder.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OnKeyEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;-><init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 2
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v0, 0x0

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v1

    if-nez v1, :cond_0

    sparse-switch p2, :sswitch_data_0

    :cond_0
    :goto_0
    return v0

    :sswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemOnClick(I)Z

    move-result v0

    goto :goto_0

    :sswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemOnKeyUp(I)Z

    move-result v0

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnKeyEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->itemOnKeyDown(I)Z

    move-result v0

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
        0x17 -> :sswitch_0
        0x42 -> :sswitch_0
    .end sparse-switch
.end method
