.class Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;
.super Ljava/lang/Object;
.source "NetworkSoftApViewHolder.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "OnFocuseChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 3
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, v0, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    iget-object v1, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, v1, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, v2, Lcom/konka/systemsetting/MainActivity;->m_networkPageManager:Lcom/konka/systemsetting/net/NetworkPageManager;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/konka/systemsetting/net/NetworkPageManager;->setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->getFocusdItemBGById(I)I
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->access$0(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etPassword:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->etSSID:Landroid/widget/EditText;

    invoke-virtual {v0}, Landroid/widget/EditText;->requestFocus()Z

    goto :goto_0

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder$OnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    # invokes: Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->getUnfocusdItemBGById(I)I
    invoke-static {v0, v1}, Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;->access$1(Lcom/konka/systemsetting/net/NetworkSoftApViewHolder;I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090047
        :pswitch_2    # com.konka.systemsetting.R.id.softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.et_softap_ssid
        :pswitch_0    # com.konka.systemsetting.R.id.softap_security
        :pswitch_1    # com.konka.systemsetting.R.id.softap_password
    .end packed-switch
.end method
