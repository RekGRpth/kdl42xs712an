.class public Lcom/konka/systemsetting/net/Constants;
.super Ljava/lang/Object;
.source "Constants.java"


# static fields
.field public static final CONNECT_FORMAT:I = 0xc

.field public static final DEFAULT_GATEWAY:I = 0x7

.field public static final FIRST_DNS:I = 0x8

.field public static final IP_ADDRESS:I = 0x5

.field public static final IS_AUTO_IP_ADDRESS:I = 0xa

.field public static final NET_STATE:I = 0x0

.field public static final PPPOE_AUTO_DIALER:I = 0x12

.field public static final PPPOE_AUTO_IP:I = 0xe

.field public static final PPPOE_DIALER:I = 0xd

.field public static final PPPOE_PASSWORD:I = 0x10

.field public static final PPPOE_PASSWORD_SHOW:I = 0x11

.field public static final PPPOE_SETTING:I = 0x3

.field public static final PPPOE_USERNAME:I = 0xf

.field public static final SECOND_DNS:I = 0x9

.field public static final SSID:I = 0xb

.field public static final SUBNET_MASK:I = 0x6

.field public static final WIRELESS_SETTING:I = 0x2

.field public static final WIRE_AUTO_IP_ADDRESS:I = 0x4

.field public static final WIRE_SETTING:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
