.class public Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;
.super Landroid/app/Service;
.source "PPPOEIsAutoDialerService.java"


# static fields
.field private static final PPPOE_TIPS:I = 0x1


# instance fields
.field private mHandler:Landroid/os/Handler;

.field mPPPOE:Landroid/net/pppoe/PppoeManager;

.field private mPreferance:Lcom/konka/utilities/PreferancesTools;

.field private updateState:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->updateState:Ljava/lang/Runnable;

    iput-object v0, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->showNotification(II)V

    return-void
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)Ljava/lang/Runnable;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->updateState:Ljava/lang/Runnable;

    return-object v0
.end method

.method private showNotification(II)V
    .locals 7
    .param p1    # I
    .param p2    # I

    const-string v4, "notification"

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/NotificationManager;

    new-instance v2, Landroid/app/Notification;

    const v4, 0x7f0600a2    # com.konka.systemsetting.R.string.new_msg

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-direct {v2, p1, v4, v5, v6}, Landroid/app/Notification;-><init>(ILjava/lang/CharSequence;J)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const/high16 v4, 0x10200000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    const/4 v4, 0x0

    const/high16 v5, 0x8000000

    invoke-static {p0, v4, v1, v5}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    const v4, 0x7f0600a3    # com.konka.systemsetting.R.string.pppoe_tips

    invoke-virtual {p0, v4}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p0, p2}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, p0, v4, v5, v0}, Landroid/app/Notification;->setLatestEventInfo(Landroid/content/Context;Ljava/lang/CharSequence;Ljava/lang/CharSequence;Landroid/app/PendingIntent;)V

    iget v4, v2, Landroid/app/Notification;->flags:I

    or-int/lit8 v4, v4, 0x10

    iput v4, v2, Landroid/app/Notification;->flags:I

    const/4 v4, 0x1

    invoke-virtual {v3, v4, v2}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1    # Landroid/content/Intent;

    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    new-instance v1, Lcom/konka/utilities/PreferancesTools;

    invoke-direct {v1, p0}, Lcom/konka/utilities/PreferancesTools;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPreferance:Lcom/konka/utilities/PreferancesTools;

    const-string v2, "pppoe_autodial"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/konka/utilities/PreferancesTools;->getBooleanData(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/net/pppoe/PppoeManager;->getInstance()Landroid/net/pppoe/PppoeManager;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    iget-object v1, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->mPPPOE:Landroid/net/pppoe/PppoeManager;

    invoke-virtual {v1}, Landroid/net/pppoe/PppoeManager;->PppoeDialup()Z

    const-string v1, "PPPOEIsAutoDialerService"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->pppoeDialer()V

    :goto_0
    return-void

    :cond_0
    const-string v1, "do not auto dial"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public pppoeDialer()V
    .locals 1

    new-instance v0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$1;-><init>(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)V

    iput-object v0, p0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;->updateState:Ljava/lang/Runnable;

    new-instance v0, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;-><init>(Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService;)V

    invoke-virtual {v0}, Lcom/konka/systemsetting/net/PPPOEIsAutoDialerService$2;->start()V

    return-void
.end method
