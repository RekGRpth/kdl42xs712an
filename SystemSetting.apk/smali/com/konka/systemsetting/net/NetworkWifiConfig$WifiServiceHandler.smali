.class Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;
.super Landroid/os/Handler;
.source "NetworkWifiConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/net/NetworkWifiConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WifiServiceHandler"
.end annotation


# static fields
.field private static synthetic $SWITCH_TABLE$android$net$wifi$WpsResult$Status:[I


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;


# direct methods
.method static synthetic $SWITCH_TABLE$android$net$wifi$WpsResult$Status()[I
    .locals 3

    sget-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->$SWITCH_TABLE$android$net$wifi$WpsResult$Status:[I

    if-eqz v0, :cond_0

    :goto_0
    return-object v0

    :cond_0
    invoke-static {}, Landroid/net/wifi/WpsResult$Status;->values()[Landroid/net/wifi/WpsResult$Status;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    :try_start_0
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->FAILURE:Landroid/net/wifi/WpsResult$Status;

    invoke-virtual {v1}, Landroid/net/wifi/WpsResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_2

    :goto_1
    :try_start_1
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->IN_PROGRESS:Landroid/net/wifi/WpsResult$Status;

    invoke-virtual {v1}, Landroid/net/wifi/WpsResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    :try_start_2
    sget-object v1, Landroid/net/wifi/WpsResult$Status;->SUCCESS:Landroid/net/wifi/WpsResult$Status;

    invoke-virtual {v1}, Landroid/net/wifi/WpsResult$Status;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_0

    :goto_3
    sput-object v0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->$SWITCH_TABLE$android$net$wifi$WpsResult$Status:[I

    goto :goto_0

    :catch_0
    move-exception v1

    goto :goto_3

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v1

    goto :goto_1
.end method

.method private constructor <init>(Lcom/konka/systemsetting/net/NetworkWifiConfig;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 5
    .param p1    # Landroid/os/Message;

    iget v2, p1, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    return-void

    :sswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkWifiConfig;->Tag:Ljava/lang/String;

    const-string v3, "AsyncChannel.STATUS_SUCCESSFUL"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget-object v2, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    iget-object v2, v2, Lcom/konka/systemsetting/net/NetworkWifiConfig;->Tag:Ljava/lang/String;

    const-string v3, "Failed to establish AsyncChannel connection"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :sswitch_1
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Landroid/net/wifi/WpsResult;

    if-eqz v1, :cond_0

    new-instance v2, Landroid/app/AlertDialog$Builder;

    iget-object v3, p0, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->this$0:Lcom/konka/systemsetting/net/NetworkWifiConfig;

    # getter for: Lcom/konka/systemsetting/net/NetworkWifiConfig;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v3}, Lcom/konka/systemsetting/net/NetworkWifiConfig;->access$4(Lcom/konka/systemsetting/net/NetworkWifiConfig;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const-string v3, "WPS\u4e00\u952e\u8fde\u63a5\uff1a"

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    const v3, 0x104000a    # android.R.string.ok

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-static {}, Lcom/konka/systemsetting/net/NetworkWifiConfig$WifiServiceHandler;->$SWITCH_TABLE$android$net$wifi$WpsResult$Status()[I

    move-result-object v2

    iget-object v3, v1, Landroid/net/wifi/WpsResult;->status:Landroid/net/wifi/WpsResult$Status;

    invoke-virtual {v3}, Landroid/net/wifi/WpsResult$Status;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    const-string v2, "\u65e0\u6cd5\u542f\u52a8 WPS\uff0c\u8bf7\u91cd\u8bd5"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    :pswitch_1
    const-string v2, "\u6b63\u5728\u8fdb\u884c WPS\uff0c\u8fd9\u4e00\u8fc7\u7a0b\u53ef\u80fd\u9700\u8981\u6570\u5341\u79d2\u7684\u65f6!"

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x11000 -> :sswitch_0
        0x2500d -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
