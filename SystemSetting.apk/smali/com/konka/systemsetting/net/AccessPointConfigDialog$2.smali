.class Lcom/konka/systemsetting/net/AccessPointConfigDialog$2;
.super Ljava/lang/Object;
.source "AccessPointConfigDialog.java"

# interfaces
.implements Landroid/net/wifi/WifiManager$ActionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/net/AccessPointConfigDialog;-><init>(Landroid/content/Context;Landroid/net/wifi/ScanResult;Landroid/net/wifi/WifiManager;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialog;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog$2;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFailure(I)V
    .locals 3
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog$2;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    # getter for: Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->access$0(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    const v1, 0x7f060094    # com.konka.systemsetting.R.string.wifi_connect_failed

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method

.method public onSuccess()V
    .locals 3

    iget-object v0, p0, Lcom/konka/systemsetting/net/AccessPointConfigDialog$2;->this$0:Lcom/konka/systemsetting/net/AccessPointConfigDialog;

    # getter for: Lcom/konka/systemsetting/net/AccessPointConfigDialog;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v0}, Lcom/konka/systemsetting/net/AccessPointConfigDialog;->access$0(Lcom/konka/systemsetting/net/AccessPointConfigDialog;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v0

    const v1, 0x7f06008f    # com.konka.systemsetting.R.string.wifi_authenticating

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    return-void
.end method
