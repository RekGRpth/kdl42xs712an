.class Lcom/konka/systemsetting/individ/IndividTimeSetting$6;
.super Ljava/lang/Object;
.source "IndividTimeSetting.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividTimeSetting;->showTimeZonePickerDialog()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$6;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1    # Landroid/content/DialogInterface;
    .param p2    # I

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$6;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->userSettings:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$14(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "timeZoneSetting"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$6;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$15(Lcom/konka/systemsetting/individ/IndividTimeSetting;Ljava/lang/Integer;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$6;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeZoneSelectUI()V
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$16(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    return-void
.end method
