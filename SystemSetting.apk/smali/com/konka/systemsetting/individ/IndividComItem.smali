.class public Lcom/konka/systemsetting/individ/IndividComItem;
.super Ljava/lang/Object;
.source "IndividComItem.java"

# interfaces
.implements Lcom/konka/systemsetting/interfaces/IComItemState;
.implements Lcom/konka/systemsetting/interfaces/IUpdateSysData;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/individ/IndividComItem$ItemOnClickEvent;,
        Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;,
        Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;
    }
.end annotation


# instance fields
.field private default_Background:Landroid/graphics/drawable/Drawable;

.field private iValueIndex:Ljava/lang/Integer;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private strItemValues:[Ljava/lang/CharSequence;

.field protected textview_RangeValue:Landroid/widget/TextView;

.field private viewgroup_Container:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # Ljava/lang/Integer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividComItem;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0}, Landroid/view/ViewGroup;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->default_Background:Landroid/graphics/drawable/Drawable;

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/individ/IndividComItem;)[Ljava/lang/CharSequence;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/individ/IndividComItem;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/individ/IndividComItem;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;

    return-void
.end method


# virtual methods
.method public doUpdate()V
    .locals 0

    return-void
.end method

.method public getInstance()Landroid/view/ViewGroup;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public onItemGainFocus()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    const v1, 0x7f020032    # com.konka.systemsetting.R.drawable.syssettingthirditemsinglefocus

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundResource(I)V

    return-void
.end method

.method public onItemLoseFocus()V
    .locals 2

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem;->default_Background:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    return-void
.end method

.method public setItemValueIndex(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;

    array-length v1, v1

    if-lt v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividComItem;->iValueIndex:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/individ/IndividComItem;->setRangeValueText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setOnClickListener(ZLandroid/view/View$OnClickListener;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/view/View$OnClickListener;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnClickEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnClickEvent;-><init>(Lcom/konka/systemsetting/individ/IndividComItem;Lcom/konka/systemsetting/individ/IndividComItem$ItemOnClickEvent;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto :goto_0
.end method

.method protected setOnFocusChangeListener(ZLandroid/view/View$OnFocusChangeListener;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/view/View$OnFocusChangeListener;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;-><init>(Lcom/konka/systemsetting/individ/IndividComItem;Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    goto :goto_0
.end method

.method protected setOnKeyListener(ZLandroid/view/View$OnKeyListener;)V
    .locals 3
    .param p1    # Z
    .param p2    # Landroid/view/View$OnKeyListener;

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setFocusableInTouchMode(Z)V

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;-><init>(Lcom/konka/systemsetting/individ/IndividComItem;Lcom/konka/systemsetting/individ/IndividComItem$ItemOnKeyEvent;)V

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, p2}, Landroid/view/ViewGroup;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    goto :goto_0
.end method

.method public setRangeValueArray(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->strItemValues:[Ljava/lang/CharSequence;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/individ/IndividComItem;->setItemValueIndex(Ljava/lang/Integer;)V

    return-void
.end method

.method protected setRangeValueText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1    # Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->textview_RangeValue:Landroid/widget/TextView;

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->textview_RangeValue:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method

.method protected setRangeValueText(Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->textview_RangeValue:Landroid/widget/TextView;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    return-void
.end method

.method public setRangeValueView(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->viewgroup_Container:Landroid/view/ViewGroup;

    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem;->textview_RangeValue:Landroid/widget/TextView;

    return-void
.end method
