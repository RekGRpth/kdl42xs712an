.class Lcom/konka/systemsetting/individ/IndividPageManager$1;
.super Ljava/lang/Object;
.source "IndividPageManager.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividPageManager;->SetOnFocusChangeListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividPageManager;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividPageManager;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 5
    .param p1    # Landroid/view/View;
    .param p2    # Z

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    if-eqz p2, :cond_1

    packed-switch v0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$1(Lcom/konka/systemsetting/individ/IndividPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-static {v1, v3}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$2(Lcom/konka/systemsetting/individ/IndividPageManager;I)V

    :goto_1
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$3(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iLastFocusPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$3(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    const/4 v3, 0x2

    # invokes: Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$4(Lcom/konka/systemsetting/individ/IndividPageManager;II)V

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v4}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$4(Lcom/konka/systemsetting/individ/IndividPageManager;II)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->showPage(I)V

    goto :goto_0

    :pswitch_1
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$1(Lcom/konka/systemsetting/individ/IndividPageManager;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    invoke-static {v1, v4}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$2(Lcom/konka/systemsetting/individ/IndividPageManager;I)V

    goto :goto_1

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager$1;->this$0:Lcom/konka/systemsetting/individ/IndividPageManager;

    # getter for: Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I

    move-result v2

    # invokes: Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V
    invoke-static {v1, v2, v3}, Lcom/konka/systemsetting/individ/IndividPageManager;->access$4(Lcom/konka/systemsetting/individ/IndividPageManager;II)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7f090021
        :pswitch_1    # com.konka.systemsetting.R.id.linearlayout_individ_tab_energysaving
        :pswitch_0    # com.konka.systemsetting.R.id.linearlayout_individ_tab_timesetting
    .end packed-switch
.end method
