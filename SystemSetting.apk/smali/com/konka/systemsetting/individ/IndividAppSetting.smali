.class public Lcom/konka/systemsetting/individ/IndividAppSetting;
.super Ljava/lang/Object;
.source "IndividAppSetting.java"


# instance fields
.field private isAllowed:Z

.field protected itemAppInst:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private strAllow:Ljava/lang/CharSequence;

.field private strNotAllow:Ljava/lang/CharSequence;

.field private tvSwitch:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 5
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v3, 0x0

    const/4 v2, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-boolean v3, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f06001e    # com.konka.systemsetting.R.string.str_individ_appsetting_allowed

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->strAllow:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f06001f    # com.konka.systemsetting.R.string.str_individ_appsetting_notallowed

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/MainActivity;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->strNotAllow:Ljava/lang/CharSequence;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v4, 0x7f090017    # com.konka.systemsetting.R.id.sys_individ_item_appsetting

    invoke-virtual {v1, v4}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->tvSwitch:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v1}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v4, "install_non_market_apps"

    invoke-static {v1, v4, v2}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    if-ne v0, v2, :cond_0

    move v1, v2

    :goto_0
    iput-boolean v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->tvSwitch:Landroid/widget/TextView;

    iget-boolean v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->strAllow:Ljava/lang/CharSequence;

    :goto_1
    invoke-virtual {v3, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/konka/systemsetting/individ/IndividAppSetting$1;

    invoke-direct {v3, p0}, Lcom/konka/systemsetting/individ/IndividAppSetting$1;-><init>(Lcom/konka/systemsetting/individ/IndividAppSetting;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/konka/systemsetting/individ/IndividAppSetting$2;

    invoke-direct {v3, p0}, Lcom/konka/systemsetting/individ/IndividAppSetting$2;-><init>(Lcom/konka/systemsetting/individ/IndividAppSetting;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/konka/systemsetting/individ/IndividAppSetting$3;

    invoke-direct {v3, p0}, Lcom/konka/systemsetting/individ/IndividAppSetting$3;-><init>(Lcom/konka/systemsetting/individ/IndividAppSetting;)V

    invoke-virtual {v1, v3}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->itemAppInst:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void

    :cond_0
    move v1, v3

    goto :goto_0

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->strNotAllow:Ljava/lang/CharSequence;

    goto :goto_1
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/individ/IndividAppSetting;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/individ/IndividAppSetting;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    return v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/individ/IndividAppSetting;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/individ/IndividAppSetting;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividAppSetting;->updateDataToUI()V

    return-void
.end method

.method private updateDataToUI()V
    .locals 3

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->tvSwitch:Landroid/widget/TextView;

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->strAllow:Ljava/lang/CharSequence;

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "install_non_market_apps"

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-static {v1, v2, v0}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting;->strNotAllow:Ljava/lang/CharSequence;

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
