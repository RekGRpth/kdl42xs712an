.class public Lcom/konka/systemsetting/individ/IndividConservation;
.super Ljava/lang/Object;
.source "IndividConservation.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;
    }
.end annotation


# static fields
.field private static m_iConservationMod:I

.field private static m_iNOSMode:I


# instance fields
.field private final ITEM_BEIGUANGTIAOJIE:I

.field private final ITEM_PINGMUBAOHU:I

.field private final ITEM_WUCAOZUODAIJI:I

.field private final ITEM_WUXINHAODAIJI:I

.field private final ITEM_ZHIKONGJIENENG:I

.field private demoDesk:Lcom/konka/kkinterface/tv/DemoDesk;

.field private isUnregisted:Z

.field private linearLayout_BackLight:Landroid/widget/LinearLayout;

.field private linearLayout_Conservation:Landroid/widget/LinearLayout;

.field private linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

.field private linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

.field private linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field mMasterResetReciever:Landroid/content/BroadcastReceiver;

.field private m_ConsModeValues:[Ljava/lang/CharSequence;

.field private m_bIsFocusOnBackLight:Z

.field private m_currItemId:Ljava/lang/Integer;

.field private m_iBackLightVal:I

.field private m_iBlMax:I

.field private m_iBlMin:I

.field private m_iNOSValues:[I

.field private m_iPreBackLightVal:I

.field private m_isNoSignalStandby:Z

.field private m_isScreenSaver:Z

.field private m_strNOSModeValues:[Ljava/lang/CharSequence;

.field private pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

.field private seekbar_BackLight:Landroid/widget/SeekBar;

.field private settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

.field private tRunSetBL:Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput v0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    sput v0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    return-void
.end method

.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;)V
    .locals 6
    .param p1    # Lcom/konka/systemsetting/MainActivity;

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->ITEM_ZHIKONGJIENENG:I

    iput v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->ITEM_PINGMUBAOHU:I

    iput v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->ITEM_BEIGUANGTIAOJIE:I

    iput v5, p0, Lcom/konka/systemsetting/individ/IndividConservation;->ITEM_WUXINHAODAIJI:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->ITEM_WUCAOZUODAIJI:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iPreBackLightVal:I

    iput v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMin:I

    const/16 v0, 0x64

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMax:I

    iput-boolean v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_bIsFocusOnBackLight:Z

    iput-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->tRunSetBL:Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;

    const v0, 0x7f090018    # com.konka.systemsetting.R.id.sys_individ_energysv_item_energyconservation

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;

    iput-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_ConsModeValues:[Ljava/lang/CharSequence;

    iput-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_strNOSModeValues:[Ljava/lang/CharSequence;

    const/4 v0, 0x4

    new-array v0, v0, [I

    const/16 v1, 0x3c

    aput v1, v0, v3

    const/16 v1, 0x78

    aput v1, v0, v4

    const/16 v1, 0xf0

    aput v1, v0, v5

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSValues:[I

    new-instance v0, Lcom/konka/systemsetting/individ/IndividConservation$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividConservation$1;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mMasterResetReciever:Landroid/content/BroadcastReceiver;

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070003    # com.konka.systemsetting.R.array.str_arr_conservation_mode

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_ConsModeValues:[Ljava/lang/CharSequence;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0}, Lcom/konka/systemsetting/MainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f070004    # com.konka.systemsetting.R.array.str_arr_nooperationstandby_mode

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getTextArray(I)[Ljava/lang/CharSequence;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_strNOSModeValues:[Ljava/lang/CharSequence;

    invoke-static {}, Lcom/konka/systemsetting/SysRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getPictureManagerInstance()Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    if-eqz v0, :cond_1

    const-string v0, "the picture desk is good in use"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_0
    invoke-static {}, Lcom/konka/systemsetting/SysRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getSettingManagerInstance()Lcom/konka/kkinterface/tv/SettingDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    if-eqz v0, :cond_2

    const-string v0, "the setting desk is good in use"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_1
    invoke-static {}, Lcom/konka/systemsetting/SysRootApp;->getTvDeskProvider()Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v0

    invoke-interface {v0}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getDemoManagerInstance()Lcom/konka/kkinterface/tv/DemoDesk;

    move-result-object v0

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->demoDesk:Lcom/konka/kkinterface/tv/DemoDesk;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->demoDesk:Lcom/konka/kkinterface/tv/DemoDesk;

    if-eqz v0, :cond_3

    const-string v0, "the demo desk is good in use"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    :goto_2
    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->initUIComponents()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->initUIData()V

    iput-boolean v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->isUnregisted:Z

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->registerReceiver()V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->tRunSetBL:Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;

    invoke-direct {v0, p0, v2}, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->tRunSetBL:Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;

    :cond_0
    return-void

    :cond_1
    const-string v0, "the picture desk is null null null!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    const-string v0, "the setting desk is null null null!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_1

    :cond_3
    const-string v0, "the demo desk is null null null!!!"

    invoke-static {v0}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    goto :goto_2
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/MainActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    return-object v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/individ/IndividConservation;)Z
    .locals 1

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_bIsFocusOnBackLight:Z

    return v0
.end method

.method static synthetic access$10(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->increaseBackLightVal()V

    return-void
.end method

.method static synthetic access$11(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->showEnergySavingPickerDialog()V

    return-void
.end method

.method static synthetic access$12(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->showNoOperationStandbyPickerDialog()V

    return-void
.end method

.method static synthetic access$13(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)Z
    .locals 1

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividConservation;->changeItemState(Ljava/lang/Integer;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$14(I)V
    .locals 0

    sput p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    return-void
.end method

.method static synthetic access$15(I)V
    .locals 0

    sput p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    return-void
.end method

.method static synthetic access$16(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/SeekBar;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;

    return-object v0
.end method

.method static synthetic access$17(Lcom/konka/systemsetting/individ/IndividConservation;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    return-void
.end method

.method static synthetic access$18(Lcom/konka/systemsetting/individ/IndividConservation;Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_bIsFocusOnBackLight:Z

    return-void
.end method

.method static synthetic access$19(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->tRunSetBL:Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/individ/IndividConservation;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iPreBackLightVal:I

    return v0
.end method

.method static synthetic access$20(Lcom/konka/systemsetting/individ/IndividConservation;)Landroid/widget/LinearLayout;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/individ/IndividConservation;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/individ/IndividConservation;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iPreBackLightVal:I

    return-void
.end method

.method static synthetic access$5(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/kkinterface/tv/PictureDesk;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    return-object v0
.end method

.method static synthetic access$6(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;

    return-object v0
.end method

.method static synthetic access$8(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->decreaseBackLightVal()V

    return-void
.end method

.method static synthetic access$9(Lcom/konka/systemsetting/individ/IndividConservation;I)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividConservation;->updateUIState(I)V

    return-void
.end method

.method private changeItemState(Ljava/lang/Integer;)Z
    .locals 3
    .param p1    # Ljava/lang/Integer;

    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :goto_0
    return v0

    :sswitch_0
    iget-boolean v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isScreenSaver:Z

    if-eqz v2, :cond_0

    :goto_1
    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isScreenSaver:Z

    invoke-direct {p0, v1}, Lcom/konka/systemsetting/individ/IndividConservation;->updateUIState(I)V

    :goto_2
    move v0, v1

    goto :goto_0

    :cond_0
    move v0, v1

    goto :goto_1

    :sswitch_1
    iget-boolean v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isNoSignalStandby:Z

    if-eqz v2, :cond_1

    :goto_3
    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isNoSignalStandby:Z

    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/konka/systemsetting/individ/IndividConservation;->updateUIState(I)V

    goto :goto_2

    :cond_1
    move v0, v1

    goto :goto_3

    :sswitch_data_0
    .sparse-switch
        0x7f090019 -> :sswitch_0    # com.konka.systemsetting.R.id.sys_individ_energysv_item_screensaver
        0x7f09001e -> :sswitch_1    # com.konka.systemsetting.R.id.sys_individ_energysv_item_nosignalstandby
    .end sparse-switch
.end method

.method private debug(Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;

    invoke-static {p1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    return-void
.end method

.method private decreaseBackLightVal()V
    .locals 2

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    iget v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMin:I

    if-ge v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMin:I

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    :cond_0
    return-void
.end method

.method private getNOSModeIndex(I)I
    .locals 2
    .param p1    # I

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSValues:[I

    array-length v1, v1

    if-lt v0, v1, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSValues:[I

    array-length v1, v1

    if-lt v0, v1, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0

    :cond_1
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSValues:[I

    aget v1, v1, v0

    if-eq v1, p1, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method private increaseBackLightVal()V
    .locals 2

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    iget v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMax:I

    if-le v0, v1, :cond_0

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMax:I

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    :cond_0
    return-void
.end method

.method private initUIComponents()V
    .locals 7

    const v6, 0x7f09001f    # com.konka.systemsetting.R.id.sys_individ_energysv_item_noactionstandby

    const v5, 0x7f09001e    # com.konka.systemsetting.R.id.sys_individ_energysv_item_nosignalstandby

    const v4, 0x7f090018    # com.konka.systemsetting.R.id.sys_individ_energysv_item_energyconservation

    const v3, 0x7f090021    # com.konka.systemsetting.R.id.linearlayout_individ_tab_energysaving

    const/4 v2, 0x1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v4}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f090019    # com.konka.systemsetting.R.id.sys_individ_energysv_item_screensaver

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09001a    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v5}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v0, v6}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    const v1, 0x7f09001c    # com.konka.systemsetting.R.id.sys_individ_energysv_item_backlight_seekbar

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/SeekBar;

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v5}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setNextFocusLeftId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v4}, Landroid/widget/LinearLayout;->setNextFocusUpId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v6}, Landroid/widget/LinearLayout;->setNextFocusDownId(I)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->SetOnKeyListener()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->SetOnClickListener()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->SetOnFocusChangeListener()V

    return-void
.end method

.method private registerReceiver()V
    .locals 3

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->isUnregisted:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mMasterResetReciever:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Lcom/konka/systemsetting/MainActivity;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->isUnregisted:Z

    :cond_0
    return-void
.end method

.method private setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    .locals 5
    .param p1    # Landroid/widget/LinearLayout;
    .param p2    # Z

    const v4, -0x515152

    const/high16 v3, -0x1000000

    const/4 v2, 0x0

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    if-eqz p2, :cond_0

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setEnabled(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setClickable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusable(Z)V

    invoke-virtual {p1, p2}, Landroid/widget/LinearLayout;->setFocusableInTouchMode(Z)V

    return-void

    :cond_0
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private showEnergySavingPickerDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060022    # com.konka.systemsetting.R.string.str_individ_energysv_item_energysaving

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividConservation$3;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/individ/IndividConservation$3;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_ConsModeValues:[Ljava/lang/CharSequence;

    sget v3, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private showNoOperationStandbyPickerDialog()V
    .locals 4

    new-instance v0, Landroid/app/AlertDialog$Builder;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f060025    # com.konka.systemsetting.R.string.str_individ_energysv_item_noactionstandby

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    const v2, 0x7f020022    # com.konka.systemsetting.R.drawable.syssettingfirsticonpersonalunfocus

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    new-instance v1, Lcom/konka/systemsetting/individ/IndividConservation$4;

    invoke-direct {v1, p0}, Lcom/konka/systemsetting/individ/IndividConservation$4;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_strNOSModeValues:[Ljava/lang/CharSequence;

    sget v3, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    invoke-virtual {v0, v2, v3, v1}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    return-void
.end method

.method private unregisterReceiver()V
    .locals 2

    iget-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->isUnregisted:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mMasterResetReciever:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/MainActivity;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->isUnregisted:Z

    :cond_0
    return-void
.end method

.method private updateBacklightStatus()V
    .locals 3

    const/4 v2, 0x1

    :try_start_0
    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    if-eqz v1, :cond_1

    sget v1, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    if-eq v1, v2, :cond_0

    invoke-static {}, Lcom/mstar/android/tvapi/common/TvManager;->getInstance()Lcom/mstar/android/tvapi/common/TvManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/TvManager;->getThreeDimensionManager()Lcom/mstar/android/tvapi/common/ThreeDimensionManager;

    move-result-object v1

    invoke-virtual {v1}, Lcom/mstar/android/tvapi/common/ThreeDimensionManager;->getCurrent3dFormat()Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    move-result-object v1

    sget-object v2, Lcom/mstar/android/tvapi/common/vo/Enum3dType;->EN_3D_NONE:Lcom/mstar/android/tvapi/common/vo/Enum3dType;

    if-eq v1, v2, :cond_2

    :cond_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    const/4 v2, 0x0

    invoke-direct {p0, v1, v2}, Lcom/konka/systemsetting/individ/IndividConservation;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V

    :cond_1
    :goto_0
    return-void

    :cond_2
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    const/4 v2, 0x1

    invoke-direct {p0, v1, v2}, Lcom/konka/systemsetting/individ/IndividConservation;->setOptionItemEnabled(Landroid/widget/LinearLayout;Z)V
    :try_end_0
    .catch Lcom/mstar/android/tvapi/common/exception/TvCommonException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/mstar/android/tvapi/common/exception/TvCommonException;->printStackTrace()V

    goto :goto_0
.end method

.method private updateUIState(I)V
    .locals 7
    .param p1    # I

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v2, 0x0

    if-nez p1, :cond_1

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_ConsModeValues:[Ljava/lang/CharSequence;

    sget v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->updateBacklightStatus()V

    invoke-direct {p0, v5}, Lcom/konka/systemsetting/individ/IndividConservation;->updateUIState(I)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-static {}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->values()[Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v4

    sget v5, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    aget-object v4, v4, v5

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/SettingDesk;->setSmartEnergySaving(Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;)Z

    :cond_0
    :goto_0
    return-void

    :cond_1
    if-ne p1, v4, :cond_3

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isScreenSaver:Z

    if-eqz v3, :cond_2

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v3, v3, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_1
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-boolean v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isScreenSaver:Z

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/SettingDesk;->setScreenSaveModeStatus(Z)Z

    goto :goto_0

    :cond_2
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v3, v3, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_1

    :cond_3
    if-ne p1, v5, :cond_5

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    sget v3, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    if-ne v3, v4, :cond_4

    const/16 v0, 0x64

    :goto_2
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    :cond_4
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;

    invoke-virtual {v3, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_2

    :cond_5
    if-ne p1, v6, :cond_7

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-boolean v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isNoSignalStandby:Z

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v3, v3, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_3
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-boolean v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isNoSignalStandby:Z

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/SettingDesk;->setStandbyNoSignal(Z)Z

    goto :goto_0

    :cond_6
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v3, v3, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_3

    :cond_7
    const/4 v3, 0x4

    if-ne p1, v3, :cond_0

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v3, v4}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_strNOSModeValues:[Ljava/lang/CharSequence;

    sget v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    aget-object v3, v3, v4

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSValues:[I

    sget v5, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    aget v4, v4, v5

    invoke-interface {v3, v4}, Lcom/konka/kkinterface/tv/SettingDesk;->setStandbyNoOperation(I)Z

    sget v3, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    packed-switch v3, :pswitch_data_0

    goto/16 :goto_0

    :pswitch_0
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_off_timeout"

    const v5, 0x7fffffff

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v3, "No operation standby: idx = 0, doing nothing."

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/individ/IndividConservation;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_1
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_off_timeout"

    sget v5, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    mul-int/lit8 v5, v5, 0x3c

    mul-int/lit8 v5, v5, 0x3c

    mul-int/lit16 v5, v5, 0x3e8

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No operation standby: idx = "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", timeout = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " hour, will call screen_off!."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/individ/IndividConservation;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-virtual {v3}, Lcom/konka/systemsetting/MainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "screen_off_timeout"

    const v5, 0xdbba00

    invoke-static {v3, v4, v5}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    const-string v3, "No operation standby: idx = 3, timeout = 4 hour, will call screen_off!."

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/individ/IndividConservation;->debug(Ljava/lang/String;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method protected SetOnClickListener()V
    .locals 3

    new-instance v0, Lcom/konka/systemsetting/individ/IndividConservation$5;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividConservation$5;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    new-instance v2, Lcom/konka/systemsetting/individ/IndividConservation$6;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/individ/IndividConservation$6;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    invoke-virtual {v1, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;

    new-instance v2, Lcom/konka/systemsetting/individ/IndividConservation$7;

    invoke-direct {v2, p0}, Lcom/konka/systemsetting/individ/IndividConservation$7;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    invoke-virtual {v1, v2}, Landroid/widget/SeekBar;->setOnSeekBarChangeListener(Landroid/widget/SeekBar$OnSeekBarChangeListener;)V

    return-void
.end method

.method protected SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/individ/IndividConservation$8;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividConservation$8;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;

    invoke-virtual {v1, v0}, Landroid/widget/SeekBar;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method protected SetOnKeyListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/individ/IndividConservation$2;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividConservation$2;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    return-void
.end method

.method protected initUIData()V
    .locals 7

    const/4 v6, 0x1

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->getSmartEnergySaving()Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/konka/kkinterface/tv/DataBaseDesk$SmartEnergySavingMode;->ordinal()I

    move-result v4

    sput v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/PictureDesk;->GetBacklight()S

    move-result v4

    iput v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->getScreenSaveModeStatus()Z

    move-result v4

    iput-boolean v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isScreenSaver:Z

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->getStandbyNoSignal()Z

    move-result v4

    iput-boolean v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isNoSignalStandby:Z

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->settingDesk:Lcom/konka/kkinterface/tv/SettingDesk;

    invoke-interface {v4}, Lcom/konka/kkinterface/tv/SettingDesk;->getStandbyNoOperation()I

    move-result v3

    invoke-direct {p0, v3}, Lcom/konka/systemsetting/individ/IndividConservation;->getNOSModeIndex(I)I

    move-result v4

    sput v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    const/4 v2, 0x0

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_Conservation:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_ConsModeValues:[Ljava/lang/CharSequence;

    sget v5, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_ScreenSaver:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isScreenSaver:Z

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v4, v4, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_0
    const/4 v4, 0x0

    const/16 v5, 0x64

    invoke-virtual {p0, v4, v5}, Lcom/konka/systemsetting/individ/IndividConservation;->setBacklightRange(II)V

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I

    sget v4, Lcom/konka/systemsetting/individ/IndividConservation;->m_iConservationMod:I

    if-ne v4, v6, :cond_1

    const/16 v0, 0x64

    :goto_1
    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_BackLight:Landroid/widget/LinearLayout;

    const/4 v5, 0x3

    invoke-virtual {v4, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoSignalStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iget-boolean v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_isNoSignalStandby:Z

    if-eqz v4, :cond_2

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v4, v4, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_ON:I

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    :goto_2
    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->linearLayout_NoOperationStandby:Landroid/widget/LinearLayout;

    invoke-virtual {v4, v6}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_strNOSModeValues:[Ljava/lang/CharSequence;

    sget v5, Lcom/konka/systemsetting/individ/IndividConservation;->m_iNOSMode:I

    aget-object v4, v4, v5

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->updateBacklightStatus()V

    return-void

    :cond_0
    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v4, v4, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_0

    :cond_1
    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->seekbar_BackLight:Landroid/widget/SeekBar;

    invoke-virtual {v4, v0}, Landroid/widget/SeekBar;->setProgress(I)V

    goto :goto_1

    :cond_2
    iget-object v4, p0, Lcom/konka/systemsetting/individ/IndividConservation;->mContext:Lcom/konka/systemsetting/MainActivity;

    iget v4, v4, Lcom/konka/systemsetting/MainActivity;->IMG_SWITCH_OFF:I

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageResource(I)V

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 1

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_bIsFocusOnBackLight:Z

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->unregisterReceiver()V

    return-void
.end method

.method public onResume()V
    .locals 0

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->registerReceiver()V

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividConservation;->initUIData()V

    return-void
.end method

.method public setBacklightRange(II)V
    .locals 0
    .param p1    # I
    .param p2    # I

    iput p1, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMin:I

    iput p2, p0, Lcom/konka/systemsetting/individ/IndividConservation;->m_iBlMax:I

    return-void
.end method
