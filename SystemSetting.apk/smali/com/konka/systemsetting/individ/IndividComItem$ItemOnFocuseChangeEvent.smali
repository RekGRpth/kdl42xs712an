.class Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;
.super Ljava/lang/Object;
.source "IndividComItem.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividComItem;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItemOnFocuseChangeEvent"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividComItem;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/individ/IndividComItem;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/individ/IndividComItem;Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;-><init>(Lcom/konka/systemsetting/individ/IndividComItem;)V

    return-void
.end method


# virtual methods
.method public onFocusChange(Landroid/view/View;Z)V
    .locals 1
    .param p1    # Landroid/view/View;
    .param p2    # Z

    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividComItem;->onItemGainFocus()V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividComItem$ItemOnFocuseChangeEvent;->this$0:Lcom/konka/systemsetting/individ/IndividComItem;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividComItem;->onItemLoseFocus()V

    goto :goto_0
.end method
