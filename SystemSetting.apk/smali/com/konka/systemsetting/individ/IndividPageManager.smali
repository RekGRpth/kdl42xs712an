.class public Lcom/konka/systemsetting/individ/IndividPageManager;
.super Ljava/lang/Object;
.source "IndividPageManager.java"


# instance fields
.field protected final PAGE_APPSETTING:I

.field protected final PAGE_ENERGYSAVING:I

.field protected final PAGE_TIMESETTING:I

.field protected final STATE_CURRENT_FOCUSED:I

.field protected final STATE_LAST_FOCUSED:I

.field protected final STATE_NO_FOCUSE:I

.field private mContext:Lcom/konka/systemsetting/MainActivity;

.field private m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

.field private m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

.field private m_iCurPage:I

.field private m_iLastFocusPage:I

.field private m_viewHolder:Lcom/konka/systemsetting/ViewHolder;


# direct methods
.method public constructor <init>(Lcom/konka/systemsetting/MainActivity;I)V
    .locals 3
    .param p1    # Lcom/konka/systemsetting/MainActivity;
    .param p2    # I

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->STATE_CURRENT_FOCUSED:I

    iput v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->STATE_LAST_FOCUSED:I

    iput v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->STATE_NO_FOCUSE:I

    iput v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->PAGE_TIMESETTING:I

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->PAGE_ENERGYSAVING:I

    iput v2, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->PAGE_APPSETTING:I

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iLastFocusPage:I

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    new-instance v0, Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/ViewHolder;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    invoke-virtual {v0}, Lcom/konka/systemsetting/ViewHolder;->findViewForPages()V

    iput p2, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I

    invoke-virtual {p0, v0}, Lcom/konka/systemsetting/individ/IndividPageManager;->showPage(I)V

    invoke-direct {p0}, Lcom/konka/systemsetting/individ/IndividPageManager;->SetOnFocusChangeListener()V

    return-void
.end method

.method private SetOnFocusChangeListener()V
    .locals 2

    new-instance v0, Lcom/konka/systemsetting/individ/IndividPageManager$1;

    invoke-direct {v0, p0}, Lcom/konka/systemsetting/individ/IndividPageManager$1;-><init>(Lcom/konka/systemsetting/individ/IndividPageManager;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v0}, Landroid/widget/LinearLayout;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/systemsetting/individ/IndividPageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I

    return v0
.end method

.method static synthetic access$1(Lcom/konka/systemsetting/individ/IndividPageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iLastFocusPage:I

    return-void
.end method

.method static synthetic access$2(Lcom/konka/systemsetting/individ/IndividPageManager;I)V
    .locals 0

    iput p1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I

    return-void
.end method

.method static synthetic access$3(Lcom/konka/systemsetting/individ/IndividPageManager;)I
    .locals 1

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iLastFocusPage:I

    return v0
.end method

.method static synthetic access$4(Lcom/konka/systemsetting/individ/IndividPageManager;II)V
    .locals 0

    invoke-direct {p0, p1, p2}, Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V

    return-void
.end method

.method private changeItemStatus(II)V
    .locals 6
    .param p1    # I
    .param p2    # I

    const/4 v5, 0x0

    const v4, -0x87e2

    const/4 v3, -0x1

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "change item statusid="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " iState="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Debug(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v1, "The item id is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabTimeSetting:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_1

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_3
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_4
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v1, v1, Lcom/konka/systemsetting/ViewHolder;->linearlayout_tabEnergySaving:Landroid/widget/LinearLayout;

    invoke-virtual {v1, v5}, Landroid/widget/LinearLayout;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    packed-switch p2, :pswitch_data_2

    const-string v1, "The item state is wrong!"

    invoke-static {v1}, Lcom/konka/debuginfo/logPrint;->Warning(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_5
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_6
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_7
    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_4
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method


# virtual methods
.method public onDestroy()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onPause()V

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividConservation;->onDestroy()V

    :cond_1
    return-void
.end method

.method public onPause()V
    .locals 0

    invoke-virtual {p0}, Lcom/konka/systemsetting/individ/IndividPageManager;->onDestroy()V

    return-void
.end method

.method public onResume()V
    .locals 1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->onResume()V

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-virtual {v0}, Lcom/konka/systemsetting/individ/IndividConservation;->onResume()V

    :cond_1
    return-void
.end method

.method public resetItemStatus()V
    .locals 2

    const/4 v1, 0x2

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iCurPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V

    iget v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_iLastFocusPage:I

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V

    return-void
.end method

.method protected setLastFocus(Ljava/lang/Integer;Ljava/lang/Integer;)V
    .locals 2
    .param p1    # Ljava/lang/Integer;
    .param p2    # Ljava/lang/Integer;

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/konka/systemsetting/individ/IndividPageManager;->changeItemStatus(II)V

    return-void
.end method

.method public showPage(I)V
    .locals 2
    .param p1    # I

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_viewHolder:Lcom/konka/systemsetting/ViewHolder;

    iget-object v0, v0, Lcom/konka/systemsetting/ViewHolder;->viewflipper_individ:Landroid/widget/ViewFlipper;

    invoke-virtual {v0, p1}, Landroid/widget/ViewFlipper;->setDisplayedChild(I)V

    packed-switch p1, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/individ/IndividTimeSetting;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_TimeSetting:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

    if-nez v0, :cond_0

    new-instance v0, Lcom/konka/systemsetting/individ/IndividConservation;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->mContext:Lcom/konka/systemsetting/MainActivity;

    invoke-direct {v0, v1}, Lcom/konka/systemsetting/individ/IndividConservation;-><init>(Lcom/konka/systemsetting/MainActivity;)V

    iput-object v0, p0, Lcom/konka/systemsetting/individ/IndividPageManager;->m_EnergySaving:Lcom/konka/systemsetting/individ/IndividConservation;

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
