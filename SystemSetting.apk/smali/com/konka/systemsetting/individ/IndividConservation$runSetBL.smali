.class Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;
.super Ljava/lang/Object;
.source "IndividConservation.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividConservation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "runSetBL"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividConservation;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/individ/IndividConservation;Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;-><init>(Lcom/konka/systemsetting/individ/IndividConservation;)V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    :goto_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_bIsFocusOnBackLight:Z
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$1(Lcom/konka/systemsetting/individ/IndividConservation;)Z

    move-result v1

    if-nez v1, :cond_0

    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_iPreBackLightVal:I
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$2(Lcom/konka/systemsetting/individ/IndividConservation;)I

    move-result v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$3(Lcom/konka/systemsetting/individ/IndividConservation;)I

    move-result v2

    if-eq v1, v2, :cond_1

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$3(Lcom/konka/systemsetting/individ/IndividConservation;)I

    move-result v2

    invoke-static {v1, v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$4(Lcom/konka/systemsetting/individ/IndividConservation;I)V

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->pictureDesk:Lcom/konka/kkinterface/tv/PictureDesk;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$5(Lcom/konka/systemsetting/individ/IndividConservation;)Lcom/konka/kkinterface/tv/PictureDesk;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividConservation$runSetBL;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_iBackLightVal:I
    invoke-static {v2}, Lcom/konka/systemsetting/individ/IndividConservation;->access$3(Lcom/konka/systemsetting/individ/IndividConservation;)I

    move-result v2

    int-to-short v2, v2

    invoke-interface {v1, v2}, Lcom/konka/kkinterface/tv/PictureDesk;->SetBacklight(S)Z

    :cond_1
    const-wide/16 v1, 0xc8

    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0
.end method
