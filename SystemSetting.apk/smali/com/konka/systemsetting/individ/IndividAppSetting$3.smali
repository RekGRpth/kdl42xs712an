.class Lcom/konka/systemsetting/individ/IndividAppSetting$3;
.super Ljava/lang/Object;
.source "IndividAppSetting.java"

# interfaces
.implements Landroid/view/View$OnKeyListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividAppSetting;-><init>(Lcom/konka/systemsetting/MainActivity;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividAppSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$3;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1    # Landroid/view/View;
    .param p2    # I
    .param p3    # Landroid/view/KeyEvent;

    const/4 v1, 0x1

    const/4 v0, 0x0

    const/16 v2, 0x17

    if-eq p2, v2, :cond_0

    const/16 v2, 0x42

    if-ne p2, v2, :cond_2

    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v2

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$3;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    iget-object v3, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$3;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividAppSetting;->isAllowed:Z
    invoke-static {v3}, Lcom/konka/systemsetting/individ/IndividAppSetting;->access$1(Lcom/konka/systemsetting/individ/IndividAppSetting;)Z

    move-result v3

    if-eqz v3, :cond_1

    :goto_0
    invoke-static {v2, v0}, Lcom/konka/systemsetting/individ/IndividAppSetting;->access$2(Lcom/konka/systemsetting/individ/IndividAppSetting;Z)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividAppSetting$3;->this$0:Lcom/konka/systemsetting/individ/IndividAppSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividAppSetting;->updateDataToUI()V
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividAppSetting;->access$3(Lcom/konka/systemsetting/individ/IndividAppSetting;)V

    :goto_1
    return v1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    move v1, v0

    goto :goto_1
.end method
