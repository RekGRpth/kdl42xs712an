.class Lcom/konka/systemsetting/individ/IndividConservation$5;
.super Ljava/lang/Object;
.source "IndividConservation.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/systemsetting/individ/IndividConservation;->SetOnClickListener()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividConservation;


# direct methods
.method constructor <init>(Lcom/konka/systemsetting/individ/IndividConservation;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1    # Landroid/view/View;

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$6(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0x7f090018    # com.konka.systemsetting.R.id.sys_individ_energysv_item_energyconservation

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->showEnergySavingPickerDialog()V
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividConservation;->access$11(Lcom/konka/systemsetting/individ/IndividConservation;)V

    :goto_0
    return-void

    :cond_0
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const v1, 0x7f09001f    # com.konka.systemsetting.R.id.sys_individ_energysv_item_noactionstandby

    if-ne v0, v1, :cond_1

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->showNoOperationStandbyPickerDialog()V
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividConservation;->access$12(Lcom/konka/systemsetting/individ/IndividConservation;)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividConservation$5;->this$0:Lcom/konka/systemsetting/individ/IndividConservation;

    # getter for: Lcom/konka/systemsetting/individ/IndividConservation;->m_currItemId:Ljava/lang/Integer;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$7(Lcom/konka/systemsetting/individ/IndividConservation;)Ljava/lang/Integer;

    move-result-object v1

    # invokes: Lcom/konka/systemsetting/individ/IndividConservation;->changeItemState(Ljava/lang/Integer;)Z
    invoke-static {v0, v1}, Lcom/konka/systemsetting/individ/IndividConservation;->access$13(Lcom/konka/systemsetting/individ/IndividConservation;Ljava/lang/Integer;)Z

    goto :goto_0
.end method
