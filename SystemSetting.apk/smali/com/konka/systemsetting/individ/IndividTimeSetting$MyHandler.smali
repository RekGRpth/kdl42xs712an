.class Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;
.super Landroid/os/Handler;
.source "IndividTimeSetting.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/konka/systemsetting/individ/IndividTimeSetting;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;


# direct methods
.method private constructor <init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;-><init>(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1    # Landroid/os/Message;

    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x58

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividTimeSetting;->getInitData()V
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$9(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # invokes: Lcom/konka/systemsetting/individ/IndividTimeSetting;->refreshUISate()V
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$10(Lcom/konka/systemsetting/individ/IndividTimeSetting;)V

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->m_iAutoTimeSelectIndex:Ljava/lang/Integer;
    invoke-static {v0}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$11(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    iget-object v1, p0, Lcom/konka/systemsetting/individ/IndividTimeSetting$MyHandler;->this$0:Lcom/konka/systemsetting/individ/IndividTimeSetting;

    # getter for: Lcom/konka/systemsetting/individ/IndividTimeSetting;->mContext:Lcom/konka/systemsetting/MainActivity;
    invoke-static {v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->access$6(Lcom/konka/systemsetting/individ/IndividTimeSetting;)Lcom/konka/systemsetting/MainActivity;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/konka/systemsetting/individ/IndividTimeSetting;->updateTimeAndDateDisplay(Landroid/content/Context;)V

    :cond_0
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    return-void
.end method
