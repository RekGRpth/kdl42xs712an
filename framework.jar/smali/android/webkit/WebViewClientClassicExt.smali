.class public Landroid/webkit/WebViewClientClassicExt;
.super Landroid/webkit/WebViewClient;
.source "WebViewClientClassicExt.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/webkit/WebViewClient;-><init>()V

    return-void
.end method


# virtual methods
.method public onProceededAfterSslError(Landroid/webkit/WebView;Landroid/net/http/SslError;)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/net/http/SslError;

    return-void
.end method

.method public onReceivedClientCertRequest(Landroid/webkit/WebView;Landroid/webkit/ClientCertRequestHandler;Ljava/lang/String;)V
    .locals 0
    .param p1    # Landroid/webkit/WebView;
    .param p2    # Landroid/webkit/ClientCertRequestHandler;
    .param p3    # Ljava/lang/String;

    invoke-virtual {p2}, Landroid/webkit/ClientCertRequestHandler;->cancel()V

    return-void
.end method
