.class public Landroid/net/http/ErrorStrings;
.super Ljava/lang/Object;
.source "ErrorStrings.java"


# static fields
.field private static final LOGTAG:Ljava/lang/String; = "Http"


# direct methods
.method private constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getResource(I)I
    .locals 4
    .param p0    # I

    const v0, 0x1040149    # android.R.string.httpError

    packed-switch p0, :pswitch_data_0

    const-string v1, "Http"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Using generic message for unknown error code: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    const v0, 0x1040148    # android.R.string.httpErrorOk

    goto :goto_0

    :pswitch_2
    const v0, 0x104014a    # android.R.string.httpErrorLookup

    goto :goto_0

    :pswitch_3
    const v0, 0x104014b    # android.R.string.httpErrorUnsupportedAuthScheme

    goto :goto_0

    :pswitch_4
    const v0, 0x104014c    # android.R.string.httpErrorAuth

    goto :goto_0

    :pswitch_5
    const v0, 0x104014d    # android.R.string.httpErrorProxyAuth

    goto :goto_0

    :pswitch_6
    const v0, 0x104014e    # android.R.string.httpErrorConnect

    goto :goto_0

    :pswitch_7
    const v0, 0x104014f    # android.R.string.httpErrorIO

    goto :goto_0

    :pswitch_8
    const v0, 0x1040150    # android.R.string.httpErrorTimeout

    goto :goto_0

    :pswitch_9
    const v0, 0x1040151    # android.R.string.httpErrorRedirectLoop

    goto :goto_0

    :pswitch_a
    const v0, 0x1040008    # android.R.string.httpErrorUnsupportedScheme

    goto :goto_0

    :pswitch_b
    const v0, 0x1040152    # android.R.string.httpErrorFailedSslHandshake

    goto :goto_0

    :pswitch_c
    const v0, 0x1040007    # android.R.string.httpErrorBadUrl

    goto :goto_0

    :pswitch_d
    const v0, 0x1040153    # android.R.string.httpErrorFile

    goto :goto_0

    :pswitch_e
    const v0, 0x1040154    # android.R.string.httpErrorFileNotFound

    goto :goto_0

    :pswitch_f
    const v0, 0x1040155    # android.R.string.httpErrorTooManyRequests

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch -0xf
        :pswitch_f
        :pswitch_e
        :pswitch_d
        :pswitch_c
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static getString(ILandroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0    # I
    .param p1    # Landroid/content/Context;

    invoke-static {p0}, Landroid/net/http/ErrorStrings;->getResource(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
