.class public final Lcom/android/internal/R$styleable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "styleable"
.end annotation


# static fields
.field public static final AbsListView:[I

.field public static final AbsListView_cacheColorHint:I = 0x6

.field public static final AbsListView_choiceMode:I = 0x7

.field public static final AbsListView_drawSelectorOnTop:I = 0x1

.field public static final AbsListView_fastScrollAlwaysVisible:I = 0xa

.field public static final AbsListView_fastScrollEnabled:I = 0x8

.field public static final AbsListView_listSelector:I = 0x0

.field public static final AbsListView_scrollingCache:I = 0x3

.field public static final AbsListView_smoothScrollbar:I = 0x9

.field public static final AbsListView_stackFromBottom:I = 0x2

.field public static final AbsListView_textFilterEnabled:I = 0x4

.field public static final AbsListView_transcriptMode:I = 0x5

.field public static final AbsSpinner:[I

.field public static final AbsSpinner_entries:I = 0x0

.field public static final AbsoluteLayout_Layout:[I

.field public static final AbsoluteLayout_Layout_layout_x:I = 0x0

.field public static final AbsoluteLayout_Layout_layout_y:I = 0x1

.field public static final AccelerateInterpolator:[I

.field public static final AccelerateInterpolator_factor:I = 0x0

.field public static final AccessibilityService:[I

.field public static final AccessibilityService_accessibilityEventTypes:I = 0x2

.field public static final AccessibilityService_accessibilityFeedbackType:I = 0x4

.field public static final AccessibilityService_accessibilityFlags:I = 0x6

.field public static final AccessibilityService_canRetrieveWindowContent:I = 0x7

.field public static final AccessibilityService_description:I = 0x0

.field public static final AccessibilityService_notificationTimeout:I = 0x5

.field public static final AccessibilityService_packageNames:I = 0x3

.field public static final AccessibilityService_settingsActivity:I = 0x1

.field public static final AccountAuthenticator:[I

.field public static final AccountAuthenticator_accountPreferences:I = 0x4

.field public static final AccountAuthenticator_accountType:I = 0x2

.field public static final AccountAuthenticator_customTokens:I = 0x5

.field public static final AccountAuthenticator_icon:I = 0x1

.field public static final AccountAuthenticator_label:I = 0x0

.field public static final AccountAuthenticator_smallIcon:I = 0x3

.field public static final ActionBar:[I

.field public static final ActionBar_LayoutParams:[I

.field public static final ActionBar_LayoutParams_layout_gravity:I = 0x0

.field public static final ActionBar_background:I = 0x2

.field public static final ActionBar_backgroundSplit:I = 0x12

.field public static final ActionBar_backgroundStacked:I = 0x11

.field public static final ActionBar_customNavigationLayout:I = 0xa

.field public static final ActionBar_displayOptions:I = 0x8

.field public static final ActionBar_divider:I = 0x3

.field public static final ActionBar_height:I = 0x4

.field public static final ActionBar_homeLayout:I = 0xf

.field public static final ActionBar_icon:I = 0x0

.field public static final ActionBar_indeterminateProgressStyle:I = 0xd

.field public static final ActionBar_itemPadding:I = 0x10

.field public static final ActionBar_logo:I = 0x6

.field public static final ActionBar_navigationMode:I = 0x7

.field public static final ActionBar_progressBarPadding:I = 0xe

.field public static final ActionBar_progressBarStyle:I = 0x1

.field public static final ActionBar_subtitle:I = 0x9

.field public static final ActionBar_subtitleTextStyle:I = 0xc

.field public static final ActionBar_title:I = 0x5

.field public static final ActionBar_titleTextStyle:I = 0xb

.field public static final ActionMenuItemView:[I

.field public static final ActionMenuItemView_minWidth:I = 0x0

.field public static final ActionMode:[I

.field public static final ActionMode_background:I = 0x0

.field public static final ActionMode_backgroundSplit:I = 0x4

.field public static final ActionMode_height:I = 0x1

.field public static final ActionMode_subtitleTextStyle:I = 0x3

.field public static final ActionMode_titleTextStyle:I = 0x2

.field public static final ActivityChooserView:[I

.field public static final ActivityChooserView_expandActivityOverflowButtonDrawable:I = 0x1

.field public static final ActivityChooserView_initialActivityCount:I = 0x0

.field public static final AdapterViewAnimator:[I

.field public static final AdapterViewAnimator_animateFirstView:I = 0x2

.field public static final AdapterViewAnimator_inAnimation:I = 0x0

.field public static final AdapterViewAnimator_loopViews:I = 0x3

.field public static final AdapterViewAnimator_outAnimation:I = 0x1

.field public static final AdapterViewFlipper:[I

.field public static final AdapterViewFlipper_autoStart:I = 0x1

.field public static final AdapterViewFlipper_flipInterval:I = 0x0

.field public static final AlertDialog:[I

.field public static final AlertDialog_bottomBright:I = 0x7

.field public static final AlertDialog_bottomDark:I = 0x3

.field public static final AlertDialog_bottomMedium:I = 0x8

.field public static final AlertDialog_centerBright:I = 0x6

.field public static final AlertDialog_centerDark:I = 0x2

.field public static final AlertDialog_centerMedium:I = 0x9

.field public static final AlertDialog_fullBright:I = 0x4

.field public static final AlertDialog_fullDark:I = 0x0

.field public static final AlertDialog_horizontalProgressLayout:I = 0x10

.field public static final AlertDialog_layout:I = 0xa

.field public static final AlertDialog_listItemLayout:I = 0xe

.field public static final AlertDialog_listLayout:I = 0xb

.field public static final AlertDialog_multiChoiceItemLayout:I = 0xc

.field public static final AlertDialog_progressLayout:I = 0xf

.field public static final AlertDialog_singleChoiceItemLayout:I = 0xd

.field public static final AlertDialog_topBright:I = 0x5

.field public static final AlertDialog_topDark:I = 0x1

.field public static final AlphaAnimation:[I

.field public static final AlphaAnimation_fromAlpha:I = 0x0

.field public static final AlphaAnimation_toAlpha:I = 0x1

.field public static final AnalogClock:[I

.field public static final AnalogClock_dial:I = 0x0

.field public static final AnalogClock_hand_hour:I = 0x1

.field public static final AnalogClock_hand_minute:I = 0x2

.field public static final AndroidManifest:[I

.field public static final AndroidManifestAction:[I

.field public static final AndroidManifestAction_name:I = 0x0

.field public static final AndroidManifestActivity:[I

.field public static final AndroidManifestActivityAlias:[I

.field public static final AndroidManifestActivityAlias_description:I = 0x6

.field public static final AndroidManifestActivityAlias_enabled:I = 0x4

.field public static final AndroidManifestActivityAlias_exported:I = 0x5

.field public static final AndroidManifestActivityAlias_icon:I = 0x1

.field public static final AndroidManifestActivityAlias_label:I = 0x0

.field public static final AndroidManifestActivityAlias_logo:I = 0x8

.field public static final AndroidManifestActivityAlias_name:I = 0x2

.field public static final AndroidManifestActivityAlias_parentActivityName:I = 0x9

.field public static final AndroidManifestActivityAlias_permission:I = 0x3

.field public static final AndroidManifestActivityAlias_targetActivity:I = 0x7

.field public static final AndroidManifestActivity_allowTaskReparenting:I = 0x13

.field public static final AndroidManifestActivity_alwaysRetainTaskState:I = 0x12

.field public static final AndroidManifestActivity_clearTaskOnLaunch:I = 0xb

.field public static final AndroidManifestActivity_configChanges:I = 0x10

.field public static final AndroidManifestActivity_description:I = 0x11

.field public static final AndroidManifestActivity_enabled:I = 0x5

.field public static final AndroidManifestActivity_excludeFromRecents:I = 0xd

.field public static final AndroidManifestActivity_exported:I = 0x6

.field public static final AndroidManifestActivity_finishOnCloseSystemDialogs:I = 0x16

.field public static final AndroidManifestActivity_finishOnTaskLaunch:I = 0xa

.field public static final AndroidManifestActivity_hardwareAccelerated:I = 0x19

.field public static final AndroidManifestActivity_icon:I = 0x2

.field public static final AndroidManifestActivity_immersive:I = 0x18

.field public static final AndroidManifestActivity_label:I = 0x1

.field public static final AndroidManifestActivity_launchMode:I = 0xe

.field public static final AndroidManifestActivity_logo:I = 0x17

.field public static final AndroidManifestActivity_multiprocess:I = 0x9

.field public static final AndroidManifestActivity_name:I = 0x3

.field public static final AndroidManifestActivity_noHistory:I = 0x15

.field public static final AndroidManifestActivity_parentActivityName:I = 0x1b

.field public static final AndroidManifestActivity_permission:I = 0x4

.field public static final AndroidManifestActivity_primaryUserOnly:I = 0x1e

.field public static final AndroidManifestActivity_process:I = 0x7

.field public static final AndroidManifestActivity_screenOrientation:I = 0xf

.field public static final AndroidManifestActivity_showOnLockScreen:I = 0x1d

.field public static final AndroidManifestActivity_singleUser:I = 0x1c

.field public static final AndroidManifestActivity_stateNotNeeded:I = 0xc

.field public static final AndroidManifestActivity_taskAffinity:I = 0x8

.field public static final AndroidManifestActivity_theme:I = 0x0

.field public static final AndroidManifestActivity_uiOptions:I = 0x1a

.field public static final AndroidManifestActivity_windowSoftInputMode:I = 0x14

.field public static final AndroidManifestApplication:[I

.field public static final AndroidManifestApplication_allowBackup:I = 0x11

.field public static final AndroidManifestApplication_allowClearUserData:I = 0x5

.field public static final AndroidManifestApplication_allowTaskReparenting:I = 0xe

.field public static final AndroidManifestApplication_backupAgent:I = 0x10

.field public static final AndroidManifestApplication_cantSaveState:I = 0x1c

.field public static final AndroidManifestApplication_debuggable:I = 0xa

.field public static final AndroidManifestApplication_description:I = 0xd

.field public static final AndroidManifestApplication_enabled:I = 0x9

.field public static final AndroidManifestApplication_hardwareAccelerated:I = 0x17

.field public static final AndroidManifestApplication_hasCode:I = 0x7

.field public static final AndroidManifestApplication_icon:I = 0x2

.field public static final AndroidManifestApplication_killAfterRestore:I = 0x12

.field public static final AndroidManifestApplication_label:I = 0x1

.field public static final AndroidManifestApplication_largeHeap:I = 0x18

.field public static final AndroidManifestApplication_logo:I = 0x16

.field public static final AndroidManifestApplication_manageSpaceActivity:I = 0x4

.field public static final AndroidManifestApplication_name:I = 0x3

.field public static final AndroidManifestApplication_neverEncrypt:I = 0x1b

.field public static final AndroidManifestApplication_permission:I = 0x6

.field public static final AndroidManifestApplication_persistent:I = 0x8

.field public static final AndroidManifestApplication_process:I = 0xb

.field public static final AndroidManifestApplication_restoreAnyVersion:I = 0x15

.field public static final AndroidManifestApplication_restoreNeedsApplication:I = 0x13
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final AndroidManifestApplication_supportsRtl:I = 0x1a

.field public static final AndroidManifestApplication_taskAffinity:I = 0xc

.field public static final AndroidManifestApplication_testOnly:I = 0xf

.field public static final AndroidManifestApplication_theme:I = 0x0

.field public static final AndroidManifestApplication_uiOptions:I = 0x19

.field public static final AndroidManifestApplication_vmSafeMode:I = 0x14

.field public static final AndroidManifestCategory:[I

.field public static final AndroidManifestCategory_name:I = 0x0

.field public static final AndroidManifestCompatibleScreensScreen:[I

.field public static final AndroidManifestCompatibleScreensScreen_screenDensity:I = 0x1

.field public static final AndroidManifestCompatibleScreensScreen_screenSize:I = 0x0

.field public static final AndroidManifestData:[I

.field public static final AndroidManifestData_host:I = 0x2

.field public static final AndroidManifestData_mimeType:I = 0x0

.field public static final AndroidManifestData_path:I = 0x4

.field public static final AndroidManifestData_pathPattern:I = 0x6

.field public static final AndroidManifestData_pathPrefix:I = 0x5

.field public static final AndroidManifestData_port:I = 0x3

.field public static final AndroidManifestData_scheme:I = 0x1

.field public static final AndroidManifestGrantUriPermission:[I

.field public static final AndroidManifestGrantUriPermission_path:I = 0x0

.field public static final AndroidManifestGrantUriPermission_pathPattern:I = 0x2

.field public static final AndroidManifestGrantUriPermission_pathPrefix:I = 0x1

.field public static final AndroidManifestInputSource:[I

.field public static final AndroidManifestInputSource_closeDoubleChannel:I = 0x1

.field public static final AndroidManifestInputSource_source:I = 0x0

.field public static final AndroidManifestInstrumentation:[I

.field public static final AndroidManifestInstrumentation_functionalTest:I = 0x5

.field public static final AndroidManifestInstrumentation_handleProfiling:I = 0x4

.field public static final AndroidManifestInstrumentation_icon:I = 0x1

.field public static final AndroidManifestInstrumentation_label:I = 0x0

.field public static final AndroidManifestInstrumentation_logo:I = 0x6

.field public static final AndroidManifestInstrumentation_name:I = 0x2

.field public static final AndroidManifestInstrumentation_targetPackage:I = 0x3

.field public static final AndroidManifestIntentFilter:[I

.field public static final AndroidManifestIntentFilter_icon:I = 0x1

.field public static final AndroidManifestIntentFilter_label:I = 0x0

.field public static final AndroidManifestIntentFilter_logo:I = 0x3

.field public static final AndroidManifestIntentFilter_priority:I = 0x2

.field public static final AndroidManifestMetaData:[I

.field public static final AndroidManifestMetaData_name:I = 0x0

.field public static final AndroidManifestMetaData_resource:I = 0x2

.field public static final AndroidManifestMetaData_value:I = 0x1

.field public static final AndroidManifestOriginalPackage:[I

.field public static final AndroidManifestOriginalPackage_name:I = 0x0

.field public static final AndroidManifestPackageVerifier:[I

.field public static final AndroidManifestPackageVerifier_name:I = 0x0

.field public static final AndroidManifestPackageVerifier_publicKey:I = 0x1

.field public static final AndroidManifestPathPermission:[I

.field public static final AndroidManifestPathPermission_path:I = 0x3

.field public static final AndroidManifestPathPermission_pathPattern:I = 0x5

.field public static final AndroidManifestPathPermission_pathPrefix:I = 0x4

.field public static final AndroidManifestPathPermission_permission:I = 0x0

.field public static final AndroidManifestPathPermission_readPermission:I = 0x1

.field public static final AndroidManifestPathPermission_writePermission:I = 0x2

.field public static final AndroidManifestPermission:[I

.field public static final AndroidManifestPermissionGroup:[I

.field public static final AndroidManifestPermissionGroup_description:I = 0x4

.field public static final AndroidManifestPermissionGroup_icon:I = 0x1

.field public static final AndroidManifestPermissionGroup_label:I = 0x0

.field public static final AndroidManifestPermissionGroup_logo:I = 0x5

.field public static final AndroidManifestPermissionGroup_name:I = 0x2

.field public static final AndroidManifestPermissionGroup_permissionGroupFlags:I = 0x6

.field public static final AndroidManifestPermissionGroup_priority:I = 0x3

.field public static final AndroidManifestPermissionTree:[I

.field public static final AndroidManifestPermissionTree_icon:I = 0x1

.field public static final AndroidManifestPermissionTree_label:I = 0x0

.field public static final AndroidManifestPermissionTree_logo:I = 0x3

.field public static final AndroidManifestPermissionTree_name:I = 0x2

.field public static final AndroidManifestPermission_description:I = 0x5

.field public static final AndroidManifestPermission_icon:I = 0x1

.field public static final AndroidManifestPermission_label:I = 0x0

.field public static final AndroidManifestPermission_logo:I = 0x6

.field public static final AndroidManifestPermission_name:I = 0x2

.field public static final AndroidManifestPermission_permissionFlags:I = 0x7

.field public static final AndroidManifestPermission_permissionGroup:I = 0x4

.field public static final AndroidManifestPermission_protectionLevel:I = 0x3

.field public static final AndroidManifestProtectedBroadcast:[I

.field public static final AndroidManifestProtectedBroadcast_name:I = 0x0

.field public static final AndroidManifestProvider:[I

.field public static final AndroidManifestProvider_authorities:I = 0xa

.field public static final AndroidManifestProvider_description:I = 0xe

.field public static final AndroidManifestProvider_enabled:I = 0x6

.field public static final AndroidManifestProvider_exported:I = 0x7

.field public static final AndroidManifestProvider_grantUriPermissions:I = 0xd

.field public static final AndroidManifestProvider_icon:I = 0x1

.field public static final AndroidManifestProvider_initOrder:I = 0xc

.field public static final AndroidManifestProvider_label:I = 0x0

.field public static final AndroidManifestProvider_logo:I = 0xf

.field public static final AndroidManifestProvider_multiprocess:I = 0x9

.field public static final AndroidManifestProvider_name:I = 0x2

.field public static final AndroidManifestProvider_permission:I = 0x3

.field public static final AndroidManifestProvider_process:I = 0x8

.field public static final AndroidManifestProvider_readPermission:I = 0x4

.field public static final AndroidManifestProvider_singleUser:I = 0x10

.field public static final AndroidManifestProvider_syncable:I = 0xb

.field public static final AndroidManifestProvider_writePermission:I = 0x5

.field public static final AndroidManifestReceiver:[I

.field public static final AndroidManifestReceiver_description:I = 0x7

.field public static final AndroidManifestReceiver_enabled:I = 0x4

.field public static final AndroidManifestReceiver_exported:I = 0x5

.field public static final AndroidManifestReceiver_icon:I = 0x1

.field public static final AndroidManifestReceiver_label:I = 0x0

.field public static final AndroidManifestReceiver_logo:I = 0x8

.field public static final AndroidManifestReceiver_name:I = 0x2

.field public static final AndroidManifestReceiver_permission:I = 0x3

.field public static final AndroidManifestReceiver_process:I = 0x6

.field public static final AndroidManifestReceiver_singleUser:I = 0x9

.field public static final AndroidManifestService:[I

.field public static final AndroidManifestService_description:I = 0x7

.field public static final AndroidManifestService_enabled:I = 0x4

.field public static final AndroidManifestService_exported:I = 0x5

.field public static final AndroidManifestService_icon:I = 0x1

.field public static final AndroidManifestService_isolatedProcess:I = 0xa

.field public static final AndroidManifestService_label:I = 0x0

.field public static final AndroidManifestService_logo:I = 0x8

.field public static final AndroidManifestService_name:I = 0x2

.field public static final AndroidManifestService_permission:I = 0x3

.field public static final AndroidManifestService_process:I = 0x6

.field public static final AndroidManifestService_singleUser:I = 0xb

.field public static final AndroidManifestService_stopWithTask:I = 0x9

.field public static final AndroidManifestSupportsScreens:[I

.field public static final AndroidManifestSupportsScreens_anyDensity:I = 0x0

.field public static final AndroidManifestSupportsScreens_compatibleWidthLimitDp:I = 0x7

.field public static final AndroidManifestSupportsScreens_largeScreens:I = 0x3

.field public static final AndroidManifestSupportsScreens_largestWidthLimitDp:I = 0x8

.field public static final AndroidManifestSupportsScreens_normalScreens:I = 0x2

.field public static final AndroidManifestSupportsScreens_requiresSmallestWidthDp:I = 0x6

.field public static final AndroidManifestSupportsScreens_resizeable:I = 0x4

.field public static final AndroidManifestSupportsScreens_smallScreens:I = 0x1

.field public static final AndroidManifestSupportsScreens_xlargeScreens:I = 0x5

.field public static final AndroidManifestUsesConfiguration:[I

.field public static final AndroidManifestUsesConfiguration_reqFiveWayNav:I = 0x4

.field public static final AndroidManifestUsesConfiguration_reqHardKeyboard:I = 0x2

.field public static final AndroidManifestUsesConfiguration_reqKeyboardType:I = 0x1

.field public static final AndroidManifestUsesConfiguration_reqNavigation:I = 0x3

.field public static final AndroidManifestUsesConfiguration_reqTouchScreen:I = 0x0

.field public static final AndroidManifestUsesFeature:[I

.field public static final AndroidManifestUsesFeature_glEsVersion:I = 0x1

.field public static final AndroidManifestUsesFeature_name:I = 0x0

.field public static final AndroidManifestUsesFeature_required:I = 0x2

.field public static final AndroidManifestUsesLibrary:[I

.field public static final AndroidManifestUsesLibrary_name:I = 0x0

.field public static final AndroidManifestUsesLibrary_required:I = 0x1

.field public static final AndroidManifestUsesPermission:[I

.field public static final AndroidManifestUsesPermission_name:I = 0x0

.field public static final AndroidManifestUsesSdk:[I

.field public static final AndroidManifestUsesSdk_maxSdkVersion:I = 0x2

.field public static final AndroidManifestUsesSdk_minSdkVersion:I = 0x0

.field public static final AndroidManifestUsesSdk_targetSdkVersion:I = 0x1

.field public static final AndroidManifest_installLocation:I = 0x4

.field public static final AndroidManifest_sharedUserId:I = 0x0

.field public static final AndroidManifest_sharedUserLabel:I = 0x3

.field public static final AndroidManifest_versionCode:I = 0x1

.field public static final AndroidManifest_versionName:I = 0x2

.field public static final AnimatedRotateDrawable:[I

.field public static final AnimatedRotateDrawable_drawable:I = 0x1

.field public static final AnimatedRotateDrawable_frameDuration:I = 0x4

.field public static final AnimatedRotateDrawable_framesCount:I = 0x5

.field public static final AnimatedRotateDrawable_pivotX:I = 0x2

.field public static final AnimatedRotateDrawable_pivotY:I = 0x3

.field public static final AnimatedRotateDrawable_visible:I = 0x0

.field public static final Animation:[I

.field public static final AnimationDrawable:[I

.field public static final AnimationDrawableItem:[I

.field public static final AnimationDrawableItem_drawable:I = 0x1

.field public static final AnimationDrawableItem_duration:I = 0x0

.field public static final AnimationDrawable_oneshot:I = 0x2

.field public static final AnimationDrawable_variablePadding:I = 0x1

.field public static final AnimationDrawable_visible:I = 0x0

.field public static final AnimationSet:[I

.field public static final AnimationSet_duration:I = 0x0

.field public static final AnimationSet_fillAfter:I = 0x3

.field public static final AnimationSet_fillBefore:I = 0x2

.field public static final AnimationSet_repeatMode:I = 0x5

.field public static final AnimationSet_shareInterpolator:I = 0x1

.field public static final AnimationSet_startOffset:I = 0x4

.field public static final Animation_background:I = 0x0

.field public static final Animation_detachWallpaper:I = 0xa

.field public static final Animation_duration:I = 0x2

.field public static final Animation_fillAfter:I = 0x4

.field public static final Animation_fillBefore:I = 0x3

.field public static final Animation_fillEnabled:I = 0x9

.field public static final Animation_interpolator:I = 0x1

.field public static final Animation_repeatCount:I = 0x6

.field public static final Animation_repeatMode:I = 0x7

.field public static final Animation_startOffset:I = 0x5

.field public static final Animation_zAdjustment:I = 0x8

.field public static final Animator:[I

.field public static final AnimatorSet:[I

.field public static final AnimatorSet_ordering:I = 0x0

.field public static final Animator_duration:I = 0x1

.field public static final Animator_interpolator:I = 0x0

.field public static final Animator_repeatCount:I = 0x3

.field public static final Animator_repeatMode:I = 0x4

.field public static final Animator_startOffset:I = 0x2

.field public static final Animator_valueFrom:I = 0x5

.field public static final Animator_valueTo:I = 0x6

.field public static final Animator_valueType:I = 0x7

.field public static final AnticipateInterpolator:[I

.field public static final AnticipateInterpolator_tension:I = 0x0

.field public static final AnticipateOvershootInterpolator:[I

.field public static final AnticipateOvershootInterpolator_extraTension:I = 0x1

.field public static final AnticipateOvershootInterpolator_tension:I = 0x0

.field public static final AppWidgetProviderInfo:[I

.field public static final AppWidgetProviderInfo_autoAdvanceViewId:I = 0x6

.field public static final AppWidgetProviderInfo_configure:I = 0x4

.field public static final AppWidgetProviderInfo_initialKeyguardLayout:I = 0xa

.field public static final AppWidgetProviderInfo_initialLayout:I = 0x3

.field public static final AppWidgetProviderInfo_minHeight:I = 0x1

.field public static final AppWidgetProviderInfo_minResizeHeight:I = 0x9

.field public static final AppWidgetProviderInfo_minResizeWidth:I = 0x8

.field public static final AppWidgetProviderInfo_minWidth:I = 0x0

.field public static final AppWidgetProviderInfo_previewImage:I = 0x5

.field public static final AppWidgetProviderInfo_resizeMode:I = 0x7

.field public static final AppWidgetProviderInfo_updatePeriodMillis:I = 0x2

.field public static final AppWidgetProviderInfo_widgetCategory:I = 0xb

.field public static final AutoCompleteTextView:[I

.field public static final AutoCompleteTextView_completionHint:I = 0x0

.field public static final AutoCompleteTextView_completionHintView:I = 0x1

.field public static final AutoCompleteTextView_completionThreshold:I = 0x2

.field public static final AutoCompleteTextView_dropDownAnchor:I = 0x6

.field public static final AutoCompleteTextView_dropDownHeight:I = 0x7

.field public static final AutoCompleteTextView_dropDownHorizontalOffset:I = 0x8

.field public static final AutoCompleteTextView_dropDownSelector:I = 0x3

.field public static final AutoCompleteTextView_dropDownVerticalOffset:I = 0x9

.field public static final AutoCompleteTextView_dropDownWidth:I = 0x5

.field public static final AutoCompleteTextView_inputType:I = 0x4

.field public static final BitmapDrawable:[I

.field public static final BitmapDrawable_antialias:I = 0x2

.field public static final BitmapDrawable_dither:I = 0x4

.field public static final BitmapDrawable_filter:I = 0x3

.field public static final BitmapDrawable_gravity:I = 0x0

.field public static final BitmapDrawable_src:I = 0x1

.field public static final BitmapDrawable_tileMode:I = 0x5

.field public static final Button:[I

.field public static final CalendarView:[I

.field public static final CalendarView_dateTextAppearance:I = 0xc

.field public static final CalendarView_firstDayOfWeek:I = 0x0

.field public static final CalendarView_focusedMonthDateColor:I = 0x6

.field public static final CalendarView_maxDate:I = 0x3

.field public static final CalendarView_minDate:I = 0x2

.field public static final CalendarView_selectedDateVerticalBar:I = 0xa

.field public static final CalendarView_selectedWeekBackgroundColor:I = 0x5

.field public static final CalendarView_showWeekNumber:I = 0x1

.field public static final CalendarView_shownWeekCount:I = 0x4

.field public static final CalendarView_unfocusedMonthDateColor:I = 0x7

.field public static final CalendarView_weekDayTextAppearance:I = 0xb

.field public static final CalendarView_weekNumberColor:I = 0x8

.field public static final CalendarView_weekSeparatorLineColor:I = 0x9

.field public static final CheckBoxPreference:[I

.field public static final CheckBoxPreference_disableDependentsState:I = 0x2

.field public static final CheckBoxPreference_summaryOff:I = 0x1

.field public static final CheckBoxPreference_summaryOn:I = 0x0

.field public static final CheckedTextView:[I

.field public static final CheckedTextView_checkMark:I = 0x1

.field public static final CheckedTextView_checked:I = 0x0

.field public static final Chronometer:[I

.field public static final Chronometer_format:I = 0x0

.field public static final ClipDrawable:[I

.field public static final ClipDrawable_clipOrientation:I = 0x2

.field public static final ClipDrawable_drawable:I = 0x1

.field public static final ClipDrawable_gravity:I = 0x0

.field public static final ColorDrawable:[I

.field public static final ColorDrawable_color:I = 0x0

.field public static final CompoundButton:[I

.field public static final CompoundButton_button:I = 0x1

.field public static final CompoundButton_checked:I = 0x0

.field public static final ContactsDataKind:[I

.field public static final ContactsDataKind_allContactsName:I = 0x5

.field public static final ContactsDataKind_detailColumn:I = 0x3

.field public static final ContactsDataKind_detailSocialSummary:I = 0x4

.field public static final ContactsDataKind_icon:I = 0x0

.field public static final ContactsDataKind_mimeType:I = 0x1

.field public static final ContactsDataKind_summaryColumn:I = 0x2

.field public static final CycleInterpolator:[I

.field public static final CycleInterpolator_cycles:I = 0x0

.field public static final DatePicker:[I

.field public static final DatePicker_calendarViewShown:I = 0x5

.field public static final DatePicker_endYear:I = 0x1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DatePicker_internalLayout:I = 0x6

.field public static final DatePicker_maxDate:I = 0x3

.field public static final DatePicker_minDate:I = 0x2

.field public static final DatePicker_spinnersShown:I = 0x4

.field public static final DatePicker_startYear:I = 0x0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final DecelerateInterpolator:[I

.field public static final DecelerateInterpolator_factor:I = 0x0

.field public static final DeviceAdmin:[I

.field public static final DeviceAdmin_visible:I = 0x0

.field public static final DialogPreference:[I

.field public static final DialogPreference_dialogIcon:I = 0x2

.field public static final DialogPreference_dialogLayout:I = 0x5

.field public static final DialogPreference_dialogMessage:I = 0x1

.field public static final DialogPreference_dialogTitle:I = 0x0

.field public static final DialogPreference_negativeButtonText:I = 0x4

.field public static final DialogPreference_positiveButtonText:I = 0x3

.field public static final Drawable:[I

.field public static final DrawableCorners:[I

.field public static final DrawableCorners_bottomLeftRadius:I = 0x3

.field public static final DrawableCorners_bottomRightRadius:I = 0x4

.field public static final DrawableCorners_radius:I = 0x0

.field public static final DrawableCorners_topLeftRadius:I = 0x1

.field public static final DrawableCorners_topRightRadius:I = 0x2

.field public static final DrawableStates:[I

.field public static final DrawableStates_state_accelerated:I = 0xd

.field public static final DrawableStates_state_accessibility_focused:I = 0x11

.field public static final DrawableStates_state_activated:I = 0xc

.field public static final DrawableStates_state_active:I = 0x6

.field public static final DrawableStates_state_checkable:I = 0x3

.field public static final DrawableStates_state_checked:I = 0x4

.field public static final DrawableStates_state_drag_can_accept:I = 0xf

.field public static final DrawableStates_state_drag_hovered:I = 0x10

.field public static final DrawableStates_state_enabled:I = 0x2

.field public static final DrawableStates_state_first:I = 0x8

.field public static final DrawableStates_state_focused:I = 0x0

.field public static final DrawableStates_state_hovered:I = 0xe

.field public static final DrawableStates_state_last:I = 0xa

.field public static final DrawableStates_state_middle:I = 0x9

.field public static final DrawableStates_state_pressed:I = 0xb

.field public static final DrawableStates_state_selected:I = 0x5

.field public static final DrawableStates_state_single:I = 0x7

.field public static final DrawableStates_state_window_focused:I = 0x1

.field public static final Drawable_visible:I = 0x0

.field public static final Dream:[I

.field public static final Dream_settingsActivity:I = 0x0

.field public static final EditText:[I

.field public static final ExpandableListChildIndicatorState:[I

.field public static final ExpandableListChildIndicatorState_state_last:I = 0x0

.field public static final ExpandableListGroupIndicatorState:[I

.field public static final ExpandableListGroupIndicatorState_state_empty:I = 0x1

.field public static final ExpandableListGroupIndicatorState_state_expanded:I = 0x0

.field public static final ExpandableListView:[I

.field public static final ExpandableListView_childDivider:I = 0x6

.field public static final ExpandableListView_childIndicator:I = 0x1

.field public static final ExpandableListView_childIndicatorLeft:I = 0x4

.field public static final ExpandableListView_childIndicatorRight:I = 0x5

.field public static final ExpandableListView_groupIndicator:I = 0x0

.field public static final ExpandableListView_indicatorLeft:I = 0x2

.field public static final ExpandableListView_indicatorRight:I = 0x3

.field public static final Extra:[I

.field public static final Extra_name:I = 0x0

.field public static final Extra_value:I = 0x1

.field public static final Fragment:[I

.field public static final FragmentAnimation:[I

.field public static final FragmentAnimation_fragmentCloseEnterAnimation:I = 0x2

.field public static final FragmentAnimation_fragmentCloseExitAnimation:I = 0x3

.field public static final FragmentAnimation_fragmentFadeEnterAnimation:I = 0x4

.field public static final FragmentAnimation_fragmentFadeExitAnimation:I = 0x5

.field public static final FragmentAnimation_fragmentOpenEnterAnimation:I = 0x0

.field public static final FragmentAnimation_fragmentOpenExitAnimation:I = 0x1

.field public static final FragmentBreadCrumbs:[I

.field public static final FragmentBreadCrumbs_gravity:I = 0x0

.field public static final Fragment_id:I = 0x1

.field public static final Fragment_name:I = 0x0

.field public static final Fragment_tag:I = 0x2

.field public static final FrameLayout:[I

.field public static final FrameLayout_Layout:[I

.field public static final FrameLayout_Layout_layout_gravity:I = 0x0

.field public static final FrameLayout_foreground:I = 0x0

.field public static final FrameLayout_foregroundGravity:I = 0x2

.field public static final FrameLayout_foregroundInsidePadding:I = 0x3

.field public static final FrameLayout_measureAllChildren:I = 0x1

.field public static final Gallery:[I

.field public static final Gallery_animationDuration:I = 0x1

.field public static final Gallery_gravity:I = 0x0

.field public static final Gallery_spacing:I = 0x2

.field public static final Gallery_unselectedAlpha:I = 0x3

.field public static final GestureOverlayView:[I

.field public static final GestureOverlayView_eventsInterceptionEnabled:I = 0xa

.field public static final GestureOverlayView_fadeDuration:I = 0x5

.field public static final GestureOverlayView_fadeEnabled:I = 0xb

.field public static final GestureOverlayView_fadeOffset:I = 0x4

.field public static final GestureOverlayView_gestureColor:I = 0x2

.field public static final GestureOverlayView_gestureStrokeAngleThreshold:I = 0x9

.field public static final GestureOverlayView_gestureStrokeLengthThreshold:I = 0x7

.field public static final GestureOverlayView_gestureStrokeSquarenessThreshold:I = 0x8

.field public static final GestureOverlayView_gestureStrokeType:I = 0x6

.field public static final GestureOverlayView_gestureStrokeWidth:I = 0x1

.field public static final GestureOverlayView_orientation:I = 0x0

.field public static final GestureOverlayView_uncertainGestureColor:I = 0x3

.field public static final GlowPadView:[I

.field public static final GlowPadView_allowScaling:I = 0x9

.field public static final GlowPadView_alwaysTrackFinger:I = 0x10

.field public static final GlowPadView_directionDescriptions:I = 0x3

.field public static final GlowPadView_feedbackCount:I = 0xf

.field public static final GlowPadView_firstItemOffset:I = 0x7

.field public static final GlowPadView_glowRadius:I = 0x6

.field public static final GlowPadView_gravity:I = 0x0

.field public static final GlowPadView_handleDrawable:I = 0xb

.field public static final GlowPadView_innerRadius:I = 0x1

.field public static final GlowPadView_magneticTargets:I = 0x8

.field public static final GlowPadView_outerRadius:I = 0xc

.field public static final GlowPadView_outerRingDrawable:I = 0x4

.field public static final GlowPadView_pointDrawable:I = 0x5

.field public static final GlowPadView_snapMargin:I = 0xe

.field public static final GlowPadView_targetDescriptions:I = 0x2

.field public static final GlowPadView_targetDrawables:I = 0xa

.field public static final GlowPadView_vibrationDuration:I = 0xd

.field public static final GradientDrawable:[I

.field public static final GradientDrawableGradient:[I

.field public static final GradientDrawableGradient_angle:I = 0x3

.field public static final GradientDrawableGradient_centerColor:I = 0x8

.field public static final GradientDrawableGradient_centerX:I = 0x5

.field public static final GradientDrawableGradient_centerY:I = 0x6

.field public static final GradientDrawableGradient_endColor:I = 0x1

.field public static final GradientDrawableGradient_gradientRadius:I = 0x7

.field public static final GradientDrawableGradient_startColor:I = 0x0

.field public static final GradientDrawableGradient_type:I = 0x4

.field public static final GradientDrawableGradient_useLevel:I = 0x2

.field public static final GradientDrawablePadding:[I

.field public static final GradientDrawablePadding_bottom:I = 0x3

.field public static final GradientDrawablePadding_left:I = 0x0

.field public static final GradientDrawablePadding_right:I = 0x2

.field public static final GradientDrawablePadding_top:I = 0x1

.field public static final GradientDrawableSize:[I

.field public static final GradientDrawableSize_height:I = 0x0

.field public static final GradientDrawableSize_width:I = 0x1

.field public static final GradientDrawableSolid:[I

.field public static final GradientDrawableSolid_color:I = 0x0

.field public static final GradientDrawableStroke:[I

.field public static final GradientDrawableStroke_color:I = 0x1

.field public static final GradientDrawableStroke_dashGap:I = 0x3

.field public static final GradientDrawableStroke_dashWidth:I = 0x2

.field public static final GradientDrawableStroke_width:I = 0x0

.field public static final GradientDrawable_dither:I = 0x0

.field public static final GradientDrawable_innerRadius:I = 0x6

.field public static final GradientDrawable_innerRadiusRatio:I = 0x3

.field public static final GradientDrawable_shape:I = 0x2

.field public static final GradientDrawable_thickness:I = 0x7

.field public static final GradientDrawable_thicknessRatio:I = 0x4

.field public static final GradientDrawable_useLevel:I = 0x5

.field public static final GradientDrawable_visible:I = 0x1

.field public static final GridLayout:[I

.field public static final GridLayoutAnimation:[I

.field public static final GridLayoutAnimation_columnDelay:I = 0x0

.field public static final GridLayoutAnimation_direction:I = 0x2

.field public static final GridLayoutAnimation_directionPriority:I = 0x3

.field public static final GridLayoutAnimation_rowDelay:I = 0x1

.field public static final GridLayout_Layout:[I

.field public static final GridLayout_Layout_layout_column:I = 0x1

.field public static final GridLayout_Layout_layout_columnSpan:I = 0x4

.field public static final GridLayout_Layout_layout_gravity:I = 0x0

.field public static final GridLayout_Layout_layout_row:I = 0x2

.field public static final GridLayout_Layout_layout_rowSpan:I = 0x3

.field public static final GridLayout_alignmentMode:I = 0x6

.field public static final GridLayout_columnCount:I = 0x3

.field public static final GridLayout_columnOrderPreserved:I = 0x4

.field public static final GridLayout_orientation:I = 0x0

.field public static final GridLayout_rowCount:I = 0x1

.field public static final GridLayout_rowOrderPreserved:I = 0x2

.field public static final GridLayout_useDefaultMargins:I = 0x5

.field public static final GridView:[I

.field public static final GridView_columnWidth:I = 0x4

.field public static final GridView_gravity:I = 0x0

.field public static final GridView_horizontalSpacing:I = 0x1

.field public static final GridView_numColumns:I = 0x5

.field public static final GridView_stretchMode:I = 0x3

.field public static final GridView_verticalSpacing:I = 0x2

.field public static final HorizontalScrollView:[I

.field public static final HorizontalScrollView_fillViewport:I = 0x0

.field public static final Icon:[I

.field public static final IconDefault:[I

.field public static final IconDefault_icon:I = 0x0

.field public static final IconMenuView:[I

.field public static final IconMenuView_maxItems:I = 0x4

.field public static final IconMenuView_maxItemsPerRow:I = 0x2

.field public static final IconMenuView_maxRows:I = 0x1

.field public static final IconMenuView_moreIcon:I = 0x3

.field public static final IconMenuView_rowHeight:I = 0x0

.field public static final Icon_icon:I = 0x0

.field public static final Icon_mimeType:I = 0x1

.field public static final ImageSwitcher:[I

.field public static final ImageView:[I

.field public static final ImageView_adjustViewBounds:I = 0x2

.field public static final ImageView_baseline:I = 0x8

.field public static final ImageView_baselineAlignBottom:I = 0x6

.field public static final ImageView_cropToPadding:I = 0x7

.field public static final ImageView_drawableAlpha:I = 0x9

.field public static final ImageView_maxHeight:I = 0x4

.field public static final ImageView_maxWidth:I = 0x3

.field public static final ImageView_scaleType:I = 0x1

.field public static final ImageView_src:I = 0x0

.field public static final ImageView_tint:I = 0x5

.field public static final InputExtras:[I

.field public static final InputMethod:[I

.field public static final InputMethodService:[I

.field public static final InputMethodService_imeExtractEnterAnimation:I = 0x1

.field public static final InputMethodService_imeExtractExitAnimation:I = 0x2

.field public static final InputMethodService_imeFullscreenBackground:I = 0x0

.field public static final InputMethod_Subtype:[I

.field public static final InputMethod_Subtype_icon:I = 0x1

.field public static final InputMethod_Subtype_imeSubtypeExtraValue:I = 0x4

.field public static final InputMethod_Subtype_imeSubtypeLocale:I = 0x2

.field public static final InputMethod_Subtype_imeSubtypeMode:I = 0x3

.field public static final InputMethod_Subtype_isAuxiliary:I = 0x5

.field public static final InputMethod_Subtype_label:I = 0x0

.field public static final InputMethod_Subtype_overridesImplicitlyEnabledSubtype:I = 0x6

.field public static final InputMethod_Subtype_subtypeId:I = 0x7

.field public static final InputMethod_isDefault:I = 0x0

.field public static final InputMethod_settingsActivity:I = 0x1

.field public static final InsetDrawable:[I

.field public static final InsetDrawable_drawable:I = 0x1

.field public static final InsetDrawable_insetBottom:I = 0x5

.field public static final InsetDrawable_insetLeft:I = 0x2

.field public static final InsetDrawable_insetRight:I = 0x3

.field public static final InsetDrawable_insetTop:I = 0x4

.field public static final InsetDrawable_visible:I = 0x0

.field public static final Intent:[I

.field public static final IntentCategory:[I

.field public static final IntentCategory_name:I = 0x0

.field public static final Intent_action:I = 0x2

.field public static final Intent_data:I = 0x3

.field public static final Intent_mimeType:I = 0x1

.field public static final Intent_targetClass:I = 0x4

.field public static final Intent_targetPackage:I = 0x0

.field public static final Keyboard:[I

.field public static final KeyboardLayout:[I

.field public static final KeyboardLayout_keyboardLayout:I = 0x2

.field public static final KeyboardLayout_label:I = 0x0

.field public static final KeyboardLayout_name:I = 0x1

.field public static final KeyboardView:[I

.field public static final KeyboardViewPreviewState:[I

.field public static final KeyboardViewPreviewState_state_long_pressable:I = 0x0

.field public static final KeyboardView_keyBackground:I = 0x2

.field public static final KeyboardView_keyPreviewHeight:I = 0x8

.field public static final KeyboardView_keyPreviewLayout:I = 0x6

.field public static final KeyboardView_keyPreviewOffset:I = 0x7

.field public static final KeyboardView_keyTextColor:I = 0x5

.field public static final KeyboardView_keyTextSize:I = 0x3

.field public static final KeyboardView_keyboardViewStyle:I = 0xb

.field public static final KeyboardView_labelTextSize:I = 0x4

.field public static final KeyboardView_popupLayout:I = 0xa

.field public static final KeyboardView_shadowColor:I = 0x0

.field public static final KeyboardView_shadowRadius:I = 0x1

.field public static final KeyboardView_verticalCorrection:I = 0x9

.field public static final Keyboard_Key:[I

.field public static final Keyboard_Key_codes:I = 0x0

.field public static final Keyboard_Key_iconPreview:I = 0x7

.field public static final Keyboard_Key_isModifier:I = 0x4

.field public static final Keyboard_Key_isRepeatable:I = 0x6

.field public static final Keyboard_Key_isSticky:I = 0x5

.field public static final Keyboard_Key_keyEdgeFlags:I = 0x3

.field public static final Keyboard_Key_keyIcon:I = 0xa

.field public static final Keyboard_Key_keyLabel:I = 0x9

.field public static final Keyboard_Key_keyOutputText:I = 0x8

.field public static final Keyboard_Key_keyboardMode:I = 0xb

.field public static final Keyboard_Key_popupCharacters:I = 0x2

.field public static final Keyboard_Key_popupKeyboard:I = 0x1

.field public static final Keyboard_Row:[I

.field public static final Keyboard_Row_keyboardMode:I = 0x1

.field public static final Keyboard_Row_rowEdgeFlags:I = 0x0

.field public static final Keyboard_horizontalGap:I = 0x2

.field public static final Keyboard_keyHeight:I = 0x1

.field public static final Keyboard_keyWidth:I = 0x0

.field public static final Keyboard_verticalGap:I = 0x3

.field public static final KeyguardGlowStripView:[I

.field public static final KeyguardGlowStripView_dotSize:I = 0x0

.field public static final KeyguardGlowStripView_glowDot:I = 0x2

.field public static final KeyguardGlowStripView_leftToRight:I = 0x3

.field public static final KeyguardGlowStripView_numDots:I = 0x1

.field public static final KeyguardSecurityViewFlipper_Layout:[I

.field public static final KeyguardSecurityViewFlipper_Layout_layout_maxHeight:I = 0x0

.field public static final KeyguardSecurityViewFlipper_Layout_layout_maxWidth:I = 0x1

.field public static final LayerDrawable:[I

.field public static final LayerDrawableItem:[I

.field public static final LayerDrawableItem_bottom:I = 0x5

.field public static final LayerDrawableItem_drawable:I = 0x1

.field public static final LayerDrawableItem_id:I = 0x0

.field public static final LayerDrawableItem_left:I = 0x2

.field public static final LayerDrawableItem_right:I = 0x4

.field public static final LayerDrawableItem_top:I = 0x3

.field public static final LayerDrawable_opacity:I = 0x0

.field public static final LayoutAnimation:[I

.field public static final LayoutAnimation_animation:I = 0x2

.field public static final LayoutAnimation_animationOrder:I = 0x3

.field public static final LayoutAnimation_delay:I = 0x1

.field public static final LayoutAnimation_interpolator:I = 0x0

.field public static final LevelListDrawableItem:[I

.field public static final LevelListDrawableItem_drawable:I = 0x0

.field public static final LevelListDrawableItem_maxLevel:I = 0x2

.field public static final LevelListDrawableItem_minLevel:I = 0x1

.field public static final LinearLayout:[I

.field public static final LinearLayout_Layout:[I

.field public static final LinearLayout_Layout_layout_gravity:I = 0x0

.field public static final LinearLayout_Layout_layout_height:I = 0x2

.field public static final LinearLayout_Layout_layout_weight:I = 0x3

.field public static final LinearLayout_Layout_layout_width:I = 0x1

.field public static final LinearLayout_baselineAligned:I = 0x2

.field public static final LinearLayout_baselineAlignedChildIndex:I = 0x3

.field public static final LinearLayout_divider:I = 0x5

.field public static final LinearLayout_dividerPadding:I = 0x8

.field public static final LinearLayout_gravity:I = 0x0

.field public static final LinearLayout_measureWithLargestChild:I = 0x6

.field public static final LinearLayout_orientation:I = 0x1

.field public static final LinearLayout_showDividers:I = 0x7

.field public static final LinearLayout_weightSum:I = 0x4

.field public static final ListPreference:[I

.field public static final ListPreference_entries:I = 0x0

.field public static final ListPreference_entryValues:I = 0x1

.field public static final ListView:[I

.field public static final ListView_divider:I = 0x1

.field public static final ListView_dividerHeight:I = 0x2

.field public static final ListView_entries:I = 0x0

.field public static final ListView_footerDividersEnabled:I = 0x4

.field public static final ListView_headerDividersEnabled:I = 0x3

.field public static final ListView_overScrollFooter:I = 0x6

.field public static final ListView_overScrollHeader:I = 0x5

.field public static final LockPatternView:[I

.field public static final LockPatternView_aspect:I = 0x0

.field public static final MapView:[I

.field public static final MapView_apiKey:I = 0x0

.field public static final MediaRouteButton:[I

.field public static final MediaRouteButton_externalRouteEnabledDrawable:I = 0x3

.field public static final MediaRouteButton_mediaRouteTypes:I = 0x2

.field public static final MediaRouteButton_minHeight:I = 0x1

.field public static final MediaRouteButton_minWidth:I = 0x0

.field public static final Menu:[I

.field public static final MenuGroup:[I

.field public static final MenuGroup_checkableBehavior:I = 0x5

.field public static final MenuGroup_enabled:I = 0x0

.field public static final MenuGroup_id:I = 0x1

.field public static final MenuGroup_menuCategory:I = 0x3

.field public static final MenuGroup_orderInCategory:I = 0x4

.field public static final MenuGroup_visible:I = 0x2

.field public static final MenuItem:[I

.field public static final MenuItemCheckedFocusedState:[I

.field public static final MenuItemCheckedFocusedState_state_checkable:I = 0x1

.field public static final MenuItemCheckedFocusedState_state_checked:I = 0x2

.field public static final MenuItemCheckedFocusedState_state_focused:I = 0x0

.field public static final MenuItemCheckedState:[I

.field public static final MenuItemCheckedState_state_checkable:I = 0x0

.field public static final MenuItemCheckedState_state_checked:I = 0x1

.field public static final MenuItemUncheckedFocusedState:[I

.field public static final MenuItemUncheckedFocusedState_state_checkable:I = 0x1

.field public static final MenuItemUncheckedFocusedState_state_focused:I = 0x0

.field public static final MenuItemUncheckedState:[I

.field public static final MenuItemUncheckedState_state_checkable:I = 0x0

.field public static final MenuItem_actionLayout:I = 0xe

.field public static final MenuItem_actionProviderClass:I = 0x10

.field public static final MenuItem_actionViewClass:I = 0xf

.field public static final MenuItem_alphabeticShortcut:I = 0x9

.field public static final MenuItem_checkable:I = 0xb

.field public static final MenuItem_checked:I = 0x3

.field public static final MenuItem_enabled:I = 0x1

.field public static final MenuItem_icon:I = 0x0

.field public static final MenuItem_id:I = 0x2

.field public static final MenuItem_menuCategory:I = 0x5

.field public static final MenuItem_numericShortcut:I = 0xa

.field public static final MenuItem_onClick:I = 0xc

.field public static final MenuItem_orderInCategory:I = 0x6

.field public static final MenuItem_showAsAction:I = 0xd

.field public static final MenuItem_title:I = 0x7

.field public static final MenuItem_titleCondensed:I = 0x8

.field public static final MenuItem_visible:I = 0x4

.field public static final MenuView:[I

.field public static final MenuView_headerBackground:I = 0x4

.field public static final MenuView_horizontalDivider:I = 0x2

.field public static final MenuView_itemBackground:I = 0x5

.field public static final MenuView_itemIconDisabledAlpha:I = 0x6

.field public static final MenuView_itemTextAppearance:I = 0x1

.field public static final MenuView_preserveIconSpacing:I = 0x7

.field public static final MenuView_verticalDivider:I = 0x3

.field public static final MenuView_windowAnimationStyle:I = 0x0

.field public static final MipmapDrawableItem:[I

.field public static final MipmapDrawableItem_drawable:I = 0x0

.field public static final MultiPaneChallengeLayout:[I

.field public static final MultiPaneChallengeLayout_Layout:[I

.field public static final MultiPaneChallengeLayout_Layout_layout_centerWithinArea:I = 0x3

.field public static final MultiPaneChallengeLayout_Layout_layout_childType:I = 0x2

.field public static final MultiPaneChallengeLayout_Layout_layout_gravity:I = 0x0

.field public static final MultiPaneChallengeLayout_Layout_layout_maxHeight:I = 0x1

.field public static final MultiPaneChallengeLayout_Layout_layout_maxWidth:I = 0x4

.field public static final MultiPaneChallengeLayout_orientation:I = 0x0

.field public static final MultiSelectListPreference:[I

.field public static final MultiSelectListPreference_entries:I = 0x0

.field public static final MultiSelectListPreference_entryValues:I = 0x1

.field public static final MultiWaveView:[I

.field public static final MultiWaveView_alwaysTrackFinger:I = 0xa

.field public static final MultiWaveView_chevronDrawables:I = 0x4

.field public static final MultiWaveView_directionDescriptions:I = 0x1

.field public static final MultiWaveView_feedbackCount:I = 0x9

.field public static final MultiWaveView_handleDrawable:I = 0x3

.field public static final MultiWaveView_outerRadius:I = 0x6

.field public static final MultiWaveView_snapMargin:I = 0x8

.field public static final MultiWaveView_targetDescriptions:I = 0x0

.field public static final MultiWaveView_targetDrawables:I = 0x2

.field public static final MultiWaveView_vibrationDuration:I = 0x7

.field public static final MultiWaveView_waveDrawable:I = 0x5

.field public static final NinePatchDrawable:[I

.field public static final NinePatchDrawable_dither:I = 0x1

.field public static final NinePatchDrawable_src:I = 0x0

.field public static final NumPadKey:[I

.field public static final NumPadKey_digit:I = 0x0

.field public static final NumPadKey_textView:I = 0x1

.field public static final NumberPicker:[I

.field public static final NumberPicker_internalLayout:I = 0x1

.field public static final NumberPicker_internalMaxHeight:I = 0x6

.field public static final NumberPicker_internalMaxWidth:I = 0x8

.field public static final NumberPicker_internalMinHeight:I = 0x5

.field public static final NumberPicker_internalMinWidth:I = 0x7

.field public static final NumberPicker_selectionDivider:I = 0x2

.field public static final NumberPicker_selectionDividerHeight:I = 0x3

.field public static final NumberPicker_selectionDividersDistance:I = 0x4

.field public static final NumberPicker_solidColor:I = 0x0

.field public static final NumberPicker_virtualButtonPressedDrawable:I = 0x9

.field public static final OvershootInterpolator:[I

.field public static final OvershootInterpolator_tension:I = 0x0

.field public static final PagedView:[I

.field public static final PagedView_pageSpacing:I = 0x0

.field public static final PagedView_scrollIndicatorPaddingLeft:I = 0x1

.field public static final PagedView_scrollIndicatorPaddingRight:I = 0x2

.field public static final Pointer:[I

.field public static final PointerIcon:[I

.field public static final PointerIcon_bitmap:I = 0x0

.field public static final PointerIcon_hotSpotX:I = 0x1

.field public static final PointerIcon_hotSpotY:I = 0x2

.field public static final Pointer_pointerIconArrow:I = 0x0

.field public static final Pointer_pointerIconSpotAnchor:I = 0x3

.field public static final Pointer_pointerIconSpotBlank1:I = 0x2d

.field public static final Pointer_pointerIconSpotBlank2:I = 0x2e

.field public static final Pointer_pointerIconSpotBlank3:I = 0x2f

.field public static final Pointer_pointerIconSpotBlank4:I = 0x30

.field public static final Pointer_pointerIconSpotBlank5:I = 0x31

.field public static final Pointer_pointerIconSpotClaw1:I = 0xa

.field public static final Pointer_pointerIconSpotClaw2:I = 0xb

.field public static final Pointer_pointerIconSpotHand:I = 0x9

.field public static final Pointer_pointerIconSpotHold1:I = 0xc

.field public static final Pointer_pointerIconSpotHold2:I = 0xd

.field public static final Pointer_pointerIconSpotHold3:I = 0xe

.field public static final Pointer_pointerIconSpotHold4:I = 0xf

.field public static final Pointer_pointerIconSpotHold5:I = 0x10

.field public static final Pointer_pointerIconSpotHold6:I = 0x11

.field public static final Pointer_pointerIconSpotHold7:I = 0x12

.field public static final Pointer_pointerIconSpotHold8:I = 0x13

.field public static final Pointer_pointerIconSpotHold9:I = 0x14

.field public static final Pointer_pointerIconSpotHover:I = 0x1

.field public static final Pointer_pointerIconSpotLback1:I = 0x15

.field public static final Pointer_pointerIconSpotLback2:I = 0x16

.field public static final Pointer_pointerIconSpotLback3:I = 0x17

.field public static final Pointer_pointerIconSpotLback4:I = 0x18

.field public static final Pointer_pointerIconSpotLback5:I = 0x19

.field public static final Pointer_pointerIconSpotLback6:I = 0x1a

.field public static final Pointer_pointerIconSpotLback7:I = 0x1b

.field public static final Pointer_pointerIconSpotLback8:I = 0x1c

.field public static final Pointer_pointerIconSpotPagedown:I = 0x21

.field public static final Pointer_pointerIconSpotPagedown1:I = 0x22

.field public static final Pointer_pointerIconSpotPagedown2:I = 0x23

.field public static final Pointer_pointerIconSpotPagedown3:I = 0x24

.field public static final Pointer_pointerIconSpotPageup:I = 0x1d

.field public static final Pointer_pointerIconSpotPageup1:I = 0x1e

.field public static final Pointer_pointerIconSpotPageup2:I = 0x1f

.field public static final Pointer_pointerIconSpotPageup3:I = 0x20

.field public static final Pointer_pointerIconSpotRback1:I = 0x25

.field public static final Pointer_pointerIconSpotRback2:I = 0x26

.field public static final Pointer_pointerIconSpotRback3:I = 0x27

.field public static final Pointer_pointerIconSpotRback4:I = 0x28

.field public static final Pointer_pointerIconSpotRback5:I = 0x29

.field public static final Pointer_pointerIconSpotRback6:I = 0x2a

.field public static final Pointer_pointerIconSpotRback7:I = 0x2b

.field public static final Pointer_pointerIconSpotRback8:I = 0x2c

.field public static final Pointer_pointerIconSpotTouch:I = 0x2

.field public static final Pointer_pointerIconSpotWave1:I = 0x4

.field public static final Pointer_pointerIconSpotWave2:I = 0x5

.field public static final Pointer_pointerIconSpotWave3:I = 0x6

.field public static final Pointer_pointerIconSpotWave4:I = 0x7

.field public static final Pointer_pointerIconSpotWave5:I = 0x8

.field public static final PopupWindow:[I

.field public static final PopupWindowBackgroundState:[I

.field public static final PopupWindowBackgroundState_state_above_anchor:I = 0x0

.field public static final PopupWindow_popupAnimationStyle:I = 0x1

.field public static final PopupWindow_popupBackground:I = 0x0

.field public static final Preference:[I

.field public static final PreferenceFrameLayout:[I

.field public static final PreferenceFrameLayout_Layout:[I

.field public static final PreferenceFrameLayout_Layout_layout_removeBorders:I = 0x0

.field public static final PreferenceFrameLayout_borderBottom:I = 0x1

.field public static final PreferenceFrameLayout_borderLeft:I = 0x2

.field public static final PreferenceFrameLayout_borderRight:I = 0x3

.field public static final PreferenceFrameLayout_borderTop:I = 0x0

.field public static final PreferenceGroup:[I

.field public static final PreferenceGroup_orderingFromXml:I = 0x0

.field public static final PreferenceHeader:[I

.field public static final PreferenceHeader_breadCrumbShortTitle:I = 0x6

.field public static final PreferenceHeader_breadCrumbTitle:I = 0x5

.field public static final PreferenceHeader_fragment:I = 0x4

.field public static final PreferenceHeader_icon:I = 0x0

.field public static final PreferenceHeader_id:I = 0x1

.field public static final PreferenceHeader_summary:I = 0x3

.field public static final PreferenceHeader_title:I = 0x2

.field public static final Preference_defaultValue:I = 0xb

.field public static final Preference_dependency:I = 0xa

.field public static final Preference_enabled:I = 0x2

.field public static final Preference_fragment:I = 0xd

.field public static final Preference_icon:I = 0x0

.field public static final Preference_key:I = 0x6

.field public static final Preference_layout:I = 0x3

.field public static final Preference_order:I = 0x8

.field public static final Preference_persistent:I = 0x1

.field public static final Preference_selectable:I = 0x5

.field public static final Preference_shouldDisableView:I = 0xc

.field public static final Preference_summary:I = 0x7

.field public static final Preference_title:I = 0x4

.field public static final Preference_widgetLayout:I = 0x9

.field public static final ProgressBar:[I

.field public static final ProgressBar_animationResolution:I = 0xe
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final ProgressBar_indeterminate:I = 0x5

.field public static final ProgressBar_indeterminateBehavior:I = 0xa

.field public static final ProgressBar_indeterminateDrawable:I = 0x7

.field public static final ProgressBar_indeterminateDuration:I = 0x9

.field public static final ProgressBar_indeterminateOnly:I = 0x6

.field public static final ProgressBar_interpolator:I = 0xd

.field public static final ProgressBar_max:I = 0x2

.field public static final ProgressBar_maxHeight:I = 0x1

.field public static final ProgressBar_maxWidth:I = 0x0

.field public static final ProgressBar_minHeight:I = 0xc

.field public static final ProgressBar_minWidth:I = 0xb

.field public static final ProgressBar_progress:I = 0x3

.field public static final ProgressBar_progressDrawable:I = 0x8

.field public static final ProgressBar_secondaryProgress:I = 0x4

.field public static final PropertyAnimator:[I

.field public static final PropertyAnimator_propertyName:I = 0x0

.field public static final QuickContactBadge:[I

.field public static final QuickContactBadge_quickContactWindowSize:I = 0x0

.field public static final RadioGroup:[I

.field public static final RadioGroup_checkedButton:I = 0x1

.field public static final RadioGroup_orientation:I = 0x0

.field public static final RatingBar:[I

.field public static final RatingBar_isIndicator:I = 0x3

.field public static final RatingBar_numStars:I = 0x0

.field public static final RatingBar_rating:I = 0x1

.field public static final RatingBar_stepSize:I = 0x2

.field public static final RecognitionService:[I

.field public static final RecognitionService_settingsActivity:I = 0x0

.field public static final RelativeLayout:[I

.field public static final RelativeLayout_Layout:[I

.field public static final RelativeLayout_Layout_layout_above:I = 0x2

.field public static final RelativeLayout_Layout_layout_alignBaseline:I = 0x4

.field public static final RelativeLayout_Layout_layout_alignBottom:I = 0x8

.field public static final RelativeLayout_Layout_layout_alignEnd:I = 0x14

.field public static final RelativeLayout_Layout_layout_alignLeft:I = 0x5

.field public static final RelativeLayout_Layout_layout_alignParentBottom:I = 0xc

.field public static final RelativeLayout_Layout_layout_alignParentEnd:I = 0x16

.field public static final RelativeLayout_Layout_layout_alignParentLeft:I = 0x9

.field public static final RelativeLayout_Layout_layout_alignParentRight:I = 0xb

.field public static final RelativeLayout_Layout_layout_alignParentStart:I = 0x15

.field public static final RelativeLayout_Layout_layout_alignParentTop:I = 0xa

.field public static final RelativeLayout_Layout_layout_alignRight:I = 0x7

.field public static final RelativeLayout_Layout_layout_alignStart:I = 0x13

.field public static final RelativeLayout_Layout_layout_alignTop:I = 0x6

.field public static final RelativeLayout_Layout_layout_alignWithParentIfMissing:I = 0x10

.field public static final RelativeLayout_Layout_layout_below:I = 0x3

.field public static final RelativeLayout_Layout_layout_centerHorizontal:I = 0xe

.field public static final RelativeLayout_Layout_layout_centerInParent:I = 0xd

.field public static final RelativeLayout_Layout_layout_centerVertical:I = 0xf

.field public static final RelativeLayout_Layout_layout_toEndOf:I = 0x12

.field public static final RelativeLayout_Layout_layout_toLeftOf:I = 0x0

.field public static final RelativeLayout_Layout_layout_toRightOf:I = 0x1

.field public static final RelativeLayout_Layout_layout_toStartOf:I = 0x11

.field public static final RelativeLayout_gravity:I = 0x0

.field public static final RelativeLayout_ignoreGravity:I = 0x1

.field public static final RingtonePreference:[I

.field public static final RingtonePreference_ringtoneType:I = 0x0

.field public static final RingtonePreference_showDefault:I = 0x1

.field public static final RingtonePreference_showSilent:I = 0x2

.field public static final RotarySelector:[I

.field public static final RotarySelector_orientation:I = 0x0

.field public static final RotateAnimation:[I

.field public static final RotateAnimation_fromDegrees:I = 0x0

.field public static final RotateAnimation_pivotX:I = 0x2

.field public static final RotateAnimation_pivotY:I = 0x3

.field public static final RotateAnimation_toDegrees:I = 0x1

.field public static final RotateDrawable:[I

.field public static final RotateDrawable_drawable:I = 0x1

.field public static final RotateDrawable_fromDegrees:I = 0x2

.field public static final RotateDrawable_pivotX:I = 0x4

.field public static final RotateDrawable_pivotY:I = 0x5

.field public static final RotateDrawable_toDegrees:I = 0x3

.field public static final RotateDrawable_visible:I = 0x0

.field public static final ScaleAnimation:[I

.field public static final ScaleAnimation_fromXScale:I = 0x2

.field public static final ScaleAnimation_fromYScale:I = 0x4

.field public static final ScaleAnimation_pivotX:I = 0x0

.field public static final ScaleAnimation_pivotY:I = 0x1

.field public static final ScaleAnimation_toXScale:I = 0x3

.field public static final ScaleAnimation_toYScale:I = 0x5

.field public static final ScaleDrawable:[I

.field public static final ScaleDrawable_drawable:I = 0x0

.field public static final ScaleDrawable_scaleGravity:I = 0x3

.field public static final ScaleDrawable_scaleHeight:I = 0x2

.field public static final ScaleDrawable_scaleWidth:I = 0x1

.field public static final ScaleDrawable_useIntrinsicSizeAsMinimum:I = 0x4

.field public static final ScrollView:[I

.field public static final ScrollView_fillViewport:I = 0x0

.field public static final SearchView:[I

.field public static final SearchView_iconifiedByDefault:I = 0x3

.field public static final SearchView_imeOptions:I = 0x2

.field public static final SearchView_inputType:I = 0x1

.field public static final SearchView_maxWidth:I = 0x0

.field public static final SearchView_queryHint:I = 0x4

.field public static final Searchable:[I

.field public static final SearchableActionKey:[I

.field public static final SearchableActionKey_keycode:I = 0x0

.field public static final SearchableActionKey_queryActionMsg:I = 0x1

.field public static final SearchableActionKey_suggestActionMsg:I = 0x2

.field public static final SearchableActionKey_suggestActionMsgColumn:I = 0x3

.field public static final Searchable_autoUrlDetect:I = 0x15

.field public static final Searchable_hint:I = 0x2

.field public static final Searchable_icon:I = 0x1

.field public static final Searchable_imeOptions:I = 0x10

.field public static final Searchable_includeInGlobalSearch:I = 0x12

.field public static final Searchable_inputType:I = 0xa

.field public static final Searchable_label:I = 0x0

.field public static final Searchable_queryAfterZeroResults:I = 0x13

.field public static final Searchable_searchButtonText:I = 0x9
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final Searchable_searchMode:I = 0x3

.field public static final Searchable_searchSettingsDescription:I = 0x14

.field public static final Searchable_searchSuggestAuthority:I = 0x4

.field public static final Searchable_searchSuggestIntentAction:I = 0x7

.field public static final Searchable_searchSuggestIntentData:I = 0x8

.field public static final Searchable_searchSuggestPath:I = 0x5

.field public static final Searchable_searchSuggestSelection:I = 0x6

.field public static final Searchable_searchSuggestThreshold:I = 0x11

.field public static final Searchable_voiceLanguage:I = 0xe

.field public static final Searchable_voiceLanguageModel:I = 0xc

.field public static final Searchable_voiceMaxResults:I = 0xf

.field public static final Searchable_voicePromptText:I = 0xd

.field public static final Searchable_voiceSearchMode:I = 0xb

.field public static final SeekBar:[I

.field public static final SeekBar_thumb:I = 0x0

.field public static final SeekBar_thumbOffset:I = 0x1

.field public static final SelectionModeDrawables:[I

.field public static final SelectionModeDrawables_actionModeCopyDrawable:I = 0x1

.field public static final SelectionModeDrawables_actionModeCutDrawable:I = 0x0

.field public static final SelectionModeDrawables_actionModePasteDrawable:I = 0x2

.field public static final SelectionModeDrawables_actionModeSelectAllDrawable:I = 0x3

.field public static final ShapeDrawable:[I

.field public static final ShapeDrawablePadding:[I

.field public static final ShapeDrawablePadding_bottom:I = 0x3

.field public static final ShapeDrawablePadding_left:I = 0x0

.field public static final ShapeDrawablePadding_right:I = 0x2

.field public static final ShapeDrawablePadding_top:I = 0x1

.field public static final ShapeDrawable_color:I = 0x3

.field public static final ShapeDrawable_dither:I = 0x0

.field public static final ShapeDrawable_height:I = 0x1

.field public static final ShapeDrawable_width:I = 0x2

.field public static final SizeAdaptiveLayout:[I

.field public static final SizeAdaptiveLayout_Layout:[I

.field public static final SizeAdaptiveLayout_Layout_layout_maxHeight:I = 0x0

.field public static final SizeAdaptiveLayout_Layout_layout_minHeight:I = 0x1

.field public static final SlidingChallengeLayout_Layout:[I

.field public static final SlidingChallengeLayout_Layout_layout_childType:I = 0x1

.field public static final SlidingChallengeLayout_Layout_layout_maxHeight:I = 0x0

.field public static final SlidingDrawer:[I

.field public static final SlidingDrawer_allowSingleTap:I = 0x3

.field public static final SlidingDrawer_animateOnClick:I = 0x6

.field public static final SlidingDrawer_bottomOffset:I = 0x1

.field public static final SlidingDrawer_content:I = 0x5

.field public static final SlidingDrawer_handle:I = 0x4

.field public static final SlidingDrawer_orientation:I = 0x0

.field public static final SlidingDrawer_topOffset:I = 0x2

.field public static final SlidingTab:[I

.field public static final SlidingTab_orientation:I = 0x0

.field public static final SpellChecker:[I

.field public static final SpellChecker_Subtype:[I

.field public static final SpellChecker_Subtype_label:I = 0x0

.field public static final SpellChecker_Subtype_subtypeExtraValue:I = 0x2

.field public static final SpellChecker_Subtype_subtypeLocale:I = 0x1

.field public static final SpellChecker_label:I = 0x0

.field public static final SpellChecker_settingsActivity:I = 0x1

.field public static final Spinner:[I

.field public static final Spinner_disableChildrenWhenDisabled:I = 0x9

.field public static final Spinner_dropDownHorizontalOffset:I = 0x5

.field public static final Spinner_dropDownSelector:I = 0x1

.field public static final Spinner_dropDownVerticalOffset:I = 0x6

.field public static final Spinner_dropDownWidth:I = 0x4

.field public static final Spinner_gravity:I = 0x0

.field public static final Spinner_popupBackground:I = 0x2

.field public static final Spinner_popupPromptView:I = 0x8

.field public static final Spinner_prompt:I = 0x3

.field public static final Spinner_spinnerMode:I = 0x7

.field public static final StackView:[I

.field public static final StackView_clickColor:I = 0x1

.field public static final StackView_resOutColor:I = 0x0

.field public static final StateListDrawable:[I

.field public static final StateListDrawable_constantSize:I = 0x3

.field public static final StateListDrawable_dither:I = 0x0

.field public static final StateListDrawable_enterFadeDuration:I = 0x4

.field public static final StateListDrawable_exitFadeDuration:I = 0x5

.field public static final StateListDrawable_variablePadding:I = 0x2

.field public static final StateListDrawable_visible:I = 0x1

.field public static final Storage:[I

.field public static final Storage_allowMassStorage:I = 0x6

.field public static final Storage_emulated:I = 0x4

.field public static final Storage_maxFileSize:I = 0x7

.field public static final Storage_mountPoint:I = 0x0

.field public static final Storage_mtpReserve:I = 0x5

.field public static final Storage_primary:I = 0x2

.field public static final Storage_removable:I = 0x3

.field public static final Storage_storageDescription:I = 0x1

.field public static final SuggestionSpan:[I

.field public static final SuggestionSpan_textUnderlineColor:I = 0x0

.field public static final SuggestionSpan_textUnderlineThickness:I = 0x1

.field public static final Switch:[I

.field public static final SwitchPreference:[I

.field public static final SwitchPreference_disableDependentsState:I = 0x2

.field public static final SwitchPreference_summaryOff:I = 0x1

.field public static final SwitchPreference_summaryOn:I = 0x0

.field public static final SwitchPreference_switchTextOff:I = 0x4

.field public static final SwitchPreference_switchTextOn:I = 0x3

.field public static final Switch_switchMinWidth:I = 0x5

.field public static final Switch_switchPadding:I = 0x6

.field public static final Switch_switchTextAppearance:I = 0x3

.field public static final Switch_textOff:I = 0x1

.field public static final Switch_textOn:I = 0x0

.field public static final Switch_thumb:I = 0x2

.field public static final Switch_thumbTextPadding:I = 0x7

.field public static final Switch_track:I = 0x4

.field public static final SyncAdapter:[I

.field public static final SyncAdapter_accountType:I = 0x1

.field public static final SyncAdapter_allowParallelSyncs:I = 0x5

.field public static final SyncAdapter_contentAuthority:I = 0x2

.field public static final SyncAdapter_isAlwaysSyncable:I = 0x6

.field public static final SyncAdapter_settingsActivity:I = 0x0

.field public static final SyncAdapter_supportsUploading:I = 0x4

.field public static final SyncAdapter_userVisible:I = 0x3

.field public static final TabWidget:[I

.field public static final TabWidget_divider:I = 0x0

.field public static final TabWidget_tabLayout:I = 0x4

.field public static final TabWidget_tabStripEnabled:I = 0x3

.field public static final TabWidget_tabStripLeft:I = 0x1

.field public static final TabWidget_tabStripRight:I = 0x2

.field public static final TableLayout:[I

.field public static final TableLayout_collapseColumns:I = 0x2

.field public static final TableLayout_shrinkColumns:I = 0x1

.field public static final TableLayout_stretchColumns:I = 0x0

.field public static final TableRow:[I

.field public static final TableRow_Cell:[I

.field public static final TableRow_Cell_layout_column:I = 0x0

.field public static final TableRow_Cell_layout_span:I = 0x1

.field public static final TextAppearance:[I

.field public static final TextAppearance_fontFamily:I = 0x8

.field public static final TextAppearance_textAllCaps:I = 0x7

.field public static final TextAppearance_textColor:I = 0x3

.field public static final TextAppearance_textColorHighlight:I = 0x4

.field public static final TextAppearance_textColorHint:I = 0x5

.field public static final TextAppearance_textColorLink:I = 0x6

.field public static final TextAppearance_textSize:I = 0x0

.field public static final TextAppearance_textStyle:I = 0x2

.field public static final TextAppearance_typeface:I = 0x1

.field public static final TextClock:[I

.field public static final TextClock_format12Hour:I = 0x0

.field public static final TextClock_format24Hour:I = 0x1

.field public static final TextClock_timeZone:I = 0x2

.field public static final TextSwitcher:[I

.field public static final TextToSpeechEngine:[I

.field public static final TextToSpeechEngine_settingsActivity:I = 0x0

.field public static final TextView:[I

.field public static final TextViewAppearance:[I

.field public static final TextViewAppearance_textAppearance:I = 0x0

.field public static final TextViewMultiLineBackgroundState:[I

.field public static final TextViewMultiLineBackgroundState_state_multiline:I = 0x0

.field public static final TextView_autoLink:I = 0xb

.field public static final TextView_autoText:I = 0x2d
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_bufferType:I = 0x11

.field public static final TextView_capitalize:I = 0x2c
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_cursorVisible:I = 0x15

.field public static final TextView_digits:I = 0x29

.field public static final TextView_drawableBottom:I = 0x31

.field public static final TextView_drawableEnd:I = 0x4a

.field public static final TextView_drawableLeft:I = 0x32

.field public static final TextView_drawablePadding:I = 0x34

.field public static final TextView_drawableRight:I = 0x33

.field public static final TextView_drawableStart:I = 0x49

.field public static final TextView_drawableTop:I = 0x30

.field public static final TextView_editable:I = 0x2e
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_editorExtras:I = 0x3a

.field public static final TextView_ellipsize:I = 0x9

.field public static final TextView_ems:I = 0x1b

.field public static final TextView_enabled:I = 0x0

.field public static final TextView_fontFamily:I = 0x4b

.field public static final TextView_freezesText:I = 0x2f

.field public static final TextView_gravity:I = 0xa

.field public static final TextView_height:I = 0x18

.field public static final TextView_hint:I = 0x13

.field public static final TextView_imeActionId:I = 0x3d

.field public static final TextView_imeActionLabel:I = 0x3c

.field public static final TextView_imeOptions:I = 0x3b

.field public static final TextView_includeFontPadding:I = 0x22

.field public static final TextView_inputMethod:I = 0x2b
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_inputType:I = 0x38

.field public static final TextView_lineSpacingExtra:I = 0x35

.field public static final TextView_lineSpacingMultiplier:I = 0x36

.field public static final TextView_lines:I = 0x17

.field public static final TextView_linksClickable:I = 0xc

.field public static final TextView_marqueeRepeatLimit:I = 0x37

.field public static final TextView_maxEms:I = 0x1a

.field public static final TextView_maxHeight:I = 0xe

.field public static final TextView_maxLength:I = 0x23

.field public static final TextView_maxLines:I = 0x16

.field public static final TextView_maxWidth:I = 0xd

.field public static final TextView_minEms:I = 0x1d

.field public static final TextView_minHeight:I = 0x10

.field public static final TextView_minLines:I = 0x19

.field public static final TextView_minWidth:I = 0xf

.field public static final TextView_numeric:I = 0x28
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_password:I = 0x1f
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_phoneNumber:I = 0x2a
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_privateImeOptions:I = 0x39

.field public static final TextView_scrollHorizontally:I = 0x1e

.field public static final TextView_selectAllOnFocus:I = 0x21

.field public static final TextView_shadowColor:I = 0x24

.field public static final TextView_shadowDx:I = 0x25

.field public static final TextView_shadowDy:I = 0x26

.field public static final TextView_shadowRadius:I = 0x27

.field public static final TextView_singleLine:I = 0x20
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final TextView_text:I = 0x12

.field public static final TextView_textAllCaps:I = 0x48

.field public static final TextView_textAppearance:I = 0x1

.field public static final TextView_textColor:I = 0x5

.field public static final TextView_textColorHighlight:I = 0x6

.field public static final TextView_textColorHint:I = 0x7

.field public static final TextView_textColorLink:I = 0x8

.field public static final TextView_textCursorDrawable:I = 0x46

.field public static final TextView_textEditNoPasteWindowLayout:I = 0x42

.field public static final TextView_textEditPasteWindowLayout:I = 0x41

.field public static final TextView_textEditSideNoPasteWindowLayout:I = 0x45

.field public static final TextView_textEditSidePasteWindowLayout:I = 0x44

.field public static final TextView_textEditSuggestionItemLayout:I = 0x47

.field public static final TextView_textIsSelectable:I = 0x43

.field public static final TextView_textScaleX:I = 0x14

.field public static final TextView_textSelectHandle:I = 0x40

.field public static final TextView_textSelectHandleLeft:I = 0x3e

.field public static final TextView_textSelectHandleRight:I = 0x3f

.field public static final TextView_textSize:I = 0x2

.field public static final TextView_textStyle:I = 0x4

.field public static final TextView_typeface:I = 0x3

.field public static final TextView_width:I = 0x1c

.field public static final Theme:[I

.field public static final Theme_absListViewStyle:I = 0x33

.field public static final Theme_accessibilityFocusedDrawable:I = 0x105

.field public static final Theme_actionBarDivider:I = 0xcc

.field public static final Theme_actionBarItemBackground:I = 0xcd

.field public static final Theme_actionBarSize:I = 0x8c

.field public static final Theme_actionBarSplitStyle:I = 0xc4

.field public static final Theme_actionBarStyle:I = 0x84

.field public static final Theme_actionBarTabBarStyle:I = 0x8f

.field public static final Theme_actionBarTabStyle:I = 0x8e

.field public static final Theme_actionBarTabTextStyle:I = 0x90

.field public static final Theme_actionBarWidgetTheme:I = 0xcb

.field public static final Theme_actionButtonStyle:I = 0x87

.field public static final Theme_actionDropDownStyle:I = 0x86

.field public static final Theme_actionMenuTextAppearance:I = 0xbc

.field public static final Theme_actionMenuTextColor:I = 0xbd

.field public static final Theme_actionModeBackground:I = 0x88

.field public static final Theme_actionModeCloseButtonStyle:I = 0x92

.field public static final Theme_actionModeCloseDrawable:I = 0x89

.field public static final Theme_actionModeCopyDrawable:I = 0xa0

.field public static final Theme_actionModeCutDrawable:I = 0x9f

.field public static final Theme_actionModeFindDrawable:I = 0xef

.field public static final Theme_actionModePasteDrawable:I = 0xa1

.field public static final Theme_actionModePopupWindowStyle:I = 0xf1

.field public static final Theme_actionModeSelectAllDrawable:I = 0xc1

.field public static final Theme_actionModeShareDrawable:I = 0xee

.field public static final Theme_actionModeSplitBackground:I = 0xce

.field public static final Theme_actionModeStyle:I = 0xca

.field public static final Theme_actionModeWebSearchDrawable:I = 0xf0

.field public static final Theme_actionOverflowButtonStyle:I = 0x91

.field public static final Theme_activatedBackgroundIndicator:I = 0x93

.field public static final Theme_activityChooserViewStyle:I = 0xed

.field public static final Theme_alertDialogButtonGroupStyle:I = 0xe3

.field public static final Theme_alertDialogCenterButtons:I = 0xe4

.field public static final Theme_alertDialogIcon:I = 0xb5

.field public static final Theme_alertDialogStyle:I = 0x2d

.field public static final Theme_alertDialogTheme:I = 0x9b

.field public static final Theme_autoCompleteTextViewStyle:I = 0x34

.field public static final Theme_backgroundDimAmount:I = 0x2

.field public static final Theme_backgroundDimEnabled:I = 0x6a

.field public static final Theme_borderlessButtonStyle:I = 0xa5

.field public static final Theme_buttonBarButtonStyle:I = 0xa8

.field public static final Theme_buttonBarStyle:I = 0xa7

.field public static final Theme_buttonStyle:I = 0x18

.field public static final Theme_buttonStyleInset:I = 0x1a

.field public static final Theme_buttonStyleSmall:I = 0x19

.field public static final Theme_buttonStyleToggle:I = 0x1b

.field public static final Theme_calendarViewStyle:I = 0xb9

.field public static final Theme_candidatesTextStyleSpans:I = 0x6d

.field public static final Theme_checkBoxPreferenceStyle:I = 0x57

.field public static final Theme_checkboxStyle:I = 0x35

.field public static final Theme_checkedTextViewStyle:I = 0xd9

.field public static final Theme_colorActivatedHighlight:I = 0xc8

.field public static final Theme_colorBackground:I = 0x1

.field public static final Theme_colorBackgroundCacheHint:I = 0x76

.field public static final Theme_colorFocusedHighlight:I = 0xc7

.field public static final Theme_colorForeground:I = 0x0

.field public static final Theme_colorForegroundInverse:I = 0x5e

.field public static final Theme_colorLongPressedHighlight:I = 0xc6

.field public static final Theme_colorMultiSelectHighlight:I = 0xc9

.field public static final Theme_colorPressedHighlight:I = 0xc5

.field public static final Theme_datePickerStyle:I = 0xb8

.field public static final Theme_detailsElementBackground:I = 0xaf

.field public static final Theme_dialogCustomTitleDecorLayout:I = 0xf5

.field public static final Theme_dialogPreferenceStyle:I = 0x59

.field public static final Theme_dialogTheme:I = 0x9a

.field public static final Theme_dialogTitleDecorLayout:I = 0xf6

.field public static final Theme_dialogTitleIconsDecorLayout:I = 0xf4

.field public static final Theme_disabledAlpha:I = 0x3

.field public static final Theme_dividerHorizontal:I = 0xa6

.field public static final Theme_dividerVertical:I = 0x9c

.field public static final Theme_dropDownHintAppearance:I = 0x50

.field public static final Theme_dropDownItemStyle:I = 0x4e

.field public static final Theme_dropDownListViewStyle:I = 0x36

.field public static final Theme_dropDownSpinnerStyle:I = 0x85

.field public static final Theme_dropdownListPreferredItemHeight:I = 0xe1

.field public static final Theme_editTextBackground:I = 0xb3

.field public static final Theme_editTextColor:I = 0xb2

.field public static final Theme_editTextPreferenceStyle:I = 0x5a

.field public static final Theme_editTextStyle:I = 0x37

.field public static final Theme_errorMessageAboveBackground:I = 0xdf

.field public static final Theme_errorMessageBackground:I = 0xde

.field public static final Theme_expandableListPreferredChildIndicatorLeft:I = 0x22

.field public static final Theme_expandableListPreferredChildIndicatorRight:I = 0x23

.field public static final Theme_expandableListPreferredChildPaddingLeft:I = 0x1f

.field public static final Theme_expandableListPreferredItemIndicatorLeft:I = 0x20

.field public static final Theme_expandableListPreferredItemIndicatorRight:I = 0x21

.field public static final Theme_expandableListPreferredItemPaddingLeft:I = 0x1e

.field public static final Theme_expandableListViewStyle:I = 0x38

.field public static final Theme_expandableListViewWhiteStyle:I = 0x7d

.field public static final Theme_fastScrollOverlayPosition:I = 0xae

.field public static final Theme_fastScrollPreviewBackgroundLeft:I = 0xab

.field public static final Theme_fastScrollPreviewBackgroundRight:I = 0xac

.field public static final Theme_fastScrollTextColor:I = 0xb6

.field public static final Theme_fastScrollThumbDrawable:I = 0xaa

.field public static final Theme_fastScrollTrackDrawable:I = 0xad

.field public static final Theme_findOnPageNextDrawable:I = 0x106

.field public static final Theme_findOnPagePreviousDrawable:I = 0x107

.field public static final Theme_galleryItemBackground:I = 0x1c

.field public static final Theme_galleryStyle:I = 0x39

.field public static final Theme_gestureOverlayViewStyle:I = 0xe8

.field public static final Theme_gridViewStyle:I = 0x3a

.field public static final Theme_homeAsUpIndicator:I = 0x9d

.field public static final Theme_horizontalScrollViewStyle:I = 0xb4

.field public static final Theme_imageButtonStyle:I = 0x3b

.field public static final Theme_imageWellStyle:I = 0x3c

.field public static final Theme_listChoiceBackgroundIndicator:I = 0x8d

.field public static final Theme_listChoiceIndicatorMultiple:I = 0x68

.field public static final Theme_listChoiceIndicatorSingle:I = 0x67

.field public static final Theme_listDivider:I = 0x66

.field public static final Theme_listDividerAlertDialog:I = 0x98

.field public static final Theme_listPopupWindowStyle:I = 0x94

.field public static final Theme_listPreferredItemHeight:I = 0x1d

.field public static final Theme_listPreferredItemHeightLarge:I = 0xc2

.field public static final Theme_listPreferredItemHeightSmall:I = 0xc3

.field public static final Theme_listPreferredItemPaddingEnd:I = 0xd6

.field public static final Theme_listPreferredItemPaddingLeft:I = 0xd1

.field public static final Theme_listPreferredItemPaddingRight:I = 0xd2

.field public static final Theme_listPreferredItemPaddingStart:I = 0xd5

.field public static final Theme_listSeparatorTextViewStyle:I = 0x60

.field public static final Theme_listViewStyle:I = 0x3d

.field public static final Theme_listViewWhiteStyle:I = 0x3e

.field public static final Theme_mapViewStyle:I = 0x52

.field public static final Theme_mediaRouteButtonStyle:I = 0xd4

.field public static final Theme_numberPickerStyle:I = 0xeb

.field public static final Theme_panelBackground:I = 0x2e

.field public static final Theme_panelColorBackground:I = 0x31

.field public static final Theme_panelColorForeground:I = 0x30

.field public static final Theme_panelFullBackground:I = 0x2f

.field public static final Theme_panelMenuIsCompact:I = 0xe5

.field public static final Theme_panelMenuListTheme:I = 0xe7

.field public static final Theme_panelMenuListWidth:I = 0xe6

.field public static final Theme_panelTextAppearance:I = 0x32

.field public static final Theme_pointerStyle:I = 0x104

.field public static final Theme_popupMenuStyle:I = 0x95

.field public static final Theme_popupWindowStyle:I = 0x3f

.field public static final Theme_preferenceCategoryStyle:I = 0x54

.field public static final Theme_preferenceFragmentStyle:I = 0xf2

.field public static final Theme_preferenceFrameLayoutStyle:I = 0x102

.field public static final Theme_preferenceInformationStyle:I = 0x55

.field public static final Theme_preferenceLayoutChild:I = 0x5c

.field public static final Theme_preferencePanelStyle:I = 0xf3

.field public static final Theme_preferenceScreenStyle:I = 0x53

.field public static final Theme_preferenceStyle:I = 0x56

.field public static final Theme_presentationTheme:I = 0xd7

.field public static final Theme_progressBarStyle:I = 0x40

.field public static final Theme_progressBarStyleHorizontal:I = 0x41

.field public static final Theme_progressBarStyleInverse:I = 0x6f

.field public static final Theme_progressBarStyleLarge:I = 0x43

.field public static final Theme_progressBarStyleLargeInverse:I = 0x71

.field public static final Theme_progressBarStyleSmall:I = 0x42

.field public static final Theme_progressBarStyleSmallInverse:I = 0x70

.field public static final Theme_progressBarStyleSmallTitle:I = 0x62

.field public static final Theme_quickContactBadgeOverlay:I = 0xe9

.field public static final Theme_quickContactBadgeStyleSmallWindowLarge:I = 0x7c

.field public static final Theme_quickContactBadgeStyleSmallWindowMedium:I = 0x7b

.field public static final Theme_quickContactBadgeStyleSmallWindowSmall:I = 0x7a

.field public static final Theme_quickContactBadgeStyleWindowLarge:I = 0x79

.field public static final Theme_quickContactBadgeStyleWindowMedium:I = 0x78

.field public static final Theme_quickContactBadgeStyleWindowSmall:I = 0x77

.field public static final Theme_radioButtonStyle:I = 0x47

.field public static final Theme_ratingBarStyle:I = 0x45

.field public static final Theme_ratingBarStyleIndicator:I = 0x63

.field public static final Theme_ratingBarStyleSmall:I = 0x46

.field public static final Theme_ringtonePreferenceStyle:I = 0x5b

.field public static final Theme_scrollViewStyle:I = 0x48

.field public static final Theme_searchDialogTheme:I = 0x101

.field public static final Theme_searchDropdownBackground:I = 0xf8

.field public static final Theme_searchResultListItemHeight:I = 0xe0

.field public static final Theme_searchViewCloseIcon:I = 0xf9

.field public static final Theme_searchViewEditQuery:I = 0xfd

.field public static final Theme_searchViewEditQueryBackground:I = 0xfe

.field public static final Theme_searchViewGoIcon:I = 0xfa

.field public static final Theme_searchViewSearchIcon:I = 0xfb

.field public static final Theme_searchViewTextField:I = 0xff

.field public static final Theme_searchViewTextFieldRight:I = 0x100

.field public static final Theme_searchViewVoiceIcon:I = 0xfc

.field public static final Theme_searchWidgetCorpusItemBackground:I = 0xd3

.field public static final Theme_seekBarStyle:I = 0x44

.field public static final Theme_segmentedButtonStyle:I = 0xa9

.field public static final Theme_selectableItemBackground:I = 0x9e

.field public static final Theme_spinnerDropDownItemStyle:I = 0x4f

.field public static final Theme_spinnerItemStyle:I = 0x51

.field public static final Theme_spinnerStyle:I = 0x49

.field public static final Theme_stackViewStyle:I = 0xea

.field public static final Theme_starStyle:I = 0x4a

.field public static final Theme_switchPreferenceStyle:I = 0xbe

.field public static final Theme_switchStyle:I = 0x103

.field public static final Theme_tabWidgetStyle:I = 0x4b

.field public static final Theme_textAppearance:I = 0x4

.field public static final Theme_textAppearanceAutoCorrectionSuggestion:I = 0xdb

.field public static final Theme_textAppearanceButton:I = 0x5f

.field public static final Theme_textAppearanceEasyCorrectSuggestion:I = 0xd8

.field public static final Theme_textAppearanceInverse:I = 0x5

.field public static final Theme_textAppearanceLarge:I = 0x10

.field public static final Theme_textAppearanceLargeInverse:I = 0x13

.field public static final Theme_textAppearanceLargePopupMenu:I = 0x96

.field public static final Theme_textAppearanceListItem:I = 0xcf

.field public static final Theme_textAppearanceListItemSmall:I = 0xd0

.field public static final Theme_textAppearanceMedium:I = 0x11

.field public static final Theme_textAppearanceMediumInverse:I = 0x14

.field public static final Theme_textAppearanceMisspelledSuggestion:I = 0xda

.field public static final Theme_textAppearanceSearchResultSubtitle:I = 0x74

.field public static final Theme_textAppearanceSearchResultTitle:I = 0x75

.field public static final Theme_textAppearanceSmall:I = 0x12

.field public static final Theme_textAppearanceSmallInverse:I = 0x15

.field public static final Theme_textAppearanceSmallPopupMenu:I = 0x97

.field public static final Theme_textCheckMark:I = 0x16

.field public static final Theme_textCheckMarkInverse:I = 0x17

.field public static final Theme_textColorAlertDialogListItem:I = 0x99

.field public static final Theme_textColorHighlightInverse:I = 0xb0

.field public static final Theme_textColorHintInverse:I = 0xf

.field public static final Theme_textColorLinkInverse:I = 0xb1

.field public static final Theme_textColorPrimary:I = 0x6

.field public static final Theme_textColorPrimaryDisableOnly:I = 0x7

.field public static final Theme_textColorPrimaryInverse:I = 0x9

.field public static final Theme_textColorPrimaryInverseDisableOnly:I = 0x72

.field public static final Theme_textColorPrimaryInverseNoDisable:I = 0xd

.field public static final Theme_textColorPrimaryNoDisable:I = 0xb

.field public static final Theme_textColorSearchUrl:I = 0x6e

.field public static final Theme_textColorSecondary:I = 0x8

.field public static final Theme_textColorSecondaryInverse:I = 0xa

.field public static final Theme_textColorSecondaryInverseNoDisable:I = 0xe

.field public static final Theme_textColorSecondaryNoDisable:I = 0xc

.field public static final Theme_textColorTertiary:I = 0x64

.field public static final Theme_textColorTertiaryInverse:I = 0x65

.field public static final Theme_textEditNoPasteWindowLayout:I = 0xa3

.field public static final Theme_textEditPasteWindowLayout:I = 0xa2

.field public static final Theme_textEditSideNoPasteWindowLayout:I = 0xbb

.field public static final Theme_textEditSidePasteWindowLayout:I = 0xba

.field public static final Theme_textEditSuggestionItemLayout:I = 0xc0

.field public static final Theme_textSelectHandle:I = 0x81

.field public static final Theme_textSelectHandleLeft:I = 0x7f

.field public static final Theme_textSelectHandleRight:I = 0x80

.field public static final Theme_textSelectHandleWindowStyle:I = 0x82

.field public static final Theme_textSuggestionsWindowStyle:I = 0xbf

.field public static final Theme_textUnderlineColor:I = 0xdc

.field public static final Theme_textUnderlineThickness:I = 0xdd

.field public static final Theme_textViewStyle:I = 0x4c

.field public static final Theme_timePickerStyle:I = 0xec

.field public static final Theme_toastFrameBackground:I = 0xf7

.field public static final Theme_webTextViewStyle:I = 0x7e

.field public static final Theme_webViewStyle:I = 0x4d

.field public static final Theme_windowActionBar:I = 0x83

.field public static final Theme_windowActionBarOverlay:I = 0x8b

.field public static final Theme_windowActionModeOverlay:I = 0x8a

.field public static final Theme_windowAnimationStyle:I = 0x5d

.field public static final Theme_windowBackground:I = 0x24

.field public static final Theme_windowCloseOnTouchOutside:I = 0xb7

.field public static final Theme_windowContentOverlay:I = 0x29

.field public static final Theme_windowDisablePreview:I = 0x6b

.field public static final Theme_windowEnableSplitTouch:I = 0xa4

.field public static final Theme_windowFrame:I = 0x25

.field public static final Theme_windowFullscreen:I = 0x61

.field public static final Theme_windowIsFloating:I = 0x27

.field public static final Theme_windowIsTranslucent:I = 0x28

.field public static final Theme_windowNoDisplay:I = 0x69

.field public static final Theme_windowNoTitle:I = 0x26

.field public static final Theme_windowShowWallpaper:I = 0x73

.field public static final Theme_windowSoftInputMode:I = 0x6c

.field public static final Theme_windowSplitActionBar:I = 0xe2

.field public static final Theme_windowTitleBackgroundStyle:I = 0x2c

.field public static final Theme_windowTitleSize:I = 0x2a

.field public static final Theme_windowTitleStyle:I = 0x2b

.field public static final Theme_yesNoPreferenceStyle:I = 0x58

.field public static final TimePicker:[I

.field public static final TimePicker_internalLayout:I = 0x0

.field public static final ToggleButton:[I

.field public static final ToggleButton_disabledAlpha:I = 0x0

.field public static final ToggleButton_textOff:I = 0x2

.field public static final ToggleButton_textOn:I = 0x1

.field public static final TranslateAnimation:[I

.field public static final TranslateAnimation_fromXDelta:I = 0x0

.field public static final TranslateAnimation_fromYDelta:I = 0x2

.field public static final TranslateAnimation_toXDelta:I = 0x1

.field public static final TranslateAnimation_toYDelta:I = 0x3

.field public static final TwoLineListItem:[I

.field public static final TwoLineListItem_mode:I = 0x0

.field public static final VerticalSlider_Layout:[I

.field public static final VerticalSlider_Layout_layout_scale:I = 0x0

.field public static final View:[I

.field public static final ViewAnimator:[I

.field public static final ViewAnimator_animateFirstView:I = 0x2

.field public static final ViewAnimator_inAnimation:I = 0x0

.field public static final ViewAnimator_outAnimation:I = 0x1

.field public static final ViewDrawableStates:[I

.field public static final ViewDrawableStates_state_accelerated:I = 0x6

.field public static final ViewDrawableStates_state_activated:I = 0x5

.field public static final ViewDrawableStates_state_drag_can_accept:I = 0x8

.field public static final ViewDrawableStates_state_drag_hovered:I = 0x9

.field public static final ViewDrawableStates_state_enabled:I = 0x2

.field public static final ViewDrawableStates_state_focused:I = 0x0

.field public static final ViewDrawableStates_state_hovered:I = 0x7

.field public static final ViewDrawableStates_state_pressed:I = 0x4

.field public static final ViewDrawableStates_state_selected:I = 0x3

.field public static final ViewDrawableStates_state_window_focused:I = 0x1

.field public static final ViewFlipper:[I

.field public static final ViewFlipper_autoStart:I = 0x1

.field public static final ViewFlipper_flipInterval:I = 0x0

.field public static final ViewGroup:[I

.field public static final ViewGroup_Layout:[I

.field public static final ViewGroup_Layout_layout_height:I = 0x1

.field public static final ViewGroup_Layout_layout_width:I = 0x0

.field public static final ViewGroup_MarginLayout:[I

.field public static final ViewGroup_MarginLayout_layout_height:I = 0x1

.field public static final ViewGroup_MarginLayout_layout_margin:I = 0x2

.field public static final ViewGroup_MarginLayout_layout_marginBottom:I = 0x6

.field public static final ViewGroup_MarginLayout_layout_marginEnd:I = 0x8

.field public static final ViewGroup_MarginLayout_layout_marginLeft:I = 0x3

.field public static final ViewGroup_MarginLayout_layout_marginRight:I = 0x5

.field public static final ViewGroup_MarginLayout_layout_marginStart:I = 0x7

.field public static final ViewGroup_MarginLayout_layout_marginTop:I = 0x4

.field public static final ViewGroup_MarginLayout_layout_width:I = 0x0

.field public static final ViewGroup_addStatesFromChildren:I = 0x6

.field public static final ViewGroup_alwaysDrawnWithCache:I = 0x5

.field public static final ViewGroup_animateLayoutChanges:I = 0x9

.field public static final ViewGroup_animationCache:I = 0x3

.field public static final ViewGroup_clipChildren:I = 0x0

.field public static final ViewGroup_clipToPadding:I = 0x1

.field public static final ViewGroup_descendantFocusability:I = 0x7

.field public static final ViewGroup_layoutAnimation:I = 0x2

.field public static final ViewGroup_persistentDrawingCache:I = 0x4

.field public static final ViewGroup_splitMotionEvents:I = 0x8

.field public static final ViewStub:[I

.field public static final ViewStub_inflatedId:I = 0x1

.field public static final ViewStub_layout:I = 0x0

.field public static final ViewSwitcher:[I

.field public static final View_alpha:I = 0x2f

.field public static final View_background:I = 0xc

.field public static final View_clickable:I = 0x1d

.field public static final View_contentDescription:I = 0x29

.field public static final View_drawingCacheQuality:I = 0x20

.field public static final View_duplicateParentState:I = 0x21

.field public static final View_fadeScrollbars:I = 0x2c

.field public static final View_fadingEdge:I = 0x17

.field public static final View_fadingEdgeLength:I = 0x18

.field public static final View_filterTouchesWhenObscured:I = 0x2e

.field public static final View_fitsSystemWindows:I = 0x15

.field public static final View_focusable:I = 0x12

.field public static final View_focusableInTouchMode:I = 0x13

.field public static final View_hapticFeedbackEnabled:I = 0x27

.field public static final View_id:I = 0x8

.field public static final View_importantForAccessibility:I = 0x3d

.field public static final View_isScrollContainer:I = 0x26

.field public static final View_keepScreenOn:I = 0x25

.field public static final View_labelFor:I = 0x43

.field public static final View_layerType:I = 0x3b

.field public static final View_layoutDirection:I = 0x40

.field public static final View_longClickable:I = 0x1e

.field public static final View_minHeight:I = 0x23

.field public static final View_minWidth:I = 0x22

.field public static final View_nextFocusDown:I = 0x1c

.field public static final View_nextFocusForward:I = 0x3a

.field public static final View_nextFocusLeft:I = 0x19

.field public static final View_nextFocusRight:I = 0x1a

.field public static final View_nextFocusUp:I = 0x1b

.field public static final View_onClick:I = 0x28

.field public static final View_overScrollMode:I = 0x2d

.field public static final View_padding:I = 0xd

.field public static final View_paddingBottom:I = 0x11

.field public static final View_paddingEnd:I = 0x42

.field public static final View_paddingLeft:I = 0xe

.field public static final View_paddingRight:I = 0x10

.field public static final View_paddingStart:I = 0x41

.field public static final View_paddingTop:I = 0xf

.field public static final View_requiresFadingEdge:I = 0x3c

.field public static final View_rotation:I = 0x36

.field public static final View_rotationX:I = 0x37

.field public static final View_rotationY:I = 0x38

.field public static final View_saveEnabled:I = 0x1f

.field public static final View_scaleX:I = 0x34

.field public static final View_scaleY:I = 0x35

.field public static final View_scrollX:I = 0xa

.field public static final View_scrollY:I = 0xb

.field public static final View_scrollbarAlwaysDrawHorizontalTrack:I = 0x5

.field public static final View_scrollbarAlwaysDrawVerticalTrack:I = 0x6

.field public static final View_scrollbarDefaultDelayBeforeFade:I = 0x2b

.field public static final View_scrollbarFadeDuration:I = 0x2a

.field public static final View_scrollbarSize:I = 0x0

.field public static final View_scrollbarStyle:I = 0x7

.field public static final View_scrollbarThumbHorizontal:I = 0x1

.field public static final View_scrollbarThumbVertical:I = 0x2

.field public static final View_scrollbarTrackHorizontal:I = 0x3

.field public static final View_scrollbarTrackVertical:I = 0x4

.field public static final View_scrollbars:I = 0x16

.field public static final View_soundEffectsEnabled:I = 0x24

.field public static final View_tag:I = 0x9

.field public static final View_textAlignment:I = 0x3f

.field public static final View_textDirection:I = 0x3e

.field public static final View_transformPivotX:I = 0x30

.field public static final View_transformPivotY:I = 0x31

.field public static final View_translationX:I = 0x32

.field public static final View_translationY:I = 0x33

.field public static final View_verticalScrollbarPosition:I = 0x39

.field public static final View_visibility:I = 0x14

.field public static final VolumePreference:[I

.field public static final VolumePreference_streamType:I = 0x0

.field public static final Wallpaper:[I

.field public static final WallpaperPreviewInfo:[I

.field public static final WallpaperPreviewInfo_staticWallpaperPreview:I = 0x0

.field public static final Wallpaper_author:I = 0x3

.field public static final Wallpaper_description:I = 0x0

.field public static final Wallpaper_settingsActivity:I = 0x1

.field public static final Wallpaper_thumbnail:I = 0x2

.field public static final WeightedLinearLayout:[I

.field public static final WeightedLinearLayout_majorWeightMax:I = 0x2

.field public static final WeightedLinearLayout_majorWeightMin:I = 0x0

.field public static final WeightedLinearLayout_minorWeightMax:I = 0x3

.field public static final WeightedLinearLayout_minorWeightMin:I = 0x1

.field public static final Window:[I

.field public static final WindowAnimation:[I

.field public static final WindowAnimation_activityCloseEnterAnimation:I = 0x6

.field public static final WindowAnimation_activityCloseExitAnimation:I = 0x7

.field public static final WindowAnimation_activityOpenEnterAnimation:I = 0x4

.field public static final WindowAnimation_activityOpenExitAnimation:I = 0x5

.field public static final WindowAnimation_taskCloseEnterAnimation:I = 0xa

.field public static final WindowAnimation_taskCloseExitAnimation:I = 0xb

.field public static final WindowAnimation_taskOpenEnterAnimation:I = 0x8

.field public static final WindowAnimation_taskOpenExitAnimation:I = 0x9

.field public static final WindowAnimation_taskToBackEnterAnimation:I = 0xe

.field public static final WindowAnimation_taskToBackExitAnimation:I = 0xf

.field public static final WindowAnimation_taskToFrontEnterAnimation:I = 0xc

.field public static final WindowAnimation_taskToFrontExitAnimation:I = 0xd

.field public static final WindowAnimation_wallpaperCloseEnterAnimation:I = 0x12

.field public static final WindowAnimation_wallpaperCloseExitAnimation:I = 0x13

.field public static final WindowAnimation_wallpaperIntraCloseEnterAnimation:I = 0x16

.field public static final WindowAnimation_wallpaperIntraCloseExitAnimation:I = 0x17

.field public static final WindowAnimation_wallpaperIntraOpenEnterAnimation:I = 0x14

.field public static final WindowAnimation_wallpaperIntraOpenExitAnimation:I = 0x15

.field public static final WindowAnimation_wallpaperOpenEnterAnimation:I = 0x10

.field public static final WindowAnimation_wallpaperOpenExitAnimation:I = 0x11

.field public static final WindowAnimation_windowEnterAnimation:I = 0x0

.field public static final WindowAnimation_windowExitAnimation:I = 0x1

.field public static final WindowAnimation_windowHideAnimation:I = 0x3

.field public static final WindowAnimation_windowShowAnimation:I = 0x2

.field public static final Window_backgroundDimAmount:I = 0x0

.field public static final Window_backgroundDimEnabled:I = 0xb

.field public static final Window_textColor:I = 0x7

.field public static final Window_windowActionBar:I = 0xf

.field public static final Window_windowActionBarOverlay:I = 0x11

.field public static final Window_windowActionModeOverlay:I = 0x10

.field public static final Window_windowAnimationStyle:I = 0x8

.field public static final Window_windowBackground:I = 0x1

.field public static final Window_windowCloseOnTouchOutside:I = 0x15

.field public static final Window_windowContentOverlay:I = 0x6

.field public static final Window_windowDisablePreview:I = 0xc

.field public static final Window_windowEnableSplitTouch:I = 0x12

.field public static final Window_windowFixedHeightMajor:I = 0x1a

.field public static final Window_windowFixedHeightMinor:I = 0x18

.field public static final Window_windowFixedWidthMajor:I = 0x17

.field public static final Window_windowFixedWidthMinor:I = 0x19

.field public static final Window_windowFrame:I = 0x2

.field public static final Window_windowFullscreen:I = 0x9

.field public static final Window_windowIsFloating:I = 0x4

.field public static final Window_windowIsTranslucent:I = 0x5

.field public static final Window_windowMinWidthMajor:I = 0x13

.field public static final Window_windowMinWidthMinor:I = 0x14

.field public static final Window_windowNoDisplay:I = 0xa

.field public static final Window_windowNoTitle:I = 0x3

.field public static final Window_windowShowWallpaper:I = 0xe

.field public static final Window_windowSoftInputMode:I = 0xd

.field public static final Window_windowSplitActionBar:I = 0x16


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/internal/R$styleable;->AbsListView:[I

    new-array v0, v3, [I

    const v1, 0x10100b2    # android.R.attr.entries

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AbsSpinner:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/android/internal/R$styleable;->AbsoluteLayout_Layout:[I

    new-array v0, v3, [I

    const v1, 0x10101d3    # android.R.attr.factor

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AccelerateInterpolator:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/android/internal/R$styleable;->AccessibilityService:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3

    sput-object v0, Lcom/android/internal/R$styleable;->AccountAuthenticator:[I

    const/16 v0, 0x13

    new-array v0, v0, [I

    fill-array-data v0, :array_4

    sput-object v0, Lcom/android/internal/R$styleable;->ActionBar:[I

    new-array v0, v3, [I

    const v1, 0x10100b3    # android.R.attr.layout_gravity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->ActionBar_LayoutParams:[I

    new-array v0, v3, [I

    const v1, 0x101013f    # android.R.attr.minWidth

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->ActionMenuItemView:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_5

    sput-object v0, Lcom/android/internal/R$styleable;->ActionMode:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_6

    sput-object v0, Lcom/android/internal/R$styleable;->ActivityChooserView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_7

    sput-object v0, Lcom/android/internal/R$styleable;->AdapterViewAnimator:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_8

    sput-object v0, Lcom/android/internal/R$styleable;->AdapterViewFlipper:[I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_9

    sput-object v0, Lcom/android/internal/R$styleable;->AlertDialog:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_a

    sput-object v0, Lcom/android/internal/R$styleable;->AlphaAnimation:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_b

    sput-object v0, Lcom/android/internal/R$styleable;->AnalogClock:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_c

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifest:[I

    new-array v0, v3, [I

    const v1, 0x1010003    # android.R.attr.name

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestAction:[I

    const/16 v0, 0x1f

    new-array v0, v0, [I

    fill-array-data v0, :array_d

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestActivity:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_e

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestActivityAlias:[I

    const/16 v0, 0x1d

    new-array v0, v0, [I

    fill-array-data v0, :array_f

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestApplication:[I

    new-array v0, v3, [I

    const v1, 0x1010003    # android.R.attr.name

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestCategory:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_10

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestCompatibleScreensScreen:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_11

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestData:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_12

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestGrantUriPermission:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_13

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestInputSource:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_14

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestInstrumentation:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_15

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestIntentFilter:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_16

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestMetaData:[I

    new-array v0, v3, [I

    const v1, 0x1010003    # android.R.attr.name

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestOriginalPackage:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_17

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestPackageVerifier:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_18

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestPathPermission:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_19

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestPermission:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_1a

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestPermissionGroup:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_1b

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestPermissionTree:[I

    new-array v0, v3, [I

    const v1, 0x1010003    # android.R.attr.name

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestProtectedBroadcast:[I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_1c

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestProvider:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_1d

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestReceiver:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_1e

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestService:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_1f

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestSupportsScreens:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_20

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestUsesConfiguration:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_21

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestUsesFeature:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_22

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestUsesLibrary:[I

    new-array v0, v3, [I

    const v1, 0x1010003    # android.R.attr.name

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestUsesPermission:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_23

    sput-object v0, Lcom/android/internal/R$styleable;->AndroidManifestUsesSdk:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_24

    sput-object v0, Lcom/android/internal/R$styleable;->AnimatedRotateDrawable:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_25

    sput-object v0, Lcom/android/internal/R$styleable;->Animation:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_26

    sput-object v0, Lcom/android/internal/R$styleable;->AnimationDrawable:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_27

    sput-object v0, Lcom/android/internal/R$styleable;->AnimationDrawableItem:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_28

    sput-object v0, Lcom/android/internal/R$styleable;->AnimationSet:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_29

    sput-object v0, Lcom/android/internal/R$styleable;->Animator:[I

    new-array v0, v3, [I

    const v1, 0x10102e2    # android.R.attr.ordering

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AnimatorSet:[I

    new-array v0, v3, [I

    const v1, 0x101026a    # android.R.attr.tension

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->AnticipateInterpolator:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_2a

    sput-object v0, Lcom/android/internal/R$styleable;->AnticipateOvershootInterpolator:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_2b

    sput-object v0, Lcom/android/internal/R$styleable;->AppWidgetProviderInfo:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_2c

    sput-object v0, Lcom/android/internal/R$styleable;->AutoCompleteTextView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_2d

    sput-object v0, Lcom/android/internal/R$styleable;->BitmapDrawable:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->Button:[I

    const/16 v0, 0xd

    new-array v0, v0, [I

    fill-array-data v0, :array_2e

    sput-object v0, Lcom/android/internal/R$styleable;->CalendarView:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_2f

    sput-object v0, Lcom/android/internal/R$styleable;->CheckBoxPreference:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_30

    sput-object v0, Lcom/android/internal/R$styleable;->CheckedTextView:[I

    new-array v0, v3, [I

    const v1, 0x1010105    # android.R.attr.format

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->Chronometer:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_31

    sput-object v0, Lcom/android/internal/R$styleable;->ClipDrawable:[I

    new-array v0, v3, [I

    const v1, 0x10101a5    # android.R.attr.color

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->ColorDrawable:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_32

    sput-object v0, Lcom/android/internal/R$styleable;->CompoundButton:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_33

    sput-object v0, Lcom/android/internal/R$styleable;->ContactsDataKind:[I

    new-array v0, v3, [I

    const v1, 0x10101d4    # android.R.attr.cycles

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->CycleInterpolator:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_34

    sput-object v0, Lcom/android/internal/R$styleable;->DatePicker:[I

    new-array v0, v3, [I

    const v1, 0x10101d3    # android.R.attr.factor

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->DecelerateInterpolator:[I

    new-array v0, v3, [I

    const v1, 0x1010194    # android.R.attr.visible

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->DeviceAdmin:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_35

    sput-object v0, Lcom/android/internal/R$styleable;->DialogPreference:[I

    new-array v0, v3, [I

    const v1, 0x1010194    # android.R.attr.visible

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->Drawable:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_36

    sput-object v0, Lcom/android/internal/R$styleable;->DrawableCorners:[I

    const/16 v0, 0x12

    new-array v0, v0, [I

    fill-array-data v0, :array_37

    sput-object v0, Lcom/android/internal/R$styleable;->DrawableStates:[I

    new-array v0, v3, [I

    const v1, 0x1010225    # android.R.attr.settingsActivity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->Dream:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->EditText:[I

    new-array v0, v3, [I

    const v1, 0x10100a6    # android.R.attr.state_last

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->ExpandableListChildIndicatorState:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_38

    sput-object v0, Lcom/android/internal/R$styleable;->ExpandableListGroupIndicatorState:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_39

    sput-object v0, Lcom/android/internal/R$styleable;->ExpandableListView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_3a

    sput-object v0, Lcom/android/internal/R$styleable;->Extra:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_3b

    sput-object v0, Lcom/android/internal/R$styleable;->Fragment:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_3c

    sput-object v0, Lcom/android/internal/R$styleable;->FragmentAnimation:[I

    new-array v0, v3, [I

    const v1, 0x10100af    # android.R.attr.gravity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->FragmentBreadCrumbs:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_3d

    sput-object v0, Lcom/android/internal/R$styleable;->FrameLayout:[I

    new-array v0, v3, [I

    const v1, 0x10100b3    # android.R.attr.layout_gravity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->FrameLayout_Layout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_3e

    sput-object v0, Lcom/android/internal/R$styleable;->Gallery:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_3f

    sput-object v0, Lcom/android/internal/R$styleable;->GestureOverlayView:[I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_40

    sput-object v0, Lcom/android/internal/R$styleable;->GlowPadView:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_41

    sput-object v0, Lcom/android/internal/R$styleable;->GradientDrawable:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_42

    sput-object v0, Lcom/android/internal/R$styleable;->GradientDrawableGradient:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_43

    sput-object v0, Lcom/android/internal/R$styleable;->GradientDrawablePadding:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_44

    sput-object v0, Lcom/android/internal/R$styleable;->GradientDrawableSize:[I

    new-array v0, v3, [I

    const v1, 0x10101a5    # android.R.attr.color

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->GradientDrawableSolid:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_45

    sput-object v0, Lcom/android/internal/R$styleable;->GradientDrawableStroke:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_46

    sput-object v0, Lcom/android/internal/R$styleable;->GridLayout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_47

    sput-object v0, Lcom/android/internal/R$styleable;->GridLayoutAnimation:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_48

    sput-object v0, Lcom/android/internal/R$styleable;->GridLayout_Layout:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_49

    sput-object v0, Lcom/android/internal/R$styleable;->GridView:[I

    new-array v0, v3, [I

    const v1, 0x101017a    # android.R.attr.fillViewport

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->HorizontalScrollView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4a

    sput-object v0, Lcom/android/internal/R$styleable;->Icon:[I

    new-array v0, v3, [I

    const v1, 0x1010002    # android.R.attr.icon

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->IconDefault:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_4b

    sput-object v0, Lcom/android/internal/R$styleable;->IconMenuView:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->ImageSwitcher:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_4c

    sput-object v0, Lcom/android/internal/R$styleable;->ImageView:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->InputExtras:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_4d

    sput-object v0, Lcom/android/internal/R$styleable;->InputMethod:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_4e

    sput-object v0, Lcom/android/internal/R$styleable;->InputMethodService:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_4f

    sput-object v0, Lcom/android/internal/R$styleable;->InputMethod_Subtype:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_50

    sput-object v0, Lcom/android/internal/R$styleable;->InsetDrawable:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_51

    sput-object v0, Lcom/android/internal/R$styleable;->Intent:[I

    new-array v0, v3, [I

    const v1, 0x1010003    # android.R.attr.name

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->IntentCategory:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_52

    sput-object v0, Lcom/android/internal/R$styleable;->Keyboard:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_53

    sput-object v0, Lcom/android/internal/R$styleable;->KeyboardLayout:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_54

    sput-object v0, Lcom/android/internal/R$styleable;->KeyboardView:[I

    new-array v0, v3, [I

    const v1, 0x101023c    # android.R.attr.state_long_pressable

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->KeyboardViewPreviewState:[I

    const/16 v0, 0xc

    new-array v0, v0, [I

    fill-array-data v0, :array_55

    sput-object v0, Lcom/android/internal/R$styleable;->Keyboard_Key:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_56

    sput-object v0, Lcom/android/internal/R$styleable;->Keyboard_Row:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_57

    sput-object v0, Lcom/android/internal/R$styleable;->KeyguardGlowStripView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_58

    sput-object v0, Lcom/android/internal/R$styleable;->KeyguardSecurityViewFlipper_Layout:[I

    new-array v0, v3, [I

    const v1, 0x101031e    # android.R.attr.opacity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->LayerDrawable:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_59

    sput-object v0, Lcom/android/internal/R$styleable;->LayerDrawableItem:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_5a

    sput-object v0, Lcom/android/internal/R$styleable;->LayoutAnimation:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_5b

    sput-object v0, Lcom/android/internal/R$styleable;->LevelListDrawableItem:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_5c

    sput-object v0, Lcom/android/internal/R$styleable;->LinearLayout:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_5d

    sput-object v0, Lcom/android/internal/R$styleable;->LinearLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_5e

    sput-object v0, Lcom/android/internal/R$styleable;->ListPreference:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_5f

    sput-object v0, Lcom/android/internal/R$styleable;->ListView:[I

    new-array v0, v3, [I

    const v1, 0x101043a    # android.R.attr.aspect

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->LockPatternView:[I

    new-array v0, v3, [I

    const v1, 0x1010211    # android.R.attr.apiKey

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->MapView:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_60

    sput-object v0, Lcom/android/internal/R$styleable;->MediaRouteButton:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->Menu:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_61

    sput-object v0, Lcom/android/internal/R$styleable;->MenuGroup:[I

    const/16 v0, 0x11

    new-array v0, v0, [I

    fill-array-data v0, :array_62

    sput-object v0, Lcom/android/internal/R$styleable;->MenuItem:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_63

    sput-object v0, Lcom/android/internal/R$styleable;->MenuItemCheckedFocusedState:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_64

    sput-object v0, Lcom/android/internal/R$styleable;->MenuItemCheckedState:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_65

    sput-object v0, Lcom/android/internal/R$styleable;->MenuItemUncheckedFocusedState:[I

    new-array v0, v3, [I

    const v1, 0x101009f    # android.R.attr.state_checkable

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->MenuItemUncheckedState:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_66

    sput-object v0, Lcom/android/internal/R$styleable;->MenuView:[I

    new-array v0, v3, [I

    const v1, 0x1010199    # android.R.attr.drawable

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->MipmapDrawableItem:[I

    new-array v0, v3, [I

    const v1, 0x10100c4    # android.R.attr.orientation

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->MultiPaneChallengeLayout:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_67

    sput-object v0, Lcom/android/internal/R$styleable;->MultiPaneChallengeLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_68

    sput-object v0, Lcom/android/internal/R$styleable;->MultiSelectListPreference:[I

    const/16 v0, 0xb

    new-array v0, v0, [I

    fill-array-data v0, :array_69

    sput-object v0, Lcom/android/internal/R$styleable;->MultiWaveView:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_6a

    sput-object v0, Lcom/android/internal/R$styleable;->NinePatchDrawable:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_6b

    sput-object v0, Lcom/android/internal/R$styleable;->NumPadKey:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_6c

    sput-object v0, Lcom/android/internal/R$styleable;->NumberPicker:[I

    new-array v0, v3, [I

    const v1, 0x101026a    # android.R.attr.tension

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->OvershootInterpolator:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_6d

    sput-object v0, Lcom/android/internal/R$styleable;->PagedView:[I

    const/16 v0, 0x32

    new-array v0, v0, [I

    fill-array-data v0, :array_6e

    sput-object v0, Lcom/android/internal/R$styleable;->Pointer:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_6f

    sput-object v0, Lcom/android/internal/R$styleable;->PointerIcon:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_70

    sput-object v0, Lcom/android/internal/R$styleable;->PopupWindow:[I

    new-array v0, v3, [I

    const v1, 0x10100aa    # android.R.attr.state_above_anchor

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->PopupWindowBackgroundState:[I

    const/16 v0, 0xe

    new-array v0, v0, [I

    fill-array-data v0, :array_71

    sput-object v0, Lcom/android/internal/R$styleable;->Preference:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_72

    sput-object v0, Lcom/android/internal/R$styleable;->PreferenceFrameLayout:[I

    new-array v0, v3, [I

    const v1, 0x101040d    # android.R.attr.layout_removeBorders

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->PreferenceFrameLayout_Layout:[I

    new-array v0, v3, [I

    const v1, 0x10101e7    # android.R.attr.orderingFromXml

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->PreferenceGroup:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_73

    sput-object v0, Lcom/android/internal/R$styleable;->PreferenceHeader:[I

    const/16 v0, 0xf

    new-array v0, v0, [I

    fill-array-data v0, :array_74

    sput-object v0, Lcom/android/internal/R$styleable;->ProgressBar:[I

    new-array v0, v3, [I

    const v1, 0x10102e1    # android.R.attr.propertyName

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->PropertyAnimator:[I

    new-array v0, v3, [I

    const v1, 0x1010416    # android.R.attr.quickContactWindowSize

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->QuickContactBadge:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_75

    sput-object v0, Lcom/android/internal/R$styleable;->RadioGroup:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_76

    sput-object v0, Lcom/android/internal/R$styleable;->RatingBar:[I

    new-array v0, v3, [I

    const v1, 0x1010225    # android.R.attr.settingsActivity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->RecognitionService:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_77

    sput-object v0, Lcom/android/internal/R$styleable;->RelativeLayout:[I

    const/16 v0, 0x17

    new-array v0, v0, [I

    fill-array-data v0, :array_78

    sput-object v0, Lcom/android/internal/R$styleable;->RelativeLayout_Layout:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_79

    sput-object v0, Lcom/android/internal/R$styleable;->RingtonePreference:[I

    new-array v0, v3, [I

    const v1, 0x10100c4    # android.R.attr.orientation

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->RotarySelector:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_7a

    sput-object v0, Lcom/android/internal/R$styleable;->RotateAnimation:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_7b

    sput-object v0, Lcom/android/internal/R$styleable;->RotateDrawable:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_7c

    sput-object v0, Lcom/android/internal/R$styleable;->ScaleAnimation:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_7d

    sput-object v0, Lcom/android/internal/R$styleable;->ScaleDrawable:[I

    new-array v0, v3, [I

    const v1, 0x101017a    # android.R.attr.fillViewport

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->ScrollView:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_7e

    sput-object v0, Lcom/android/internal/R$styleable;->SearchView:[I

    const/16 v0, 0x16

    new-array v0, v0, [I

    fill-array-data v0, :array_7f

    sput-object v0, Lcom/android/internal/R$styleable;->Searchable:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_80

    sput-object v0, Lcom/android/internal/R$styleable;->SearchableActionKey:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_81

    sput-object v0, Lcom/android/internal/R$styleable;->SeekBar:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_82

    sput-object v0, Lcom/android/internal/R$styleable;->SelectionModeDrawables:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_83

    sput-object v0, Lcom/android/internal/R$styleable;->ShapeDrawable:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_84

    sput-object v0, Lcom/android/internal/R$styleable;->ShapeDrawablePadding:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->SizeAdaptiveLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_85

    sput-object v0, Lcom/android/internal/R$styleable;->SizeAdaptiveLayout_Layout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_86

    sput-object v0, Lcom/android/internal/R$styleable;->SlidingChallengeLayout_Layout:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_87

    sput-object v0, Lcom/android/internal/R$styleable;->SlidingDrawer:[I

    new-array v0, v3, [I

    const v1, 0x10100c4    # android.R.attr.orientation

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->SlidingTab:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_88

    sput-object v0, Lcom/android/internal/R$styleable;->SpellChecker:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_89

    sput-object v0, Lcom/android/internal/R$styleable;->SpellChecker_Subtype:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_8a

    sput-object v0, Lcom/android/internal/R$styleable;->Spinner:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_8b

    sput-object v0, Lcom/android/internal/R$styleable;->StackView:[I

    const/4 v0, 0x6

    new-array v0, v0, [I

    fill-array-data v0, :array_8c

    sput-object v0, Lcom/android/internal/R$styleable;->StateListDrawable:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_8d

    sput-object v0, Lcom/android/internal/R$styleable;->Storage:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_8e

    sput-object v0, Lcom/android/internal/R$styleable;->SuggestionSpan:[I

    const/16 v0, 0x8

    new-array v0, v0, [I

    fill-array-data v0, :array_8f

    sput-object v0, Lcom/android/internal/R$styleable;->Switch:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_90

    sput-object v0, Lcom/android/internal/R$styleable;->SwitchPreference:[I

    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_91

    sput-object v0, Lcom/android/internal/R$styleable;->SyncAdapter:[I

    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_92

    sput-object v0, Lcom/android/internal/R$styleable;->TabWidget:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_93

    sput-object v0, Lcom/android/internal/R$styleable;->TableLayout:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->TableRow:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_94

    sput-object v0, Lcom/android/internal/R$styleable;->TableRow_Cell:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_95

    sput-object v0, Lcom/android/internal/R$styleable;->TextAppearance:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_96

    sput-object v0, Lcom/android/internal/R$styleable;->TextClock:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->TextSwitcher:[I

    new-array v0, v3, [I

    const v1, 0x1010225    # android.R.attr.settingsActivity

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->TextToSpeechEngine:[I

    const/16 v0, 0x4c

    new-array v0, v0, [I

    fill-array-data v0, :array_97

    sput-object v0, Lcom/android/internal/R$styleable;->TextView:[I

    new-array v0, v3, [I

    const v1, 0x1010034    # android.R.attr.textAppearance

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->TextViewAppearance:[I

    new-array v0, v3, [I

    const v1, 0x101034d    # android.R.attr.state_multiline

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->TextViewMultiLineBackgroundState:[I

    const/16 v0, 0x108

    new-array v0, v0, [I

    fill-array-data v0, :array_98

    sput-object v0, Lcom/android/internal/R$styleable;->Theme:[I

    new-array v0, v3, [I

    const v1, 0x1010415    # android.R.attr.internalLayout

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->TimePicker:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_99

    sput-object v0, Lcom/android/internal/R$styleable;->ToggleButton:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_9a

    sput-object v0, Lcom/android/internal/R$styleable;->TranslateAnimation:[I

    new-array v0, v3, [I

    const v1, 0x101017e    # android.R.attr.mode

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->TwoLineListItem:[I

    new-array v0, v3, [I

    const v1, 0x1010193    # android.R.attr.layout_scale

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->VerticalSlider_Layout:[I

    const/16 v0, 0x44

    new-array v0, v0, [I

    fill-array-data v0, :array_9b

    sput-object v0, Lcom/android/internal/R$styleable;->View:[I

    new-array v0, v6, [I

    fill-array-data v0, :array_9c

    sput-object v0, Lcom/android/internal/R$styleable;->ViewAnimator:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_9d

    sput-object v0, Lcom/android/internal/R$styleable;->ViewDrawableStates:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_9e

    sput-object v0, Lcom/android/internal/R$styleable;->ViewFlipper:[I

    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_9f

    sput-object v0, Lcom/android/internal/R$styleable;->ViewGroup:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_a0

    sput-object v0, Lcom/android/internal/R$styleable;->ViewGroup_Layout:[I

    const/16 v0, 0x9

    new-array v0, v0, [I

    fill-array-data v0, :array_a1

    sput-object v0, Lcom/android/internal/R$styleable;->ViewGroup_MarginLayout:[I

    new-array v0, v4, [I

    fill-array-data v0, :array_a2

    sput-object v0, Lcom/android/internal/R$styleable;->ViewStub:[I

    new-array v0, v2, [I

    sput-object v0, Lcom/android/internal/R$styleable;->ViewSwitcher:[I

    new-array v0, v3, [I

    const v1, 0x1010209    # android.R.attr.streamType

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->VolumePreference:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_a3

    sput-object v0, Lcom/android/internal/R$styleable;->Wallpaper:[I

    new-array v0, v3, [I

    const v1, 0x1010331    # android.R.attr.staticWallpaperPreview

    aput v1, v0, v2

    sput-object v0, Lcom/android/internal/R$styleable;->WallpaperPreviewInfo:[I

    new-array v0, v5, [I

    fill-array-data v0, :array_a4

    sput-object v0, Lcom/android/internal/R$styleable;->WeightedLinearLayout:[I

    const/16 v0, 0x1b

    new-array v0, v0, [I

    fill-array-data v0, :array_a5

    sput-object v0, Lcom/android/internal/R$styleable;->Window:[I

    const/16 v0, 0x18

    new-array v0, v0, [I

    fill-array-data v0, :array_a6

    sput-object v0, Lcom/android/internal/R$styleable;->WindowAnimation:[I

    return-void

    :array_0
    .array-data 4
        0x10100fb    # android.R.attr.listSelector
        0x10100fc    # android.R.attr.drawSelectorOnTop
        0x10100fd    # android.R.attr.stackFromBottom
        0x10100fe    # android.R.attr.scrollingCache
        0x10100ff    # android.R.attr.textFilterEnabled
        0x1010100    # android.R.attr.transcriptMode
        0x1010101    # android.R.attr.cacheColorHint
        0x101012b    # android.R.attr.choiceMode
        0x1010226    # android.R.attr.fastScrollEnabled
        0x1010231    # android.R.attr.smoothScrollbar
        0x1010335    # android.R.attr.fastScrollAlwaysVisible
    .end array-data

    :array_1
    .array-data 4
        0x101017f    # android.R.attr.layout_x
        0x1010180    # android.R.attr.layout_y
    .end array-data

    :array_2
    .array-data 4
        0x1010020    # android.R.attr.description
        0x1010225    # android.R.attr.settingsActivity
        0x1010380    # android.R.attr.accessibilityEventTypes
        0x1010381    # android.R.attr.packageNames
        0x1010382    # android.R.attr.accessibilityFeedbackType
        0x1010383    # android.R.attr.notificationTimeout
        0x1010384    # android.R.attr.accessibilityFlags
        0x1010385    # android.R.attr.canRetrieveWindowContent
    .end array-data

    :array_3
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x101028f    # android.R.attr.accountType
        0x101029e    # android.R.attr.smallIcon
        0x101029f    # android.R.attr.accountPreferences
        0x101033b    # android.R.attr.customTokens
    .end array-data

    :array_4
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x1010077    # android.R.attr.progressBarStyle
        0x10100d4    # android.R.attr.background
        0x1010129    # android.R.attr.divider
        0x1010155    # android.R.attr.height
        0x10101e1    # android.R.attr.title
        0x10102be    # android.R.attr.logo
        0x10102cf    # android.R.attr.navigationMode
        0x10102d0    # android.R.attr.displayOptions
        0x10102d1    # android.R.attr.subtitle
        0x10102d2    # android.R.attr.customNavigationLayout
        0x10102f8    # android.R.attr.titleTextStyle
        0x10102f9    # android.R.attr.subtitleTextStyle
        0x1010318    # android.R.attr.indeterminateProgressStyle
        0x1010319    # android.R.attr.progressBarPadding
        0x101031d    # android.R.attr.homeLayout
        0x101032d    # android.R.attr.itemPadding
        0x101038a    # android.R.attr.backgroundStacked
        0x101038b    # android.R.attr.backgroundSplit
    .end array-data

    :array_5
    .array-data 4
        0x10100d4    # android.R.attr.background
        0x1010155    # android.R.attr.height
        0x10102f8    # android.R.attr.titleTextStyle
        0x10102f9    # android.R.attr.subtitleTextStyle
        0x101038b    # android.R.attr.backgroundSplit
    .end array-data

    :array_6
    .array-data 4
        0x1010426    # android.R.attr.initialActivityCount
        0x1010427    # android.R.attr.expandActivityOverflowButtonDrawable
    .end array-data

    :array_7
    .array-data 4
        0x1010177    # android.R.attr.inAnimation
        0x1010178    # android.R.attr.outAnimation
        0x10102d5    # android.R.attr.animateFirstView
        0x1010307    # android.R.attr.loopViews
    .end array-data

    :array_8
    .array-data 4
        0x1010179    # android.R.attr.flipInterval
        0x10102b5    # android.R.attr.autoStart
    .end array-data

    :array_9
    .array-data 4
        0x10100c6    # android.R.attr.fullDark
        0x10100c7    # android.R.attr.topDark
        0x10100c8    # android.R.attr.centerDark
        0x10100c9    # android.R.attr.bottomDark
        0x10100ca    # android.R.attr.fullBright
        0x10100cb    # android.R.attr.topBright
        0x10100cc    # android.R.attr.centerBright
        0x10100cd    # android.R.attr.bottomBright
        0x10100ce    # android.R.attr.bottomMedium
        0x10100cf    # android.R.attr.centerMedium
        0x10100f2    # android.R.attr.layout
        0x10103ff    # android.R.attr.listLayout
        0x1010402    # android.R.attr.multiChoiceItemLayout
        0x1010403    # android.R.attr.singleChoiceItemLayout
        0x1010404    # android.R.attr.listItemLayout
        0x1010405    # android.R.attr.progressLayout
        0x1010406    # android.R.attr.horizontalProgressLayout
    .end array-data

    :array_a
    .array-data 4
        0x10101ca    # android.R.attr.fromAlpha
        0x10101cb    # android.R.attr.toAlpha
    .end array-data

    :array_b
    .array-data 4
        0x1010102    # android.R.attr.dial
        0x1010103    # android.R.attr.hand_hour
        0x1010104    # android.R.attr.hand_minute
    .end array-data

    :array_c
    .array-data 4
        0x101000b    # android.R.attr.sharedUserId
        0x101021b    # android.R.attr.versionCode
        0x101021c    # android.R.attr.versionName
        0x1010261    # android.R.attr.sharedUserLabel
        0x10102b7    # android.R.attr.installLocation
    .end array-data

    :array_d
    .array-data 4
        0x1010000    # android.R.attr.theme
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010006    # android.R.attr.permission
        0x101000e    # android.R.attr.enabled
        0x1010010    # android.R.attr.exported
        0x1010011    # android.R.attr.process
        0x1010012    # android.R.attr.taskAffinity
        0x1010013    # android.R.attr.multiprocess
        0x1010014    # android.R.attr.finishOnTaskLaunch
        0x1010015    # android.R.attr.clearTaskOnLaunch
        0x1010016    # android.R.attr.stateNotNeeded
        0x1010017    # android.R.attr.excludeFromRecents
        0x101001d    # android.R.attr.launchMode
        0x101001e    # android.R.attr.screenOrientation
        0x101001f    # android.R.attr.configChanges
        0x1010020    # android.R.attr.description
        0x1010203    # android.R.attr.alwaysRetainTaskState
        0x1010204    # android.R.attr.allowTaskReparenting
        0x101022b    # android.R.attr.windowSoftInputMode
        0x101022d    # android.R.attr.noHistory
        0x10102a7    # android.R.attr.finishOnCloseSystemDialogs
        0x10102be    # android.R.attr.logo
        0x10102c0    # android.R.attr.immersive
        0x10102d3    # android.R.attr.hardwareAccelerated
        0x1010398    # android.R.attr.uiOptions
        0x10103a7    # android.R.attr.parentActivityName
        0x10103bf    # android.R.attr.singleUser
        0x10103c9    # android.R.attr.showOnLockScreen
        0x1010487    # android.R.attr.primaryUserOnly
    .end array-data

    :array_e
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010006    # android.R.attr.permission
        0x101000e    # android.R.attr.enabled
        0x1010010    # android.R.attr.exported
        0x1010020    # android.R.attr.description
        0x1010202    # android.R.attr.targetActivity
        0x10102be    # android.R.attr.logo
        0x10103a7    # android.R.attr.parentActivityName
    .end array-data

    :array_f
    .array-data 4
        0x1010000    # android.R.attr.theme
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010004    # android.R.attr.manageSpaceActivity
        0x1010005    # android.R.attr.allowClearUserData
        0x1010006    # android.R.attr.permission
        0x101000c    # android.R.attr.hasCode
        0x101000d    # android.R.attr.persistent
        0x101000e    # android.R.attr.enabled
        0x101000f    # android.R.attr.debuggable
        0x1010011    # android.R.attr.process
        0x1010012    # android.R.attr.taskAffinity
        0x1010020    # android.R.attr.description
        0x1010204    # android.R.attr.allowTaskReparenting
        0x1010272    # android.R.attr.testOnly
        0x101027f    # android.R.attr.backupAgent
        0x1010280    # android.R.attr.allowBackup
        0x101029c    # android.R.attr.killAfterRestore
        0x101029d    # android.R.attr.restoreNeedsApplication
        0x10102b8    # android.R.attr.vmSafeMode
        0x10102ba    # android.R.attr.restoreAnyVersion
        0x10102be    # android.R.attr.logo
        0x10102d3    # android.R.attr.hardwareAccelerated
        0x101035a    # android.R.attr.largeHeap
        0x1010398    # android.R.attr.uiOptions
        0x10103af    # android.R.attr.supportsRtl
        0x1010485    # android.R.attr.neverEncrypt
        0x1010486    # android.R.attr.cantSaveState
    .end array-data

    :array_10
    .array-data 4
        0x10102ca    # android.R.attr.screenSize
        0x10102cb    # android.R.attr.screenDensity
    .end array-data

    :array_11
    .array-data 4
        0x1010026    # android.R.attr.mimeType
        0x1010027    # android.R.attr.scheme
        0x1010028    # android.R.attr.host
        0x1010029    # android.R.attr.port
        0x101002a    # android.R.attr.path
        0x101002b    # android.R.attr.pathPrefix
        0x101002c    # android.R.attr.pathPattern
    .end array-data

    :array_12
    .array-data 4
        0x101002a    # android.R.attr.path
        0x101002b    # android.R.attr.pathPrefix
        0x101002c    # android.R.attr.pathPattern
    .end array-data

    :array_13
    .array-data 4
        0x1010400    # android.R.attr.source
        0x1010401    # android.R.attr.closeDoubleChannel
    .end array-data

    :array_14
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010021    # android.R.attr.targetPackage
        0x1010022    # android.R.attr.handleProfiling
        0x1010023    # android.R.attr.functionalTest
        0x10102be    # android.R.attr.logo
    .end array-data

    :array_15
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x101001c    # android.R.attr.priority
        0x10102be    # android.R.attr.logo
    .end array-data

    :array_16
    .array-data 4
        0x1010003    # android.R.attr.name
        0x1010024    # android.R.attr.value
        0x1010025    # android.R.attr.resource
    .end array-data

    :array_17
    .array-data 4
        0x1010003    # android.R.attr.name
        0x10103a6    # android.R.attr.publicKey
    .end array-data

    :array_18
    .array-data 4
        0x1010006    # android.R.attr.permission
        0x1010007    # android.R.attr.readPermission
        0x1010008    # android.R.attr.writePermission
        0x101002a    # android.R.attr.path
        0x101002b    # android.R.attr.pathPrefix
        0x101002c    # android.R.attr.pathPattern
    .end array-data

    :array_19
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010009    # android.R.attr.protectionLevel
        0x101000a    # android.R.attr.permissionGroup
        0x1010020    # android.R.attr.description
        0x10102be    # android.R.attr.logo
        0x10103c7    # android.R.attr.permissionFlags
    .end array-data

    :array_1a
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x101001c    # android.R.attr.priority
        0x1010020    # android.R.attr.description
        0x10102be    # android.R.attr.logo
        0x10103c5    # android.R.attr.permissionGroupFlags
    .end array-data

    :array_1b
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x10102be    # android.R.attr.logo
    .end array-data

    :array_1c
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010006    # android.R.attr.permission
        0x1010007    # android.R.attr.readPermission
        0x1010008    # android.R.attr.writePermission
        0x101000e    # android.R.attr.enabled
        0x1010010    # android.R.attr.exported
        0x1010011    # android.R.attr.process
        0x1010013    # android.R.attr.multiprocess
        0x1010018    # android.R.attr.authorities
        0x1010019    # android.R.attr.syncable
        0x101001a    # android.R.attr.initOrder
        0x101001b    # android.R.attr.grantUriPermissions
        0x1010020    # android.R.attr.description
        0x10102be    # android.R.attr.logo
        0x10103bf    # android.R.attr.singleUser
    .end array-data

    :array_1d
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010006    # android.R.attr.permission
        0x101000e    # android.R.attr.enabled
        0x1010010    # android.R.attr.exported
        0x1010011    # android.R.attr.process
        0x1010020    # android.R.attr.description
        0x10102be    # android.R.attr.logo
        0x10103bf    # android.R.attr.singleUser
    .end array-data

    :array_1e
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010003    # android.R.attr.name
        0x1010006    # android.R.attr.permission
        0x101000e    # android.R.attr.enabled
        0x1010010    # android.R.attr.exported
        0x1010011    # android.R.attr.process
        0x1010020    # android.R.attr.description
        0x10102be    # android.R.attr.logo
        0x101036a    # android.R.attr.stopWithTask
        0x10103a9    # android.R.attr.isolatedProcess
        0x10103bf    # android.R.attr.singleUser
    .end array-data

    :array_1f
    .array-data 4
        0x101026c    # android.R.attr.anyDensity
        0x1010284    # android.R.attr.smallScreens
        0x1010285    # android.R.attr.normalScreens
        0x1010286    # android.R.attr.largeScreens
        0x101028d    # android.R.attr.resizeable
        0x10102bf    # android.R.attr.xlargeScreens
        0x1010364    # android.R.attr.requiresSmallestWidthDp
        0x1010365    # android.R.attr.compatibleWidthLimitDp
        0x1010366    # android.R.attr.largestWidthLimitDp
    .end array-data

    :array_20
    .array-data 4
        0x1010227    # android.R.attr.reqTouchScreen
        0x1010228    # android.R.attr.reqKeyboardType
        0x1010229    # android.R.attr.reqHardKeyboard
        0x101022a    # android.R.attr.reqNavigation
        0x1010232    # android.R.attr.reqFiveWayNav
    .end array-data

    :array_21
    .array-data 4
        0x1010003    # android.R.attr.name
        0x1010281    # android.R.attr.glEsVersion
        0x101028e    # android.R.attr.required
    .end array-data

    :array_22
    .array-data 4
        0x1010003    # android.R.attr.name
        0x101028e    # android.R.attr.required
    .end array-data

    :array_23
    .array-data 4
        0x101020c    # android.R.attr.minSdkVersion
        0x1010270    # android.R.attr.targetSdkVersion
        0x1010271    # android.R.attr.maxSdkVersion
    .end array-data

    :array_24
    .array-data 4
        0x1010194    # android.R.attr.visible
        0x1010199    # android.R.attr.drawable
        0x10101b5    # android.R.attr.pivotX
        0x10101b6    # android.R.attr.pivotY
        0x1010423    # android.R.attr.frameDuration
        0x1010424    # android.R.attr.framesCount
    .end array-data

    :array_25
    .array-data 4
        0x10100d4    # android.R.attr.background
        0x1010141    # android.R.attr.interpolator
        0x1010198    # android.R.attr.duration
        0x10101bc    # android.R.attr.fillBefore
        0x10101bd    # android.R.attr.fillAfter
        0x10101be    # android.R.attr.startOffset
        0x10101bf    # android.R.attr.repeatCount
        0x10101c0    # android.R.attr.repeatMode
        0x10101c1    # android.R.attr.zAdjustment
        0x101024f    # android.R.attr.fillEnabled
        0x10102a6    # android.R.attr.detachWallpaper
    .end array-data

    :array_26
    .array-data 4
        0x1010194    # android.R.attr.visible
        0x1010195    # android.R.attr.variablePadding
        0x1010197    # android.R.attr.oneshot
    .end array-data

    :array_27
    .array-data 4
        0x1010198    # android.R.attr.duration
        0x1010199    # android.R.attr.drawable
    .end array-data

    :array_28
    .array-data 4
        0x1010198    # android.R.attr.duration
        0x10101bb    # android.R.attr.shareInterpolator
        0x10101bc    # android.R.attr.fillBefore
        0x10101bd    # android.R.attr.fillAfter
        0x10101be    # android.R.attr.startOffset
        0x10101c0    # android.R.attr.repeatMode
    .end array-data

    :array_29
    .array-data 4
        0x1010141    # android.R.attr.interpolator
        0x1010198    # android.R.attr.duration
        0x10101be    # android.R.attr.startOffset
        0x10101bf    # android.R.attr.repeatCount
        0x10101c0    # android.R.attr.repeatMode
        0x10102de    # android.R.attr.valueFrom
        0x10102df    # android.R.attr.valueTo
        0x10102e0    # android.R.attr.valueType
    .end array-data

    :array_2a
    .array-data 4
        0x101026a    # android.R.attr.tension
        0x101026b    # android.R.attr.extraTension
    .end array-data

    :array_2b
    .array-data 4
        0x101013f    # android.R.attr.minWidth
        0x1010140    # android.R.attr.minHeight
        0x1010250    # android.R.attr.updatePeriodMillis
        0x1010251    # android.R.attr.initialLayout
        0x101025d    # android.R.attr.configure
        0x10102da    # android.R.attr.previewImage
        0x101030f    # android.R.attr.autoAdvanceViewId
        0x1010363    # android.R.attr.resizeMode
        0x1010395    # android.R.attr.minResizeWidth
        0x1010396    # android.R.attr.minResizeHeight
        0x10103c2    # android.R.attr.initialKeyguardLayout
        0x10103c4    # android.R.attr.widgetCategory
    .end array-data

    :array_2c
    .array-data 4
        0x1010172    # android.R.attr.completionHint
        0x1010173    # android.R.attr.completionHintView
        0x1010174    # android.R.attr.completionThreshold
        0x1010175    # android.R.attr.dropDownSelector
        0x1010220    # android.R.attr.inputType
        0x1010262    # android.R.attr.dropDownWidth
        0x1010263    # android.R.attr.dropDownAnchor
        0x1010283    # android.R.attr.dropDownHeight
        0x10102ac    # android.R.attr.dropDownHorizontalOffset
        0x10102ad    # android.R.attr.dropDownVerticalOffset
    .end array-data

    :array_2d
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x1010119    # android.R.attr.src
        0x101011a    # android.R.attr.antialias
        0x101011b    # android.R.attr.filter
        0x101011c    # android.R.attr.dither
        0x1010201    # android.R.attr.tileMode
    .end array-data

    :array_2e
    .array-data 4
        0x101033d    # android.R.attr.firstDayOfWeek
        0x101033e    # android.R.attr.showWeekNumber
        0x101033f    # android.R.attr.minDate
        0x1010340    # android.R.attr.maxDate
        0x1010341    # android.R.attr.shownWeekCount
        0x1010342    # android.R.attr.selectedWeekBackgroundColor
        0x1010343    # android.R.attr.focusedMonthDateColor
        0x1010344    # android.R.attr.unfocusedMonthDateColor
        0x1010345    # android.R.attr.weekNumberColor
        0x1010346    # android.R.attr.weekSeparatorLineColor
        0x1010347    # android.R.attr.selectedDateVerticalBar
        0x1010348    # android.R.attr.weekDayTextAppearance
        0x1010349    # android.R.attr.dateTextAppearance
    .end array-data

    :array_2f
    .array-data 4
        0x10101ef    # android.R.attr.summaryOn
        0x10101f0    # android.R.attr.summaryOff
        0x10101f1    # android.R.attr.disableDependentsState
    .end array-data

    :array_30
    .array-data 4
        0x1010106    # android.R.attr.checked
        0x1010108    # android.R.attr.checkMark
    .end array-data

    :array_31
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x1010199    # android.R.attr.drawable
        0x101020a    # android.R.attr.clipOrientation
    .end array-data

    :array_32
    .array-data 4
        0x1010106    # android.R.attr.checked
        0x1010107    # android.R.attr.button
    .end array-data

    :array_33
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x1010026    # android.R.attr.mimeType
        0x10102a2    # android.R.attr.summaryColumn
        0x10102a3    # android.R.attr.detailColumn
        0x10102a4    # android.R.attr.detailSocialSummary
        0x10102cc    # android.R.attr.allContactsName
    .end array-data

    :array_34
    .array-data 4
        0x101017c    # android.R.attr.startYear
        0x101017d    # android.R.attr.endYear
        0x101033f    # android.R.attr.minDate
        0x1010340    # android.R.attr.maxDate
        0x101034b    # android.R.attr.spinnersShown
        0x101034c    # android.R.attr.calendarViewShown
        0x1010415    # android.R.attr.internalLayout
    .end array-data

    :array_35
    .array-data 4
        0x10101f2    # android.R.attr.dialogTitle
        0x10101f3    # android.R.attr.dialogMessage
        0x10101f4    # android.R.attr.dialogIcon
        0x10101f5    # android.R.attr.positiveButtonText
        0x10101f6    # android.R.attr.negativeButtonText
        0x10101f7    # android.R.attr.dialogLayout
    .end array-data

    :array_36
    .array-data 4
        0x10101a8    # android.R.attr.radius
        0x10101a9    # android.R.attr.topLeftRadius
        0x10101aa    # android.R.attr.topRightRadius
        0x10101ab    # android.R.attr.bottomLeftRadius
        0x10101ac    # android.R.attr.bottomRightRadius
    .end array-data

    :array_37
    .array-data 4
        0x101009c    # android.R.attr.state_focused
        0x101009d    # android.R.attr.state_window_focused
        0x101009e    # android.R.attr.state_enabled
        0x101009f    # android.R.attr.state_checkable
        0x10100a0    # android.R.attr.state_checked
        0x10100a1    # android.R.attr.state_selected
        0x10100a2    # android.R.attr.state_active
        0x10100a3    # android.R.attr.state_single
        0x10100a4    # android.R.attr.state_first
        0x10100a5    # android.R.attr.state_middle
        0x10100a6    # android.R.attr.state_last
        0x10100a7    # android.R.attr.state_pressed
        0x10102fe    # android.R.attr.state_activated
        0x101031b    # android.R.attr.state_accelerated
        0x1010367    # android.R.attr.state_hovered
        0x1010368    # android.R.attr.state_drag_can_accept
        0x1010369    # android.R.attr.state_drag_hovered
        0x1010425    # android.R.attr.state_accessibility_focused
    .end array-data

    :array_38
    .array-data 4
        0x10100a8    # android.R.attr.state_expanded
        0x10100a9    # android.R.attr.state_empty
    .end array-data

    :array_39
    .array-data 4
        0x101010b    # android.R.attr.groupIndicator
        0x101010c    # android.R.attr.childIndicator
        0x101010d    # android.R.attr.indicatorLeft
        0x101010e    # android.R.attr.indicatorRight
        0x101010f    # android.R.attr.childIndicatorLeft
        0x1010110    # android.R.attr.childIndicatorRight
        0x1010111    # android.R.attr.childDivider
    .end array-data

    :array_3a
    .array-data 4
        0x1010003    # android.R.attr.name
        0x1010024    # android.R.attr.value
    .end array-data

    :array_3b
    .array-data 4
        0x1010003    # android.R.attr.name
        0x10100d0    # android.R.attr.id
        0x10100d1    # android.R.attr.tag
    .end array-data

    :array_3c
    .array-data 4
        0x10102e5    # android.R.attr.fragmentOpenEnterAnimation
        0x10102e6    # android.R.attr.fragmentOpenExitAnimation
        0x10102e7    # android.R.attr.fragmentCloseEnterAnimation
        0x10102e8    # android.R.attr.fragmentCloseExitAnimation
        0x10102e9    # android.R.attr.fragmentFadeEnterAnimation
        0x10102ea    # android.R.attr.fragmentFadeExitAnimation
    .end array-data

    :array_3d
    .array-data 4
        0x1010109    # android.R.attr.foreground
        0x101010a    # android.R.attr.measureAllChildren
        0x1010200    # android.R.attr.foregroundGravity
        0x1010407    # android.R.attr.foregroundInsidePadding
    .end array-data

    :array_3e
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x1010112    # android.R.attr.animationDuration
        0x1010113    # android.R.attr.spacing
        0x101020e    # android.R.attr.unselectedAlpha
    .end array-data

    :array_3f
    .array-data 4
        0x10100c4    # android.R.attr.orientation
        0x1010274    # android.R.attr.gestureStrokeWidth
        0x1010275    # android.R.attr.gestureColor
        0x1010276    # android.R.attr.uncertainGestureColor
        0x1010277    # android.R.attr.fadeOffset
        0x1010278    # android.R.attr.fadeDuration
        0x1010279    # android.R.attr.gestureStrokeType
        0x101027a    # android.R.attr.gestureStrokeLengthThreshold
        0x101027b    # android.R.attr.gestureStrokeSquarenessThreshold
        0x101027c    # android.R.attr.gestureStrokeAngleThreshold
        0x101027d    # android.R.attr.eventsInterceptionEnabled
        0x101027e    # android.R.attr.fadeEnabled
    .end array-data

    :array_40
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x101025f    # android.R.attr.innerRadius
        0x10103a0    # android.R.attr.targetDescriptions
        0x10103a1    # android.R.attr.directionDescriptions
        0x1010429    # android.R.attr.outerRingDrawable
        0x101042a    # android.R.attr.pointDrawable
        0x101042b    # android.R.attr.glowRadius
        0x101042c    # android.R.attr.firstItemOffset
        0x101042d    # android.R.attr.magneticTargets
        0x101042e    # android.R.attr.allowScaling
        0x101042f    # android.R.attr.targetDrawables
        0x1010430    # android.R.attr.handleDrawable
        0x1010433    # android.R.attr.outerRadius
        0x1010434    # android.R.attr.vibrationDuration
        0x1010435    # android.R.attr.snapMargin
        0x1010436    # android.R.attr.feedbackCount
        0x1010437    # android.R.attr.alwaysTrackFinger
    .end array-data

    :array_41
    .array-data 4
        0x101011c    # android.R.attr.dither
        0x1010194    # android.R.attr.visible
        0x101019a    # android.R.attr.shape
        0x101019b    # android.R.attr.innerRadiusRatio
        0x101019c    # android.R.attr.thicknessRatio
        0x101019f    # android.R.attr.useLevel
        0x101025f    # android.R.attr.innerRadius
        0x1010260    # android.R.attr.thickness
    .end array-data

    :array_42
    .array-data 4
        0x101019d    # android.R.attr.startColor
        0x101019e    # android.R.attr.endColor
        0x101019f    # android.R.attr.useLevel
        0x10101a0    # android.R.attr.angle
        0x10101a1    # android.R.attr.type
        0x10101a2    # android.R.attr.centerX
        0x10101a3    # android.R.attr.centerY
        0x10101a4    # android.R.attr.gradientRadius
        0x101020b    # android.R.attr.centerColor
    .end array-data

    :array_43
    .array-data 4
        0x10101ad    # android.R.attr.left
        0x10101ae    # android.R.attr.top
        0x10101af    # android.R.attr.right
        0x10101b0    # android.R.attr.bottom
    .end array-data

    :array_44
    .array-data 4
        0x1010155    # android.R.attr.height
        0x1010159    # android.R.attr.width
    .end array-data

    :array_45
    .array-data 4
        0x1010159    # android.R.attr.width
        0x10101a5    # android.R.attr.color
        0x10101a6    # android.R.attr.dashWidth
        0x10101a7    # android.R.attr.dashGap
    .end array-data

    :array_46
    .array-data 4
        0x10100c4    # android.R.attr.orientation
        0x1010375    # android.R.attr.rowCount
        0x1010376    # android.R.attr.rowOrderPreserved
        0x1010377    # android.R.attr.columnCount
        0x1010378    # android.R.attr.columnOrderPreserved
        0x1010379    # android.R.attr.useDefaultMargins
        0x101037a    # android.R.attr.alignmentMode
    .end array-data

    :array_47
    .array-data 4
        0x10101cf    # android.R.attr.columnDelay
        0x10101d0    # android.R.attr.rowDelay
        0x10101d1    # android.R.attr.direction
        0x10101d2    # android.R.attr.directionPriority
    .end array-data

    :array_48
    .array-data 4
        0x10100b3    # android.R.attr.layout_gravity
        0x101014c    # android.R.attr.layout_column
        0x101037b    # android.R.attr.layout_row
        0x101037c    # android.R.attr.layout_rowSpan
        0x101037d    # android.R.attr.layout_columnSpan
    .end array-data

    :array_49
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x1010114    # android.R.attr.horizontalSpacing
        0x1010115    # android.R.attr.verticalSpacing
        0x1010116    # android.R.attr.stretchMode
        0x1010117    # android.R.attr.columnWidth
        0x1010118    # android.R.attr.numColumns
    .end array-data

    :array_4a
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x1010026    # android.R.attr.mimeType
    .end array-data

    :array_4b
    .array-data 4
        0x1010132    # android.R.attr.rowHeight
        0x1010133    # android.R.attr.maxRows
        0x1010134    # android.R.attr.maxItemsPerRow
        0x1010135    # android.R.attr.moreIcon
        0x101040f    # android.R.attr.maxItems
    .end array-data

    :array_4c
    .array-data 4
        0x1010119    # android.R.attr.src
        0x101011d    # android.R.attr.scaleType
        0x101011e    # android.R.attr.adjustViewBounds
        0x101011f    # android.R.attr.maxWidth
        0x1010120    # android.R.attr.maxHeight
        0x1010121    # android.R.attr.tint
        0x1010122    # android.R.attr.baselineAlignBottom
        0x1010123    # android.R.attr.cropToPadding
        0x101031c    # android.R.attr.baseline
        0x1010408    # android.R.attr.drawableAlpha
    .end array-data

    :array_4d
    .array-data 4
        0x1010221    # android.R.attr.isDefault
        0x1010225    # android.R.attr.settingsActivity
    .end array-data

    :array_4e
    .array-data 4
        0x101022c    # android.R.attr.imeFullscreenBackground
        0x1010268    # android.R.attr.imeExtractEnterAnimation
        0x1010269    # android.R.attr.imeExtractExitAnimation
    .end array-data

    :array_4f
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x10102ec    # android.R.attr.imeSubtypeLocale
        0x10102ed    # android.R.attr.imeSubtypeMode
        0x10102ee    # android.R.attr.imeSubtypeExtraValue
        0x101037f    # android.R.attr.isAuxiliary
        0x10103a2    # android.R.attr.overridesImplicitlyEnabledSubtype
        0x10103c1    # android.R.attr.subtypeId
    .end array-data

    :array_50
    .array-data 4
        0x1010194    # android.R.attr.visible
        0x1010199    # android.R.attr.drawable
        0x10101b7    # android.R.attr.insetLeft
        0x10101b8    # android.R.attr.insetRight
        0x10101b9    # android.R.attr.insetTop
        0x10101ba    # android.R.attr.insetBottom
    .end array-data

    :array_51
    .array-data 4
        0x1010021    # android.R.attr.targetPackage
        0x1010026    # android.R.attr.mimeType
        0x101002d    # android.R.attr.action
        0x101002e    # android.R.attr.data
        0x101002f    # android.R.attr.targetClass
    .end array-data

    :array_52
    .array-data 4
        0x101023d    # android.R.attr.keyWidth
        0x101023e    # android.R.attr.keyHeight
        0x101023f    # android.R.attr.horizontalGap
        0x1010240    # android.R.attr.verticalGap
    .end array-data

    :array_53
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010003    # android.R.attr.name
        0x10103ab    # android.R.attr.keyboardLayout
    .end array-data

    :array_54
    .array-data 4
        0x1010161    # android.R.attr.shadowColor
        0x1010164    # android.R.attr.shadowRadius
        0x1010233    # android.R.attr.keyBackground
        0x1010234    # android.R.attr.keyTextSize
        0x1010235    # android.R.attr.labelTextSize
        0x1010236    # android.R.attr.keyTextColor
        0x1010237    # android.R.attr.keyPreviewLayout
        0x1010238    # android.R.attr.keyPreviewOffset
        0x1010239    # android.R.attr.keyPreviewHeight
        0x101023a    # android.R.attr.verticalCorrection
        0x101023b    # android.R.attr.popupLayout
        0x1010428    # android.R.attr.keyboardViewStyle
    .end array-data

    :array_55
    .array-data 4
        0x1010242    # android.R.attr.codes
        0x1010243    # android.R.attr.popupKeyboard
        0x1010244    # android.R.attr.popupCharacters
        0x1010245    # android.R.attr.keyEdgeFlags
        0x1010246    # android.R.attr.isModifier
        0x1010247    # android.R.attr.isSticky
        0x1010248    # android.R.attr.isRepeatable
        0x1010249    # android.R.attr.iconPreview
        0x101024a    # android.R.attr.keyOutputText
        0x101024b    # android.R.attr.keyLabel
        0x101024c    # android.R.attr.keyIcon
        0x101024d    # android.R.attr.keyboardMode
    .end array-data

    :array_56
    .array-data 4
        0x1010241    # android.R.attr.rowEdgeFlags
        0x101024d    # android.R.attr.keyboardMode
    .end array-data

    :array_57
    .array-data 4
        0x101047c    # android.R.attr.dotSize
        0x101047d    # android.R.attr.numDots
        0x101047e    # android.R.attr.glowDot
        0x101047f    # android.R.attr.leftToRight
    .end array-data

    :array_58
    .array-data 4
        0x1010438    # android.R.attr.layout_maxHeight
        0x1010482    # android.R.attr.layout_maxWidth
    .end array-data

    :array_59
    .array-data 4
        0x10100d0    # android.R.attr.id
        0x1010199    # android.R.attr.drawable
        0x10101ad    # android.R.attr.left
        0x10101ae    # android.R.attr.top
        0x10101af    # android.R.attr.right
        0x10101b0    # android.R.attr.bottom
    .end array-data

    :array_5a
    .array-data 4
        0x1010141    # android.R.attr.interpolator
        0x10101cc    # android.R.attr.delay
        0x10101cd    # android.R.attr.animation
        0x10101ce    # android.R.attr.animationOrder
    .end array-data

    :array_5b
    .array-data 4
        0x1010199    # android.R.attr.drawable
        0x10101b1    # android.R.attr.minLevel
        0x10101b2    # android.R.attr.maxLevel
    .end array-data

    :array_5c
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x10100c4    # android.R.attr.orientation
        0x1010126    # android.R.attr.baselineAligned
        0x1010127    # android.R.attr.baselineAlignedChildIndex
        0x1010128    # android.R.attr.weightSum
        0x1010129    # android.R.attr.divider
        0x10102d4    # android.R.attr.measureWithLargestChild
        0x1010329    # android.R.attr.showDividers
        0x101032a    # android.R.attr.dividerPadding
    .end array-data

    :array_5d
    .array-data 4
        0x10100b3    # android.R.attr.layout_gravity
        0x10100f4    # android.R.attr.layout_width
        0x10100f5    # android.R.attr.layout_height
        0x1010181    # android.R.attr.layout_weight
    .end array-data

    :array_5e
    .array-data 4
        0x10100b2    # android.R.attr.entries
        0x10101f8    # android.R.attr.entryValues
    .end array-data

    :array_5f
    .array-data 4
        0x10100b2    # android.R.attr.entries
        0x1010129    # android.R.attr.divider
        0x101012a    # android.R.attr.dividerHeight
        0x101022e    # android.R.attr.headerDividersEnabled
        0x101022f    # android.R.attr.footerDividersEnabled
        0x10102c2    # android.R.attr.overScrollHeader
        0x10102c3    # android.R.attr.overScrollFooter
    .end array-data

    :array_60
    .array-data 4
        0x101013f    # android.R.attr.minWidth
        0x1010140    # android.R.attr.minHeight
        0x10103ae    # android.R.attr.mediaRouteTypes
        0x1010478    # android.R.attr.externalRouteEnabledDrawable
    .end array-data

    :array_61
    .array-data 4
        0x101000e    # android.R.attr.enabled
        0x10100d0    # android.R.attr.id
        0x1010194    # android.R.attr.visible
        0x10101de    # android.R.attr.menuCategory
        0x10101df    # android.R.attr.orderInCategory
        0x10101e0    # android.R.attr.checkableBehavior
    .end array-data

    :array_62
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101000e    # android.R.attr.enabled
        0x10100d0    # android.R.attr.id
        0x1010106    # android.R.attr.checked
        0x1010194    # android.R.attr.visible
        0x10101de    # android.R.attr.menuCategory
        0x10101df    # android.R.attr.orderInCategory
        0x10101e1    # android.R.attr.title
        0x10101e2    # android.R.attr.titleCondensed
        0x10101e3    # android.R.attr.alphabeticShortcut
        0x10101e4    # android.R.attr.numericShortcut
        0x10101e5    # android.R.attr.checkable
        0x101026f    # android.R.attr.onClick
        0x10102d9    # android.R.attr.showAsAction
        0x10102fb    # android.R.attr.actionLayout
        0x10102fc    # android.R.attr.actionViewClass
        0x1010389    # android.R.attr.actionProviderClass
    .end array-data

    :array_63
    .array-data 4
        0x101009c    # android.R.attr.state_focused
        0x101009f    # android.R.attr.state_checkable
        0x10100a0    # android.R.attr.state_checked
    .end array-data

    :array_64
    .array-data 4
        0x101009f    # android.R.attr.state_checkable
        0x10100a0    # android.R.attr.state_checked
    .end array-data

    :array_65
    .array-data 4
        0x101009c    # android.R.attr.state_focused
        0x101009f    # android.R.attr.state_checkable
    .end array-data

    :array_66
    .array-data 4
        0x10100ae    # android.R.attr.windowAnimationStyle
        0x101012c    # android.R.attr.itemTextAppearance
        0x101012d    # android.R.attr.horizontalDivider
        0x101012e    # android.R.attr.verticalDivider
        0x101012f    # android.R.attr.headerBackground
        0x1010130    # android.R.attr.itemBackground
        0x1010131    # android.R.attr.itemIconDisabledAlpha
        0x101040e    # android.R.attr.preserveIconSpacing
    .end array-data

    :array_67
    .array-data 4
        0x10100b3    # android.R.attr.layout_gravity
        0x1010438    # android.R.attr.layout_maxHeight
        0x1010480    # android.R.attr.layout_childType
        0x1010481    # android.R.attr.layout_centerWithinArea
        0x1010482    # android.R.attr.layout_maxWidth
    .end array-data

    :array_68
    .array-data 4
        0x10100b2    # android.R.attr.entries
        0x10101f8    # android.R.attr.entryValues
    .end array-data

    :array_69
    .array-data 4
        0x10103a0    # android.R.attr.targetDescriptions
        0x10103a1    # android.R.attr.directionDescriptions
        0x101042f    # android.R.attr.targetDrawables
        0x1010430    # android.R.attr.handleDrawable
        0x1010431    # android.R.attr.chevronDrawables
        0x1010432    # android.R.attr.waveDrawable
        0x1010433    # android.R.attr.outerRadius
        0x1010434    # android.R.attr.vibrationDuration
        0x1010435    # android.R.attr.snapMargin
        0x1010436    # android.R.attr.feedbackCount
        0x1010437    # android.R.attr.alwaysTrackFinger
    .end array-data

    :array_6a
    .array-data 4
        0x1010119    # android.R.attr.src
        0x101011c    # android.R.attr.dither
    .end array-data

    :array_6b
    .array-data 4
        0x1010483    # android.R.attr.digit
        0x1010484    # android.R.attr.textView
    .end array-data

    :array_6c
    .array-data 4
        0x101034a    # android.R.attr.solidColor
        0x1010415    # android.R.attr.internalLayout
        0x101041b    # android.R.attr.selectionDivider
        0x101041c    # android.R.attr.selectionDividerHeight
        0x101041d    # android.R.attr.selectionDividersDistance
        0x101041e    # android.R.attr.internalMinHeight
        0x101041f    # android.R.attr.internalMaxHeight
        0x1010420    # android.R.attr.internalMinWidth
        0x1010421    # android.R.attr.internalMaxWidth
        0x1010422    # android.R.attr.virtualButtonPressedDrawable
    .end array-data

    :array_6d
    .array-data 4
        0x1010479    # android.R.attr.pageSpacing
        0x101047a    # android.R.attr.scrollIndicatorPaddingLeft
        0x101047b    # android.R.attr.scrollIndicatorPaddingRight
    .end array-data

    :array_6e
    .array-data 4
        0x101043b    # android.R.attr.pointerIconArrow
        0x101043c    # android.R.attr.pointerIconSpotHover
        0x101043d    # android.R.attr.pointerIconSpotTouch
        0x101043e    # android.R.attr.pointerIconSpotAnchor
        0x101043f    # android.R.attr.pointerIconSpotWave1
        0x1010440    # android.R.attr.pointerIconSpotWave2
        0x1010441    # android.R.attr.pointerIconSpotWave3
        0x1010442    # android.R.attr.pointerIconSpotWave4
        0x1010443    # android.R.attr.pointerIconSpotWave5
        0x1010444    # android.R.attr.pointerIconSpotHand
        0x1010445    # android.R.attr.pointerIconSpotClaw1
        0x1010446    # android.R.attr.pointerIconSpotClaw2
        0x1010447    # android.R.attr.pointerIconSpotHold1
        0x1010448    # android.R.attr.pointerIconSpotHold2
        0x1010449    # android.R.attr.pointerIconSpotHold3
        0x101044a    # android.R.attr.pointerIconSpotHold4
        0x101044b    # android.R.attr.pointerIconSpotHold5
        0x101044c    # android.R.attr.pointerIconSpotHold6
        0x101044d    # android.R.attr.pointerIconSpotHold7
        0x101044e    # android.R.attr.pointerIconSpotHold8
        0x101044f    # android.R.attr.pointerIconSpotHold9
        0x1010450    # android.R.attr.pointerIconSpotLback1
        0x1010451    # android.R.attr.pointerIconSpotLback2
        0x1010452    # android.R.attr.pointerIconSpotLback3
        0x1010453    # android.R.attr.pointerIconSpotLback4
        0x1010454    # android.R.attr.pointerIconSpotLback5
        0x1010455    # android.R.attr.pointerIconSpotLback6
        0x1010456    # android.R.attr.pointerIconSpotLback7
        0x1010457    # android.R.attr.pointerIconSpotLback8
        0x1010458    # android.R.attr.pointerIconSpotPageup
        0x1010459    # android.R.attr.pointerIconSpotPageup1
        0x101045a    # android.R.attr.pointerIconSpotPageup2
        0x101045b    # android.R.attr.pointerIconSpotPageup3
        0x101045c    # android.R.attr.pointerIconSpotPagedown
        0x101045d    # android.R.attr.pointerIconSpotPagedown1
        0x101045e    # android.R.attr.pointerIconSpotPagedown2
        0x101045f    # android.R.attr.pointerIconSpotPagedown3
        0x1010460    # android.R.attr.pointerIconSpotRback1
        0x1010461    # android.R.attr.pointerIconSpotRback2
        0x1010462    # android.R.attr.pointerIconSpotRback3
        0x1010463    # android.R.attr.pointerIconSpotRback4
        0x1010464    # android.R.attr.pointerIconSpotRback5
        0x1010465    # android.R.attr.pointerIconSpotRback6
        0x1010466    # android.R.attr.pointerIconSpotRback7
        0x1010467    # android.R.attr.pointerIconSpotRback8
        0x1010468    # android.R.attr.pointerIconSpotBlank1
        0x1010469    # android.R.attr.pointerIconSpotBlank2
        0x101046a    # android.R.attr.pointerIconSpotBlank3
        0x101046b    # android.R.attr.pointerIconSpotBlank4
        0x101046c    # android.R.attr.pointerIconSpotBlank5
    .end array-data

    :array_6f
    .array-data 4
        0x101046d    # android.R.attr.bitmap
        0x101046e    # android.R.attr.hotSpotX
        0x101046f    # android.R.attr.hotSpotY
    .end array-data

    :array_70
    .array-data 4
        0x1010176    # android.R.attr.popupBackground
        0x10102c9    # android.R.attr.popupAnimationStyle
    .end array-data

    :array_71
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x101000d    # android.R.attr.persistent
        0x101000e    # android.R.attr.enabled
        0x10100f2    # android.R.attr.layout
        0x10101e1    # android.R.attr.title
        0x10101e6    # android.R.attr.selectable
        0x10101e8    # android.R.attr.key
        0x10101e9    # android.R.attr.summary
        0x10101ea    # android.R.attr.order
        0x10101eb    # android.R.attr.widgetLayout
        0x10101ec    # android.R.attr.dependency
        0x10101ed    # android.R.attr.defaultValue
        0x10101ee    # android.R.attr.shouldDisableView
        0x10102e3    # android.R.attr.fragment
    .end array-data

    :array_72
    .array-data 4
        0x1010409    # android.R.attr.borderTop
        0x101040a    # android.R.attr.borderBottom
        0x101040b    # android.R.attr.borderLeft
        0x101040c    # android.R.attr.borderRight
    .end array-data

    :array_73
    .array-data 4
        0x1010002    # android.R.attr.icon
        0x10100d0    # android.R.attr.id
        0x10101e1    # android.R.attr.title
        0x10101e9    # android.R.attr.summary
        0x10102e3    # android.R.attr.fragment
        0x1010303    # android.R.attr.breadCrumbTitle
        0x1010304    # android.R.attr.breadCrumbShortTitle
    .end array-data

    :array_74
    .array-data 4
        0x101011f    # android.R.attr.maxWidth
        0x1010120    # android.R.attr.maxHeight
        0x1010136    # android.R.attr.max
        0x1010137    # android.R.attr.progress
        0x1010138    # android.R.attr.secondaryProgress
        0x1010139    # android.R.attr.indeterminate
        0x101013a    # android.R.attr.indeterminateOnly
        0x101013b    # android.R.attr.indeterminateDrawable
        0x101013c    # android.R.attr.progressDrawable
        0x101013d    # android.R.attr.indeterminateDuration
        0x101013e    # android.R.attr.indeterminateBehavior
        0x101013f    # android.R.attr.minWidth
        0x1010140    # android.R.attr.minHeight
        0x1010141    # android.R.attr.interpolator
        0x101031a    # android.R.attr.animationResolution
    .end array-data

    :array_75
    .array-data 4
        0x10100c4    # android.R.attr.orientation
        0x1010148    # android.R.attr.checkedButton
    .end array-data

    :array_76
    .array-data 4
        0x1010144    # android.R.attr.numStars
        0x1010145    # android.R.attr.rating
        0x1010146    # android.R.attr.stepSize
        0x1010147    # android.R.attr.isIndicator
    .end array-data

    :array_77
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x10101ff    # android.R.attr.ignoreGravity
    .end array-data

    :array_78
    .array-data 4
        0x1010182    # android.R.attr.layout_toLeftOf
        0x1010183    # android.R.attr.layout_toRightOf
        0x1010184    # android.R.attr.layout_above
        0x1010185    # android.R.attr.layout_below
        0x1010186    # android.R.attr.layout_alignBaseline
        0x1010187    # android.R.attr.layout_alignLeft
        0x1010188    # android.R.attr.layout_alignTop
        0x1010189    # android.R.attr.layout_alignRight
        0x101018a    # android.R.attr.layout_alignBottom
        0x101018b    # android.R.attr.layout_alignParentLeft
        0x101018c    # android.R.attr.layout_alignParentTop
        0x101018d    # android.R.attr.layout_alignParentRight
        0x101018e    # android.R.attr.layout_alignParentBottom
        0x101018f    # android.R.attr.layout_centerInParent
        0x1010190    # android.R.attr.layout_centerHorizontal
        0x1010191    # android.R.attr.layout_centerVertical
        0x1010192    # android.R.attr.layout_alignWithParentIfMissing
        0x10103b7    # android.R.attr.layout_toStartOf
        0x10103b8    # android.R.attr.layout_toEndOf
        0x10103b9    # android.R.attr.layout_alignStart
        0x10103ba    # android.R.attr.layout_alignEnd
        0x10103bb    # android.R.attr.layout_alignParentStart
        0x10103bc    # android.R.attr.layout_alignParentEnd
    .end array-data

    :array_79
    .array-data 4
        0x10101f9    # android.R.attr.ringtoneType
        0x10101fa    # android.R.attr.showDefault
        0x10101fb    # android.R.attr.showSilent
    .end array-data

    :array_7a
    .array-data 4
        0x10101b3    # android.R.attr.fromDegrees
        0x10101b4    # android.R.attr.toDegrees
        0x10101b5    # android.R.attr.pivotX
        0x10101b6    # android.R.attr.pivotY
    .end array-data

    :array_7b
    .array-data 4
        0x1010194    # android.R.attr.visible
        0x1010199    # android.R.attr.drawable
        0x10101b3    # android.R.attr.fromDegrees
        0x10101b4    # android.R.attr.toDegrees
        0x10101b5    # android.R.attr.pivotX
        0x10101b6    # android.R.attr.pivotY
    .end array-data

    :array_7c
    .array-data 4
        0x10101b5    # android.R.attr.pivotX
        0x10101b6    # android.R.attr.pivotY
        0x10101c2    # android.R.attr.fromXScale
        0x10101c3    # android.R.attr.toXScale
        0x10101c4    # android.R.attr.fromYScale
        0x10101c5    # android.R.attr.toYScale
    .end array-data

    :array_7d
    .array-data 4
        0x1010199    # android.R.attr.drawable
        0x10101fc    # android.R.attr.scaleWidth
        0x10101fd    # android.R.attr.scaleHeight
        0x10101fe    # android.R.attr.scaleGravity
        0x1010310    # android.R.attr.useIntrinsicSizeAsMinimum
    .end array-data

    :array_7e
    .array-data 4
        0x101011f    # android.R.attr.maxWidth
        0x1010220    # android.R.attr.inputType
        0x1010264    # android.R.attr.imeOptions
        0x10102fa    # android.R.attr.iconifiedByDefault
        0x1010358    # android.R.attr.queryHint
    .end array-data

    :array_7f
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010002    # android.R.attr.icon
        0x1010150    # android.R.attr.hint
        0x10101d5    # android.R.attr.searchMode
        0x10101d6    # android.R.attr.searchSuggestAuthority
        0x10101d7    # android.R.attr.searchSuggestPath
        0x10101d8    # android.R.attr.searchSuggestSelection
        0x10101d9    # android.R.attr.searchSuggestIntentAction
        0x10101da    # android.R.attr.searchSuggestIntentData
        0x1010205    # android.R.attr.searchButtonText
        0x1010220    # android.R.attr.inputType
        0x1010252    # android.R.attr.voiceSearchMode
        0x1010253    # android.R.attr.voiceLanguageModel
        0x1010254    # android.R.attr.voicePromptText
        0x1010255    # android.R.attr.voiceLanguage
        0x1010256    # android.R.attr.voiceMaxResults
        0x1010264    # android.R.attr.imeOptions
        0x101026d    # android.R.attr.searchSuggestThreshold
        0x101026e    # android.R.attr.includeInGlobalSearch
        0x1010282    # android.R.attr.queryAfterZeroResults
        0x101028a    # android.R.attr.searchSettingsDescription
        0x101028c    # android.R.attr.autoUrlDetect
    .end array-data

    :array_80
    .array-data 4
        0x10100c5    # android.R.attr.keycode
        0x10101db    # android.R.attr.queryActionMsg
        0x10101dc    # android.R.attr.suggestActionMsg
        0x10101dd    # android.R.attr.suggestActionMsgColumn
    .end array-data

    :array_81
    .array-data 4
        0x1010142    # android.R.attr.thumb
        0x1010143    # android.R.attr.thumbOffset
    .end array-data

    :array_82
    .array-data 4
        0x1010311    # android.R.attr.actionModeCutDrawable
        0x1010312    # android.R.attr.actionModeCopyDrawable
        0x1010313    # android.R.attr.actionModePasteDrawable
        0x101037e    # android.R.attr.actionModeSelectAllDrawable
    .end array-data

    :array_83
    .array-data 4
        0x101011c    # android.R.attr.dither
        0x1010155    # android.R.attr.height
        0x1010159    # android.R.attr.width
        0x10101a5    # android.R.attr.color
    .end array-data

    :array_84
    .array-data 4
        0x10101ad    # android.R.attr.left
        0x10101ae    # android.R.attr.top
        0x10101af    # android.R.attr.right
        0x10101b0    # android.R.attr.bottom
    .end array-data

    :array_85
    .array-data 4
        0x1010438    # android.R.attr.layout_maxHeight
        0x1010439    # android.R.attr.layout_minHeight
    .end array-data

    :array_86
    .array-data 4
        0x1010438    # android.R.attr.layout_maxHeight
        0x1010480    # android.R.attr.layout_childType
    .end array-data

    :array_87
    .array-data 4
        0x10100c4    # android.R.attr.orientation
        0x1010257    # android.R.attr.bottomOffset
        0x1010258    # android.R.attr.topOffset
        0x1010259    # android.R.attr.allowSingleTap
        0x101025a    # android.R.attr.handle
        0x101025b    # android.R.attr.content
        0x101025c    # android.R.attr.animateOnClick
    .end array-data

    :array_88
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010225    # android.R.attr.settingsActivity
    .end array-data

    :array_89
    .array-data 4
        0x1010001    # android.R.attr.label
        0x1010399    # android.R.attr.subtypeLocale
        0x101039a    # android.R.attr.subtypeExtraValue
    .end array-data

    :array_8a
    .array-data 4
        0x10100af    # android.R.attr.gravity
        0x1010175    # android.R.attr.dropDownSelector
        0x1010176    # android.R.attr.popupBackground
        0x101017b    # android.R.attr.prompt
        0x1010262    # android.R.attr.dropDownWidth
        0x10102ac    # android.R.attr.dropDownHorizontalOffset
        0x10102ad    # android.R.attr.dropDownVerticalOffset
        0x10102f1    # android.R.attr.spinnerMode
        0x1010413    # android.R.attr.popupPromptView
        0x1010414    # android.R.attr.disableChildrenWhenDisabled
    .end array-data

    :array_8b
    .array-data 4
        0x1010410    # android.R.attr.resOutColor
        0x1010411    # android.R.attr.clickColor
    .end array-data

    :array_8c
    .array-data 4
        0x101011c    # android.R.attr.dither
        0x1010194    # android.R.attr.visible
        0x1010195    # android.R.attr.variablePadding
        0x1010196    # android.R.attr.constantSize
        0x101030c    # android.R.attr.enterFadeDuration
        0x101030d    # android.R.attr.exitFadeDuration
    .end array-data

    :array_8d
    .array-data 4
        0x1010470    # android.R.attr.mountPoint
        0x1010471    # android.R.attr.storageDescription
        0x1010472    # android.R.attr.primary
        0x1010473    # android.R.attr.removable
        0x1010474    # android.R.attr.emulated
        0x1010475    # android.R.attr.mtpReserve
        0x1010476    # android.R.attr.allowMassStorage
        0x1010477    # android.R.attr.maxFileSize
    .end array-data

    :array_8e
    .array-data 4
        0x10103cf    # android.R.attr.textUnderlineColor
        0x10103d0    # android.R.attr.textUnderlineThickness
    .end array-data

    :array_8f
    .array-data 4
        0x1010124    # android.R.attr.textOn
        0x1010125    # android.R.attr.textOff
        0x1010142    # android.R.attr.thumb
        0x101036e    # android.R.attr.switchTextAppearance
        0x101036f    # android.R.attr.track
        0x1010370    # android.R.attr.switchMinWidth
        0x1010371    # android.R.attr.switchPadding
        0x1010372    # android.R.attr.thumbTextPadding
    .end array-data

    :array_90
    .array-data 4
        0x10101ef    # android.R.attr.summaryOn
        0x10101f0    # android.R.attr.summaryOff
        0x10101f1    # android.R.attr.disableDependentsState
        0x101036b    # android.R.attr.switchTextOn
        0x101036c    # android.R.attr.switchTextOff
    .end array-data

    :array_91
    .array-data 4
        0x1010225    # android.R.attr.settingsActivity
        0x101028f    # android.R.attr.accountType
        0x1010290    # android.R.attr.contentAuthority
        0x1010291    # android.R.attr.userVisible
        0x101029b    # android.R.attr.supportsUploading
        0x1010332    # android.R.attr.allowParallelSyncs
        0x1010333    # android.R.attr.isAlwaysSyncable
    .end array-data

    :array_92
    .array-data 4
        0x1010129    # android.R.attr.divider
        0x10102bb    # android.R.attr.tabStripLeft
        0x10102bc    # android.R.attr.tabStripRight
        0x10102bd    # android.R.attr.tabStripEnabled
        0x1010412    # android.R.attr.tabLayout
    .end array-data

    :array_93
    .array-data 4
        0x1010149    # android.R.attr.stretchColumns
        0x101014a    # android.R.attr.shrinkColumns
        0x101014b    # android.R.attr.collapseColumns
    .end array-data

    :array_94
    .array-data 4
        0x101014c    # android.R.attr.layout_column
        0x101014d    # android.R.attr.layout_span
    .end array-data

    :array_95
    .array-data 4
        0x1010095    # android.R.attr.textSize
        0x1010096    # android.R.attr.typeface
        0x1010097    # android.R.attr.textStyle
        0x1010098    # android.R.attr.textColor
        0x1010099    # android.R.attr.textColorHighlight
        0x101009a    # android.R.attr.textColorHint
        0x101009b    # android.R.attr.textColorLink
        0x101038c    # android.R.attr.textAllCaps
        0x10103ac    # android.R.attr.fontFamily
    .end array-data

    :array_96
    .array-data 4
        0x10103ca    # android.R.attr.format12Hour
        0x10103cb    # android.R.attr.format24Hour
        0x10103cc    # android.R.attr.timeZone
    .end array-data

    :array_97
    .array-data 4
        0x101000e    # android.R.attr.enabled
        0x1010034    # android.R.attr.textAppearance
        0x1010095    # android.R.attr.textSize
        0x1010096    # android.R.attr.typeface
        0x1010097    # android.R.attr.textStyle
        0x1010098    # android.R.attr.textColor
        0x1010099    # android.R.attr.textColorHighlight
        0x101009a    # android.R.attr.textColorHint
        0x101009b    # android.R.attr.textColorLink
        0x10100ab    # android.R.attr.ellipsize
        0x10100af    # android.R.attr.gravity
        0x10100b0    # android.R.attr.autoLink
        0x10100b1    # android.R.attr.linksClickable
        0x101011f    # android.R.attr.maxWidth
        0x1010120    # android.R.attr.maxHeight
        0x101013f    # android.R.attr.minWidth
        0x1010140    # android.R.attr.minHeight
        0x101014e    # android.R.attr.bufferType
        0x101014f    # android.R.attr.text
        0x1010150    # android.R.attr.hint
        0x1010151    # android.R.attr.textScaleX
        0x1010152    # android.R.attr.cursorVisible
        0x1010153    # android.R.attr.maxLines
        0x1010154    # android.R.attr.lines
        0x1010155    # android.R.attr.height
        0x1010156    # android.R.attr.minLines
        0x1010157    # android.R.attr.maxEms
        0x1010158    # android.R.attr.ems
        0x1010159    # android.R.attr.width
        0x101015a    # android.R.attr.minEms
        0x101015b    # android.R.attr.scrollHorizontally
        0x101015c    # android.R.attr.password
        0x101015d    # android.R.attr.singleLine
        0x101015e    # android.R.attr.selectAllOnFocus
        0x101015f    # android.R.attr.includeFontPadding
        0x1010160    # android.R.attr.maxLength
        0x1010161    # android.R.attr.shadowColor
        0x1010162    # android.R.attr.shadowDx
        0x1010163    # android.R.attr.shadowDy
        0x1010164    # android.R.attr.shadowRadius
        0x1010165    # android.R.attr.numeric
        0x1010166    # android.R.attr.digits
        0x1010167    # android.R.attr.phoneNumber
        0x1010168    # android.R.attr.inputMethod
        0x1010169    # android.R.attr.capitalize
        0x101016a    # android.R.attr.autoText
        0x101016b    # android.R.attr.editable
        0x101016c    # android.R.attr.freezesText
        0x101016d    # android.R.attr.drawableTop
        0x101016e    # android.R.attr.drawableBottom
        0x101016f    # android.R.attr.drawableLeft
        0x1010170    # android.R.attr.drawableRight
        0x1010171    # android.R.attr.drawablePadding
        0x1010217    # android.R.attr.lineSpacingExtra
        0x1010218    # android.R.attr.lineSpacingMultiplier
        0x101021d    # android.R.attr.marqueeRepeatLimit
        0x1010220    # android.R.attr.inputType
        0x1010223    # android.R.attr.privateImeOptions
        0x1010224    # android.R.attr.editorExtras
        0x1010264    # android.R.attr.imeOptions
        0x1010265    # android.R.attr.imeActionLabel
        0x1010266    # android.R.attr.imeActionId
        0x10102c5    # android.R.attr.textSelectHandleLeft
        0x10102c6    # android.R.attr.textSelectHandleRight
        0x10102c7    # android.R.attr.textSelectHandle
        0x1010314    # android.R.attr.textEditPasteWindowLayout
        0x1010315    # android.R.attr.textEditNoPasteWindowLayout
        0x1010316    # android.R.attr.textIsSelectable
        0x101035e    # android.R.attr.textEditSidePasteWindowLayout
        0x101035f    # android.R.attr.textEditSideNoPasteWindowLayout
        0x1010362    # android.R.attr.textCursorDrawable
        0x1010374    # android.R.attr.textEditSuggestionItemLayout
        0x101038c    # android.R.attr.textAllCaps
        0x1010392    # android.R.attr.drawableStart
        0x1010393    # android.R.attr.drawableEnd
        0x10103ac    # android.R.attr.fontFamily
    .end array-data

    :array_98
    .array-data 4
        0x1010030    # android.R.attr.colorForeground
        0x1010031    # android.R.attr.colorBackground
        0x1010032    # android.R.attr.backgroundDimAmount
        0x1010033    # android.R.attr.disabledAlpha
        0x1010034    # android.R.attr.textAppearance
        0x1010035    # android.R.attr.textAppearanceInverse
        0x1010036    # android.R.attr.textColorPrimary
        0x1010037    # android.R.attr.textColorPrimaryDisableOnly
        0x1010038    # android.R.attr.textColorSecondary
        0x1010039    # android.R.attr.textColorPrimaryInverse
        0x101003a    # android.R.attr.textColorSecondaryInverse
        0x101003b    # android.R.attr.textColorPrimaryNoDisable
        0x101003c    # android.R.attr.textColorSecondaryNoDisable
        0x101003d    # android.R.attr.textColorPrimaryInverseNoDisable
        0x101003e    # android.R.attr.textColorSecondaryInverseNoDisable
        0x101003f    # android.R.attr.textColorHintInverse
        0x1010040    # android.R.attr.textAppearanceLarge
        0x1010041    # android.R.attr.textAppearanceMedium
        0x1010042    # android.R.attr.textAppearanceSmall
        0x1010043    # android.R.attr.textAppearanceLargeInverse
        0x1010044    # android.R.attr.textAppearanceMediumInverse
        0x1010045    # android.R.attr.textAppearanceSmallInverse
        0x1010046    # android.R.attr.textCheckMark
        0x1010047    # android.R.attr.textCheckMarkInverse
        0x1010048    # android.R.attr.buttonStyle
        0x1010049    # android.R.attr.buttonStyleSmall
        0x101004a    # android.R.attr.buttonStyleInset
        0x101004b    # android.R.attr.buttonStyleToggle
        0x101004c    # android.R.attr.galleryItemBackground
        0x101004d    # android.R.attr.listPreferredItemHeight
        0x101004e    # android.R.attr.expandableListPreferredItemPaddingLeft
        0x101004f    # android.R.attr.expandableListPreferredChildPaddingLeft
        0x1010050    # android.R.attr.expandableListPreferredItemIndicatorLeft
        0x1010051    # android.R.attr.expandableListPreferredItemIndicatorRight
        0x1010052    # android.R.attr.expandableListPreferredChildIndicatorLeft
        0x1010053    # android.R.attr.expandableListPreferredChildIndicatorRight
        0x1010054    # android.R.attr.windowBackground
        0x1010055    # android.R.attr.windowFrame
        0x1010056    # android.R.attr.windowNoTitle
        0x1010057    # android.R.attr.windowIsFloating
        0x1010058    # android.R.attr.windowIsTranslucent
        0x1010059    # android.R.attr.windowContentOverlay
        0x101005a    # android.R.attr.windowTitleSize
        0x101005b    # android.R.attr.windowTitleStyle
        0x101005c    # android.R.attr.windowTitleBackgroundStyle
        0x101005d    # android.R.attr.alertDialogStyle
        0x101005e    # android.R.attr.panelBackground
        0x101005f    # android.R.attr.panelFullBackground
        0x1010060    # android.R.attr.panelColorForeground
        0x1010061    # android.R.attr.panelColorBackground
        0x1010062    # android.R.attr.panelTextAppearance
        0x101006a    # android.R.attr.absListViewStyle
        0x101006b    # android.R.attr.autoCompleteTextViewStyle
        0x101006c    # android.R.attr.checkboxStyle
        0x101006d    # android.R.attr.dropDownListViewStyle
        0x101006e    # android.R.attr.editTextStyle
        0x101006f    # android.R.attr.expandableListViewStyle
        0x1010070    # android.R.attr.galleryStyle
        0x1010071    # android.R.attr.gridViewStyle
        0x1010072    # android.R.attr.imageButtonStyle
        0x1010073    # android.R.attr.imageWellStyle
        0x1010074    # android.R.attr.listViewStyle
        0x1010075    # android.R.attr.listViewWhiteStyle
        0x1010076    # android.R.attr.popupWindowStyle
        0x1010077    # android.R.attr.progressBarStyle
        0x1010078    # android.R.attr.progressBarStyleHorizontal
        0x1010079    # android.R.attr.progressBarStyleSmall
        0x101007a    # android.R.attr.progressBarStyleLarge
        0x101007b    # android.R.attr.seekBarStyle
        0x101007c    # android.R.attr.ratingBarStyle
        0x101007d    # android.R.attr.ratingBarStyleSmall
        0x101007e    # android.R.attr.radioButtonStyle
        0x1010080    # android.R.attr.scrollViewStyle
        0x1010081    # android.R.attr.spinnerStyle
        0x1010082    # android.R.attr.starStyle
        0x1010083    # android.R.attr.tabWidgetStyle
        0x1010084    # android.R.attr.textViewStyle
        0x1010085    # android.R.attr.webViewStyle
        0x1010086    # android.R.attr.dropDownItemStyle
        0x1010087    # android.R.attr.spinnerDropDownItemStyle
        0x1010088    # android.R.attr.dropDownHintAppearance
        0x1010089    # android.R.attr.spinnerItemStyle
        0x101008a    # android.R.attr.mapViewStyle
        0x101008b    # android.R.attr.preferenceScreenStyle
        0x101008c    # android.R.attr.preferenceCategoryStyle
        0x101008d    # android.R.attr.preferenceInformationStyle
        0x101008e    # android.R.attr.preferenceStyle
        0x101008f    # android.R.attr.checkBoxPreferenceStyle
        0x1010090    # android.R.attr.yesNoPreferenceStyle
        0x1010091    # android.R.attr.dialogPreferenceStyle
        0x1010092    # android.R.attr.editTextPreferenceStyle
        0x1010093    # android.R.attr.ringtonePreferenceStyle
        0x1010094    # android.R.attr.preferenceLayoutChild
        0x10100ae    # android.R.attr.windowAnimationStyle
        0x1010206    # android.R.attr.colorForegroundInverse
        0x1010207    # android.R.attr.textAppearanceButton
        0x1010208    # android.R.attr.listSeparatorTextViewStyle
        0x101020d    # android.R.attr.windowFullscreen
        0x101020f    # android.R.attr.progressBarStyleSmallTitle
        0x1010210    # android.R.attr.ratingBarStyleIndicator
        0x1010212    # android.R.attr.textColorTertiary
        0x1010213    # android.R.attr.textColorTertiaryInverse
        0x1010214    # android.R.attr.listDivider
        0x1010219    # android.R.attr.listChoiceIndicatorSingle
        0x101021a    # android.R.attr.listChoiceIndicatorMultiple
        0x101021e    # android.R.attr.windowNoDisplay
        0x101021f    # android.R.attr.backgroundDimEnabled
        0x1010222    # android.R.attr.windowDisablePreview
        0x101022b    # android.R.attr.windowSoftInputMode
        0x1010230    # android.R.attr.candidatesTextStyleSpans
        0x1010267    # android.R.attr.textColorSearchUrl
        0x1010287    # android.R.attr.progressBarStyleInverse
        0x1010288    # android.R.attr.progressBarStyleSmallInverse
        0x1010289    # android.R.attr.progressBarStyleLargeInverse
        0x101028b    # android.R.attr.textColorPrimaryInverseDisableOnly
        0x1010292    # android.R.attr.windowShowWallpaper
        0x10102a0    # android.R.attr.textAppearanceSearchResultSubtitle
        0x10102a1    # android.R.attr.textAppearanceSearchResultTitle
        0x10102ab    # android.R.attr.colorBackgroundCacheHint
        0x10102ae    # android.R.attr.quickContactBadgeStyleWindowSmall
        0x10102af    # android.R.attr.quickContactBadgeStyleWindowMedium
        0x10102b0    # android.R.attr.quickContactBadgeStyleWindowLarge
        0x10102b1    # android.R.attr.quickContactBadgeStyleSmallWindowSmall
        0x10102b2    # android.R.attr.quickContactBadgeStyleSmallWindowMedium
        0x10102b3    # android.R.attr.quickContactBadgeStyleSmallWindowLarge
        0x10102b6    # android.R.attr.expandableListViewWhiteStyle
        0x10102b9    # android.R.attr.webTextViewStyle
        0x10102c5    # android.R.attr.textSelectHandleLeft
        0x10102c6    # android.R.attr.textSelectHandleRight
        0x10102c7    # android.R.attr.textSelectHandle
        0x10102c8    # android.R.attr.textSelectHandleWindowStyle
        0x10102cd    # android.R.attr.windowActionBar
        0x10102ce    # android.R.attr.actionBarStyle
        0x10102d6    # android.R.attr.dropDownSpinnerStyle
        0x10102d7    # android.R.attr.actionDropDownStyle
        0x10102d8    # android.R.attr.actionButtonStyle
        0x10102db    # android.R.attr.actionModeBackground
        0x10102dc    # android.R.attr.actionModeCloseDrawable
        0x10102dd    # android.R.attr.windowActionModeOverlay
        0x10102e4    # android.R.attr.windowActionBarOverlay
        0x10102eb    # android.R.attr.actionBarSize
        0x10102f0    # android.R.attr.listChoiceBackgroundIndicator
        0x10102f3    # android.R.attr.actionBarTabStyle
        0x10102f4    # android.R.attr.actionBarTabBarStyle
        0x10102f5    # android.R.attr.actionBarTabTextStyle
        0x10102f6    # android.R.attr.actionOverflowButtonStyle
        0x10102f7    # android.R.attr.actionModeCloseButtonStyle
        0x10102fd    # android.R.attr.activatedBackgroundIndicator
        0x10102ff    # android.R.attr.listPopupWindowStyle
        0x1010300    # android.R.attr.popupMenuStyle
        0x1010301    # android.R.attr.textAppearanceLargePopupMenu
        0x1010302    # android.R.attr.textAppearanceSmallPopupMenu
        0x1010305    # android.R.attr.listDividerAlertDialog
        0x1010306    # android.R.attr.textColorAlertDialogListItem
        0x1010308    # android.R.attr.dialogTheme
        0x1010309    # android.R.attr.alertDialogTheme
        0x101030a    # android.R.attr.dividerVertical
        0x101030b    # android.R.attr.homeAsUpIndicator
        0x101030e    # android.R.attr.selectableItemBackground
        0x1010311    # android.R.attr.actionModeCutDrawable
        0x1010312    # android.R.attr.actionModeCopyDrawable
        0x1010313    # android.R.attr.actionModePasteDrawable
        0x1010314    # android.R.attr.textEditPasteWindowLayout
        0x1010315    # android.R.attr.textEditNoPasteWindowLayout
        0x1010317    # android.R.attr.windowEnableSplitTouch
        0x101032b    # android.R.attr.borderlessButtonStyle
        0x101032c    # android.R.attr.dividerHorizontal
        0x101032e    # android.R.attr.buttonBarStyle
        0x101032f    # android.R.attr.buttonBarButtonStyle
        0x1010330    # android.R.attr.segmentedButtonStyle
        0x1010336    # android.R.attr.fastScrollThumbDrawable
        0x1010337    # android.R.attr.fastScrollPreviewBackgroundLeft
        0x1010338    # android.R.attr.fastScrollPreviewBackgroundRight
        0x1010339    # android.R.attr.fastScrollTrackDrawable
        0x101033a    # android.R.attr.fastScrollOverlayPosition
        0x101034e    # android.R.attr.detailsElementBackground
        0x101034f    # android.R.attr.textColorHighlightInverse
        0x1010350    # android.R.attr.textColorLinkInverse
        0x1010351    # android.R.attr.editTextColor
        0x1010352    # android.R.attr.editTextBackground
        0x1010353    # android.R.attr.horizontalScrollViewStyle
        0x1010355    # android.R.attr.alertDialogIcon
        0x1010359    # android.R.attr.fastScrollTextColor
        0x101035b    # android.R.attr.windowCloseOnTouchOutside
        0x101035c    # android.R.attr.datePickerStyle
        0x101035d    # android.R.attr.calendarViewStyle
        0x101035e    # android.R.attr.textEditSidePasteWindowLayout
        0x101035f    # android.R.attr.textEditSideNoPasteWindowLayout
        0x1010360    # android.R.attr.actionMenuTextAppearance
        0x1010361    # android.R.attr.actionMenuTextColor
        0x101036d    # android.R.attr.switchPreferenceStyle
        0x1010373    # android.R.attr.textSuggestionsWindowStyle
        0x1010374    # android.R.attr.textEditSuggestionItemLayout
        0x101037e    # android.R.attr.actionModeSelectAllDrawable
        0x1010386    # android.R.attr.listPreferredItemHeightLarge
        0x1010387    # android.R.attr.listPreferredItemHeightSmall
        0x1010388    # android.R.attr.actionBarSplitStyle
        0x101038d    # android.R.attr.colorPressedHighlight
        0x101038e    # android.R.attr.colorLongPressedHighlight
        0x101038f    # android.R.attr.colorFocusedHighlight
        0x1010390    # android.R.attr.colorActivatedHighlight
        0x1010391    # android.R.attr.colorMultiSelectHighlight
        0x1010394    # android.R.attr.actionModeStyle
        0x1010397    # android.R.attr.actionBarWidgetTheme
        0x101039b    # android.R.attr.actionBarDivider
        0x101039c    # android.R.attr.actionBarItemBackground
        0x101039d    # android.R.attr.actionModeSplitBackground
        0x101039e    # android.R.attr.textAppearanceListItem
        0x101039f    # android.R.attr.textAppearanceListItemSmall
        0x10103a3    # android.R.attr.listPreferredItemPaddingLeft
        0x10103a4    # android.R.attr.listPreferredItemPaddingRight
        0x10103a8    # android.R.attr.searchWidgetCorpusItemBackground
        0x10103ad    # android.R.attr.mediaRouteButtonStyle
        0x10103bd    # android.R.attr.listPreferredItemPaddingStart
        0x10103be    # android.R.attr.listPreferredItemPaddingEnd
        0x10103c0    # android.R.attr.presentationTheme
        0x10103c3    # android.R.attr.textAppearanceEasyCorrectSuggestion
        0x10103c8    # android.R.attr.checkedTextViewStyle
        0x10103cd    # android.R.attr.textAppearanceMisspelledSuggestion
        0x10103ce    # android.R.attr.textAppearanceAutoCorrectionSuggestion
        0x10103cf    # android.R.attr.textUnderlineColor
        0x10103d0    # android.R.attr.textUnderlineThickness
        0x10103d1    # android.R.attr.errorMessageBackground
        0x10103d2    # android.R.attr.errorMessageAboveBackground
        0x10103d3    # android.R.attr.searchResultListItemHeight
        0x10103d4    # android.R.attr.dropdownListPreferredItemHeight
        0x10103d5    # android.R.attr.windowSplitActionBar
        0x10103d6    # android.R.attr.alertDialogButtonGroupStyle
        0x10103d7    # android.R.attr.alertDialogCenterButtons
        0x10103d8    # android.R.attr.panelMenuIsCompact
        0x10103d9    # android.R.attr.panelMenuListWidth
        0x10103da    # android.R.attr.panelMenuListTheme
        0x10103db    # android.R.attr.gestureOverlayViewStyle
        0x10103dc    # android.R.attr.quickContactBadgeOverlay
        0x10103dd    # android.R.attr.stackViewStyle
        0x10103de    # android.R.attr.numberPickerStyle
        0x10103df    # android.R.attr.timePickerStyle
        0x10103e0    # android.R.attr.activityChooserViewStyle
        0x10103e1    # android.R.attr.actionModeShareDrawable
        0x10103e2    # android.R.attr.actionModeFindDrawable
        0x10103e3    # android.R.attr.actionModeWebSearchDrawable
        0x10103e4    # android.R.attr.actionModePopupWindowStyle
        0x10103e5    # android.R.attr.preferenceFragmentStyle
        0x10103e6    # android.R.attr.preferencePanelStyle
        0x10103e7    # android.R.attr.dialogTitleIconsDecorLayout
        0x10103e8    # android.R.attr.dialogCustomTitleDecorLayout
        0x10103e9    # android.R.attr.dialogTitleDecorLayout
        0x10103ea    # android.R.attr.toastFrameBackground
        0x10103eb    # android.R.attr.searchDropdownBackground
        0x10103ec    # android.R.attr.searchViewCloseIcon
        0x10103ed    # android.R.attr.searchViewGoIcon
        0x10103ee    # android.R.attr.searchViewSearchIcon
        0x10103ef    # android.R.attr.searchViewVoiceIcon
        0x10103f0    # android.R.attr.searchViewEditQuery
        0x10103f1    # android.R.attr.searchViewEditQueryBackground
        0x10103f2    # android.R.attr.searchViewTextField
        0x10103f3    # android.R.attr.searchViewTextFieldRight
        0x10103f4    # android.R.attr.searchDialogTheme
        0x10103f5    # android.R.attr.preferenceFrameLayoutStyle
        0x10103f6    # android.R.attr.switchStyle
        0x10103f7    # android.R.attr.pointerStyle
        0x10103f8    # android.R.attr.accessibilityFocusedDrawable
        0x10103f9    # android.R.attr.findOnPageNextDrawable
        0x10103fa    # android.R.attr.findOnPagePreviousDrawable
    .end array-data

    :array_99
    .array-data 4
        0x1010033    # android.R.attr.disabledAlpha
        0x1010124    # android.R.attr.textOn
        0x1010125    # android.R.attr.textOff
    .end array-data

    :array_9a
    .array-data 4
        0x10101c6    # android.R.attr.fromXDelta
        0x10101c7    # android.R.attr.toXDelta
        0x10101c8    # android.R.attr.fromYDelta
        0x10101c9    # android.R.attr.toYDelta
    .end array-data

    :array_9b
    .array-data 4
        0x1010063    # android.R.attr.scrollbarSize
        0x1010064    # android.R.attr.scrollbarThumbHorizontal
        0x1010065    # android.R.attr.scrollbarThumbVertical
        0x1010066    # android.R.attr.scrollbarTrackHorizontal
        0x1010067    # android.R.attr.scrollbarTrackVertical
        0x1010068    # android.R.attr.scrollbarAlwaysDrawHorizontalTrack
        0x1010069    # android.R.attr.scrollbarAlwaysDrawVerticalTrack
        0x101007f    # android.R.attr.scrollbarStyle
        0x10100d0    # android.R.attr.id
        0x10100d1    # android.R.attr.tag
        0x10100d2    # android.R.attr.scrollX
        0x10100d3    # android.R.attr.scrollY
        0x10100d4    # android.R.attr.background
        0x10100d5    # android.R.attr.padding
        0x10100d6    # android.R.attr.paddingLeft
        0x10100d7    # android.R.attr.paddingTop
        0x10100d8    # android.R.attr.paddingRight
        0x10100d9    # android.R.attr.paddingBottom
        0x10100da    # android.R.attr.focusable
        0x10100db    # android.R.attr.focusableInTouchMode
        0x10100dc    # android.R.attr.visibility
        0x10100dd    # android.R.attr.fitsSystemWindows
        0x10100de    # android.R.attr.scrollbars
        0x10100df    # android.R.attr.fadingEdge
        0x10100e0    # android.R.attr.fadingEdgeLength
        0x10100e1    # android.R.attr.nextFocusLeft
        0x10100e2    # android.R.attr.nextFocusRight
        0x10100e3    # android.R.attr.nextFocusUp
        0x10100e4    # android.R.attr.nextFocusDown
        0x10100e5    # android.R.attr.clickable
        0x10100e6    # android.R.attr.longClickable
        0x10100e7    # android.R.attr.saveEnabled
        0x10100e8    # android.R.attr.drawingCacheQuality
        0x10100e9    # android.R.attr.duplicateParentState
        0x101013f    # android.R.attr.minWidth
        0x1010140    # android.R.attr.minHeight
        0x1010215    # android.R.attr.soundEffectsEnabled
        0x1010216    # android.R.attr.keepScreenOn
        0x101024e    # android.R.attr.isScrollContainer
        0x101025e    # android.R.attr.hapticFeedbackEnabled
        0x101026f    # android.R.attr.onClick
        0x1010273    # android.R.attr.contentDescription
        0x10102a8    # android.R.attr.scrollbarFadeDuration
        0x10102a9    # android.R.attr.scrollbarDefaultDelayBeforeFade
        0x10102aa    # android.R.attr.fadeScrollbars
        0x10102c1    # android.R.attr.overScrollMode
        0x10102c4    # android.R.attr.filterTouchesWhenObscured
        0x101031f    # android.R.attr.alpha
        0x1010320    # android.R.attr.transformPivotX
        0x1010321    # android.R.attr.transformPivotY
        0x1010322    # android.R.attr.translationX
        0x1010323    # android.R.attr.translationY
        0x1010324    # android.R.attr.scaleX
        0x1010325    # android.R.attr.scaleY
        0x1010326    # android.R.attr.rotation
        0x1010327    # android.R.attr.rotationX
        0x1010328    # android.R.attr.rotationY
        0x1010334    # android.R.attr.verticalScrollbarPosition
        0x101033c    # android.R.attr.nextFocusForward
        0x1010354    # android.R.attr.layerType
        0x10103a5    # android.R.attr.requiresFadingEdge
        0x10103aa    # android.R.attr.importantForAccessibility
        0x10103b0    # android.R.attr.textDirection
        0x10103b1    # android.R.attr.textAlignment
        0x10103b2    # android.R.attr.layoutDirection
        0x10103b3    # android.R.attr.paddingStart
        0x10103b4    # android.R.attr.paddingEnd
        0x10103c6    # android.R.attr.labelFor
    .end array-data

    :array_9c
    .array-data 4
        0x1010177    # android.R.attr.inAnimation
        0x1010178    # android.R.attr.outAnimation
        0x10102d5    # android.R.attr.animateFirstView
    .end array-data

    :array_9d
    .array-data 4
        0x101009c    # android.R.attr.state_focused
        0x101009d    # android.R.attr.state_window_focused
        0x101009e    # android.R.attr.state_enabled
        0x10100a1    # android.R.attr.state_selected
        0x10100a7    # android.R.attr.state_pressed
        0x10102fe    # android.R.attr.state_activated
        0x101031b    # android.R.attr.state_accelerated
        0x1010367    # android.R.attr.state_hovered
        0x1010368    # android.R.attr.state_drag_can_accept
        0x1010369    # android.R.attr.state_drag_hovered
    .end array-data

    :array_9e
    .array-data 4
        0x1010179    # android.R.attr.flipInterval
        0x10102b5    # android.R.attr.autoStart
    .end array-data

    :array_9f
    .array-data 4
        0x10100ea    # android.R.attr.clipChildren
        0x10100eb    # android.R.attr.clipToPadding
        0x10100ec    # android.R.attr.layoutAnimation
        0x10100ed    # android.R.attr.animationCache
        0x10100ee    # android.R.attr.persistentDrawingCache
        0x10100ef    # android.R.attr.alwaysDrawnWithCache
        0x10100f0    # android.R.attr.addStatesFromChildren
        0x10100f1    # android.R.attr.descendantFocusability
        0x10102ef    # android.R.attr.splitMotionEvents
        0x10102f2    # android.R.attr.animateLayoutChanges
    .end array-data

    :array_a0
    .array-data 4
        0x10100f4    # android.R.attr.layout_width
        0x10100f5    # android.R.attr.layout_height
    .end array-data

    :array_a1
    .array-data 4
        0x10100f4    # android.R.attr.layout_width
        0x10100f5    # android.R.attr.layout_height
        0x10100f6    # android.R.attr.layout_margin
        0x10100f7    # android.R.attr.layout_marginLeft
        0x10100f8    # android.R.attr.layout_marginTop
        0x10100f9    # android.R.attr.layout_marginRight
        0x10100fa    # android.R.attr.layout_marginBottom
        0x10103b5    # android.R.attr.layout_marginStart
        0x10103b6    # android.R.attr.layout_marginEnd
    .end array-data

    :array_a2
    .array-data 4
        0x10100f2    # android.R.attr.layout
        0x10100f3    # android.R.attr.inflatedId
    .end array-data

    :array_a3
    .array-data 4
        0x1010020    # android.R.attr.description
        0x1010225    # android.R.attr.settingsActivity
        0x10102a5    # android.R.attr.thumbnail
        0x10102b4    # android.R.attr.author
    .end array-data

    :array_a4
    .array-data 4
        0x1010417    # android.R.attr.majorWeightMin
        0x1010418    # android.R.attr.minorWeightMin
        0x1010419    # android.R.attr.majorWeightMax
        0x101041a    # android.R.attr.minorWeightMax
    .end array-data

    :array_a5
    .array-data 4
        0x1010032    # android.R.attr.backgroundDimAmount
        0x1010054    # android.R.attr.windowBackground
        0x1010055    # android.R.attr.windowFrame
        0x1010056    # android.R.attr.windowNoTitle
        0x1010057    # android.R.attr.windowIsFloating
        0x1010058    # android.R.attr.windowIsTranslucent
        0x1010059    # android.R.attr.windowContentOverlay
        0x1010098    # android.R.attr.textColor
        0x10100ae    # android.R.attr.windowAnimationStyle
        0x101020d    # android.R.attr.windowFullscreen
        0x101021e    # android.R.attr.windowNoDisplay
        0x101021f    # android.R.attr.backgroundDimEnabled
        0x1010222    # android.R.attr.windowDisablePreview
        0x101022b    # android.R.attr.windowSoftInputMode
        0x1010292    # android.R.attr.windowShowWallpaper
        0x10102cd    # android.R.attr.windowActionBar
        0x10102dd    # android.R.attr.windowActionModeOverlay
        0x10102e4    # android.R.attr.windowActionBarOverlay
        0x1010317    # android.R.attr.windowEnableSplitTouch
        0x1010356    # android.R.attr.windowMinWidthMajor
        0x1010357    # android.R.attr.windowMinWidthMinor
        0x101035b    # android.R.attr.windowCloseOnTouchOutside
        0x10103d5    # android.R.attr.windowSplitActionBar
        0x10103fb    # android.R.attr.windowFixedWidthMajor
        0x10103fc    # android.R.attr.windowFixedHeightMinor
        0x10103fd    # android.R.attr.windowFixedWidthMinor
        0x10103fe    # android.R.attr.windowFixedHeightMajor
    .end array-data

    :array_a6
    .array-data 4
        0x10100b4    # android.R.attr.windowEnterAnimation
        0x10100b5    # android.R.attr.windowExitAnimation
        0x10100b6    # android.R.attr.windowShowAnimation
        0x10100b7    # android.R.attr.windowHideAnimation
        0x10100b8    # android.R.attr.activityOpenEnterAnimation
        0x10100b9    # android.R.attr.activityOpenExitAnimation
        0x10100ba    # android.R.attr.activityCloseEnterAnimation
        0x10100bb    # android.R.attr.activityCloseExitAnimation
        0x10100bc    # android.R.attr.taskOpenEnterAnimation
        0x10100bd    # android.R.attr.taskOpenExitAnimation
        0x10100be    # android.R.attr.taskCloseEnterAnimation
        0x10100bf    # android.R.attr.taskCloseExitAnimation
        0x10100c0    # android.R.attr.taskToFrontEnterAnimation
        0x10100c1    # android.R.attr.taskToFrontExitAnimation
        0x10100c2    # android.R.attr.taskToBackEnterAnimation
        0x10100c3    # android.R.attr.taskToBackExitAnimation
        0x1010293    # android.R.attr.wallpaperOpenEnterAnimation
        0x1010294    # android.R.attr.wallpaperOpenExitAnimation
        0x1010295    # android.R.attr.wallpaperCloseEnterAnimation
        0x1010296    # android.R.attr.wallpaperCloseExitAnimation
        0x1010297    # android.R.attr.wallpaperIntraOpenEnterAnimation
        0x1010298    # android.R.attr.wallpaperIntraOpenExitAnimation
        0x1010299    # android.R.attr.wallpaperIntraCloseEnterAnimation
        0x101029a    # android.R.attr.wallpaperIntraCloseExitAnimation
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
