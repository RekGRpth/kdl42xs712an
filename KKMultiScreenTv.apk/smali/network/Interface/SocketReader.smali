.class public Lnetwork/Interface/SocketReader;
.super Ljava/lang/Object;
.source "SocketReader.java"


# instance fields
.field private m_bytesRead:J

.field private final m_nioService:Lnetwork/NetCore/asyncService;

.field private m_previousBytes:Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(Lnetwork/NetCore/asyncService;)V
    .locals 2
    .param p1    # Lnetwork/NetCore/asyncService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/Interface/SocketReader;->m_nioService:Lnetwork/NetCore/asyncService;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnetwork/Interface/SocketReader;->m_bytesRead:J

    return-void
.end method


# virtual methods
.method public compact()V
    .locals 2

    invoke-virtual {p0}, Lnetwork/Interface/SocketReader;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    if-lez v1, :cond_0

    invoke-static {v0}, Lnetwork/util/NIOUtils;->copy(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v1

    iput-object v1, p0, Lnetwork/Interface/SocketReader;->m_previousBytes:Ljava/nio/ByteBuffer;

    :cond_0
    return-void
.end method

.method public getBuffer()Ljava/nio/ByteBuffer;
    .locals 1

    iget-object v0, p0, Lnetwork/Interface/SocketReader;->m_nioService:Lnetwork/NetCore/asyncService;

    invoke-virtual {v0}, Lnetwork/NetCore/asyncService;->getSharedBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    return-object v0
.end method

.method public getBytesRead()J
    .locals 2

    iget-wide v0, p0, Lnetwork/Interface/SocketReader;->m_bytesRead:J

    return-wide v0
.end method

.method public read(Ljava/nio/channels/SocketChannel;)I
    .locals 8
    .param p1    # Ljava/nio/channels/SocketChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x0

    invoke-virtual {p0}, Lnetwork/Interface/SocketReader;->getBuffer()Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    iget-object v4, p0, Lnetwork/Interface/SocketReader;->m_previousBytes:Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_0

    iget-object v4, p0, Lnetwork/Interface/SocketReader;->m_previousBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v4

    invoke-virtual {v0, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    :cond_0
    invoke-virtual {p1, v0}, Ljava/nio/channels/SocketChannel;->read(Ljava/nio/ByteBuffer;)I

    move-result v2

    if-gez v2, :cond_1

    new-instance v3, Ljava/io/EOFException;

    const-string v4, "Buffer read -1"

    invoke-direct {v3, v4}, Ljava/io/EOFException;-><init>(Ljava/lang/String;)V

    throw v3

    :cond_1
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-nez v4, :cond_2

    new-instance v3, Ljava/nio/BufferOverflowException;

    invoke-direct {v3}, Ljava/nio/BufferOverflowException;-><init>()V

    throw v3

    :cond_2
    iget-wide v4, p0, Lnetwork/Interface/SocketReader;->m_bytesRead:J

    int-to-long v6, v2

    add-long/2addr v4, v6

    iput-wide v4, p0, Lnetwork/Interface/SocketReader;->m_bytesRead:J

    if-nez v2, :cond_3

    move v2, v3

    :goto_0
    return v2

    :cond_3
    iget-object v4, p0, Lnetwork/Interface/SocketReader;->m_previousBytes:Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_4

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v3, p0, Lnetwork/Interface/SocketReader;->m_previousBytes:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    const/4 v3, 0x0

    iput-object v3, p0, Lnetwork/Interface/SocketReader;->m_previousBytes:Ljava/nio/ByteBuffer;

    :cond_4
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    goto :goto_0
.end method
