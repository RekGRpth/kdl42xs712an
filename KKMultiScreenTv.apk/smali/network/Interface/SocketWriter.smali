.class public Lnetwork/Interface/SocketWriter;
.super Ljava/lang/Object;
.source "SocketWriter.java"


# instance fields
.field private m_bytesWritten:J

.field private m_currentBuffer:I

.field private m_packetWriter:Lnetwork/Interface/PacketWriter;

.field private m_tag:Ljava/lang/Object;

.field private m_writeBuffers:[Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>()V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lnetwork/Interface/SocketWriter;->m_bytesWritten:J

    const/4 v0, 0x0

    iput-object v0, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    sget-object v0, Lnetwork/NetIO/packetwriter/RawPacketWriter;->INSTANCE:Lnetwork/NetIO/packetwriter/RawPacketWriter;

    iput-object v0, p0, Lnetwork/Interface/SocketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    return-void
.end method


# virtual methods
.method public getBytesWritten()J
    .locals 2

    iget-wide v0, p0, Lnetwork/Interface/SocketWriter;->m_bytesWritten:J

    return-wide v0
.end method

.method public getPacketWriter()Lnetwork/Interface/PacketWriter;
    .locals 1

    iget-object v0, p0, Lnetwork/Interface/SocketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lnetwork/Interface/SocketWriter;->m_tag:Ljava/lang/Object;

    return-object v0
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setPacket([BLjava/lang/Object;)V
    .locals 4
    .param p1    # [B
    .param p2    # Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p0}, Lnetwork/Interface/SocketWriter;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "This method should only called when m_writeBuffers == null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lnetwork/Interface/SocketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/nio/ByteBuffer;

    invoke-static {p1}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    aput-object v2, v1, v3

    invoke-interface {v0, v1}, Lnetwork/Interface/PacketWriter;->write([Ljava/nio/ByteBuffer;)[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    iput v3, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    iput-object p2, p0, Lnetwork/Interface/SocketWriter;->m_tag:Ljava/lang/Object;

    return-void
.end method

.method public setPacketWriter(Lnetwork/Interface/PacketWriter;)V
    .locals 0
    .param p1    # Lnetwork/Interface/PacketWriter;

    iput-object p1, p0, Lnetwork/Interface/SocketWriter;->m_packetWriter:Lnetwork/Interface/PacketWriter;

    return-void
.end method

.method public write(Ljava/nio/channels/SocketChannel;)Z
    .locals 9
    .param p1    # Ljava/nio/channels/SocketChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v3, 0x1

    const/4 v8, 0x0

    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_0

    iget v4, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    iget-object v5, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    if-ne v4, v5, :cond_2

    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    iget v5, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-nez v4, :cond_2

    :cond_0
    iput-object v8, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    :cond_1
    :goto_0
    return v3

    :cond_2
    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    iget v5, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    iget-object v6, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    array-length v6, v6

    iget v7, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    sub-int/2addr v6, v7

    invoke-virtual {p1, v4, v5, v6}, Ljava/nio/channels/SocketChannel;->write([Ljava/nio/ByteBuffer;II)J

    move-result-wide v1

    const-wide/16 v4, 0x0

    cmp-long v4, v1, v4

    if-nez v4, :cond_3

    const/4 v3, 0x0

    goto :goto_0

    :cond_3
    iget-wide v4, p0, Lnetwork/Interface/SocketWriter;->m_bytesWritten:J

    add-long/2addr v4, v1

    iput-wide v4, p0, Lnetwork/Interface/SocketWriter;->m_bytesWritten:J

    iget v0, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    :goto_1
    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    array-length v4, v4

    if-lt v0, v4, :cond_4

    :goto_2
    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    iget v5, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    aget-object v4, v4, v5

    if-nez v4, :cond_1

    iput-object v8, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    goto :goto_0

    :cond_4
    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    aget-object v4, v4, v0

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v4

    if-eqz v4, :cond_5

    iput v0, p0, Lnetwork/Interface/SocketWriter;->m_currentBuffer:I

    goto :goto_2

    :cond_5
    iget-object v4, p0, Lnetwork/Interface/SocketWriter;->m_writeBuffers:[Ljava/nio/ByteBuffer;

    aput-object v8, v4, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method
