.class public interface abstract Lnetwork/Interface/PacketReader;
.super Ljava/lang/Object;
.source "PacketReader.java"


# static fields
.field public static final SKIP_PACKET:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    new-array v0, v0, [B

    sput-object v0, Lnetwork/Interface/PacketReader;->SKIP_PACKET:[B

    return-void
.end method


# virtual methods
.method public abstract nextPacket(Ljava/nio/ByteBuffer;)[B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lnetwork/exception/ProtocolViolationException;
        }
    .end annotation
.end method
