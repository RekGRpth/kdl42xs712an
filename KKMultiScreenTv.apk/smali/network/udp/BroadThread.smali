.class public Lnetwork/udp/BroadThread;
.super Ljava/lang/Thread;
.source "BroadThread.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lnetwork/udp/BroadThread$Handler;
    }
.end annotation


# static fields
.field private static final DEF_NAME:Ljava/lang/String; = "tv device"

.field private static final SO_TIME_OUT:I = 0x1f4

.field private static final TAG:Ljava/lang/String; = "BroadThread"

.field private static TIMESCOUNT:I = 0x0

.field private static final UDP_BUFF_SIZE:I = 0x80

.field private static final UDP_DEST_PORT:I = 0x1f49

.field private static final UDP_SESSION_PORT:I = 0x1f40

.field public static connMap:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Long;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private static timeClear:I


# instance fields
.field private bFristOpt:Z

.field private buff:[B

.field public clear_flag:Z

.field private devName:Ljava/lang/String;

.field private devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

.field public g_buffer:[B

.field public run_flag:Z

.field private s_buffer:[B

.field private s_packet:Ljava/net/DatagramPacket;

.field private s_socket:Ljava/net/DatagramSocket;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/16 v0, 0x7b

    sput v0, Lnetwork/udp/BroadThread;->TIMESCOUNT:I

    sget v0, Lnetwork/udp/BroadThread;->TIMESCOUNT:I

    sput v0, Lnetwork/udp/BroadThread;->timeClear:I

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    return-void
.end method

.method public constructor <init>([B)V
    .locals 8
    .param p1    # [B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v3, 0x0

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    iput-object v3, p0, Lnetwork/udp/BroadThread;->s_buffer:[B

    iput-object v3, p0, Lnetwork/udp/BroadThread;->g_buffer:[B

    iput-boolean v4, p0, Lnetwork/udp/BroadThread;->bFristOpt:Z

    iput-boolean v4, p0, Lnetwork/udp/BroadThread;->run_flag:Z

    iput-boolean v7, p0, Lnetwork/udp/BroadThread;->clear_flag:Z

    iput-object v3, p0, Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;

    iput-object v3, p0, Lnetwork/udp/BroadThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iput-object v3, p0, Lnetwork/udp/BroadThread;->buff:[B

    const-string v3, "BroadThread"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BroadThread: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v5, p1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "255.255.255.255"

    invoke-static {v3}, Lnetwork/udp/UdpService;->CreateInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    const/16 v3, 0x1f40

    const/16 v4, 0x1f4

    invoke-static {v3, v4}, Lnetwork/udp/UdpService;->CreateBroadUdpSocket(II)Ljava/net/DatagramSocket;

    move-result-object v3

    iput-object v3, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    iput-object p1, p0, Lnetwork/udp/BroadThread;->s_buffer:[B

    new-instance v1, LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {v1}, LprotocolAnalysis/analysis/packetHandle;-><init>()V

    invoke-virtual {v1}, LprotocolAnalysis/analysis/packetHandle;->setDeviceNamePacket()[B

    move-result-object v2

    new-instance v3, Ljava/lang/String;

    invoke-direct {v3, v2}, Ljava/lang/String;-><init>([B)V

    iput-object v3, p0, Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;

    new-instance v3, LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v4, p0, Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    invoke-direct {v3, v4}, LprotocolAnalysis/protocol/SendUdpInfoPacket;-><init>(I)V

    iput-object v3, p0, Lnetwork/udp/BroadThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v3, p0, Lnetwork/udp/BroadThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    const/4 v4, 0x4

    const/16 v5, 0x2ff

    iget-object v6, p0, Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;

    invoke-virtual {v3, v7, v4, v5, v6}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->SetDeviceInfo(ISSLjava/lang/String;)V

    iget-object v3, p0, Lnetwork/udp/BroadThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    invoke-virtual {v3}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->sizeOf()I

    move-result v3

    new-array v3, v3, [B

    iput-object v3, p0, Lnetwork/udp/BroadThread;->buff:[B

    iget-object v3, p0, Lnetwork/udp/BroadThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v4, p0, Lnetwork/udp/BroadThread;->buff:[B

    invoke-virtual {v3, v4}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->format([B)V

    const-string v3, "BroadThread"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "devPacket: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lnetwork/udp/BroadThread;->devPacket:LprotocolAnalysis/protocol/SendUdpInfoPacket;

    iget-object v6, p0, Lnetwork/udp/BroadThread;->buff:[B

    invoke-virtual {v5, v6}, LprotocolAnalysis/protocol/SendUdpInfoPacket;->printf([B)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lnetwork/udp/BroadThread;->buff:[B

    iget-object v4, p0, Lnetwork/udp/BroadThread;->buff:[B

    array-length v4, v4

    const/16 v5, 0x1f49

    invoke-static {v3, v4, v0, v5}, Lnetwork/udp/UdpService;->CreateDatagramPacket([BILjava/net/InetAddress;I)Ljava/net/DatagramPacket;

    move-result-object v3

    iput-object v3, p0, Lnetwork/udp/BroadThread;->s_packet:Ljava/net/DatagramPacket;

    return-void
.end method

.method public static ByteToString([B)Ljava/lang/String;
    .locals 15
    .param p0    # [B

    array-length v11, p0

    const/16 v12, 0xc

    if-ge v11, v12, :cond_0

    const-string v11, ""

    :goto_0
    return-object v11

    :cond_0
    const/4 v11, 0x3

    new-array v4, v11, [B

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p0, v11, v4, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v4, v11

    const/4 v11, 0x3

    new-array v5, v11, [B

    const/4 v11, 0x2

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p0, v11, v5, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v5, v11

    invoke-static {v5}, LprotocolAnalysis/protocol/BaseProtocol;->byteToShort([B)S

    move-result v10

    const/4 v11, 0x5

    new-array v6, v11, [B

    const/4 v11, 0x4

    const/4 v12, 0x0

    const/4 v13, 0x4

    invoke-static {p0, v11, v6, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x4

    const/4 v12, 0x0

    aput-byte v12, v6, v11

    const/4 v11, 0x3

    new-array v7, v11, [B

    const/16 v11, 0x8

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p0, v11, v7, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v7, v11

    const/4 v11, 0x3

    new-array v8, v11, [B

    const/16 v11, 0xa

    const/4 v12, 0x0

    const/4 v13, 0x2

    invoke-static {p0, v11, v8, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const/4 v11, 0x2

    const/4 v12, 0x0

    aput-byte v12, v8, v11

    invoke-static {v8}, LprotocolAnalysis/protocol/BaseProtocol;->byteToShort([B)S

    move-result v0

    array-length v11, p0

    add-int/lit8 v11, v11, -0xc

    new-array v9, v11, [B

    const/16 v11, 0xc

    const/4 v12, 0x0

    array-length v13, p0

    add-int/lit8 v13, v13, -0xc

    invoke-static {p0, v11, v9, v12, v13}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    array-length v11, v9

    const/4 v12, 0x1

    if-gt v11, v12, :cond_1

    const/4 v11, 0x0

    goto :goto_0

    :cond_1
    const/4 v2, 0x0

    :try_start_0
    new-instance v3, Ljava/lang/String;

    const-string v11, "UTF-8"

    invoke-direct {v3, v9, v11}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    :goto_1
    new-instance v11, Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v4}, LprotocolAnalysis/protocol/BaseProtocol;->byteToShort([B)S

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "&"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {v12}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v12, Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v10}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "&"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v12, Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v6}, LprotocolAnalysis/protocol/BaseProtocol;->byteToInt([B)I

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "&"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v12, Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v7}, LprotocolAnalysis/protocol/BaseProtocol;->byteToShort([B)S

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "&"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    new-instance v12, Ljava/lang/String;

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-static {v8}, LprotocolAnalysis/protocol/BaseProtocol;->byteToShort([B)S

    move-result v14

    invoke-static {v14}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v13, v14}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v14, "&"

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v2}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v14

    invoke-virtual {v13, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v13

    invoke-virtual {v13}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    invoke-direct {v12, v13}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    goto/16 :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto/16 :goto_1
.end method

.method static synthetic access$0(Lnetwork/udp/BroadThread;)Ljava/net/DatagramSocket;
    .locals 1

    iget-object v0, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    return-object v0
.end method

.method static synthetic access$1(Lnetwork/udp/BroadThread;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2(Lnetwork/udp/BroadThread;Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lnetwork/udp/BroadThread;->devName:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$3(Lnetwork/udp/BroadThread;)[B
    .locals 1

    iget-object v0, p0, Lnetwork/udp/BroadThread;->buff:[B

    return-object v0
.end method


# virtual methods
.method public ClearDataSet()V
    .locals 1

    const/4 v0, 0x1

    iput-boolean v0, p0, Lnetwork/udp/BroadThread;->clear_flag:Z

    return-void
.end method

.method public run()V
    .locals 10

    const/4 v9, 0x0

    const-string v6, "BroadThread"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "BroadThread running ......"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    new-instance v4, Ljava/lang/Thread;

    new-instance v6, Lnetwork/udp/BroadThread$Handler;

    invoke-direct {v6, p0}, Lnetwork/udp/BroadThread$Handler;-><init>(Lnetwork/udp/BroadThread;)V

    invoke-direct {v4, v6}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v4}, Ljava/lang/Thread;->start()V

    new-instance v5, Lnetwork/udp/UdpSearchThread;

    invoke-direct {v5}, Lnetwork/udp/UdpSearchThread;-><init>()V

    invoke-virtual {v5}, Lnetwork/udp/UdpSearchThread;->listen()V

    :goto_0
    iget-boolean v6, p0, Lnetwork/udp/BroadThread;->run_flag:Z

    if-nez v6, :cond_2

    :cond_0
    iget-object v6, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    if-eqz v6, :cond_1

    iget-object v6, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    invoke-virtual {v6}, Ljava/net/DatagramSocket;->close()V

    :cond_1
    iput-object v9, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    invoke-virtual {p0}, Lnetwork/udp/BroadThread;->interrupt()V

    invoke-virtual {v4}, Ljava/lang/Thread;->interrupt()V

    invoke-virtual {v5}, Lnetwork/udp/UdpSearchThread;->unListen()V

    return-void

    :cond_2
    :try_start_0
    iget-boolean v6, p0, Lnetwork/udp/BroadThread;->clear_flag:Z

    if-eqz v6, :cond_3

    sget-object v6, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    const-string v6, "BroadThread"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "BroadThread connMap.clear() ......"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    iput-boolean v6, p0, Lnetwork/udp/BroadThread;->clear_flag:Z

    :cond_3
    iget-object v6, p0, Lnetwork/udp/BroadThread;->g_buffer:[B

    if-eqz v6, :cond_4

    iget-object v6, p0, Lnetwork/udp/BroadThread;->g_buffer:[B

    iput-object v6, p0, Lnetwork/udp/BroadThread;->s_buffer:[B

    const-string v6, "255.255.255.255"

    invoke-static {v6}, Lnetwork/udp/UdpService;->CreateInetAddress(Ljava/lang/String;)Ljava/net/InetAddress;

    move-result-object v0

    iget-object v6, p0, Lnetwork/udp/BroadThread;->s_buffer:[B

    iget-object v7, p0, Lnetwork/udp/BroadThread;->s_buffer:[B

    array-length v7, v7

    const/16 v8, 0x1f49

    invoke-static {v6, v7, v0, v8}, Lnetwork/udp/UdpService;->CreateDatagramPacket([BILjava/net/InetAddress;I)Ljava/net/DatagramPacket;

    move-result-object v6

    iput-object v6, p0, Lnetwork/udp/BroadThread;->s_packet:Ljava/net/DatagramPacket;

    const-string v6, "BroadThread"

    const-string v7, "new create a broadcast packet!"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v6, 0x0

    iput-object v6, p0, Lnetwork/udp/BroadThread;->g_buffer:[B

    :cond_4
    iget-boolean v6, p0, Lnetwork/udp/BroadThread;->bFristOpt:Z

    if-eqz v6, :cond_7

    add-int/lit8 v2, v2, 0x1

    const/16 v6, 0x14

    if-lt v2, v6, :cond_5

    const/4 v6, 0x0

    iput-boolean v6, p0, Lnetwork/udp/BroadThread;->bFristOpt:Z

    :cond_5
    const-wide/16 v6, 0x1f4

    invoke-static {v6, v7}, Lnetwork/udp/BroadThread;->sleep(J)V

    :cond_6
    :goto_1
    iget-object v6, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lnetwork/udp/BroadThread;->s_socket:Ljava/net/DatagramSocket;

    iget-object v7, p0, Lnetwork/udp/BroadThread;->s_packet:Ljava/net/DatagramPacket;

    invoke-virtual {v6, v7}, Ljava/net/DatagramSocket;->send(Ljava/net/DatagramPacket;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    goto/16 :goto_0

    :catch_0
    move-exception v1

    :goto_2
    const-string v6, "BroadThread"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "e "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_7
    add-int/lit8 v3, v2, -0x1

    if-ltz v2, :cond_8

    const-wide/16 v6, 0x1f4

    :try_start_1
    invoke-static {v6, v7}, Ljava/lang/Thread;->sleep(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    move v2, v3

    goto/16 :goto_0

    :cond_8
    const/16 v2, 0xa

    :try_start_2
    sget v6, Lnetwork/udp/BroadThread;->timeClear:I

    add-int/lit8 v6, v6, -0x1

    sput v6, Lnetwork/udp/BroadThread;->timeClear:I

    sget v6, Lnetwork/udp/BroadThread;->timeClear:I

    if-nez v6, :cond_6

    sget-object v6, Lnetwork/udp/BroadThread;->connMap:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->clear()V

    sget v6, Lnetwork/udp/BroadThread;->TIMESCOUNT:I

    sput v6, Lnetwork/udp/BroadThread;->timeClear:I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    :catch_1
    move-exception v1

    :goto_3
    const-string v6, "BroadThread"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "e "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :catch_2
    move-exception v1

    move v2, v3

    goto :goto_3

    :catch_3
    move-exception v1

    move v2, v3

    goto :goto_2
.end method
