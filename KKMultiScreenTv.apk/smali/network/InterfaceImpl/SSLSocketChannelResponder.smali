.class public Lnetwork/InterfaceImpl/SSLSocketChannelResponder;
.super Ljava/lang/Object;
.source "SSLSocketChannelResponder.java"

# interfaces
.implements Lnetwork/Interface/NIOSocketSSL;
.implements Lnetwork/Interface/SocketObserver;


# instance fields
.field private final m_nioService:Lnetwork/NetCore/asyncService;

.field private m_observer:Lnetwork/Interface/SocketObserver;

.field private final m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

.field private final m_wrappedSocket:Lnetwork/Interface/NIOSocket;


# direct methods
.method public constructor <init>(Lnetwork/NetCore/asyncService;Lnetwork/Interface/NIOSocket;Ljavax/net/ssl/SSLEngine;Z)V
    .locals 2
    .param p1    # Lnetwork/NetCore/asyncService;
    .param p2    # Lnetwork/Interface/NIOSocket;
    .param p3    # Ljavax/net/ssl/SSLEngine;
    .param p4    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_nioService:Lnetwork/NetCore/asyncService;

    iput-object p2, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    new-instance v0, Lnetwork/InterfaceImpl/SSLPacketHandler;

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-direct {v0, p3, v1, p0}, Lnetwork/InterfaceImpl/SSLPacketHandler;-><init>(Ljavax/net/ssl/SSLEngine;Lnetwork/Interface/NIOSocket;Lnetwork/InterfaceImpl/SSLSocketChannelResponder;)V

    iput-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-interface {v0, v1}, Lnetwork/Interface/NIOSocket;->setPacketReader(Lnetwork/Interface/PacketReader;)V

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-interface {v0, v1}, Lnetwork/Interface/NIOSocket;->setPacketWriter(Lnetwork/Interface/PacketWriter;)V

    invoke-virtual {p3, p4}, Ljavax/net/ssl/SSLEngine;->setUseClientMode(Z)V

    return-void
.end method

.method static synthetic access$0(Lnetwork/InterfaceImpl/SSLSocketChannelResponder;)Lnetwork/InterfaceImpl/SSLPacketHandler;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    return-object v0
.end method


# virtual methods
.method public beginHandshake()V
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljavax/net/ssl/SSLException;
        }
    .end annotation

    invoke-virtual {p0}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->getSSLEngine()Ljavax/net/ssl/SSLEngine;

    move-result-object v0

    invoke-virtual {v0}, Ljavax/net/ssl/SSLEngine;->getHandshakeStatus()Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    move-result-object v0

    sget-object v1, Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;->NOT_HANDSHAKING:Ljavax/net/ssl/SSLEngineResult$HandshakeStatus;

    if-eq v0, v1, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Tried to start handshake during handshake."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/SSLPacketHandler;->begin()V

    return-void
.end method

.method public close()V
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->close()V

    return-void
.end method

.method public closeAfterWrite()V
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/SSLPacketHandler;->closeEngine()V

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->closeAfterWrite()V

    return-void
.end method

.method closeDueToSSLException(Ljavax/net/ssl/SSLException;)V
    .locals 2
    .param p1    # Ljavax/net/ssl/SSLException;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p1}, Lnetwork/Interface/SocketObserver;->connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v1}, Lnetwork/Interface/NIOSocket;->close()V

    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_nioService:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1, p1}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    .locals 2
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # Ljava/lang/Exception;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p2}, Lnetwork/Interface/SocketObserver;->connectionBroken(Lnetwork/Interface/NIOSocket;Ljava/lang/Exception;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_nioService:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public connectionOpened(Lnetwork/Interface/NIOSocket;)V
    .locals 2
    .param p1    # Lnetwork/Interface/NIOSocket;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0}, Lnetwork/Interface/SocketObserver;->connectionOpened(Lnetwork/Interface/NIOSocket;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_nioService:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public getAddress()Ljava/net/InetSocketAddress;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getAddress()Ljava/net/InetSocketAddress;

    move-result-object v0

    return-object v0
.end method

.method public getBytesRead()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getBytesRead()J

    move-result-wide v0

    return-wide v0
.end method

.method public getBytesWritten()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getBytesWritten()J

    move-result-wide v0

    return-wide v0
.end method

.method public getIp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMaxQueueSize()I
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getMaxQueueSize()I

    move-result v0

    return v0
.end method

.method public getPort()I
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getPort()I

    move-result v0

    return v0
.end method

.method public getSSLEngine()Ljavax/net/ssl/SSLEngine;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/SSLPacketHandler;->getSSLEngine()Ljavax/net/ssl/SSLEngine;

    move-result-object v0

    return-object v0
.end method

.method public getTag()Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getTag()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getTimeOpen()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getTimeOpen()J

    move-result-wide v0

    return-wide v0
.end method

.method public getWriteQueueSize()J
    .locals 2

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->getWriteQueueSize()J

    move-result-wide v0

    return-wide v0
.end method

.method public isEncrypted()Z
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-virtual {v0}, Lnetwork/InterfaceImpl/SSLPacketHandler;->isEncrypted()Z

    move-result v0

    return v0
.end method

.method public isOpen()Z
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->isOpen()Z

    move-result v0

    return v0
.end method

.method public listen(Lnetwork/Interface/SocketObserver;)V
    .locals 1
    .param p1    # Lnetwork/Interface/SocketObserver;

    iput-object p1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p0}, Lnetwork/Interface/NIOSocket;->listen(Lnetwork/Interface/SocketObserver;)V

    return-void
.end method

.method public packetReceived(Lnetwork/Interface/NIOSocket;[B)V
    .locals 2
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # [B

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p2}, Lnetwork/Interface/SocketObserver;->packetReceived(Lnetwork/Interface/NIOSocket;[B)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_nioService:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public packetSent(Lnetwork/Interface/NIOSocket;Ljava/lang/Object;)V
    .locals 2
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # Ljava/lang/Object;

    :try_start_0
    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_observer:Lnetwork/Interface/SocketObserver;

    invoke-interface {v1, p0, p2}, Lnetwork/Interface/SocketObserver;->packetSent(Lnetwork/Interface/NIOSocket;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v1, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_nioService:Lnetwork/NetCore/asyncService;

    invoke-virtual {v1, v0}, Lnetwork/NetCore/asyncService;->notifyException(Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public queue(Ljava/lang/Runnable;)V
    .locals 1
    .param p1    # Ljava/lang/Runnable;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p1}, Lnetwork/Interface/NIOSocket;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setMaxQueueSize(I)V
    .locals 1
    .param p1    # I

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p1}, Lnetwork/Interface/NIOSocket;->setMaxQueueSize(I)V

    return-void
.end method

.method public setPacketReader(Lnetwork/Interface/PacketReader;)V
    .locals 1
    .param p1    # Lnetwork/Interface/PacketReader;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_packetHandler:Lnetwork/InterfaceImpl/SSLPacketHandler;

    invoke-virtual {v0, p1}, Lnetwork/InterfaceImpl/SSLPacketHandler;->setReader(Lnetwork/Interface/PacketReader;)V

    return-void
.end method

.method public setPacketWriter(Lnetwork/Interface/PacketWriter;)V
    .locals 2
    .param p1    # Lnetwork/Interface/PacketWriter;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    new-instance v1, Lnetwork/InterfaceImpl/SSLSocketChannelResponder$1;

    invoke-direct {v1, p0, p1}, Lnetwork/InterfaceImpl/SSLSocketChannelResponder$1;-><init>(Lnetwork/InterfaceImpl/SSLSocketChannelResponder;Lnetwork/Interface/PacketWriter;)V

    invoke-interface {v0, v1}, Lnetwork/Interface/NIOSocket;->queue(Ljava/lang/Runnable;)V

    return-void
.end method

.method public setTag(Ljava/lang/Object;)V
    .locals 1
    .param p1    # Ljava/lang/Object;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p1}, Lnetwork/Interface/NIOSocket;->setTag(Ljava/lang/Object;)V

    return-void
.end method

.method public socket()Ljava/net/Socket;
    .locals 1

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0}, Lnetwork/Interface/NIOSocket;->socket()Ljava/net/Socket;

    move-result-object v0

    return-object v0
.end method

.method public write([B)Z
    .locals 1
    .param p1    # [B

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p1}, Lnetwork/Interface/NIOSocket;->write([B)Z

    move-result v0

    return v0
.end method

.method public write([BLjava/lang/Object;)Z
    .locals 1
    .param p1    # [B
    .param p2    # Ljava/lang/Object;

    iget-object v0, p0, Lnetwork/InterfaceImpl/SSLSocketChannelResponder;->m_wrappedSocket:Lnetwork/Interface/NIOSocket;

    invoke-interface {v0, p1, p2}, Lnetwork/Interface/NIOSocket;->write([BLjava/lang/Object;)Z

    move-result v0

    return v0
.end method
