.class public LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;
.super Ljava/lang/Object;
.source "DevConcurrentLinkedQueuePool.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Worker"
.end annotation


# instance fields
.field private fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

.field private mHandle:LprotocolAnalysis/analysis/packetHandle;

.field private poolIndex:I

.field public runTag:Z


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput v0, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    const/4 v0, 0x1

    iput-boolean v0, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->runTag:Z

    new-instance v0, Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    invoke-direct {v0}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;-><init>()V

    iput-object v0, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    new-instance v0, LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {v0}, LprotocolAnalysis/analysis/packetHandle;-><init>()V

    iput-object v0, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->mHandle:LprotocolAnalysis/analysis/packetHandle;

    iput p1, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    const-wide/16 v8, 0x64

    const/4 v2, 0x0

    sget-object v4, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget v5, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    aget-object v3, v4, v5

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Worker start id:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    :goto_0
    iget-boolean v4, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->runTag:Z

    if-nez v4, :cond_0

    return-void

    :cond_0
    :try_start_0
    invoke-virtual {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_3

    sget-object v4, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->lockObj:[Ljava/lang/Object;

    iget v5, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    aget-object v5, v4, v5

    monitor-enter v5
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    sget-object v4, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;

    iget v6, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    aget-object v4, v4, v6

    invoke-virtual {v4}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v4

    move-object v0, v4

    check-cast v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;

    move-object v2, v0

    iget v4, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    const/4 v6, 0x1

    if-ne v4, v6, :cond_2

    iget-object v4, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->fos:Lcom/konka/IntelligentControl/jniInterface/IfileOper;

    iget-object v6, v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->mBuffer:[B

    iget v7, v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->cmd:I

    invoke-virtual {v4, v6, v7}, Lcom/konka/IntelligentControl/jniInterface/IfileOper;->handle([BI)V

    :cond_1
    :goto_1
    monitor-exit v5

    goto :goto_0

    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v4
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    :catch_0
    move-exception v1

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "InterruptedException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v8, v9}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :cond_2
    :try_start_3
    iget v4, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->poolIndex:I

    if-nez v4, :cond_1

    iget-object v4, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;->mHandle:LprotocolAnalysis/analysis/packetHandle;

    iget-object v6, v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->sock:Lnetwork/Interface/NIOSocket;

    iget-object v7, v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;->mBuffer:[B

    invoke-virtual {v4, v6, v7}, LprotocolAnalysis/analysis/packetHandle;->multiScreenPacketHandle(Lnetwork/Interface/NIOSocket;[B)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    :cond_3
    const-wide/16 v4, 0xa

    :try_start_4
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/util/NoSuchElementException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    goto :goto_0

    :catch_1
    move-exception v1

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "NoSuchElementException "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/NoSuchElementException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v8, v9}, Landroid/os/SystemClock;->sleep(J)V

    goto :goto_0

    :catch_2
    move-exception v1

    sget-object v4, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Exception "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-static {v8, v9}, Landroid/os/SystemClock;->sleep(J)V

    goto/16 :goto_0
.end method
