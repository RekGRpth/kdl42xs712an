.class public LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;
.super Ljava/lang/Object;
.source "DevConcurrentLinkedQueuePool.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;,
        LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$Worker;
    }
.end annotation


# static fields
.field private static final DEF_POOL_SIZE:I = 0x2

.field public static lockObj:[Ljava/lang/Object;

.field public static queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[",
            "Ljava/util/concurrent/ConcurrentLinkedQueue",
            "<",
            "LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private threadNum:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    sput-object v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;

    sput-object v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->lockObj:[Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 5

    const/4 v4, 0x2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v4, p0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->threadNum:I

    new-array v2, v4, [Ljava/util/concurrent/ConcurrentLinkedQueue;

    sput-object v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-array v2, v4, [Ljava/lang/Object;

    sput-object v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->lockObj:[Ljava/lang/Object;

    const/4 v1, 0x0

    :goto_0
    if-lt v1, v4, :cond_0

    :try_start_0
    const-string v2, "start  thread pool"

    invoke-direct {p0, v2}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->mainRun(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :cond_0
    sget-object v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;

    new-instance v3, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v3}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    aput-object v3, v2, v1

    sget-object v2, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->lockObj:[Ljava/lang/Object;

    new-instance v3, Ljava/lang/Object;

    invoke-direct {v3}, Ljava/lang/Object;-><init>()V

    aput-object v3, v2, v1

    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    sget-object v2, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "mainRun "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_1
.end method

.method private mainRun(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    new-instance v0, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;

    invoke-direct {v0, p0}, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$1;-><init>(LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method


# virtual methods
.method public inPoolQueue(ILdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;)V
    .locals 4
    .param p1    # I
    .param p2    # LdataLayer/dataCenter/DevConcurrentLinkedQueuePool$CmdPacketWriter;

    :try_start_0
    sget-object v1, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->lockObj:[Ljava/lang/Object;

    aget-object v2, v1, p1

    monitor-enter v2
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    :try_start_1
    sget-object v1, LdataLayer/dataCenter/DevConcurrentLinkedQueuePool;->queuePool:[Ljava/util/concurrent/ConcurrentLinkedQueue;

    aget-object v1, v1, p1

    invoke-virtual {v1, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    monitor-exit v2

    :goto_0
    return-void

    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    throw v1
    :try_end_2
    .catch Ljava/util/NoSuchElementException; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :catch_0
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NoSuchElementException "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/util/NoSuchElementException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    :catch_1
    move-exception v0

    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0
.end method
