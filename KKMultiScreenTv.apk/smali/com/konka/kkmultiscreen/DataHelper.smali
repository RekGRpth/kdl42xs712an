.class public Lcom/konka/kkmultiscreen/DataHelper;
.super Ljava/lang/Object;
.source "DataHelper.java"


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = ""

.field private static final DESCRIPTOR_LEN:I = 0x8

.field private static final INNERMSG:Ljava/lang/Integer;

.field private static IsConnSucc:Ljava/lang/Integer; = null

.field private static final LOG_TAG:Ljava/lang/String; = "DataHelper"

.field private static final OUTERMSG:Ljava/lang/Integer;

.field public static myService:Lcom/konka/kkmultiscreen/DataService;

.field private static serviceConnection:Landroid/content/ServiceConnection;

.field private static serviceIntent:Landroid/content/Intent;


# instance fields
.field private IsNeedInit:Z

.field private activity:Landroid/content/Context;

.field private innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0x4e21

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->OUTERMSG:Ljava/lang/Integer;

    const/16 v0, 0x4e22

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->INNERMSG:Ljava/lang/Integer;

    sput-object v1, Lcom/konka/kkmultiscreen/DataHelper;->myService:Lcom/konka/kkmultiscreen/DataService;

    sput-object v1, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    sput-object v1, Lcom/konka/kkmultiscreen/DataHelper;->serviceIntent:Landroid/content/Intent;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->IsConnSucc:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1    # Landroid/content/Context;

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/konka/kkmultiscreen/DataHelper;->IsNeedInit:Z

    iput-object v1, p0, Lcom/konka/kkmultiscreen/DataHelper;->innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    const-string v0, "DataHelper"

    const-string v1, "DataHelper create"

    invoke-static {v0, v1}, Lutil/log/CLog;->vLog(Ljava/lang/String;Ljava/lang/String;)V

    iput-object p1, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataHelper;->ToStartHelper()V

    return-void
.end method

.method static synthetic access$0(Ljava/lang/Integer;)V
    .locals 0

    sput-object p0, Lcom/konka/kkmultiscreen/DataHelper;->IsConnSucc:Ljava/lang/Integer;

    return-void
.end method

.method static synthetic access$1()Landroid/content/ServiceConnection;
    .locals 1

    sget-object v0, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    return-object v0
.end method

.method static synthetic access$2(Lcom/konka/kkmultiscreen/DataHelper;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$3(Lcom/konka/kkmultiscreen/DataHelper;)Lcom/konka/kkmultiscreen/MsgDef$MsgInner;
    .locals 1

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper;->innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    return-object v0
.end method

.method static synthetic access$4(Lcom/konka/kkmultiscreen/DataHelper;Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/kkmultiscreen/DataHelper;->innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    return-void
.end method


# virtual methods
.method public SendDataToService(I)V
    .locals 5
    .param p1    # I

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    const-class v3, Lcom/konka/kkmultiscreen/DataService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "name"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "This is from ShowMsg! "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v2, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public SendDataToService(Landroid/content/Intent;)V
    .locals 1
    .param p1    # Landroid/content/Intent;

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    return-void
.end method

.method public SendInnerMsgToService(Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)Landroid/os/Parcel;
    .locals 6
    .param p1    # Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    sget-object v3, Lcom/konka/kkmultiscreen/DataHelper;->IsConnSucc:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_0

    iput-object p1, p0, Lcom/konka/kkmultiscreen/DataHelper;->innerCacheMsg:Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    const-string v3, "DataHelper"

    const-string v4, "SendInnerMsgToService: service not start let start wait serivce!"

    invoke-static {v3, v4}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v3, "DataHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SendDataToService: send MSG ORDER "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadOrder()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DataHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SendDataToService: send MSG Data "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v3, "|"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadOrder()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/kkmultiscreen/DataHelper;->myService:Lcom/konka/kkmultiscreen/DataService;

    invoke-virtual {v3}, Lcom/konka/kkmultiscreen/DataService;->getIBind()Lcom/konka/kkmultiscreen/DataService$MyBinder;

    move-result-object v3

    sget-object v4, Lcom/konka/kkmultiscreen/DataHelper;->INNERMSG:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v1, v5}, Lcom/konka/kkmultiscreen/DataService$MyBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    invoke-virtual {v2}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public SendOuterMsgToService(Lcom/konka/kkmultiscreen/MsgDef$MsgInner;)Landroid/os/Parcel;
    .locals 6
    .param p1    # Lcom/konka/kkmultiscreen/MsgDef$MsgInner;

    sget-object v3, Lcom/konka/kkmultiscreen/DataHelper;->IsConnSucc:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "DataHelper"

    const-string v4, "SendOuterMsgToService: service not start let start wait serivce!"

    invoke-static {v3, v4}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v0

    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    const-string v3, "DataHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SendDataToService: send MSG ORDER "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadOrder()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "DataHelper"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "SendDataToService: send MSG Data "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadData()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    const-string v3, "|"

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInterfaceToken(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadOrder()I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {p1}, Lcom/konka/kkmultiscreen/MsgDef$MsgInner;->ReadData()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    sget-object v3, Lcom/konka/kkmultiscreen/DataHelper;->myService:Lcom/konka/kkmultiscreen/DataService;

    invoke-virtual {v3}, Lcom/konka/kkmultiscreen/DataService;->getIBind()Lcom/konka/kkmultiscreen/DataService$MyBinder;

    move-result-object v3

    sget-object v4, Lcom/konka/kkmultiscreen/DataHelper;->OUTERMSG:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x0

    invoke-virtual {v3, v4, v0, v1, v5}, Lcom/konka/kkmultiscreen/DataService$MyBinder;->transact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    invoke-virtual {v1}, Landroid/os/Parcel;->readException()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catch_0
    move-exception v2

    :try_start_1
    invoke-virtual {v2}, Landroid/os/RemoteException;->getStackTrace()[Ljava/lang/StackTraceElement;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    :catchall_0
    move-exception v3

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    invoke-virtual {v0}, Landroid/os/Parcel;->recycle()V

    throw v3
.end method

.method public ToDestroyHelper()V
    .locals 2

    sget-object v0, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    const-string v0, "DataHelper"

    const-string v1, "ToDestroy"

    invoke-static {v0, v1}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    sget-object v1, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    :cond_0
    return-void
.end method

.method public ToStartHelper()V
    .locals 5

    const/4 v2, 0x1

    iget-boolean v1, p0, Lcom/konka/kkmultiscreen/DataHelper;->IsNeedInit:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/konka/kkmultiscreen/DataHelper;->init()Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    :goto_0
    iput-boolean v1, p0, Lcom/konka/kkmultiscreen/DataHelper;->IsNeedInit:Z

    const-string v1, "DataHelper"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Need to relay Init: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/konka/kkmultiscreen/DataHelper;->IsNeedInit:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    sget-object v3, Lcom/konka/kkmultiscreen/DataHelper;->serviceIntent:Landroid/content/Intent;

    sget-object v4, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v3, v4, v2}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_1

    const-string v1, "DataHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "activity bindService call state "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v3, p0, Lcom/konka/kkmultiscreen/DataHelper;->IsNeedInit:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-void

    :cond_2
    move v1, v2

    goto :goto_0
.end method

.method public init()Z
    .locals 3

    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    const-class v2, Lcom/konka/kkmultiscreen/DataService;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->serviceIntent:Landroid/content/Intent;

    const-string v0, "DataHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "init serviceIntent: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/konka/kkmultiscreen/DataHelper;->serviceIntent:Landroid/content/Intent;

    invoke-virtual {v2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/konka/kkmultiscreen/DataHelper;->activity:Landroid/content/Context;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Lcom/konka/kkmultiscreen/DataHelper$1;

    invoke-direct {v0, p0}, Lcom/konka/kkmultiscreen/DataHelper$1;-><init>(Lcom/konka/kkmultiscreen/DataHelper;)V

    sput-object v0, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    const-string v0, "DataHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "serviceConnection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/konka/kkmultiscreen/DataHelper;->serviceIntent:Landroid/content/Intent;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/konka/kkmultiscreen/DataHelper;->serviceConnection:Landroid/content/ServiceConnection;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
