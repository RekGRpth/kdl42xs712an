.class public Lcom/konka/IntelligentControl/ioop/specificOp/CTouchOp;
.super Ljava/lang/Object;
.source "CTouchOp.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "CTouchOp"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static writeTouchEventBuffer(IIIIII)V
    .locals 9
    .param p0    # I
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I

    const/4 v8, 0x4

    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x0

    const-string v1, "fd: ------>"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "+++X1 = "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " -----Y1="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " X2 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  Y2="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " X3 = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "  Y3="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/16 v1, 0x2710

    if-ne p3, v1, :cond_0

    packed-switch p0, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x35

    invoke-virtual {v1, v2, v6, p1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x36

    invoke-virtual {v1, v2, v6, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto :goto_0

    :pswitch_1
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x14a

    invoke-virtual {v1, v2, v5, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    invoke-virtual {v1, v4, v6, p1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v5, v4, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    const/16 v2, 0x18

    invoke-virtual {v1, v2, v4, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    const/16 v2, 0x14a

    invoke-virtual {v1, v2, v5, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    const/16 v2, 0x18

    invoke-virtual {v1, v2, v6, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v2, 0x5

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v2, 0x6

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    const/4 v2, 0x7

    aget-object v1, v1, v2

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_2
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    invoke-virtual {v1, v4, v6, p1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v5, v4, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    const/16 v2, 0x18

    invoke-virtual {v1, v2, v4, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x14a

    invoke-virtual {v1, v2, v5, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x18

    invoke-virtual {v1, v2, v6, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_4
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x14a

    invoke-virtual {v1, v2, v5, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    invoke-virtual {v1, v4, v6, p1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v5, v6, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    const/16 v2, 0x18

    invoke-virtual {v1, v2, v6, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    const-wide/16 v1, 0x64

    :try_start_0
    invoke-static {v1, v2}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    invoke-virtual {v1, v4, v6, p1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v5, v6, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    const/16 v2, 0x18

    invoke-virtual {v1, v2, v6, v5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v8

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_1

    :cond_0
    const/16 v1, 0x2710

    if-ne p4, v1, :cond_1

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x35

    invoke-virtual {v1, v2, v6, p0}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x36

    rsub-int v3, p1, 0x4c5

    invoke-virtual {v1, v2, v6, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x35

    invoke-virtual {v1, v2, v6, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x36

    invoke-virtual {v1, v2, v6, p3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :cond_1
    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x35

    invoke-virtual {v1, v2, v6, p0}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x36

    rsub-int v3, p1, 0x4c5

    invoke-virtual {v1, v2, v6, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x35

    invoke-virtual {v1, v2, v6, p2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x36

    invoke-virtual {v1, v2, v6, p3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    const/16 v2, 0x35

    invoke-virtual {v1, v2, v6, p4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    const/16 v2, 0x36

    invoke-virtual {v1, v2, v6, p5}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    invoke-virtual {v1, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v4

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v5

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v7

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    invoke-virtual {v1, v4, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v1, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v1, v1, v6

    iget-object v1, v1, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->gettouchFd()Ljava/io/DataOutputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
