.class public Lcom/konka/IntelligentControl/ioop/specificOp/CIRemoteOp;
.super Ljava/lang/Object;
.source "CIRemoteOp.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "CIRemoteOp"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static writeIRBuffer(S)V
    .locals 10
    .param p0    # S

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    move v0, p0

    const-string v3, "CIRemoteOp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ir key value: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch v0, :pswitch_data_0

    :goto_0
    sget v3, Lnetwork/udp/UdpBroadcast;->devNameType:I

    packed-switch v3, :pswitch_data_1

    :cond_0
    :goto_1
    const v3, 0xf000

    and-int/2addr v3, v0

    shr-int/lit8 v3, v3, 0xc

    int-to-short v1, v3

    if-lez v1, :cond_5

    and-int/lit16 v3, v0, 0xfff

    int-to-short v2, v3

    if-ne v1, v7, :cond_4

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v6

    invoke-virtual {v3, v2, v7, v7}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v7

    invoke-virtual {v3, v6, v6, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v7

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    :cond_1
    :goto_2
    return-void

    :pswitch_0
    const/16 v0, 0x52

    goto :goto_0

    :pswitch_1
    const/16 v0, 0x4f

    goto :goto_0

    :pswitch_2
    const/16 v0, 0x50

    goto :goto_0

    :pswitch_3
    const/16 v0, 0x1d5

    goto :goto_0

    :pswitch_4
    const/16 v0, 0x1db

    goto :goto_0

    :pswitch_5
    const/16 v3, 0x2ba

    if-ne v0, v3, :cond_2

    const/16 v0, 0x43

    goto :goto_1

    :cond_2
    const/16 v3, 0x2bb

    if-ne v0, v3, :cond_3

    const/16 v0, 0x42

    goto :goto_1

    :cond_3
    const/16 v3, 0x2bc

    if-ne v0, v3, :cond_0

    const/16 v0, 0x41

    goto :goto_1

    :cond_4
    if-ne v1, v8, :cond_1

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v8

    invoke-virtual {v3, v2, v7, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v9

    invoke-virtual {v3, v6, v6, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v8

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v9

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto :goto_2

    :cond_5
    const-string v3, "CIRemoteOp"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "ir key value2: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v6

    invoke-virtual {v3, v0, v7, v7}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v7

    invoke-virtual {v3, v6, v6, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v7

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v8

    invoke-virtual {v3, v0, v7, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v9

    invoke-virtual {v3, v6, v6, v6}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v8

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v3, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v3, v3, v9

    iget-object v3, v3, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getIrFd()Ljava/io/DataOutputStream;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x1d6
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_5
    .end packed-switch
.end method
