.class public Lcom/konka/IntelligentControl/ioop/specificOp/CMouseOp;
.super Ljava/lang/Object;
.source "CMouseOp.java"


# static fields
.field public static final TAG:Ljava/lang/String; = "CMouseOp"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static mouseleftPress()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    const v1, 0x90001

    invoke-virtual {v0, v5, v5, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    const/16 v1, 0x110

    invoke-virtual {v0, v1, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2, v2, v2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    return-void
.end method

.method private static mouseleftRelease()V
    .locals 6

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    const v1, 0x90001

    invoke-virtual {v0, v5, v5, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    const/16 v1, 0x110

    invoke-virtual {v0, v1, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2, v2, v2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    return-void
.end method

.method private static mouseleftclick()V
    .locals 7

    const/16 v6, 0x110

    const/4 v5, 0x4

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    const v1, 0x90001

    invoke-virtual {v0, v5, v5, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    invoke-virtual {v0, v6, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2, v2, v2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    const v1, 0x90001

    invoke-virtual {v0, v5, v5, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    invoke-virtual {v0, v6, v3, v2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v2, v2, v2}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v2

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    return-void
.end method

.method public static writeMouseBuffer(SSSS)V
    .locals 8
    .param p0    # S
    .param p1    # S
    .param p2    # S
    .param p3    # S

    const/16 v7, 0x110

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    const-string v0, "K: ------>"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "x="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "--y = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " -----z= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "----motion= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    packed-switch p3, :pswitch_data_0

    :goto_0
    :pswitch_0
    return-void

    :pswitch_1
    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    invoke-virtual {v0, v3, v5, p0}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v4, v5, p1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    const-string v0, "K: ------>"

    const-string v1, "move -------------"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :pswitch_2
    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    const v1, 0x90001

    invoke-virtual {v0, v6, v6, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v7, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_3
    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    const v1, 0x90001

    invoke-virtual {v0, v6, v6, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    invoke-virtual {v0, v7, v4, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_4
    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    const v1, 0x90002

    invoke-virtual {v0, v6, v6, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    const/16 v1, 0x111

    invoke-virtual {v0, v1, v4, v4}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_5
    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    const v1, 0x90002

    invoke-virtual {v0, v6, v6, v1}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    const/16 v1, 0x111

    invoke-virtual {v0, v1, v4, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    invoke-virtual {v0, v3, v3, v3}, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->setValue(SSI)[B

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v3

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v4

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    sget-object v0, Lcom/konka/IntelligentControl/ioop/fileOp/CDevPacketInfo;->ievent:[Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;

    aget-object v0, v0, v5

    iget-object v0, v0, Lcom/konka/IntelligentControl/ioop/constDevdef/CInputEvent32;->InputEvent:[B

    invoke-static {}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->getmouseFd()Ljava/io/DataOutputStream;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/konka/IntelligentControl/ioop/fileOp/CFileOper;->writeData([BLjava/io/DataOutputStream;)V

    goto/16 :goto_0

    :pswitch_6
    invoke-static {}, Lcom/konka/IntelligentControl/ioop/specificOp/CMouseOp;->mouseleftclick()V

    goto/16 :goto_0

    :pswitch_7
    invoke-static {}, Lcom/konka/IntelligentControl/ioop/specificOp/CMouseOp;->mouseleftPress()V

    goto/16 :goto_0

    :pswitch_8
    invoke-static {}, Lcom/konka/IntelligentControl/ioop/specificOp/CMouseOp;->mouseleftRelease()V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method
