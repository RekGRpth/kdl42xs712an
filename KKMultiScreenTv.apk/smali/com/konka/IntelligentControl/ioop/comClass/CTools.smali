.class public Lcom/konka/IntelligentControl/ioop/comClass/CTools;
.super Ljava/lang/Object;
.source "CTools.java"


# static fields
.field private static final LOG_TAG:Ljava/lang/String; = "CTools"


# direct methods
.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static BinstrToChar(Ljava/lang/String;)C
    .locals 4
    .param p0    # Ljava/lang/String;

    invoke-static {p0}, Lcom/konka/IntelligentControl/ioop/comClass/CTools;->BinstrToIntArray(Ljava/lang/String;)[I

    move-result-object v2

    const/4 v1, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_0

    int-to-char v3, v1

    return v3

    :cond_0
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    sub-int/2addr v3, v0

    aget v3, v2, v3

    shl-int/2addr v3, v0

    add-int/2addr v1, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static BinstrToIntArray(Ljava/lang/String;)[I
    .locals 4
    .param p0    # Ljava/lang/String;

    invoke-virtual {p0}, Ljava/lang/String;->toCharArray()[C

    move-result-object v2

    array-length v3, v2

    new-array v1, v3, [I

    const/4 v0, 0x0

    :goto_0
    array-length v3, v2

    if-lt v0, v3, :cond_0

    return-object v1

    :cond_0
    aget-char v3, v2, v0

    add-int/lit8 v3, v3, -0x30

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public static ByteToString([B)Ljava/lang/String;
    .locals 4
    .param p0    # [B

    const/4 v3, 0x0

    const/4 v0, 0x0

    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-lt v0, v2, :cond_1

    :cond_0
    if-nez v0, :cond_2

    const-string v2, ""

    :goto_1
    return-object v2

    :cond_1
    aget-byte v2, p0, v0

    if-eqz v2, :cond_0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    new-array v1, v0, [B

    invoke-static {p0, v3, v1, v3, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    new-instance v2, Ljava/lang/String;

    invoke-direct {v2, v1}, Ljava/lang/String;-><init>([B)V

    goto :goto_1
.end method

.method public static dis(FFFF)D
    .locals 3
    .param p0    # F
    .param p1    # F
    .param p2    # F
    .param p3    # F

    sub-float v0, p3, p1

    sub-float v1, p3, p1

    mul-float/2addr v0, v1

    sub-float v1, p2, p0

    sub-float v2, p2, p0

    mul-float/2addr v1, v2

    add-float/2addr v0, v1

    float-to-double v0, v0

    invoke-static {v0, v1}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v0

    return-wide v0
.end method

.method public static formatDuring(J)I
    .locals 6
    .param p0    # J

    const-wide/16 v4, 0x3e8

    const-wide/32 v2, 0xea60

    rem-long v2, p0, v2

    div-long/2addr v2, v4

    long-to-int v1, v2

    rem-long v2, p0, v4

    div-long/2addr v2, v4

    long-to-int v0, v2

    const-string v2, "CTools"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "seconds: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " milseconds: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v1
.end method

.method public static slope(FFFF)F
    .locals 2
    .param p0    # F
    .param p1    # F
    .param p2    # F
    .param p3    # F

    sub-float v0, p3, p1

    sub-float v1, p2, p0

    div-float/2addr v0, v1

    return v0
.end method

.method public static sqrt(D)D
    .locals 10
    .param p0    # D

    const-wide/high16 v8, 0x4000000000000000L    # 2.0

    div-double v0, p0, v8

    div-double v4, p0, v0

    add-double/2addr v4, v0

    div-double v2, v4, v8

    :goto_0
    sub-double v4, v0, v2

    invoke-static {v4, v5}, Ljava/lang/Math;->abs(D)D

    move-result-wide v4

    const-wide v6, 0x3cd203af9ee75616L    # 1.0E-15

    cmpl-double v4, v4, v6

    if-gtz v4, :cond_0

    return-wide v0

    :cond_0
    move-wide v0, v2

    div-double v4, p0, v0

    add-double/2addr v4, v0

    div-double v2, v4, v8

    goto :goto_0
.end method
