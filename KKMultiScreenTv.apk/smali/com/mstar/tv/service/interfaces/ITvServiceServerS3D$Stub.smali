.class public abstract Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;
.super Landroid/os/Binder;
.source "ITvServiceServerS3D.java"

# interfaces
.implements Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Stub"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub$Proxy;
    }
.end annotation


# static fields
.field private static final DESCRIPTOR:Ljava/lang/String; = "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

.field static final TRANSACTION_get3DDepthMode:I = 0xa

.field static final TRANSACTION_get3DOffsetMode:I = 0xc

.field static final TRANSACTION_get3DOutputAspectMode:I = 0x10

.field static final TRANSACTION_getAutoStartMode:I = 0xe

.field static final TRANSACTION_getDisplay3DTo2DMode:I = 0x8

.field static final TRANSACTION_getDisplayFormat:I = 0x6

.field static final TRANSACTION_getLRViewSwitch:I = 0x12

.field static final TRANSACTION_getSelfAdaptiveDetect:I = 0x2

.field static final TRANSACTION_getSelfAdaptiveLevel:I = 0x4

.field static final TRANSACTION_set3DDepthMode:I = 0x9

.field static final TRANSACTION_set3DOffsetMode:I = 0xb

.field static final TRANSACTION_set3DOutputAspectMode:I = 0xf

.field static final TRANSACTION_set3DTo2D:I = 0x7

.field static final TRANSACTION_setAutoStartMode:I = 0xd

.field static final TRANSACTION_setDisplayFormat:I = 0x5

.field static final TRANSACTION_setDisplayFormatForUI:I = 0x13

.field static final TRANSACTION_setLRViewSwitch:I = 0x11

.field static final TRANSACTION_setSelfAdaptiveDetect:I = 0x1

.field static final TRANSACTION_setSelfAdaptiveLevel:I = 0x3


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    const-string v0, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p0, p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    return-void
.end method

.method public static asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;
    .locals 2
    .param p0    # Landroid/os/IBinder;

    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    const-string v1, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-interface {p0, v1}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    if-eqz v1, :cond_1

    check-cast v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D;

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub$Proxy;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub$Proxy;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5
    .param p1    # I
    .param p2    # Landroid/os/Parcel;
    .param p3    # Landroid/os/Parcel;
    .param p4    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    const/4 v2, 0x0

    const/4 v3, 0x1

    sparse-switch p1, :sswitch_data_0

    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v3

    :goto_0
    return v3

    :sswitch_0
    const-string v2, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    :sswitch_1
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    :goto_1
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->setSelfAdaptiveDetect(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_0

    move v2, v3

    :cond_0
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->getSelfAdaptiveDetect()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_2

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_DETECT;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_2
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_3
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_4

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;

    :goto_2
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->setSelfAdaptiveLevel(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_3

    move v2, v3

    :cond_3
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_2

    :sswitch_4
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->getSelfAdaptiveLevel()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_5

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_SELFADAPTIVE_LEVEL;->writeToParcel(Landroid/os/Parcel;I)V

    goto :goto_0

    :cond_5
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto :goto_0

    :sswitch_5
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_7

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;

    :goto_3
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->setDisplayFormat(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_6

    move v2, v3

    :cond_6
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_7
    const/4 v0, 0x0

    goto :goto_3

    :sswitch_6
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->getDisplayFormat()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_8

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_DISPLAYFORMAT;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_8
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_7
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_a

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    :goto_4
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->set3DTo2D(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_9

    move v2, v3

    :cond_9
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_a
    const/4 v0, 0x0

    goto :goto_4

    :sswitch_8
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->getDisplay3DTo2DMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_b

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DTO2D;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_b
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_9
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->set3DDepthMode(I)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_c

    move v2, v3

    :cond_c
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_a
    const-string v2, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->get3DDepthMode()I

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_b
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->set3DOffsetMode(I)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_d

    move v2, v3

    :cond_d
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_c
    const-string v2, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->get3DOffsetMode()I

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    invoke-virtual {p3, v1}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_d
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_f

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;

    :goto_5
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->setAutoStartMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_e

    move v2, v3

    :cond_e
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_f
    const/4 v0, 0x0

    goto :goto_5

    :sswitch_e
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->getAutoStartMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_10

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_AUTOSTART;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_f
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_12

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;

    :goto_6
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->set3DOutputAspectMode(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_11

    move v2, v3

    :cond_11
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_12
    const/4 v0, 0x0

    goto :goto_6

    :sswitch_10
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->get3DOutputAspectMode()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_13

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_3DOUTPUTASPECT;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_13
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_11
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_15

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v4, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;

    :goto_7
    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->setLRViewSwitch(Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;)Z

    move-result v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_14

    move v2, v3

    :cond_14
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :cond_15
    const/4 v0, 0x0

    goto :goto_7

    :sswitch_12
    const-string v4, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v4}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->getLRViewSwitch()Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;

    move-result-object v1

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    if-eqz v1, :cond_16

    invoke-virtual {p3, v3}, Landroid/os/Parcel;->writeInt(I)V

    invoke-virtual {v1, p3, v3}, Lcom/mstar/tv/service/aidl/EN_ThreeD_Video_LRVIEWSWITCH;->writeToParcel(Landroid/os/Parcel;I)V

    goto/16 :goto_0

    :cond_16
    invoke-virtual {p3, v2}, Landroid/os/Parcel;->writeInt(I)V

    goto/16 :goto_0

    :sswitch_13
    const-string v2, "com.mstar.tv.service.interfaces.ITvServiceServerS3D"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/mstar/tv/service/interfaces/ITvServiceServerS3D$Stub;->setDisplayFormatForUI(I)V

    invoke-virtual {p3}, Landroid/os/Parcel;->writeNoException()V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x7 -> :sswitch_7
        0x8 -> :sswitch_8
        0x9 -> :sswitch_9
        0xa -> :sswitch_a
        0xb -> :sswitch_b
        0xc -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0x11 -> :sswitch_11
        0x12 -> :sswitch_12
        0x13 -> :sswitch_13
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
