.class public Lcom/mstar/tv/service/skin/PipSkin;
.super Ljava/lang/Object;
.source "PipSkin.java"


# instance fields
.field private handler:Landroid/os/Handler;

.field private iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

.field private isBindOk:Z

.field private subsrclist:[I

.field private superContext:Landroid/content/Context;

.field protected tvServicePipConnection:Landroid/content/ServiceConnection;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/mstar/tv/service/skin/PipSkin;->handler:Landroid/os/Handler;

    new-instance v0, Lcom/mstar/tv/service/skin/PipSkin$1;

    invoke-direct {v0, p0}, Lcom/mstar/tv/service/skin/PipSkin$1;-><init>(Lcom/mstar/tv/service/skin/PipSkin;)V

    iput-object v0, p0, Lcom/mstar/tv/service/skin/PipSkin;->tvServicePipConnection:Landroid/content/ServiceConnection;

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PipSkin;->superContext:Landroid/content/Context;

    return-void
.end method

.method static synthetic access$0(Lcom/mstar/tv/service/skin/PipSkin;Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;)V
    .locals 0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    return-void
.end method

.method static synthetic access$1(Lcom/mstar/tv/service/skin/PipSkin;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PipSkin;->handler:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public checkPipSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->checkPipSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public checkPipSupportOnSubSrc(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->checkPipSupportOnSubSrc(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public checkPopSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->checkPopSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public checkTravelingModeSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-ge v2, v3, :cond_1

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    sget-object v3, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v3}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    if-lt v2, v3, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->checkTravelingModeSupport(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public connect(Landroid/os/Handler;)Z
    .locals 5
    .param p1    # Landroid/os/Handler;

    const/4 v4, 0x0

    iput-object p1, p0, Lcom/mstar/tv/service/skin/PipSkin;->handler:Landroid/os/Handler;

    const-string v3, "tv_services"

    invoke-static {v3}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServer$Stub;->asInterface(Landroid/os/IBinder;)Lcom/mstar/tv/service/interfaces/ITvServiceServer;

    move-result-object v2

    if-eqz v2, :cond_0

    :try_start_0
    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServer;->getPipManager()Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    move-result-object v3

    iput-object v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->isBindOk:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-boolean v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->isBindOk:Z

    return v3

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/PipSkin;->isBindOk:Z

    goto :goto_0

    :cond_0
    iput-boolean v4, p0, Lcom/mstar/tv/service/skin/PipSkin;->isBindOk:Z

    goto :goto_0
.end method

.method public disable3dDualView()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->disable3dDualView()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disablePip()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->disablePip()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disablePop()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->disablePop()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disableTravelingMode()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->disableTravelingMode()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public disconnect()V
    .locals 0

    return-void
.end method

.method public enable3dDualView(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Z
    .locals 6
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p3    # Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;
    .param p4    # Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v3, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Pip service head is null!\t"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    aget-object v5, v5, v2

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    :goto_0
    return v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v4}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v4

    if-ge v3, v4, :cond_1

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v3

    sget-object v4, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v4}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v4

    if-lt v3, v4, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Input params out of boundary!\t"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    aget-object v5, v5, v2

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v1, v2

    goto :goto_0

    :cond_2
    :try_start_0
    const-string v3, "PipSkin"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "!!!!!!!!!!!!!!!mainWin is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!!!!!!!!!!!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v3, "PipSkin"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "!!!!!!!!!!!!!!!subWin is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!!!!!!!!!!!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v3, p1, p2, p3, p4}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enable3dDualView(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Z

    move-result v1

    const-string v3, "PipSkin"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "!!!!!!!!!!!!!!!reeeet is "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "!!!!!!!!!!!!!"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move v1, v2

    goto/16 :goto_0
.end method

.method public enablePipMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    :goto_0
    return-object v1

    :cond_0
    if-nez p2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input pointer is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_2
    iget v1, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    if-ltz v1, :cond_3

    iget v1, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    if-ltz v1, :cond_3

    iget v1, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    if-ltz v1, :cond_3

    iget v1, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    if-ltz v1, :cond_3

    iget v1, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    iget v2, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    add-int/2addr v1, v2

    const/16 v2, 0x780

    if-gt v1, v2, :cond_3

    iget v1, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    iget v2, p2, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    add-int/2addr v1, v2

    const/16 v2, 0x438

    if-le v1, v2, :cond_4

    :cond_3
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto/16 :goto_0

    :cond_4
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enablePipMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto/16 :goto_0
.end method

.method public enablePipTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p3    # Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    :goto_0
    return-object v1

    :cond_0
    if-nez p3, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input pointer is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_1
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-ge v1, v2, :cond_2

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_3

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_3
    iget v1, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    if-ltz v1, :cond_4

    iget v1, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    if-ltz v1, :cond_4

    iget v1, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    if-ltz v1, :cond_4

    iget v1, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    if-ltz v1, :cond_4

    iget v1, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    iget v2, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    add-int/2addr v1, v2

    const/16 v2, 0x780

    if-gt v1, v2, :cond_4

    iget v1, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    iget v2, p3, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    add-int/2addr v1, v2

    const/16 v2, 0x438

    if-le v1, v2, :cond_5

    :cond_4
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto/16 :goto_0

    :cond_5
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1, p2, p3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enablePipTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto/16 :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto/16 :goto_0
.end method

.method public enablePopMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enablePopMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0
.end method

.method public enablePopTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enablePopTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0
.end method

.method public enableTravelingModeMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enableTravelingModeMM(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0
.end method

.method public enableTravelingModeTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .param p2    # Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    const/4 v4, 0x0

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {p1}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v1

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NUM:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    invoke-virtual {v2}, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->ordinal()I

    move-result v2

    if-lt v1, v2, :cond_2

    :cond_1
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Input params out of boundary!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0

    :cond_2
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1, p2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->enableTravelingModeTV(Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;)Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;->E_PIP_NOT_SUPPORT:Lcom/mstar/tv/service/aidl/EN_PIP_RETURN;

    goto :goto_0
.end method

.method public getIsDualViewOn()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->getIsDualViewOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsPipOn()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->getIsPipOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getIsPopOn()Z
    .locals 2

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->getIsPopOn()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    :goto_0
    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getMainWindowSourceList()Lcom/mstar/tv/service/aidl/IntArrayList;
    .locals 7

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v3, :cond_0

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v4, "ssssssssssssss"

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Pip service handler is null!\t"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v5, Ljava/lang/Exception;

    invoke-direct {v5}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v5}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v5

    const/4 v6, 0x0

    aget-object v5, v5, v6

    invoke-virtual {v5}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v1, v2

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v3}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->getMainWindowSourceList()Lcom/mstar/tv/service/aidl/IntArrayList;

    move-result-object v1

    sget-object v3, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "=======Main source list:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "================="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    move-object v1, v2

    goto :goto_0
.end method

.method public getSubInputSource()Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    .locals 5

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NONE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    :goto_0
    return-object v1

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->getSubInputSource()Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;->E_INPUT_SOURCE_NONE:Lcom/mstar/tv/service/aidl/EN_INPUT_SOURCE_TYPE;

    goto :goto_0
.end method

.method public getSubWindowSourceList(Z)Lcom/mstar/tv/service/aidl/IntArrayList;
    .locals 6
    .param p1    # Z

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service handler is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    const/4 v5, 0x0

    aget-object v4, v4, v5

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-object v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->getSubWindowSourceList(Z)Lcom/mstar/tv/service/aidl/IntArrayList;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isConnectionOk()Z
    .locals 1

    iget-object v0, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isDualViewEnabled()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->isDualViewEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isPipEnabled()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->isPipEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isPipModeEnabled()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->isPipModeEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public isPopEnabled()Z
    .locals 5

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->isPopEnabled()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setDualViewOnFlag(Z)Z
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->setDualViewOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPipDisplayFocusWindow(Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;)V
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;

    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v1, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Pip service head is null!\t"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v3}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v3

    const/4 v4, 0x0

    aget-object v3, v3, v4

    invoke-virtual {v3}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return-void

    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->setPipDisplayFocusWindow(Lcom/mstar/tv/service/aidl/EN_SCALER_WIN;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPipOnFlag(Z)Z
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->setPipOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPipSubwindow(Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Z
    .locals 5
    .param p1    # Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    if-nez v2, :cond_0

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pip service head is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_0
    return v1

    :cond_0
    if-nez p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input pointer is null!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_1
    iget v2, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    if-ltz v2, :cond_2

    iget v2, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    if-ltz v2, :cond_2

    iget v2, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    if-ltz v2, :cond_2

    iget v2, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    if-ltz v2, :cond_2

    iget v2, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->x:I

    iget v3, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->width:I

    add-int/2addr v2, v3

    const/16 v3, 0x780

    if-gt v2, v3, :cond_2

    iget v2, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->y:I

    iget v3, p1, Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;->height:I

    add-int/2addr v2, v3

    const/16 v3, 0x438

    if-le v2, v3, :cond_3

    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Input params out of boundary!\t"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-virtual {v4}, Ljava/lang/Exception;->getStackTrace()[Ljava/lang/StackTraceElement;

    move-result-object v4

    aget-object v4, v4, v1

    invoke-virtual {v4}, Ljava/lang/StackTraceElement;->getMethodName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_3
    :try_start_0
    iget-object v2, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v2, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->setPipSubwindow(Lcom/mstar/tv/service/aidl/VIDEO_WINDOW_TYPE;)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method

.method public setPopOnFlag(Z)Z
    .locals 2
    .param p1    # Z

    :try_start_0
    iget-object v1, p0, Lcom/mstar/tv/service/skin/PipSkin;->iTvServicePip:Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;

    invoke-interface {v1, p1}, Lcom/mstar/tv/service/interfaces/ITvServiceServerPip;->setPopOnFlag(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v1, 0x1

    return v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_0
.end method
