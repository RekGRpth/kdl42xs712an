.class public final enum Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;
.super Ljava/lang/Enum;
.source "EN_FIRST_SERVICE_TYPE.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

.field public static final enum E_AC_DC_BOOT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

.field public static final enum E_AUTO_SCAN:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

.field public static final enum E_DEFAULT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

.field public static final enum E_MENU_SCAN:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

.field public static final enum E_ON_TIME_BOOT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    const-string v1, "E_ON_TIME_BOOT"

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_ON_TIME_BOOT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    const-string v1, "E_AC_DC_BOOT"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_AC_DC_BOOT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    const-string v1, "E_AUTO_SCAN"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_AUTO_SCAN:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    const-string v1, "E_MENU_SCAN"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_MENU_SCAN:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    const-string v1, "E_DEFAULT"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_DEFAULT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    const/4 v0, 0x5

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_ON_TIME_BOOT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    aput-object v1, v0, v2

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_AC_DC_BOOT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_AUTO_SCAN:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_MENU_SCAN:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->E_DEFAULT:Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    aput-object v1, v0, v6

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_FIRST_SERVICE_TYPE;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
