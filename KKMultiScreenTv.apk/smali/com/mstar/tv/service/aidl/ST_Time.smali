.class public Lcom/mstar/tv/service/aidl/ST_Time;
.super Landroid/text/format/Time;
.source "ST_Time.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/ST_Time;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/ST_Time$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/ST_Time$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/ST_Time;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    return-void
.end method

.method public constructor <init>(IIIIII)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # I
    .param p5    # I
    .param p6    # I

    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/ST_Time;->year:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/ST_Time;->month:I

    iput p3, p0, Lcom/mstar/tv/service/aidl/ST_Time;->monthDay:I

    iput p4, p0, Lcom/mstar/tv/service/aidl/ST_Time;->hour:I

    iput p5, p0, Lcom/mstar/tv/service/aidl/ST_Time;->minute:I

    iput p6, p0, Lcom/mstar/tv/service/aidl/ST_Time;->second:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 1
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Landroid/text/format/Time;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->year:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->month:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->monthDay:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->hour:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->minute:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->second:I

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/ST_Time;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/ST_Time;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->year:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->month:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->monthDay:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->hour:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->minute:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_Time;->second:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
