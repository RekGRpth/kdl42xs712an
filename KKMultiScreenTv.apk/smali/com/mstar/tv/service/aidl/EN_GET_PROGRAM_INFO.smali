.class public final enum Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;
.super Ljava/lang/Enum;
.source "EN_GET_PROGRAM_INFO.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;",
        ">;",
        "Landroid/os/Parcelable;"
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;",
            ">;"
        }
    .end annotation
.end field

.field private static final synthetic ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_AFT_OFFSET:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_AUDIO_STANDARD:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_CHANNEL_INDEX:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_DUAL_AUDIO_SELECTED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_DUMMY1:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_DUMMY2:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_FINE_TUNE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_PROGRAM_PLL_DATA:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_SORTING_PRIORITY:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_GET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_IS_AFT_NEED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_IS_DIRECT_TUNED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_IS_PROGRAM_HIDE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_IS_PROGRAM_LOCKED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_IS_PROGRAM_SKIPPED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

.field public static final enum E_IS_REALTIME_AUDIO_DETECTION_ENABLE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_FINE_TUNE"

    invoke-direct {v0, v1, v3}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_FINE_TUNE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_PROGRAM_PLL_DATA"

    invoke-direct {v0, v1, v4}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_PROGRAM_PLL_DATA:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_AUDIO_STANDARD"

    invoke-direct {v0, v1, v5}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_AUDIO_STANDARD:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_VIDEO_STANDARD_OF_PROGRAM"

    invoke-direct {v0, v1, v6}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_DUAL_AUDIO_SELECTED"

    invoke-direct {v0, v1, v7}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_DUAL_AUDIO_SELECTED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_IS_PROGRAM_SKIPPED"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_PROGRAM_SKIPPED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_IS_PROGRAM_LOCKED"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_PROGRAM_LOCKED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_IS_PROGRAM_HIDE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_PROGRAM_HIDE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_IS_AFT_NEED"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_AFT_NEED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_IS_DIRECT_TUNED"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_DIRECT_TUNED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_AFT_OFFSET"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_AFT_OFFSET:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_IS_REALTIME_AUDIO_DETECTION_ENABLE"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_REALTIME_AUDIO_DETECTION_ENABLE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_DUMMY1"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_DUMMY1:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_SORTING_PRIORITY"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_SORTING_PRIORITY:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_CHANNEL_INDEX"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_CHANNEL_INDEX:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const-string v1, "E_GET_DUMMY2"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_DUMMY2:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    const/16 v0, 0x10

    new-array v0, v0, [Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_FINE_TUNE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v1, v0, v3

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_PROGRAM_PLL_DATA:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v1, v0, v4

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_AUDIO_STANDARD:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v1, v0, v5

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_VIDEO_STANDARD_OF_PROGRAM:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v1, v0, v6

    sget-object v1, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_DUAL_AUDIO_SELECTED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_PROGRAM_SKIPPED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_PROGRAM_LOCKED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_PROGRAM_HIDE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_AFT_NEED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_DIRECT_TUNED:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_AFT_OFFSET:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_IS_REALTIME_AUDIO_DETECTION_ENABLE:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_DUMMY1:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_SORTING_PRIORITY:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_CHANNEL_INDEX:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->E_GET_DUMMY2:Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    aput-object v2, v0, v1

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    new-instance v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;
    .locals 1

    const-class v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    return-object v0
.end method

.method public static values()[Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->ENUM$VALUES:[Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    array-length v1, v0

    new-array v2, v1, [Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    invoke-virtual {p0}, Lcom/mstar/tv/service/aidl/EN_GET_PROGRAM_INFO;->ordinal()I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void
.end method
