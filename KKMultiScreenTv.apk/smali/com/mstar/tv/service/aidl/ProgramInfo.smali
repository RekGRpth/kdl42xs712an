.class public Lcom/mstar/tv/service/aidl/ProgramInfo;
.super Ljava/lang/Object;
.source "ProgramInfo.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/ProgramInfo;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public antennaType:I

.field public favorite:S

.field public frequency:I

.field public isDelete:Z

.field public isHide:Z

.field public isLock:Z

.field public isScramble:Z

.field public isSkip:Z

.field public isVisible:Z

.field public majorNum:S

.field public minorNum:S

.field public number:I

.field public progId:I

.field public queryIndex:I

.field public screenMuteStatus:I

.field public serviceId:I

.field public serviceName:Ljava/lang/String;

.field public serviceType:S

.field public transportStreamId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/ProgramInfo$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/ProgramInfo$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/ProgramInfo;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1    # I

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 p1, 0x0

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->number:I

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->majorNum:S

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->minorNum:S

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->progId:I

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->favorite:S

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isLock:Z

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isSkip:Z

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isScramble:Z

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isDelete:Z

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isVisible:Z

    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isHide:Z

    iput-short v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceType:S

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->screenMuteStatus:I

    const-string v0, ""

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceName:Ljava/lang/String;

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->frequency:I

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->transportStreamId:I

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceId:I

    iput v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->antennaType:I

    return-void
.end method

.method public constructor <init>(IISSISZZZZZZSILjava/lang/String;IIII)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # S
    .param p4    # S
    .param p5    # I
    .param p6    # S
    .param p7    # Z
    .param p8    # Z
    .param p9    # Z
    .param p10    # Z
    .param p11    # Z
    .param p12    # Z
    .param p13    # S
    .param p14    # I
    .param p15    # Ljava/lang/String;
    .param p16    # I
    .param p17    # I
    .param p18    # I
    .param p19    # I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->queryIndex:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->number:I

    iput-short p3, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->majorNum:S

    iput-short p4, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->minorNum:S

    iput p5, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->progId:I

    iput-short p6, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->favorite:S

    iput-boolean p7, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isLock:Z

    iput-boolean p8, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isSkip:Z

    iput-boolean p9, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isScramble:Z

    iput-boolean p10, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isDelete:Z

    iput-boolean p11, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isVisible:Z

    iput-boolean p12, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isHide:Z

    iput-short p13, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceType:S

    iput p14, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->screenMuteStatus:I

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceName:Ljava/lang/String;

    move/from16 v0, p16

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->frequency:I

    move/from16 v0, p17

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->transportStreamId:I

    move/from16 v0, p18

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceId:I

    move/from16 v0, p19

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->antennaType:I

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    const/4 v2, 0x0

    const/4 v1, 0x1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->queryIndex:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->number:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->majorNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->minorNum:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->progId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->favorite:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isLock:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isSkip:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_2

    move v0, v1

    :goto_2
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isScramble:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_3
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isDelete:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_4
    iput-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isVisible:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-ne v0, v1, :cond_5

    :goto_5
    iput-boolean v1, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isHide:Z

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    int-to-short v0, v0

    iput-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceType:S

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->screenMuteStatus:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->frequency:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->transportStreamId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceId:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->antennaType:I

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/ProgramInfo;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/ProgramInfo;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    const/4 v1, 0x1

    const/4 v2, 0x0

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->queryIndex:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->number:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->majorNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->minorNum:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->progId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->favorite:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isLock:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isSkip:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isScramble:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_2
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isDelete:Z

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isVisible:Z

    if-eqz v0, :cond_4

    move v0, v1

    :goto_4
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-boolean v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->isHide:Z

    if-eqz v0, :cond_5

    :goto_5
    invoke-virtual {p1, v1}, Landroid/os/Parcel;->writeInt(I)V

    iget-short v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceType:S

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->screenMuteStatus:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->frequency:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->transportStreamId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->serviceId:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ProgramInfo;->antennaType:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    return-void

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v2

    goto :goto_1

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v2

    goto :goto_3

    :cond_4
    move v0, v2

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_5
.end method
