.class public Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;
.super Ljava/lang/Object;
.source "ST_VIDEO_INFO.java"

# interfaces
.implements Landroid/os/Parcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;",
            ">;"
        }
    .end annotation
.end field

.field private static final EN_SCAN_TYPE:Ljava/lang/ClassLoader;


# instance fields
.field public enScanType:Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

.field public s16FrameRate:I

.field public s16HResolution:I

.field public s16VResolution:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO$1;

    invoke-direct {v0}, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO$1;-><init>()V

    sput-object v0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->CREATOR:Landroid/os/Parcelable$Creator;

    const/4 v0, 0x0

    sput-object v0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->EN_SCAN_TYPE:Ljava/lang/ClassLoader;

    return-void
.end method

.method public constructor <init>(IIILcom/mstar/tv/service/aidl/EN_SCAN_TYPE;)V
    .locals 0
    .param p1    # I
    .param p2    # I
    .param p3    # I
    .param p4    # Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16HResolution:I

    iput p2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16VResolution:I

    iput p3, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16FrameRate:I

    iput-object p4, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->enScanType:Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    return-void
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 3
    .param p1    # Landroid/os/Parcel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16HResolution:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16VResolution:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16FrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->enScanType:Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16HResolution:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16VResolution:I

    invoke-virtual {p1}, Landroid/os/Parcel;->readByte()B

    move-result v0

    iput v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16FrameRate:I

    sget-object v0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->EN_SCAN_TYPE:Ljava/lang/ClassLoader;

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    iput-object v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->enScanType:Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    return-void
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1    # Landroid/os/Parcel;
    .param p2    # I

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16HResolution:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16VResolution:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16FrameRate:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    iget-object v0, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->enScanType:Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v1, Ljava/lang/StringBuilder;

    iget v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16HResolution:I

    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16VResolution:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->s16FrameRate:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/mstar/tv/service/aidl/ST_VIDEO_INFO;->enScanType:Lcom/mstar/tv/service/aidl/EN_SCAN_TYPE;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    return-void
.end method
