.class public interface abstract Lcom/bestv/ott/online/auth/IBesTVAuthService;
.super Ljava/lang/Object;
.source "IBesTVAuthService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/bestv/ott/online/auth/IBesTVAuthService$Stub;
    }
.end annotation


# virtual methods
.method public abstract getProfile(Ljava/lang/String;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract ifAuthOK()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
