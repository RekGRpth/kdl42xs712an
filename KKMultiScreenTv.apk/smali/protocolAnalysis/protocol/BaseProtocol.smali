.class public abstract LprotocolAnalysis/protocol/BaseProtocol;
.super Ljava/lang/Object;
.source "BaseProtocol.java"


# static fields
.field public static final MB_TYPE:S = 0x12cs

.field public static final OT_TYPE:S = 0x12cs

.field public static final PC_TYPE:S = 0xc8s

.field public static final TV_TYPE:S = 0x64s

.field private static endian:Z

.field public static tag:Z


# instance fields
.field public devType:S

.field public packetType:S


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, LprotocolAnalysis/protocol/BaseProtocol;->tag:Z

    const/4 v0, 0x0

    sput-boolean v0, LprotocolAnalysis/protocol/BaseProtocol;->endian:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x0

    iput-short v0, p0, LprotocolAnalysis/protocol/BaseProtocol;->packetType:S

    const/16 v0, 0x12c

    iput-short v0, p0, LprotocolAnalysis/protocol/BaseProtocol;->devType:S

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;->GetPlatformEndian()V

    return-void
.end method

.method private GetPlatformEndian()V
    .locals 9

    const/4 v8, 0x4

    const/4 v6, 0x0

    const/4 v5, 0x1

    new-array v4, v8, [B

    aput-byte v5, v4, v6

    const/16 v1, 0xff

    const/4 v3, 0x0

    const/4 v2, 0x0

    const/4 v0, 0x0

    :goto_0
    if-lt v0, v8, :cond_0

    if-ne v2, v5, :cond_1

    :goto_1
    sput-boolean v5, LprotocolAnalysis/protocol/BaseProtocol;->endian:Z

    return-void

    :cond_0
    shl-int/lit8 v2, v2, 0x8

    aget-byte v7, v4, v0

    and-int v3, v7, v1

    or-int/2addr v2, v3

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v5, v6

    goto :goto_1
.end method

.method public static byteToInt([B)I
    .locals 10
    .param p0    # [B

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    sget-boolean v5, LprotocolAnalysis/protocol/BaseProtocol;->endian:Z

    if-nez v5, :cond_0

    aget-byte v5, p0, v9

    and-int/lit16 v1, v5, 0xff

    aget-byte v5, p0, v8

    and-int/lit16 v2, v5, 0xff

    aget-byte v5, p0, v7

    and-int/lit16 v3, v5, 0xff

    aget-byte v5, p0, v6

    and-int/lit16 v4, v5, 0xff

    :goto_0
    shl-int/lit8 v4, v4, 0x18

    shl-int/lit8 v3, v3, 0x10

    shl-int/lit8 v2, v2, 0x8

    or-int v5, v1, v2

    or-int/2addr v5, v3

    or-int v0, v5, v4

    return v0

    :cond_0
    aget-byte v5, p0, v6

    and-int/lit16 v1, v5, 0xff

    aget-byte v5, p0, v7

    and-int/lit16 v2, v5, 0xff

    aget-byte v5, p0, v8

    and-int/lit16 v3, v5, 0xff

    aget-byte v5, p0, v9

    and-int/lit16 v4, v5, 0xff

    goto :goto_0
.end method

.method public static byteToShort([B)S
    .locals 6
    .param p0    # [B

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x0

    sget-boolean v3, LprotocolAnalysis/protocol/BaseProtocol;->endian:Z

    if-eqz v3, :cond_0

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    int-to-short v1, v3

    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xff

    int-to-short v2, v3

    :goto_0
    shl-int/lit8 v3, v2, 0x8

    int-to-short v2, v3

    or-int v3, v1, v2

    int-to-short v0, v3

    return v0

    :cond_0
    aget-byte v3, p0, v5

    and-int/lit16 v3, v3, 0xff

    int-to-short v1, v3

    aget-byte v3, p0, v4

    and-int/lit16 v3, v3, 0xff

    int-to-short v2, v3

    goto :goto_0
.end method

.method public static intToByte(I[BI)V
    .locals 5
    .param p0    # I
    .param p1    # [B
    .param p2    # I

    const/high16 v4, 0xff0000

    const v3, 0xff00

    const/high16 v2, -0x1000000

    sget-boolean v0, LprotocolAnalysis/protocol/BaseProtocol;->endian:Z

    if-nez v0, :cond_0

    add-int/lit8 v0, p2, 0x3

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x2

    and-int v1, v3, p0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    and-int v1, v4, p0

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x0

    and-int v1, v2, p0

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v0, p2, 0x0

    and-int/lit16 v1, p0, 0xff

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    and-int v1, v3, p0

    shr-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x2

    and-int v1, v4, p0

    shr-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x3

    and-int v1, v2, p0

    shr-int/lit8 v1, v1, 0x18

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    goto :goto_0
.end method

.method public static shortToByte(S[BI)V
    .locals 2
    .param p0    # S
    .param p1    # [B
    .param p2    # I

    sget-boolean v0, LprotocolAnalysis/protocol/BaseProtocol;->endian:Z

    if-eqz v0, :cond_0

    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x0

    shr-int/lit8 v1, p0, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    :goto_0
    return-void

    :cond_0
    add-int/lit8 v0, p2, 0x0

    shr-int/lit8 v1, p0, 0x8

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    add-int/lit8 v0, p2, 0x1

    shr-int/lit8 v1, p0, 0x0

    int-to-byte v1, v1

    aput-byte v1, p1, v0

    goto :goto_0
.end method


# virtual methods
.method public abstract format([B)V
.end method

.method public abstract printf([B)Ljava/lang/String;
.end method

.method public abstract sizeOf()I
.end method
