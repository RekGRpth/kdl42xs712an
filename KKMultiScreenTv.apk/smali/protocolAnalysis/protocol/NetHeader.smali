.class public LprotocolAnalysis/protocol/NetHeader;
.super LprotocolAnalysis/protocol/BaseProtocol;
.source "NetHeader.java"


# instance fields
.field public data_len:S

.field public data_type:S


# direct methods
.method public constructor <init>(SS)V
    .locals 0
    .param p1    # S
    .param p2    # S

    invoke-direct {p0}, LprotocolAnalysis/protocol/BaseProtocol;-><init>()V

    iput-short p1, p0, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    iput-short p2, p0, LprotocolAnalysis/protocol/NetHeader;->data_type:S

    return-void
.end method


# virtual methods
.method public Format([BI)V
    .locals 2
    .param p1    # [B
    .param p2    # I

    iget-short v0, p0, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    add-int/lit8 v1, p2, 0x0

    invoke-static {v0, p1, v1}, LprotocolAnalysis/protocol/NetHeader;->shortToByte(S[BI)V

    iget-short v0, p0, LprotocolAnalysis/protocol/NetHeader;->data_type:S

    add-int/lit8 v1, p2, 0x2

    invoke-static {v0, p1, v1}, LprotocolAnalysis/protocol/NetHeader;->shortToByte(S[BI)V

    return-void
.end method

.method public ReadIn([BI)V
    .locals 4
    .param p1    # [B
    .param p2    # I

    const/4 v3, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x5

    new-array v0, v1, [B

    add-int/lit8 v1, p2, 0x0

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v2, v0, v3

    invoke-static {v0}, LprotocolAnalysis/protocol/NetHeader;->byteToShort([B)S

    move-result v1

    iput-short v1, p0, LprotocolAnalysis/protocol/NetHeader;->data_len:S

    add-int/lit8 v1, p2, 0x2

    invoke-static {p1, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v2, v0, v3

    invoke-static {v0}, LprotocolAnalysis/protocol/NetHeader;->byteToShort([B)S

    move-result v1

    iput-short v1, p0, LprotocolAnalysis/protocol/NetHeader;->data_type:S

    return-void
.end method

.method public format([B)V
    .locals 0
    .param p1    # [B

    return-void
.end method

.method public printf([B)Ljava/lang/String;
    .locals 7
    .param p1    # [B

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v3, 0x5

    new-array v2, v3, [B

    invoke-static {p1, v5, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v5, v2, v6

    new-instance v0, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, LprotocolAnalysis/protocol/NetHeader;->byteToShort([B)S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-static {p1, v6, v2, v5, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    aput-byte v5, v2, v6

    new-instance v1, Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v2}, LprotocolAnalysis/protocol/NetHeader;->byteToShort([B)S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->toString(S)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, ","

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    return-object v3
.end method

.method public sizeOf()I
    .locals 1

    const/4 v0, 0x4

    return v0
.end method
