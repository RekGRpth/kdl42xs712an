.class LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;
.super Landroid/content/BroadcastReceiver;
.source "packetHandle.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = LprotocolAnalysis/analysis/packetHandle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DataPlayerStateReceiver"
.end annotation


# instance fields
.field final synthetic this$0:LprotocolAnalysis/analysis/packetHandle;


# direct methods
.method private constructor <init>(LprotocolAnalysis/analysis/packetHandle;)V
    .locals 0

    iput-object p1, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(LprotocolAnalysis/analysis/packetHandle;LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;)V
    .locals 0

    invoke-direct {p0, p1}, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;-><init>(LprotocolAnalysis/analysis/packetHandle;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # Landroid/content/Intent;

    const/4 v3, 0x0

    const-string v1, "TAG"

    const-string v2, "onReceive-------DataPlayerStateReceiver----------"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    const-string v2, "playerState"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, LprotocolAnalysis/analysis/packetHandle;->playerState:I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    const-string v2, "currentTime"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, LprotocolAnalysis/analysis/packetHandle;->currentTime:I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    const-string v2, "DurationTime"

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v2

    iput v2, v1, LprotocolAnalysis/analysis/packetHandle;->DurationTime:I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    const-string v2, "fileUrl"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, LprotocolAnalysis/analysis/packetHandle;->fileUrl:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, LprotocolAnalysis/analysis/packetHandle;->switchSuccess:Ljava/lang/Boolean;

    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "playerState===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget v3, v3, LprotocolAnalysis/analysis/packetHandle;->playerState:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "currentTime===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget v3, v3, LprotocolAnalysis/analysis/packetHandle;->currentTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "DurationTime===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget v3, v3, LprotocolAnalysis/analysis/packetHandle;->DurationTime:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "fileUrl===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v3, v3, LprotocolAnalysis/analysis/packetHandle;->fileUrl:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "TAG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "mobileShareIP===="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LprotocolAnalysis/analysis/packetHandle;->mobileShareIP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sendPlayerStateToMobile@@"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v3, LprotocolAnalysis/analysis/packetHandle;->mobileShareIP:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    return-void
.end method
