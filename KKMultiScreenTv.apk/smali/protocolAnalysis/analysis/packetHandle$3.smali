.class LprotocolAnalysis/analysis/packetHandle$3;
.super Ljava/lang/Object;
.source "packetHandle.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = LprotocolAnalysis/analysis/packetHandle;->startPlayWeiKan(Landroid/content/Context;[B)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:LprotocolAnalysis/analysis/packetHandle;

.field private final synthetic val$mContext:Landroid/content/Context;

.field private final synthetic val$packet:[B


# direct methods
.method constructor <init>(LprotocolAnalysis/analysis/packetHandle;[BLandroid/content/Context;)V
    .locals 0

    iput-object p1, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iput-object p2, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$packet:[B

    iput-object p3, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    const/4 v6, 0x0

    const/4 v5, 0x1

    const-string v3, "weikan"

    const-string v4, "have been in thread to play!!!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$packet:[B

    # invokes: LprotocolAnalysis/analysis/packetHandle;->getWeiKanInfo([B)V
    invoke-static {v3, v4}, LprotocolAnalysis/analysis/packetHandle;->access$3(LprotocolAnalysis/analysis/packetHandle;[B)V

    new-instance v3, LprotocolAnalysis/analysis/packetAnalytic;

    invoke-direct {v3}, LprotocolAnalysis/analysis/packetAnalytic;-><init>()V

    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$4(LprotocolAnalysis/analysis/packetAnalytic;)V

    # getter for: LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$5()Ljava/lang/String;

    move-result-object v3

    if-eqz v3, :cond_0

    # getter for: LprotocolAnalysis/analysis/packetHandle;->mWeiKanURLType:I
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$6()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    :cond_0
    :goto_0
    return-void

    :pswitch_0
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$5()Ljava/lang/String;

    move-result-object v4

    # invokes: LprotocolAnalysis/analysis/packetHandle;->analysis(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, LprotocolAnalysis/analysis/packetHandle;->access$7(LprotocolAnalysis/analysis/packetHandle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$8(Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v3

    sget-object v4, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_weikan:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v4}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v4

    iput v4, v3, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const/high16 v3, 0x10000000

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$5()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-virtual {v3, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :pswitch_1
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$5()Ljava/lang/String;

    move-result-object v4

    # invokes: LprotocolAnalysis/analysis/packetHandle;->analysisURL(Ljava/lang/String;)V
    invoke-static {v3, v4}, LprotocolAnalysis/analysis/packetHandle;->access$9(LprotocolAnalysis/analysis/packetHandle;Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v4, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v4}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v4

    if-eq v3, v4, :cond_1

    :try_start_0
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, LprotocolAnalysis/analysis/packetHandle;->switchMediaPlayer(Landroid/content/Context;)V

    const-wide/16 v3, 0x3e8

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LprotocolAnalysis/analysis/packetHandle;->startPlayWeikan(Landroid/content/Context;Ljava/lang/Boolean;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto :goto_0

    :cond_1
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    if-ne v3, v5, :cond_2

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LprotocolAnalysis/analysis/packetHandle;->sendWeikanMediaInfo(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    :cond_2
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LprotocolAnalysis/analysis/packetHandle;->startPlayWeikan(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    :pswitch_2
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;
    invoke-static {}, LprotocolAnalysis/analysis/packetHandle;->access$5()Ljava/lang/String;

    move-result-object v4

    # invokes: LprotocolAnalysis/analysis/packetHandle;->analysis(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v3, v4}, LprotocolAnalysis/analysis/packetHandle;->access$7(LprotocolAnalysis/analysis/packetHandle;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$8(Ljava/lang/String;)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v4, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v4}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v4

    if-eq v3, v4, :cond_3

    :try_start_1
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-virtual {v3, v4}, LprotocolAnalysis/analysis/packetHandle;->switchMediaPlayer(Landroid/content/Context;)V

    const-wide/16 v3, 0x3e8

    invoke-static {v3, v4}, Ljava/lang/Thread;->sleep(J)V

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LprotocolAnalysis/analysis/packetHandle;->startPlayWeikan(Landroid/content/Context;Ljava/lang/Boolean;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_0

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V

    goto/16 :goto_0

    :cond_3
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    # getter for: LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    invoke-static {v3}, LprotocolAnalysis/analysis/packetHandle;->access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    move-result-object v3

    iget v3, v3, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    if-ne v3, v5, :cond_4

    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LprotocolAnalysis/analysis/packetHandle;->sendWeikanMediaInfo(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    :cond_4
    iget-object v3, p0, LprotocolAnalysis/analysis/packetHandle$3;->this$0:LprotocolAnalysis/analysis/packetHandle;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle$3;->val$mContext:Landroid/content/Context;

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, LprotocolAnalysis/analysis/packetHandle;->startPlayWeikan(Landroid/content/Context;Ljava/lang/Boolean;)V

    goto/16 :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method
