.class public LprotocolAnalysis/analysis/packetHandle;
.super Ljava/lang/Object;
.source "packetHandle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;,
        LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;,
        LprotocolAnalysis/analysis/packetHandle$EnumSubSurceList;
    }
.end annotation


# static fields
.field public static ack:LprotocolAnalysis/protocol/AlivePacketAck; = null

.field private static mWeiKanType:I = 0x0

.field private static mWeiKanURL:Ljava/lang/String; = null

.field private static mWeiKanURLType:I = 0x0

.field public static mobileShareIP:Ljava/lang/String; = null

.field private static packetAny:LprotocolAnalysis/analysis/packetAnalytic; = null

.field public static final sign:Ljava/lang/String; = "@@"

.field public static source:Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

.field public static switchSuccess:Ljava/lang/Boolean;


# instance fields
.field DurationTime:I

.field private SHARELOG:Ljava/lang/String;

.field private TAG:Ljava/lang/String;

.field currentTime:I

.field dataReceiver:LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;

.field private dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

.field deviceNameInfo:[B

.field private durationList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field filePath:[B

.field fileSize:J

.field fileUrl:Ljava/lang/String;

.field haveNextFlag:I

.field private mBFlag:Ljava/lang/String;

.field private final mPlayLive:I

.field private final mPlayVod:I

.field private final mURLFakeWeb:I

.field private final mURLScript:I

.field private final mURLVideo:I

.field private final mURLVideoID:I

.field private final mURLWebSite:I

.field private mVideoTitle:Ljava/lang/String;

.field private m_serviceProvider:Lcom/konka/kkinterface/tv/TvDeskProvider;

.field mediaPath:Ljava/lang/String;

.field mediaPathTmp:Ljava/lang/String;

.field mediaShareState:I

.field private mobileIP:Ljava/lang/String;

.field musicdestroy:I

.field packetPro:LprotocolAnalysis/protocol/packetProtocol;

.field picdestroy:I

.field platforminfo:[B

.field playThread:Ljava/lang/Thread;

.field playerState:I

.field playerStateReceiver:LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;

.field resumePos:I

.field private sendFlag:LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

.field private urlList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field videodestroy:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    new-instance v0, LprotocolAnalysis/analysis/packetAnalytic;

    invoke-direct {v0}, LprotocolAnalysis/analysis/packetAnalytic;-><init>()V

    sput-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    new-instance v0, LprotocolAnalysis/protocol/AlivePacketAck;

    invoke-direct {v0}, LprotocolAnalysis/protocol/AlivePacketAck;-><init>()V

    sput-object v0, LprotocolAnalysis/analysis/packetHandle;->ack:LprotocolAnalysis/protocol/AlivePacketAck;

    sput-object v1, LprotocolAnalysis/analysis/packetHandle;->mobileShareIP:Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    sput-object v0, LprotocolAnalysis/analysis/packetHandle;->switchSuccess:Ljava/lang/Boolean;

    sput-object v1, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 6

    const/16 v5, 0x10

    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "packetHandle"

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v0, "0.0.0."

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->mobileIP:Ljava/lang/String;

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->packetPro:LprotocolAnalysis/protocol/packetProtocol;

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->mediaShareState:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->picdestroy:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->musicdestroy:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->videodestroy:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->playerState:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->currentTime:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->DurationTime:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->fileUrl:Ljava/lang/String;

    const/16 v0, 0x200

    new-array v0, v0, [B

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->filePath:[B

    new-array v0, v5, [B

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->platforminfo:[B

    new-array v0, v5, [B

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->sendFlag:LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->mBFlag:Ljava/lang/String;

    new-instance v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    invoke-direct {v0}, Lcom/konka/kkmultiscreen/DataService$BroadMsg;-><init>()V

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput-object v3, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    const-string v0, "PlayerManager"

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->mPlayLive:I

    iput v4, p0, LprotocolAnalysis/analysis/packetHandle;->mPlayVod:I

    iput v2, p0, LprotocolAnalysis/analysis/packetHandle;->mURLVideoID:I

    iput v4, p0, LprotocolAnalysis/analysis/packetHandle;->mURLWebSite:I

    const/4 v0, 0x2

    iput v0, p0, LprotocolAnalysis/analysis/packetHandle;->mURLScript:I

    const/4 v0, 0x3

    iput v0, p0, LprotocolAnalysis/analysis/packetHandle;->mURLFakeWeb:I

    const/4 v0, 0x4

    iput v0, p0, LprotocolAnalysis/analysis/packetHandle;->mURLVideo:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    const-string v0, ""

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$0(LprotocolAnalysis/analysis/packetHandle;)Lcom/konka/kkmultiscreen/DataService$BroadMsg;
    .locals 1

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    return-object v0
.end method

.method static synthetic access$1(LprotocolAnalysis/analysis/packetHandle;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$2()LprotocolAnalysis/analysis/packetAnalytic;
    .locals 1

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    return-object v0
.end method

.method static synthetic access$3(LprotocolAnalysis/analysis/packetHandle;[B)V
    .locals 0

    invoke-direct {p0, p1}, LprotocolAnalysis/analysis/packetHandle;->getWeiKanInfo([B)V

    return-void
.end method

.method static synthetic access$4(LprotocolAnalysis/analysis/packetAnalytic;)V
    .locals 0

    sput-object p0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    return-void
.end method

.method static synthetic access$5()Ljava/lang/String;
    .locals 1

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$6()I
    .locals 1

    sget v0, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURLType:I

    return v0
.end method

.method static synthetic access$7(LprotocolAnalysis/analysis/packetHandle;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    invoke-direct {p0, p1}, LprotocolAnalysis/analysis/packetHandle;->analysis(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$8(Ljava/lang/String;)V
    .locals 0

    sput-object p0, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    return-void
.end method

.method static synthetic access$9(LprotocolAnalysis/analysis/packetHandle;Ljava/lang/String;)V
    .locals 0

    invoke-direct {p0, p1}, LprotocolAnalysis/analysis/packetHandle;->analysisURL(Ljava/lang/String;)V

    return-void
.end method

.method private analysis(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1    # Ljava/lang/String;

    const/4 v7, 0x1

    const-string v4, "<title>(.+?)</title>"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    :cond_0
    const-string v4, "weikan"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mVideoTitle = "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "<url>(.+?)</url>"

    invoke-static {v4}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v4

    if-eqz v4, :cond_1

    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    invoke-virtual {v1, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    invoke-virtual {v1, v7}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object p1

    :cond_1
    return-object p1
.end method

.method private analysisURL(Ljava/lang/String;)V
    .locals 10
    .param p1    # Ljava/lang/String;

    const/4 v9, 0x1

    const-string v6, "weikan"

    const-string v7, "analysisURL!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "<url>(.+?)</url>"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v5

    const-string v6, "<duration>(.+?)</duration>"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v3

    const-string v6, "<title>(.+?)</title>"

    invoke-static {v6}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v4

    invoke-virtual {v5, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v3, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v4, p1}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v6

    if-nez v6, :cond_0

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    :cond_0
    :goto_0
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_1

    :goto_1
    invoke-virtual {v2}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_2

    :goto_2
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->find()Z

    move-result v6

    if-nez v6, :cond_3

    return-void

    :cond_1
    const-string v6, "weikan"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "analysisURL!!!:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v1, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    goto :goto_0

    :cond_2
    const-string v6, "weikan"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "analysisURL!!!:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v2, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_3
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2
.end method

.method private getWeiKanInfo([B)V
    .locals 1
    .param p1    # [B

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketWeiKanType([B)I

    move-result v0

    sput v0, LprotocolAnalysis/analysis/packetHandle;->mWeiKanType:I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketmWeiKanURLType([B)I

    move-result v0

    sput v0, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURLType:I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getWeiKanPacketURL([B)Ljava/lang/String;

    move-result-object v0

    sput-object v0, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public closeInputMethod(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.multiScreen.InputMethod"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "stop"

    const/4 v3, 0x1

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v3, "closeInputMethod"

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public exitPlayer(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    sput-object v1, LprotocolAnalysis/analysis/packetHandle;->switchSuccess:Ljava/lang/Boolean;

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    const/4 v2, 0x0

    iput v2, v1, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "closePlayer"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public getMediaFileInfo([B)V
    .locals 4
    .param p1    # [B

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    sget-object v1, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v1, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileTypeString([B)I

    move-result v1

    iput v1, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileName([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileSize([B)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMediaFileInfo fileSize111111 ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketHaveNextFlag([B)I

    move-result v0

    iput v0, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    return-void
.end method

.method public getMediaFileInfoPos([B)V
    .locals 4
    .param p1    # [B

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    sget-object v1, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v1, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileTypeString([B)I

    move-result v1

    iput v1, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMediaFileInfo dataservice.mediaType ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v2, v2, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileName([B)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMediaFileInfo mediaPath ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileSize([B)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMediaFileInfo fileSize ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketHaveNextFlag([B)I

    move-result v0

    iput v0, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMediaFileInfo haveNextFlag ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v0, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v0, p1}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketResumePos([B)I

    move-result v0

    iput v0, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getMediaFileInfo resumePos ="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public getPacketDeviceIp([B)Ljava/lang/String;
    .locals 5
    .param p1    # [B

    const/16 v2, 0x20

    const/4 v1, 0x0

    :try_start_0
    new-instance v1, Ljava/lang/String;

    const/16 v3, 0xc

    const-string v4, "UTF-8"

    invoke-direct {v1, p1, v3, v2, v4}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v1

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    const-string v1, "0.0.0.0"

    goto :goto_0
.end method

.method public getPlatformInfo()Ljava/lang/String;
    .locals 2

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v1, v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mAppContext:Landroid/content/Context;

    invoke-static {v1}, Lcom/konka/kkimplements/tv/TvDeskProviderImpl;->getInstance(Landroid/content/Context;)Lcom/konka/kkinterface/tv/TvDeskProvider;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/TvDeskProvider;->getCommonManagerInstance()Lcom/konka/kkinterface/tv/CommonDesk;

    move-result-object v1

    invoke-interface {v1}, Lcom/konka/kkinterface/tv/CommonDesk;->getSystemBoardInfo()Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;

    move-result-object v0

    iget-object v1, v0, Lcom/konka/kkinterface/tv/CommonDesk$SystemBoardInfo;->strPlatform:Ljava/lang/String;

    return-object v1
.end method

.method public getSubSrcList()[B
    .locals 12

    const/16 v11, 0xd

    const/4 v6, 0x4

    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v10, 0x1

    const/4 v3, 0x0

    check-cast v3, [I

    new-array v4, v11, [I

    aput v10, v4, v10

    aput v8, v4, v8

    aput v9, v4, v9

    aput v6, v4, v6

    const/4 v6, 0x5

    const/16 v7, 0x10

    aput v7, v4, v6

    const/4 v6, 0x6

    const/16 v7, 0x11

    aput v7, v4, v6

    const/4 v6, 0x7

    const/16 v7, 0x12

    aput v7, v4, v6

    const/16 v6, 0x8

    const/16 v7, 0x17

    aput v7, v4, v6

    const/16 v6, 0x9

    const/16 v7, 0x18

    aput v7, v4, v6

    const/16 v6, 0xa

    const/16 v7, 0x19

    aput v7, v4, v6

    const/16 v6, 0xb

    const/16 v7, 0x1c

    aput v7, v4, v6

    const/16 v6, 0xc

    const/16 v7, 0x22

    aput v7, v4, v6

    const/16 v6, 0x11

    new-array v5, v6, [B

    aput-byte v11, v5, v10

    const/16 v6, 0x42

    aput-byte v6, v5, v8

    aput-byte v8, v5, v9

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/mstar/android/tv/TvPipPopManager;->isPipModeEnabled()Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x4

    :goto_0
    array-length v6, v5

    if-lt v0, v6, :cond_1

    :cond_0
    return-object v5

    :cond_1
    const/4 v6, 0x0

    aput-byte v6, v5, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v6

    invoke-virtual {v6, v10}, Lcom/mstar/android/tv/TvPipPopManager;->getSubWindowSourceList(Z)[I

    move-result-object v3

    const/4 v0, 0x0

    :goto_1
    array-length v6, v3

    if-lt v0, v6, :cond_3

    const/4 v2, 0x0

    :goto_2
    array-length v6, v5

    if-ge v2, v6, :cond_0

    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    :cond_3
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "getSubWindowSourceList=====:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget v8, v3, v0

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v1, 0x0

    :goto_3
    array-length v6, v4

    if-lt v1, v6, :cond_4

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_4
    aget v6, v4, v1

    aget v7, v3, v0

    if-ne v6, v7, :cond_5

    invoke-static {}, Lcom/mstar/android/tv/TvPipPopManager;->getInstance()Lcom/mstar/android/tv/TvPipPopManager;

    move-result-object v6

    invoke-static {}, Lcom/mstar/android/tv/TvCommonManager;->getInstance()Lcom/mstar/android/tv/TvCommonManager;

    move-result-object v7

    invoke-virtual {v7}, Lcom/mstar/android/tv/TvCommonManager;->getCurrentInputSource()Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v7

    invoke-static {}, Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;->values()[Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;

    move-result-object v8

    aget v9, v3, v0

    aget-object v8, v8, v9

    invoke-virtual {v6, v7, v8}, Lcom/mstar/android/tv/TvPipPopManager;->checkTravelingModeSupport(Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;Lcom/mstar/android/tvapi/common/vo/TvOsType$EnumInputSource;)Z

    move-result v6

    if-eqz v6, :cond_5

    add-int/lit8 v6, v1, 0x4

    aput-byte v10, v5, v6

    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_3
.end method

.method public getUnsignedFromByte(B)I
    .locals 4
    .param p1    # B

    const/4 v3, 0x1

    new-array v0, v3, [B

    const/4 v3, 0x0

    aput-byte p1, v0, v3

    new-instance v1, Ljava/io/ByteArrayInputStream;

    invoke-direct {v1, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    invoke-virtual {v1}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v2

    return v2
.end method

.method public mWeiKanStartPlay(Landroid/content/Context;[B)V
    .locals 0
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    return-void
.end method

.method public mediaShareStartPlay(Landroid/content/Context;[B)V
    .locals 10
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    const/4 v9, 0x0

    const/16 v8, 0x2005

    const/4 v7, 0x1

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getPacketCmd 0x2005 ===== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v4, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    if-ne v4, v8, :cond_2

    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getMediaFileInfoPos([B)V

    :goto_0
    new-instance v4, LprotocolAnalysis/analysis/packetAnalytic;

    invoke-direct {v4}, LprotocolAnalysis/analysis/packetAnalytic;-><init>()V

    sput-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mediaPath: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "filesize"

    iget-wide v5, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eqz v4, :cond_1

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_3

    const-string v4, "com.konka.picturePlayer.picturePlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "picPaths"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eq v4, v5, :cond_0

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mediaPathTmp==="

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "mediaPath:====="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iput-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    :cond_0
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v7, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->picPlayerCreat:I

    :cond_1
    :goto_1
    return-void

    :cond_2
    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getMediaFileInfo([B)V

    goto/16 :goto_0

    :cond_3
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_6

    const-string v4, "com.konka.musicPlayer.musicPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "musicPath"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "getPacketCmd 0x2005 ===== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v4, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    if-ne v4, v8, :cond_5

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "resumePos ===== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eq v4, v5, :cond_4

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iput-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    :cond_4
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v7, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->musicPlayerCreat:I

    goto :goto_1

    :cond_5
    const-string v4, "resumePos"

    invoke-virtual {v0, v4, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    :cond_6
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_9

    const-string v4, "com.konka.videoPlayer.videoPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "videofile"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    sget-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v4, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    if-ne v4, v8, :cond_8

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_3
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eq v4, v5, :cond_7

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iput-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    :cond_7
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v7, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    goto/16 :goto_1

    :cond_8
    const-string v4, "resumePos"

    invoke-virtual {v0, v4, v9}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_3

    :cond_9
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_a

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v6, "<posProfile>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v7, "</posProfile>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "com.bestv.ctv.main"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "------>goto bestv player1111---------"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "param"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eq v4, v5, :cond_1

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iput-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPathTmp:Ljava/lang/String;

    goto/16 :goto_1

    :cond_a
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "has no this function-------------"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public mediaShareStartPlayPos(Landroid/content/Context;[B)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    const/4 v7, 0x1

    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getMediaFileInfoPos([B)V

    new-instance v4, LprotocolAnalysis/analysis/packetAnalytic;

    invoke-direct {v4}, LprotocolAnalysis/analysis/packetAnalytic;-><init>()V

    sput-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "mediaPath: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "filesize"

    iget-wide v5, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_1

    const-string v4, "com.konka.picturePlayer.picturePlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "picPaths"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v7, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->picPlayerCreat:I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_2

    const-string v4, "com.konka.musicPlayer.musicPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "musicPath"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v7, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->musicPlayerCreat:I

    goto :goto_0

    :cond_2
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_3

    const-string v4, "com.konka.videoPlayer.videoPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "videofile"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v7, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    goto :goto_0

    :cond_3
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_4

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v6, "<posProfile>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v7, "</posProfile>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "android.intent.action.bestv.ott.login"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "------>goto bestv player1111---------"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "PosProfile"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v6, "has no this function-------------"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public multiScreenPacketHandle(Lnetwork/Interface/NIOSocket;[B)V
    .locals 13
    .param p1    # Lnetwork/Interface/NIOSocket;
    .param p2    # [B

    const/4 v12, 0x3

    const/4 v11, 0x2

    const/4 v10, 0x1

    const/4 v9, 0x0

    const/4 v5, 0x0

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v1

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "cmd = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sparse-switch v1, :sswitch_data_0

    :goto_0
    :sswitch_0
    return-void

    :sswitch_1
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a Alive packet: 0x1000"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v7

    sget-object v8, LprotocolAnalysis/analysis/packetHandle;->ack:LprotocolAnalysis/protocol/AlivePacketAck;

    iget-object v8, v8, LprotocolAnalysis/protocol/AlivePacketAck;->alivePacketBuff:[B

    invoke-virtual {v6, v7, v8}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToOuterMsgToClient(Ljava/lang/String;[B)V

    move-object p2, v5

    check-cast p2, [B

    goto :goto_0

    :sswitch_2
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a keybord packet: 0x1001"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, LinterfaceLayer/devI0/IDevDataOperate;->KEYBOARD:LinterfaceLayer/devI0/IDevDataOperate;

    new-array v7, v10, [I

    sget-object v8, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v8, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getBoardKeyCode([B)I

    move-result v8

    aput v8, v7, v9

    invoke-interface {v6, v7}, LinterfaceLayer/devI0/IDevDataOperate;->wirteKeyboarddata([I)V

    move-object p2, v5

    check-cast p2, [B

    goto :goto_0

    :sswitch_3
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a ir packet: 0x1003"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, LinterfaceLayer/devI0/IDevDataOperate;->IR:LinterfaceLayer/devI0/IDevDataOperate;

    new-array v7, v10, [I

    sget-object v8, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v8, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getIrKeyCode([B)I

    move-result v8

    aput v8, v7, v9

    invoke-interface {v6, v7}, LinterfaceLayer/devI0/IDevDataOperate;->wirteIRdata([I)V

    move-object p2, v5

    check-cast p2, [B

    goto :goto_0

    :sswitch_4
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a mouse packet: 0x1004"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getMouseEventValue([B)[S

    move-result-object v3

    sget-object v6, LinterfaceLayer/devI0/IDevDataOperate;->MOUSE:LinterfaceLayer/devI0/IDevDataOperate;

    const/4 v7, 0x4

    new-array v7, v7, [I

    aget-short v8, v3, v9

    aput v8, v7, v9

    aget-short v8, v3, v10

    aput v8, v7, v10

    aget-short v8, v3, v11

    aput v8, v7, v11

    aget-short v8, v3, v12

    aput v8, v7, v12

    invoke-interface {v6, v7}, LinterfaceLayer/devI0/IDevDataOperate;->wirteMousedata([I)V

    move-object p2, v5

    check-cast p2, [B

    goto :goto_0

    :sswitch_5
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a single touch packet: 0x1005"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getSingleTouchValue([B)[S

    move-result-object v4

    sget-object v6, LinterfaceLayer/devI0/IDevDataOperate;->TOUCH:LinterfaceLayer/devI0/IDevDataOperate;

    const/4 v7, 0x6

    new-array v7, v7, [I

    aget-short v8, v4, v9

    aput v8, v7, v9

    aget-short v8, v4, v10

    aput v8, v7, v10

    aget-short v8, v4, v11

    aput v8, v7, v11

    aget-short v8, v4, v12

    aput v8, v7, v12

    const/4 v8, 0x4

    const/4 v9, 0x4

    aget-short v9, v4, v9

    aput v9, v7, v8

    const/4 v8, 0x5

    const/4 v9, 0x5

    aget-short v9, v4, v9

    aput v9, v7, v8

    invoke-interface {v6, v7}, LinterfaceLayer/devI0/IDevDataOperate;->wirteTouchdata([I)V

    move-object p2, v5

    check-cast p2, [B

    goto/16 :goto_0

    :sswitch_6
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a multi touch packet: 0x1006"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getMultiTouchValue([B)[S

    move-result-object v2

    if-eqz v2, :cond_0

    sget-object v6, LinterfaceLayer/devI0/IDevDataOperate;->TOUCH:LinterfaceLayer/devI0/IDevDataOperate;

    const/4 v7, 0x6

    new-array v7, v7, [I

    aget-short v8, v2, v9

    aput v8, v7, v9

    aget-short v8, v2, v10

    aput v8, v7, v10

    aget-short v8, v2, v11

    aput v8, v7, v11

    aget-short v8, v2, v12

    aput v8, v7, v12

    const/4 v8, 0x4

    const/4 v9, 0x4

    aget-short v9, v2, v9

    aput v9, v7, v8

    const/4 v8, 0x5

    const/4 v9, 0x5

    aget-short v9, v2, v9

    aput v9, v7, v8

    invoke-interface {v6, v7}, LinterfaceLayer/devI0/IDevDataOperate;->wirteTouchdata([I)V

    :cond_0
    move-object p2, v5

    check-cast p2, [B

    goto/16 :goto_0

    :sswitch_7
    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v6, "recv a openTouchEffect packet: 0x1007"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v6}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->openTouchEffect(Landroid/content/ContentResolver;)V

    goto/16 :goto_0

    :sswitch_8
    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v6, "recv a closeTouchEffect packet: 0x1008"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v6}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->closeTouchEffect(Landroid/content/ContentResolver;)V

    goto/16 :goto_0

    :sswitch_9
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a sensor packet: 0x100B"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getSensorValue([B)[S

    move-result-object v0

    sget-object v6, LinterfaceLayer/devI0/IDevDataOperate;->SENSOR:LinterfaceLayer/devI0/IDevDataOperate;

    const/4 v7, 0x4

    new-array v7, v7, [I

    aget-short v8, v0, v9

    aput v8, v7, v9

    aget-short v8, v0, v10

    aput v8, v7, v10

    aget-short v8, v0, v11

    aput v8, v7, v11

    aget-short v8, v0, v12

    aput v8, v7, v12

    invoke-interface {v6, v7}, LinterfaceLayer/devI0/IDevDataOperate;->wirteSensordata([I)V

    move-object p2, v5

    check-cast p2, [B

    goto/16 :goto_0

    :sswitch_a
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v7, "recv a openInputMethod packet: 0x1009"

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v7, "openInputMethod"

    invoke-virtual {v6, v7, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_b
    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "sendInputMethodData@@"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v8, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketInputStringValue([B)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_c
    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v7, "closeInputMethod"

    invoke-virtual {v6, v7, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getPacketDeviceIp([B)Ljava/lang/String;

    move-result-object v5

    sput-object v5, LprotocolAnalysis/analysis/packetHandle;->mobileShareIP:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    invoke-static {v7}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v7, " localIp------"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    sget-object v7, LprotocolAnalysis/analysis/packetHandle;->mobileShareIP:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v6, "startPlayer"

    invoke-virtual {v5, v6, p2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_e
    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v6, "stopPlay"

    invoke-virtual {v5, v6, p2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_f
    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "seekTo@@"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v8, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v8, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketSeekBarPos([B)I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_10
    const-string v6, "weikan"

    const-string v7, "ok exit!!!"

    invoke-static {v6, v7}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v6, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v7, "exitPlayer"

    invoke-virtual {v6, v7, v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_11
    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v6, "recv a login packet: 0x2005"

    invoke-static {v5, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getPacketDeviceIp([B)Ljava/lang/String;

    move-result-object v5

    sput-object v5, LprotocolAnalysis/analysis/packetHandle;->mobileShareIP:Ljava/lang/String;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v6, "startPlayer"

    invoke-virtual {v5, v6, p2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_12
    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v6, "sendCurrPlayStateToMobile"

    invoke-virtual {v5, v6, p2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_13
    const-string v5, "weikan"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "have been 2008!!!"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v5, "weikan"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "URL = "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Lcom/konka/IntelligentControl/ioop/comClass/CTools;->ByteToString([B)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_14
    const-string v5, "yidian"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "have been 2009!!!"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_15
    const-string v5, "weikan"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "have been 200B!!!"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v7, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketWard([B)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_16
    const-string v5, "weikan"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "have been 200C!!!"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v7, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketWard([B)I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :sswitch_17
    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    const-string v6, "Statistical"

    invoke-virtual {v5, v6, p2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->SendDataToService(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_18
    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-interface {p1}, Lnetwork/Interface/NIOSocket;->getIp()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {p0}, LprotocolAnalysis/analysis/packetHandle;->sendBusinessFlagToMobile()[B

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToOuterMsgToClient(Ljava/lang/String;[B)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x70 -> :sswitch_0
        0x1000 -> :sswitch_1
        0x1001 -> :sswitch_2
        0x1002 -> :sswitch_0
        0x1003 -> :sswitch_3
        0x1004 -> :sswitch_4
        0x1005 -> :sswitch_5
        0x1006 -> :sswitch_6
        0x1007 -> :sswitch_7
        0x1008 -> :sswitch_8
        0x1009 -> :sswitch_a
        0x100a -> :sswitch_b
        0x100b -> :sswitch_9
        0x100c -> :sswitch_c
        0x2001 -> :sswitch_d
        0x2002 -> :sswitch_e
        0x2003 -> :sswitch_f
        0x2004 -> :sswitch_10
        0x2005 -> :sswitch_11
        0x2006 -> :sswitch_12
        0x2008 -> :sswitch_13
        0x2009 -> :sswitch_14
        0x200a -> :sswitch_0
        0x200b -> :sswitch_15
        0x200c -> :sswitch_16
        0x3000 -> :sswitch_0
        0x3001 -> :sswitch_17
        0x3002 -> :sswitch_18
        0x3003 -> :sswitch_0
        0x4001 -> :sswitch_0
    .end sparse-switch
.end method

.method public multiScreenStatisticalInterface(Landroid/content/Context;[B)V
    .locals 6
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    sget-object v3, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v3, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFunType([B)I

    move-result v0

    sget-object v3, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v3, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketOptionSystemType([B)I

    move-result v2

    const-string v3, "sendBroadcast"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sendBroadcast aidl-----ostype:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "---------"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, "funtype:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    const-string v3, "com.konka.passport.intent.action.KKSTATISTICTVLOG"

    invoke-direct {v1, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v3, "BusinessId"

    const/16 v4, 0xa28

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v3, "action"

    const-string v4, "login"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v3, 0x1

    if-ne v2, v3, :cond_1

    const-string v3, "arg1"

    const-string v4, "android"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    :goto_1
    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void

    :cond_1
    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const-string v3, "arg1"

    const-string v4, "apple"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    :pswitch_0
    const-string v3, "arg2"

    const-string v4, "\u5eb7\u4f73\u667a\u63a7"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_1
    const-string v3, "arg2"

    const-string v4, "\u5a92\u4f53\u5206\u4eab"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_2
    const-string v3, "arg2"

    const-string v4, "\u767e\u89c6\u901a"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_3
    const-string v3, "arg2"

    const-string v4, "\u4f20\u5c4f"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_4
    const-string v3, "arg2"

    const-string v4, "\u591a\u5c4f\u4e92\u52a8\u8f93\u5165\u6cd5"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    :pswitch_5
    const-string v3, "arg2"

    const-string v4, "\u91cd\u529b\u611f\u5e94"

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method

.method public openInputMethod(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v2, "openInputMethod1"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.multiScreen.InputMethod"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v2, "openInputMethod2"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public registerPlayerStateBroadcastReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    const-string v2, "registerPlayerStateBroadcastReceiver()----------------"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;-><init>(LprotocolAnalysis/analysis/packetHandle;LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;)V

    iput-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->playerStateReceiver:LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.MultiScreeenPlayer.PlayerState"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->playerStateReceiver:LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public registerShareStateBroadcastReceiver(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    const-string v2, "registerBroadcastReceiver()----------------"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;-><init>(LprotocolAnalysis/analysis/packetHandle;LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;)V

    iput-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->dataReceiver:LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;

    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "com.konka.media_player_status"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->dataReceiver:LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;

    invoke-virtual {p1, v1, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    return-void
.end method

.method public seekTo(Landroid/content/Context;I)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.MultiScreeenPlayer.playerseek"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "seekto"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public sendBestvAuth(Ljava/lang/String;[B)V
    .locals 8
    .param p1    # Ljava/lang/String;
    .param p2    # [B

    const/4 v4, 0x0

    move-object v2, v4

    check-cast v2, [B

    new-instance v0, LprotocolAnalysis/protocol/LoginAck;

    invoke-direct {v0}, LprotocolAnalysis/protocol/LoginAck;-><init>()V

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "userid:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v7, Lcom/konka/InitApp/CInitAppSys;->best:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v7, v7, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->best_useid:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v5, Lcom/konka/InitApp/CInitAppSys;->best:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    iget-object v3, v5, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;->best_useid:Ljava/lang/String;

    if-nez v3, :cond_0

    :goto_0
    return-void

    :cond_0
    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v5

    const/16 v6, 0xc

    if-ge v5, v6, :cond_1

    sput-object v4, Lcom/konka/InitApp/CInitAppSys;->best:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    new-instance v4, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    sget-object v5, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v5}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getBaseContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;-><init>(Landroid/content/Context;)V

    sput-object v4, Lcom/konka/InitApp/CInitAppSys;->best:Lcom/konka/IntelligentControl/ioop/specificOp/BestvOp;

    goto :goto_0

    :cond_1
    invoke-virtual {v0, v3}, LprotocolAnalysis/protocol/LoginAck;->SetData(Ljava/lang/String;)V

    invoke-virtual {v0}, LprotocolAnalysis/protocol/LoginAck;->sizeOf()I

    move-result v4

    new-array v2, v4, [B

    invoke-virtual {v0, v2}, LprotocolAnalysis/protocol/LoginAck;->Format([B)V

    const/4 v1, 0x1

    :goto_1
    array-length v4, v2

    if-le v1, v4, :cond_2

    sget-object v4, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v4, p1, v2}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToOuterMsgToClient(Ljava/lang/String;[B)V

    goto :goto_0

    :cond_2
    sget-object v4, Ljava/lang/System;->out:Ljava/io/PrintStream;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "userid:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    add-int/lit8 v6, v1, -0x1

    aget-byte v6, v2, v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public sendBusinessFlagToMobile()[B
    .locals 3

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->getBussinessPlatformInfo()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->mBFlag:Ljava/lang/String;

    new-instance v1, LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->mBFlag:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    invoke-direct {v1, v2}, LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;-><init>(I)V

    iput-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->sendFlag:LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->sendFlag:LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->mBFlag:Ljava/lang/String;

    invoke-virtual {v1, v2}, LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;->setBusinessInfo(Ljava/lang/String;)V

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->sendFlag:LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

    invoke-virtual {v1}, LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;->sizeOf()I

    move-result v1

    new-array v0, v1, [B

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->sendFlag:LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;

    invoke-virtual {v1, v0}, LprotocolAnalysis/protocol/SendBusinessFlagInfoPacket;->format([B)V

    return-object v0
.end method

.method public sendCurrPlayStateToMobile(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "resumePos"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public sendDeviceNameToMobile()V
    .locals 3

    invoke-virtual {p0}, LprotocolAnalysis/analysis/packetHandle;->setDeviceNamePacket()[B

    move-result-object v0

    sget-object v1, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v2, v2, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToOuterMsgToClient(Ljava/lang/String;[B)V

    return-void
.end method

.method public sendInputMethodResult(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/String;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendInputMethodResult"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.multiScreen.InputMethod"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v2, "result"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sendInputMethodResult"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public sendMediaDataInfoToPlayer(Landroid/content/Context;[B)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    const/16 v8, 0x2005

    const/4 v7, 0x0

    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getMediaFileInfo([B)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_1

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v5, "------>goto Photo player2222--------------555"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-string v4, "com.konka.MultiScreenPlayer.picturePlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "picPaths"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "flag"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "filesize"

    iget-wide v5, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_3

    const-string v4, "com.konka.MultiScreenPlayer.musicPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "musicPath"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "send getPacketCmd 0x2005 =1111==== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v4, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    if-ne v4, v8, :cond_2

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "sendMediaDataInfoToPlayer resumePos ===== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    :goto_1
    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    const-string v4, "resumePos"

    invoke-virtual {v1, v4, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto :goto_1

    :cond_3
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_5

    const-string v4, "com.konka.MultiScreenPlayer.videoPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "videofile"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "send getPacketCmd 0x2005 =22222==== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v6, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    sget-object v4, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v4, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketCmd([B)I

    move-result v4

    if-ne v4, v8, :cond_4

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "resumePos ===== "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_4
    const-string v4, "resumePos"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    goto :goto_2

    :cond_5
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_0

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v6, "<posProfile>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v7, "</posProfile>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "com.bestv.ctv.main"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "------>goto bestv player2222---------"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "param"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public sendMediaDataInfoToPlayerPos(Landroid/content/Context;[B)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    invoke-virtual {p0, p2}, LprotocolAnalysis/analysis/packetHandle;->getMediaFileInfoPos([B)V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    if-eqz v4, :cond_0

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_photo:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_1

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    const-string v5, "------>goto Photo player2222--------------555"

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    const-string v4, "com.konka.MultiScreenPlayer.picturePlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    const-string v4, "picPaths"

    invoke-virtual {v0, v4, v3}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "flag"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "filesize"

    iget-wide v5, p0, LprotocolAnalysis/analysis/packetHandle;->fileSize:J

    invoke-virtual {v0, v4, v5, v6}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_music:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_2

    const-string v4, "com.konka.MultiScreenPlayer.musicPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "musicPath"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_2
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_3

    const-string v4, "com.konka.MultiScreenPlayer.videoPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "videofile"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "haveNextFlag"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->haveNextFlag:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "resumePos"

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->resumePos:I

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "mobileIp"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget-object v5, v5, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mobileIp:Ljava/lang/String;

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_3
    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v4, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    if-ne v4, v5, :cond_0

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v6, "<posProfile>"

    invoke-virtual {v5, v6}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v5

    add-int/lit8 v5, v5, 0xc

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mediaPath:Ljava/lang/String;

    const-string v7, "</posProfile>"

    invoke-virtual {v6, v7}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v6

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v4, "android.intent.action.bestv.ott.login"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->SHARELOG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "------>goto bestv player2222---------"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/high16 v4, 0x10000000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "PosProfile"

    invoke-virtual {v0, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public sendPlayerStateToMobile(Ljava/lang/String;)V
    .locals 7
    .param p1    # Ljava/lang/String;

    const/4 v2, 0x0

    iput-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->packetPro:LprotocolAnalysis/protocol/packetProtocol;

    new-instance v2, LprotocolAnalysis/protocol/packetProtocol;

    invoke-direct {v2}, LprotocolAnalysis/protocol/packetProtocol;-><init>()V

    iput-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->packetPro:LprotocolAnalysis/protocol/packetProtocol;

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->packetPro:LprotocolAnalysis/protocol/packetProtocol;

    iget v3, p0, LprotocolAnalysis/analysis/packetHandle;->playerState:I

    iget v4, p0, LprotocolAnalysis/analysis/packetHandle;->currentTime:I

    iget v5, p0, LprotocolAnalysis/analysis/packetHandle;->DurationTime:I

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->fileUrl:Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v5, v6}, LprotocolAnalysis/protocol/packetProtocol;->SetPlayStatePacket(IIILjava/lang/String;)[B

    move-result-object v1

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " mobileIP-----------------1: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " playStatePacket length------------: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    array-length v4, v1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    :goto_0
    array-length v2, v1

    if-lt v0, v2, :cond_0

    sget-object v2, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->mApp:Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;

    invoke-virtual {v2, p1, v1}, Lcom/konka/kkmultiscreen/KKMutiScreenTvApp;->ToOuterMsgToClient(Ljava/lang/String;[B)V

    return-void

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public sendWeikanMediaInfo(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 8
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Boolean;

    const/4 v7, 0x2

    const/4 v6, 0x1

    sget v4, LprotocolAnalysis/analysis/packetHandle;->mWeiKanType:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v4, "com.konka.MultiScreenPlayer.videoPlayer"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "hideController"

    invoke-virtual {v2, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "isStringArrayList"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "videotitle"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "urllist"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "durationtime"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "haveNextFlag"

    invoke-virtual {v2, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_1
    const-string v4, "comfrom"

    invoke-virtual {v2, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v3, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    invoke-virtual {p1, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const-string v4, "videofile"

    sget-object v5, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    const-string v4, "com.konka.MultiScreenPlayer.videoPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "hideController"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "isStringArrayList"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "videotitle"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "urllist"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "durationtime"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "haveNextFlag"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    const-string v4, "comfrom"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v4, "weikan"

    const-string v5, "vod play have been to start activity!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1
    const-string v4, "videofile"

    sget-object v5, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public setDeviceNamePacket()[B
    .locals 13

    const/4 v12, 0x4

    const/4 v11, 0x0

    invoke-static {}, LprotocolAnalysis/analysis/GetDeviceMacAddressToCodeString;->getMacAddress_eth0()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, LprotocolAnalysis/analysis/GetDeviceMacAddressToCodeString;->encodeMacAddress(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-instance v6, LprotocolAnalysis/protocol/packetProtocol;

    invoke-direct {v6}, LprotocolAnalysis/protocol/packetProtocol;-><init>()V

    iput-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->packetPro:LprotocolAnalysis/protocol/packetProtocol;

    new-array v0, v12, [B

    const/4 v6, 0x2

    const/16 v7, 0x40

    aput-byte v7, v0, v6

    const/4 v6, 0x3

    aput-byte v12, v0, v6

    :try_start_0
    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v6

    iput-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "platforminfo:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/lang/String;

    iget-object v9, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    const-string v10, "UTF-8"

    invoke-direct {v8, v9, v10}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "deviceName:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    array-length v6, v6

    int-to-short v5, v6

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->packetPro:LprotocolAnalysis/protocol/packetProtocol;

    invoke-virtual {v6, v5, v0, v11}, LprotocolAnalysis/protocol/packetProtocol;->shortToByte(S[BI)V

    const/4 v4, 0x0

    :goto_1
    array-length v6, v0

    if-lt v4, v6, :cond_0

    array-length v6, v0

    add-int/2addr v6, v5

    new-array v2, v6, [B

    array-length v6, v0

    invoke-static {v0, v11, v2, v11, v6}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    iget-object v7, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    array-length v7, v7

    invoke-static {v6, v11, v2, v12, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    const-string v6, "length"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "deviceNameInfo.length : "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v8, v2

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :try_start_1
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "deviceNameInfoPac:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    new-instance v8, Ljava/lang/String;

    const-string v9, "UTF-8"

    invoke-direct {v8, v2, v9}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->deviceNameInfo:[B

    return-object v6

    :catch_0
    move-exception v3

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_0

    :cond_0
    sget-object v6, Ljava/lang/System;->out:Ljava/io/PrintStream;

    aget-byte v7, v0, v4

    invoke-virtual {v6, v7}, Ljava/io/PrintStream;->println(I)V

    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    :catch_1
    move-exception v3

    invoke-virtual {v3}, Ljava/io/UnsupportedEncodingException;->printStackTrace()V

    goto :goto_2
.end method

.method public startPlayWeiKan(Landroid/content/Context;[B)V
    .locals 2
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    new-instance v0, Ljava/lang/Thread;

    new-instance v1, LprotocolAnalysis/analysis/packetHandle$3;

    invoke-direct {v1, p0, p2, p1}, LprotocolAnalysis/analysis/packetHandle$3;-><init>(LprotocolAnalysis/analysis/packetHandle;[BLandroid/content/Context;)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void
.end method

.method public startPlayWeikan(Landroid/content/Context;Ljava/lang/Boolean;)V
    .locals 9
    .param p1    # Landroid/content/Context;
    .param p2    # Ljava/lang/Boolean;

    const/high16 v8, 0x10000000

    const/4 v7, 0x2

    const/4 v6, 0x1

    iget-object v4, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    sget-object v5, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_video:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v5}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v5

    iput v5, v4, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget v4, LprotocolAnalysis/analysis/packetHandle;->mWeiKanType:I

    packed-switch v4, :pswitch_data_0

    :goto_0
    return-void

    :pswitch_0
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3}, Landroid/content/Intent;-><init>()V

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v3, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "com.konka.videoPlayer.videoPlayer"

    invoke-virtual {v3, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "hideController"

    invoke-virtual {v2, v4, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "comfrom"

    invoke-virtual {v2, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "isStringArrayList"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "videotitle"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "urllist"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "durationtime"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "haveNextFlag"

    invoke-virtual {v2, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_1
    invoke-virtual {v3, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v4, "weikan"

    const-string v5, "live play have been to start activity!!!"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v3}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    :cond_0
    const-string v4, "videofile"

    sget-object v5, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    invoke-virtual {v2, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :pswitch_1
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v8}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    const-string v4, "com.konka.videoPlayer.videoPlayer"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v4, "hideController"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "comfrom"

    invoke-virtual {v0, v4, v6}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    const-string v4, "isStringArrayList"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    const-string v4, "videotitle"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "urllist"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->urlList:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "durationtime"

    iget-object v5, p0, LprotocolAnalysis/analysis/packetHandle;->durationList:Ljava/util/ArrayList;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    const-string v4, "haveNextFlag"

    invoke-virtual {v0, v4, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    :goto_2
    invoke-virtual {v1, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    const-string v4, "weikan"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "vod play have been to start activity!!!"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, LprotocolAnalysis/analysis/packetHandle;->mVideoTitle:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    :cond_1
    const-string v4, "videofile"

    sget-object v5, LprotocolAnalysis/analysis/packetHandle;->mWeiKanURL:Ljava/lang/String;

    invoke-virtual {v0, v4, v5}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public startPlayer(Landroid/content/Context;[B)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    const-string v0, "player service "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediaType-------:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v2, v2, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "player service "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediaType111-------:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v2, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileTypeString([B)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v0, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v1}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    const/4 v1, 0x0

    iput v1, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->bestvPlayerCreat:I

    :goto_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, LprotocolAnalysis/analysis/packetHandle$2;

    invoke-direct {v1, p0, p1, p2}, LprotocolAnalysis/analysis/packetHandle$2;-><init>(LprotocolAnalysis/analysis/packetHandle;Landroid/content/Context;[B)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :cond_0
    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    const/4 v1, 0x1

    iput v1, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->bestvPlayerCreat:I

    goto :goto_0
.end method

.method public startPlayerResumePosFromMobile(Landroid/content/Context;[B)V
    .locals 3
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    const-string v0, "player service "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediaType-------:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v2, v2, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "player service "

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mediaType111-------:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v2, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketFileTypeString([B)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iget v0, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->mediaType:I

    sget-object v1, LprotocolAnalysis/analysis/MediaTypeEnum;->multiscreen_bestv:LprotocolAnalysis/analysis/MediaTypeEnum;

    invoke-virtual {v1}, LprotocolAnalysis/analysis/MediaTypeEnum;->ordinal()I

    move-result v1

    if-eq v0, v1, :cond_0

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    const/4 v1, 0x0

    iput v1, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->bestvPlayerCreat:I

    :goto_0
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, LprotocolAnalysis/analysis/packetHandle$1;

    invoke-direct {v1, p0, p1, p2}, LprotocolAnalysis/analysis/packetHandle$1;-><init>(LprotocolAnalysis/analysis/packetHandle;Landroid/content/Context;[B)V

    invoke-direct {v0, v1}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    iput-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playThread:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    return-void

    :cond_0
    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    const/4 v1, 0x1

    iput v1, v0, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->bestvPlayerCreat:I

    goto :goto_0
.end method

.method public startWeikanBackward(Landroid/content/Context;[B)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    sget-object v2, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v2, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketWard([B)I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.MultiScreeenPlayer.playerseek"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "seekto"

    const/4 v3, -0x2

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "pressStatus"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public startWeikanForward(Landroid/content/Context;[B)V
    .locals 4
    .param p1    # Landroid/content/Context;
    .param p2    # [B

    sget-object v2, LprotocolAnalysis/analysis/packetHandle;->packetAny:LprotocolAnalysis/analysis/packetAnalytic;

    invoke-virtual {v2, p2}, LprotocolAnalysis/analysis/packetAnalytic;->getPacketWard([B)I

    move-result v2

    add-int/lit8 v1, v2, 0x1

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.konka.MultiScreeenPlayer.playerseek"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v2, "seekto"

    const/4 v3, -0x1

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    const-string v2, "pressStatus"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public startYidianService(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.rockitv.android"

    const-string v2, "com.rockitv.android.MyService"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.rockitv.android.START_SERVER"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public stopPlay(Landroid/content/Context;)V
    .locals 3
    .param p1    # Landroid/content/Context;

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerService"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mediastop"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public switchMediaPlayer(Landroid/content/Context;)V
    .locals 4
    .param p1    # Landroid/content/Context;

    const/4 v3, 0x0

    const-string v1, "media stop"

    const-string v2, "stop ----------switchMediaPlayer------------------------3"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v3, v1, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->picPlayerCreat:I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v3, v1, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->musicPlayerCreat:I

    iget-object v1, p0, LprotocolAnalysis/analysis/packetHandle;->dataservice:Lcom/konka/kkmultiscreen/DataService$BroadMsg;

    iput v3, v1, Lcom/konka/kkmultiscreen/DataService$BroadMsg;->videoPlayerCreat:I

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.konka.playerService.MultiScreenPlayerSwtich"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "mediaSwitch"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    return-void
.end method

.method public unregisterPlayerStateBroadcastReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->playerStateReceiver:LprotocolAnalysis/analysis/packetHandle$DataPlayerStateReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method

.method public unregisterShareStateBroadcastReceiver(Landroid/content/Context;)V
    .locals 1
    .param p1    # Landroid/content/Context;

    iget-object v0, p0, LprotocolAnalysis/analysis/packetHandle;->dataReceiver:LprotocolAnalysis/analysis/packetHandle$DataShareStateReceiver;

    invoke-virtual {p1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    return-void
.end method
