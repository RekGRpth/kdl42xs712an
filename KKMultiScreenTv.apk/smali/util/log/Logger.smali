.class public Lutil/log/Logger;
.super Ljava/lang/Object;
.source "Logger.java"


# static fields
.field private static logger:Lutil/log/Logger;

.field private static final logobj:Lutil/log/CLog;

.field private static tag:Z


# instance fields
.field private level:Lutil/log/Level;

.field private name:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lutil/log/CLog;

    invoke-direct {v0}, Lutil/log/CLog;-><init>()V

    sput-object v0, Lutil/log/Logger;->logobj:Lutil/log/CLog;

    const/4 v0, 0x0

    sput-object v0, Lutil/log/Logger;->logger:Lutil/log/Logger;

    const/4 v0, 0x1

    sput-boolean v0, Lutil/log/Logger;->tag:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lutil/log/Logger;->level:Lutil/log/Level;

    iput-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    sput-object p0, Lutil/log/Logger;->logger:Lutil/log/Logger;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    const/4 v0, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lutil/log/Logger;->level:Lutil/log/Level;

    iput-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    iput-object p1, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    return-void
.end method

.method public static getLogger()Lutil/log/Logger;
    .locals 1

    sget-boolean v0, Lutil/log/Logger;->tag:Z

    if-eqz v0, :cond_0

    new-instance v0, Lutil/log/Logger;

    invoke-direct {v0}, Lutil/log/Logger;-><init>()V

    sput-object v0, Lutil/log/Logger;->logger:Lutil/log/Logger;

    const/4 v0, 0x0

    sput-boolean v0, Lutil/log/Logger;->tag:Z

    :cond_0
    sget-object v0, Lutil/log/Logger;->logger:Lutil/log/Logger;

    return-object v0
.end method

.method public static getLogger(Ljava/lang/String;)Lutil/log/Logger;
    .locals 1
    .param p0    # Ljava/lang/String;

    if-eqz p0, :cond_0

    new-instance v0, Lutil/log/Logger;

    invoke-direct {v0, p0}, Lutil/log/Logger;-><init>(Ljava/lang/String;)V

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lutil/log/Logger;

    invoke-direct {v0}, Lutil/log/Logger;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public debug(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "info"

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public debug(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public error(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lutil/log/CLog;->eLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public fine(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "fine"

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public finer(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "finer"

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public finest(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "finest"

    invoke-static {v0, p1}, Lutil/log/CLog;->dLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public info(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "info"

    invoke-static {v0, p1}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public info(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public isLoggable(Lutil/log/Level;)Z
    .locals 1
    .param p1    # Lutil/log/Level;

    iget-object v0, p0, Lutil/log/Logger;->level:Lutil/log/Level;

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public log(Lutil/log/Level;Ljava/lang/String;)V
    .locals 1
    .param p1    # Lutil/log/Level;
    .param p2    # Ljava/lang/String;

    const-string v0, "info"

    invoke-static {v0, p2}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public setLevel(Lutil/log/Level;)V
    .locals 0
    .param p1    # Lutil/log/Level;

    iput-object p1, p0, Lutil/log/Logger;->level:Lutil/log/Level;

    return-void
.end method

.method public severe(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "severe"

    invoke-static {v0, p1}, Lutil/log/CLog;->iLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public warning(Ljava/lang/String;)V
    .locals 1
    .param p1    # Ljava/lang/String;

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lutil/log/Logger;->name:Ljava/lang/String;

    invoke-static {v0, p1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const-string v0, "warn"

    invoke-static {v0, p1}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public warning(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1    # Ljava/lang/String;
    .param p2    # Ljava/lang/String;

    invoke-static {p1, p2}, Lutil/log/CLog;->wLog(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method
