.class public final enum Lutil/log/Level;
.super Ljava/lang/Enum;
.source "Level.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lutil/log/Level;",
        ">;"
    }
.end annotation


# static fields
.field public static final enum ALL:Lutil/log/Level;

.field public static final enum CONFIG:Lutil/log/Level;

.field private static final synthetic ENUM$VALUES:[Lutil/log/Level;

.field public static final enum FINE:Lutil/log/Level;

.field public static final enum FINER:Lutil/log/Level;

.field public static final enum FINEST:Lutil/log/Level;

.field public static final enum INFO:Lutil/log/Level;

.field public static final enum OFF:Lutil/log/Level;

.field public static final enum SEVERE:Lutil/log/Level;

.field public static final enum WARNING:Lutil/log/Level;


# instance fields
.field public value:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    new-instance v0, Lutil/log/Level;

    const-string v1, "ALL"

    invoke-direct {v0, v1, v3}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->ALL:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "CONFIG"

    invoke-direct {v0, v1, v4}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->CONFIG:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "FINE"

    invoke-direct {v0, v1, v5}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->FINE:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "FINER"

    invoke-direct {v0, v1, v6}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->FINER:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "FINEST"

    invoke-direct {v0, v1, v7}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->FINEST:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "INFO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->INFO:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "OFF"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->OFF:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "SEVERE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->SEVERE:Lutil/log/Level;

    new-instance v0, Lutil/log/Level;

    const-string v1, "WARNING"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lutil/log/Level;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lutil/log/Level;->WARNING:Lutil/log/Level;

    const/16 v0, 0x9

    new-array v0, v0, [Lutil/log/Level;

    sget-object v1, Lutil/log/Level;->ALL:Lutil/log/Level;

    aput-object v1, v0, v3

    sget-object v1, Lutil/log/Level;->CONFIG:Lutil/log/Level;

    aput-object v1, v0, v4

    sget-object v1, Lutil/log/Level;->FINE:Lutil/log/Level;

    aput-object v1, v0, v5

    sget-object v1, Lutil/log/Level;->FINER:Lutil/log/Level;

    aput-object v1, v0, v6

    sget-object v1, Lutil/log/Level;->FINEST:Lutil/log/Level;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lutil/log/Level;->INFO:Lutil/log/Level;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lutil/log/Level;->OFF:Lutil/log/Level;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lutil/log/Level;->SEVERE:Lutil/log/Level;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lutil/log/Level;->WARNING:Lutil/log/Level;

    aput-object v2, v0, v1

    sput-object v0, Lutil/log/Level;->ENUM$VALUES:[Lutil/log/Level;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 1

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 v0, 0x0

    iput v0, p0, Lutil/log/Level;->value:I

    iget v0, p0, Lutil/log/Level;->value:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lutil/log/Level;->value:I

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 1
    .param p3    # I

    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    const/4 v0, 0x0

    iput v0, p0, Lutil/log/Level;->value:I

    iput p3, p0, Lutil/log/Level;->value:I

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lutil/log/Level;
    .locals 1

    const-class v0, Lutil/log/Level;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lutil/log/Level;

    return-object v0
.end method

.method public static values()[Lutil/log/Level;
    .locals 4

    const/4 v3, 0x0

    sget-object v0, Lutil/log/Level;->ENUM$VALUES:[Lutil/log/Level;

    array-length v1, v0

    new-array v2, v1, [Lutil/log/Level;

    invoke-static {v0, v3, v2, v3, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    return-object v2
.end method
