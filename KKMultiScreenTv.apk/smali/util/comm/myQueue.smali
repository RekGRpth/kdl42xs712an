.class public Lutil/comm/myQueue;
.super Ljava/lang/Object;
.source "myQueue.java"

# interfaces
.implements Ljava/lang/Iterable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lutil/comm/myQueue$ListIterator;,
        Lutil/comm/myQueue$Node;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Item:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Ljava/lang/Iterable",
        "<TItem;>;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field private N:I

.field private first:Lutil/comm/myQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lutil/comm/myQueue",
            "<TItem;>.Node;"
        }
    .end annotation
.end field

.field private last:Lutil/comm/myQueue$Node;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lutil/comm/myQueue",
            "<TItem;>.Node;"
        }
    .end annotation
.end field

.field private maxSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-class v0, Lutil/comm/myQueue;

    invoke-virtual {v0}, Ljava/lang/Class;->desiredAssertionStatus()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    sput-boolean v0, Lutil/comm/myQueue;->$assertionsDisabled:Z

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>()V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/16 v0, 0x7d0

    iput v0, p0, Lutil/comm/myQueue;->maxSize:I

    iput-object v1, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    iput-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    const/4 v0, 0x0

    iput v0, p0, Lutil/comm/myQueue;->N:I

    sget-boolean v0, Lutil/comm/myQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lutil/comm/myQueue;->check()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic access$0(Lutil/comm/myQueue;)Lutil/comm/myQueue$Node;
    .locals 1

    iget-object v0, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    return-object v0
.end method

.method private check()Z
    .locals 7

    const/4 v4, 0x1

    const/4 v3, 0x0

    iget v5, p0, Lutil/comm/myQueue;->N:I

    if-nez v5, :cond_3

    iget-object v5, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    if-eqz v5, :cond_1

    :cond_0
    :goto_0
    return v3

    :cond_1
    iget-object v5, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    if-nez v5, :cond_0

    :cond_2
    move v3, v4

    goto :goto_0

    :cond_3
    iget v5, p0, Lutil/comm/myQueue;->N:I

    if-ne v5, v4, :cond_4

    iget-object v5, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    iget-object v6, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    if-ne v5, v6, :cond_0

    iget-object v5, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v5}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v5

    if-eqz v5, :cond_2

    goto :goto_0

    :cond_4
    iget-object v5, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    iget-object v6, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    if-eq v5, v6, :cond_0

    iget-object v5, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v5}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v5}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v5

    if-nez v5, :cond_0

    const/4 v1, 0x0

    iget-object v2, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    :goto_1
    if-nez v2, :cond_5

    iget v5, p0, Lutil/comm/myQueue;->N:I

    if-ne v1, v5, :cond_0

    iget-object v0, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    :goto_2
    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v0}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v5

    if-nez v5, :cond_6

    iget-object v5, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    if-eq v5, v0, :cond_2

    goto :goto_0

    :cond_5
    add-int/lit8 v1, v1, 0x1

    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v2}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v2

    goto :goto_1

    :cond_6
    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v0}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v0

    goto :goto_2
.end method


# virtual methods
.method public clear()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    iput-object v0, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    const/4 v0, 0x0

    iput v0, p0, Lutil/comm/myQueue;->N:I

    sget-boolean v0, Lutil/comm/myQueue;->$assertionsDisabled:Z

    if-nez v0, :cond_0

    invoke-direct {p0}, Lutil/comm/myQueue;->check()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :cond_0
    return-void
.end method

.method public dequeue()Ljava/lang/Object;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TItem;"
        }
    .end annotation

    invoke-virtual {p0}, Lutil/comm/myQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Queue underflow"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    :cond_0
    iget-object v1, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->item:Ljava/lang/Object;
    invoke-static {v1}, Lutil/comm/myQueue$Node;->access$0(Lutil/comm/myQueue$Node;)Ljava/lang/Object;

    move-result-object v0

    iget-object v1, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->next:Lutil/comm/myQueue$Node;
    invoke-static {v1}, Lutil/comm/myQueue$Node;->access$1(Lutil/comm/myQueue$Node;)Lutil/comm/myQueue$Node;

    move-result-object v1

    iput-object v1, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    iget v1, p0, Lutil/comm/myQueue;->N:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lutil/comm/myQueue;->N:I

    invoke-virtual {p0}, Lutil/comm/myQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    iput-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    :cond_1
    sget-boolean v1, Lutil/comm/myQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lutil/comm/myQueue;->check()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_2
    return-object v0
.end method

.method public enqueue(Ljava/lang/Object;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TItem;)V"
        }
    .end annotation

    const/4 v3, 0x0

    iget v1, p0, Lutil/comm/myQueue;->N:I

    iget v2, p0, Lutil/comm/myQueue;->maxSize:I

    if-le v1, v2, :cond_0

    iput-object v3, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    iput-object v3, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    const/4 v1, 0x0

    iput v1, p0, Lutil/comm/myQueue;->N:I

    sget-boolean v1, Lutil/comm/myQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_0

    invoke-direct {p0}, Lutil/comm/myQueue;->check()Z

    move-result v1

    if-nez v1, :cond_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_0
    iget-object v0, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    new-instance v1, Lutil/comm/myQueue$Node;

    invoke-direct {v1, p0, v3}, Lutil/comm/myQueue$Node;-><init>(Lutil/comm/myQueue;Lutil/comm/myQueue$Node;)V

    iput-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    iget-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    invoke-static {v1, p1}, Lutil/comm/myQueue$Node;->access$3(Lutil/comm/myQueue$Node;Ljava/lang/Object;)V

    iget-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    invoke-static {v1, v3}, Lutil/comm/myQueue$Node;->access$4(Lutil/comm/myQueue$Node;Lutil/comm/myQueue$Node;)V

    invoke-virtual {p0}, Lutil/comm/myQueue;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    iput-object v1, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    :goto_0
    iget v1, p0, Lutil/comm/myQueue;->N:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lutil/comm/myQueue;->N:I

    sget-boolean v1, Lutil/comm/myQueue;->$assertionsDisabled:Z

    if-nez v1, :cond_2

    invoke-direct {p0}, Lutil/comm/myQueue;->check()Z

    move-result v1

    if-nez v1, :cond_2

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :cond_1
    iget-object v1, p0, Lutil/comm/myQueue;->last:Lutil/comm/myQueue$Node;

    invoke-static {v0, v1}, Lutil/comm/myQueue$Node;->access$4(Lutil/comm/myQueue$Node;Lutil/comm/myQueue$Node;)V

    goto :goto_0

    :cond_2
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    iget-object v0, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public iterator()Ljava/util/Iterator;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Iterator",
            "<TItem;>;"
        }
    .end annotation

    new-instance v0, Lutil/comm/myQueue$ListIterator;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lutil/comm/myQueue$ListIterator;-><init>(Lutil/comm/myQueue;Lutil/comm/myQueue$ListIterator;)V

    return-object v0
.end method

.method public peek()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TItem;"
        }
    .end annotation

    invoke-virtual {p0}, Lutil/comm/myQueue;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Queue underflow"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lutil/comm/myQueue;->first:Lutil/comm/myQueue$Node;

    # getter for: Lutil/comm/myQueue$Node;->item:Ljava/lang/Object;
    invoke-static {v0}, Lutil/comm/myQueue$Node;->access$0(Lutil/comm/myQueue$Node;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public size()I
    .locals 1

    iget v0, p0, Lutil/comm/myQueue;->N:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lutil/comm/myQueue;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    return-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Object;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0
.end method
