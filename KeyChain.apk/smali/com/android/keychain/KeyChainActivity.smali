.class public Lcom/android/keychain/KeyChainActivity;
.super Landroid/app/Activity;
.source "KeyChainActivity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/keychain/KeyChainActivity$6;,
        Lcom/android/keychain/KeyChainActivity$ResponseSender;,
        Lcom/android/keychain/KeyChainActivity$ViewHolder;,
        Lcom/android/keychain/KeyChainActivity$CertificateAdapter;,
        Lcom/android/keychain/KeyChainActivity$AliasLoader;,
        Lcom/android/keychain/KeyChainActivity$State;
    }
.end annotation


# static fields
.field private static KEY_STATE:Ljava/lang/String;


# instance fields
.field private mKeyStore:Landroid/security/KeyStore;

.field private mSender:Landroid/app/PendingIntent;

.field private mSenderUid:I

.field private mState:Lcom/android/keychain/KeyChainActivity$State;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const-string v0, "state"

    sput-object v0, Lcom/android/keychain/KeyChainActivity;->KEY_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    invoke-static {}, Landroid/security/KeyStore;->getInstance()Landroid/security/KeyStore;

    move-result-object v0

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    return-void
.end method

.method static synthetic access$100(Lcom/android/keychain/KeyChainActivity;)Landroid/security/KeyStore;
    .locals 1
    .param p0    # Lcom/android/keychain/KeyChainActivity;

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/android/keychain/KeyChainActivity;)I
    .locals 1
    .param p0    # Lcom/android/keychain/KeyChainActivity;

    iget v0, p0, Lcom/android/keychain/KeyChainActivity;->mSenderUid:I

    return v0
.end method

.method static synthetic access$300(Lcom/android/keychain/KeyChainActivity;Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    .locals 0
    .param p0    # Lcom/android/keychain/KeyChainActivity;
    .param p1    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainActivity;->displayCertChooserDialog(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V

    return-void
.end method

.method static synthetic access$500(Lcom/android/keychain/KeyChainActivity;Ljava/lang/String;)V
    .locals 0
    .param p0    # Lcom/android/keychain/KeyChainActivity;
    .param p1    # Ljava/lang/String;

    invoke-direct {p0, p1}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    return-void
.end method

.method private displayCertChooserDialog(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V
    .locals 33
    .param p1    # Lcom/android/keychain/KeyChainActivity$CertificateAdapter;

    new-instance v8, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v8, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v29, 0x7f020002    # com.android.keychain.R.layout.cert_chooser_header

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/TextView;

    const v29, 0x7f020001    # com.android.keychain.R.layout.cert_chooser_footer

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    const/high16 v29, 0x7f020000    # com.android.keychain.R.layout.cert_chooser

    const/16 v30, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v29

    move-object/from16 v2, v30

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v22

    check-cast v22, Landroid/widget/ListView;

    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v10, v1, v2}, Landroid/widget/ListView;->addHeaderView(Landroid/view/View;Ljava/lang/Object;Z)V

    const/16 v29, 0x0

    const/16 v30, 0x0

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-virtual {v0, v14, v1, v2}, Landroid/widget/ListView;->addFooterView(Landroid/view/View;Ljava/lang/Object;Z)V

    move-object/from16 v0, v22

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    move-object/from16 v0, v22

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    new-instance v29, Lcom/android/keychain/KeyChainActivity$1;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    invoke-direct {v0, v1, v2}, Lcom/android/keychain/KeyChainActivity$1;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/widget/ListView;)V

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    # getter for: Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->access$400(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->isEmpty()Z

    move-result v13

    if-eqz v13, :cond_2

    const/high16 v23, 0x1040000    # android.R.string.cancel

    :goto_0
    new-instance v29, Lcom/android/keychain/KeyChainActivity$2;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/keychain/KeyChainActivity$2;-><init>(Lcom/android/keychain/KeyChainActivity;)V

    move/from16 v0, v23

    move-object/from16 v1, v29

    invoke-virtual {v8, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    if-eqz v13, :cond_3

    const v29, 0x7f030001    # com.android.keychain.R.string.title_no_certs

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    :goto_1
    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v8}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v11

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    move-object/from16 v29, v0

    invoke-virtual/range {v29 .. v29}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Landroid/content/IntentSender;->getTargetPackage()Ljava/lang/String;

    move-result-object v24

    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v25

    const/16 v29, 0x0

    :try_start_0
    move-object/from16 v0, v25

    move-object/from16 v1, v24

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v29

    move-object/from16 v0, v25

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/Object;->toString()Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v7

    :goto_2
    const v29, 0x7f030003    # com.android.keychain.R.string.requesting_application

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    aput-object v7, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object v9, v6

    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v30, "host"

    invoke-virtual/range {v29 .. v30}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_1

    move-object/from16 v17, v15

    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v30, "port"

    const/16 v31, -0x1

    invoke-virtual/range {v29 .. v31}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v26

    const/16 v29, -0x1

    move/from16 v0, v26

    move/from16 v1, v29

    if-eq v0, v1, :cond_0

    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, ":"

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v17

    :cond_0
    const v29, 0x7f030004    # com.android.keychain.R.string.requesting_server

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x1

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    aput-object v17, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    if-nez v9, :cond_6

    move-object/from16 v9, v16

    :cond_1
    :goto_3
    invoke-virtual {v10, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v29, 0x7f030005    # com.android.keychain.R.string.install_new_cert_message

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    const/16 v30, 0x2

    move/from16 v0, v30

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v30, v0

    const/16 v31, 0x0

    const-string v32, ".pfx"

    aput-object v32, v30, v31

    const/16 v31, 0x1

    const-string v32, ".p12"

    aput-object v32, v30, v31

    invoke-static/range {v29 .. v30}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    const/high16 v29, 0x7f050000    # com.android.keychain.R.id.cert_chooser_install_message

    move/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    move-object/from16 v0, v20

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const v29, 0x7f050001    # com.android.keychain.R.id.cert_chooser_install_button

    move/from16 v0, v29

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v18

    check-cast v18, Landroid/widget/Button;

    new-instance v29, Lcom/android/keychain/KeyChainActivity$4;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v11}, Lcom/android/keychain/KeyChainActivity$4;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/app/Dialog;)V

    move-object/from16 v0, v18

    move-object/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    new-instance v29, Lcom/android/keychain/KeyChainActivity$5;

    move-object/from16 v0, v29

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/android/keychain/KeyChainActivity$5;-><init>(Lcom/android/keychain/KeyChainActivity;)V

    move-object/from16 v0, v29

    invoke-virtual {v11, v0}, Landroid/app/Dialog;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)V

    invoke-virtual {v11}, Landroid/app/Dialog;->show()V

    return-void

    :cond_2
    const v23, 0x7f030008    # com.android.keychain.R.string.deny_button

    goto/16 :goto_0

    :cond_3
    const v29, 0x7f030002    # com.android.keychain.R.string.title_select_cert

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    invoke-virtual/range {p0 .. p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v29

    const-string v30, "alias"

    invoke-virtual/range {v29 .. v30}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_5

    # getter for: Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->access$400(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;

    move-result-object v29

    move-object/from16 v0, v29

    invoke-interface {v0, v5}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v4

    const/16 v29, -0x1

    move/from16 v0, v29

    if-eq v4, v0, :cond_4

    add-int/lit8 v21, v4, 0x1

    const/16 v29, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v21

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    :cond_4
    :goto_4
    const v29, 0x7f030007    # com.android.keychain.R.string.allow_button

    new-instance v30, Lcom/android/keychain/KeyChainActivity$3;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    move-object/from16 v2, v22

    move-object/from16 v3, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/android/keychain/KeyChainActivity$3;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/widget/ListView;Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)V

    move/from16 v0, v29

    move-object/from16 v1, v30

    invoke-virtual {v8, v0, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    goto/16 :goto_1

    :cond_5
    # getter for: Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->mAliases:Ljava/util/List;
    invoke-static/range {p1 .. p1}, Lcom/android/keychain/KeyChainActivity$CertificateAdapter;->access$400(Lcom/android/keychain/KeyChainActivity$CertificateAdapter;)Ljava/util/List;

    move-result-object v29

    invoke-interface/range {v29 .. v29}, Ljava/util/List;->size()I

    move-result v29

    const/16 v30, 0x1

    move/from16 v0, v29

    move/from16 v1, v30

    if-ne v0, v1, :cond_4

    const/4 v4, 0x0

    add-int/lit8 v21, v4, 0x1

    const/16 v29, 0x1

    move-object/from16 v0, v22

    move/from16 v1, v21

    move/from16 v2, v29

    invoke-virtual {v0, v1, v2}, Landroid/widget/ListView;->setItemChecked(IZ)V

    goto :goto_4

    :catch_0
    move-exception v12

    move-object/from16 v7, v24

    goto/16 :goto_2

    :cond_6
    new-instance v29, Ljava/lang/StringBuilder;

    invoke-direct/range {v29 .. v29}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v29

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    const-string v30, " "

    invoke-virtual/range {v29 .. v30}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    move-object/from16 v0, v29

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v29

    invoke-virtual/range {v29 .. v29}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    goto/16 :goto_3
.end method

.method private finish(Ljava/lang/String;)V
    .locals 5
    .param p1    # Ljava/lang/String;

    const/4 v4, 0x0

    if-nez p1, :cond_0

    invoke-virtual {p0, v4}, Lcom/android/keychain/KeyChainActivity;->setResult(I)V

    :goto_0
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "response"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getIBinderExtra(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v2

    invoke-static {v2}, Landroid/security/IKeyChainAliasCallback$Stub;->asInterface(Landroid/os/IBinder;)Landroid/security/IKeyChainAliasCallback;

    move-result-object v0

    if-eqz v0, :cond_1

    new-instance v2, Lcom/android/keychain/KeyChainActivity$ResponseSender;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v0, p1, v3}, Lcom/android/keychain/KeyChainActivity$ResponseSender;-><init>(Lcom/android/keychain/KeyChainActivity;Landroid/security/IKeyChainAliasCallback;Ljava/lang/String;Lcom/android/keychain/KeyChainActivity$1;)V

    new-array v3, v4, [Ljava/lang/Void;

    invoke-virtual {v2, v3}, Lcom/android/keychain/KeyChainActivity$ResponseSender;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :goto_1
    return-void

    :cond_0
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "android.intent.extra.TEXT"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const/4 v2, -0x1

    invoke-virtual {p0, v2, v1}, Lcom/android/keychain/KeyChainActivity;->setResult(ILandroid/content/Intent;)V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->finish()V

    goto :goto_1
.end method

.method private isKeyStoreUnlocked()Z
    .locals 2

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mKeyStore:Landroid/security/KeyStore;

    invoke-virtual {v0}, Landroid/security/KeyStore;->state()Landroid/security/KeyStore$State;

    move-result-object v0

    sget-object v1, Landroid/security/KeyStore$State;->UNLOCKED:Landroid/security/KeyStore$State;

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private showCertChooserDialog()V
    .locals 2

    new-instance v0, Lcom/android/keychain/KeyChainActivity$AliasLoader;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;-><init>(Lcom/android/keychain/KeyChainActivity;Lcom/android/keychain/KeyChainActivity$1;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/android/keychain/KeyChainActivity$AliasLoader;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1    # I
    .param p2    # I
    .param p3    # Landroid/content/Intent;

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/AssertionError;

    invoke-direct {v0}, Ljava/lang/AssertionError;-><init>()V

    throw v0

    :pswitch_0
    invoke-direct {p0}, Lcom/android/keychain/KeyChainActivity;->isKeyStoreUnlocked()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/android/keychain/KeyChainActivity;->showCertChooserDialog()V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    if-nez p1, :cond_1

    sget-object v0, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    :cond_0
    :goto_0
    return-void

    :cond_1
    sget-object v0, Lcom/android/keychain/KeyChainActivity;->KEY_STATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    if-nez v0, :cond_0

    sget-object v0, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    goto :goto_0
.end method

.method public onResume()V
    .locals 5

    const/4 v4, 0x0

    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v2, "sender"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Landroid/app/PendingIntent;

    iput-object v1, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    iget-object v1, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    if-nez v1, :cond_0

    invoke-direct {p0, v4}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    :goto_0
    :pswitch_0
    return-void

    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/android/keychain/KeyChainActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iget-object v2, p0, Lcom/android/keychain/KeyChainActivity;->mSender:Landroid/app/PendingIntent;

    invoke-virtual {v2}, Landroid/app/PendingIntent;->getIntentSender()Landroid/content/IntentSender;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/IntentSender;->getTargetPackage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v1, Landroid/content/pm/ApplicationInfo;->uid:I

    iput v1, p0, Lcom/android/keychain/KeyChainActivity;->mSenderUid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    sget-object v1, Lcom/android/keychain/KeyChainActivity$6;->$SwitchMap$com$android$keychain$KeyChainActivity$State:[I

    iget-object v2, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    invoke-virtual {v2}, Lcom/android/keychain/KeyChainActivity$State;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1}, Ljava/lang/AssertionError;-><init>()V

    throw v1

    :catch_0
    move-exception v0

    invoke-direct {p0, v4}, Lcom/android/keychain/KeyChainActivity;->finish(Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    invoke-direct {p0}, Lcom/android/keychain/KeyChainActivity;->isKeyStoreUnlocked()Z

    move-result v1

    if-nez v1, :cond_1

    sget-object v1, Lcom/android/keychain/KeyChainActivity$State;->UNLOCK_REQUESTED:Lcom/android/keychain/KeyChainActivity$State;

    iput-object v1, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.android.credentials.UNLOCK"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2}, Lcom/android/keychain/KeyChainActivity;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/android/keychain/KeyChainActivity;->showCertChooserDialog()V

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1    # Landroid/os/Bundle;

    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    sget-object v1, Lcom/android/keychain/KeyChainActivity$State;->INITIAL:Lcom/android/keychain/KeyChainActivity$State;

    if-eq v0, v1, :cond_0

    sget-object v0, Lcom/android/keychain/KeyChainActivity;->KEY_STATE:Ljava/lang/String;

    iget-object v1, p0, Lcom/android/keychain/KeyChainActivity;->mState:Lcom/android/keychain/KeyChainActivity$State;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    :cond_0
    return-void
.end method
