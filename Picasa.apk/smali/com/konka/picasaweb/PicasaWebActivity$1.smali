.class Lcom/konka/picasaweb/PicasaWebActivity$1;
.super Landroid/webkit/WebChromeClient;
.source "PicasaWebActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/konka/picasaweb/PicasaWebActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/konka/picasaweb/PicasaWebActivity;


# direct methods
.method constructor <init>(Lcom/konka/picasaweb/PicasaWebActivity;)V
    .locals 0

    iput-object p1, p0, Lcom/konka/picasaweb/PicasaWebActivity$1;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    invoke-direct {p0}, Landroid/webkit/WebChromeClient;-><init>()V

    return-void
.end method

.method static synthetic access$0(Lcom/konka/picasaweb/PicasaWebActivity$1;)Lcom/konka/picasaweb/PicasaWebActivity;
    .locals 1

    iget-object v0, p0, Lcom/konka/picasaweb/PicasaWebActivity$1;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    return-object v0
.end method


# virtual methods
.method public onProgressChanged(Landroid/webkit/WebView;I)V
    .locals 4
    .param p1    # Landroid/webkit/WebView;
    .param p2    # I

    invoke-super {p0, p1, p2}, Landroid/webkit/WebChromeClient;->onProgressChanged(Landroid/webkit/WebView;I)V

    move v0, p2

    iget-object v1, p0, Lcom/konka/picasaweb/PicasaWebActivity$1;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    if-eqz v1, :cond_0

    const/16 v1, 0x64

    if-ne p2, v1, :cond_0

    iget-object v1, p0, Lcom/konka/picasaweb/PicasaWebActivity$1;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->isShowing()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/konka/picasaweb/PicasaWebActivity$1;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->mProgressDialog:Landroid/app/ProgressDialog;
    invoke-static {v1}, Lcom/konka/picasaweb/PicasaWebActivity;->access$0(Lcom/konka/picasaweb/PicasaWebActivity;)Landroid/app/ProgressDialog;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/ProgressDialog;->dismiss()V

    :cond_0
    iget-object v1, p0, Lcom/konka/picasaweb/PicasaWebActivity$1;->this$0:Lcom/konka/picasaweb/PicasaWebActivity;

    new-instance v2, Lcom/konka/picasaweb/PicasaWebActivity$1$1;

    invoke-direct {v2, p0, v0}, Lcom/konka/picasaweb/PicasaWebActivity$1$1;-><init>(Lcom/konka/picasaweb/PicasaWebActivity$1;I)V

    invoke-virtual {v1, v2}, Lcom/konka/picasaweb/PicasaWebActivity;->runOnUiThread(Ljava/lang/Runnable;)V

    # getter for: Lcom/konka/picasaweb/PicasaWebActivity;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/konka/picasaweb/PicasaWebActivity;->access$1()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onProgressChanged "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "%"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method
