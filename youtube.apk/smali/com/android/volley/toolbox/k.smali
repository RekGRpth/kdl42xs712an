.class final Lcom/android/volley/toolbox/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/android/volley/toolbox/g;

.field private final b:Lcom/android/volley/Request;

.field private c:Landroid/graphics/Bitmap;

.field private d:Lcom/android/volley/VolleyError;

.field private final e:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>(Lcom/android/volley/toolbox/g;Lcom/android/volley/Request;Lcom/android/volley/toolbox/m;)V
    .locals 1

    iput-object p1, p0, Lcom/android/volley/toolbox/k;->a:Lcom/android/volley/toolbox/g;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/android/volley/toolbox/k;->e:Ljava/util/LinkedList;

    iput-object p2, p0, Lcom/android/volley/toolbox/k;->b:Lcom/android/volley/Request;

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, p3}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method static synthetic a(Lcom/android/volley/toolbox/k;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 0

    iput-object p1, p0, Lcom/android/volley/toolbox/k;->c:Landroid/graphics/Bitmap;

    return-object p1
.end method

.method static synthetic a(Lcom/android/volley/toolbox/k;)Ljava/util/LinkedList;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->e:Ljava/util/LinkedList;

    return-object v0
.end method

.method static synthetic b(Lcom/android/volley/toolbox/k;)Landroid/graphics/Bitmap;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->c:Landroid/graphics/Bitmap;

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/android/volley/VolleyError;
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->d:Lcom/android/volley/VolleyError;

    return-object v0
.end method

.method public final a(Lcom/android/volley/VolleyError;)V
    .locals 0

    iput-object p1, p0, Lcom/android/volley/toolbox/k;->d:Lcom/android/volley/VolleyError;

    return-void
.end method

.method public final a(Lcom/android/volley/toolbox/m;)V
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    return-void
.end method

.method public final b(Lcom/android/volley/toolbox/m;)Z
    .locals 1

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->e:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->e:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/toolbox/k;->b:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->f()V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
