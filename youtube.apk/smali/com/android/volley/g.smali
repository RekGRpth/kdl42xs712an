.class final Lcom/android/volley/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/android/volley/e;

.field private final b:Lcom/android/volley/Request;

.field private final c:Lcom/android/volley/m;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/android/volley/e;Lcom/android/volley/Request;Lcom/android/volley/m;Ljava/lang/Runnable;)V
    .locals 0

    iput-object p1, p0, Lcom/android/volley/g;->a:Lcom/android/volley/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p2, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    iput-object p3, p0, Lcom/android/volley/g;->c:Lcom/android/volley/m;

    iput-object p4, p0, Lcom/android/volley/g;->d:Ljava/lang/Runnable;

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    iget-object v0, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    invoke-virtual {v0}, Lcom/android/volley/Request;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    const-string v1, "canceled-at-delivery"

    invoke-virtual {v0, v1}, Lcom/android/volley/Request;->b(Ljava/lang/String;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/android/volley/g;->c:Lcom/android/volley/m;

    iget-object v0, v0, Lcom/android/volley/m;->c:Lcom/android/volley/VolleyError;

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    iget-object v1, p0, Lcom/android/volley/g;->c:Lcom/android/volley/m;

    iget-object v1, v1, Lcom/android/volley/m;->a:Ljava/lang/Object;

    invoke-virtual {v0, v1}, Lcom/android/volley/Request;->a(Ljava/lang/Object;)V

    :goto_2
    iget-object v0, p0, Lcom/android/volley/g;->c:Lcom/android/volley/m;

    iget-boolean v0, v0, Lcom/android/volley/m;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    const-string v1, "intermediate-response"

    invoke-virtual {v0, v1}, Lcom/android/volley/Request;->a(Ljava/lang/String;)V

    :goto_3
    iget-object v0, p0, Lcom/android/volley/g;->d:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/android/volley/g;->d:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    :cond_3
    iget-object v0, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    iget-object v1, p0, Lcom/android/volley/g;->c:Lcom/android/volley/m;

    iget-object v1, v1, Lcom/android/volley/m;->c:Lcom/android/volley/VolleyError;

    invoke-virtual {v0, v1}, Lcom/android/volley/Request;->b(Lcom/android/volley/VolleyError;)V

    goto :goto_2

    :cond_4
    iget-object v0, p0, Lcom/android/volley/g;->b:Lcom/android/volley/Request;

    const-string v1, "done"

    invoke-virtual {v0, v1}, Lcom/android/volley/Request;->b(Ljava/lang/String;)V

    goto :goto_3
.end method
