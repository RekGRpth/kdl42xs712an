.class final Lcom/google/android/gms/b/d;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/c;


# instance fields
.field final synthetic a:Lcom/google/android/gms/b/e;

.field final synthetic b:Ljava/lang/Integer;

.field final synthetic c:Ljava/lang/Long;

.field final synthetic d:Ljava/lang/Integer;

.field final synthetic e:Ljava/lang/Integer;

.field final synthetic f:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/b/e;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/b/d;->a:Lcom/google/android/gms/b/e;

    iput-object p2, p0, Lcom/google/android/gms/b/d;->b:Ljava/lang/Integer;

    iput-object p3, p0, Lcom/google/android/gms/b/d;->c:Ljava/lang/Long;

    iput-object p4, p0, Lcom/google/android/gms/b/d;->d:Ljava/lang/Integer;

    iput-object p5, p0, Lcom/google/android/gms/b/d;->e:Ljava/lang/Integer;

    iput-object p6, p0, Lcom/google/android/gms/b/d;->f:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b()V
    .locals 2

    sget-boolean v0, Lcom/google/android/gms/internal/ij;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "disconnected from network quality service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method

.method public final u_()V
    .locals 6

    sget-boolean v0, Lcom/google/android/gms/internal/ij;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "MDM"

    const-string v1, "connected to network quality service"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/b/d;->a:Lcom/google/android/gms/b/e;

    iget-object v1, p0, Lcom/google/android/gms/b/d;->b:Ljava/lang/Integer;

    iget-object v2, p0, Lcom/google/android/gms/b/d;->c:Ljava/lang/Long;

    iget-object v3, p0, Lcom/google/android/gms/b/d;->d:Ljava/lang/Integer;

    iget-object v4, p0, Lcom/google/android/gms/b/d;->e:Ljava/lang/Integer;

    iget-object v5, p0, Lcom/google/android/gms/b/d;->f:Landroid/os/Bundle;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/b/e;->a(Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V

    iget-object v0, p0, Lcom/google/android/gms/b/d;->a:Lcom/google/android/gms/b/e;

    invoke-virtual {v0}, Lcom/google/android/gms/b/e;->b()V

    return-void
.end method
