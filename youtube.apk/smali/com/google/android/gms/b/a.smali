.class public final Lcom/google/android/gms/b/a;
.super Ljava/lang/Object;


# static fields
.field private static a:Z

.field private static final b:Lcom/google/android/gms/common/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput-boolean v0, Lcom/google/android/gms/b/a;->a:Z

    new-instance v0, Lcom/google/android/gms/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/b/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/b/a;->b:Lcom/google/android/gms/common/c;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V
    .locals 7

    sget-boolean v0, Lcom/google/android/gms/b/a;->a:Z

    if-nez v0, :cond_0

    :goto_0
    return-void

    :cond_0
    new-instance v1, Lcom/google/android/gms/b/e;

    sget-object v0, Lcom/google/android/gms/b/a;->b:Lcom/google/android/gms/common/c;

    new-instance v2, Lcom/google/android/gms/b/c;

    invoke-direct {v2}, Lcom/google/android/gms/b/c;-><init>()V

    invoke-direct {v1, p0, v0, v2}, Lcom/google/android/gms/b/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;)V

    sget-object v0, Lcom/google/android/gms/b/a;->b:Lcom/google/android/gms/common/c;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/b/e;->b(Lcom/google/android/gms/common/c;)V

    new-instance v0, Lcom/google/android/gms/b/d;

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/b/d;-><init>(Lcom/google/android/gms/b/e;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Integer;Ljava/lang/Integer;Landroid/os/Bundle;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/b/e;->a(Lcom/google/android/gms/common/c;)V

    invoke-virtual {v1}, Lcom/google/android/gms/b/e;->a()V

    goto :goto_0
.end method

.method static synthetic a(Z)Z
    .locals 1

    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/b/a;->a:Z

    return v0
.end method
