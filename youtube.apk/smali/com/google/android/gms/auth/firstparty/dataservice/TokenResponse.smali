.class public Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/firstparty/dataservice/t;


# instance fields
.field accountName:Ljava/lang/String;

.field lV:Ljava/lang/String;

.field lX:Ljava/lang/String;

.field lY:Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

.field mO:Ljava/lang/String;

.field mT:Ljava/lang/String;

.field mp:Ljava/lang/String;

.field mq:Ljava/lang/String;

.field ne:Ljava/lang/String;

.field nf:Ljava/lang/String;

.field ng:Ljava/lang/String;

.field nh:Z

.field ni:Z

.field nj:Z

.field nk:Z

.field nl:Ljava/util/List;

.field nm:Z

.field final version:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/t;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->CREATOR:Lcom/google/android/gms/auth/firstparty/dataservice/t;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->version:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nl:Ljava/util/List;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZZLcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->version:I

    iput-object p2, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->accountName:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lV:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ne:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nf:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lX:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ng:Ljava/lang/String;

    iput-object p8, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mp:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mq:Ljava/lang/String;

    iput-boolean p10, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nh:Z

    iput-boolean p11, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ni:Z

    iput-boolean p12, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nj:Z

    iput-boolean p13, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nk:Z

    iput-object p14, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lY:Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nl:Ljava/util/List;

    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mT:Ljava/lang/String;

    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mO:Ljava/lang/String;

    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nm:Z

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public getAccountName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->accountName:Ljava/lang/String;

    return-object v0
.end method

.method public getCaptchaChallenge()Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lY:Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    return-object v0
.end method

.method public getDetail()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lX:Ljava/lang/String;

    return-object v0
.end method

.method public getFirstName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mp:Ljava/lang/String;

    return-object v0
.end method

.method public getLastName()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mq:Ljava/lang/String;

    return-object v0
.end method

.method public getPicasaUser()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ng:Ljava/lang/String;

    return-object v0
.end method

.method public getRopRevision()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mO:Ljava/lang/String;

    return-object v0
.end method

.method public getRopText()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mT:Ljava/lang/String;

    return-object v0
.end method

.method public getScopeData()Ljava/util/List;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nl:Ljava/util/List;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getSignInUrl()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nf:Ljava/lang/String;

    return-object v0
.end method

.method public getStatus()Lcom/google/android/gms/auth/firstparty/shared/Status;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lV:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/shared/Status;->fromWireCode(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/shared/Status;

    move-result-object v0

    return-object v0
.end method

.method public getToken()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ne:Ljava/lang/String;

    return-object v0
.end method

.method public isBrowserSignInSuggested()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nk:Z

    return v0
.end method

.method public isEsMobileServiceEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nj:Z

    return v0
.end method

.method public isGPlusServiceAllowed()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nh:Z

    return v0
.end method

.method public isGPlusServiceEnabled()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ni:Z

    return v0
.end method

.method public isTokenCached()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nm:Z

    return v0
.end method

.method public setAccountName(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->accountName:Ljava/lang/String;

    return-object p0
.end method

.method public setBrowserSignInSuggested(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nk:Z

    return-object p0
.end method

.method public setCaptchaChallenge(Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lY:Lcom/google/android/gms/auth/firstparty/shared/CaptchaChallenge;

    return-object p0
.end method

.method public setDetail(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lX:Ljava/lang/String;

    return-object p0
.end method

.method public setEsMobileServiceEnabled(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nj:Z

    return-object p0
.end method

.method public setFirstName(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mp:Ljava/lang/String;

    return-object p0
.end method

.method public setGPlusServiceAllowed(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nh:Z

    return-object p0
.end method

.method public setGPlusServiceEnabled(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ni:Z

    return-object p0
.end method

.method public setIsTokenCached(Z)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-boolean p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nm:Z

    return-object p0
.end method

.method public setLastName(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mq:Ljava/lang/String;

    return-object p0
.end method

.method public setPicasaUser(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ng:Ljava/lang/String;

    return-object p0
.end method

.method public setRopRevision(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mO:Ljava/lang/String;

    return-object p0
.end method

.method public setRopText(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->mT:Ljava/lang/String;

    return-object p0
.end method

.method public setScopeData(Ljava/util/List;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nl:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nl:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    return-object p0
.end method

.method public setSignInUrl(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->nf:Ljava/lang/String;

    return-object p0
.end method

.method public setStatus(Lcom/google/android/gms/auth/firstparty/shared/Status;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1

    invoke-static {p1}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/shared/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/firstparty/shared/Status;->getWire()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->lV:Ljava/lang/String;

    return-object p0
.end method

.method public setToken(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;->ne:Ljava/lang/String;

    return-object p0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/auth/firstparty/dataservice/t;->a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;Landroid/os/Parcel;I)V

    return-void
.end method
