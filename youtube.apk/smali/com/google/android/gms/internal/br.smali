.class public final Lcom/google/android/gms/internal/br;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/bh;


# instance fields
.field public final en:Lcom/google/android/gms/internal/cz;

.field public final gJ:Lcom/google/android/gms/internal/bo;

.field public final gK:Lcom/google/android/gms/internal/ka;

.field public final gL:Lcom/google/android/gms/internal/bi;

.field public final gM:Lcom/google/android/gms/internal/db;

.field public final gN:Lcom/google/android/gms/internal/p;

.field public final gO:Ljava/lang/String;

.field public final gP:Z

.field public final gQ:Ljava/lang/String;

.field public final gR:Lcom/google/android/gms/internal/bl;

.field public final gS:I

.field public final orientation:I

.field public final url:Ljava/lang/String;

.field public final versionCode:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/bh;

    invoke-direct {v0}, Lcom/google/android/gms/internal/bh;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/br;->CREATOR:Lcom/google/android/gms/internal/bh;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/internal/bo;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Landroid/os/IBinder;Ljava/lang/String;ZLjava/lang/String;Landroid/os/IBinder;IILjava/lang/String;Lcom/google/android/gms/internal/cz;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/br;->versionCode:I

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->gJ:Lcom/google/android/gms/internal/bo;

    invoke-static {p3}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/ka;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    invoke-static {p4}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bi;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    invoke-static {p5}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/db;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    invoke-static {p6}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/p;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    iput-object p7, p0, Lcom/google/android/gms/internal/br;->gO:Ljava/lang/String;

    iput-boolean p8, p0, Lcom/google/android/gms/internal/br;->gP:Z

    iput-object p9, p0, Lcom/google/android/gms/internal/br;->gQ:Ljava/lang/String;

    invoke-static {p10}, Lcom/google/android/gms/dynamic/l;->a(Landroid/os/IBinder;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Lcom/google/android/gms/dynamic/k;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/bl;

    iput-object v0, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    iput p11, p0, Lcom/google/android/gms/internal/br;->orientation:I

    iput p12, p0, Lcom/google/android/gms/internal/br;->gS:I

    iput-object p13, p0, Lcom/google/android/gms/internal/br;->url:Ljava/lang/String;

    iput-object p14, p0, Lcom/google/android/gms/internal/br;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/bo;Lcom/google/android/gms/internal/ka;Lcom/google/android/gms/internal/bi;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/cz;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/br;->versionCode:I

    iput-object p1, p0, Lcom/google/android/gms/internal/br;->gJ:Lcom/google/android/gms/internal/bo;

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    iput-object p3, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gO:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/br;->gP:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gQ:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/br;->orientation:I

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/internal/br;->gS:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->url:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/gms/internal/br;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/ka;Lcom/google/android/gms/internal/bi;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/db;ILcom/google/android/gms/internal/cz;)V
    .locals 3

    const/4 v2, 0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v2, p0, Lcom/google/android/gms/internal/br;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gJ:Lcom/google/android/gms/internal/bo;

    iput-object p1, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    iput-object p4, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gO:Ljava/lang/String;

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/br;->gP:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gQ:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    iput p5, p0, Lcom/google/android/gms/internal/br;->orientation:I

    iput v2, p0, Lcom/google/android/gms/internal/br;->gS:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->url:Ljava/lang/String;

    iput-object p6, p0, Lcom/google/android/gms/internal/br;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/ka;Lcom/google/android/gms/internal/bi;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/db;ZILcom/google/android/gms/internal/cz;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/br;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gJ:Lcom/google/android/gms/internal/bo;

    iput-object p1, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    iput-object p4, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gO:Ljava/lang/String;

    iput-boolean p5, p0, Lcom/google/android/gms/internal/br;->gP:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gQ:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    iput p6, p0, Lcom/google/android/gms/internal/br;->orientation:I

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/internal/br;->gS:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->url:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/gms/internal/br;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/ka;Lcom/google/android/gms/internal/bi;Lcom/google/android/gms/internal/p;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/db;ZILjava/lang/String;Lcom/google/android/gms/internal/cz;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/br;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gJ:Lcom/google/android/gms/internal/bo;

    iput-object p1, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    iput-object p5, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    iput-object p3, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gO:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/br;->gP:Z

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gQ:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    iput p7, p0, Lcom/google/android/gms/internal/br;->orientation:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/br;->gS:I

    iput-object p8, p0, Lcom/google/android/gms/internal/br;->url:Ljava/lang/String;

    iput-object p9, p0, Lcom/google/android/gms/internal/br;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/internal/ka;Lcom/google/android/gms/internal/bi;Lcom/google/android/gms/internal/p;Lcom/google/android/gms/internal/bl;Lcom/google/android/gms/internal/db;ZILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/internal/cz;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/br;->versionCode:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->gJ:Lcom/google/android/gms/internal/bo;

    iput-object p1, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    iput-object p2, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    iput-object p5, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    iput-object p3, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    iput-object p9, p0, Lcom/google/android/gms/internal/br;->gO:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/internal/br;->gP:Z

    iput-object p8, p0, Lcom/google/android/gms/internal/br;->gQ:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    iput p7, p0, Lcom/google/android/gms/internal/br;->orientation:I

    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/internal/br;->gS:I

    iput-object v1, p0, Lcom/google/android/gms/internal/br;->url:Ljava/lang/String;

    iput-object p10, p0, Lcom/google/android/gms/internal/br;->en:Lcom/google/android/gms/internal/cz;

    return-void
.end method

.method public static a(Landroid/content/Intent;)Lcom/google/android/gms/internal/br;
    .locals 2

    :try_start_0
    const-string v0, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/internal/br;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/br;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/content/Intent;Lcom/google/android/gms/internal/br;)V
    .locals 2

    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    const-string v1, "com.google.android.gms.ads.inernal.overlay.AdOverlayInfo"

    invoke-virtual {p0, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    return-void
.end method


# virtual methods
.method final aa()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/br;->gK:Lcom/google/android/gms/internal/ka;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final ab()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/br;->gL:Lcom/google/android/gms/internal/bi;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final ac()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/br;->gM:Lcom/google/android/gms/internal/db;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final ad()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/br;->gN:Lcom/google/android/gms/internal/p;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method final ae()Landroid/os/IBinder;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/br;->gR:Lcom/google/android/gms/internal/bl;

    invoke-static {v0}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/dynamic/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/bh;->a(Lcom/google/android/gms/internal/br;Landroid/os/Parcel;I)V

    return-void
.end method
