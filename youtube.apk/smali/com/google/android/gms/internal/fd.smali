.class public abstract Lcom/google/android/gms/internal/fd;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/b;
.implements Lcom/google/android/gms/internal/fq;


# static fields
.field public static final d:[Ljava/lang/String;


# instance fields
.field final a:Landroid/os/Handler;

.field b:Z

.field c:Z

.field private final e:Landroid/content/Context;

.field private f:Landroid/os/IInterface;

.field private final g:Ljava/util/ArrayList;

.field private h:Lcom/google/android/gms/internal/fl;

.field private final i:[Ljava/lang/String;

.field private final j:Lcom/google/android/gms/internal/fo;

.field private final k:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "service_esmobile"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "service_googleme"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/internal/fd;->d:[Ljava/lang/String;

    return-void
.end method

.method private varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/b;Lcom/google/android/gms/common/api/c;[Ljava/lang/String;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->g:Ljava/util/ArrayList;

    iput-boolean v1, p0, Lcom/google/android/gms/internal/fd;->b:Z

    iput-boolean v1, p0, Lcom/google/android/gms/internal/fd;->c:Z

    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->k:Ljava/lang/Object;

    invoke-static {p1}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    new-instance v0, Lcom/google/android/gms/internal/fo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/fo;-><init>(Lcom/google/android/gms/internal/fq;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    new-instance v0, Lcom/google/android/gms/internal/ff;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/internal/ff;-><init>(Lcom/google/android/gms/internal/fd;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    iput-object p4, p0, Lcom/google/android/gms/internal/fd;->i:[Ljava/lang/String;

    invoke-static {p2}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/b;

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/fo;->a(Lcom/google/android/gms/common/api/b;)V

    invoke-static {p3}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/c;

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/internal/fo;->a(Lcom/google/android/gms/common/d;)V

    return-void
.end method

.method protected varargs constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/c;Lcom/google/android/gms/common/d;[Ljava/lang/String;)V
    .locals 2

    new-instance v0, Lcom/google/android/gms/internal/fi;

    invoke-direct {v0, p2}, Lcom/google/android/gms/internal/fi;-><init>(Lcom/google/android/gms/common/c;)V

    new-instance v1, Lcom/google/android/gms/internal/fm;

    invoke-direct {v1, p3}, Lcom/google/android/gms/internal/fm;-><init>(Lcom/google/android/gms/common/d;)V

    invoke-direct {p0, p1, v0, v1, p4}, Lcom/google/android/gms/internal/fd;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/b;Lcom/google/android/gms/common/api/c;[Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/internal/fd;Landroid/os/IInterface;)Landroid/os/IInterface;
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/fd;->f:Landroid/os/IInterface;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/internal/fd;Lcom/google/android/gms/internal/fl;)Lcom/google/android/gms/internal/fl;
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/fd;)Lcom/google/android/gms/internal/fo;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/internal/fd;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->k:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/fd;)Ljava/util/ArrayList;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->g:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/fd;)Landroid/os/IInterface;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->f:Landroid/os/IInterface;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/internal/fd;)Lcom/google/android/gms/internal/fl;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/internal/fd;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    return-object v0
.end method


# virtual methods
.method protected abstract a(Landroid/os/IBinder;)Landroid/os/IInterface;
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/gms/internal/fn;

    invoke-direct {v3, p0, p1, p2, p3}, Lcom/google/android/gms/internal/fn;-><init>(Lcom/google/android/gms/internal/fd;ILandroid/os/IBinder;Landroid/os/Bundle;)V

    invoke-virtual {v1, v2, v3}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    new-instance v1, Lcom/google/android/gms/internal/fi;

    invoke-direct {v1, p1}, Lcom/google/android/gms/internal/fi;-><init>(Lcom/google/android/gms/common/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fo;->a(Lcom/google/android/gms/common/api/b;)V

    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/internal/fy;Lcom/google/android/gms/internal/fj;)V
.end method

.method protected abstract b()Ljava/lang/String;
.end method

.method protected final b(Landroid/os/IBinder;)V
    .locals 2

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/internal/fz;->a(Landroid/os/IBinder;)Lcom/google/android/gms/internal/fy;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/internal/fj;

    invoke-direct {v1, p0}, Lcom/google/android/gms/internal/fj;-><init>(Lcom/google/android/gms/internal/fd;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/internal/fd;->a(Lcom/google/android/gms/internal/fy;Lcom/google/android/gms/internal/fj;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    const-string v0, "GmsClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/c;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    new-instance v1, Lcom/google/android/gms/internal/fi;

    invoke-direct {v1, p1}, Lcom/google/android/gms/internal/fi;-><init>(Lcom/google/android/gms/common/c;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fo;->b(Lcom/google/android/gms/common/api/b;)V

    return-void
.end method

.method public final d()V
    .locals 4

    const/4 v3, 0x3

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/fd;->b:Z

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->k:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/internal/fd;->c:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/e;->a(Landroid/content/Context;)I

    move-result v0

    if-eqz v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    :cond_0
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    if-eqz v0, :cond_2

    const-string v0, "GmsClient"

    const-string v1, "Calling connect() while still connected, missing disconnect()."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->f:Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/fr;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/fr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fd;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/fr;->b(Ljava/lang/String;Lcom/google/android/gms/internal/fl;)V

    :cond_2
    new-instance v0, Lcom/google/android/gms/internal/fl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/internal/fl;-><init>(Lcom/google/android/gms/internal/fd;)V

    iput-object v0, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/fr;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/fr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fd;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/fr;->a(Ljava/lang/String;Lcom/google/android/gms/internal/fl;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "GmsClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unable to connect to service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fd;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->a:Landroid/os/Handler;

    const/16 v2, 0x9

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v3, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method

.method public final e()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->f:Landroid/os/IInterface;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Z
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->k:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/internal/fd;->c:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()V
    .locals 5

    const/4 v4, 0x0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/internal/fd;->b:Z

    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->k:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-boolean v2, p0, Lcom/google/android/gms/internal/fd;->c:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    iget-object v2, p0, Lcom/google/android/gms/internal/fd;->g:Ljava/util/ArrayList;

    monitor-enter v2

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/internal/fd;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/fg;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fg;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iput-object v4, p0, Lcom/google/android/gms/internal/fd;->f:Landroid/os/IInterface;

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/internal/fr;->a(Landroid/content/Context;)Lcom/google/android/gms/internal/fr;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fd;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/internal/fr;->b(Ljava/lang/String;Lcom/google/android/gms/internal/fl;)V

    iput-object v4, p0, Lcom/google/android/gms/internal/fd;->h:Lcom/google/android/gms/internal/fl;

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->j:Lcom/google/android/gms/internal/fo;

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fo;->a(I)V

    :cond_1
    return-void

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final h()Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->e:Landroid/content/Context;

    return-object v0
.end method

.method protected final i()V
    .locals 2

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fd;->e()Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Not connected. Call connect() and wait for onConnected() to be called."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method protected final j()Landroid/os/IInterface;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fd;->i()V

    iget-object v0, p0, Lcom/google/android/gms/internal/fd;->f:Landroid/os/IInterface;

    return-object v0
.end method

.method public final k()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/fd;->b:Z

    return v0
.end method
