.class public final Lcom/google/android/gms/internal/kq$b;
.super Lcom/google/android/gms/internal/fe;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/plus/a/b/c;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/ji;

.field private static final Mu:Ljava/util/HashMap;


# instance fields
.field private final Mv:Ljava/util/Set;

.field private NQ:Lcom/google/android/gms/internal/kq$b$a;

.field private NR:Lcom/google/android/gms/internal/kq$b$b;

.field private NS:I

.field private final jE:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    const/4 v6, 0x0

    new-instance v0, Lcom/google/android/gms/internal/ji;

    invoke-direct {v0}, Lcom/google/android/gms/internal/ji;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/kq$b;->CREATOR:Lcom/google/android/gms/internal/ji;

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/kq$b;->Mu:Ljava/util/HashMap;

    const-string v1, "coverInfo"

    const-string v2, "coverInfo"

    const/4 v3, 0x2

    const-class v4, Lcom/google/android/gms/internal/kq$b$a;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/internal/fe$a;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/internal/fe$a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/kq$b;->Mu:Ljava/util/HashMap;

    const-string v1, "coverPhoto"

    const-string v2, "coverPhoto"

    const/4 v3, 0x3

    const-class v4, Lcom/google/android/gms/internal/kq$b$b;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/internal/fe$a;->a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/internal/fe$a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v0, Lcom/google/android/gms/internal/kq$b;->Mu:Ljava/util/HashMap;

    const-string v1, "layout"

    const-string v2, "layout"

    const/4 v3, 0x4

    new-instance v4, Lcom/google/android/gms/internal/fb;

    invoke-direct {v4}, Lcom/google/android/gms/internal/fb;-><init>()V

    const-string v5, "banner"

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/internal/fb;->c(Ljava/lang/String;I)Lcom/google/android/gms/internal/fb;

    move-result-object v4

    invoke-static {v2, v3, v4, v6}, Lcom/google/android/gms/internal/fe$a;->a(Ljava/lang/String;ILcom/google/android/gms/internal/gq;Z)Lcom/google/android/gms/internal/fe$a;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/gms/internal/fe;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/kq$b;->jE:I

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILcom/google/android/gms/internal/kq$b$a;Lcom/google/android/gms/internal/kq$b$b;I)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/internal/fe;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    iput p2, p0, Lcom/google/android/gms/internal/kq$b;->jE:I

    iput-object p3, p0, Lcom/google/android/gms/internal/kq$b;->NQ:Lcom/google/android/gms/internal/kq$b$a;

    iput-object p4, p0, Lcom/google/android/gms/internal/kq$b;->NR:Lcom/google/android/gms/internal/kq$b$b;

    iput p5, p0, Lcom/google/android/gms/internal/kq$b;->NS:I

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/internal/fe$a;)Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/internal/fe$a;->cq()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method protected final ac(Ljava/lang/String;)Ljava/lang/Object;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method protected final ad(Ljava/lang/String;)Z
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method protected final b(Lcom/google/android/gms/internal/fe$a;)Ljava/lang/Object;
    .locals 3

    invoke-virtual {p1}, Lcom/google/android/gms/internal/fe$a;->cq()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown safe parcelable id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/internal/fe$a;->cq()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->NQ:Lcom/google/android/gms/internal/kq$b$a;

    :goto_0
    return-object v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->NR:Lcom/google/android/gms/internal/kq$b$b;

    goto :goto_0

    :pswitch_2
    iget v0, p0, Lcom/google/android/gms/internal/kq$b;->NS:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final cj()Ljava/util/HashMap;
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/kq$b;->Mu:Ljava/util/HashMap;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/kq$b;->CREATOR:Lcom/google/android/gms/internal/ji;

    const/4 v0, 0x0

    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 5

    const/4 v2, 0x1

    const/4 v1, 0x0

    instance-of v0, p1, Lcom/google/android/gms/internal/kq$b;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    check-cast p1, Lcom/google/android/gms/internal/kq$b;

    sget-object v0, Lcom/google/android/gms/internal/kq$b;->Mu:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/fe$a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/kq$b;->a(Lcom/google/android/gms/internal/fe$a;)Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/kq$b;->a(Lcom/google/android/gms/internal/fe$a;)Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/kq$b;->b(Lcom/google/android/gms/internal/fe$a;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/kq$b;->b(Lcom/google/android/gms/internal/fe$a;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0

    :cond_4
    invoke-virtual {p1, v0}, Lcom/google/android/gms/internal/kq$b;->a(Lcom/google/android/gms/internal/fe$a;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    goto :goto_0

    :cond_5
    move v0, v2

    goto :goto_0
.end method

.method final fD()Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    return-object v0
.end method

.method public final synthetic freeze()Ljava/lang/Object;
    .locals 1

    invoke-virtual {p0}, Lcom/google/android/gms/internal/kq$b;->gj()Lcom/google/android/gms/internal/kq$b;

    move-result-object v0

    return-object v0
.end method

.method public final getCoverInfo()Lcom/google/android/gms/plus/a/b/d;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->NQ:Lcom/google/android/gms/internal/kq$b$a;

    return-object v0
.end method

.method public final getCoverPhoto()Lcom/google/android/gms/plus/a/b/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->NR:Lcom/google/android/gms/internal/kq$b$b;

    return-object v0
.end method

.method public final getLayout()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/kq$b;->NS:I

    return v0
.end method

.method final getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/kq$b;->jE:I

    return v0
.end method

.method final gh()Lcom/google/android/gms/internal/kq$b$a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->NQ:Lcom/google/android/gms/internal/kq$b$a;

    return-object v0
.end method

.method final gi()Lcom/google/android/gms/internal/kq$b$b;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->NR:Lcom/google/android/gms/internal/kq$b$b;

    return-object v0
.end method

.method public final gj()Lcom/google/android/gms/internal/kq$b;
    .locals 0

    return-object p0
.end method

.method public final hasCoverInfo()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hasCoverPhoto()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hasLayout()Z
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/kq$b;->Mv:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final hashCode()I
    .locals 4

    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/gms/internal/kq$b;->Mu:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/internal/fe$a;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/kq$b;->a(Lcom/google/android/gms/internal/fe$a;)Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/internal/fe$a;->cq()I

    move-result v3

    add-int/2addr v1, v3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/kq$b;->b(Lcom/google/android/gms/internal/fe$a;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->hashCode()I

    move-result v0

    add-int/2addr v0, v1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final isDataValid()Z
    .locals 1

    const/4 v0, 0x1

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/kq$b;->CREATOR:Lcom/google/android/gms/internal/ji;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/ji;->a(Lcom/google/android/gms/internal/kq$b;Landroid/os/Parcel;I)V

    return-void
.end method
