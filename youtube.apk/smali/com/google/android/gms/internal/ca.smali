.class public final Lcom/google/android/gms/internal/ca;
.super Lcom/google/android/gms/internal/de;

# interfaces
.implements Lcom/google/android/gms/internal/ch;
.implements Lcom/google/android/gms/internal/du;


# instance fields
.field private final a:Lcom/google/android/gms/internal/at;

.field private final b:Lcom/google/android/gms/internal/bz;

.field private final c:Lcom/google/android/gms/internal/db;

.field private final d:Ljava/lang/Object;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/lang/Object;

.field private final g:Lcom/google/android/gms/internal/cm;

.field private final h:Lcom/google/android/gms/internal/hh;

.field private i:Lcom/google/android/gms/internal/de;

.field private j:Lcom/google/android/gms/internal/cg;

.field private k:Z

.field private l:Lcom/google/android/gms/internal/ag;

.field private m:Lcom/google/android/gms/internal/aj;

.field private n:Lcom/google/android/gms/internal/ap;


# direct methods
.method private a(Lcom/google/android/gms/internal/ce;)Lcom/google/android/gms/internal/ac;
    .locals 11

    const/4 v3, 0x0

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v0, v0, Lcom/google/android/gms/internal/cg;->hE:Ljava/lang/String;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    const-string v1, "The ad response must specify one of the supported ad sizes."

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v0, v0, Lcom/google/android/gms/internal/cg;->hE:Ljava/lang/String;

    const-string v1, "x"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x2

    if-eq v1, v2, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse the ad size from the ad response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v2, v2, Lcom/google/android/gms/internal/cg;->hE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    const/4 v1, 0x0

    :try_start_0
    aget-object v1, v0, v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v1, 0x1

    aget-object v0, v0, v1

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v5

    iget-object v0, p1, Lcom/google/android/gms/internal/ce;->eq:Lcom/google/android/gms/internal/ac;

    iget-object v6, v0, Lcom/google/android/gms/internal/ac;->eL:[Lcom/google/android/gms/internal/ac;

    array-length v7, v6

    move v2, v3

    :goto_0
    if-ge v2, v7, :cond_5

    aget-object v8, v6, v2

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v1, v0, Landroid/util/DisplayMetrics;->density:F

    iget v0, v8, Lcom/google/android/gms/internal/ac;->width:I

    const/4 v9, -0x1

    if-ne v0, v9, :cond_2

    iget v0, v8, Lcom/google/android/gms/internal/ac;->widthPixels:I

    int-to-float v0, v0

    div-float/2addr v0, v1

    float-to-int v0, v0

    :goto_1
    iget v9, v8, Lcom/google/android/gms/internal/ac;->height:I

    const/4 v10, -0x2

    if-ne v9, v10, :cond_3

    iget v9, v8, Lcom/google/android/gms/internal/ac;->heightPixels:I

    int-to-float v9, v9

    div-float v1, v9, v1

    float-to-int v1, v1

    :goto_2
    if-ne v4, v0, :cond_4

    if-ne v5, v1, :cond_4

    new-instance v0, Lcom/google/android/gms/internal/ac;

    iget-object v1, p1, Lcom/google/android/gms/internal/ce;->eq:Lcom/google/android/gms/internal/ac;

    iget-object v1, v1, Lcom/google/android/gms/internal/ac;->eL:[Lcom/google/android/gms/internal/ac;

    invoke-direct {v0, v8, v1}, Lcom/google/android/gms/internal/ac;-><init>(Lcom/google/android/gms/internal/ac;[Lcom/google/android/gms/internal/ac;)V

    return-object v0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not parse the ad size from the ad response: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v2, v2, Lcom/google/android/gms/internal/cg;->hE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_2
    iget v0, v8, Lcom/google/android/gms/internal/ac;->width:I

    goto :goto_1

    :cond_3
    iget v1, v8, Lcom/google/android/gms/internal/ac;->height:I

    goto :goto_2

    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_5
    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "The ad size from the ad response was not one of the requested sizes: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v2, v2, Lcom/google/android/gms/internal/cg;->hE:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/internal/ca;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    return-object v0
.end method

.method private a(J)V
    .locals 3

    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/internal/ca;->b(J)Z

    move-result v0

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    const-string v1, "Timed out waiting for WebView to finish loading."

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v0

    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/internal/ca;->k:Z

    if-eqz v0, :cond_0

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/internal/ca;)Lcom/google/android/gms/internal/bz;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->b:Lcom/google/android/gms/internal/bz;

    return-object v0
.end method

.method private b(J)Z
    .locals 4

    const-wide/32 v0, 0xea60

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v2, p1

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    invoke-virtual {v2, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v0, 0x1

    goto :goto_0

    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/internal/ca$a;

    const-string v1, "Ad request cancelled."

    const/4 v2, -0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/ca;)Lcom/google/android/gms/internal/cg;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/internal/ca;)Lcom/google/android/gms/internal/db;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->c:Lcom/google/android/gms/internal/db;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 25

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    move-object/from16 v24, v0

    monitor-enter v24

    :try_start_0
    const-string v2, "AdLoaderBackgroundTask started."

    invoke-static {v2}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->h:Lcom/google/android/gms/internal/hh;

    invoke-virtual {v2}, Lcom/google/android/gms/internal/hh;->a()Lcom/google/android/gms/internal/dp;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/internal/ca;->e:Landroid/content/Context;

    invoke-interface {v2, v3}, Lcom/google/android/gms/internal/dp;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    new-instance v12, Lcom/google/android/gms/internal/ce;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/internal/ca;->g:Lcom/google/android/gms/internal/cm;

    invoke-direct {v12, v3, v2}, Lcom/google/android/gms/internal/ce;-><init>(Lcom/google/android/gms/internal/cm;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    const/4 v3, 0x0

    const/4 v6, -0x2

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->e:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-static {v2, v12, v0}, Lcom/google/android/gms/internal/cf;->a(Landroid/content/Context;Lcom/google/android/gms/internal/ce;Lcom/google/android/gms/internal/ch;)Lcom/google/android/gms/internal/de;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ca;->d:Ljava/lang/Object;

    monitor-enter v7
    :try_end_1
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ca;->i:Lcom/google/android/gms/internal/de;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->i:Lcom/google/android/gms/internal/de;

    if-nez v2, :cond_1

    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    const-string v4, "Could not start the ad request service."

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v7

    throw v2
    :try_end_3
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v2

    :try_start_4
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ca$a;->getErrorCode()I

    move-result v6

    const/4 v4, 0x3

    if-eq v6, v4, :cond_0

    const/4 v4, -0x1

    if-ne v6, v4, :cond_9

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ca$a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/do;->b(Ljava/lang/String;)V

    :goto_0
    new-instance v2, Lcom/google/android/gms/internal/cg;

    invoke-direct {v2, v6}, Lcom/google/android/gms/internal/cg;-><init>(I)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    sget-object v2, Lcom/google/android/gms/internal/dn;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/internal/cb;

    move-object/from16 v0, p0

    invoke-direct {v4, v0}, Lcom/google/android/gms/internal/cb;-><init>(Lcom/google/android/gms/internal/ca;)V

    invoke-virtual {v2, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v21, v3

    :goto_1
    new-instance v2, Lcom/google/android/gms/internal/dc;

    iget-object v3, v12, Lcom/google/android/gms/internal/ce;->hu:Lcom/google/android/gms/internal/aa;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/internal/ca;->c:Lcom/google/android/gms/internal/db;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v5, v5, Lcom/google/android/gms/internal/cg;->fO:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v7, v7, Lcom/google/android/gms/internal/cg;->fP:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v8, v8, Lcom/google/android/gms/internal/cg;->hD:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget v9, v9, Lcom/google/android/gms/internal/cg;->orientation:I

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-wide v10, v10, Lcom/google/android/gms/internal/cg;->fS:J

    iget-object v12, v12, Lcom/google/android/gms/internal/ce;->hx:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-boolean v13, v13, Lcom/google/android/gms/internal/cg;->hB:Z

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    if-eqz v14, :cond_a

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    iget-object v14, v14, Lcom/google/android/gms/internal/ap;->b:Lcom/google/android/gms/internal/ai;

    :goto_2
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    if-eqz v15, :cond_b

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    iget-object v15, v15, Lcom/google/android/gms/internal/ap;->c:Lcom/google/android/gms/internal/av;

    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    move-object/from16 v16, v0

    if-eqz v16, :cond_c

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/android/gms/internal/ap;->d:Ljava/lang/String;

    move-object/from16 v16, v0

    :goto_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->m:Lcom/google/android/gms/internal/aj;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    move-object/from16 v18, v0

    if-eqz v18, :cond_d

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/google/android/gms/internal/ap;->e:Lcom/google/android/gms/internal/al;

    move-object/from16 v18, v0

    :goto_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    iget-wide v0, v0, Lcom/google/android/gms/internal/cg;->hC:J

    move-wide/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    move-object/from16 v22, v0

    move-object/from16 v0, v22

    iget-wide v0, v0, Lcom/google/android/gms/internal/cg;->hA:J

    move-wide/from16 v22, v0

    invoke-direct/range {v2 .. v23}, Lcom/google/android/gms/internal/dc;-><init>(Lcom/google/android/gms/internal/aa;Lcom/google/android/gms/internal/db;Ljava/util/List;ILjava/util/List;Ljava/util/List;IJLjava/lang/String;ZLcom/google/android/gms/internal/ai;Lcom/google/android/gms/internal/av;Ljava/lang/String;Lcom/google/android/gms/internal/aj;Lcom/google/android/gms/internal/al;JLcom/google/android/gms/internal/ac;J)V

    sget-object v3, Lcom/google/android/gms/internal/dn;->a:Landroid/os/Handler;

    new-instance v4, Lcom/google/android/gms/internal/cc;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/internal/cc;-><init>(Lcom/google/android/gms/internal/ca;Lcom/google/android/gms/internal/dc;)V

    invoke-virtual {v3, v4}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    monitor-exit v24
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    return-void

    :cond_1
    :try_start_5
    monitor-exit v7
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_2
    :try_start_6
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/ca;->b(J)Z

    move-result v2

    if-nez v2, :cond_3

    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    const-string v4, "Timed out waiting for ad response."

    const/4 v5, 0x2

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2
    :try_end_6
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    :catchall_1
    move-exception v2

    monitor-exit v24

    throw v2

    :cond_3
    :try_start_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    if-eqz v2, :cond_2

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ca;->d:Ljava/lang/Object;

    monitor-enter v7
    :try_end_7
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    const/4 v2, 0x0

    :try_start_8
    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ca;->i:Lcom/google/android/gms/internal/de;

    monitor-exit v7
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    :try_start_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget v2, v2, Lcom/google/android/gms/internal/cg;->errorCode:I

    const/4 v7, -0x2

    if-eq v2, v7, :cond_4

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget v2, v2, Lcom/google/android/gms/internal/cg;->errorCode:I

    const/4 v7, -0x3

    if-eq v2, v7, :cond_4

    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "There was a problem getting an ad response. ErrorCode: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget v5, v5, Lcom/google/android/gms/internal/cg;->errorCode:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget v5, v5, Lcom/google/android/gms/internal/cg;->errorCode:I

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2

    :catchall_2
    move-exception v2

    monitor-exit v7

    throw v2

    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget v2, v2, Lcom/google/android/gms/internal/cg;->errorCode:I

    const/4 v7, -0x3

    if-eq v2, v7, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v2, v2, Lcom/google/android/gms/internal/cg;->hz:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    const-string v4, "No fill from ad server."

    const/4 v5, 0x3

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/cg;->hB:Z
    :try_end_9
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_9 .. :try_end_9} :catch_0
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    if-eqz v2, :cond_6

    :try_start_a
    new-instance v2, Lcom/google/android/gms/internal/aj;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v7, v7, Lcom/google/android/gms/internal/cg;->hz:Ljava/lang/String;

    invoke-direct {v2, v7}, Lcom/google/android/gms/internal/aj;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ca;->m:Lcom/google/android/gms/internal/aj;
    :try_end_a
    .catch Lorg/json/JSONException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_a .. :try_end_a} :catch_0
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    :cond_6
    :try_start_b
    iget-object v2, v12, Lcom/google/android/gms/internal/ce;->eq:Lcom/google/android/gms/internal/ac;

    iget-object v2, v2, Lcom/google/android/gms/internal/ac;->eL:[Lcom/google/android/gms/internal/ac;

    if-eqz v2, :cond_7

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/gms/internal/ca;->a(Lcom/google/android/gms/internal/ce;)Lcom/google/android/gms/internal/ac;

    move-result-object v3

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-boolean v2, v2, Lcom/google/android/gms/internal/cg;->hB:Z

    if-eqz v2, :cond_8

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/gms/internal/ca;->d:Ljava/lang/Object;

    monitor-enter v7
    :try_end_b
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_b .. :try_end_b} :catch_0
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    :try_start_c
    new-instance v2, Lcom/google/android/gms/internal/ag;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/gms/internal/ca;->e:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/internal/ca;->a:Lcom/google/android/gms/internal/at;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/internal/ca;->m:Lcom/google/android/gms/internal/aj;

    invoke-direct {v2, v8, v12, v9, v10}, Lcom/google/android/gms/internal/ag;-><init>(Landroid/content/Context;Lcom/google/android/gms/internal/ce;Lcom/google/android/gms/internal/at;Lcom/google/android/gms/internal/aj;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ca;->l:Lcom/google/android/gms/internal/ag;

    monitor-exit v7
    :try_end_c
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->l:Lcom/google/android/gms/internal/ag;

    const-wide/32 v7, 0xea60

    invoke-virtual {v2, v4, v5, v7, v8}, Lcom/google/android/gms/internal/ag;->a(JJ)Lcom/google/android/gms/internal/ap;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    iget v2, v2, Lcom/google/android/gms/internal/ap;->a:I

    packed-switch v2, :pswitch_data_0

    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unexpected mediation result: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/ca;->n:Lcom/google/android/gms/internal/ap;

    iget v5, v5, Lcom/google/android/gms/internal/ap;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2

    :catch_1
    move-exception v2

    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not parse mediation config: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v5, v5, Lcom/google/android/gms/internal/cg;->hz:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2

    :catchall_3
    move-exception v2

    monitor-exit v7

    throw v2

    :pswitch_0
    new-instance v2, Lcom/google/android/gms/internal/ca$a;

    const-string v4, "No fill from any mediation ad networks."

    const/4 v5, 0x3

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/internal/ca$a;-><init>(Ljava/lang/String;I)V

    throw v2

    :cond_8
    sget-object v2, Lcom/google/android/gms/internal/dn;->a:Landroid/os/Handler;

    new-instance v7, Lcom/google/android/gms/internal/cd;

    move-object/from16 v0, p0

    invoke-direct {v7, v0}, Lcom/google/android/gms/internal/cd;-><init>(Lcom/google/android/gms/internal/ca;)V

    invoke-virtual {v2, v7}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    move-object/from16 v0, p0

    invoke-direct {v0, v4, v5}, Lcom/google/android/gms/internal/ca;->a(J)V
    :try_end_d
    .catch Lcom/google/android/gms/internal/ca$a; {:try_start_d .. :try_end_d} :catch_0
    .catchall {:try_start_d .. :try_end_d} :catchall_1

    :pswitch_1
    move-object/from16 v21, v3

    goto/16 :goto_1

    :cond_9
    :try_start_e
    invoke-virtual {v2}, Lcom/google/android/gms/internal/ca$a;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/internal/do;->d(Ljava/lang/String;)V
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_1

    goto/16 :goto_0

    :cond_a
    const/4 v14, 0x0

    goto/16 :goto_2

    :cond_b
    const/4 v15, 0x0

    goto/16 :goto_3

    :cond_c
    const/16 v16, 0x0

    goto/16 :goto_4

    :cond_d
    const/16 v18, 0x0

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/internal/cg;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "Received ad response."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    iput-object p1, p0, Lcom/google/android/gms/internal/ca;->j:Lcom/google/android/gms/internal/cg;

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/internal/db;)V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    const-string v0, "WebView finished loading."

    invoke-static {v0}, Lcom/google/android/gms/internal/do;->a(Ljava/lang/String;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/internal/ca;->k:Z

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->f:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notify()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final v_()V
    .locals 2

    iget-object v1, p0, Lcom/google/android/gms/internal/ca;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->i:Lcom/google/android/gms/internal/de;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->i:Lcom/google/android/gms/internal/de;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/de;->f()V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->c:Lcom/google/android/gms/internal/db;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/db;->stopLoading()V

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->c:Lcom/google/android/gms/internal/db;

    invoke-static {v0}, Lcom/google/android/gms/internal/dj;->a(Landroid/webkit/WebView;)V

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->l:Lcom/google/android/gms/internal/ag;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/internal/ca;->l:Lcom/google/android/gms/internal/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ag;->a()V

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
