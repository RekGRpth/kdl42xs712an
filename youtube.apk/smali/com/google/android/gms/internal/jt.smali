.class public abstract Lcom/google/android/gms/internal/jt;
.super Ljava/lang/Object;


# instance fields
.field protected f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/internal/jt;->f:I

    return-void
.end method

.method public static final a(Lcom/google/android/gms/internal/jt;[BII)V
    .locals 3

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0, p3}, Lcom/google/android/gms/internal/ll;->a([BII)Lcom/google/android/gms/internal/ll;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/jt;->a(Lcom/google/android/gms/internal/ll;)V

    invoke-virtual {v0}, Lcom/google/android/gms/internal/ll;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-void

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Serializing to a byte array threw an IOException (should never happen)."

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static final b(Lcom/google/android/gms/internal/jt;[BII)Lcom/google/android/gms/internal/jt;
    .locals 2

    const/4 v0, 0x0

    :try_start_0
    invoke-static {p1, v0, p3}, Lcom/google/android/gms/internal/js;->a([BII)Lcom/google/android/gms/internal/js;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/internal/jt;->a(Lcom/google/android/gms/internal/js;)Lcom/google/android/gms/internal/jt;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/js;->a(I)V
    :try_end_0
    .catch Lcom/google/android/gms/internal/lm; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    return-object p0

    :catch_0
    move-exception v0

    throw v0

    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Reading from a byte array threw an IOException (should never happen)."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()I
    .locals 1

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/internal/jt;->f:I

    return v0
.end method

.method public abstract a(Lcom/google/android/gms/internal/js;)Lcom/google/android/gms/internal/jt;
.end method

.method public abstract a(Lcom/google/android/gms/internal/ll;)V
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    invoke-static {p0}, Lcom/google/android/gms/internal/jv;->a(Lcom/google/android/gms/internal/jt;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
