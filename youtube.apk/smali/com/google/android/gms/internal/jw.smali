.class public final Lcom/google/android/gms/internal/jw;
.super Ljava/lang/Object;


# static fields
.field static final a:I

.field static final b:I

.field static final c:I

.field static final d:I

.field public static final e:[I

.field public static final f:[J

.field public static final g:[F

.field public static final h:[D

.field public static final i:[Z

.field public static final j:[Ljava/lang/String;

.field public static final k:[[B

.field public static final l:[B


# direct methods
.method static constructor <clinit>()V
    .locals 2

    const/4 v1, 0x0

    const/16 v0, 0xb

    sput v0, Lcom/google/android/gms/internal/jw;->a:I

    const/16 v0, 0xc

    sput v0, Lcom/google/android/gms/internal/jw;->b:I

    const/16 v0, 0x10

    sput v0, Lcom/google/android/gms/internal/jw;->c:I

    const/16 v0, 0x1a

    sput v0, Lcom/google/android/gms/internal/jw;->d:I

    new-array v0, v1, [I

    sput-object v0, Lcom/google/android/gms/internal/jw;->e:[I

    new-array v0, v1, [J

    sput-object v0, Lcom/google/android/gms/internal/jw;->f:[J

    new-array v0, v1, [F

    sput-object v0, Lcom/google/android/gms/internal/jw;->g:[F

    new-array v0, v1, [D

    sput-object v0, Lcom/google/android/gms/internal/jw;->h:[D

    new-array v0, v1, [Z

    sput-object v0, Lcom/google/android/gms/internal/jw;->i:[Z

    new-array v0, v1, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/internal/jw;->j:[Ljava/lang/String;

    new-array v0, v1, [[B

    sput-object v0, Lcom/google/android/gms/internal/jw;->k:[[B

    new-array v0, v1, [B

    sput-object v0, Lcom/google/android/gms/internal/jw;->l:[B

    return-void
.end method

.method static a(I)I
    .locals 1

    and-int/lit8 v0, p0, 0x7

    return v0
.end method

.method static a(II)I
    .locals 1

    shl-int/lit8 v0, p0, 0x3

    or-int/2addr v0, p1

    return v0
.end method

.method public static a(Lcom/google/android/gms/internal/js;I)Z
    .locals 1

    invoke-virtual {p0, p1}, Lcom/google/android/gms/internal/js;->b(I)Z

    move-result v0

    return v0
.end method

.method public static b(I)I
    .locals 1

    ushr-int/lit8 v0, p0, 0x3

    return v0
.end method
