.class public Lcom/google/android/gms/internal/fe$a;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/internal/gr;


# instance fields
.field private final jE:I

.field protected final up:I

.field protected final uq:Z

.field protected final ur:I

.field protected final us:Z

.field protected final ut:Ljava/lang/String;

.field protected final uu:I

.field protected final uv:Ljava/lang/Class;

.field protected final uw:Ljava/lang/String;

.field private ux:Lcom/google/android/gms/internal/fh;

.field private uy:Lcom/google/android/gms/internal/gq;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/internal/gr;

    invoke-direct {v0}, Lcom/google/android/gms/internal/gr;-><init>()V

    sput-object v0, Lcom/google/android/gms/internal/fe$a;->CREATOR:Lcom/google/android/gms/internal/gr;

    return-void
.end method

.method constructor <init>(IIZIZLjava/lang/String;ILjava/lang/String;Lcom/google/android/gms/internal/ez;)V
    .locals 2

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/internal/fe$a;->jE:I

    iput p2, p0, Lcom/google/android/gms/internal/fe$a;->up:I

    iput-boolean p3, p0, Lcom/google/android/gms/internal/fe$a;->uq:Z

    iput p4, p0, Lcom/google/android/gms/internal/fe$a;->ur:I

    iput-boolean p5, p0, Lcom/google/android/gms/internal/fe$a;->us:Z

    iput-object p6, p0, Lcom/google/android/gms/internal/fe$a;->ut:Ljava/lang/String;

    iput p7, p0, Lcom/google/android/gms/internal/fe$a;->uu:I

    if-nez p8, :cond_0

    iput-object v1, p0, Lcom/google/android/gms/internal/fe$a;->uv:Ljava/lang/Class;

    iput-object v1, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    :goto_0
    if-nez p9, :cond_1

    iput-object v1, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    :goto_1
    return-void

    :cond_0
    const-class v0, Lcom/google/android/gms/internal/fk;

    iput-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uv:Ljava/lang/Class;

    iput-object p8, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    goto :goto_0

    :cond_1
    invoke-virtual {p9}, Lcom/google/android/gms/internal/ez;->cf()Lcom/google/android/gms/internal/gq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    goto :goto_1
.end method

.method protected constructor <init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/internal/fe$a;->jE:I

    iput p1, p0, Lcom/google/android/gms/internal/fe$a;->up:I

    iput-boolean p2, p0, Lcom/google/android/gms/internal/fe$a;->uq:Z

    iput p3, p0, Lcom/google/android/gms/internal/fe$a;->ur:I

    iput-boolean p4, p0, Lcom/google/android/gms/internal/fe$a;->us:Z

    iput-object p5, p0, Lcom/google/android/gms/internal/fe$a;->ut:Ljava/lang/String;

    iput p6, p0, Lcom/google/android/gms/internal/fe$a;->uu:I

    iput-object p7, p0, Lcom/google/android/gms/internal/fe$a;->uv:Ljava/lang/Class;

    if-nez p7, :cond_0

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    :goto_0
    iput-object p8, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    return-void

    :cond_0
    invoke-virtual {p7}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;ILcom/google/android/gms/internal/gq;Z)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    invoke-interface {p2}, Lcom/google/android/gms/internal/gq;->ch()I

    move-result v1

    invoke-interface {p2}, Lcom/google/android/gms/internal/gq;->ci()I

    move-result v3

    const/4 v4, 0x0

    const/4 v7, 0x0

    move v2, p3

    move-object v5, p0

    move v6, p1

    move-object v8, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/16 v1, 0xb

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    const/4 v8, 0x0

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method public static b(Ljava/lang/String;ILjava/lang/Class;)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/16 v1, 0xb

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    const/4 v8, 0x0

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v7, p2

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/internal/fe$a;)Lcom/google/android/gms/internal/gq;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    return-object v0
.end method

.method public static d(Ljava/lang/String;I)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x0

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    move v2, v1

    move v3, v1

    move v4, v1

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method public static e(Ljava/lang/String;I)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x4

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method public static f(Ljava/lang/String;I)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x6

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method public static g(Ljava/lang/String;I)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x7

    const/4 v2, 0x0

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method

.method public static h(Ljava/lang/String;I)Lcom/google/android/gms/internal/fe$a;
    .locals 9

    const/4 v7, 0x0

    const/4 v1, 0x7

    const/4 v2, 0x1

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    move v3, v1

    move v4, v2

    move-object v5, p0

    move v6, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/internal/fe$a;-><init>(IZIZLjava/lang/String;ILjava/lang/Class;Lcom/google/android/gms/internal/gq;)V

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/internal/fh;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/internal/fe$a;->ux:Lcom/google/android/gms/internal/fh;

    return-void
.end method

.method public ch()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/fe$a;->up:I

    return v0
.end method

.method public ci()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/fe$a;->ur:I

    return v0
.end method

.method public cm()Lcom/google/android/gms/internal/fe$a;
    .locals 10

    new-instance v0, Lcom/google/android/gms/internal/fe$a;

    iget v1, p0, Lcom/google/android/gms/internal/fe$a;->jE:I

    iget v2, p0, Lcom/google/android/gms/internal/fe$a;->up:I

    iget-boolean v3, p0, Lcom/google/android/gms/internal/fe$a;->uq:Z

    iget v4, p0, Lcom/google/android/gms/internal/fe$a;->ur:I

    iget-boolean v5, p0, Lcom/google/android/gms/internal/fe$a;->us:Z

    iget-object v6, p0, Lcom/google/android/gms/internal/fe$a;->ut:Ljava/lang/String;

    iget v7, p0, Lcom/google/android/gms/internal/fe$a;->uu:I

    iget-object v8, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fe$a;->cu()Lcom/google/android/gms/internal/ez;

    move-result-object v9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/internal/fe$a;-><init>(IIZIZLjava/lang/String;ILjava/lang/String;Lcom/google/android/gms/internal/ez;)V

    return-object v0
.end method

.method public cn()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/fe$a;->uq:Z

    return v0
.end method

.method public co()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/internal/fe$a;->us:Z

    return v0
.end method

.method public cp()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->ut:Ljava/lang/String;

    return-object v0
.end method

.method public cq()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/fe$a;->uu:I

    return v0
.end method

.method public cr()Ljava/lang/Class;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uv:Ljava/lang/Class;

    return-object v0
.end method

.method cs()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    goto :goto_0
.end method

.method public ct()Z
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method cu()Lcom/google/android/gms/internal/ez;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    invoke-static {v0}, Lcom/google/android/gms/internal/ez;->a(Lcom/google/android/gms/internal/gq;)Lcom/google/android/gms/internal/ez;

    move-result-object v0

    goto :goto_0
.end method

.method public cv()Ljava/util/HashMap;
    .locals 2

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->ux:Lcom/google/android/gms/internal/fh;

    invoke-static {v0}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->ux:Lcom/google/android/gms/internal/fh;

    iget-object v1, p0, Lcom/google/android/gms/internal/fe$a;->uw:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/internal/fh;->ag(Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/fe$a;->CREATOR:Lcom/google/android/gms/internal/gr;

    const/4 v0, 0x0

    return v0
.end method

.method public g(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    invoke-interface {v0, p1}, Lcom/google/android/gms/internal/gq;->g(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/internal/fe$a;->jE:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    const/16 v3, 0xa

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Field\n"

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, "            versionCode="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/internal/fe$a;->jE:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "                 typeIn="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/internal/fe$a;->up:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "            typeInArray="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fe$a;->uq:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "                typeOut="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/internal/fe$a;->ur:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "           typeOutArray="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/internal/fe$a;->us:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "        outputFieldName="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/internal/fe$a;->ut:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "      safeParcelFieldId="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/internal/fe$a;->uu:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v0, "       concreteTypeName="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fe$a;->cs()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fe$a;->cr()Ljava/lang/Class;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v0, "     concreteType.class="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/internal/fe$a;->cr()Ljava/lang/Class;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    :cond_0
    const-string v0, "          converterName="

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/internal/fe$a;->uy:Lcom/google/android/gms/internal/gq;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/internal/fe$a;->CREATOR:Lcom/google/android/gms/internal/gr;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/internal/gr;->a(Lcom/google/android/gms/internal/fe$a;Landroid/os/Parcel;I)V

    return-void
.end method
