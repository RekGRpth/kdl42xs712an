.class public Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ad;


# instance fields
.field final jE:I

.field final jV:[Lcom/google/android/gms/appdatasearch/CorpusId;

.field final jW:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

.field private final transient jX:Ljava/util/Map;

.field private final transient jY:Ljava/util/Map;

.field public final scoringVerbosityLevel:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/ad;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ad;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ad;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/appdatasearch/CorpusId;I[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;)V
    .locals 6

    const/4 v5, 0x0

    const/4 v2, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jV:[Lcom/google/android/gms/appdatasearch/CorpusId;

    iput p3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->scoringVerbosityLevel:I

    iput-object p4, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jW:[Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_4

    :cond_0
    iput-object v5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jX:Ljava/util/Map;

    :cond_1
    if-eqz p4, :cond_2

    array-length v0, p4

    if-nez v0, :cond_7

    :cond_2
    iput-object v5, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    :cond_3
    return-void

    :cond_4
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jX:Ljava/util/Map;

    move v1, v2

    :goto_0
    array-length v0, p2

    if-ge v1, v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jX:Ljava/util/Map;

    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_5

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jX:Ljava/util/Map;

    aget-object v4, p2, v1

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/CorpusId;->packageName:Ljava/lang/String;

    invoke-interface {v3, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_5
    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->corpusName:Ljava/lang/String;

    if-eqz v3, :cond_6

    aget-object v3, p2, v1

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/CorpusId;->corpusName:Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    :cond_6
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_7
    new-instance v0, Ljava/util/HashMap;

    array-length v1, p4

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    :goto_1
    array-length v0, p4

    if-ge v2, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    aget-object v1, p4, v2

    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->corpus:Lcom/google/android/gms/appdatasearch/CorpusId;

    aget-object v3, p4, v2

    invoke-interface {v0, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ad;

    const/4 v0, 0x0

    return v0
.end method

.method public getCorpusScoringInfo(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;
    .locals 3

    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    if-nez v0, :cond_1

    move-object v0, v1

    :cond_0
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v2, p1, p2}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v2, p1, v1}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    goto :goto_0
.end method

.method public getCorpusScoringInfos()Ljava/util/Collection;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jY:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    goto :goto_0
.end method

.method public shouldBeSearched(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jX:Ljava/util/Map;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->jX:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_2

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ad;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ad;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
