.class public Lcom/google/android/gms/appdatasearch/w;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ao;


# instance fields
.field final jE:I

.field final jz:Ljava/lang/String;

.field final kI:[Lcom/google/android/gms/appdatasearch/DocumentId;

.field final kJ:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/ao;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ao;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/w;->CREATOR:Lcom/google/android/gms/appdatasearch/ao;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/w;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/w;->jz:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/w;->kI:[Lcom/google/android/gms/appdatasearch/DocumentId;

    iput p4, p0, Lcom/google/android/gms/appdatasearch/w;->kJ:I

    return-void
.end method

.method constructor <init>(Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V
    .locals 1

    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/appdatasearch/w;-><init>(ILjava/lang/String;[Lcom/google/android/gms/appdatasearch/DocumentId;I)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/w;->CREATOR:Lcom/google/android/gms/appdatasearch/ao;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/w;->CREATOR:Lcom/google/android/gms/appdatasearch/ao;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ao;->a(Lcom/google/android/gms/appdatasearch/w;Landroid/os/Parcel;I)V

    return-void
.end method
