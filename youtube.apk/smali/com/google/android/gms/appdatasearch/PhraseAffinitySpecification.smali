.class public Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/appdatasearch/ai;

.field public static final MAX_NUM_PHRASES:I = 0x64


# instance fields
.field final jE:I

.field final ks:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/ai;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/ai;-><init>()V

    sput-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ai;

    return-void
.end method

.method constructor <init>(I[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->jE:I

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->ks:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-void
.end method

.method public constructor <init>([Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V
    .locals 2

    const/4 v1, 0x1

    invoke-virtual {p1}, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;-><init>(I[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)V

    return-void
.end method

.method public static builder()Lcom/google/android/gms/appdatasearch/h;
    .locals 1

    new-instance v0, Lcom/google/android/gms/appdatasearch/h;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/h;-><init>()V

    return-object v0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ai;

    const/4 v0, 0x0

    return v0
.end method

.method public getCorpusSpecs()[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->ks:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-virtual {v0}, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    sget-object v0, Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;->CREATOR:Lcom/google/android/gms/appdatasearch/ai;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/appdatasearch/ai;->a(Lcom/google/android/gms/appdatasearch/PhraseAffinitySpecification;Landroid/os/Parcel;I)V

    return-void
.end method
