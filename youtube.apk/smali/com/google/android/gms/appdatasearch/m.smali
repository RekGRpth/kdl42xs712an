.class final Lcom/google/android/gms/appdatasearch/m;
.super Lcom/google/android/gms/appdatasearch/l;


# instance fields
.field final synthetic c:Lcom/google/android/gms/appdatasearch/SearchResults;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/appdatasearch/SearchResults;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/gms/appdatasearch/m;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {p0, p1}, Lcom/google/android/gms/appdatasearch/l;-><init>(Lcom/google/android/gms/appdatasearch/SearchResults;)V

    iput-object p2, p0, Lcom/google/android/gms/appdatasearch/m;->d:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/appdatasearch/m;->e:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/m;->a:I

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/m;->a()V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    const/4 v1, 0x1

    const/4 v2, 0x0

    :cond_0
    iget v0, p0, Lcom/google/android/gms/appdatasearch/m;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/appdatasearch/m;->a:I

    iget v0, p0, Lcom/google/android/gms/appdatasearch/m;->a:I

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/m;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-virtual {v3}, Lcom/google/android/gms/appdatasearch/SearchResults;->getNumResults()I

    move-result v3

    if-ge v0, v3, :cond_2

    iget v0, p0, Lcom/google/android/gms/appdatasearch/m;->a:I

    iget-object v3, p0, Lcom/google/android/gms/appdatasearch/m;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/SearchResults;->kR:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/appdatasearch/m;->c:Lcom/google/android/gms/appdatasearch/SearchResults;

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/SearchResults;->kQ:[I

    aget v0, v4, v0

    aget-object v3, v3, v0

    const/16 v0, 0x2d

    invoke-virtual {v3, v0}, Ljava/lang/String;->indexOf(I)I

    move-result v4

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/m;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v4, v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/m;->d:Ljava/lang/String;

    invoke-virtual {v3, v2, v0, v2, v4}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/m;->e:Ljava/lang/String;

    if-eqz v5, :cond_1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v0

    sub-int/2addr v0, v4

    add-int/lit8 v0, v0, -0x1

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/m;->e:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v5

    if-ne v0, v5, :cond_4

    add-int/lit8 v4, v4, 0x1

    iget-object v5, p0, Lcom/google/android/gms/appdatasearch/m;->e:Ljava/lang/String;

    invoke-virtual {v3, v4, v5, v2, v0}, Ljava/lang/String;->regionMatches(ILjava/lang/String;II)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    :cond_1
    :goto_1
    if-eqz v0, :cond_0

    :cond_2
    return-void

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v2

    goto :goto_1
.end method
