.class public final Lcom/google/android/gms/location/reporting/c;
.super Ljava/lang/Object;


# instance fields
.field private final a:Landroid/accounts/Account;

.field private final b:Ljava/lang/String;

.field private final c:J

.field private d:J

.field private e:J

.field private f:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/accounts/Account;Ljava/lang/String;J)V
    .locals 2

    const-wide v0, 0x7fffffffffffffffL

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-wide v0, p0, Lcom/google/android/gms/location/reporting/c;->d:J

    iput-wide v0, p0, Lcom/google/android/gms/location/reporting/c;->e:J

    const-string v0, "account"

    invoke-static {p1, v0}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iput-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Landroid/accounts/Account;

    const-string v0, "reason"

    invoke-static {p2, v0}, Lcom/google/android/gms/internal/gi;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/location/reporting/c;->b:Ljava/lang/String;

    iput-wide p3, p0, Lcom/google/android/gms/location/reporting/c;->c:J

    return-void
.end method

.method synthetic constructor <init>(Landroid/accounts/Account;Ljava/lang/String;JB)V
    .locals 0

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/location/reporting/c;-><init>(Landroid/accounts/Account;Ljava/lang/String;J)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/location/reporting/c;)Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->a:Landroid/accounts/Account;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/location/reporting/c;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/location/reporting/c;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/location/reporting/c;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/gms/location/reporting/c;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/location/reporting/c;->d:J

    return-wide v0
.end method

.method static synthetic e(Lcom/google/android/gms/location/reporting/c;)J
    .locals 2

    iget-wide v0, p0, Lcom/google/android/gms/location/reporting/c;->e:J

    return-wide v0
.end method

.method static synthetic f(Lcom/google/android/gms/location/reporting/c;)Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/location/reporting/c;->f:Ljava/lang/String;

    return-object v0
.end method
