.class public final Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;
.super Ljava/lang/Object;

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field OA:I

.field OB:Z

.field OC:Z

.field OD:Z

.field OE:Z

.field OF:[Lcom/google/android/gms/wallet/CountrySpecification;

.field Oz:Ljava/lang/String;

.field account:Landroid/accounts/Account;

.field environment:I

.field hy:Ljava/lang/String;

.field private final jE:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Lcom/google/android/gms/wallet/l;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->jE:I

    return-void
.end method

.method constructor <init>(IILandroid/accounts/Account;Ljava/lang/String;IZZZLjava/lang/String;Z[Lcom/google/android/gms/wallet/CountrySpecification;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->jE:I

    iput p2, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->environment:I

    iput-object p3, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->account:Landroid/accounts/Account;

    iput-object p4, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->Oz:Ljava/lang/String;

    iput p5, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OA:I

    iput-boolean p6, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OB:Z

    iput-boolean p7, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OC:Z

    iput-boolean p8, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OD:Z

    iput-object p9, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->hy:Ljava/lang/String;

    iput-boolean p10, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OE:Z

    iput-object p11, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OF:[Lcom/google/android/gms/wallet/CountrySpecification;

    return-void
.end method

.method public static newBuilder()Lcom/google/android/gms/wallet/c;
    .locals 3

    new-instance v0, Lcom/google/android/gms/wallet/c;

    new-instance v1, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/c;-><init>(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;B)V

    return-object v0
.end method

.method public static newBuilderFrom(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;)Lcom/google/android/gms/wallet/c;
    .locals 3

    invoke-static {}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->newBuilder()Lcom/google/android/gms/wallet/c;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->getAccount()Landroid/accounts/Account;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->account:Landroid/accounts/Account;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->getEnvironment()I

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->environment:I

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->getMerchantDomain()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->Oz:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->isPhoneNumberRequired()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OB:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->getSessionId()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->hy:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->isShippingAddressRequired()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OC:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->shouldAllowSaveToChromeOption()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OE:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->useMinimalBillingAddress()Z

    move-result v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-boolean v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OD:Z

    invoke-virtual {p0}, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->getAllowedShippingCountrySpecifications()[Lcom/google/android/gms/wallet/CountrySpecification;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/wallet/c;->a:Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;

    iput-object v1, v2, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OF:[Lcom/google/android/gms/wallet/CountrySpecification;

    return-object v0
.end method


# virtual methods
.method public final describeContents()I
    .locals 1

    const/4 v0, 0x0

    return v0
.end method

.method public final getAccount()Landroid/accounts/Account;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->account:Landroid/accounts/Account;

    return-object v0
.end method

.method public final getAllowedShippingCountrySpecifications()[Lcom/google/android/gms/wallet/CountrySpecification;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OF:[Lcom/google/android/gms/wallet/CountrySpecification;

    return-object v0
.end method

.method public final getEnvironment()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->environment:I

    return v0
.end method

.method public final getMerchantDomain()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->Oz:Ljava/lang/String;

    return-object v0
.end method

.method public final getSessionId()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->hy:Ljava/lang/String;

    return-object v0
.end method

.method public final getVersionCode()I
    .locals 1

    iget v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->jE:I

    return v0
.end method

.method public final isPhoneNumberRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OB:Z

    return v0
.end method

.method public final isShippingAddressRequired()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OC:Z

    return v0
.end method

.method public final shouldAllowSaveToChromeOption()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OE:Z

    return v0
.end method

.method public final useMinimalBillingAddress()Z
    .locals 1

    iget-boolean v0, p0, Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;->OD:Z

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/l;->a(Lcom/google/android/gms/wallet/ImmediateFullWalletRequest;Landroid/os/Parcel;I)V

    return-void
.end method
