.class public abstract Lcom/google/android/gms/wallet/ia/a;
.super Ljava/lang/Object;


# instance fields
.field private Pr:Lcom/google/android/gms/wallet/shared/b;

.field private Ps:Lcom/google/android/gms/wallet/shared/a;

.field protected mArgs:Landroid/os/Bundle;

.field protected mIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->mIntent:Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->mIntent:Landroid/content/Intent;

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->mIntent:Landroid/content/Intent;

    invoke-virtual {v0, p2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->mArgs:Landroid/os/Bundle;

    invoke-static {}, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->newBuilder()Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/a;->mArgs:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/a;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Ps:Lcom/google/android/gms/wallet/shared/a;

    invoke-static {}, Lcom/google/android/gms/wallet/shared/BuyFlowConfig;->newBuilder()Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/b;->b(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/wallet/shared/b;->c(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Pr:Lcom/google/android/gms/wallet/shared/b;

    return-void
.end method


# virtual methods
.method public build()Landroid/content/Intent;
    .locals 3

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Pr:Lcom/google/android/gms/wallet/shared/b;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/a;->Ps:Lcom/google/android/gms/wallet/shared/a;

    invoke-virtual {v1}, Lcom/google/android/gms/wallet/shared/a;->a()Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/wallet/shared/b;->a(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)Lcom/google/android/gms/wallet/shared/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/wallet/shared/b;->a()Lcom/google/android/gms/wallet/shared/BuyFlowConfig;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/a;->mIntent:Landroid/content/Intent;

    const-string v2, "com.google.android.gms.wallet.buyFlowConfig"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/wallet/ia/a;->mIntent:Landroid/content/Intent;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/wallet/ia/a;->onIntentBuilt(Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method protected onIntentBuilt(Landroid/content/Intent;Lcom/google/android/gms/wallet/shared/BuyFlowConfig;)Landroid/content/Intent;
    .locals 0

    return-object p1
.end method

.method public setBuyerAccount(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/ia/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Ps:Lcom/google/android/gms/wallet/shared/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/shared/a;->a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;

    return-object p0
.end method

.method public setEnvironment(I)Lcom/google/android/gms/wallet/ia/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Ps:Lcom/google/android/gms/wallet/shared/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/shared/a;->a(I)Lcom/google/android/gms/wallet/shared/a;

    return-object p0
.end method

.method public setTheme(I)Lcom/google/android/gms/wallet/ia/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Ps:Lcom/google/android/gms/wallet/shared/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/shared/a;->b(I)Lcom/google/android/gms/wallet/shared/a;

    return-object p0
.end method

.method public setTransactionId(Ljava/lang/String;)Lcom/google/android/gms/wallet/ia/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/gms/wallet/ia/a;->Pr:Lcom/google/android/gms/wallet/shared/b;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/wallet/shared/b;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/shared/b;

    return-object p0
.end method
