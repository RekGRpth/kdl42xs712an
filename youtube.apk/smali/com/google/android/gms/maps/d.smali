.class final Lcom/google/android/gms/maps/d;
.super Lcom/google/android/gms/dynamic/b;


# instance fields
.field protected a:Lcom/google/android/gms/dynamic/o;

.field private final b:Landroid/app/Fragment;

.field private c:Landroid/app/Activity;


# direct methods
.method constructor <init>(Landroid/app/Fragment;)V
    .locals 0

    invoke-direct {p0}, Lcom/google/android/gms/dynamic/b;-><init>()V

    iput-object p1, p0, Lcom/google/android/gms/maps/d;->b:Landroid/app/Fragment;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/maps/d;Landroid/app/Activity;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/maps/d;->c:Landroid/app/Activity;

    invoke-direct {p0}, Lcom/google/android/gms/maps/d;->g()V

    return-void
.end method

.method private g()V
    .locals 4

    iget-object v0, p0, Lcom/google/android/gms/maps/d;->c:Landroid/app/Activity;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/maps/d;->a:Lcom/google/android/gms/dynamic/o;

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/maps/d;->a()Lcom/google/android/gms/dynamic/a;

    move-result-object v0

    if-nez v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/d;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/g;->a(Landroid/content/Context;)I

    iget-object v0, p0, Lcom/google/android/gms/maps/d;->c:Landroid/app/Activity;

    invoke-static {v0}, Lcom/google/android/gms/maps/a/bn;->a(Landroid/content/Context;)Lcom/google/android/gms/maps/a/z;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/d;->c:Landroid/app/Activity;

    invoke-static {v1}, Lcom/google/android/gms/dynamic/n;->a(Ljava/lang/Object;)Lcom/google/android/gms/dynamic/k;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/a/z;->b(Lcom/google/android/gms/dynamic/k;)Lcom/google/android/gms/maps/a/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/maps/d;->a:Lcom/google/android/gms/dynamic/o;

    new-instance v2, Lcom/google/android/gms/maps/c;

    iget-object v3, p0, Lcom/google/android/gms/maps/d;->b:Landroid/app/Fragment;

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/maps/c;-><init>(Landroid/app/Fragment;Lcom/google/android/gms/maps/a/j;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/dynamic/o;->a(Lcom/google/android/gms/dynamic/a;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/GooglePlayServicesNotAvailableException; {:try_start_0 .. :try_end_0} :catch_1

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    new-instance v1, Lcom/google/android/gms/maps/model/RuntimeRemoteException;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/RuntimeRemoteException;-><init>(Landroid/os/RemoteException;)V

    throw v1

    :catch_1
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/dynamic/o;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/gms/maps/d;->a:Lcom/google/android/gms/dynamic/o;

    invoke-direct {p0}, Lcom/google/android/gms/maps/d;->g()V

    return-void
.end method
