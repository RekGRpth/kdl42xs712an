.class final Lcom/google/android/apps/youtube/api/b/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/api/t;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/b/a;

.field private b:Lcom/google/android/youtube/player/internal/y;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/b/a;Lcom/google/android/youtube/player/internal/y;)V
    .locals 1

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/b/h;->a:Lcom/google/android/apps/youtube/api/b/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/player/internal/y;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/h;->b:Lcom/google/android/youtube/player/internal/y;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/h;->b:Lcom/google/android/youtube/player/internal/y;

    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;Ljava/lang/String;ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/h;->b:Lcom/google/android/youtube/player/internal/y;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/h;->b:Lcom/google/android/youtube/player/internal/y;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/youtube/player/internal/y;->a(Landroid/graphics/Bitmap;Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZ)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/h;->b:Lcom/google/android/youtube/player/internal/y;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/h;->b:Lcom/google/android/youtube/player/internal/y;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/youtube/player/internal/y;->a(Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
