.class final Lcom/google/android/apps/youtube/api/jar/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/as;


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/jar/l;-><init>(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->e()V

    :cond_0
    return-void
.end method

.method public final a(D)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v3}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->c(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L    # 20000.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    sub-int v0, v3, v0

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a(I)V

    :cond_0
    return-void
.end method

.method public final a(I)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a(I)V

    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PLAYING:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a()V

    goto :goto_0
.end method

.method public final b(D)V
    .locals 6

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->max(DD)D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v3}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->c(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)I

    move-result v3

    const-wide v4, 0x40d3880000000000L    # 20000.0

    mul-double/2addr v0, v4

    double-to-int v0, v0

    add-int/2addr v0, v3

    invoke-interface {v2, v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->a(I)V

    :cond_0
    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->a(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;->PAUSED:Lcom/google/android/apps/youtube/core/player/overlay/ControlsState;

    if-ne v0, v1, :cond_1

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->b()V

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->d()V

    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/l;->a:Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;->b(Lcom/google/android/apps/youtube/api/jar/ApiTvControlsOverlay;)Lcom/google/android/apps/youtube/core/player/overlay/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/core/player/overlay/p;->g()V

    :cond_0
    return-void
.end method
