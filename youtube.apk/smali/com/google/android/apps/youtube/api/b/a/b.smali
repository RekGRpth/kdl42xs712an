.class public final Lcom/google/android/apps/youtube/api/b/a/b;
.super Lcom/google/android/apps/youtube/api/b/a/am;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/os/Handler;

.field private final c:Lcom/google/android/youtube/api/service/c;

.field private final d:Lcom/google/android/apps/youtube/api/j;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Handler;Lcom/google/android/youtube/api/service/c;Lcom/google/android/apps/youtube/api/j;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/am;-><init>()V

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->a:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->b:Landroid/os/Handler;

    invoke-static {p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/youtube/api/service/c;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->c:Lcom/google/android/youtube/api/service/c;

    invoke-static {p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/j;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->d:Lcom/google/android/apps/youtube/api/j;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/b;)Landroid/content/Context;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->a:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/b/a/b;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->b:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/apps/youtube/api/b/a/b;)Lcom/google/android/youtube/api/service/c;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->c:Lcom/google/android/youtube/api/service/c;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/apps/youtube/api/b/a/b;)Lcom/google/android/apps/youtube/api/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/b;->d:Lcom/google/android/apps/youtube/api/j;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/api/jar/a/ck;Lcom/google/android/apps/youtube/api/jar/a/cw;Lcom/google/android/apps/youtube/api/jar/a/dc;Lcom/google/android/apps/youtube/api/jar/a/df;Lcom/google/android/apps/youtube/api/jar/a/ct;Lcom/google/android/apps/youtube/api/jar/a/ce;Lcom/google/android/apps/youtube/api/jar/a/di;Lcom/google/android/apps/youtube/api/jar/a/ch;Lcom/google/android/apps/youtube/api/jar/a/cn;Lcom/google/android/apps/youtube/api/jar/a/cq;Lcom/google/android/apps/youtube/api/jar/a/cz;Lcom/google/android/apps/youtube/api/jar/a/dl;Z)Lcom/google/android/apps/youtube/api/b/a/ao;
    .locals 19

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p2 .. p2}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    if-nez p13, :cond_0

    invoke-static/range {p3 .. p3}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    :goto_0
    invoke-static/range {p5 .. p5}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p6 .. p6}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p7 .. p7}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p8 .. p8}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p9 .. p9}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p10 .. p10}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p11 .. p11}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-static/range {p12 .. p12}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v17, Landroid/os/ConditionVariable;

    invoke-direct/range {v17 .. v17}, Landroid/os/ConditionVariable;-><init>()V

    new-instance v16, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/apps/youtube/api/b/a/b;->b:Landroid/os/Handler;

    move-object/from16 v18, v0

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/c;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move-object/from16 v10, p8

    move-object/from16 v11, p9

    move-object/from16 v12, p10

    move-object/from16 v13, p11

    move-object/from16 v14, p12

    move/from16 v15, p13

    invoke-direct/range {v1 .. v17}, Lcom/google/android/apps/youtube/api/b/a/c;-><init>(Lcom/google/android/apps/youtube/api/b/a/b;Lcom/google/android/apps/youtube/api/jar/a/ck;Lcom/google/android/apps/youtube/api/jar/a/cw;Lcom/google/android/apps/youtube/api/jar/a/dc;Lcom/google/android/apps/youtube/api/jar/a/df;Lcom/google/android/apps/youtube/api/jar/a/ct;Lcom/google/android/apps/youtube/api/jar/a/ce;Lcom/google/android/apps/youtube/api/jar/a/di;Lcom/google/android/apps/youtube/api/jar/a/ch;Lcom/google/android/apps/youtube/api/jar/a/cn;Lcom/google/android/apps/youtube/api/jar/a/cq;Lcom/google/android/apps/youtube/api/jar/a/cz;Lcom/google/android/apps/youtube/api/jar/a/dl;ZLjava/util/concurrent/atomic/AtomicReference;Landroid/os/ConditionVariable;)V

    move-object/from16 v0, v18

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual/range {v17 .. v17}, Landroid/os/ConditionVariable;->block()V

    invoke-virtual/range {v16 .. v16}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/api/b/a/ao;

    return-object v1

    :cond_0
    invoke-static/range {p4 .. p4}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
