.class public final Lcom/google/android/apps/youtube/api/b/a/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/a;


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/b/a/bk;

.field private b:Lcom/google/android/apps/youtube/api/jar/a/ce;


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/apps/youtube/api/jar/a/ce;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    const-string v0, "client cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/a/ce;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    new-instance v0, Lcom/google/android/apps/youtube/api/b/a/bk;

    invoke-direct {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/bk;-><init>(Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->a:Lcom/google/android/apps/youtube/api/b/a/bk;

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->a:Lcom/google/android/apps/youtube/api/b/a/bk;

    invoke-interface {p2, v0}, Lcom/google/android/apps/youtube/api/jar/a/ce;->a(Lcom/google/android/apps/youtube/api/b/a/af;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    return-void
.end method

.method public final a(II)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    invoke-interface {v0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/ce;->a(II)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;ZZLjava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/apps/youtube/api/jar/a/ce;->a(Ljava/lang/String;ZZLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final b()Landroid/view/View;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final c()Lcom/google/android/apps/youtube/core/player/overlay/PlayerOverlaysLayout$LayoutParams;
    .locals 1

    const/4 v0, 0x0

    return-object v0
.end method

.method public final d()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/jar/a/ce;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setAdTitle(Ljava/lang/String;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ce;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setChannel(Landroid/graphics/Bitmap;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ce;->a(Landroid/graphics/Bitmap;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setFullscreen(Z)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    if-eqz v0, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->b:Lcom/google/android/apps/youtube/api/jar/a/ce;

    invoke-interface {v0, p1}, Lcom/google/android/apps/youtube/api/jar/a/ce;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final setListener(Lcom/google/android/apps/youtube/core/player/overlay/b;)V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/bj;->a:Lcom/google/android/apps/youtube/api/b/a/bk;

    invoke-virtual {v0, p1}, Lcom/google/android/apps/youtube/api/b/a/bk;->a(Lcom/google/android/apps/youtube/core/player/overlay/b;)V

    return-void
.end method
