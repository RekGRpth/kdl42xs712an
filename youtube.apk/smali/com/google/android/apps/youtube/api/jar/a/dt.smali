.class public final Lcom/google/android/apps/youtube/api/jar/a/dt;
.super Lcom/google/android/apps/youtube/api/jar/a/cu;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/api/jar/h;

.field private final b:Landroid/os/Handler;

.field private final c:Ljava/lang/Runnable;

.field private final d:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/api/jar/h;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/cu;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/h;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->a:Lcom/google/android/apps/youtube/api/jar/h;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/a/du;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/du;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dt;Lcom/google/android/apps/youtube/api/jar/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->c:Ljava/lang/Runnable;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/a/dv;

    invoke-direct {v0, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/dv;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dt;Lcom/google/android/apps/youtube/api/jar/h;)V

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->d:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/dt;)Lcom/google/android/apps/youtube/api/jar/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->a:Lcom/google/android/apps/youtube/api/jar/h;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/dw;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/dw;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dt;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(II)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/dx;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/apps/youtube/api/jar/a/dx;-><init>(Lcom/google/android/apps/youtube/api/jar/a/dt;II)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->c:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final c()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->b:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/a/dt;->d:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
