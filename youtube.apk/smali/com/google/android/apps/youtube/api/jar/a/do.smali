.class public final Lcom/google/android/apps/youtube/api/jar/a/do;
.super Lcom/google/android/apps/youtube/api/jar/a/cr;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/apps/youtube/core/player/overlay/ak;

.field private final b:Landroid/os/Handler;

.field private c:Lcom/google/android/apps/youtube/api/jar/a/ds;


# direct methods
.method public constructor <init>(Lcom/google/android/apps/youtube/core/player/overlay/ak;Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/jar/a/cr;-><init>()V

    const-string v0, "target cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/ak;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->a:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    const-string v0, "uiHandler cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->b:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/do;)Lcom/google/android/apps/youtube/api/jar/a/ds;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->c:Lcom/google/android/apps/youtube/api/jar/a/ds;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/a/do;Lcom/google/android/apps/youtube/api/jar/a/ds;)Lcom/google/android/apps/youtube/api/jar/a/ds;
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->c:Lcom/google/android/apps/youtube/api/jar/a/ds;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/a/do;)Lcom/google/android/apps/youtube/core/player/overlay/ak;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->a:Lcom/google/android/apps/youtube/core/player/overlay/ak;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/dr;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/a/dr;-><init>(Lcom/google/android/apps/youtube/api/jar/a/do;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(JZZ)V
    .locals 7

    iget-object v6, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->b:Landroid/os/Handler;

    new-instance v0, Lcom/google/android/apps/youtube/api/jar/a/dq;

    move-object v1, p0

    move-wide v2, p1

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/apps/youtube/api/jar/a/dq;-><init>(Lcom/google/android/apps/youtube/api/jar/a/do;JZZ)V

    invoke-virtual {v6, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/b/a/au;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/a/dp;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/jar/a/dp;-><init>(Lcom/google/android/apps/youtube/api/jar/a/do;Lcom/google/android/apps/youtube/api/b/a/au;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public final b()V
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->c:Lcom/google/android/apps/youtube/api/jar/a/ds;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->c:Lcom/google/android/apps/youtube/api/jar/a/ds;

    invoke-virtual {v0}, Lcom/google/android/apps/youtube/api/jar/a/ds;->a()V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/a/do;->c:Lcom/google/android/apps/youtube/api/jar/a/ds;

    :cond_0
    return-void
.end method
