.class public final Lcom/google/android/apps/youtube/api/jar/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:I

.field private b:I

.field private c:I

.field private final d:Landroid/widget/TextView;

.field private final e:Landroid/widget/TextView;

.field private final f:Landroid/widget/ProgressBar;

.field private g:Lcom/google/android/apps/youtube/api/jar/q;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    sget v0, Lcom/google/android/youtube/api/c;->r:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->d:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/api/c;->d:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->e:Landroid/widget/TextView;

    sget v0, Lcom/google/android/youtube/api/c;->p:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->f:Landroid/widget/ProgressBar;

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->f:Landroid/widget/ProgressBar;

    new-instance v1, Lcom/google/android/apps/youtube/api/jar/p;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/jar/p;-><init>(Lcom/google/android/apps/youtube/api/jar/o;)V

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/jar/o;)Lcom/google/android/apps/youtube/api/jar/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->g:Lcom/google/android/apps/youtube/api/jar/q;

    return-object v0
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 0

    if-eqz p0, :cond_0

    if-eqz p1, :cond_0

    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    :cond_0
    return-void
.end method

.method static synthetic b(Lcom/google/android/apps/youtube/api/jar/o;)I
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->a:I

    return v0
.end method


# virtual methods
.method public final a(III)V
    .locals 3

    const/4 v2, 0x3

    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->b:I

    if-eq v0, p1, :cond_0

    iput p1, p0, Lcom/google/android/apps/youtube/api/jar/o;->b:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->e:Landroid/widget/TextView;

    invoke-static {p1, v2}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/jar/o;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_0
    iget v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->a:I

    if-eq v0, p2, :cond_1

    iput p2, p0, Lcom/google/android/apps/youtube/api/jar/o;->a:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->d:Landroid/widget/TextView;

    invoke-static {p2, v2}, Lcom/google/android/apps/youtube/common/e/m;->a(II)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/api/jar/o;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    :cond_1
    mul-int v0, p3, p2

    div-int/lit8 v0, v0, 0x64

    iput v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->c:I

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->f:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/o;->a:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setMax(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->f:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/o;->b:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setProgress(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->f:Landroid/widget/ProgressBar;

    iget v1, p0, Lcom/google/android/apps/youtube/api/jar/o;->c:I

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setSecondaryProgress(I)V

    return-void
.end method

.method public final a(Lcom/google/android/apps/youtube/api/jar/q;)V
    .locals 1

    invoke-static {p1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/jar/q;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/jar/o;->g:Lcom/google/android/apps/youtube/api/jar/q;

    return-void
.end method

.method public final a(Z)V
    .locals 2

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/o;->d:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/o;->e:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/jar/o;->f:Landroid/widget/ProgressBar;

    invoke-virtual {v1, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    return-void

    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method
