.class final Lcom/google/android/apps/youtube/api/q;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/apps/youtube/api/ApiPlayer;


# direct methods
.method private constructor <init>(Lcom/google/android/apps/youtube/api/ApiPlayer;)V
    .locals 0

    iput-object p1, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/apps/youtube/api/ApiPlayer;B)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/google/android/apps/youtube/api/q;-><init>(Lcom/google/android/apps/youtube/api/ApiPlayer;)V

    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->a(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    move-result-object v0

    sget-object v1, Lcom/google/android/apps/youtube/api/ApiPlayer$State;->DESTROYED:Lcom/google/android/apps/youtube/api/ApiPlayer$State;

    if-ne v0, v1, :cond_0

    :goto_0
    return-void

    :cond_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->b(Lcom/google/android/apps/youtube/api/ApiPlayer;)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->d(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->c(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/ApiPlayer;->c(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/ae;->f()I

    move-result v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/apps/youtube/api/p;->a(II)V

    goto :goto_0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->d(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/p;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v1}, Lcom/google/android/apps/youtube/api/ApiPlayer;->c(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->a(I)V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->e(Lcom/google/android/apps/youtube/api/ApiPlayer;)V

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->d(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/p;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/apps/youtube/api/p;->h()V

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->e(Lcom/google/android/apps/youtube/api/ApiPlayer;)V

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->d(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/p;

    move-result-object v1

    iget v0, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x5

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    iget-object v2, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v2}, Lcom/google/android/apps/youtube/api/ApiPlayer;->c(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/core/player/ae;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/apps/youtube/core/player/ae;->e()I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/apps/youtube/api/p;->a(ZI)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/apps/youtube/api/q;->a:Lcom/google/android/apps/youtube/api/ApiPlayer;

    invoke-static {v0}, Lcom/google/android/apps/youtube/api/ApiPlayer;->d(Lcom/google/android/apps/youtube/api/ApiPlayer;)Lcom/google/android/apps/youtube/api/p;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-interface {v0, v1}, Lcom/google/android/apps/youtube/api/p;->b(I)V

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_4
    .end packed-switch
.end method
