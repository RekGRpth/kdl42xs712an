.class final Lcom/google/android/apps/youtube/api/b/a/cp;
.super Lcom/google/android/apps/youtube/api/b/a/av;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/player/overlay/al;


# instance fields
.field private final a:Landroid/os/Handler;

.field private b:Lcom/google/android/apps/youtube/core/player/overlay/al;


# direct methods
.method public constructor <init>(Landroid/os/Handler;)V
    .locals 1

    invoke-direct {p0}, Lcom/google/android/apps/youtube/api/b/a/av;-><init>()V

    const-string v0, "uiHandler cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Handler;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cp;->a:Landroid/os/Handler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/b/a/cp;)Lcom/google/android/apps/youtube/core/player/overlay/al;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cp;->b:Lcom/google/android/apps/youtube/core/player/overlay/al;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/apps/youtube/core/player/overlay/al;)V
    .locals 1

    const-string v0, "listener cannot be null"

    invoke-static {p1, v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/core/player/overlay/al;

    iput-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cp;->b:Lcom/google/android/apps/youtube/core/player/overlay/al;

    return-void
.end method

.method public final b()V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/b/a/cp;->a:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/api/b/a/cq;

    invoke-direct {v1, p0}, Lcom/google/android/apps/youtube/api/b/a/cq;-><init>(Lcom/google/android/apps/youtube/api/b/a/cp;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method
