.class public final Lcom/google/android/apps/youtube/api/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/apps/youtube/core/client/bd;
.implements Lcom/google/android/apps/youtube/core/player/fetcher/d;
.implements Lcom/google/android/apps/youtube/core/player/p;


# static fields
.field private static final a:Ljava/util/Map;

.field private static final b:Ljava/util/concurrent/atomic/AtomicReference;

.field private static final c:Ljava/util/List;

.field private static final d:Ljava/util/List;

.field private static final e:Ljava/util/List;


# instance fields
.field private final A:Lcom/google/android/apps/youtube/core/au;

.field private final B:Landroid/content/SharedPreferences;

.field private final C:Lcom/google/android/apps/youtube/common/network/h;

.field private final D:Lcom/google/android/apps/youtube/datalib/offline/n;

.field private final E:Lcom/google/android/apps/youtube/core/offline/store/q;

.field private final F:Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;

.field private final G:Lcom/google/android/apps/youtube/core/aw;

.field private final H:Lcom/google/android/apps/youtube/datalib/e/b;

.field private final I:Lcom/google/android/apps/youtube/common/c/a;

.field private final J:Ljava/util/concurrent/Executor;

.field private final K:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

.field private L:I

.field private final f:Landroid/content/Context;

.field private final g:Lcom/google/android/apps/youtube/api/n;

.field private final h:Lcom/google/android/apps/youtube/common/e/n;

.field private final i:Landroid/os/Handler;

.field private final j:Ljava/util/concurrent/Executor;

.field private final k:Lorg/apache/http/client/HttpClient;

.field private final l:Lorg/apache/http/client/HttpClient;

.field private final m:Lcom/google/android/apps/youtube/core/converter/n;

.field private final n:Lcom/google/android/apps/youtube/datalib/innertube/ah;

.field private final o:Lcom/google/android/apps/youtube/datalib/innertube/m;

.field private final p:Lcom/google/android/apps/youtube/core/player/w;

.field private final q:Lcom/google/android/apps/youtube/api/ab;

.field private final r:Lcom/google/android/apps/youtube/core/utils/a;

.field private final s:Lcom/google/android/apps/youtube/core/client/h;

.field private final t:Lcom/google/android/apps/youtube/core/player/ad;

.field private final u:Lcom/google/android/apps/youtube/core/client/e;

.field private final v:Lcom/google/android/apps/youtube/core/client/bj;

.field private final w:Lcom/google/android/apps/youtube/core/client/ce;

.field private final x:Lcom/google/android/apps/youtube/api/a;

.field private final y:Lcom/google/android/apps/youtube/medialib/a;

.field private final z:Lcom/google/android/apps/youtube/core/Analytics;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/api/j;->a:Ljava/util/Map;

    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    sput-object v0, Lcom/google/android/apps/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/api/j;->c:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/api/j;->d:Ljava/util/List;

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/apps/youtube/api/j;->e:Ljava/util/List;

    const-string v0, "YouTubeAndroidPlayerAPI"

    invoke-static {v0}, Lcom/google/android/apps/youtube/common/L;->a(Ljava/lang/String;)V

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/api/n;)V
    .locals 16

    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    const-string v1, "application cannot be null"

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/Context;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    const-string v1, "clientIdentifier cannot be null"

    move-object/from16 v0, p3

    invoke-static {v0, v1}, Lcom/google/android/apps/youtube/common/fromguava/c;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/apps/youtube/api/n;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->g:Lcom/google/android/apps/youtube/api/n;

    invoke-static {}, Lcom/google/android/apps/youtube/common/fromguava/c;->a()V

    const-string v1, "youtube"

    const/4 v2, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    new-instance v1, Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/e/n;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    new-instance v1, Landroid/os/Handler;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->i:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/apps/youtube/common/c/a;

    new-instance v2, Lcom/google/android/apps/youtube/common/b/b;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/common/b/b;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/common/c/a;-><init>(Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/common/e/b;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    new-instance v1, Ljava/util/concurrent/ThreadPoolExecutor;

    const/16 v2, 0x10

    const/16 v3, 0x10

    const-wide/16 v4, 0x3c

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    new-instance v7, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v7}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    new-instance v8, Lcom/google/android/apps/youtube/core/utils/r;

    const/4 v9, 0x1

    invoke-direct {v8, v9}, Lcom/google/android/apps/youtube/core/utils/r;-><init>(I)V

    invoke-direct/range {v1 .. v8}, Ljava/util/concurrent/ThreadPoolExecutor;-><init>(IIJLjava/util/concurrent/TimeUnit;Ljava/util/concurrent/BlockingQueue;Ljava/util/concurrent/ThreadFactory;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/n;->b:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "com.google.android.youtube.player"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x2f

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/player/internal/b/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v4, 0x20

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2f

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x20

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "(Linux; U; Android "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "; "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_0

    const-string v2, "; "

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    sget-object v1, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v2

    if-lez v2, :cond_1

    const-string v2, " Build/"

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_1
    const/16 v1, 0x29

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/apps/youtube/common/network/l;->a(Ljava/lang/String;)Lorg/apache/http/impl/client/AbstractHttpClient;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->k:Lorg/apache/http/client/HttpClient;

    invoke-static {v9}, Lcom/google/android/apps/youtube/common/network/l;->b(Ljava/lang/String;)Lorg/apache/http/impl/client/AbstractHttpClient;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->l:Lorg/apache/http/client/HttpClient;

    invoke-static {}, Lcom/google/android/apps/youtube/core/converter/n;->a()Lcom/google/android/apps/youtube/core/converter/n;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->m:Lcom/google/android/apps/youtube/core/converter/n;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->K:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    new-instance v2, Lcom/google/android/apps/youtube/common/network/b;

    const-string v1, "connectivity"

    move-object/from16 v0, p1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    sget-object v3, Lcom/google/android/apps/youtube/common/network/b;->a:Lcom/google/android/apps/youtube/common/network/d;

    invoke-direct {v2, v1, v3}, Lcom/google/android/apps/youtube/common/network/b;-><init>(Landroid/net/ConnectivityManager;Lcom/google/android/apps/youtube/common/network/d;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/offline/g;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/datalib/offline/g;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->D:Lcom/google/android/apps/youtube/datalib/offline/n;

    new-instance v1, Lcom/google/android/apps/youtube/core/offline/store/b;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/offline/store/b;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->E:Lcom/google/android/apps/youtube/core/offline/store/q;

    new-instance v1, Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/c/a;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->F:Lcom/google/android/apps/youtube/common/network/ConnectivityReceiver;

    new-instance v1, Lcom/google/android/apps/youtube/core/av;

    new-instance v2, Lcom/google/android/apps/youtube/core/client/az;

    invoke-direct {v2}, Lcom/google/android/apps/youtube/core/client/az;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/av;-><init>(Lcom/google/android/apps/youtube/core/client/l;Lcom/google/android/apps/youtube/common/network/h;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->z:Lcom/google/android/apps/youtube/core/Analytics;

    new-instance v1, Lcom/google/android/apps/youtube/core/aw;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v2}, Lcom/google/android/apps/youtube/core/aw;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/network/h;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->G:Lcom/google/android/apps/youtube/core/aw;

    new-instance v1, Lcom/google/android/apps/youtube/common/b/b;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/common/b/b;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->J:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/ai;

    const/16 v2, 0x78

    const/16 v3, 0x1e0

    const/16 v4, 0x53

    const/4 v5, 0x1

    const/4 v6, 0x1

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/youtube/core/client/ai;-><init>(IIIZZ)V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->k:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    const/16 v7, 0x46

    const/16 v8, 0x1e

    move-object v6, v1

    invoke-static/range {v2 .. v8}, Lcom/google/android/apps/youtube/core/client/ah;->a(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/client/ai;II)Lcom/google/android/apps/youtube/core/client/ah;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->v:Lcom/google/android/apps/youtube/core/client/bj;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/ar;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->k:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->m:Lcom/google/android/apps/youtube/core/converter/n;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/apps/youtube/core/client/ar;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->w:Lcom/google/android/apps/youtube/core/client/ce;

    new-instance v1, Lcom/google/android/apps/youtube/core/ax;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "youtube"

    invoke-direct {v1, v2, v3}, Lcom/google/android/apps/youtube/core/ax;-><init>(Landroid/content/ContentResolver;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->A:Lcom/google/android/apps/youtube/core/au;

    new-instance v1, Lcom/google/android/apps/youtube/medialib/a;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->i:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/core/utils/l;->c(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->A:Lcom/google/android/apps/youtube/core/au;

    invoke-interface {v3}, Lcom/google/android/apps/youtube/core/au;->I()Z

    move-result v3

    if-eqz v3, :cond_2

    const/4 v5, 0x1

    :goto_0
    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/core/utils/Util;->c(Landroid/content/Context;)Z

    move-result v6

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/j;->K:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    move-object v3, v9

    invoke-direct/range {v1 .. v8}, Lcom/google/android/apps/youtube/medialib/a;-><init>(Landroid/os/Handler;Ljava/lang/String;Lcom/google/android/apps/youtube/common/network/h;ZZZLcom/google/android/apps/youtube/common/fromguava/e;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->y:Lcom/google/android/apps/youtube/medialib/a;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/a/o;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    invoke-direct {v1, v2}, Lcom/google/android/apps/youtube/datalib/a/o;-><init>(Landroid/content/SharedPreferences;)V

    new-instance v4, Lcom/google/android/apps/youtube/datalib/config/g;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    sget-object v3, Lcom/google/android/apps/youtube/api/z;->d:Landroid/util/SparseArray;

    invoke-direct {v4, v1, v2, v3}, Lcom/google/android/apps/youtube/datalib/config/g;-><init>(Lcom/google/android/apps/youtube/datalib/config/e;Landroid/content/SharedPreferences;Landroid/util/SparseArray;)V

    new-instance v3, Lcom/google/android/apps/youtube/common/network/YouTubeHttpClient;

    invoke-static {v9}, Lcom/google/android/apps/youtube/common/network/l;->a(Ljava/lang/String;)Lorg/apache/http/impl/client/AbstractHttpClient;

    move-result-object v2

    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "%s/%s"

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v8, 0x0

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    aput-object v9, v7, v8

    const/4 v8, 0x1

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/n;->b:Ljava/lang/String;

    aput-object v9, v7, v8

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p1

    invoke-direct {v3, v0, v2, v5}, Lcom/google/android/apps/youtube/common/network/YouTubeHttpClient;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Ljava/lang/String;)V

    new-instance v2, Lcom/android/volley/l;

    new-instance v5, Lcom/android/volley/toolbox/t;

    invoke-direct {v5}, Lcom/android/volley/toolbox/t;-><init>()V

    new-instance v6, Lcom/google/android/apps/youtube/datalib/a/h;

    new-instance v7, Lcom/android/volley/toolbox/d;

    invoke-direct {v7, v3}, Lcom/android/volley/toolbox/d;-><init>(Lorg/apache/http/client/HttpClient;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v6, v7, v1, v3}, Lcom/google/android/apps/youtube/datalib/a/h;-><init>(Lcom/android/volley/toolbox/f;Lcom/google/android/apps/youtube/datalib/config/e;Lcom/google/android/apps/youtube/common/e/b;)V

    invoke-direct {v2, v5, v6}, Lcom/android/volley/l;-><init>(Lcom/android/volley/a;Lcom/android/volley/h;)V

    invoke-virtual {v2}, Lcom/android/volley/l;->a()V

    new-instance v8, Lcom/google/android/apps/youtube/api/r;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/n;->c:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-direct {v8, v0, v1, v3}, Lcom/google/android/apps/youtube/api/r;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v15, v8}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/h;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->A:Lcom/google/android/apps/youtube/core/au;

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v3, v5}, Lcom/google/android/apps/youtube/datalib/innertube/h;-><init>(Landroid/content/Context;Landroid/content/pm/PackageManager;Lcom/google/android/apps/youtube/core/au;)V

    invoke-interface {v15, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/k;->a(Landroid/content/ContentResolver;)Ljava/lang/String;

    move-result-object v6

    const-string v1, "%s_%s_%s"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v5, 0x0

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    aput-object v7, v3, v5

    const/4 v5, 0x1

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/n;->b:Ljava/lang/String;

    aput-object v7, v3, v5

    const/4 v5, 0x2

    move-object/from16 v0, p3

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/n;->c:Ljava/lang/String;

    aput-object v7, v3, v5

    invoke-static {v1, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    new-instance v1, Lcom/google/android/apps/youtube/datalib/c/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    const-string v5, "AIzaSyCjc_pVEDi4qsv5MtC2dMXzpIaDoRFLsxw"

    invoke-direct/range {v1 .. v7}, Lcom/google/android/apps/youtube/datalib/c/a;-><init>(Lcom/android/volley/l;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/apps/youtube/datalib/innertube/r;

    const/4 v9, 0x0

    sget-object v10, Lcom/google/android/apps/youtube/api/j;->e:Ljava/util/List;

    sget-object v11, Lcom/google/android/apps/youtube/api/j;->d:Ljava/util/List;

    const-string v13, "AIzaSyCjc_pVEDi4qsv5MtC2dMXzpIaDoRFLsxw"

    invoke-virtual {v8}, Lcom/google/android/apps/youtube/api/r;->a()Ljava/lang/String;

    move-result-object v14

    move-object v8, v1

    move-object v12, v4

    invoke-direct/range {v7 .. v14}, Lcom/google/android/apps/youtube/datalib/innertube/r;-><init>(Lcom/google/android/apps/youtube/datalib/c/f;Lcom/google/android/apps/youtube/common/fromguava/e;Ljava/util/List;Ljava/util/List;Lcom/google/android/apps/youtube/datalib/config/a;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/player/w;

    invoke-direct {v1}, Lcom/google/android/apps/youtube/core/player/w;-><init>()V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->p:Lcom/google/android/apps/youtube/core/player/w;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/ah;

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/p;

    invoke-direct {v3, v15}, Lcom/google/android/apps/youtube/datalib/innertube/p;-><init>(Ljava/util/List;)V

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct {v1, v7, v3, v2, v4}, Lcom/google/android/apps/youtube/datalib/innertube/ah;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;Lcom/google/android/apps/youtube/common/e/b;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->n:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    new-instance v1, Lcom/google/android/apps/youtube/datalib/innertube/m;

    new-instance v3, Lcom/google/android/apps/youtube/datalib/innertube/p;

    invoke-direct {v3, v15}, Lcom/google/android/apps/youtube/datalib/innertube/p;-><init>(Ljava/util/List;)V

    invoke-direct {v1, v7, v3, v2}, Lcom/google/android/apps/youtube/datalib/innertube/m;-><init>(Lcom/google/android/apps/youtube/datalib/innertube/r;Lcom/google/android/apps/youtube/datalib/innertube/p;Lcom/android/volley/l;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->o:Lcom/google/android/apps/youtube/datalib/innertube/m;

    new-instance v1, Lcom/google/android/apps/youtube/core/async/ac;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    invoke-direct {v1, v3}, Lcom/google/android/apps/youtube/core/async/ac;-><init>(Landroid/content/SharedPreferences;)V

    new-instance v15, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;

    const/16 v3, 0xf

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-direct {v15, v1, v3, v4, v5}, Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;-><init>(Lcom/google/android/apps/youtube/core/async/t;ILcom/google/android/apps/youtube/core/utils/v;Ljava/lang/String;)V

    new-instance v7, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/api/j;->k:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/apps/youtube/api/n;->c:Ljava/lang/String;

    sget-object v13, Lcom/google/android/apps/youtube/api/z;->b:[B

    sget-object v14, Lcom/google/android/apps/youtube/api/z;->c:[B

    move-object/from16 v8, p1

    move-object v12, v6

    invoke-direct/range {v7 .. v14}, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis;-><init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Ljava/lang/String;Ljava/lang/String;[B[B)V

    new-instance v6, Lcom/google/android/apps/youtube/api/a;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/j;->i:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v12, v0, Lcom/google/android/apps/youtube/api/n;->b:Ljava/lang/String;

    move-object/from16 v0, p3

    iget-object v13, v0, Lcom/google/android/apps/youtube/api/n;->c:Ljava/lang/String;

    invoke-direct/range {v6 .. v13}, Lcom/google/android/apps/youtube/api/a;-><init>(Lcom/google/android/apps/youtube/core/client/as;Ljava/util/concurrent/Executor;Landroid/os/Handler;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v6, v0, Lcom/google/android/apps/youtube/api/j;->x:Lcom/google/android/apps/youtube/api/a;

    invoke-static {}, Lcom/google/android/apps/youtube/core/identity/l;->a()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v1

    new-instance v12, Lcom/google/android/apps/youtube/core/identity/ar;

    invoke-direct {v12, v1}, Lcom/google/android/apps/youtube/core/identity/ar;-><init>(Lcom/google/android/apps/youtube/core/identity/l;)V

    new-instance v3, Lcom/google/android/apps/youtube/api/ab;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->k:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/j;->m:Lcom/google/android/apps/youtube/core/converter/n;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/j;->x:Lcom/google/android/apps/youtube/api/a;

    sget-object v10, Lcom/google/android/apps/youtube/api/j;->c:Ljava/util/List;

    sget-object v11, Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;->V_2_1:Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;

    move-object v8, v15

    invoke-direct/range {v3 .. v12}, Lcom/google/android/apps/youtube/api/ab;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/core/async/GDataRequestFactory;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Ljava/util/List;Lcom/google/android/apps/youtube/core/async/GDataRequest$Version;Lcom/google/android/apps/youtube/core/identity/ar;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/j;->q:Lcom/google/android/apps/youtube/api/ab;

    new-instance v1, Lcom/google/android/apps/youtube/core/utils/a;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->y:Lcom/google/android/apps/youtube/medialib/a;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/medialib/a;->b()Lcom/google/android/apps/youtube/medialib/player/ae;

    move-result-object v3

    invoke-static/range {p1 .. p1}, Lcom/google/android/youtube/player/internal/b/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    invoke-direct {v1, v0, v3, v4}, Lcom/google/android/apps/youtube/core/utils/a;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/medialib/player/ae;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->r:Lcom/google/android/apps/youtube/core/utils/a;

    new-instance v1, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->z:Lcom/google/android/apps/youtube/core/Analytics;

    invoke-direct {v1, v3, v4}, Lcom/google/android/apps/youtube/core/player/DefaultPlaybackMonitor;-><init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/Analytics;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->t:Lcom/google/android/apps/youtube/core/player/ad;

    new-instance v1, Lcom/google/android/apps/youtube/core/client/i;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/apps/youtube/core/client/i;-><init>(Lcom/google/android/apps/youtube/common/e/b;Landroid/content/SharedPreferences;Lcom/google/android/apps/youtube/common/network/h;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->r:Lcom/google/android/apps/youtube/core/utils/a;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/utils/a;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v1

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/core/client/i;->b(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v1

    invoke-static/range {p1 .. p1}, Lcom/google/android/apps/youtube/core/utils/Util;->a(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/core/client/i;->a(Ljava/lang/String;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->t:Lcom/google/android/apps/youtube/core/player/ad;

    invoke-virtual {v1, v3}, Lcom/google/android/apps/youtube/core/client/i;->a(Lcom/google/android/apps/youtube/core/player/ad;)Lcom/google/android/apps/youtube/core/client/i;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/i;->a()Lcom/google/android/apps/youtube/core/client/h;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/google/android/apps/youtube/api/j;->s:Lcom/google/android/apps/youtube/core/client/h;

    new-instance v3, Lcom/google/android/apps/youtube/core/client/r;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->l:Lorg/apache/http/client/HttpClient;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/api/j;->m:Lcom/google/android/apps/youtube/core/converter/n;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    invoke-direct/range {v3 .. v8}, Lcom/google/android/apps/youtube/core/client/r;-><init>(Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/apps/youtube/core/converter/n;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->s:Lcom/google/android/apps/youtube/core/client/h;

    invoke-virtual {v3, v1}, Lcom/google/android/apps/youtube/core/client/r;->a(Lcom/google/android/apps/youtube/core/client/h;)Lcom/google/android/apps/youtube/core/client/r;

    move-result-object v1

    move-object/from16 v0, p0

    invoke-virtual {v1, v0}, Lcom/google/android/apps/youtube/core/client/r;->a(Lcom/google/android/apps/youtube/core/player/fetcher/d;)Lcom/google/android/apps/youtube/core/client/r;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/apps/youtube/core/client/r;->a()Lcom/google/android/apps/youtube/core/client/q;

    move-result-object v1

    new-instance v3, Lcom/google/android/apps/youtube/core/client/e;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    invoke-direct {v3, v1, v4}, Lcom/google/android/apps/youtube/core/client/e;-><init>(Lcom/google/android/apps/youtube/core/client/AdsClient;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/j;->u:Lcom/google/android/apps/youtube/core/client/e;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/youtube/player/internal/b/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, "/player."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/c;->a(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/c;->a(Lcom/google/android/apps/youtube/common/e/b;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/player/c;->a(Lcom/google/android/apps/youtube/common/network/h;)V

    new-instance v3, Lcom/google/android/apps/youtube/datalib/e/b;

    sget-object v4, Lcom/google/android/apps/youtube/api/j;->e:Ljava/util/List;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/apps/youtube/api/j;->x:Lcom/google/android/apps/youtube/api/a;

    sget-object v7, Lcom/google/android/apps/youtube/datalib/offline/j;->a:Lcom/google/android/apps/youtube/datalib/offline/j;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/j;->A:Lcom/google/android/apps/youtube/core/au;

    new-instance v10, Lcom/google/android/apps/youtube/datalib/config/d;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v10, v1}, Lcom/google/android/apps/youtube/datalib/config/d;-><init>(Landroid/content/ContentResolver;)V

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/api/j;->J:Ljava/util/concurrent/Executor;

    move-object v6, v2

    invoke-direct/range {v3 .. v11}, Lcom/google/android/apps/youtube/datalib/e/b;-><init>(Ljava/util/List;Lcom/google/android/apps/youtube/core/async/DeviceAuthorizerForV2Apis;Lcom/android/volley/l;Lcom/google/android/apps/youtube/datalib/offline/j;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/au;Lcom/google/android/apps/youtube/datalib/config/c;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/apps/youtube/api/j;->H:Lcom/google/android/apps/youtube/datalib/e/b;

    return-void

    :cond_2
    const/4 v5, 0x0

    goto/16 :goto_0
.end method

.method static synthetic a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/apps/youtube/api/j;
    .locals 4

    new-instance v1, Lcom/google/android/apps/youtube/api/n;

    invoke-direct {v1, p2, p3, p1}, Lcom/google/android/apps/youtube/api/n;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/apps/youtube/api/j;->a:Ljava/util/Map;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/j;

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/j;

    if-eqz v0, :cond_1

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->g:Lcom/google/android/apps/youtube/api/n;

    invoke-virtual {v2, v1}, Lcom/google/android/apps/youtube/api/n;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    sget-object v2, Lcom/google/android/apps/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :goto_0
    sget-object v2, Lcom/google/android/apps/youtube/api/j;->a:Ljava/util/Map;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/apps/youtube/api/j;

    invoke-direct {v0, p0, p4, v1}, Lcom/google/android/apps/youtube/api/j;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/apps/youtube/api/n;)V

    goto :goto_0
.end method

.method public static a(Ljava/lang/Exception;)Lcom/google/android/youtube/player/YouTubeInitializationResult;
    .locals 1

    instance-of v0, p0, Lcom/google/android/apps/youtube/core/utils/PackageUtil$IllegalPackageSignatureException;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->INVALID_APPLICATION_SIGNATURE:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    :goto_0
    return-object v0

    :cond_0
    instance-of v0, p0, Lcom/google/android/apps/youtube/api/ApiDeviceRegistrationClientForV2Apis$InvalidDeveloperException;

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->DEVELOPER_KEY_INVALID:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    goto :goto_0

    :cond_1
    instance-of v0, p0, Ljava/net/UnknownHostException;

    if-nez v0, :cond_2

    instance-of v0, p0, Ljava/net/SocketException;

    if-nez v0, :cond_2

    instance-of v0, p0, Lorg/apache/http/client/HttpResponseException;

    if-eqz v0, :cond_3

    :cond_2
    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->NETWORK_ERROR:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/youtube/player/YouTubeInitializationResult;->INTERNAL_ERROR:Lcom/google/android/youtube/player/YouTubeInitializationResult;

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/j;)V
    .locals 1

    iget v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/api/m;)V
    .locals 2

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->x:Lcom/google/android/apps/youtube/api/a;

    new-instance v1, Lcom/google/android/apps/youtube/api/l;

    invoke-direct {v1, p0, p1}, Lcom/google/android/apps/youtube/api/l;-><init>(Lcom/google/android/apps/youtube/api/j;Lcom/google/android/apps/youtube/api/m;)V

    invoke-virtual {v0, v1}, Lcom/google/android/apps/youtube/api/a;->a(Lcom/google/android/apps/youtube/api/f;)V

    return-void
.end method

.method public static a(Lcom/google/android/apps/youtube/api/m;Landroid/os/Handler;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    new-instance v0, Lcom/google/android/apps/youtube/api/k;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/apps/youtube/api/k;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/api/m;)V

    invoke-virtual {p1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 3

    sget-object v0, Lcom/google/android/apps/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/apps/youtube/api/j;

    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->g:Lcom/google/android/apps/youtube/api/n;

    iget-object v1, v1, Lcom/google/android/apps/youtube/api/n;->a:Ljava/lang/String;

    invoke-virtual {p0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Ljava/util/concurrent/atomic/AtomicReference;->compareAndSet(Ljava/lang/Object;Ljava/lang/Object;)Z

    :cond_0
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;
    .locals 18

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/PackageUtil;->e(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;->TV:Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

    :goto_0
    new-instance v5, Lcom/google/android/apps/youtube/core/client/DeviceClassification;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ".api"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/youtube/player/internal/b/b;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;->OTHERAPP:Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;

    invoke-direct {v5, v2, v3, v1, v4}, Lcom/google/android/apps/youtube/core/client/DeviceClassification;-><init>(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;Lcom/google/android/apps/youtube/core/client/DeviceClassification$SoftwareInterface;)V

    new-instance v15, Lcom/google/android/apps/youtube/core/client/d;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->H:Lcom/google/android/apps/youtube/datalib/e/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->H:Lcom/google/android/apps/youtube/datalib/e/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    invoke-direct {v15, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/d;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/c/a;)V

    new-instance v16, Lcom/google/android/apps/youtube/core/client/bf;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->i:Landroid/os/Handler;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->o:Lcom/google/android/apps/youtube/datalib/innertube/m;

    move-object/from16 v0, v16

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/bf;-><init>(Lcom/google/android/apps/youtube/common/e/b;Ljava/util/concurrent/Executor;Landroid/os/Handler;Lcom/google/android/apps/youtube/datalib/innertube/m;)V

    new-instance v17, Lcom/google/android/apps/youtube/core/client/bs;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->y:Lcom/google/android/apps/youtube/medialib/a;

    invoke-virtual {v3}, Lcom/google/android/apps/youtube/medialib/a;->a()Lcom/google/android/apps/youtube/medialib/a/a;

    move-result-object v3

    new-instance v4, Lcom/google/android/apps/youtube/core/utils/t;

    invoke-direct {v4}, Lcom/google/android/apps/youtube/core/utils/t;-><init>()V

    move-object/from16 v0, v17

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/apps/youtube/core/client/bs;-><init>(Landroid/content/Context;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/medialib/a/a;Lcom/google/android/apps/youtube/core/utils/o;)V

    new-instance v14, Lcom/google/android/apps/youtube/core/client/bx;

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->H:Lcom/google/android/apps/youtube/datalib/e/b;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->r:Lcom/google/android/apps/youtube/core/utils/a;

    invoke-direct {v14, v1, v2, v5, v3}, Lcom/google/android/apps/youtube/core/client/bx;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/core/utils/a;)V

    new-instance v1, Lcom/google/android/apps/youtube/core/client/cc;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/apps/youtube/api/j;->H:Lcom/google/android/apps/youtube/datalib/e/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/apps/youtube/api/j;->y:Lcom/google/android/apps/youtube/medialib/a;

    invoke-virtual {v6}, Lcom/google/android/apps/youtube/medialib/a;->a()Lcom/google/android/apps/youtube/medialib/a/a;

    move-result-object v6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/apps/youtube/core/client/cc;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/DeviceClassification;Lcom/google/android/apps/youtube/medialib/a/a;)V

    new-instance v6, Lcom/google/android/apps/youtube/core/client/ch;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/apps/youtube/api/j;->H:Lcom/google/android/apps/youtube/datalib/e/b;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/j;->z:Lcom/google/android/apps/youtube/core/Analytics;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    move-object v12, v5

    invoke-direct/range {v6 .. v12}, Lcom/google/android/apps/youtube/core/client/ch;-><init>(Lcom/google/android/apps/youtube/datalib/e/b;Lcom/google/android/apps/youtube/common/e/b;Lcom/google/android/apps/youtube/core/Analytics;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/core/client/DeviceClassification;)V

    invoke-static {}, Lcom/google/android/apps/youtube/core/client/bv;->a()Lcom/google/android/apps/youtube/core/client/bv;

    move-result-object v13

    new-instance v7, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;

    new-instance v8, Ljava/security/SecureRandom;

    invoke-direct {v8}, Ljava/security/SecureRandom;-><init>()V

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    move-object v10, v15

    move-object/from16 v11, v16

    move-object/from16 v12, v17

    move-object v15, v1

    move-object/from16 v16, v6

    invoke-direct/range {v7 .. v16}, Lcom/google/android/apps/youtube/core/player/PlaybackClientManager;-><init>(Ljava/util/Random;Lcom/google/android/apps/youtube/common/network/h;Lcom/google/android/apps/youtube/core/client/d;Lcom/google/android/apps/youtube/core/client/bf;Lcom/google/android/apps/youtube/core/client/bs;Lcom/google/android/apps/youtube/core/client/bv;Lcom/google/android/apps/youtube/core/client/bx;Lcom/google/android/apps/youtube/core/client/cc;Lcom/google/android/apps/youtube/core/client/ch;)V

    return-object v7

    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/apps/youtube/api/j;->f:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/apps/youtube/core/utils/l;->a(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;->TABLET:Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

    goto/16 :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;->MOBILE:Lcom/google/android/apps/youtube/core/client/DeviceClassification$Platform;

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;
    .locals 10

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/j;->n:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/j;->p:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/j;->q:Lcom/google/android/apps/youtube/api/ab;

    invoke-static {}, Lcom/google/android/apps/youtube/core/identity/l;->a()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/youtube/core/offline/store/b;

    invoke-direct {v6}, Lcom/google/android/apps/youtube/core/offline/store/b;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    new-instance v8, Lcom/google/android/apps/youtube/core/client/a;

    invoke-direct {v8, p1}, Lcom/google/android/apps/youtube/core/client/a;-><init>(Lcom/google/android/apps/youtube/datalib/legacy/model/VastAd;)V

    iget-object v9, p0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;-><init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/e/b;)V

    return-object v0
.end method

.method public final a(Z)V
    .locals 2

    iget v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    if-lez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/apps/youtube/common/fromguava/c;->b(Z)V

    iget v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    iget v0, p0, Lcom/google/android/apps/youtube/api/j;->L:I

    if-gtz v0, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/api/j;->a:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/j;->g:Lcom/google/android/apps/youtube/api/n;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz p1, :cond_0

    sget-object v0, Lcom/google/android/apps/youtube/api/j;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p0}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    :cond_0
    return-void

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Lcom/google/android/apps/youtube/common/e/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    return-object v0
.end method

.method public final c()Ljava/util/concurrent/Executor;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    return-object v0
.end method

.method public final d()Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->K:Lcom/google/android/apps/youtube/datalib/innertube/model/media/j;

    return-object v0
.end method

.method public final e()Lcom/google/android/apps/youtube/common/c/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    return-object v0
.end method

.method public final f()Lcom/google/android/apps/youtube/core/client/bc;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->q:Lcom/google/android/apps/youtube/api/ab;

    return-object v0
.end method

.method public final g()Lcom/google/android/apps/youtube/core/client/e;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->u:Lcom/google/android/apps/youtube/core/client/e;

    return-object v0
.end method

.method public final h()Lcom/google/android/apps/youtube/core/client/bj;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->v:Lcom/google/android/apps/youtube/core/client/bj;

    return-object v0
.end method

.method public final i()Lcom/google/android/apps/youtube/core/client/ce;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->w:Lcom/google/android/apps/youtube/core/client/ce;

    return-object v0
.end method

.method public final j()Lcom/google/android/apps/youtube/medialib/a;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->y:Lcom/google/android/apps/youtube/medialib/a;

    return-object v0
.end method

.method public final k()Lcom/google/android/apps/youtube/core/Analytics;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->z:Lcom/google/android/apps/youtube/core/Analytics;

    return-object v0
.end method

.method public final l()Lcom/google/android/apps/youtube/core/au;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->A:Lcom/google/android/apps/youtube/core/au;

    return-object v0
.end method

.method public final m()Lcom/google/android/apps/youtube/core/aw;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->G:Lcom/google/android/apps/youtube/core/aw;

    return-object v0
.end method

.method public final n()Landroid/content/SharedPreferences;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->B:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method public final o()Lcom/google/android/apps/youtube/common/network/h;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->C:Lcom/google/android/apps/youtube/common/network/h;

    return-object v0
.end method

.method public final p()Lcom/google/android/apps/youtube/datalib/offline/n;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->D:Lcom/google/android/apps/youtube/datalib/offline/n;

    return-object v0
.end method

.method public final q()Lcom/google/android/apps/youtube/core/offline/store/q;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->E:Lcom/google/android/apps/youtube/core/offline/store/q;

    return-object v0
.end method

.method public final r()Lcom/google/android/apps/youtube/core/player/w;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->p:Lcom/google/android/apps/youtube/core/player/w;

    return-object v0
.end method

.method public final s()Lcom/google/android/apps/youtube/core/player/ad;
    .locals 1

    iget-object v0, p0, Lcom/google/android/apps/youtube/api/j;->t:Lcom/google/android/apps/youtube/core/player/ad;

    return-object v0
.end method

.method public final t()Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;
    .locals 10

    new-instance v0, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;

    iget-object v1, p0, Lcom/google/android/apps/youtube/api/j;->I:Lcom/google/android/apps/youtube/common/c/a;

    iget-object v2, p0, Lcom/google/android/apps/youtube/api/j;->n:Lcom/google/android/apps/youtube/datalib/innertube/ah;

    iget-object v3, p0, Lcom/google/android/apps/youtube/api/j;->p:Lcom/google/android/apps/youtube/core/player/w;

    iget-object v4, p0, Lcom/google/android/apps/youtube/api/j;->q:Lcom/google/android/apps/youtube/api/ab;

    invoke-static {}, Lcom/google/android/apps/youtube/core/identity/l;->a()Lcom/google/android/apps/youtube/core/identity/l;

    move-result-object v5

    new-instance v6, Lcom/google/android/apps/youtube/core/offline/store/b;

    invoke-direct {v6}, Lcom/google/android/apps/youtube/core/offline/store/b;-><init>()V

    iget-object v7, p0, Lcom/google/android/apps/youtube/api/j;->j:Ljava/util/concurrent/Executor;

    new-instance v8, Lcom/google/android/apps/youtube/core/client/n;

    iget-object v9, p0, Lcom/google/android/apps/youtube/api/j;->s:Lcom/google/android/apps/youtube/core/client/h;

    invoke-direct {v8, v9}, Lcom/google/android/apps/youtube/core/client/n;-><init>(Lcom/google/android/apps/youtube/core/client/h;)V

    iget-object v9, p0, Lcom/google/android/apps/youtube/api/j;->h:Lcom/google/android/apps/youtube/common/e/n;

    invoke-direct/range {v0 .. v9}, Lcom/google/android/apps/youtube/core/player/fetcher/PlayerFetcher;-><init>(Lcom/google/android/apps/youtube/common/c/a;Lcom/google/android/apps/youtube/datalib/innertube/ah;Lcom/google/android/apps/youtube/core/player/w;Lcom/google/android/apps/youtube/core/client/bc;Lcom/google/android/apps/youtube/core/identity/l;Lcom/google/android/apps/youtube/core/offline/store/q;Ljava/util/concurrent/Executor;Lcom/google/android/apps/youtube/datalib/innertube/ag;Lcom/google/android/apps/youtube/common/e/b;)V

    return-object v0
.end method
